#!/bin/sh

# Author : Andrzej
# Script to set up development enviroment

SCRIPT_DIR="$(dirname $(realpath -s "$0"))"
echo "Changing folder to project root" && cd ${SCRIPT_DIR}/../

case "$1" in
    "start-incomplete")
      echo "##### Started setting up WarMaker test enviroment #####"
      echo "##### Starting Postgresql database #####"
      sudo docker-compose -f src/main/docker/postgresql.yml up -d
      echo "##### Starting Jhipster Registry #####"
      docker-compose -f src/main/docker/jhipster-registry.yml up -d
      echo "##### Starting Kafka #####"
      docker-compose -f src/main/docker/kafka.yml up -d
      exit 0
    ;;

    "start")
      echo "##### Started setting up WarMaker test environment #####"
      echo "##### Starting Postgresql database #####"
      sudo docker-compose -f src/main/docker/postgresql.yml up -d
      echo "##### Starting Jhipster Registry #####"
      docker-compose -f src/main/docker/jhipster-registry.yml up -d
      echo "##### Starting Kafka #####"
      docker-compose -f src/main/docker/kafka.yml up -d
      echo "##### Starting Message Box #####"
      if [ -d "../message-box" ]
      then
          echo "##### MessageBox directory exists, starting application #####"
          echo "##### Starting MessageBox MongoDb #####"
          docker-compose -f ../message-box/src/main/docker/mongodb.yml up -d
          echo "##### Starting MessageBox #####"
          ../message-box/gradlew -p "../message-box" bootRun
      else
          echo "##### MessageBox directory doesn't exists, please clone it from Gitlab! #####"
      fi
      exit 0
    ;;

    "stop")
      echo "##### Started cleaning up WarMaker test environment #####"
      echo "##### Tearing down postgresql database #####"
      sudo docker-compose -f src/main/docker/postgresql.yml down
      echo "##### Tearing down Kafka #####"
      docker-compose -f src/main/docker/kafka.yml down
      echo "##### Tearing down Jhipster Registry #####"
      docker-compose -f /src/main/docker/jhipster-registry.yml down
      exit 0
    ;;
esac

exit 1
