# WarMaker Changelog

# 0.5.0 [21-01-2020]
## Overall
- Initial WarMaker release

## Added
1. City development
    * Resources income
    * Buildings upgrade
        * Resources Buildings
        * Army Buildings
        * Technology Buildings
2. Army development
    * Multiple types of troops recruitment
    * War Parties and Army creation
3. World Map
    * 2d tile-based map
    * Real time War Parties position indication
    * Attack, station and move commands
    * Hostile PvE characters
    * Player Alliances
4. Other
    * Events message Box
