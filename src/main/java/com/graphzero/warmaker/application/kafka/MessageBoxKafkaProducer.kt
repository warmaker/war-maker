package com.graphzero.warmaker.application.kafka

import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.player.WarDeclaredEvent
import com.graphzero.warmaker.application.warparty.battle.WarPartyBattleEvent
import io.vavr.control.Either
import org.apache.kafka.clients.producer.RecordMetadata
import org.springframework.stereotype.Service
import java.util.concurrent.Future

@Service
class MessageBoxKafkaProducer(private val kafkaMessageProducer: KafkaMessageProducer) {
    fun sendBattleMessage(message: WarPartyBattleEvent): Either<Future<RecordMetadata>, Exception> {
        return kafkaMessageProducer.send(BATTLE_TOPIC, createMessage(message))
    }

    fun sendWarDeclarationMessage(message: WarDeclaredEvent): Either<Future<RecordMetadata>, Exception> {
        return kafkaMessageProducer.send(WAR_DECLARATION_TOPIC, createMessage(message))
    }

    private fun createMessage(message: DomainEvent<*>): String {
        return message.dto().toJson()
    }

    companion object {
        private const val BATTLE_TOPIC = "battle_topic"
        private const val WAR_DECLARATION_TOPIC = "war_declaration_topic"
    }
}
