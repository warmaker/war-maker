package com.graphzero.warmaker.application.kafka

import com.graphzero.warmaker.application.kafka.WarMakerKafkaProducer
import com.graphzero.warmaker.config.KafkaProperties
import io.vavr.control.Either
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.clients.producer.RecordMetadata
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.concurrent.Future

@Service
class KafkaMessageProducer(private val kafkaProperties: KafkaProperties) {
    private var kafkaProducer: KafkaProducer<String, String>? = null
    private val log = LoggerFactory.getLogger(WarMakerKafkaProducer::class.java)

    fun init() {
        log.info("Kafka producer initializing...")
        kafkaProducer = KafkaProducer(kafkaProperties.producerProps)
        Runtime.getRuntime().addShutdownHook(Thread { shutdown() })
        log.info("Kafka producer initialized")
    }

    fun send(topic: String, message: String): Either<Future<RecordMetadata>, Exception> {
        val record = ProducerRecord<String, String>(topic, message)
        return try {
            Either.left(kafkaProducer!!.send(record))
        } catch (e: Exception) {
            log.error(e.message, e)
            Either.right(e)
        }
    }

    private fun shutdown() {
        log.info("Shutdown Kafka producer")
        kafkaProducer!!.close()
    }
}
