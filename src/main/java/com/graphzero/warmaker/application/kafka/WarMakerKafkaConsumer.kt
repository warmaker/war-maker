package com.graphzero.warmaker.application.kafka

import com.graphzero.warmaker.application.kafka.WarMakerKafkaConsumer
import com.graphzero.warmaker.config.KafkaProperties
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.errors.WakeupException
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.Duration
import java.util.concurrent.atomic.AtomicBoolean

@Service
class WarMakerKafkaConsumer(private val kafkaProperties: KafkaProperties) {
    private val log = LoggerFactory.getLogger(WarMakerKafkaConsumer::class.java)
    private val closed = AtomicBoolean(false)
    private var kafkaConsumer: KafkaConsumer<String, String>? = null

    fun start() {
        log.info("Kafka consumer starting...")
        kafkaConsumer = KafkaConsumer(kafkaProperties.consumerProps)
        Runtime.getRuntime().addShutdownHook(Thread { shutdown() })
        val consumerThread = Thread {
            try {
                kafkaConsumer!!.subscribe(listOf(TOPIC))
                log.info("Kafka consumer started")
                while (!closed.get()) {
                    val records = kafkaConsumer!!.poll(Duration.ofSeconds(3))
                    for (record in records) {
                        log.info("Consumed message in {} : {}", TOPIC, record.value())
                    }
                }
                kafkaConsumer!!.commitSync()
            } catch (e: WakeupException) {
                // Ignore exception if closing
                if (!closed.get()) throw e
            } catch (e: Exception) {
                log.error(e.message, e)
            } finally {
                kafkaConsumer!!.close()
            }
        }
        consumerThread.start()
    }

    private fun shutdown() {
        log.info("Shutdown Kafka consumer")
        closed.set(true)
        kafkaConsumer!!.wakeup()
    }

    companion object {
        const val TOPIC = "topic_warmaker"
    }
}
