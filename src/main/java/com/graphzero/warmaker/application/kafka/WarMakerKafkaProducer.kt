package com.graphzero.warmaker.application.kafka

import com.graphzero.warmaker.application.kafka.WarMakerKafkaProducer
import com.graphzero.warmaker.config.KafkaProperties
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class WarMakerKafkaProducer(private val kafkaProperties: KafkaProperties) {
    private val log = LoggerFactory.getLogger(WarMakerKafkaProducer::class.java)
    private var kafkaProducer: KafkaProducer<String, String>? = null
    fun init() {
        log.info("Kafka producer initializing...")
        kafkaProducer = KafkaProducer(kafkaProperties.producerProps)
        Runtime.getRuntime().addShutdownHook(Thread { shutdown() })
        log.info("Kafka producer initialized")
    }

    fun send(message: String) {
        val record = ProducerRecord<String, String>(TOPIC, message)
        try {
            kafkaProducer!!.send(record)
        } catch (e: Exception) {
            log.error(e.message, e)
        }
    }

    private fun shutdown() {
        log.info("Shutdown Kafka producer")
        kafkaProducer!!.close()
    }

    companion object {
        private const val TOPIC = "topic_warmaker"
    }
}
