package com.graphzero.warmaker.application.resources

interface ResourcesAmount {
    val denars: Double
    val wood: Double
    val stone: Double
    val steel: Double
    val food: Double
    val faith: Double
    fun substract(amount: ResourcesAmount): ResourcesAmount
    fun add(amount: ResourcesAmount): ResourcesAmount
    fun reverse(): ResourcesAmount
}
