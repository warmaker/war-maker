package com.graphzero.warmaker.application.resources

import com.graphzero.warmaker.domain.ResourcesEntity
import java.util.*

/**
 * Immutable class representing value of resources, not correlated with any any table in database
 */
class ResourcesValue(
    override val denars: Double,
    override val wood: Double,
    override val stone: Double,
    override val steel: Double,
    override val food: Double,
    override val faith: Double
) : ResourcesAmount {
    override fun add(resources: ResourcesAmount): ResourcesValue {
        return ResourcesValue(
            denars + resources.denars,
            wood + resources.wood,
            stone + resources.stone,
            steel + resources.steel,
            food + resources.food,
            faith + resources.faith
        )
    }

    override fun reverse(): ResourcesValue {
        return ResourcesValue(
            -denars,
            -wood,
            -stone,
            -steel,
            -food,
            -faith
        )
    }

    override fun substract(resources: ResourcesAmount): ResourcesValue {
        return ResourcesValue(
            denars - resources.denars,
            wood - resources.wood,
            stone - resources.stone,
            steel - resources.steel,
            food - resources.food,
            faith - resources.faith
        )
    }

    override fun toString(): String {
        return "ResourcesValue{" +
                "denars=" + denars +
                ", wood=" + wood +
                ", stone=" + stone +
                ", steel=" + steel +
                ", food=" + food +
                ", faith=" + faith +
                '}'
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ResourcesValue

        if (denars != other.denars) return false
        if (wood != other.wood) return false
        if (stone != other.stone) return false
        if (steel != other.steel) return false
        if (food != other.food) return false
        if (faith != other.faith) return false

        return true
    }

    override fun hashCode(): Int {
        var result = denars.hashCode()
        result = 31 * result + wood.hashCode()
        result = 31 * result + stone.hashCode()
        result = 31 * result + steel.hashCode()
        result = 31 * result + food.hashCode()
        result = 31 * result + faith.hashCode()
        return result
    }

    companion object {
        fun of(resources: ResourcesValue): ResourcesValue {
            return ResourcesValue(
                resources.denars,
                resources.wood,
                resources.stone,
                resources.steel,
                resources.food,
                resources.faith
            )
        }

        fun of(resources: ResourcesEntity): ResourcesValue {
            return ResourcesValue(
                resources.denars,
                resources.wood,
                resources.stone,
                resources.steel,
                resources.food,
                resources.faith
            )
        }

        @kotlin.jvm.JvmStatic
        fun of(resources: Resources): ResourcesValue {
            return ResourcesValue(
                resources.denars,
                resources.wood,
                resources.stone,
                resources.steel,
                resources.food,
                resources.faith
            )
        }

        @kotlin.jvm.JvmStatic
        fun empty(): ResourcesValue {
            return ResourcesValue(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        }
    }
}
