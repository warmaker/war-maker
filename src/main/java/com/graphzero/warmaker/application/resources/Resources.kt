package com.graphzero.warmaker.application.resources

import com.graphzero.warmaker.domain.ResourcesEntity
import java.util.*

/**
 * Class representing resources with their presence in DB
 */
class Resources(
    val id: Long,
    override var denars: Double,
    override var wood: Double,
    override var stone: Double,
    override var steel: Double,
    override var food: Double,
    override var faith: Double
) : ResourcesAmount {
    fun collect(denars: Int, wood: Int, stone: Int, steel: Int, food: Int) {
        this.denars += denars.toDouble()
        this.wood += wood.toDouble()
        this.stone += stone.toDouble()
        this.steel += steel.toDouble()
        this.food += food.toDouble()
    }

    fun collect(loot: Resources) {
        denars += loot.denars
        wood += loot.wood
        stone += loot.stone
        steel += loot.steel
        food += loot.food
    }

    fun collect(resourcesValue: ResourcesValue) {
        denars += resourcesValue.denars
        wood += resourcesValue.wood
        stone += resourcesValue.stone
        steel += resourcesValue.steel
        food += resourcesValue.food
    }

    fun plunder() {
        denars = 0.0
        wood = 0.0
        stone = 0.0
        steel = 0.0
        food = 0.0
        faith = 0.0
    }

    fun value(): ResourcesValue {
        return ResourcesValue(denars, wood, stone, steel, food, 0.0)
    }

    override fun substract(amount: ResourcesAmount): Resources {
        denars -= amount.denars
        wood -= amount.wood
        stone -= amount.stone
        steel -= amount.steel
        food -= amount.food
        faith -= amount.faith
        return this
    }

    override fun add(amount: ResourcesAmount): Resources {
        denars += amount.denars
        wood += amount.wood
        stone += amount.stone
        steel += amount.steel
        food += amount.food
        faith += amount.faith
        return this
    }

    override fun reverse(): Resources {
        return Resources(
            id,
            -denars,
            -wood,
            -stone,
            -steel,
            -food,
            -faith
        )
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Resources

        if (id != other.id) return false
        if (denars != other.denars) return false
        if (wood != other.wood) return false
        if (stone != other.stone) return false
        if (steel != other.steel) return false
        if (food != other.food) return false
        if (faith != other.faith) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + denars.hashCode()
        result = 31 * result + wood.hashCode()
        result = 31 * result + stone.hashCode()
        result = 31 * result + steel.hashCode()
        result = 31 * result + food.hashCode()
        result = 31 * result + faith.hashCode()
        return result
    }


    companion object {
        fun of(resources: ResourcesEntity): Resources {
            return Resources(
                resources.id,
                resources.denars,
                resources.wood,
                resources.stone,
                resources.steel,
                resources.food,
                resources.faith
            )
        }

        @kotlin.jvm.JvmStatic
        fun emptyResources(): Resources {
            return Resources(-1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        }
    }
}
