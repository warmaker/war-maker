package com.graphzero.warmaker.application.time

import java.time.Clock
import java.time.ZoneId

object TimeConfiguration {
    private const val ZONE_ID = "Europe/Warsaw"
    val serverClock = Clock.system(ZoneId.of(ZONE_ID))!!
}
