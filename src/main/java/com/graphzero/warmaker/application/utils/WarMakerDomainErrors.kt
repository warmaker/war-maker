package com.graphzero.warmaker.application.utils

enum class WarMakerDomainErrors(val errorMessage: String) {
    WAR_PARTY_NOT_FOUND("War Party couldn't be found"),
    LAIR_NOT_FOUND("Lair couldn't be found"),
    WRONG_COMMAND("Not valid command"),
    WAR_PARTY_ALREADY_MOVING("War Party is already moving");
}
