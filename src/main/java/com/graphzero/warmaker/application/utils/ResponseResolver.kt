package com.graphzero.warmaker.application.utils

import io.vavr.control.Either
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

class ResponseResolver<E : WarMakerDomainException, T> {
    fun resolve(either: Either<E, T>): ResponseEntity<*> {
        return either
            .map { `object`: T -> createObject(`object`) }
            .getOrElseGet { error: E -> createError(error) }
    }

    private fun createObject(`object`: T): ResponseEntity<Any> {
        return ResponseEntity(`object`, HttpStatus.OK)
    }

    private fun createError(error: E): ResponseEntity<Any> {
        return ResponseEntity(
            error.errorReason.errorMessage, HTTP_STATUS_MAP.getOrElse(
                error.errorReason,
                { HttpStatus.BAD_REQUEST }
            )
        )
    }

    companion object {
        private val HTTP_STATUS_MAP: Map<WarMakerDomainErrors, HttpStatus> = hashMapOf(
            Pair(WarMakerDomainErrors.WAR_PARTY_ALREADY_MOVING, HttpStatus.BAD_REQUEST),
            Pair(WarMakerDomainErrors.WAR_PARTY_NOT_FOUND, HttpStatus.BAD_REQUEST),
            Pair(WarMakerDomainErrors.WRONG_COMMAND, HttpStatus.BAD_REQUEST)
        )
    }
}
