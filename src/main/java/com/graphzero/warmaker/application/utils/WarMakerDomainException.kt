package com.graphzero.warmaker.application.utils

open class WarMakerDomainException : RuntimeException {
    val errorReason: WarMakerDomainErrors

    constructor(errorReason: WarMakerDomainErrors) {
        this.errorReason = errorReason
    }

    constructor(s: String, errorReason: WarMakerDomainErrors) : super(s) {
        this.errorReason = errorReason
    }

    constructor(s: String, throwable: Throwable, errorReason: WarMakerDomainErrors) : super(s, throwable) {
        this.errorReason = errorReason
    }
}
