package com.graphzero.warmaker.application.utils

open class WarMakerApplicationError : RuntimeException {
    constructor(s: String) : super(s)
    constructor(s: String, throwable: Throwable) : super(s, throwable)
}
