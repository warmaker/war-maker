package com.graphzero.warmaker.application.utils

import org.apache.logging.log4j.message.FormattedMessage

class MissingEntityException : WarMakerApplicationError {
    private val clazz: Class<*>

    constructor(clazz: Class<*>, entityId: Long) : super(
        FormattedMessage(
            ERROR_MESSAGE,
            clazz.name,
            entityId
        ).formattedMessage
    ) {
        this.clazz = clazz
    }

    constructor(throwable: Throwable, clazz: Class<*>, entityId: Long) : super(
        FormattedMessage(
            ERROR_MESSAGE,
            clazz.name,
            entityId
        ).formattedMessage, throwable
    ) {
        this.clazz = clazz
    }

    companion object {
        private const val ERROR_MESSAGE = "Missing entity [{}] with id: [{}]"
    }
}
