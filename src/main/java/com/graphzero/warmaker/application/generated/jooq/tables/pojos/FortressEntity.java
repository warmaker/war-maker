/*
 * This file is generated by jOOQ.
 */
package jooq.tables.pojos;


import java.io.Serializable;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class FortressEntity implements Serializable {

    private static final long serialVersionUID = -1137853957;

    private final Long    id;
    private final String  fortressName;
    private final Integer warBarracks;
    private final Integer wall;
    private final Integer moat;

    public FortressEntity(FortressEntity value) {
        this.id = value.id;
        this.fortressName = value.fortressName;
        this.warBarracks = value.warBarracks;
        this.wall = value.wall;
        this.moat = value.moat;
    }

    public FortressEntity(
        Long    id,
        String  fortressName,
        Integer warBarracks,
        Integer wall,
        Integer moat
    ) {
        this.id = id;
        this.fortressName = fortressName;
        this.warBarracks = warBarracks;
        this.wall = wall;
        this.moat = moat;
    }

    public Long getId() {
        return this.id;
    }

    public String getFortressName() {
        return this.fortressName;
    }

    public Integer getWarBarracks() {
        return this.warBarracks;
    }

    public Integer getWall() {
        return this.wall;
    }

    public Integer getMoat() {
        return this.moat;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("FortressEntity (");

        sb.append(id);
        sb.append(", ").append(fortressName);
        sb.append(", ").append(warBarracks);
        sb.append(", ").append(wall);
        sb.append(", ").append(moat);

        sb.append(")");
        return sb.toString();
    }
}
