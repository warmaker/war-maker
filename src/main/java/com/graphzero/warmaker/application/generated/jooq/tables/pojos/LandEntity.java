/*
 * This file is generated by jOOQ.
 */
package jooq.tables.pojos;


import java.io.Serializable;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class LandEntity implements Serializable {

    private static final long serialVersionUID = -1044032686;

    private final Long    id;
    private final Integer road;
    private final Integer scoutTower;
    private final Long    fortressId;
    private final Long    armyId;
    private final Long    locationId;
    private final Long    ownerId;

    public LandEntity(LandEntity value) {
        this.id = value.id;
        this.road = value.road;
        this.scoutTower = value.scoutTower;
        this.fortressId = value.fortressId;
        this.armyId = value.armyId;
        this.locationId = value.locationId;
        this.ownerId = value.ownerId;
    }

    public LandEntity(
        Long    id,
        Integer road,
        Integer scoutTower,
        Long    fortressId,
        Long    armyId,
        Long    locationId,
        Long    ownerId
    ) {
        this.id = id;
        this.road = road;
        this.scoutTower = scoutTower;
        this.fortressId = fortressId;
        this.armyId = armyId;
        this.locationId = locationId;
        this.ownerId = ownerId;
    }

    public Long getId() {
        return this.id;
    }

    public Integer getRoad() {
        return this.road;
    }

    public Integer getScoutTower() {
        return this.scoutTower;
    }

    public Long getFortressId() {
        return this.fortressId;
    }

    public Long getArmyId() {
        return this.armyId;
    }

    public Long getLocationId() {
        return this.locationId;
    }

    public Long getOwnerId() {
        return this.ownerId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("LandEntity (");

        sb.append(id);
        sb.append(", ").append(road);
        sb.append(", ").append(scoutTower);
        sb.append(", ").append(fortressId);
        sb.append(", ").append(armyId);
        sb.append(", ").append(locationId);
        sb.append(", ").append(ownerId);

        sb.append(")");
        return sb.toString();
    }
}
