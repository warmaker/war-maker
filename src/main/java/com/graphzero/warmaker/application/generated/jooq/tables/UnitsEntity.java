/*
 * This file is generated by jOOQ.
 */
package jooq.tables;


import java.util.Arrays;
import java.util.List;

import jooq.Keys;
import jooq.Public;
import jooq.tables.records.UnitsEntityRecord;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row5;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UnitsEntity extends TableImpl<UnitsEntityRecord> {

    private static final long serialVersionUID = -1208875703;

    /**
     * The reference instance of <code>public.units_entity</code>
     */
    public static final UnitsEntity UNITS_ENTITY = new UnitsEntity();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<UnitsEntityRecord> getRecordType() {
        return UnitsEntityRecord.class;
    }

    /**
     * The column <code>public.units_entity.id</code>.
     */
    public final TableField<UnitsEntityRecord, Long> ID = createField(DSL.name("id"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>public.units_entity.spearmen</code>.
     */
    public final TableField<UnitsEntityRecord, Integer> SPEARMEN = createField(DSL.name("spearmen"), org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>public.units_entity.swordsmen</code>.
     */
    public final TableField<UnitsEntityRecord, Integer> SWORDSMEN = createField(DSL.name("swordsmen"), org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>public.units_entity.archers</code>.
     */
    public final TableField<UnitsEntityRecord, Integer> ARCHERS = createField(DSL.name("archers"), org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>public.units_entity.crossbowmen</code>.
     */
    public final TableField<UnitsEntityRecord, Integer> CROSSBOWMEN = createField(DSL.name("crossbowmen"), org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * Create a <code>public.units_entity</code> table reference
     */
    public UnitsEntity() {
        this(DSL.name("units_entity"), null);
    }

    /**
     * Create an aliased <code>public.units_entity</code> table reference
     */
    public UnitsEntity(String alias) {
        this(DSL.name(alias), UNITS_ENTITY);
    }

    /**
     * Create an aliased <code>public.units_entity</code> table reference
     */
    public UnitsEntity(Name alias) {
        this(alias, UNITS_ENTITY);
    }

    private UnitsEntity(Name alias, Table<UnitsEntityRecord> aliased) {
        this(alias, aliased, null);
    }

    private UnitsEntity(Name alias, Table<UnitsEntityRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    public <O extends Record> UnitsEntity(Table<O> child, ForeignKey<O, UnitsEntityRecord> key) {
        super(child, key, UNITS_ENTITY);
    }

    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    @Override
    public UniqueKey<UnitsEntityRecord> getPrimaryKey() {
        return Keys.UNITS_ENTITY_PKEY;
    }

    @Override
    public List<UniqueKey<UnitsEntityRecord>> getKeys() {
        return Arrays.<UniqueKey<UnitsEntityRecord>>asList(Keys.UNITS_ENTITY_PKEY);
    }

    @Override
    public UnitsEntity as(String alias) {
        return new UnitsEntity(DSL.name(alias), this);
    }

    @Override
    public UnitsEntity as(Name alias) {
        return new UnitsEntity(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public UnitsEntity rename(String name) {
        return new UnitsEntity(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public UnitsEntity rename(Name name) {
        return new UnitsEntity(name, null);
    }

    // -------------------------------------------------------------------------
    // Row5 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row5<Long, Integer, Integer, Integer, Integer> fieldsRow() {
        return (Row5) super.fieldsRow();
    }
}
