/*
 * This file is generated by jOOQ.
 */
package jooq.tables;


import java.util.Arrays;
import java.util.List;

import jooq.Keys;
import jooq.Public;
import jooq.tables.records.LairEntityRecord;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row5;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class LairEntity extends TableImpl<LairEntityRecord> {

    private static final long serialVersionUID = -597717051;

    /**
     * The reference instance of <code>public.lair_entity</code>
     */
    public static final LairEntity LAIR_ENTITY = new LairEntity();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<LairEntityRecord> getRecordType() {
        return LairEntityRecord.class;
    }

    /**
     * The column <code>public.lair_entity.id</code>.
     */
    public final TableField<LairEntityRecord, Long> ID = createField(DSL.name("id"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>public.lair_entity.lair_name</code>.
     */
    public final TableField<LairEntityRecord, String> LAIR_NAME = createField(DSL.name("lair_name"), org.jooq.impl.SQLDataType.VARCHAR(255), this, "");

    /**
     * The column <code>public.lair_entity.treasure_id</code>.
     */
    public final TableField<LairEntityRecord, Long> TREASURE_ID = createField(DSL.name("treasure_id"), org.jooq.impl.SQLDataType.BIGINT, this, "");

    /**
     * The column <code>public.lair_entity.garrison_id</code>.
     */
    public final TableField<LairEntityRecord, Long> GARRISON_ID = createField(DSL.name("garrison_id"), org.jooq.impl.SQLDataType.BIGINT, this, "");

    /**
     * The column <code>public.lair_entity.location_id</code>.
     */
    public final TableField<LairEntityRecord, Long> LOCATION_ID = createField(DSL.name("location_id"), org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * Create a <code>public.lair_entity</code> table reference
     */
    public LairEntity() {
        this(DSL.name("lair_entity"), null);
    }

    /**
     * Create an aliased <code>public.lair_entity</code> table reference
     */
    public LairEntity(String alias) {
        this(DSL.name(alias), LAIR_ENTITY);
    }

    /**
     * Create an aliased <code>public.lair_entity</code> table reference
     */
    public LairEntity(Name alias) {
        this(alias, LAIR_ENTITY);
    }

    private LairEntity(Name alias, Table<LairEntityRecord> aliased) {
        this(alias, aliased, null);
    }

    private LairEntity(Name alias, Table<LairEntityRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    public <O extends Record> LairEntity(Table<O> child, ForeignKey<O, LairEntityRecord> key) {
        super(child, key, LAIR_ENTITY);
    }

    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    @Override
    public UniqueKey<LairEntityRecord> getPrimaryKey() {
        return Keys.LAIR_ENTITY_PKEY;
    }

    @Override
    public List<UniqueKey<LairEntityRecord>> getKeys() {
        return Arrays.<UniqueKey<LairEntityRecord>>asList(Keys.LAIR_ENTITY_PKEY, Keys.UX_LAIR_ENTITY_TREASURE_ID, Keys.UX_LAIR_ENTITY_GARRISON_ID, Keys.UX_LAIR_ENTITY_LOCATION_ID);
    }

    @Override
    public List<ForeignKey<LairEntityRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<LairEntityRecord, ?>>asList(Keys.LAIR_ENTITY__FK_LAIR_ENTITY_TREASURE_ID, Keys.LAIR_ENTITY__FK_LAIR_ENTITY_GARRISON_ID, Keys.LAIR_ENTITY__FK_LAIR_ENTITY_LOCATION_ID);
    }

    public ResourcesEntity resourcesEntity() {
        return new ResourcesEntity(this, Keys.LAIR_ENTITY__FK_LAIR_ENTITY_TREASURE_ID);
    }

    public WarPartyEntity warPartyEntity() {
        return new WarPartyEntity(this, Keys.LAIR_ENTITY__FK_LAIR_ENTITY_GARRISON_ID);
    }

    public FieldEntity fieldEntity() {
        return new FieldEntity(this, Keys.LAIR_ENTITY__FK_LAIR_ENTITY_LOCATION_ID);
    }

    @Override
    public LairEntity as(String alias) {
        return new LairEntity(DSL.name(alias), this);
    }

    @Override
    public LairEntity as(Name alias) {
        return new LairEntity(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public LairEntity rename(String name) {
        return new LairEntity(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public LairEntity rename(Name name) {
        return new LairEntity(name, null);
    }

    // -------------------------------------------------------------------------
    // Row5 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row5<Long, String, Long, Long, Long> fieldsRow() {
        return (Row5) super.fieldsRow();
    }
}
