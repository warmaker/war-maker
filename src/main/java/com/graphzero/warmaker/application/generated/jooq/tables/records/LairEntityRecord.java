/*
 * This file is generated by jOOQ.
 */
package jooq.tables.records;


import jooq.tables.LairEntity;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record5;
import org.jooq.Row5;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class LairEntityRecord extends UpdatableRecordImpl<LairEntityRecord> implements Record5<Long, String, Long, Long, Long> {

    private static final long serialVersionUID = -386112264;

    /**
     * Setter for <code>public.lair_entity.id</code>.
     */
    public LairEntityRecord setId(Long value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>public.lair_entity.id</code>.
     */
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>public.lair_entity.lair_name</code>.
     */
    public LairEntityRecord setLairName(String value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>public.lair_entity.lair_name</code>.
     */
    public String getLairName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.lair_entity.treasure_id</code>.
     */
    public LairEntityRecord setTreasureId(Long value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>public.lair_entity.treasure_id</code>.
     */
    public Long getTreasureId() {
        return (Long) get(2);
    }

    /**
     * Setter for <code>public.lair_entity.garrison_id</code>.
     */
    public LairEntityRecord setGarrisonId(Long value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>public.lair_entity.garrison_id</code>.
     */
    public Long getGarrisonId() {
        return (Long) get(3);
    }

    /**
     * Setter for <code>public.lair_entity.location_id</code>.
     */
    public LairEntityRecord setLocationId(Long value) {
        set(4, value);
        return this;
    }

    /**
     * Getter for <code>public.lair_entity.location_id</code>.
     */
    public Long getLocationId() {
        return (Long) get(4);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record5 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row5<Long, String, Long, Long, Long> fieldsRow() {
        return (Row5) super.fieldsRow();
    }

    @Override
    public Row5<Long, String, Long, Long, Long> valuesRow() {
        return (Row5) super.valuesRow();
    }

    @Override
    public Field<Long> field1() {
        return LairEntity.LAIR_ENTITY.ID;
    }

    @Override
    public Field<String> field2() {
        return LairEntity.LAIR_ENTITY.LAIR_NAME;
    }

    @Override
    public Field<Long> field3() {
        return LairEntity.LAIR_ENTITY.TREASURE_ID;
    }

    @Override
    public Field<Long> field4() {
        return LairEntity.LAIR_ENTITY.GARRISON_ID;
    }

    @Override
    public Field<Long> field5() {
        return LairEntity.LAIR_ENTITY.LOCATION_ID;
    }

    @Override
    public Long component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getLairName();
    }

    @Override
    public Long component3() {
        return getTreasureId();
    }

    @Override
    public Long component4() {
        return getGarrisonId();
    }

    @Override
    public Long component5() {
        return getLocationId();
    }

    @Override
    public Long value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getLairName();
    }

    @Override
    public Long value3() {
        return getTreasureId();
    }

    @Override
    public Long value4() {
        return getGarrisonId();
    }

    @Override
    public Long value5() {
        return getLocationId();
    }

    @Override
    public LairEntityRecord value1(Long value) {
        setId(value);
        return this;
    }

    @Override
    public LairEntityRecord value2(String value) {
        setLairName(value);
        return this;
    }

    @Override
    public LairEntityRecord value3(Long value) {
        setTreasureId(value);
        return this;
    }

    @Override
    public LairEntityRecord value4(Long value) {
        setGarrisonId(value);
        return this;
    }

    @Override
    public LairEntityRecord value5(Long value) {
        setLocationId(value);
        return this;
    }

    @Override
    public LairEntityRecord values(Long value1, String value2, Long value3, Long value4, Long value5) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached LairEntityRecord
     */
    public LairEntityRecord() {
        super(LairEntity.LAIR_ENTITY);
    }

    /**
     * Create a detached, initialised LairEntityRecord
     */
    public LairEntityRecord(Long id, String lairName, Long treasureId, Long garrisonId, Long locationId) {
        super(LairEntity.LAIR_ENTITY);

        set(0, id);
        set(1, lairName);
        set(2, treasureId);
        set(3, garrisonId);
        set(4, locationId);
    }
}
