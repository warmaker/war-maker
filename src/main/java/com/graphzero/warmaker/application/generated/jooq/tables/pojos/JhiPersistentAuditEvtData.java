/*
 * This file is generated by jOOQ.
 */
package jooq.tables.pojos;


import java.io.Serializable;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JhiPersistentAuditEvtData implements Serializable {

    private static final long serialVersionUID = 134377462;

    private final Long   eventId;
    private final String name;
    private final String value;

    public JhiPersistentAuditEvtData(JhiPersistentAuditEvtData value) {
        this.eventId = value.eventId;
        this.name = value.name;
        this.value = value.value;
    }

    public JhiPersistentAuditEvtData(
        Long   eventId,
        String name,
        String value
    ) {
        this.eventId = eventId;
        this.name = name;
        this.value = value;
    }

    public Long getEventId() {
        return this.eventId;
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("JhiPersistentAuditEvtData (");

        sb.append(eventId);
        sb.append(", ").append(name);
        sb.append(", ").append(value);

        sb.append(")");
        return sb.toString();
    }
}
