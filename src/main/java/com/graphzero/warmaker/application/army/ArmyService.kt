package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.application.events.domain.DomainEventDto
import com.graphzero.warmaker.application.events.domain.DomainEventsPublisher
import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.application.warparty.WarPartyFactory
import com.graphzero.warmaker.application.warparty.WarPartyRootRepository
import com.graphzero.warmaker.application.warparty.battle.WarPartyBattleEvent
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import com.graphzero.warmaker.application.warparty.value.WarPartyAnnihilatedEvent
import com.graphzero.warmaker.application.warparty.value.WarPartyResourcesChangeEvent
import com.graphzero.warmaker.config.ApplicationProperties
import com.graphzero.warmaker.domain.ArmyEntity
import com.graphzero.warmaker.domain.WarPartyEntity
import com.graphzero.warmaker.shared.annotations.ApplicationService
import io.vavr.control.Try
import org.springframework.transaction.annotation.Transactional
import java.time.ZonedDateTime
import java.util.*
import java.util.stream.Collectors

@ApplicationService
internal class ArmyService(
    private val armyRepository: ArmyRootRepository,
    private val armyEventsRepository: ArmyEventsRepository,
    private val warPartyRootRepository: WarPartyRootRepository,
    private val armyFactory: ArmyFactory,
    private val eventsPublisher: DomainEventsPublisher,
    private val warPartyFactory: WarPartyFactory,
    private val applicationProperties: ApplicationProperties
) {
    /**
     * Implements the lazy loading idea of recruitment logic.
     *
     * @param cityId
     * @return
     */
    @Transactional
    fun getCityArmy(cityId: Long): ArmyDto {
        val army = armyFactory.buildFromCityId(cityId)
        armyRepository.persistArmy(army)
        eventsPublisher.publish(army.getDomainEvents())
        return army.value()
    }

    @Transactional
    fun createWarParty(armyId: Long, units: HumanUnitsList): Try<WarPartyEntity> {
        val army = armyFactory.buildFromArmyId(armyId)
        return army
            .createWarParty(units, applicationProperties.getUnits())
            .map { warParty: WarParty ->
                armyRepository.persistArmy(army)
                warPartyRootRepository.save(armyId, warParty)
            }
    }

    fun getArmyEvents(armyId: Long): List<DomainEventDto> {
        return armyEventsRepository.getAllEventsForArmy(armyId)
            .stream()
            .map { obj: ArmyDomainEvent -> obj.dto() }
            .collect(Collectors.toList())
    }

    fun removeWarPartyIfArmyExists(event: WarPartyAnnihilatedEvent) {
        val warPartyEntity = warPartyRootRepository.repository.findById(event.warPartyId).get()
        Optional.ofNullable(warPartyEntity.armyEntity)
            .ifPresentOrElse(
                { armyEntity: ArmyEntity ->
                    armyEntity.warParties.remove(warPartyEntity)
                    warPartyRootRepository.remove(event.warPartyId)
                    armyRepository.armyEntityRepository.save(armyEntity)
                }
            ) {
                val warParty = warPartyFactory.buildWarParty(event.warPartyId, ZonedDateTime.now())
                warPartyRootRepository.persistUnitsUpdate(warParty.id, warParty.unitsList.units)
            }
    }

    fun handleWarPartyBattle(event: WarPartyBattleEvent) {
        val warParty = warPartyFactory.buildWarParty(event.warParty.id, ZonedDateTime.now())
        warPartyRootRepository.persistUnitsUpdate(warParty.id, event.remainingUnits)
    }

    fun handleWarPartyResourcesChange(event: WarPartyResourcesChangeEvent) {
        warPartyRootRepository.persistResourcesUpdate(event.warPartyId, event.resourcesChange)
    }
}
