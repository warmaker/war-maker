package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.repository.ArmyEntityRepository
import com.graphzero.warmaker.repository.UnitsEntityRepository
import org.springframework.stereotype.Service

@Service
internal class ArmyRootRepository(
    val armyEntityRepository: ArmyEntityRepository,
    private val unitsEntityRepository: UnitsEntityRepository
) {
    fun persistArmy(army: Army) {
        val armyEntity = armyEntityRepository.findById(army.id).get()
        val units = armyEntity.units
        units.spearmen = army.armyUnits.availableUnits.units.spearman
        units.swordsmen = army.armyUnits.availableUnits.units.swordsman
        units.archers = army.armyUnits.availableUnits.units.archers
        units.crossbowmen = army.armyUnits.availableUnits.units.crossbowman
        unitsEntityRepository.save(units)
        armyEntityRepository.save(armyEntity)
    }
}
