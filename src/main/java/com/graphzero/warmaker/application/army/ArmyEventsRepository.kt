package com.graphzero.warmaker.application.army

import java.util.*

interface ArmyEventsRepository {
    fun getAllEventsForArmy(armyId: Long): List<ArmyDomainEvent>
    fun getAllUnitsRecruitmentEvents(armyId: Long): List<StartedUnitsRecruitmentEvent>
    fun addEvent(event: ArmyDomainEvent): Boolean
    fun remove(armyId: Long, uuid: UUID): Boolean
    fun isEventQueued(armyId: Long, uuid: UUID): Boolean
}
