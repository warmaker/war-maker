package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.application.warparty.value.HumanUnitsList

data class ArmyUnitsDto(
    val id: Long,
    val available: HumanUnitsList,
    val stationed: HumanUnitsList,
    val fielded: HumanUnitsList?
)
