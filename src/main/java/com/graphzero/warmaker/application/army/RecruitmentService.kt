package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.shared.annotations.ApplicationService
import org.springframework.transaction.annotation.Transactional

@ApplicationService
@Transactional
internal class RecruitmentService(private val armyEventsRepository: ArmyEventsRepository) {
    /**
     * Adds event to a repository so it can be later lazily computed
     *
     * @param event
     */
    fun startUnitsRecruitment(event: StartedUnitsRecruitmentEvent) {
        armyEventsRepository.addEvent(event)
    }

    fun finishRecruitment(event: FinishedUnitsRecruitmentEvent) {
        armyEventsRepository.remove(event.armyId, event.recruitUnitsEventIdentifier)
        armyEventsRepository.remove(event.armyId, event.uuid)
    }
}
