package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.events.domain.DomainEventDto

import java.util.*

internal class FinishedUnitsRecruitmentEvent(
    source: Any, uuid: UUID,
    override val armyId: Long,
    val recruitUnitsEventIdentifier: UUID
) : DomainEvent<WarMakerArmyEvent>(source, WarMakerArmyEvent.FINISHED_UNITS_RECRUITMENT, uuid), ArmyDomainEvent {
    override val type: WarMakerArmyEvent
        get() = WarMakerArmyEvent.FINISHED_UNITS_RECRUITMENT

    override fun dto(): FinishedUnitsRecruitmentDto {
        return FinishedUnitsRecruitmentDto(uuid, warMakerEventType)
    }

    internal class FinishedUnitsRecruitmentDto(uuid: UUID, type: WarMakerEventType) : DomainEventDto(uuid, type)
}
