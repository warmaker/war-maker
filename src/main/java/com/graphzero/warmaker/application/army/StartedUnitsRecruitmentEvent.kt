package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.application.army.value.RecruitmentOrders
import com.graphzero.warmaker.application.cities.CityDomainEvent
import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEventDto
import com.graphzero.warmaker.application.time.TimeConfiguration
import java.time.ZonedDateTime
import java.util.*

class StartedUnitsRecruitmentEvent(
    source: Any,
    uuid: UUID,
    cityId: Long,
    override val armyId: Long,
    val recruitmentOrders: RecruitmentOrders,
    var lastReadTime: ZonedDateTime,
    val orderTime: ZonedDateTime = ZonedDateTime.now(TimeConfiguration.serverClock)
) : CityDomainEvent<WarMakerArmyEvent>(source, uuid, WarMakerArmyEvent.UNITS_RECRUITED, cityId), ArmyDomainEvent {


    var finishTime: ZonedDateTime
    override val type: WarMakerArmyEvent
        get() = WarMakerArmyEvent.UNITS_RECRUITED

    override fun dto(): StartedUnitsRecruitmentEventDto {
        return StartedUnitsRecruitmentEventDto(
            uuid,
            warMakerEventType,
            recruitmentOrders,
            orderTime,
            finishTime,
            armyId,
            lastReadTime
        )
    }

    fun setFinishTime(finishTime: ZonedDateTime): StartedUnitsRecruitmentEvent {
        this.finishTime = finishTime
        return this
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as StartedUnitsRecruitmentEvent

        if (armyId != other.armyId) return false
        if (recruitmentOrders != other.recruitmentOrders) return false
        if (lastReadTime != other.lastReadTime) return false
        if (orderTime != other.orderTime) return false
        if (finishTime != other.finishTime) return false

        return true
    }

    override fun hashCode(): Int {
        var result = armyId.hashCode()
        result = 31 * result + recruitmentOrders.hashCode()
        result = 31 * result + lastReadTime.hashCode()
        result = 31 * result + orderTime.hashCode()
        result = 31 * result + finishTime.hashCode()
        return result
    }

    class StartedUnitsRecruitmentEventDto(
        uuid: UUID, type: WarMakerEventType,
        val recruitmentOrders: RecruitmentOrders,
        val orderTime: ZonedDateTime,
        val finishTime: ZonedDateTime,
        val armyId: Long,
        val lastReadTime: ZonedDateTime
    ) : DomainEventDto(uuid, type) {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as StartedUnitsRecruitmentEventDto

            if (recruitmentOrders != other.recruitmentOrders) return false
            if (orderTime != other.orderTime) return false
            if (finishTime != other.finishTime) return false
            if (armyId != other.armyId) return false
            if (lastReadTime != other.lastReadTime) return false

            return true
        }

        override fun hashCode(): Int {
            var result = recruitmentOrders.hashCode()
            result = 31 * result + orderTime.hashCode()
            result = 31 * result + finishTime.hashCode()
            result = 31 * result + armyId.hashCode()
            result = 31 * result + lastReadTime.hashCode()
            return result
        }
    }

    init {
        finishTime = ZonedDateTime.now(TimeConfiguration.serverClock)
    }


}
