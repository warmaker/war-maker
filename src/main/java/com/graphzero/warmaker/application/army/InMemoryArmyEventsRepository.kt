package com.graphzero.warmaker.application.army

import com.google.common.collect.Lists
import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import org.springframework.stereotype.Repository
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.stream.Collectors

@Repository
internal class InMemoryArmyEventsRepository : ArmyEventsRepository {
    private val events = ConcurrentHashMap<Long, MutableList<ArmyDomainEvent>>()

    override fun getAllEventsForArmy(armyId: Long): List<ArmyDomainEvent> {
        return events.getOrDefault(armyId, ArrayList())
    }

    override fun getAllUnitsRecruitmentEvents(armyId: Long): List<StartedUnitsRecruitmentEvent> {
        return events
            .getOrDefault(armyId, ArrayList())
            .stream()
            .filter { event: ArmyDomainEvent -> event.type == WarMakerArmyEvent.UNITS_RECRUITED }
            .map { event: ArmyDomainEvent -> event as StartedUnitsRecruitmentEvent }
            .collect(Collectors.toList())
    }

    override fun addEvent(event: ArmyDomainEvent): Boolean {
        if (events.containsKey(event.armyId)) {
            events[event.armyId]!!.add(event)
        } else {
            events[event.armyId] = Lists.newArrayList(event)
        }
        return true
    }

    override fun remove(armyId: Long, uuid: UUID): Boolean {
        synchronized(events) {
            return if (events.containsKey(armyId)) {
                events[armyId]!!
                    .removeIf { armyDomainEvent: ArmyDomainEvent -> armyDomainEvent.uuid == uuid }
                true
            } else {
                false
            }
        }
    }

    override fun isEventQueued(armyId: Long, uuid: UUID): Boolean {
        return if (events.containsKey(armyId)) {
            events[armyId]!!
                .stream()
                .anyMatch { event: ArmyDomainEvent -> event.uuid == uuid }
        } else {
            false
        }
    }
}
