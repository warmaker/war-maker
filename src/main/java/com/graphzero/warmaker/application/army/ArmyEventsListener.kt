package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.application.warparty.battle.WarPartyBattleEvent
import com.graphzero.warmaker.application.warparty.value.WarPartyAnnihilatedEvent
import com.graphzero.warmaker.application.warparty.value.WarPartyResourcesChangeEvent
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
internal class ArmyEventsListener(private val armyService: ArmyService) {
    private val log = LoggerFactory.getLogger(ArmyEventsListener::class.java)

    @EventListener
    fun handleWarPartyAnnihilation(event: WarPartyAnnihilatedEvent) {
        log.debug("War Party [{}] has been annihilated", event.warPartyId)
        armyService.removeWarPartyIfArmyExists(event)
    }

    @EventListener
    fun handleWarPartyBattle(event: WarPartyBattleEvent) {
        log.debug("War Party [{}] has battled it's loses are [{}]", event.warParty.id, event.unitsLoses)
        armyService.handleWarPartyBattle(event)
    }

    @EventListener
    fun handleWarPartyResourcesChange(event: WarPartyResourcesChangeEvent) {
        log.debug(
            "War Party [{}] resources have changed. Resources change is [{}]",
            event.warPartyId,
            event.resourcesChange
        )
        armyService.handleWarPartyResourcesChange(event)
    }
}
