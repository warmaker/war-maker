package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.config.ApplicationProperties
import com.graphzero.warmaker.config.ApplicationProperties.Units.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UnitsConfig @Autowired constructor(applicationProperties: ApplicationProperties) {
    companion object {
        var units: ApplicationProperties.Units? = null
            private set
        var spearmanConfig: Spearman? = null
            private set
        var swordsmanConfig: Swordsman? = null
            private set
        var archerConfig: Archer? = null
            private set
        var crossbowmanConfig: Crossbowman? = null
            private set
    }

    init {
        units = applicationProperties.getUnits()
        spearmanConfig = applicationProperties.getUnits().getSpearman()
        swordsmanConfig = applicationProperties.getUnits().getSwordsman()
        archerConfig = applicationProperties.getUnits().getArcher()
        crossbowmanConfig = applicationProperties.getUnits().getCrossbowman()
    }
}
