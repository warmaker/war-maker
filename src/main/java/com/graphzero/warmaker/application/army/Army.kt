package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.application.army.value.RecruitUnitOrder
import com.graphzero.warmaker.application.ddd.AggregateRoot
import com.graphzero.warmaker.application.ddd.AggregateRootLogic
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.time.TimeConfiguration
import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.application.warparty.builders.WarPartyBuilder
import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import com.graphzero.warmaker.application.world.Coordinates
import com.graphzero.warmaker.config.ApplicationProperties
import com.graphzero.warmaker.domain.enumeration.WarPartyState
import io.vavr.collection.Stream
import io.vavr.control.Try
import java.time.ZonedDateTime
import java.util.*
import java.util.function.Consumer
import kotlin.math.floor

class Army : AggregateRoot {
    private val aggregateRoot: AggregateRootLogic
    private val unitsRecruitmentEvents: List<StartedUnitsRecruitmentEvent>
    val armyUnits: ArmyUnits
    private val lastRecruitmentTime: ZonedDateTime
    private val coordinates: Coordinates
    private var availableRecruitmentTime: Long = 0

    constructor(
        id: Long,
        armyUnits: ArmyUnits,
        unitsRecruitmentEvents: List<StartedUnitsRecruitmentEvent>,
        coordinates: Coordinates
    ) {
        aggregateRoot = AggregateRootLogic(id)
        this.armyUnits = armyUnits
        this.unitsRecruitmentEvents = unitsRecruitmentEvents
        this.coordinates = coordinates
        lastRecruitmentTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        handleUnitsRecruitment()
    }

    constructor(
        id: Long, armyUnits: ArmyUnits,
        unitsRecruitmentEvents: List<StartedUnitsRecruitmentEvent>,
        lastRecruitmentTime: ZonedDateTime,
        coordinates: Coordinates
    ) {
        aggregateRoot = AggregateRootLogic(id)
        this.armyUnits = armyUnits
        this.lastRecruitmentTime = lastRecruitmentTime
        this.unitsRecruitmentEvents = unitsRecruitmentEvents
        this.coordinates = coordinates
        handleUnitsRecruitment()
    }

    private fun handleUnitsRecruitment() {
        if (unitsRecruitmentEvents.isNotEmpty()) {
            unitsRecruitmentEvents.sortedBy { it.orderTime }
            availableRecruitmentTime =
                lastRecruitmentTime.toEpochSecond() - unitsRecruitmentEvents[0].lastReadTime.toEpochSecond()
            Stream.ofAll(
                unitsRecruitmentEvents
                    .stream()
            )
                .takeUntil { event: StartedUnitsRecruitmentEvent -> recruitUnits(event) }
                .forEach(Consumer { change: StartedUnitsRecruitmentEvent ->
                    if (change.recruitmentOrders.isFinished) registerDomainEvent(
                        FinishedUnitsRecruitmentEvent(this, UUID.randomUUID(), aggregateRoot.id!!, change.uuid)
                    )
                })
            unitsRecruitmentEvents.forEach(Consumer { event: StartedUnitsRecruitmentEvent ->
                event.lastReadTime = lastRecruitmentTime
            })
        }
    }

    /**
     * Returns whether further recruitment is possible
     *
     * @param event
     * @return
     */
    private fun recruitUnits(event: StartedUnitsRecruitmentEvent): Boolean {
        event.lastReadTime = lastRecruitmentTime
        val spearmanRecruitmentReport = recruit(event.recruitmentOrders.recruitSpearman, availableRecruitmentTime)
        availableRecruitmentTime -= spearmanRecruitmentReport.timeSpendOnRecruitment
        val swordsmanRecruitmentReport = recruit(event.recruitmentOrders.recruitSwordsman, availableRecruitmentTime)
        availableRecruitmentTime -= swordsmanRecruitmentReport.timeSpendOnRecruitment
        val archersRecruitmentReport = recruit(event.recruitmentOrders.recruitArchers, availableRecruitmentTime)
        availableRecruitmentTime -= archersRecruitmentReport.timeSpendOnRecruitment
        val crossbowmanRecruitmentReport = recruit(event.recruitmentOrders.recruitCrossbowman, availableRecruitmentTime)
        armyUnits.recruitUnits(
            HumanUnitsList(
                spearmanRecruitmentReport.recruitedUnits,
                swordsmanRecruitmentReport.recruitedUnits,
                archersRecruitmentReport.recruitedUnits,
                crossbowmanRecruitmentReport.recruitedUnits
            )
        )
        return availableRecruitmentTime < 0
    }

    private fun recruit(order: RecruitUnitOrder, availableRecruitmentTime: Long): RecruitmentReport {
        if (!order.isFinished) {
            val currentUnitRecruitment = (order.timeSpendOnRecruiting % order.timeToRecruitOneUnit).toInt()
            val remainingRecruitmentTime = order.remainingRecruitmentTime
            val timeSpendOnRecruitment: Long = if (availableRecruitmentTime - remainingRecruitmentTime >= 0) {
                order.recruit(remainingRecruitmentTime)
            } else {
                order.recruit(availableRecruitmentTime)
            }
            val recruitedUnits =
                floor(((currentUnitRecruitment + timeSpendOnRecruitment) / order.timeToRecruitOneUnit).toDouble())
                    .toInt()
            return RecruitmentReport(recruitedUnits, timeSpendOnRecruitment)
        }
        return RecruitmentReport(0, 0L)
    }

    fun createWarParty(units: HumanUnitsList, unitsConfig: ApplicationProperties.Units): Try<WarParty> {
        return armyUnits.removeUnits(units)
            .map {
                WarPartyBuilder()
                    .state(WarPartyState.STATIONED)
                    .unitsList(HumanUnits.of(units))
                    .carriedResources(Resources.emptyResources())
                    .location(coordinates)
                    .fieldEntryDate(ZonedDateTime.now(TimeConfiguration.serverClock))
                    .owner(null)
                    .unitsConfig(unitsConfig)
                    .build()
            }
    }

    fun value(): ArmyDto {
        return ArmyDto(aggregateRoot.id!!, armyUnits.value())
    }

    override val id: Long
        get() = aggregateRoot.id!!

    override fun registerDomainEvent(event: DomainEvent<out WarMakerEventType>) {
        aggregateRoot.registerDomainEvent(event)
    }

    override fun clearEvents() {
        aggregateRoot.clearEvents()
    }

    override fun getDomainEvents(): List<DomainEvent<out WarMakerEventType>> {
        return aggregateRoot.getDomainEvents()
    }

    private class RecruitmentReport(val recruitedUnits: Int, val timeSpendOnRecruitment: Long)
}
