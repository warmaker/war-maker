package com.graphzero.warmaker.application.army.value

import java.util.*

class RecruitUnitOrder(val numberOfUnitsToRecruit: Int, val timeToRecruitOneUnit: Int) {
    var timeSpendOnRecruiting: Long = 0
        private set
    val isEmpty: Boolean
        get() = numberOfUnitsToRecruit == 0
    val isFinished: Boolean
        get() = totalRecruitmentTime == timeSpendOnRecruiting

    /**
     * Minus long means recruit all order
     *
     * @param timePassed
     * @return
     */
    fun recruit(timePassed: Long): Long {
        timeSpendOnRecruiting += timePassed
        return timePassed
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RecruitUnitOrder

        if (numberOfUnitsToRecruit != other.numberOfUnitsToRecruit) return false
        if (timeToRecruitOneUnit != other.timeToRecruitOneUnit) return false
        if (timeSpendOnRecruiting != other.timeSpendOnRecruiting) return false

        return true
    }

    override fun hashCode(): Int {
        var result = numberOfUnitsToRecruit
        result = 31 * result + timeToRecruitOneUnit
        result = 31 * result + timeSpendOnRecruiting.hashCode()
        return result
    }

    val totalRecruitmentTime: Long
        get() = numberOfUnitsToRecruit * timeToRecruitOneUnit.toLong()
    val remainingRecruitmentTime: Long
        get() = totalRecruitmentTime - timeSpendOnRecruiting

}
