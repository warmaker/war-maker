package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.events.domain.DomainEventDto

import java.util.*

interface ArmyDomainEvent {
    val armyId: Long
    val type: WarMakerArmyEvent
    val uuid: UUID
    fun dto(): DomainEventDto
}
