package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.domain.ArmyEntity
import com.graphzero.warmaker.repository.ArmyEntityRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*


internal interface ArmyEntityCustomRepository : ArmyEntityRepository {
    fun findByCityId(cityId: Long): Optional<ArmyEntity>

    @Query(
        "SELECT a " +
                "FROM ArmyEntity a " +
                "   JOIN FETCH a.city " +
                "   LEFT JOIN FETCH a.warParties " +
                "WHERE a.city.id = :cityId"
    )
    fun findWithEager(@Param("cityId") cityId: Long): Optional<ArmyEntity>
}
