package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.application.events.domain.DomainEventDto
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import com.graphzero.warmaker.domain.WarPartyEntity
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api/army")
internal class ArmyController(private val armyService: ArmyService) {
    private val log = LoggerFactory.getLogger(ArmyController::class.java)

    @GetMapping("/{cityId}")
    fun getArmy(@PathVariable cityId: Long): ResponseEntity<ArmyDto> {
        log.debug("REST request to get army for city {}", cityId)
        return ResponseEntity.ok(armyService.getCityArmy(cityId))
    }

    @GetMapping("/{armyId}/events")
    fun getArmyEvents(@PathVariable armyId: Long): ResponseEntity<List<DomainEventDto>> {
        return ResponseEntity.ok(armyService.getArmyEvents(armyId))
    }

    @PostMapping("create-war-party")
    fun createWarParty(
        @RequestParam armyId: Long,
        @RequestBody units: HumanUnitsList
    ): ResponseEntity<WarPartyEntity> {
        log.debug("REST request to create War Party for army [{}]", armyId)
        val warParty = armyService.createWarParty(armyId, units).get()
        return ResponseEntity.ok(warParty)
    }
}
