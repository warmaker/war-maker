package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.application.world.Coordinates.Companion.of
import com.graphzero.warmaker.domain.ArmyEntity
import com.graphzero.warmaker.domain.WarPartyEntity
import com.graphzero.warmaker.domain.enumeration.WarPartyState
import org.springframework.stereotype.Service
import java.util.stream.Collectors

@Service
internal class ArmyFactory(
    private val armyEntityRepository: ArmyEntityCustomRepository,
    private val armyEventsRepository: ArmyEventsRepository
) {
    fun buildFromCityId(cityId: Long): Army {
        val armyEntity = armyEntityRepository.findWithEager(cityId).get()
        return getArmyDomainObject(armyEntity)
    }

    fun buildFromArmyId(armyId: Long): Army {
        val armyEntity = armyEntityRepository.findById(armyId).get()
        return getArmyDomainObject(armyEntity)
    }

    private fun getArmyDomainObject(armyEntity: ArmyEntity): Army {
        val coordinates = of(armyEntity.city)
        val allUnits = armyEntity
            .warParties
            .stream()
            .collect(Collectors.groupingBy({ obj: WarPartyEntity -> obj.state }, Collectors.toSet()))
        val stationedUnits =
            allUnits.getOrDefault(WarPartyState.STATIONED, emptySet()).stream().map { obj: WarPartyEntity -> obj.units }
                .collect(Collectors.toSet())
        val fieldedUnits =
            allUnits.getOrDefault(WarPartyState.MOVING, emptySet()).stream().map { obj: WarPartyEntity -> obj.units }
                .collect(Collectors.toSet())
        return Army(
            armyEntity.id,
            ArmyUnits(
                armyEntity.units.id,
                armyEntity.units.id,
                armyEntity.units,
                stationedUnits, fieldedUnits
            ),
            armyEventsRepository.getAllUnitsRecruitmentEvents(armyEntity.id), coordinates
        )
    }
}
