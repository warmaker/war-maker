package com.graphzero.warmaker.application.army

data class ArmyDto(val id: Long, val units: ArmyUnitsDto)
