package com.graphzero.warmaker.application.army.value

import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import java.time.temporal.ChronoUnit
import java.util.*

class RecruitmentOrders(
    val recruitSpearman: RecruitUnitOrder,
    val recruitSwordsman: RecruitUnitOrder,
    val recruitArchers: RecruitUnitOrder,
    val recruitCrossbowman: RecruitUnitOrder
) {
    val timeUnit: ChronoUnit = ChronoUnit.SECONDS
    fun unitsChange(): HumanUnitsList {
        return HumanUnitsList(
            recruitSpearman.numberOfUnitsToRecruit, recruitSwordsman.numberOfUnitsToRecruit,
            recruitArchers.numberOfUnitsToRecruit, recruitCrossbowman.numberOfUnitsToRecruit
        )
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RecruitmentOrders

        if (recruitSpearman != other.recruitSpearman) return false
        if (recruitSwordsman != other.recruitSwordsman) return false
        if (recruitArchers != other.recruitArchers) return false
        if (recruitCrossbowman != other.recruitCrossbowman) return false
        if (timeUnit != other.timeUnit) return false

        return true
    }

    override fun hashCode(): Int {
        var result = recruitSpearman.hashCode()
        result = 31 * result + recruitSwordsman.hashCode()
        result = 31 * result + recruitArchers.hashCode()
        result = 31 * result + recruitCrossbowman.hashCode()
        result = 31 * result + timeUnit.hashCode()
        return result
    }

    val isFinished: Boolean
        get() = recruitSpearman.isFinished && recruitSwordsman.isFinished && recruitArchers.isFinished && recruitCrossbowman
            .isFinished
    val totalRecruitmentTime: Long
        get() = recruitSpearman.totalRecruitmentTime +
                recruitSwordsman.totalRecruitmentTime +
                recruitArchers.totalRecruitmentTime +
                recruitCrossbowman.totalRecruitmentTime


}
