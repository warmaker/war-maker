package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import com.graphzero.warmaker.domain.UnitsEntity
import io.vavr.control.Try

class ArmyUnits {
    private val id: Long

    /**
     * Fresh units not yet assigned to any War Party
     */
    var availableUnits: HumanUnits
        private set

    /**
     * Units assigned to certain War Party
     */
    private val stationedUnits: HumanUnits
    private val fieldedUnits: HumanUnits

    constructor(
        id: Long, stationedUnitsId: Long,
        availableUnits: UnitsEntity,
        stationedUnits: Collection<UnitsEntity>,
        fieldedUnits: Collection<UnitsEntity>
    ) {
        this.id = id
        this.availableUnits = HumanUnits(stationedUnitsId, availableUnits)
        this.stationedUnits = HumanUnits(stationedUnitsId, stationedUnits)
        this.fieldedUnits = HumanUnits(stationedUnitsId, fieldedUnits)
    }

    constructor(
        id: Long,
        availableUnits: HumanUnits,
        stationedUnits: HumanUnits,
        fieldedUnits: HumanUnits
    ) {
        this.id = id
        this.availableUnits = availableUnits
        this.stationedUnits = stationedUnits
        this.fieldedUnits = fieldedUnits
    }

    fun recruitUnits(dto: HumanUnitsList): Boolean {
        if (dto.isEmpty) return false
        availableUnits = availableUnits.addUnits(dto)
        return true
    }

    fun removeUnits(units: HumanUnitsList): Try<HumanUnits> {
        return availableUnits.removeUnits(units)
            .peek { humanUnits: HumanUnits -> availableUnits = humanUnits }
    }

    fun value(): ArmyUnitsDto {
        return ArmyUnitsDto(
            id,
            availableUnits.units.copy(),
            stationedUnits.units.copy(),
            fieldedUnits.units.copy()
        )
    }
}
