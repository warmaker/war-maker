package com.graphzero.warmaker.application.army

import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service

@Service
internal class RecruitmentEventsListener(private val recruitmentService: RecruitmentService) {
    private val log = LoggerFactory.getLogger(RecruitmentEventsListener::class.java)

    @EventListener
    fun handleUnitsRecruitmentFinished(event: FinishedUnitsRecruitmentEvent) {
        log.debug("Recruitment finished for army [{}]", event.armyId)
        recruitmentService.finishRecruitment(event)
    }

    @EventListener
    fun handleUnitsRecruited(event: StartedUnitsRecruitmentEvent) {
        log.debug("Event to recruit units for [{}]", event.armyId)
        recruitmentService.startUnitsRecruitment(event)
    }
}
