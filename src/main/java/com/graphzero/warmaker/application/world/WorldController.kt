package com.graphzero.warmaker.application.world

import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/world")
internal class WorldController(private val worldService: WorldService) {
    private val log = LoggerFactory.getLogger(WorldController::class.java)
    @GetMapping
    fun getWorld(@RequestParam playerId: Long): ResponseEntity<List<MutableList<PlayerFieldView>>> {
        log.debug("REST request to get world for player [{}]", playerId)
        return ResponseEntity.ok(worldService.getWorldFromPlayersPerspective(playerId))
    }
}
