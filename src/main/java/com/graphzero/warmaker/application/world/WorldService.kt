package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.application.researches.scouting.Scouting
import com.graphzero.warmaker.application.researches.scouting.ScoutingRepository
import org.springframework.stereotype.Service
import java.util.*
import java.util.stream.Stream

@Service
internal class WorldService(private val worldMapRepository: WorldMapRepository,
                                 private val scoutingRepository: ScoutingRepository) {

    fun getWorldFromPlayersPerspective(playerId: Long): List<MutableList<PlayerFieldView>> {
        val scoutingObservationPosition = scoutingRepository.findScoutingObservationPosition(playerId)
        val scoutingBounds = Scouting().calculateBaseScoutingBounds(scoutingObservationPosition)
        val fields = worldMapRepository.findAllFromPlayerPerspective(playerId, scoutingBounds)
        return mapWorldTo2d(fields)
    }

    private fun mapWorldTo2d(fields: List<PlayerFieldView>): List<MutableList<PlayerFieldView>> {
        val columnSize = 10
        val rows: MutableList<MutableList<PlayerFieldView>> = ArrayList()
        Stream.iterate(0, { i: Int -> i + 1 })
                .limit((fields.size / columnSize).toLong())
                .forEach { i: Int ->
                    rows.add(ArrayList())
                    rows[i]
                            .addAll(fields.subList(columnSize * i, columnSize * i + columnSize))
                }
        return rows
    }
}
