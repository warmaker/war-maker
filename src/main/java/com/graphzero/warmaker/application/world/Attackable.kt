package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.application.warparty.battle.BattleReport

interface Attackable {
    /**
     * Domain that can be attacked
     *
     * @param attacker
     */
    fun getAttacked(attacker: WarParty): BattleReport
}
