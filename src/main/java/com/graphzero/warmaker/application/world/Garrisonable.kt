package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.application.warparty.value.HumanUnits

interface Garrisonable {
    /**
     * Garrison [WarParty] in this field
     * @param garrison
     */
    fun garrison(garrison: WarParty)
    fun addUnits(units: HumanUnits)
}
