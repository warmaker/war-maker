package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.application.researches.scouting.ScoutingDetectRadius
import jooq.Tables
import org.jooq.*
import org.jooq.impl.DSL
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.util.stream.Collectors

@Repository
internal class WorldMapRepository(private val create: DSLContext) {

    // TODO add indexes to FIELD_ENTITY
    fun findAllFromPlayerPerspective(playerId: Long, scoutingDetectRadius: ScoutingDetectRadius): List<PlayerFieldView> {
        val scoutedWarPartiesSubselect = getScoutedWarPartiesSubselect(scoutingDetectRadius)
        val relationsSubselect = getRelationsSubselect(playerId)
        val result = create.select(
                Tables.FIELD_ENTITY.X_COORDINATE,
                Tables.FIELD_ENTITY.Y_COORDINATE,
                Tables.FIELD_ENTITY.TYPE,
                relationsSubselect.field(Tables.PLAYER_RELATIONS.RELATION),
                Tables.CITY_ENTITY.OWNER_ID,
                scoutedWarPartiesSubselect.field(SCOUTED_WAR_PARTIES_ID_ALIAS, Array<Long>::class.java),
                scoutedWarPartiesSubselect.field(IS_FIELD_SCOUTED_ALIAS, Boolean::class.java))
                .from(Tables.FIELD_ENTITY)
                .leftJoin(Tables.CITY_ENTITY).on(Tables.FIELD_ENTITY.ID.eq(Tables.CITY_ENTITY.LOCATION_ID))
                .leftJoin(relationsSubselect).on(Tables.CITY_ENTITY.OWNER_ID.eq(relationsSubselect.field(Tables.PLAYER_RELATIONS.TARGET_PLAYER_ID)))
                .leftJoin(scoutedWarPartiesSubselect).on(Tables.FIELD_ENTITY.ID.eq(scoutedWarPartiesSubselect.field(Tables.FIELD_ENTITY.ID)))
                .orderBy(Tables.FIELD_ENTITY.Y_COORDINATE, Tables.FIELD_ENTITY.X_COORDINATE)
        return result.map {
                record: Record7<Int, Int, String, String, Long, Array<Long>, Boolean> -> PlayerFieldView.of(record)
        }
    }

    private fun getScoutedWarPartiesSubselect(scoutingDetectRadius: ScoutingDetectRadius): SelectHavingStep<Record3<Long, Boolean, Array<Long>>> {
        return create.select(Tables.FIELD_ENTITY.ID,
                DSL.`val`(true).`as`(IS_FIELD_SCOUTED_ALIAS),
                DSL.arrayAgg(Tables.WAR_PARTY_ENTITY.ID).filterWhere(Tables.WAR_PARTY_ENTITY.ID.isNotNull).`as`(SCOUTED_WAR_PARTIES_ID_ALIAS))
                .from(Tables.WAR_PARTY_ENTITY)
                .rightJoin(Tables.FIELD_ENTITY).on(Tables.WAR_PARTY_ENTITY.POSITION_ID.eq(Tables.FIELD_ENTITY.ID))
                .where(Tables.FIELD_ENTITY.X_COORDINATE
                        .between(scoutingDetectRadius.xCoordinateBounds.lowerBound, scoutingDetectRadius.xCoordinateBounds.upperBound)
                        .and(Tables.FIELD_ENTITY.Y_COORDINATE
                                .between(scoutingDetectRadius.yCoordinatesBounds.lowerBound, scoutingDetectRadius.yCoordinatesBounds.upperBound)))
                .groupBy(Tables.FIELD_ENTITY.ID)
    }

    private fun getRelationsSubselect(playerId: Long): SelectConditionStep<Record3<Long, String, Long>> {
        return create.select(Tables.PROFILE_ENTITY.ID,
                Tables.PLAYER_RELATIONS.RELATION,
                Tables.PLAYER_RELATIONS.TARGET_PLAYER_ID)
                .from(Tables.PROFILE_ENTITY)
                .leftJoin(Tables.PLAYER_RELATIONS).on(Tables.PROFILE_ENTITY.ID.eq(Tables.PLAYER_RELATIONS.OWNER_ID))
                .where(Tables.PROFILE_ENTITY.ID.eq(playerId))
    }

    companion object {
        private const val SCOUTED_WAR_PARTIES_ID_ALIAS = "SCOUTED_WAR_PARTIES"
        private const val IS_FIELD_SCOUTED_ALIAS = "IS_FIELD_SCOUTED"
    }
}
