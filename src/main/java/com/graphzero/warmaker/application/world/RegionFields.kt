package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.application.ddd.AggregateRoot
import com.graphzero.warmaker.application.ddd.AggregateRootLogic
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.domain.enumeration.FieldType

class RegionFields(id: Long,
                   coordinates: Coordinates,
                   moveModifier: Double,
                   fieldType: FieldType,
                   warPartiesInField: List<WarParty>) : AggregateRoot {
    private val aggregateRoot: AggregateRootLogic = AggregateRootLogic(id)

    @JvmField
    val fieldType: FieldType
    @JvmField
    val coordinates: Coordinates?
    @JvmField
    val moveModifier: Double
    val warPartiesInField: List<WarParty>

    override fun registerDomainEvent(event: DomainEvent<out WarMakerEventType>) {
        aggregateRoot.registerDomainEvent(event)
    }

    override fun clearEvents() {
        aggregateRoot.clearEvents()
    }

    override fun getDomainEvents(): List<DomainEvent<out WarMakerEventType>> {
        return aggregateRoot.getDomainEvents()
    }

    override val id: Long?
        get() = aggregateRoot.id

    init {
        this.coordinates = coordinates
        this.moveModifier = moveModifier
        this.fieldType = fieldType
        this.warPartiesInField = warPartiesInField
    }
}
