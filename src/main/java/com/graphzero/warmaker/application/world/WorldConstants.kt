package com.graphzero.warmaker.application.world

object WorldConstants {
    /**
     * Y coordinate is doubled to make algorithms easier,
     * take a look at "Doubled coordinates" https://www.redblobgames.com/grids/hexagons/#coordinates
     */
    const val DOUBLED_Y_COORDINATE = 2
}
