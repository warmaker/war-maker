package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.application.ddd.AggregateRoot
import com.graphzero.warmaker.application.player.Player
import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneyConclusion
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneySaga
import java.util.*

/**
 * Abstract class representing field in game world
 */
interface Region : AggregateRoot {
    fun isEnemyOf(player: Player): Boolean
    val ownerId: Optional<Long>
    fun handlePassThrough(warParty: WarParty): WarPartyJourneyConclusion
    fun handleReachingDestination(warParty: WarParty, saga: WarPartyJourneySaga): WarPartyJourneyConclusion
    val coordinates: Coordinates?

    fun enter(warParty: WarParty, saga: WarPartyJourneySaga): WarPartyJourneyConclusion {
        return if (saga.hasReachedItsDestination()) {
            handleReachingDestination(warParty, saga)
        } else {
            handlePassThrough(warParty)
        }
    }
}
