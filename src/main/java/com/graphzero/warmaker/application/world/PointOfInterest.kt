package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.player.Player
import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.application.warparty.journey.WarPartyCommand
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneyConclusion
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneySaga
import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.domain.enumeration.FieldType
import java.util.*

internal class PointOfInterest(id: Long, coordinates: Coordinates, moveModifier: Double, warPartiesInField: List<WarParty>) : Region, Battlefield, Garrisonable {
    private val region: RegionFields =
        RegionFields(id, coordinates, moveModifier, FieldType.POINT_OF_INTEREST, warPartiesInField)

    override fun performBattle(defender: WarParty, attacker: WarParty) {}
    override fun garrison(garrison: WarParty) {}
    override fun addUnits(units: HumanUnits) {}
    override fun isEnemyOf(player: Player): Boolean {
        return false
    }

    override val ownerId: Optional<Long>
        get() = Optional.empty()

    override fun handlePassThrough(warParty: WarParty): WarPartyJourneyConclusion {
        return WarPartyJourneyConclusion.MOVING_TO_NEXT_POSITION
    }

    override fun handleReachingDestination(warParty: WarParty, saga: WarPartyJourneySaga): WarPartyJourneyConclusion {
        return when (saga.command) {
            WarPartyCommand.GARRISON -> WarPartyJourneyConclusion.GARRISONED
            else -> WarPartyJourneyConclusion.RETURNING
        }
    }

    override val coordinates: Coordinates?
        get() = region.coordinates


    override fun getDomainEvents(): List<DomainEvent<out WarMakerEventType>> {
        return region.getDomainEvents()
    }

    override val id: Long?
        get() = region.id

    override fun registerDomainEvent(event: DomainEvent<out WarMakerEventType>) {
        region.registerDomainEvent(event)
    }

    override fun clearEvents() {
        region.clearEvents()
    }

    override fun toString(): String {
        return "PointOfInterest{" +
                "region=" + region +
                '}'
    }

}
