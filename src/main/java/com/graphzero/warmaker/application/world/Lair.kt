package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.player.Player
import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.application.warparty.battle.BattleReport
import com.graphzero.warmaker.application.warparty.journey.WarPartyCommand
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneyConclusion
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneySaga
import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.application.world.Lair
import com.graphzero.warmaker.domain.enumeration.FieldType
import org.slf4j.LoggerFactory
import java.util.*

internal class Lair(id: Long,
                    coordinates: Coordinates,
                    moveModifier: Double,
                    private val storedLoot: Resources,
                    garrison: WarParty,
                    warPartiesInField: List<WarParty>) : Region, Attackable, Garrisonable, Lootable {

    private val region: RegionFields = RegionFields(id, coordinates, moveModifier, FieldType.LAIR, warPartiesInField)

    var garrison: WarParty
        private set

    override fun getAttacked(attacker: WarParty): BattleReport {
        return garrison.battle(attacker)
    }

    override fun garrison(garrison: WarParty) {
        this.garrison = garrison
    }

    override fun addUnits(units: HumanUnits) {
        garrison.addUnits(units)
    }

    override fun loot(): ResourcesValue {
        val gatheredLoot = ResourcesValue.of(storedLoot)
        storedLoot.plunder()
        return gatheredLoot
    }

    override fun isEnemyOf(player: Player): Boolean {
        return player.isEnemy(garrison.owner!!.id)
    }

    override val ownerId: Optional<Long>
        get() = Optional.of(garrison.owner!!.id)

    override fun handlePassThrough(warParty: WarParty): WarPartyJourneyConclusion {
        return WarPartyJourneyConclusion.MOVING_TO_NEXT_POSITION
    }

    override fun handleReachingDestination(warParty: WarParty, saga: WarPartyJourneySaga): WarPartyJourneyConclusion {
        return when (saga.command) {
            WarPartyCommand.ATTACK -> {
                log.debug("Battle between War Party [{}] and lair [{}] begins", warParty.id, id)
                val battleResult = getAttacked(warParty)
                log.debug("Battle between WarParty [{}] and Lair [{}] ended, output is: [{}]", warParty.id, id, battleResult.battleResult)
                registerDomainEvents(garrison.getDomainEvents())
                if (!warParty.isAnnihilated) {
                    WarPartyJourneyConclusion.RETURNING
                } else {
                    WarPartyJourneyConclusion.ANNIHILATED
                }
            }
            WarPartyCommand.GARRISON -> {
                log.debug("War Party [{}] moves to the location [{}] and waits there", warParty.id, id)
                WarPartyJourneyConclusion.GARRISONED
            }
            else -> {
                log.debug("Unsupported command [{}] for War Party [{}] ", saga.command, warParty.id)
                WarPartyJourneyConclusion.RETURNING
            }
        }
    }

    override val coordinates: Coordinates?
        get() = region.coordinates

    override fun registerDomainEvent(event: DomainEvent<out WarMakerEventType>) {
        region.registerDomainEvent(event)
    }

    override fun clearEvents() {
        region.clearEvents()
    }

    override fun getDomainEvents(): List<DomainEvent<out WarMakerEventType>> {
        return region.getDomainEvents()
    }

    override val id: Long?
        get() = region.id

    override fun toString(): String {
        return "Lair{" +
            "region=" + region +
            ", garrison=" + garrison +
            ", storedLoot=" + storedLoot +
            '}'
    }

    companion object {
        private val log = LoggerFactory.getLogger(Lair::class.java)
    }

    init {
        this.garrison = garrison
    }
}
