package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.application.warparty.WarParty

interface Battlefield {
    /**
     * Method indicating two players can fight against each other on this field
     *
     * @param defender
     * @param attacker
     */
    fun performBattle(defender: WarParty, attacker: WarParty)
}
