package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.application.cities.region.CityRegionService
import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.warparty.WarPartyFactory
import com.graphzero.warmaker.application.warparty.WarPartyRootRepository
import com.graphzero.warmaker.config.ApplicationProperties
import com.graphzero.warmaker.domain.FieldEntity
import com.graphzero.warmaker.domain.enumeration.FieldType
import com.graphzero.warmaker.repository.LairEntityRepository
import com.graphzero.warmaker.repository.LandEntityRepository
import com.graphzero.warmaker.repository.PointOfInterestEntityRepository
import org.springframework.stereotype.Service
import java.time.ZonedDateTime
import java.util.function.Supplier

@Service
class RegionsFactory(
    private val lairRepository: LairEntityRepository,
    private val pointOfInterestEntityRepository: PointOfInterestEntityRepository,
    private val landEntityRepository: LandEntityRepository,
    private val warPartyRepository: WarPartyRootRepository,
    applicationProperties: ApplicationProperties,
    private val cityRegionService: CityRegionService,
    private val warPartyFactory: WarPartyFactory
) {
    private val worldProperties: ApplicationProperties.World = applicationProperties.getWorld()

    fun of(field: FieldEntity): Region {
        val warPartiesEntitiesInField = warPartyRepository.repository.findByPosition(field)
        val warPartiesInField = warPartyFactory.buildWarParties(warPartiesEntitiesInField)
        return when (field.type) {
            FieldType.CITY -> cityRegionService.getCity(field.id, warPartiesInField)
                .getOrElseThrow(Supplier { RuntimeException("Missing city for field " + field.id) })
            FieldType.POINT_OF_INTEREST -> PointOfInterest(
                pointOfInterestEntityRepository.findByLocationId(field.id).get().id,
                Coordinates(field.getxCoordinate(), field.getyCoordinate()),
                modifier(field.type),
                warPartiesInField
            )
            FieldType.LAIR -> {
                val lairEntity = lairRepository.findByLocationId(field.id).get()
                val warParty =
                    warPartyFactory.buildWarParty(lairEntity.garrison, lairEntity.treasure, ZonedDateTime.now())
                Lair(
                    lairEntity.id,
                    Coordinates(field.getxCoordinate(), field.getyCoordinate()),
                    modifier(field.type),
                    Resources.of(lairEntity.treasure),
                    warParty,
                    warPartiesInField
                )
            }
            FieldType.LAND -> Land(
                landEntityRepository.findByLocationId(field.id).get().id,
                Coordinates(field.getxCoordinate(), field.getyCoordinate()),
                modifier(field.type),
                warPartiesInField
            )
            else -> Land(
                landEntityRepository.findByLocationId(field.id).get().id,
                Coordinates(field.getxCoordinate(), field.getyCoordinate()),
                modifier(field.type),
                warPartiesInField
            )
        }
    }

    private fun modifier(fieldType: FieldType): Double {
        return when (fieldType) {
            FieldType.LAIR -> worldProperties.fieldModifiers.lair
            FieldType.LAND -> worldProperties.fieldModifiers.land
            FieldType.POINT_OF_INTEREST -> worldProperties.fieldModifiers.pointOfInterest
            FieldType.CITY -> worldProperties.fieldModifiers.city
            else -> 1.0
        }
    }

}
