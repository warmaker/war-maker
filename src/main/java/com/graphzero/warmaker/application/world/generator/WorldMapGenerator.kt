package com.graphzero.warmaker.application.world.generator

import com.graphzero.warmaker.config.ApplicationProperties
import org.springframework.stereotype.Component

@Component
internal class WorldMapGenerator(applicationProperties: ApplicationProperties,
                                 mapGenerator: RandomMapGenerator) {
    private fun generateRandomMap(applicationProperties: ApplicationProperties, mapGenerator: RandomMapGenerator) {
        if (applicationProperties.getWorld().generator.regenerate()) {
            mapGenerator.generateRandomMap(10)
        }
    }

    init {
        generateRandomMap(applicationProperties, mapGenerator)
    }
}
