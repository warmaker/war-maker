package com.graphzero.warmaker.application.world

import com.fasterxml.jackson.annotation.JsonProperty
import com.graphzero.warmaker.domain.CityEntity
import com.graphzero.warmaker.domain.FieldEntity
import java.io.Serializable
import java.util.*

class Coordinates(@get:JsonProperty("x") val x: Int,
                  @get:JsonProperty("y") val y: Int) : Serializable {
    override fun toString(): String {
        return "Coordinates{" +
                "x=" + x +
                ", y=" + y +
                '}'
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Coordinates

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        return result
    }


    companion object {
        fun of(field: FieldEntity): Coordinates {
            return Coordinates(field.getxCoordinate(), field.getyCoordinate())
        }

        @JvmStatic
        fun of(cityEntity: CityEntity): Coordinates {
            return Coordinates(cityEntity.location.getxCoordinate(), cityEntity.location.getyCoordinate())
        }
    }
}
