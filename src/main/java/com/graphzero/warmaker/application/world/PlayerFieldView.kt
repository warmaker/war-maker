package com.graphzero.warmaker.application.world

import com.fasterxml.jackson.annotation.JsonProperty
import com.graphzero.warmaker.domain.enumeration.FieldType
import com.graphzero.warmaker.domain.enumeration.RelationType
import org.jooq.Record7
import java.util.*

/**
 * Class representing world field from the given players perspective
 */
internal class PlayerFieldView(@get:JsonProperty("xCoordinate") val xCoordinate: Int,
                               @get:JsonProperty("yCoordinate") val yCoordinate: Int,
                               @get:JsonProperty("modifier") val modifier: Double,
                               @get:JsonProperty("fieldType") val fieldType: FieldType,
                               @get:JsonProperty("relation") val relation: RelationType,
                               @get:JsonProperty("ownerId") val ownerId: Long?,
                               @get:JsonProperty("canDeclareWar") val canDeclareWar: Boolean,
                               @get:JsonProperty("canAttack") val canAttack: Boolean,
                               @get:JsonProperty("scoutedWarParties") val scoutedWarParties: Array<Long>?,
                               @get:JsonProperty("isScouted") val isScouted: Boolean) {

    companion object {
        fun of(record: Record7<Int, Int, String, String, Long, Array<Long>, Boolean>): PlayerFieldView {
            val fieldType = mapFieldType(record)
            val relationType = calculateRelationType(record, fieldType)
            val canDeclareWar = calculateIfCanDeclareWar(fieldType, relationType)
            val canAttack = calculateIfCanAttack(relationType)
            return PlayerFieldView(
                    record.value1(),
                    record.value2(),
                    1.0,
                    fieldType,
                    relationType,
                    record.value5(),
                    canDeclareWar,
                    canAttack,
                    record.value6(),
                    Optional.ofNullable(record.value7()).orElse(false))
        }

        private fun mapFieldType(record: Record7<Int, Int, String, String, Long, Array<Long>, Boolean>): FieldType {
            return Optional.ofNullable(record.value3())
                .map { name: String -> FieldType.valueOf(name) }
                .orElse(FieldType.LAND)
        }

        private fun calculateIfCanAttack(relationType: RelationType): Boolean {
            return relationType == RelationType.ENEMY
        }

        private fun calculateIfCanDeclareWar(fieldType: FieldType, relationType: RelationType): Boolean {
            return fieldType == FieldType.CITY && !calculateIfCanAttack(relationType)
        }

        private fun calculateRelationType(record: Record7<Int, Int, String, String, Long, Array<Long>, Boolean>,
                                  fieldType: FieldType): RelationType {
            var relationType = Optional.ofNullable(record.value4())
                .map { name: String -> RelationType.valueOf(name) }
                .orElse(RelationType.NEUTRAL)
            if (fieldType == FieldType.LAIR) {
                relationType = RelationType.ENEMY
            }
            return relationType
        }
    }
}
