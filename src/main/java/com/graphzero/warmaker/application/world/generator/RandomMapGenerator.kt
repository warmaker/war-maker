package com.graphzero.warmaker.application.world.generator

import com.graphzero.warmaker.application.player.Player
import com.graphzero.warmaker.application.player.PlayerService
import com.graphzero.warmaker.application.time.TimeConfiguration
import com.graphzero.warmaker.application.world.WorldConstants
import com.graphzero.warmaker.application.world.generator.RandomMapGenerator
import com.graphzero.warmaker.domain.*
import com.graphzero.warmaker.domain.enumeration.CityType
import com.graphzero.warmaker.domain.enumeration.FieldType
import com.graphzero.warmaker.domain.enumeration.PointOfInterestType
import com.graphzero.warmaker.domain.enumeration.WarPartyState
import com.graphzero.warmaker.repository.*
import org.apache.commons.lang.RandomStringUtils
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.ZonedDateTime
import java.util.*
import java.util.function.Consumer

@Service
internal class RandomMapGenerator(private val fieldRepository: FieldEntityRepository,
                                       private val warPartyEntityRepository: WarPartyEntityRepository,
                                       private val unitsEntityRepository: UnitsEntityRepository,
                                       private val resourcesEntityRepository: ResourcesEntityRepository,
                                       private val lairEntityRepository: LairEntityRepository,
                                       private val cityEntityRepository: CityEntityRepository,
                                       private val cityBuildingsEntityRepository: CityBuildingsEntityRepository,
                                       private val armyEntityRepository: ArmyEntityRepository,
                                       private val landEntityRepository: LandEntityRepository,
                                       private val pointOfInterestEntityRepository: PointOfInterestEntityRepository,
                                       private val profileEntityRepository: ProfileEntityRepository,
                                       private val researchesEntityRepository: ResearchesEntityRepository,
                                       private val userRepository: UserRepository,
                                       private val playerRelationsRepository: PlayerRelationsRepository, private val playerService: PlayerService) {
    private var playersCount = 0
    private var banditArmy: ArmyEntity? = null
    private var bandit: ProfileEntity? = null
    private fun cleanDatabase() {
        resourcesEntityRepository.deleteAll()
        unitsEntityRepository.deleteAll()
        warPartyEntityRepository.deleteAll()
        armyEntityRepository.deleteAll()
        cityBuildingsEntityRepository.deleteAll()
        lairEntityRepository.deleteAll()
        cityEntityRepository.deleteAll()
        landEntityRepository.deleteAll()
        pointOfInterestEntityRepository.deleteAll()
        researchesEntityRepository.deleteAll()
        playerRelationsRepository.deleteAll()
        profileEntityRepository.deleteAll()
        fieldRepository.deleteAll()
    }

    @Transactional
    fun generateRandomMap(size: Int) {
        log.debug("####### New map is being generated! #######")
        var lands = 0
        var lairs = 0
        var pois = 0
        var cities = 0
        cleanDatabase()
        for (i in 0 until size) {
            for (j in 0 until size) {
                val field = FieldEntity().xCoordinate(i).yCoordinate(j * WorldConstants.DOUBLED_Y_COORDINATE).type(FieldType.LAND)
                fieldRepository.save(field)
                val landType = Random().nextGaussian()
                if (landType <= -0.5) {
                    field.type = FieldType.CITY
                    createCity(field)
                    cities++
                }
                if (landType >= -0.5 && landType <= -0.25 || landType in 0.25..0.5) {
                    field.type = FieldType.LAIR
                    createLair(field, lairs)
                    lairs++
                }
                if (landType >= -0.4 && landType <= 0.4) {
                    field.type = FieldType.LAND
                    createLand(field)
                    lands++
                }
                if (landType >= 0.5) {
                    field.type = FieldType.POINT_OF_INTEREST
                    createPointOfInterest(field)
                    pois++
                }
            }
        }
        log.debug("Fields distribution: lands: {}, lairs {}, cities {}, points of interests {}.", lands, lairs,
                cities, pois)
        bindUserToCity("admin")
        bindUserToCity("player1")
        bindUserToCity("player2")
        bindUserToCity("player3")
        createBanditPlayer()
        declareWarOnBandits()
    }

    private fun declareWarOnBandits() {
        profileEntityRepository.findAll().forEach(Consumer { profileEntity: ProfileEntity -> playerService.declareWar(profileEntity.id, bandit!!.id) })
    }

    private fun bindUserToCity(userLogin: String) {
        val profileEntity = profileEntityRepository.findAll()[playersCount++]
        val user = userRepository.findOneByLogin(userLogin).get()
        profileEntity.user = user
        profileEntityRepository.save(profileEntity)
        log.debug("Binding profile [{}] to user [{}]", profileEntity.id, user.id)
    }

    private fun createBanditPlayer() {
        val user = createUser()
        banditArmy = ArmyEntity()
        bandit = ProfileEntity()
                .accountName(Player.BANDIT_NAME)
                .email("bandit@email")
                .user(user)
        profileEntityRepository.save(bandit)
        armyEntityRepository.save(banditArmy)
    }

    private fun createLair(fieldEntity: FieldEntity, lairNumber: Int): LairEntity {
        val warParty = createWarParty(fieldEntity).armyEntity(banditArmy)
        val lairEntity = LairEntity()
                .location(fieldEntity)
                .lairName("lair $lairNumber")
                .garrison(warParty)
                .treasure(createResources())
        return lairEntityRepository.save(lairEntity)
    }

    private fun createWarParty(fieldEntity: FieldEntity): WarPartyEntity {
        val units = WarPartyEntity()
                .state(WarPartyState.STATIONED)
                .capacity(1L)
                .speed(1.0)
                .resourcesCarried(createResources())
                .position(fieldEntity)
                .units(createUnits(10))
        return warPartyEntityRepository.save(units)
    }

    private fun createUnits(amount: Int): UnitsEntity {
        val unitsEntity = UnitsEntity()
                .archers(amount)
                .crossbowmen(amount)
                .spearmen(amount)
                .swordsmen(amount)
        return unitsEntityRepository.save(unitsEntity)
    }

    private fun createResources(): ResourcesEntity {
        val resourcesEntity = ResourcesEntity()
                .changeDate(ZonedDateTime.now(clock))
                .denars(500.0)
                .wood(100.0)
                .stone(50.0)
                .steel(10.0)
                .food(0.0)
                .faith(0.0)
        return resourcesEntityRepository.save(resourcesEntity)
    }

    private fun createBuildings(): CityBuildingsEntity {
        val cityBuildingsEntity = CityBuildingsEntity()
                .cityHall(1)
                .sawMill(1)
                .oreMine(1)
                .goldMine(1)
                .ironworks(1)
                .wall(0)
                .tradingPost(0)
                .chapel(0)
                .forge(0)
                .university(0)
                .barracks(0)
        return cityBuildingsEntityRepository.saveAndFlush(cityBuildingsEntity)
    }

    private fun createArmy(warPartyEntity: WarPartyEntity): ArmyEntity {
        val armyEntity = ArmyEntity()
                .units(createUnits(10000))
                .addWarParties(warPartyEntity.resourcesCarried(createResources()))
        warPartyEntity.armyEntity = armyEntity
        warPartyEntityRepository.save(warPartyEntity)
        return armyEntityRepository.saveAndFlush(armyEntity)
    }

    private fun createCity(fieldEntity: FieldEntity): CityEntity {
        val city = CityEntity()
                .cityName(UUID.randomUUID().toString())
                .cityType(CityType.PLAYER)
                .recruitmentModifier(1.0)
                .buildSpeedModifier(1.0)
                .army(createArmy(createWarParty(fieldEntity)))
                .storedResources(createResources())
                .buildings(createBuildings())
                .location(fieldEntity)
                .owner(createProfile().researches(createResearches()))
        return cityEntityRepository.saveAndFlush(city)
    }

    private fun createLand(fieldEntity: FieldEntity): LandEntity {
        val landEntity = LandEntity()
                .location(fieldEntity)
                .road(1)
                .scoutTower(1)
        return landEntityRepository.saveAndFlush(landEntity)
    }

    private fun createPointOfInterest(fieldEntity: FieldEntity): PointOfInterestEntity {
        val pointOfInterestEntity = PointOfInterestEntity()
                .location(fieldEntity)
                .type(PointOfInterestType.FOREST)
        return pointOfInterestEntityRepository.saveAndFlush(pointOfInterestEntity)
    }

    private fun createProfile(): ProfileEntity {
        val profileEntity = ProfileEntity()
                .user(createUser())
                .accountName("Profile")
                .email("a@b")
        return profileEntityRepository.saveAndFlush(profileEntity)
    }

    private fun createUser(): User {
        val user = User()
        user.firstName = RandomStringUtils.random(10, true, true)
        user.lastName = RandomStringUtils.random(10, true, true)
        user.login = RandomStringUtils.random(10, true, true)
        user.password = "admin"
        user.email = RandomStringUtils.random(5, true, true) + "@" + RandomStringUtils.random(5, true, true)
        user.activated = true
        return userRepository.saveAndFlush(user)
    }

    private fun createResearches(): ResearchesEntity {
        val researchesEntity = ResearchesEntity()
                .cavalrySpeed(1)
                .cavalryVitality(1)
                .cityBuildingSpeed(1)
                .heavyArmorValue(1)
                .infantrySpeed(1)
                .infantryVitality(1)
                .lightArmorValue(1)
                .meleeAttack(1)
                .rangedAttack(1)
                .recruitmentSpeed(1)
                .scouting(1)
        return researchesEntityRepository.saveAndFlush(researchesEntity)
    }

    companion object {
        private val clock = TimeConfiguration.serverClock
        private val log = LoggerFactory.getLogger(RandomMapGenerator::class.java)
    }
}
