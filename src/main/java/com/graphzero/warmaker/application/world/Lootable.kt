package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.application.resources.ResourcesValue

interface Lootable {
    /**
     * Loot this domain object and return looted resources
     * @return
     */
    fun loot(): ResourcesValue
}
