package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.player.Player
import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.application.warparty.journey.WarPartyCommand
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneyConclusion
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneySaga
import com.graphzero.warmaker.application.world.Lair
import com.graphzero.warmaker.domain.enumeration.FieldType
import org.slf4j.LoggerFactory
import java.util.*
import java.util.function.Consumer

internal class Land(id: Long, coordinates: Coordinates, moveModifier: Double, warPartiesInField: List<WarParty>) : Region {
    private val region: RegionFields = RegionFields(id, coordinates, moveModifier, FieldType.LAND, warPartiesInField)
    override fun isEnemyOf(player: Player): Boolean {
        return false
    }

    override val ownerId: Optional<Long>
        get() = Optional.empty()

    override fun handlePassThrough(warParty: WarParty): WarPartyJourneyConclusion {
        battleWithEnemyWarParties(warParty)
        return if (!warParty.isAnnihilated) {
            WarPartyJourneyConclusion.MOVING_TO_NEXT_POSITION
        } else {
            WarPartyJourneyConclusion.ANNIHILATED
        }
    }

    private fun battleWithEnemyWarParties(visitor: WarParty) {
        region.warPartiesInField.stream()
                .filter { stationed: WarParty -> stationed.owner!!.isEnemy(visitor.owner!!.id) }
                .forEach { stationedWarParty: WarParty ->
                    stationedWarParty.battle(visitor)
                    stationedWarParty.getDomainEvents().forEach(Consumer { event: DomainEvent<out WarMakerEventType> -> registerDomainEvent(event) })
                }
    }

    override fun handleReachingDestination(warParty: WarParty, saga: WarPartyJourneySaga): WarPartyJourneyConclusion {
        return when (saga.command) {
            WarPartyCommand.GARRISON -> {
                log.debug("War Party [{}] moves to the location [{}] and waits there", warParty.id, id)
                WarPartyJourneyConclusion.GARRISONED
            }
            else -> {
                log.debug("Unsupported command [{}] for War Party [{}] ", saga.command, warParty.id)
                WarPartyJourneyConclusion.RETURNING
            }
        }
    }

    override val coordinates: Coordinates?
        get() = region.coordinates


    override fun registerDomainEvent(event: DomainEvent<out WarMakerEventType>) {
        region.registerDomainEvent(event)
    }

    override fun clearEvents() {
        region.clearEvents()
    }

    override fun getDomainEvents(): List<DomainEvent<out WarMakerEventType>> {
        return region.getDomainEvents()
    }

    override val id: Long?
        get() = region.id

    override fun toString(): String {
        return "Land{" +
                "region=" + region +
                '}'
    }

    companion object {
        private val log = LoggerFactory.getLogger(Lair::class.java)
    }

}
