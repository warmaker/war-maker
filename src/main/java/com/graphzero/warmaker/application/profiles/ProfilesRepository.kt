package com.graphzero.warmaker.application.profiles

import com.graphzero.warmaker.domain.ProfileEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
internal interface ProfilesRepository : JpaRepository<ProfileEntity, Long> {
    @Query(
        "SELECT NEW com.graphzero.warmaker.application.profiles.ProfileWorldDTO(p.id, p.accountName)" +
                "FROM ProfileEntity p " +
                "WHERE p.user.id = :userId"
    )
    fun findByUserId(@Param("userId") userId: Long): List<ProfileWorldDTO>

    @Query(
        "SELECT NEW com.graphzero.warmaker.application.profiles.ProfileWorldDTO(p.id, p.accountName)" +
                "FROM ProfileEntity p " +
                "WHERE p.user.login = :login"
    )
    fun findByUserLogin(@Param("login") login: String): List<ProfileWorldDTO>
}
