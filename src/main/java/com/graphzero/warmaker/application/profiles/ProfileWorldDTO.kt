package com.graphzero.warmaker.application.profiles

internal class ProfileWorldDTO(val profileId: Long, val profileName: String)
