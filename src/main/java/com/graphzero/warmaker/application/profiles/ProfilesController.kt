package com.graphzero.warmaker.application.profiles

import com.graphzero.warmaker.application.events.schedulable.AbstractSchedulableEventDto
import com.graphzero.warmaker.repository.CityEntityRepository
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/profiles")
internal class ProfilesController(
    private val profilesRepository: ProfilesRepository,
    private val cityRepository: CityEntityRepository,
    private val profilesApplicationService: ProfilesApplicationService
) {
    @GetMapping("/by-user/{userId}")
    fun getAllProfiles(@PathVariable(value = "userId") userId: Long): ResponseEntity<List<ProfileWorldDTO>> {
        return ResponseEntity.ok(profilesRepository.findByUserId(userId))
    }

    @GetMapping("/by-user-login/{login}")
    fun getAllProfiles(@PathVariable(value = "login") login: String): ResponseEntity<List<ProfileWorldDTO>> {
        return ResponseEntity.ok(profilesRepository.findByUserLogin(login))
    }

    @GetMapping("/{profileId}/cities")
    fun getCitiesForProfile(@PathVariable(value = "profileId") profileId: Long): ResponseEntity<List<Long>> {
        return ResponseEntity.ok(cityRepository.findCityIdsByUserId(profileId))
    }

    @GetMapping("/{profileId}/events")
    fun getEvents(@PathVariable(value = "profileId") profileId: Long): ResponseEntity<List<AbstractSchedulableEventDto>> {
        return ResponseEntity.ok(profilesApplicationService.getEventsForProfile(profileId))
    }
}
