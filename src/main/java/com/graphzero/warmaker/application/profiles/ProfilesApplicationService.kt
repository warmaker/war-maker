package com.graphzero.warmaker.application.profiles

import com.graphzero.warmaker.application.events.schedulable.AbstractSchedulableEventDto
import com.graphzero.warmaker.application.events.schedulable.ScheduledEventsService
import org.springframework.stereotype.Service

@Service
internal class ProfilesApplicationService(private val scheduledEventsService: ScheduledEventsService) {
    fun getEventsForProfile(profileId: Long): List<AbstractSchedulableEventDto> {
        return scheduledEventsService.findEventsDtoForUser(profileId)
    }
}
