package com.graphzero.warmaker.application.warparty.journey

enum class WarPartyJourneyConclusion {
    MOVING_TO_NEXT_POSITION, GARRISONED, RETURNING, ANNIHILATED
}
