package com.graphzero.warmaker.application.warparty.journey

import com.graphzero.warmaker.application.warparty.value.WarPartyNotFound
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
internal class JourneyEventsListener(private val warPartyJourneyService: WarPartyJourneyService) {
    private val log = LoggerFactory.getLogger(JourneyEventsListener::class.java)

    @EventListener
    @Throws(WarPartyNotFound::class)
    fun handleWarPartyJourneySaga(saga: WarPartyJourneySaga) {
        log.debug("Saga to move war party [{}]", saga.warPartyId)
        try {
            warPartyJourneyService.moveWarParty(saga)
        } catch (e: WarPartyNotFound) {
            log.error("Fatal error: [{}]", e.message)
            throw e
        }
    }
}
