package com.graphzero.warmaker.application.warparty.battle

import com.graphzero.warmaker.application.ddd.AggregateRoot
import com.graphzero.warmaker.application.ddd.AggregateRootLogic
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.player.Player
import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import com.graphzero.warmaker.application.warparty.value.WarPartyAnnihilatedEvent
import com.graphzero.warmaker.config.ApplicationProperties
import org.slf4j.LoggerFactory

class WarPartyBattle(
    private val aggregateRoot: AggregateRootLogic,
    val initialUnits: HumanUnits,
    val owner: Player,
    unitsConfig: ApplicationProperties.Units
) : AggregateRoot {
    private val warPartyBattleUnits: WarPartyBattleUnits = WarPartyBattleUnits(unitsConfig, initialUnits)

    companion object {
        private val log = LoggerFactory.getLogger(WarPartyBattle::class.java)
    }

    fun battle(attackingWarPartyBattle: WarPartyBattle, battleModificators: List<BattleAlteration>?): BattleReport {
        val battleReport = Battle(
            attackingWarPartyBattle.warPartyBattleUnits,
            warPartyBattleUnits,
            battleModificators,
            attackerWarPartyId = attackingWarPartyBattle.id,
            attackerOwnerId = attackingWarPartyBattle.owner.id,
            defenderWarPartyId = id,
            defenderOwnerId = owner.id
        )
            .battle()
        log.debug("Battle result of defender [$id] vs attacker [$attackingWarPartyBattle.id]: ${battleReport.battleResult}")
        when (battleReport.battleResult) {
            BattleResult.ATTACKER_WINS -> {
                attackingWarPartyBattle.handleWonBattle(battleReport.attackerLoses, battleReport)
                handleLostBattle(battleReport.defenderLoses, battleReport)
            }
            BattleResult.DEFENDER_WINS -> {
                handleWonBattle(battleReport.defenderLoses, battleReport)
                attackingWarPartyBattle.handleLostBattle(battleReport.attackerLoses, battleReport)
            }
            BattleResult.DRAW -> {
                handleLostBattle(battleReport.defenderLoses, battleReport)
                attackingWarPartyBattle.handleLostBattle(battleReport.attackerLoses, battleReport)
            }
        }
        return battleReport
    }

    private fun handleLostBattle(loses: HumanUnitsList, battleReport: BattleReport) {
        registerDomainEvent(WarPartyBattleEvent(this, this, loses, getRemainingUnits(loses), battleReport))
        if (warPartyBattleUnits.isAnnihilated) {
            registerDomainEvent(WarPartyAnnihilatedEvent(this, id))
        }
    }

    private fun getRemainingUnits(loses: HumanUnitsList): HumanUnitsList {
        return initialUnits.units.minus(loses)
    }

    private fun handleWonBattle(loses: HumanUnitsList, battleReport: BattleReport) {
        registerDomainEvent(WarPartyBattleEvent(this, this, loses, getRemainingUnits(loses), battleReport))
    }

    override val id: Long
        get() = aggregateRoot.id!!

    override fun registerDomainEvent(event: DomainEvent<out WarMakerEventType>) {
        aggregateRoot.registerDomainEvent(event)
    }

    override fun clearEvents() {
        aggregateRoot.clearEvents()
    }

    override fun getDomainEvents(): List<DomainEvent<out WarMakerEventType>> {
        return aggregateRoot.getDomainEvents()
    }
}
