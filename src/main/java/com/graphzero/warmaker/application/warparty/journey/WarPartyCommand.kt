package com.graphzero.warmaker.application.warparty.journey

enum class WarPartyCommand {
    ATTACK, GARRISON, RETURN, MOVE
}
