package com.graphzero.warmaker.application.warparty

import com.graphzero.warmaker.application.army.*
import com.graphzero.warmaker.application.player.PlayerFactory
import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.warparty.builders.WarPartyBuilder
import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.application.warparty.value.WarPartyNotFound
import com.graphzero.warmaker.application.world.Coordinates.Companion.of
import com.graphzero.warmaker.domain.ResourcesEntity
import com.graphzero.warmaker.domain.WarPartyEntity
import com.graphzero.warmaker.repository.WarPartyEntityRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.ZonedDateTime
import java.util.stream.Collectors

@Transactional
@Service
class WarPartyFactory(
    private val repository: WarPartyEntityRepository,
    private val playerFactory: PlayerFactory
) {

    fun buildWarParty(
        warParty: WarPartyEntity,
        resourcesCarried: ResourcesEntity,
        entryDate: ZonedDateTime
    ): WarParty {
        return WarPartyBuilder().id(warParty.id).state(warParty.state)
            .unitsList(HumanUnits.of(warParty.units))
            .carriedResources(Resources.of(resourcesCarried))
            .location(of(warParty.position)).fieldEntryDate(entryDate)
            .owner(playerFactory.getOwner(warParty.id))
            .unitsConfig(UnitsConfig.units!!)
            .build()
    }

    private fun buildWarParty(warParty: WarPartyEntity, entryDate: ZonedDateTime): WarParty {
        return WarPartyBuilder().id(warParty.id).state(warParty.state)
            .unitsList(HumanUnits.of(warParty.units))
            .carriedResources(Resources.of(warParty.resourcesCarried))
            .location(of(warParty.position)).fieldEntryDate(entryDate)
            .owner(playerFactory.getOwner(warParty.id))
            .unitsConfig(UnitsConfig.units!!)
            .build()
    }

    fun buildWarParty(warPartyId: Long, entryDate: ZonedDateTime): WarParty {
        val warParty = findEntityOrThrow(warPartyId)
        return WarPartyBuilder()
            .id(warParty.id)
            .state(warParty.state)
            .unitsList(HumanUnits.of(warParty.units))
            .carriedResources(Resources.of(warParty.resourcesCarried))
            .location(of(warParty.position)).fieldEntryDate(entryDate).owner(playerFactory.getOwner(warPartyId))
            .unitsConfig(UnitsConfig.units!!)
            .build()
    }

    // TODO - add position entry time
    fun buildWarParties(warParty: List<WarPartyEntity>): List<WarParty> {
        return warParty
            .stream()
            .map { warPartyEntity: WarPartyEntity -> buildWarParty(warPartyEntity, ZonedDateTime.now()) }
            .collect(Collectors.toList())
    }

    @Throws(WarPartyNotFound::class)
    fun findEntityOrThrow(warPartyId: Long): WarPartyEntity {
        return repository
            .findById(warPartyId)
            .orElseThrow { WarPartyNotFound() }
    }
}
