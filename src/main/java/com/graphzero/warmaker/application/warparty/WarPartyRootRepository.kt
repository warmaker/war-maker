package com.graphzero.warmaker.application.warparty

import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import com.graphzero.warmaker.domain.ResourcesEntity
import com.graphzero.warmaker.domain.UnitsEntity
import com.graphzero.warmaker.domain.WarPartyEntity
import com.graphzero.warmaker.repository.*
import org.springframework.stereotype.Service

@Service
class WarPartyRootRepository(
    val repository: WarPartyEntityRepository,
    private val unitsRepository: UnitsEntityRepository,
    private val resourcesRepository: ResourcesEntityRepository,
    private val fieldRepository: FieldEntityRepository,
    private val armyEntityRepository: ArmyEntityRepository
) {
    fun persistWarPartyPositionUpdate(warParty: WarParty) {
        repository
            .findById(warParty.id)
            .ifPresent { warPartyEntity: WarPartyEntity ->
                warPartyEntity.state(warParty.state)
                val position =
                    fieldRepository.findByXCoordinateAndYCoordinate(warParty.location.x, warParty.location.y)
                warPartyEntity.position = position
                repository.save(warPartyEntity)
            }
    }

    fun persistUnitsUpdate(warPartyId: Long, humanUnits: HumanUnitsList) {
        repository
            .findById(warPartyId)
            .ifPresent { warPartyEntity: WarPartyEntity ->
                val unitsEntity = warPartyEntity.units
                persistUnits(unitsEntity, humanUnits)
            }
    }

    fun persistResourcesUpdate(warPartyId: Long, resourcesChange: ResourcesValue) {
        val resources = resourcesRepository.findByWarPartyId(warPartyId).get()
        val finalResources: ResourcesValue = ResourcesValue.of(resources).add(resourcesChange)
        resources.denars = finalResources.denars
        resources.wood = finalResources.wood
        resources.stone = finalResources.stone
        resources.food = finalResources.food
        resources.steel = finalResources.steel
        resourcesRepository.save(resources)
    }

    fun save(armyId: Long, warParty: WarParty): WarPartyEntity {
        val warPartyEntity = WarPartyEntity()
            .armyEntity(armyEntityRepository.getOne(armyId))
            .resourcesCarried(resourcesRepository.save(toResourcesEntity(warParty.carriedResources)))
            .position(
                fieldRepository.findByXCoordinateAndYCoordinate(
                    warParty.location.x,
                    warParty.location.y
                )
            )
            .units(unitsRepository.save(toUnitsEntity(warParty.unitsList)))
            .speed(1.0)
            .state(warParty.state)
            .capacity(1L)
        return repository.save(warPartyEntity)
    }

    fun remove(warPartyId: Long) {
        repository.findById(warPartyId)
            .ifPresent { warPartyEntity: WarPartyEntity ->
                unitsRepository.delete(warPartyEntity.units)
                resourcesRepository.delete(warPartyEntity.resourcesCarried)
                repository.delete(warPartyEntity)
            }
    }

    private fun persistUnits(units: UnitsEntity, newUnits: HumanUnitsList) {
        units
            .spearmen(newUnits.spearman)
            .swordsmen(newUnits.swordsman)
            .archers(newUnits.archers)
            .crossbowmen(newUnits.crossbowman)
        unitsRepository.save(units)
    }

    private fun toUnitsEntity(units: HumanUnits): UnitsEntity {
        return UnitsEntity()
            .spearmen(units.units.spearman)
            .swordsmen(units.units.swordsman)
            .archers(units.units.archers)
            .crossbowmen(units.units.crossbowman)
    }

    private fun toResourcesEntity(resources: Resources): ResourcesEntity {
        return ResourcesEntity()
            .denars(resources.denars)
            .wood(resources.wood)
            .stone(resources.stone)
            .steel(resources.steel)
            .food(resources.food)
            .faith(resources.faith)
    }
}
