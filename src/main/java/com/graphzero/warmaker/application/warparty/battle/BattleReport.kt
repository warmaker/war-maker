package com.graphzero.warmaker.application.warparty.battle

import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import java.time.ZonedDateTime

class BattleReport(
    val battleTime: String,
    val battleResult: BattleResult,
    val attackerLoses: HumanUnitsList,
    val defenderLoses: HumanUnitsList,
    val attackerWarPartyId: Long,
    val defenderWarPartyId: Long,
    val attackerPlayerId: Long,
    val defenderPlayerId: Long
) {
    companion object {
        fun of(
            battleTime: ZonedDateTime,
            battleResult: BattleResult,
            attackerLoses: HumanUnitsList,
            defenderLoses: HumanUnitsList,
            attacker: WarPartyBattle,
            defender: WarPartyBattle
        ): BattleReport {
            return BattleReport(
                battleTime.toString(),
                battleResult,
                attackerLoses,
                defenderLoses,
                attacker.id,
                defender.id,
                attacker.owner.id,
                defender.owner.id
            )
        }

        fun of(
            battleTime: ZonedDateTime,
            battleResult: BattleResult,
            attackerLoses: HumanUnitsList,
            defenderLoses: HumanUnitsList,
            attackerId: Long,
            defenderId: Long,
            attackerPlayerId: Long,
            defenderPlayerId: Long
        ): BattleReport {
            return BattleReport(
                battleTime.toString(),
                battleResult,
                attackerLoses,
                defenderLoses,
                attackerId,
                defenderId,
                attackerPlayerId,
                defenderPlayerId
            )
        }
    }
}
