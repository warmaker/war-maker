package com.graphzero.warmaker.application.warparty.journey

import com.graphzero.warmaker.application.utils.WarMakerDomainErrors
import com.graphzero.warmaker.application.utils.WarMakerDomainException

internal class WarPartyMovingException : WarMakerDomainException(
    WarMakerDomainErrors.WAR_PARTY_ALREADY_MOVING.errorMessage,
    WarMakerDomainErrors.WAR_PARTY_ALREADY_MOVING
)
