package com.graphzero.warmaker.application.warparty.journey

import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent

internal class MovementResult(
    val warPartyJourneyConclusion: WarPartyJourneyConclusion,
    val eventsHappened: List<DomainEvent<out WarMakerEventType>>?
)
