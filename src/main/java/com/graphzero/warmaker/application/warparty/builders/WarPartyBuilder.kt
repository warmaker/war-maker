package com.graphzero.warmaker.application.warparty.builders

import com.graphzero.warmaker.application.player.Player
import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.application.world.Coordinates
import com.graphzero.warmaker.config.ApplicationProperties
import com.graphzero.warmaker.domain.enumeration.WarPartyState
import java.time.ZonedDateTime

class WarPartyBuilder {
    private var id: Long? = null
    private var state: WarPartyState? = null
    private var unitsList: HumanUnits? = null
    private var carriedResources: Resources? = null
    private var location: Coordinates? = null
    private var fieldEntryDate: ZonedDateTime? = null
    private var owner: Player? = null
    private var unitsConfig: ApplicationProperties.Units? = null

    fun id(id: Long): WarPartyBuilder {
        this.id = id
        return this
    }

    fun state(state: WarPartyState): WarPartyBuilder {
        this.state = state
        return this
    }

    fun unitsList(unitsList: HumanUnits): WarPartyBuilder {
        this.unitsList = unitsList
        return this
    }

    fun carriedResources(carriedResources: Resources): WarPartyBuilder {
        this.carriedResources = carriedResources
        return this
    }

    fun location(location: Coordinates): WarPartyBuilder {
        this.location = location
        return this
    }

    fun fieldEntryDate(fieldEntryDate: ZonedDateTime): WarPartyBuilder {
        this.fieldEntryDate = fieldEntryDate
        return this
    }

    fun owner(owner: Player?): WarPartyBuilder {
        this.owner = owner
        return this
    }

    fun unitsConfig(unitsConfig: ApplicationProperties.Units): WarPartyBuilder {
        this.unitsConfig = unitsConfig
        return this
    }

    fun build(): WarParty {
        return if (id == null) WarParty(
            state!!,
            unitsList!!,
            carriedResources!!,
            location!!,
            fieldEntryDate!!,
            owner,
            unitsConfig!!
        ) else WarParty(
            id!!, state!!, unitsList!!, carriedResources!!, location!!, fieldEntryDate!!, owner, unitsConfig!!
        )
    }
}
