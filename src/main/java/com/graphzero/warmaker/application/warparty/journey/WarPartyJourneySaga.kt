package com.graphzero.warmaker.application.warparty.journey

import com.fasterxml.jackson.annotation.JsonProperty
import com.google.common.collect.Lists
import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.events.schedulable.AbstractSchedulableEventDto
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneySaga
import com.graphzero.warmaker.application.world.Coordinates
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import java.time.ZonedDateTime

class WarPartyJourneySaga(
    source: Any,
    publisher: ApplicationEventPublisher,
    executionTime: ZonedDateTime,
    ownerId: Long,
    val warPartyId: Long,
    private val movePath: List<Coordinates>,
    val command: WarPartyCommand,
    val returning: Boolean
) : ArmySchedulableEvent(source, ownerId, executionTime, WarMakerArmyEvent.WAR_PARTY_JOURNEY, publisher) {
    private var actualPosition = 0

    fun moveToNextStop(): Int {
        return if (actualPosition + 1 >= movePath.size) {
            actualPosition
        } else {
            ++actualPosition
        }
    }

    fun hasReachedItsDestination(): Boolean {
        return actualPosition == movePath.size - 1
    }

    val actualCoordinates: Coordinates
        get() = movePath[actualPosition]

    val nextStop: Coordinates
        get() = if (actualPosition + 1 >= movePath.size) movePath[movePath.size - 1] else movePath[actualPosition + 1]

    val currentPosition: Coordinates
        get() = if (actualPosition >= movePath.size) movePath[movePath.size - 1] else movePath[actualPosition]

    fun setNextExecutionTime(executionTime: ZonedDateTime) {
        this.executionTime = executionTime
    }

    /**
     * Returns move path without last stop
     *
     * @return
     */
    val backMovePath: List<Coordinates>
        get() = Lists.reverse(movePath.subList(0, movePath.size))
    val destination: Coordinates
        get() = movePath[movePath.size - 1]

    override fun dto(): WarPartyJourneySagaDto {
        return WarPartyJourneySagaDto(
            ownerId, movePath, warPartyId, command, returning,
            executionTime, nextStop, currentPosition
        )
    }

    override fun execute() {
        log.debug("Publishing event: [{}]", uuid)
        publisher!!.publishEvent(this)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as WarPartyJourneySaga

        if (warPartyId != other.warPartyId) return false
        if (movePath != other.movePath) return false
        if (command != other.command) return false
        if (returning != other.returning) return false
        if (actualPosition != other.actualPosition) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + warPartyId.hashCode()
        result = 31 * result + movePath.hashCode()
        result = 31 * result + command.hashCode()
        result = 31 * result + returning.hashCode()
        result = 31 * result + actualPosition
        return result
    }


    class WarPartyJourneySagaDto(
        ownerId: Long,
        @get:JsonProperty("movePath") val movePath: List<Coordinates>,
        @get:JsonProperty("warPartyId") val warPartyId: Long,
        @get:JsonProperty("command") val command: WarPartyCommand,
        @get:JsonProperty("returning") val returning: Boolean,
        @get:JsonProperty("nextStopArrivalTime") val nextStopArrivalTime: ZonedDateTime,
        @get:JsonProperty("nextStop") val nextStop: Coordinates,
        @get:JsonProperty("currentPosition") val currentPosition: Coordinates?
    ) : AbstractSchedulableEventDto(ownerId, WarMakerArmyEvent.WAR_PARTY_JOURNEY)

    companion object {
        private val log = LoggerFactory.getLogger(WarPartyJourneySaga::class.java)
    }
}
