package com.graphzero.warmaker.application.warparty.value

import com.graphzero.warmaker.application.utils.WarMakerDomainErrors
import com.graphzero.warmaker.application.utils.WarMakerDomainException

class WarPartyNotFound : WarMakerDomainException {
    constructor() : super(ERROR_TEXT, error)

    constructor(s: String, throwable: Throwable) : super(s, throwable, error)

    companion object {
        private const val ERROR_TEXT = "War party couldn't be found."
        private val error = WarMakerDomainErrors.WAR_PARTY_NOT_FOUND
    }
}
