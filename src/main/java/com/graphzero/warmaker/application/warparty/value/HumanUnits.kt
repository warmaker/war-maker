package com.graphzero.warmaker.application.warparty.value

import com.graphzero.warmaker.domain.UnitsEntity
import io.vavr.control.Try
import java.util.*

class HumanUnits : UnitsList {
    val id: Long
    override val units: HumanUnitsList

    constructor(id: Long, spearman: Int, swordsman: Int, archers: Int, crossbowman: Int) {
        this.id = id
        units = HumanUnitsList(
            spearman,
            swordsman,
            archers,
            crossbowman
        )
    }

    constructor(id: Long, humanUnitsList: HumanUnitsList) {
        this.id = id
        units = HumanUnitsList(
            humanUnitsList.spearman,
            humanUnitsList.swordsman,
            humanUnitsList.archers,
            humanUnitsList.crossbowman
        )
    }

    constructor(id: Long, units: UnitsEntity) {
        this.id = id
        this.units = HumanUnitsList(
            units.spearmen,
            units.swordsmen,
            units.archers,
            units.crossbowmen
        )
    }

    constructor(id: Long, units: Collection<UnitsEntity>) {
        this.id = id
        this.units = HumanUnitsList(
            units.stream().mapToInt { obj: UnitsEntity -> obj.spearmen }.sum(),
            units.stream().mapToInt { obj: UnitsEntity -> obj.swordsmen }.sum(),
            units.stream().mapToInt { obj: UnitsEntity -> obj.archers }.sum(),
            units.stream().mapToInt { obj: UnitsEntity -> obj.crossbowmen }.sum()
        )
    }

    /** This method always succeeds. If more units are erased, 0 will be set as value.   */
    fun eraseUnits(units: HumanUnitsList): HumanUnits {
        val newUnits = this.units.minus(units)
        return HumanUnits(id, newUnits)
    }

    fun addUnits(unitsToAdd: HumanUnitsList): HumanUnits {
        val newUnits = units.plus(unitsToAdd)
        return HumanUnits(id, newUnits)
    }

    /** Try to remove units  */
    fun removeUnits(unitsToRemove: HumanUnitsList): Try<HumanUnits> {
        if (unitsToRemove.spearman > units.spearman || unitsToRemove.swordsman > units.swordsman || unitsToRemove.archers > units.archers || unitsToRemove.crossbowman > units.crossbowman) return Try.failure(
            NotEnoughUnitsInArmy()
        )
        val newUnits = units.minus(unitsToRemove)
        return Try.success(HumanUnits(id, newUnits))
    }

    fun value(): HumanUnitsList = units

    override fun hasInfantry(): Boolean {
        return units.spearman != 0 || units.swordsman != 0
    }

    companion object {
        fun of(units: UnitsEntity): HumanUnits =
            HumanUnits(units.id, units.spearmen, units.swordsmen, units.archers, units.crossbowmen)

        fun of(units: HumanUnitsList): HumanUnits =
            HumanUnits(-1, units.spearman, units.swordsman, units.archers, units.crossbowman)

        @kotlin.jvm.JvmStatic
        fun concatOf(id: Long, units: List<HumanUnits>): HumanUnits {
            var spearman = 0
            var swordsman = 0
            var archers = 0
            var crossbowman = 0
            for (humanUnits in units) {
                spearman += humanUnits.units.spearman
                swordsman += humanUnits.units.swordsman
                archers += humanUnits.units.archers
                crossbowman += humanUnits.units.crossbowman
            }
            return HumanUnits(id, spearman, swordsman, archers, crossbowman)
        }
    }
}
