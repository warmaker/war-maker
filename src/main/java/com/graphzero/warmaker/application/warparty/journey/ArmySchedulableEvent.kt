package com.graphzero.warmaker.application.warparty.journey

import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.events.schedulable.AbstractSchedulableEvent
import org.springframework.context.ApplicationEventPublisher
import java.time.ZonedDateTime
import java.util.*

abstract class ArmySchedulableEvent(
    source: Any,
    ownerId: Long,
    executionTime: ZonedDateTime,
    eventType: WarMakerArmyEvent,
    @field:Transient protected val publisher: ApplicationEventPublisher?
) : AbstractSchedulableEvent<WarMakerArmyEvent>(source, ownerId, eventType, executionTime, UUID.randomUUID())
