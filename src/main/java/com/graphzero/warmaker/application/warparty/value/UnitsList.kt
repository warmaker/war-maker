package com.graphzero.warmaker.application.warparty.value

internal interface UnitsList {
    fun hasInfantry(): Boolean
    val units: HumanUnitsList
}
