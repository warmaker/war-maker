package com.graphzero.warmaker.application.warparty.journey

import com.fasterxml.jackson.annotation.JsonProperty
import com.graphzero.warmaker.application.world.Coordinates

internal class WarPartyJourneyReq(
    @get:JsonProperty("warPartyId") val warPartyId: Long,
    @get:JsonProperty("path") val path: List<Coordinates>,
    @get:JsonProperty("command") val command: WarPartyCommand
) {
    override fun toString(): String {
        return "WarPartyMovePathReq{" +
                "warPartyId=" + warPartyId +
                ", path=" + path +
                '}'
    }
}
