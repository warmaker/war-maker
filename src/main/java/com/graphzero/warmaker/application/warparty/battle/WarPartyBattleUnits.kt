package com.graphzero.warmaker.application.warparty.battle

import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import com.graphzero.warmaker.config.ApplicationProperties

internal class WarPartyBattleUnits(
    private val unitsConfig: ApplicationProperties.Units,
    var unitsList: HumanUnits
) {
    fun calculateInfantryMeleeAttack(): Long {
        return unitsConfig.spearman.meleeAttack * unitsList.units.spearman + unitsConfig.swordsman.meleeAttack
            .toLong() * unitsList.units.swordsman
    }

    fun calculateInfantryMeleeAPAttack(): Long {
        return unitsConfig.spearman.meleeArmorPenetration * unitsList.units.spearman + unitsConfig.swordsman.meleeArmorPenetration
            .toLong() * unitsList.units.swordsman
    }

    fun calculateArchersRangedAttack(): Long {
        return unitsConfig.archer.rangedAttack * unitsList.units.archers.toLong()
    }

    fun calculateArchersRangedAPAttack(): Long {
        return unitsConfig.archer.rangedArmorPenetration * unitsList.units.archers.toLong()
    }

    fun calculateCrossbowmanRangedAttack(): Long {
        return unitsConfig.crossbowman.rangedAttack * unitsList.units.crossbowman.toLong()
    }

    fun calculateCrossbowmanRangedAPAttack(): Long {
        return unitsConfig.crossbowman.rangedArmorPenetration * unitsList.units.crossbowman.toLong()
    }

    fun calculateArchersMeleeAttack(): Long {
        return unitsConfig.archer.meleeAttack * unitsList.units.archers.toLong()
    }

    fun calculateCrossbowmanMeleeAttack(): Long {
        return unitsConfig.crossbowman.meleeAttack * unitsList.units.crossbowman.toLong()
    }

    fun applyLosesToInfantryFromDamage(damage: Long, armorPenetratingDamage: Long): HumanUnitsList {
        val proportions =
            unitsList.units.spearman.toDouble() / (unitsList.units.swordsman + unitsList.units
                .spearman.toDouble())
        val spearmanArmorValue = unitsConfig.spearman.armor * unitsList.units.spearman.toLong()
        val swordsmanArmorValue = unitsConfig.swordsman.armor * unitsList.units.swordsman.toLong()
        val spearmanDamage = (proportions * damage).toLong() - spearmanArmorValue
        val swordsmanDamage = ((1 - proportions) * damage).toLong() - swordsmanArmorValue
        val armorPenetrationSpearmanLoses = (proportions * armorPenetratingDamage).toInt() / unitsConfig.spearman.health
        val armorPenetrationSwordsmanLoses =
            ((1 - proportions) * armorPenetratingDamage).toInt() / unitsConfig.swordsman.health
        val loses = HumanUnitsList(
            if (spearmanDamage > 0) spearmanDamage.toInt() / unitsConfig.spearman.health + armorPenetrationSpearmanLoses else armorPenetrationSpearmanLoses,
            if (swordsmanDamage > 0) swordsmanDamage.toInt() / unitsConfig.swordsman.health + armorPenetrationSwordsmanLoses else armorPenetrationSwordsmanLoses,
            0, 0
        )
        unitsList = unitsList.eraseUnits(loses)
        return loses
    }

    fun applyLosesToRangedUnitsFromDamage(damage: Long, armorPenetratingDamage: Long): HumanUnitsList {
        val proportions = unitsList.units.archers.toDouble() / (unitsList.units.archers + unitsList.units
            .crossbowman.toDouble())
        val archersArmorValue = unitsConfig.spearman.armor * unitsList.units.spearman .toLong()
        val crossbowmanArmorValue = unitsConfig.swordsman.armor * unitsList.units.swordsman .toLong()
        val archersDamage = (proportions * damage).toLong() - archersArmorValue
        val crossbowmanDamage = ((1 - proportions) * damage).toLong() - crossbowmanArmorValue
        val armorPenetrationArcherLoses = (proportions * armorPenetratingDamage).toInt() / unitsConfig.archer.health
        val armorPenetrationCrossbowmanLoses =
            ((1 - proportions) * armorPenetratingDamage).toInt() / unitsConfig.crossbowman.health
        val loses = HumanUnitsList(
            0, 0,
            if (archersDamage > 0) archersDamage.toInt() / unitsConfig.archer.health + armorPenetrationArcherLoses else armorPenetrationArcherLoses,
            if (crossbowmanDamage > 0) crossbowmanDamage.toInt() / unitsConfig.crossbowman.health + armorPenetrationCrossbowmanLoses else armorPenetrationCrossbowmanLoses
        )
        unitsList = unitsList.eraseUnits(loses)
        return loses
    }

    fun unitsCount(): Int {
        return unitsList.units.numberOfUnits
    }

    val isAnnihilated: Boolean
        get() = unitsList.units.isEmpty
}
