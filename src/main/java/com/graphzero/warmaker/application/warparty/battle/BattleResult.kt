package com.graphzero.warmaker.application.warparty.battle

enum class BattleResult {
    ATTACKER_WINS,
    DEFENDER_WINS,
    DRAW
}
