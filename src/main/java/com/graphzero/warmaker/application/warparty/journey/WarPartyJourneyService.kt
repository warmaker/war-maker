package com.graphzero.warmaker.application.warparty.journey

import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.events.domain.DomainEventsPublisher
import com.graphzero.warmaker.application.events.schedulable.ScheduledEventsService
import com.graphzero.warmaker.application.time.TimeConfiguration
import com.graphzero.warmaker.application.utils.WarMakerDomainErrors
import com.graphzero.warmaker.application.utils.WarMakerDomainException
import com.graphzero.warmaker.application.warparty.WarPartyFactory
import com.graphzero.warmaker.application.warparty.WarPartyRootRepository
import com.graphzero.warmaker.application.warparty.value.WarPartyNotFound
import com.graphzero.warmaker.application.world.RegionsFactory
import com.graphzero.warmaker.domain.WarPartyEntity
import com.graphzero.warmaker.domain.enumeration.WarPartyState
import com.graphzero.warmaker.repository.FieldEntityRepository
import com.graphzero.warmaker.shared.annotations.ApplicationService
import io.vavr.control.Try
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import java.time.Clock
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.function.Consumer
import javax.transaction.Transactional

@ApplicationService
internal class WarPartyJourneyService(
    private val fieldRepository: FieldEntityRepository,
    private val warPartyRepository: WarPartyRootRepository,
    private val warPartyFactory: WarPartyFactory,
    private val regionFactory: RegionsFactory,
    private val scheduledEventsService: ScheduledEventsService,
    private val domainEventsPublisher: DomainEventsPublisher,
    private val publisher: ApplicationEventPublisher,
    private val log: Logger = LoggerFactory.getLogger(WarPartyJourneyService::class.java),
    private val clock: Clock = TimeConfiguration.serverClock
) {

    @Transactional
    fun startWarPartyJourney(command: WarPartyJourneyReq, returning: Boolean): Try<WarPartyJourneySaga> {
        return if (!validateWarPartyJourneySagaCommand(command)) Try.failure(
            WarMakerDomainException(
                WarMakerDomainErrors.WRONG_COMMAND
            )
        ) else warPartyRepository.repository
            .findById(command.warPartyId)
            .map {
                if (it.state == WarPartyState.MOVING) {
                    Try.failure(WarPartyMovingException())
                } else {
                    val saga = buildWarPartyJourneySaga(command, it, warPartyJourneyTime, returning)
                    it.state = WarPartyState.MOVING
                    warPartyRepository.repository.save(it)
                    scheduledEventsService.schedule(saga)
                    Try.success(saga)
                }
            }
            .orElseGet { Try.failure(WarPartyNotFound()) }
    }

    private fun buildWarPartyJourneySaga(
        command: WarPartyJourneyReq,
        warPartyEntity: WarPartyEntity,
        firstExecutionDate: ZonedDateTime,
        returning: Boolean
    ): WarPartyJourneySaga {
        return WarPartyJourneySaga(
            this, publisher,
            firstExecutionDate,
            warPartyEntity.armyEntity.city.owner.id,
            warPartyEntity.id,
            command.path, command.command, returning
        )
    }

    private val warPartyJourneyTime: ZonedDateTime
        private get() = ZonedDateTime.now(clock).plus(5, ChronoUnit.SECONDS)

    private fun validateWarPartyJourneySagaCommand(command: WarPartyJourneyReq): Boolean {
        return command.command == WarPartyCommand.ATTACK || command.command == WarPartyCommand.GARRISON
    }

    @Throws(WarPartyNotFound::class)
    fun moveWarParty(saga: WarPartyJourneySaga): MovementResult {
        val warParty = warPartyFactory.buildWarParty(saga.warPartyId, ZonedDateTime.now(clock))
        val fieldEntity =
            fieldRepository.findByXCoordinateAndYCoordinate(saga.actualCoordinates.x, saga.actualCoordinates.y)
        val field = regionFactory.of(fieldEntity)
        val moveConclusion = warParty.move(field, saga)
        warParty.getDomainEvents().forEach(Consumer { event: DomainEvent<out WarMakerEventType> ->
            domainEventsPublisher.publish(
                event
            )
        })
        field.getDomainEvents().forEach(Consumer { event: DomainEvent<out WarMakerEventType> ->
            domainEventsPublisher.publish(
                event
            )
        })
        warPartyRepository.persistWarPartyPositionUpdate(warParty)
        when (moveConclusion) {
            WarPartyJourneyConclusion.MOVING_TO_NEXT_POSITION -> scheduledEventsService.schedule(saga)
            WarPartyJourneyConclusion.RETURNING -> {
                val returnSaga = startWarPartyJourney(
                    WarPartyJourneyReq(warParty.id, saga.backMovePath, WarPartyCommand.GARRISON),
                    true
                )
                log.debug("War Party with id [{}] returns to [{}]", warParty.id, returnSaga.get().destination)
            }
            else -> {
            }
        }
        return MovementResult(moveConclusion, warParty.getDomainEvents())
    }
}
