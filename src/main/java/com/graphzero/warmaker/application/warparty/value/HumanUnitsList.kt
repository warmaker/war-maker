package com.graphzero.warmaker.application.warparty.value

import com.fasterxml.jackson.annotation.JsonProperty
import com.graphzero.warmaker.domain.UnitsEntity
import java.util.*
import kotlin.math.max

data class HumanUnitsList(
    @get:JsonProperty("spearman") val spearman: Int,
    @get:JsonProperty("swordsman") val swordsman: Int,
    @get:JsonProperty("archers") val archers: Int,
    @get:JsonProperty("crossbowman") val crossbowman: Int
) {
    operator fun minus(list: HumanUnitsList): HumanUnitsList {
        val spearmanValue = max(0, spearman - list.spearman)
        val swordsmanValue = max(0, swordsman - list.swordsman)
        val archersValue = max(0, archers - list.archers)
        val crossbowmanValue = max(0, crossbowman - list.crossbowman)
        return HumanUnitsList(spearmanValue, swordsmanValue, archersValue, crossbowmanValue)
    }

    operator fun plus(units: HumanUnitsList): HumanUnitsList {
        val spearmanValue = spearman + units.spearman
        val swordsmanValue = swordsman + units.swordsman
        val archersValue = archers + units.archers
        val crossbowmanValue = crossbowman + units.crossbowman
        return HumanUnitsList(spearmanValue, swordsmanValue, archersValue, crossbowmanValue)
    }

    val numberOfUnits: Int
        get() = spearman + swordsman + archers + crossbowman
    val isEmpty: Boolean
        get() = spearman == 0 && swordsman == 0 && archers == 0 && crossbowman == 0

    fun copy(): HumanUnitsList {
        return HumanUnitsList(spearman, swordsman, archers, crossbowman)
    }

    companion object {
        fun empty(): HumanUnitsList {
            return HumanUnitsList(0, 0, 0, 0)
        }

        fun of(units: UnitsEntity): HumanUnitsList {
            return HumanUnitsList(units.spearmen, units.swordsmen, units.archers, units.crossbowmen)
        }
    }
}
