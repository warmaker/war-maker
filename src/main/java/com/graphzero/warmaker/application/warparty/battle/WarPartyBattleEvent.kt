package com.graphzero.warmaker.application.warparty.battle

import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.events.domain.DomainEventDto
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import java.util.*

class WarPartyBattleEvent(
    source: Any,
    val warParty: WarPartyBattle,
    val unitsLoses: HumanUnitsList,
    val remainingUnits: HumanUnitsList,
    private val battleReport: BattleReport
) : DomainEvent<WarMakerArmyEvent>(source, WarMakerArmyEvent.WAR_PARTY_BATTLES) {
    override fun dto(): WarPartyBattleDto {
        return WarPartyBattleDto(uuid, warMakerEventType, warParty.owner.id, battleReport)
    }

    class WarPartyBattleDto(uuid: UUID, type: WarMakerEventType, val ownerId: Long, val battleReport: BattleReport) :
        DomainEventDto(uuid, type)
}
