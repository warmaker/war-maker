package com.graphzero.warmaker.application.warparty.battle

import com.graphzero.warmaker.application.time.TimeConfiguration
import com.graphzero.warmaker.application.warparty.battle.Battle
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import org.slf4j.LoggerFactory
import java.time.ZonedDateTime

// TODO fix event because one of the players doesn't get battle message
internal class Battle(
    private val attacker: WarPartyBattleUnits,
    private val defender: WarPartyBattleUnits,
    private val battleModificators: List<BattleAlteration>?,
    attackerWarPartyId: Long,
    attackerOwnerId: Long,
    defenderWarPartyId: Long,
    defenderOwnerId: Long
) {
    private var round = 1
    private var battleState = BattleState.BOTH_HAVE_INFANTRY
    private var executionTime: ZonedDateTime? = null
    private val initialAttackerUnits: HumanUnitsList?
    private val initialDefenderUnits: HumanUnitsList?
    private val attackerWarPartyId: Long
    private val attackerOwnerId: Long
    private val defenderWarPartyId: Long
    private val defenderOwnerId: Long
    private val log = LoggerFactory.getLogger(Battle::class.java)

    fun battle(): BattleReport {
        executionTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        log.debug("Battle begins, ranged units start skirmish")
        log.debug("Attacker before skirmish has [{}] unit left [{}]", attacker.unitsCount(), attacker.unitsList)
        log.debug("Defender before skirmish has [{}] unit left [{}]", defender.unitsCount(), defender.unitsList)
        executeSkirmish()
        while (!attacker.isAnnihilated && !defender.isAnnihilated && round <= ROUNDS_LIMIT) {
            changeState()
            executeRound()
            round++
        }
        log.debug(
            "Battle finished after round [{}], attacker has [{}] units, defender has [{}] units", round - 1,
            attacker.unitsCount(), defender.unitsCount()
        )
        return calculateBattleResult()
    }

    private fun calculateBattleResult(): BattleReport {
        if (defender.isAnnihilated && !attacker.isAnnihilated) {
            return BattleReport.of(
                executionTime!!,
                BattleResult.ATTACKER_WINS,
                getLoses(attacker, initialAttackerUnits),
                getLoses(defender, initialDefenderUnits),
                attackerWarPartyId, defenderWarPartyId,
                attackerOwnerId, defenderOwnerId
            )
        }
        return if (attacker.isAnnihilated && !defender.isAnnihilated) {
            BattleReport.of(
                executionTime!!,
                BattleResult.DEFENDER_WINS,
                getLoses(attacker, initialAttackerUnits),
                getLoses(defender, initialDefenderUnits),
                attackerWarPartyId, defenderWarPartyId,
                attackerOwnerId, defenderOwnerId
            )
        } else BattleReport.of(
            executionTime!!,
            BattleResult.DRAW,
            getLoses(attacker, initialAttackerUnits),
            getLoses(defender, initialDefenderUnits),
            attackerWarPartyId, defenderWarPartyId,
            attackerOwnerId, defenderOwnerId
        )
    }

    private fun getLoses(
        postBattleWarParty: WarPartyBattleUnits,
        initialWarPartyUnits: HumanUnitsList?
    ): HumanUnitsList {
        return initialWarPartyUnits!!.minus(postBattleWarParty.unitsList.units)
    }

    private fun executeRound() {
        log.debug("============= Round number [{}] begins =============", round)
        log.debug("Attacker has {} unit left {}", attacker.unitsCount(), attacker.unitsList)
        log.debug("Defender has {} unit left {}", defender.unitsCount(), defender.unitsList)
        executeRangedBattle()
        val defenderMeleeAttack = defender.calculateInfantryMeleeAttack()
        val attackerMeleeAttack = attacker.calculateInfantryMeleeAttack()
        val defenderMeleeAPAttack = defender.calculateInfantryMeleeAPAttack()
        val attackerMeleeAPAttack = attacker.calculateInfantryMeleeAPAttack()
        when (battleState) {
            BattleState.ATTACKER_NO_INFANTRY -> {
                log.debug("Defender infantry fights against attacker ranged units")
                attacker.applyLosesToRangedUnitsFromDamage(defenderMeleeAttack, defenderMeleeAPAttack)
                defender.applyLosesToInfantryFromDamage(attackerMeleeAttack, attackerMeleeAPAttack)
            }
            BattleState.DEFENDER_NO_INFANTRY -> {
                log.debug("Attacker infantry fights against defender ranged units")
                defender.applyLosesToRangedUnitsFromDamage(attackerMeleeAttack, attackerMeleeAPAttack)
                attacker.applyLosesToInfantryFromDamage(defenderMeleeAttack, defenderMeleeAPAttack)
            }
            BattleState.BOTH_HAVE_INFANTRY -> {
                log.debug("Defender and attacker infantry clashes")
                defender.applyLosesToInfantryFromDamage(attackerMeleeAttack, attackerMeleeAPAttack)
                attacker.applyLosesToInfantryFromDamage(defenderMeleeAttack, defenderMeleeAPAttack)
            }
            BattleState.BOTH_NO_INFANTRY -> log.debug("No infantry left")
        }
    }

    private fun executeRangedBattle() {
        val defenderArchersRangedAttack = defender.calculateArchersRangedAttack()
        val attackerArchersRangedAttack = attacker.calculateArchersRangedAttack()
        val defenderCrossbowmanRangedAttack = defender.calculateCrossbowmanRangedAttack()
        val attackerCrossbowmanRangedAttack = attacker.calculateCrossbowmanRangedAttack()
        val defenderArchersMeleeAttack = defender.calculateArchersMeleeAttack()
        val attackerArchersMeleeAttack = attacker.calculateArchersMeleeAttack()
        val defenderCrossbowmanMeleeAttack = defender.calculateCrossbowmanMeleeAttack()
        val attackerCrossbowmanMeleeAttack = attacker.calculateCrossbowmanMeleeAttack()
        val defenderArchersRangedAPAttack = defender.calculateArchersRangedAPAttack()
        val attackerArchersRangedAPAttack = attacker.calculateArchersRangedAPAttack()
        val defenderCrossbowmanRangedAPAttack = defender.calculateCrossbowmanRangedAPAttack()
        val attackerCrossbowmanRangedAPAttack = attacker.calculateCrossbowmanRangedAPAttack()
        if ((round + 1) % 2 == 0) {
            clashRangeForces(
                defenderArchersRangedAttack, attackerArchersRangedAttack, defenderArchersRangedAPAttack,
                attackerArchersRangedAPAttack, defenderArchersMeleeAttack, attackerArchersMeleeAttack,
                "archers"
            )
        }
        if ((round + 2) % 3 == 0) {
            clashRangeForces(
                defenderCrossbowmanRangedAttack, attackerCrossbowmanRangedAttack,
                defenderCrossbowmanRangedAPAttack, attackerCrossbowmanRangedAPAttack,
                defenderCrossbowmanMeleeAttack, attackerCrossbowmanMeleeAttack, "crossbowman"
            )
        }
        if ((round + 1) % 2 == 0 || (round + 2) % 3 == 0) {
            log.debug(
                "After ranged fire attacker has [{}] unit left [{}]", attacker.unitsCount(), attacker
                    .unitsList
            )
            log.debug(
                "After ranged fire defender has [{}] unit left [{}]", defender.unitsCount(), defender
                    .unitsList
            )
        }
    }

    private fun clashRangeForces(
        defenderArchersRangedAttack: Long, attackerArchersRangedAttack: Long,
        defenderAPAttack: Long,
        attackerAPAttack: Long, defenderArchersMeleeAttack: Long,
        attackerArchersMeleeAttack: Long,
        forceName: String
    ) {
        when (battleState) {
            BattleState.ATTACKER_NO_INFANTRY -> {
                log.debug(
                    "Defender [{}] fire at attacker ranged units, attacker [{}] fight in melee.", forceName,
                    forceName
                )
                attacker.applyLosesToRangedUnitsFromDamage(defenderArchersRangedAttack, defenderAPAttack)
                defender.applyLosesToInfantryFromDamage(attackerArchersMeleeAttack, 0)
            }
            BattleState.DEFENDER_NO_INFANTRY -> {
                log.debug(
                    "Attacker [{}] fire at defender ranged units, defender [{}] fight in melee.", forceName,
                    forceName
                )
                defender
                    .applyLosesToRangedUnitsFromDamage(attackerArchersRangedAttack, attackerAPAttack)
                attacker.applyLosesToInfantryFromDamage(defenderArchersMeleeAttack, 0)
            }
            BattleState.BOTH_HAVE_INFANTRY -> {
                log.debug("Defender and attacker [{}] fire at infantry", forceName)
                attacker.applyLosesToInfantryFromDamage(defenderArchersRangedAttack, defenderAPAttack)
                defender.applyLosesToInfantryFromDamage(attackerArchersRangedAttack, attackerAPAttack)
            }
            BattleState.BOTH_NO_INFANTRY -> {
                log.debug("Attacker and defender [{}] exchange fire", forceName)
                attacker.applyLosesToRangedUnitsFromDamage(defenderArchersRangedAttack, defenderAPAttack)
                defender.applyLosesToRangedUnitsFromDamage(attackerArchersRangedAttack, attackerAPAttack)
            }
        }
    }

    private fun changeState() {
        if (!attacker.unitsList.hasInfantry()) battleState = BattleState.ATTACKER_NO_INFANTRY
        if (!defender.unitsList.hasInfantry()) battleState = BattleState.DEFENDER_NO_INFANTRY
        if (!attacker.unitsList.hasInfantry() && !defender.unitsList.hasInfantry()) battleState =
            BattleState.BOTH_NO_INFANTRY
    }

    private fun executeSkirmish() {
        val defenderArchersAttackPower = defender.calculateArchersRangedAttack()
        val attackerArchersAttackPower = attacker.calculateArchersRangedAttack()
        val defenderCrossbowmanAttackPower = defender.calculateCrossbowmanRangedAttack()
        val attackerCrossbowmanAttackPower = attacker.calculateCrossbowmanRangedAttack()
        val defenderArchersRangedAPAttack = defender.calculateArchersRangedAPAttack()
        val attackerArchersRangedAPAttack = attacker.calculateArchersRangedAPAttack()
        val defenderCrossbowmanRangedAPAttack = defender.calculateCrossbowmanRangedAPAttack()
        val attackerCrossbowmanRangedAPAttack = attacker.calculateCrossbowmanRangedAPAttack()
        attacker.applyLosesToInfantryFromDamage(
            defenderArchersAttackPower + defenderCrossbowmanAttackPower,
            defenderArchersRangedAPAttack + defenderCrossbowmanRangedAPAttack
        )
        defender.applyLosesToInfantryFromDamage(
            attackerArchersAttackPower + attackerCrossbowmanAttackPower,
            attackerArchersRangedAPAttack + attackerCrossbowmanRangedAPAttack
        )
    }

    private enum class BattleState {
        BOTH_HAVE_INFANTRY, ATTACKER_NO_INFANTRY, DEFENDER_NO_INFANTRY, BOTH_NO_INFANTRY
    }

    companion object {
        private const val ROUNDS_LIMIT = 10
    }

    init {
        initialAttackerUnits = attacker.unitsList.units
        initialDefenderUnits = defender.unitsList.units
        this.attackerWarPartyId = attackerWarPartyId
        this.attackerOwnerId = attackerOwnerId
        this.defenderWarPartyId = defenderWarPartyId
        this.defenderOwnerId = defenderOwnerId
    }
}
