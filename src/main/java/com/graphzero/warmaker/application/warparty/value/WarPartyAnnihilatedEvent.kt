package com.graphzero.warmaker.application.warparty.value

import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.events.domain.DomainEventDto
import java.util.*

class WarPartyAnnihilatedEvent(source: Any, val warPartyId: Long) :
    DomainEvent<WarMakerArmyEvent>(source, WarMakerArmyEvent.WAR_PARTY_ANNIHILATED) {
    override fun dto(): WarPartyAnnihilatedEventDto {
        return WarPartyAnnihilatedEventDto(uuid, warMakerEventType, warPartyId)
    }

    class WarPartyAnnihilatedEventDto(uuid: UUID, type: WarMakerEventType, val warPartyId: Long) :
        DomainEventDto(uuid, type)
}
