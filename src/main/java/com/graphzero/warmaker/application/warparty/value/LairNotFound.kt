package com.graphzero.warmaker.application.warparty.value

import com.graphzero.warmaker.application.utils.WarMakerDomainErrors
import com.graphzero.warmaker.application.utils.WarMakerDomainException

internal class LairNotFound : WarMakerDomainException {
    constructor() : super(error)
    constructor(s: String) : super(s, error)
    constructor(s: String, throwable: Throwable) : super(s, throwable, error)

    companion object {
        private val error = WarMakerDomainErrors.LAIR_NOT_FOUND
    }
}
