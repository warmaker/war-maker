package com.graphzero.warmaker.application.warparty

import com.graphzero.warmaker.application.ddd.AggregateRoot
import com.graphzero.warmaker.application.ddd.AggregateRootLogic
import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.player.Player
import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.application.warparty.battle.BattleReport
import com.graphzero.warmaker.application.warparty.battle.BattleResult
import com.graphzero.warmaker.application.warparty.battle.WarPartyBattle
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneyConclusion
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneySaga
import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.application.warparty.value.WarPartyResourcesChangeEvent
import com.graphzero.warmaker.application.world.Coordinates
import com.graphzero.warmaker.application.world.Lootable
import com.graphzero.warmaker.application.world.Region
import com.graphzero.warmaker.config.ApplicationProperties
import com.graphzero.warmaker.domain.enumeration.WarPartyState
import org.slf4j.LoggerFactory
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

class WarParty : AggregateRoot, Lootable {
    private val aggregateRoot: AggregateRootLogic
    var state: WarPartyState
        private set
    var location: Coordinates
        private set
    val unitsList: HumanUnits
    val carriedResources: Resources
    private val fieldEntryDate: ZonedDateTime
    val owner: Player?
    private val unitsConfig: ApplicationProperties.Units

    constructor(
        id: Long,
        state: WarPartyState,
        unitsList: HumanUnits,
        carriedResources: Resources,
        location: Coordinates,
        fieldEntryDate: ZonedDateTime,
        owner: Player?,
        unitsConfig: ApplicationProperties.Units
    ) {
        aggregateRoot = AggregateRootLogic(id)
        this.state = state
        this.unitsList = unitsList
        this.location = location
        this.carriedResources = carriedResources
        this.fieldEntryDate = fieldEntryDate
        this.owner = owner
        this.unitsConfig = unitsConfig
    }

    constructor(
        state: WarPartyState,
        unitsList: HumanUnits,
        carriedResources: Resources,
        location: Coordinates,
        fieldEntryDate: ZonedDateTime,
        owner: Player?,
        unitsConfig: ApplicationProperties.Units
    ) {
        aggregateRoot =
            AggregateRootLogic(null) // TODO nullable id - think of better way to represent non-persitable aggregate roots
        this.state = state
        this.unitsList = unitsList
        this.location = location
        this.carriedResources = carriedResources
        this.fieldEntryDate = fieldEntryDate
        this.owner = owner
        this.unitsConfig = unitsConfig
    }

    /**
     * Move starts from the first field.
     *
     * @param field - next journey stop
     * @param saga  - current journey of War Party
     * @return WarPartyJourneyConclusion.class
     */
    fun move(field: Region, saga: WarPartyJourneySaga): WarPartyJourneyConclusion {
        location = Coordinates(field.coordinates!!.x, field.coordinates!!.y)
        log.debug("War Party with id [{}] enters: [{}]", saga.warPartyId, field)
        val journeyConclusion = field.enter(this, saga)
        return handleJourneyConclusion(journeyConclusion, saga)
    }

    private fun handleJourneyConclusion(
        journeyConclusion: WarPartyJourneyConclusion,
        saga: WarPartyJourneySaga
    ): WarPartyJourneyConclusion {
        return when (journeyConclusion) {
            WarPartyJourneyConclusion.ANNIHILATED -> journeyConclusion
            WarPartyJourneyConclusion.MOVING_TO_NEXT_POSITION -> {
                state = WarPartyState.MOVING
                saga.moveToNextStop()
                saga.setNextExecutionTime(fieldEntryDate.plus(2, ChronoUnit.SECONDS))
                journeyConclusion
            }
            else -> {
                state = WarPartyState.STATIONED
                journeyConclusion
            }
        }
    }

    fun battle(attacker: WarParty): BattleReport {
        log.debug("Battle between defender [{}] and attacker [{}] starts", id, attacker.id)
        val warPartyBattle = WarPartyBattle(AggregateRootLogic(id), unitsList, owner!!, unitsConfig)
        val attackerWarPartyBattle =
            WarPartyBattle(AggregateRootLogic(attacker.id), attacker.unitsList, attacker.owner!!, unitsConfig)
        val battleReport = warPartyBattle.battle(attackerWarPartyBattle, null)
        when (battleReport.battleResult) {
            BattleResult.ATTACKER_WINS -> attacker.collectLoot(this)
            BattleResult.DEFENDER_WINS -> collectLoot(attacker)
            BattleResult.DRAW -> {
            }
        }
        registerDomainEvents(warPartyBattle.getDomainEvents())
        attacker.registerDomainEvents(attackerWarPartyBattle.getDomainEvents())
        return battleReport
    }

    fun addUnits(unitsList: HumanUnits) {
        this.unitsList.addUnits(unitsList.units)
    }

    private fun collectLoot(loser: Lootable) {
        val loot = loser.loot()
        carriedResources.collect(loot)
        registerDomainEvent(WarPartyResourcesChangeEvent(this, id, loot))
    }

    override fun loot(): ResourcesValue {
        val lootedResources = carriedResources.value()
        carriedResources.plunder()
        registerDomainEvent(WarPartyResourcesChangeEvent(this, id, lootedResources.reverse()))
        return lootedResources
    }

    val isAnnihilated: Boolean
        get() = getDomainEvents().stream()
            .anyMatch { domainEvent: DomainEvent<out WarMakerEventType> -> domainEvent.warMakerEventType == WarMakerArmyEvent.WAR_PARTY_ANNIHILATED }
    override val id: Long
        get() = aggregateRoot.id!!

    override fun registerDomainEvent(event: DomainEvent<out WarMakerEventType>) {
        aggregateRoot.registerDomainEvent(event)
    }

    override fun clearEvents() {
        aggregateRoot.clearEvents()
    }

    override fun getDomainEvents(): List<DomainEvent<out WarMakerEventType>> {
        return aggregateRoot.getDomainEvents()
    }

    override fun toString(): String {
        return "WarParty{" +
                "coordinates=" + location +
                ", unitsList=" + unitsList +
                ", carriedResources=" + carriedResources +
                '}'
    }

    companion object {
        private val log = LoggerFactory.getLogger(WarParty::class.java)
    }
}
