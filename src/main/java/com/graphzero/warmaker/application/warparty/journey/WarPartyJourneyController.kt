package com.graphzero.warmaker.application.warparty.journey

import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/war-party")
internal class WarPartyJourneyController(private val warPartyJourneyService: WarPartyJourneyService) {
    private val log = LoggerFactory.getLogger(WarPartyJourneyController::class.java)

    @PostMapping("/move")
    fun moveWarParty(@RequestBody req: WarPartyJourneyReq): ResponseEntity<Any> {
        log.debug("REST request to move War Party [{}] to {}", req.warPartyId, req.path)
        val saga = warPartyJourneyService.startWarPartyJourney(req, false)
        return saga.fold(
            { ResponseEntity.badRequest().body(it.message) },
            { ResponseEntity.ok(it) }
        )
    }
}
