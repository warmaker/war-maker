package com.graphzero.warmaker.application.warparty.value

import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.events.domain.DomainEventDto
import com.graphzero.warmaker.application.resources.ResourcesValue
import java.util.*

class WarPartyResourcesChangeEvent(source: Any, val warPartyId: Long, val resourcesChange: ResourcesValue) :
    DomainEvent<WarMakerArmyEvent>(source, WarMakerArmyEvent.WAR_PARTY_RESOURCES_CHANGE) {
    override fun dto(): WarPartyResourcesChangeEventDto {
        return WarPartyResourcesChangeEventDto(uuid, warPartyId, resourcesChange)
    }

    class WarPartyResourcesChangeEventDto(uuid: UUID, val warPartyId: Long, val resourcesChange: ResourcesValue) :
        DomainEventDto(uuid, WarMakerArmyEvent.WAR_PARTY_RESOURCES_CHANGE)
}
