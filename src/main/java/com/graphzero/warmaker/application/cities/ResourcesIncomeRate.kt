package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.domain.enumeration.ResourceType
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.function.BiConsumer

class ResourcesIncomeRate private constructor() {
    var denarsIncome = 0.0
        private set
    var woodIncome = 0.0
        private set
    var stoneIncome = 0.0
        private set
    var steelIncome = 0.0
        private set
    var foodIncome = 0.0
        private set
    var faithIncome = 0.0
        private set
    var timeFormat: ChronoUnit? = null
        private set

    fun raiseDenarsIncome(denarsIncome: Double) {
        this.denarsIncome += denarsIncome
    }

    fun raiseWoodIncome(woodIncome: Double) {
        this.woodIncome += woodIncome
    }

    fun raiseStoneIncome(stoneIncome: Double) {
        this.stoneIncome += stoneIncome
    }

    fun raiseSteelIncome(steelIncome: Double) {
        this.steelIncome += steelIncome
    }

    fun raiseFoodIncome(foodIncome: Double) {
        this.foodIncome += foodIncome
    }

    fun raiseFaithIncome(faithIncome: Double) {
        this.faithIncome += faithIncome
    }

    companion object {
        @kotlin.jvm.JvmStatic
        fun of(income: Map<ResourceType, Double>): ResourcesIncomeRate {
            val obj = ResourcesIncomeRate()
            obj.timeFormat = ChronoUnit.SECONDS
            income.forEach { (key: ResourceType, value: Double) ->
                when (key) {
                    ResourceType.DENARS -> obj.raiseDenarsIncome(value)
                    ResourceType.WOOD -> obj.raiseWoodIncome(value)
                    ResourceType.STONE -> obj.raiseStoneIncome(value)
                    ResourceType.STEEL -> obj.raiseSteelIncome(value)
                    ResourceType.FOOD -> obj.raiseFoodIncome(value)
                    ResourceType.FAITH -> obj.raiseFaithIncome(value)
                }
            }
            return obj
        }
    }
}
