package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.application.army.ArmyEventsRepository
import com.graphzero.warmaker.application.cities.buildings.BuildingsFactory
import com.graphzero.warmaker.application.cities.buildings.CityBuildings
import com.graphzero.warmaker.application.cities.recruitment.CityRecruitment
import com.graphzero.warmaker.application.cities.region.CityRegion
import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.application.world.Coordinates.Companion.of
import com.graphzero.warmaker.config.ApplicationProperties
import com.graphzero.warmaker.domain.CityEntity
import com.graphzero.warmaker.domain.enumeration.CityType
import org.springframework.stereotype.Service

@Service
class CityFactory(
    private val armyEventsRepository: ArmyEventsRepository,
    private val buildingsFactory: BuildingsFactory,
    private val properties: ApplicationProperties
) {
    /**
     * Creates CityRegion.
     *
     * @param entity
     * @return
     */
    fun buildCityRegion(entity: CityEntity, warParties: List<WarParty>): CityRegion {
        val cityEvents = getCityEvents(entity)
        return CityRegion(
            of(entity),
            entity.id,
            getCityInfo(entity),
            buildingsFactory.buildings(entity, cityEvents),
            Resources.of(entity.storedResources),
            entity.storedResources.changeDate,
            warParties
        )
    }

    /**
     * Creates CityRecruitment.
     *
     * @param entity
     * @return
     */
    fun buildCityRecruitment(entity: CityEntity): CityRecruitment {
        val cityEvents = getCityEvents(entity)
        return CityRecruitment(
            entity.id,
            getCityInfo(entity),
            buildingsFactory.buildings(entity, cityEvents),
            Resources.of(entity.storedResources),
            entity.storedResources.changeDate,
            cityEvents,
            properties.getUnits()
        )
    }

    /**
     * Creates CityBuildings.
     *
     * @param entity
     * @return
     */
    fun buildCityBuildings(entity: CityEntity): CityBuildings {
        val cityEvents = getCityEvents(entity)
        return CityBuildings(
            entity.id,
            getCityInfo(entity),
            buildingsFactory.buildings(entity, cityEvents),
            Resources.of(entity.storedResources),
            entity.storedResources.changeDate,
            cityEvents
        )
    }

    private fun getCityInfo(cityEntity: CityEntity): CityInfo {
        return CityInfoBuilder()
            .ownerId(cityEntity.owner.id)
            .armyId(cityEntity.army.id)
            .cityName(cityEntity.cityName)
            .cityType(cityEntity.cityType)
            .recruitmentModifier(1.0)
            .buildSpeedModifier(1.0)
            .createCityInfo()
    }

    private fun getCityEvents(entity: CityEntity): CityScheduledEvents {
        val unitsRecruitmentEvents = armyEventsRepository
            .getAllUnitsRecruitmentEvents(entity.army.id)
        val buildingUpgradeEvents = buildingsFactory.getBuildingsEvents(entity)
            .currentStartedBuildingUpgradeEvents
        return CityScheduledEvents(unitsRecruitmentEvents, buildingUpgradeEvents)
    }

    enum class IgnoreField {
        LOCATION, ARMY
    }

    internal class CityInfoBuilder {
        private var ownerId: Long = 0
        private var armyId: Long = 0
        private var cityName: String? = null
        private var cityType: CityType? = null
        private var recruitmentModifier: Double? = null
        private var buildSpeedModifier: Double? = null
        fun ownerId(ownerId: Long): CityInfoBuilder {
            this.ownerId = ownerId
            return this
        }

        fun armyId(armyId: Long): CityInfoBuilder {
            this.armyId = armyId
            return this
        }

        fun cityName(cityName: String): CityInfoBuilder {
            this.cityName = cityName
            return this
        }

        fun cityType(cityType: CityType): CityInfoBuilder {
            this.cityType = cityType
            return this
        }

        fun recruitmentModifier(recruitmentModifier: Double): CityInfoBuilder {
            this.recruitmentModifier = recruitmentModifier
            return this
        }

        fun buildSpeedModifier(buildSpeedModifier: Double): CityInfoBuilder {
            this.buildSpeedModifier = buildSpeedModifier
            return this
        }

        fun createCityInfo(): CityInfo {
            return CityInfo(ownerId, armyId, cityName!!, cityType!!, recruitmentModifier!!, buildSpeedModifier)
        }
    }
}
