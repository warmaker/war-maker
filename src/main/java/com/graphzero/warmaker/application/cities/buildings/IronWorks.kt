package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.events.WarMakerBuildingEvent
import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.domain.enumeration.ResourceType
import java.util.*

class IronWorks(stats: InfrastructureBaseStats, level: Int, upgradingLevel: Int) :
    CityInfrastructure(stats, level, upgradingLevel, BuildingName.IRONWORKS), ResourceInfrastructure {
    override fun nextLevelCost(): ResourcesValue {
        return ResourcesValue(level * 1000.0 + 2500,
            level * 500 + 1000.0,
            level * 70 + 500.0,
            level * 30 + 100.0,
            0.0,
            0.0)
    }

    override fun calculatePointValue(): Long {
        return level * stats.basePointValue
    }

    override fun calculateToughness(): Long {
        return level * stats.basePointValue
    }

    override val upgradeEvent: WarMakerBuildingEvent
        get() = WarMakerBuildingEvent.UPGRADE_IRONWORKS

    override fun canUpgrade(buildings: Buildings): Boolean {
        return true
    }

    override fun instance(level: Int): IronWorks {
        return IronWorks(stats, level, upgradingLevel)
    }

    override fun enhance() {}
    override fun incomeModifier(): Map<ResourceType, Double> {
        return Collections.singletonMap(ResourceType.STEEL, level * 0.1)
    }
}
