package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.domain.enumeration.ResourceType


/**
 * Interface marking building as a resource generating infrastructure
 */
internal interface ResourceInfrastructure {
    fun incomeModifier(): Map<ResourceType, Double>
}
