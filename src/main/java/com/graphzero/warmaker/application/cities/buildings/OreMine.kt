package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.events.WarMakerBuildingEvent
import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.domain.enumeration.ResourceType
import java.util.*

class OreMine(stats: InfrastructureBaseStats, level: Int, upgradingLevel: Int) :
    CityInfrastructure(stats, level, upgradingLevel, BuildingName.ORE_MINE), ResourceInfrastructure {
    override fun nextLevelCost(): ResourcesValue {
        return ResourcesValue(level * 700 + 1500.0,
            level * 400 + 1000.0,
            level * 50 + 500.0,
            level * 20 + 100.0,
            0.0,
            0.0)
    }

    override fun calculatePointValue(): Long {
        return (level + 3) * stats.basePointValue
    }

    override fun calculateToughness(): Long {
        return (level + 3) * stats.basePointValue
    }

    override val upgradeEvent: WarMakerBuildingEvent
        get() = WarMakerBuildingEvent.UPGRADE_ORE_MINE

    override fun canUpgrade(buildings: Buildings): Boolean {
        return true
    }

    override fun instance(level: Int): OreMine {
        return OreMine(stats, level, upgradingLevel)
    }

    override fun enhance() {}
    override fun incomeModifier(): Map<ResourceType, Double> {
        return Collections.singletonMap(ResourceType.STONE, level * 0.2)
    }
}
