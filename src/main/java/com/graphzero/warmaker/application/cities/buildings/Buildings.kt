package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.domain.enumeration.ResourceType
import io.vavr.control.Either
import java.util.*
import java.util.function.Function
import java.util.stream.Collectors
import java.util.stream.Stream

class Buildings(
    val id: Long,
    val sawMill: SawMill,
    val oreMine: OreMine,
    val goldMine: GoldMine,
    val ironworks: IronWorks,
    val cityHall: CityHall,
    val university: University,
    val forge: Forge,
    val barracks: Barracks?
) {
    val resourceIncome: Map<ResourceType, Double>
        get() = Stream
            .of(
                sawMill.incomeModifier().entries.stream(),
                oreMine.incomeModifier().entries.stream(),
                goldMine.incomeModifier().entries.stream(),
                ironworks.incomeModifier().entries.stream()
            )
            .flatMap(Function.identity())
            .collect(
                Collectors.toMap(
                    { it.key },
                    { it.value })
            )

    fun <T : CityInfrastructure> scheduleBuildingUpgrade(
        building: T,
        totalBuildCost: ResourcesValue
    ): Either<BuildingUpgradeProhibited, StartedBuildingUpgradeEvent> {
        return if (building.canUpgrade(this)) {
            Either.right(
                cityHall.scheduleBuildingUpgrade(
                    building,
                    totalBuildCost
                )
            )
        } else {
            Either.left(BuildingUpgradeProhibited(BuildingUpgradeProhibited.Reason.REQUIREMENTS_NOT_MATCHED))
        }
    }

    fun calculateBuildingUpgradeAvailability(): BuildingsUpgradeRequirementMatched {
        return BuildingsUpgradeRequirementMatched(
            sawMill.canUpgrade(this),
            oreMine.canUpgrade(this),
            goldMine.canUpgrade(this),
            ironworks.canUpgrade(this),
            cityHall.canUpgrade(this),
            university.canUpgrade(this),
            forge.canUpgrade(this),
            barracks!!.canUpgrade(this)
        )
    }

    val barracksActualLevel: Int
        get() = barracks!!.level

    class BuildingsBuilder {
        private var id: Long = 0
        private var sawMill: SawMill? = null
        private var oreMine: OreMine? = null
        private var goldMine: GoldMine? = null
        private var ironworks: IronWorks? = null
        private var cityHall: CityHall? = null
        private var university: University? = null
        private var forge: Forge? = null
        private var barracks: Barracks? = null
        fun id(id: Long): BuildingsBuilder {
            this.id = id
            return this
        }

        fun sawMill(sawMill: SawMill): BuildingsBuilder {
            this.sawMill = sawMill
            return this
        }

        fun oreMine(oreMine: OreMine): BuildingsBuilder {
            this.oreMine = oreMine
            return this
        }

        fun goldMine(goldMine: GoldMine): BuildingsBuilder {
            this.goldMine = goldMine
            return this
        }

        fun ironworks(ironworks: IronWorks): BuildingsBuilder {
            this.ironworks = ironworks
            return this
        }

        fun cityHall(cityHall: CityHall): BuildingsBuilder {
            this.cityHall = cityHall
            return this
        }

        fun university(university: University): BuildingsBuilder {
            this.university = university
            return this
        }

        fun forge(forge: Forge): BuildingsBuilder {
            this.forge = forge
            return this
        }

        fun barracks(barracks: Barracks): BuildingsBuilder {
            this.barracks = barracks
            return this
        }

        fun createBuildings(): Buildings {
            return Buildings(id, sawMill!!, oreMine!!, goldMine!!, ironworks!!, cityHall!!, university!!, forge!!, barracks)
        }
    }

    class BuildingsUpgradeRequirementMatched(
        val canUpgradeSawmill: Boolean,
        val canUpgradeOreMine: Boolean,
        val canUpgradeGoldMine: Boolean,
        val canUpgradeIronworks: Boolean,
        val canUpgradeCityHall: Boolean,
        val canUpgradeUniversity: Boolean,
        val canUpgradeForge: Boolean,
        val canUpgradeBarracks: Boolean
    ) {

        override fun hashCode(): Int {
            return Objects.hash(
                canUpgradeSawmill,
                canUpgradeOreMine,
                canUpgradeGoldMine,
                canUpgradeIronworks,
                canUpgradeCityHall,
                canUpgradeUniversity,
                canUpgradeForge,
                canUpgradeBarracks
            )
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as BuildingsUpgradeRequirementMatched

            if (canUpgradeSawmill != other.canUpgradeSawmill) return false
            if (canUpgradeOreMine != other.canUpgradeOreMine) return false
            if (canUpgradeGoldMine != other.canUpgradeGoldMine) return false
            if (canUpgradeIronworks != other.canUpgradeIronworks) return false
            if (canUpgradeCityHall != other.canUpgradeCityHall) return false
            if (canUpgradeUniversity != other.canUpgradeUniversity) return false
            if (canUpgradeForge != other.canUpgradeForge) return false
            if (canUpgradeBarracks != other.canUpgradeBarracks) return false

            return true
        }
    }
}
