package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.cities.buildings.CityBuildingsListener
import com.graphzero.warmaker.application.events.WarMakerBuildingEvent
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
internal class CityBuildingsListener(private val cityBuildingsService: CityBuildingsService) {

    @EventListener
    fun handleUpgradeBuildingEvent(event: UpgradeBuildingScheduledEvent) {
        log.debug("Event to upgrade building for city {}", event.cityId)
        when (event.warMakerEventType) {
            WarMakerBuildingEvent.UPGRADE_SAWMILL -> cityBuildingsService.upgradeSawMill(event.cityId)
            WarMakerBuildingEvent.UPGRADE_ORE_MINE -> cityBuildingsService.upgradeOreMine(event.cityId)
            WarMakerBuildingEvent.UPGRADE_GOLD_MINE -> cityBuildingsService.upgradeGoldMine(event.cityId)
            WarMakerBuildingEvent.UPGRADE_IRONWORKS -> cityBuildingsService.upgradeIronworks(event.cityId)
            WarMakerBuildingEvent.UPGRADE_BARRACKS -> cityBuildingsService.upgradeBarracks(event.cityId)
            WarMakerBuildingEvent.UPGRADE_CITY_HALL -> cityBuildingsService.upgradeCityHall(event.cityId)
            WarMakerBuildingEvent.UPGRADE_UNIVERSITY -> cityBuildingsService.upgradeUniversity(event.cityId)
            WarMakerBuildingEvent.UPGRADE_FORGE -> cityBuildingsService.upgradeForge(event.cityId)
            else -> {
            }
        }
    }

    companion object {
        private val log = LoggerFactory.getLogger(CityBuildingsListener::class.java)
    }
}
