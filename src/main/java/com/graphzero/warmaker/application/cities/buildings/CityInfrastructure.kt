package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.events.WarMakerBuildingEvent
import com.graphzero.warmaker.application.resources.ResourcesValue
import java.util.stream.IntStream

abstract class CityInfrastructure(
    val stats: InfrastructureBaseStats,
    /**
     * Return actual level
     */
    var level: Int,
    var upgradingLevel: Int,
    val name: BuildingName,
    var pointValue: Long = 0,
    var toughness: Double = 0.0
) {

    protected var buildingTimeModificator = 0.0
    abstract fun calculatePointValue(): Long
    abstract fun calculateToughness(): Long
    abstract fun nextLevelCost(): ResourcesValue
    abstract val upgradeEvent: WarMakerBuildingEvent
    protected abstract fun instance(level: Int): CityInfrastructure
    abstract fun canUpgrade(buildings: Buildings): Boolean

    /**
     * This method should be overwritten to allow customized behavior while upgrading building
     */
    protected abstract fun enhance()

    /**
     * Base method ensuring correct level increase. Should be used by external API's
     */
    fun increaseLevel() {
        raiseBaseStats()
        enhance()
    }

    /**
     * Return level which corresponds to the level building will have
     */
    val futureLevel: Int
        get() = level + upgradingLevel - 1

    private fun raiseBaseStats() {
        level += 1
        pointValue = calculatePointValue()
        toughness = calculateToughness().toDouble()
    }

    fun resourceCostForLevel(targetLevel: Int): ResourcesValue {
        val building = instance(targetLevel - 1)
        if (targetLevel < level) return building.nextLevelCost()
        IntStream
            .range(0, targetLevel - building.level - 1)
            .forEach { building.increaseLevel() }
        return building.nextLevelCost()
    }

    enum class BuildingName(val buildingName: String) {
        GOLD_MINE("Gold_Mine"), SAW_MILL("Saw_Mill"), ORE_MINE("Ore_Mine"), IRONWORKS("Ironworks"), BARRACKS("Barracks"), UNIVERSITY(
            "University"
        ),
        FORGE("Forge"), CITY_HALL("City_Hall");
    }
}
