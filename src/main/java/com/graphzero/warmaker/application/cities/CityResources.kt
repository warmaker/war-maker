package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.application.time.TimeConfiguration
import com.graphzero.warmaker.domain.enumeration.ResourceType
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.*

/**
 * Domain object grouping resources related objects, used in City aggregate
 */
class CityResources(
    var storedResources: Resources,
    val incomeRate: ResourcesIncomeRate,
    var changeDate: ZonedDateTime?
) {
    /**
     * Method used to lazily updates resources. Returns current value. Overwrites state so use carefully.
     *
     * @return upgraded resources
     */
    fun updateResources(): CityResources {
        val timeNow = time
        val interval = ChronoUnit.SECONDS.between(changeDate, timeNow)
        if (interval > 0) {
            storedResources = raiseResources(interval)
            changeDate = timeNow
        }
        return this
    }

    private val time: ZonedDateTime
        private get() = ZonedDateTime.now(TimeConfiguration.serverClock)

    fun raiseIncome(resource: Map.Entry<ResourceType, Double>): ResourcesIncomeRate {
        when (resource.key) {
            ResourceType.DENARS -> incomeRate.raiseDenarsIncome(resource.value)
            ResourceType.WOOD -> incomeRate.raiseWoodIncome(resource.value)
            ResourceType.STONE -> incomeRate.raiseStoneIncome(resource.value)
            ResourceType.STEEL -> incomeRate.raiseSteelIncome(resource.value)
            ResourceType.FOOD -> incomeRate.raiseFoodIncome(resource.value)
            ResourceType.FAITH -> incomeRate.raiseFaithIncome(resource.value)
        }
        return incomeRate
    }

    fun hasEnough(resources: ResourcesValue): Boolean {
        return storedResources.denars >= resources.denars && storedResources.wood >= resources.wood && storedResources.stone >= resources.stone && storedResources.steel >= resources.steel && storedResources.food >= resources.food && storedResources.faith >= resources.faith
    }

    fun subtract(resources: ResourcesValue): Boolean {
        if (hasEnough(resources)) {
            storedResources.substract(resources)
            return true
        }
        return false
    }

    private fun raiseResources(seconds: Long): Resources {
        val resourcesIncome = ResourcesValue(
            seconds * incomeRate.denarsIncome,
            seconds * incomeRate.woodIncome,
            seconds * incomeRate.stoneIncome,
            seconds * incomeRate.steelIncome,
            seconds * incomeRate.foodIncome,
            seconds * incomeRate.faithIncome
        )
        storedResources.add(resourcesIncome)
        return storedResources
    }

    fun addLoot(resourcesValue: ResourcesValue) {
        storedResources.add(resourcesValue)
    }
}
