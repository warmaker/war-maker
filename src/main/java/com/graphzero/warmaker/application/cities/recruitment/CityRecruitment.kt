package com.graphzero.warmaker.application.cities.recruitment

import com.graphzero.warmaker.application.army.StartedUnitsRecruitmentEvent
import com.graphzero.warmaker.application.army.value.RecruitUnitOrder
import com.graphzero.warmaker.application.army.value.RecruitmentOrders
import com.graphzero.warmaker.application.cities.*
import com.graphzero.warmaker.application.cities.buildings.Buildings
import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.application.time.TimeConfiguration
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import com.graphzero.warmaker.config.ApplicationProperties
import io.vavr.control.Either
import java.time.ZonedDateTime
import java.util.*
import kotlin.math.max

/**
 * Aggregate root modeling City domain object
 */
class CityRecruitment(
    id: Long,
    info: CityInfo,
    buildings: Buildings,
    resource: Resources,
    resourcesChangeDate: ZonedDateTime,
    private val cityScheduledEvents: CityScheduledEvents,
    private val unitsConfig: ApplicationProperties.Units
) : CityDtoProvider, City(id = id, resource = resource, resourcesChangeDate = resourcesChangeDate, info, buildings = buildings) {

    fun recruitUnits(unitsList: HumanUnitsList): Either<RecruitmentFailure, ResourcesValue> {
        val totalRecruitmentCost = calculateBaseUnitsCost(unitsList)
        if (!resources.hasEnough(totalRecruitmentCost)) {
            return Either.left(RecruitmentFailure.notEnoughResources())
        }
        if (unitsList.isEmpty) {
            return Either.left(RecruitmentFailure.emptyRecruitmentOrder())
        }
        resources.subtract(totalRecruitmentCost)
        val recruitmentResults = recruitUnits(unitsList, totalRecruitmentCost)
        registerDomainEvent(recruitmentResults.event)
        return Either.right(recruitmentResults.recruitmentCost)
    }

    private fun recruitUnits(unitsList: HumanUnitsList, recruitmentCost: ResourcesValue): RecruitmentResult {
        val event = StartedUnitsRecruitmentEvent(
            this, UUID.randomUUID(), id, info.armyId,
            RecruitmentOrders(
                RecruitUnitOrder(
                    unitsList.spearman,
                    calculateSecondsToRecruitOneUnit(unitsConfig.getSpearman().recruitmentTime)
                ),
                RecruitUnitOrder(
                    unitsList.swordsman,
                    calculateSecondsToRecruitOneUnit(unitsConfig.getSwordsman().recruitmentTime)
                ),
                RecruitUnitOrder(
                    unitsList.archers,
                    calculateSecondsToRecruitOneUnit(unitsConfig.getArcher().recruitmentTime)
                ),
                RecruitUnitOrder(
                    unitsList.crossbowman,
                    calculateSecondsToRecruitOneUnit(unitsConfig.getCrossbowman().recruitmentTime)
                )
            ), ZonedDateTime.now(TimeConfiguration.serverClock)
        )
        if (cityScheduledEvents.areUnitsRecruited()) {
            val eventFinishTime = cityScheduledEvents.calculateRecruitmentFinishTime(event)
            event.finishTime = eventFinishTime
        }
        return RecruitmentResult(recruitmentCost, unitsList, event)
    }

    private fun calculateSecondsToRecruitOneUnit(baseRecruitmentTime: Int): Int {
        val time = baseRecruitmentTime - buildings.barracksActualLevel * 2
        return max(time, 1)
    }

    fun calculateBaseUnitsCost(unitsList: HumanUnitsList): ResourcesValue {
        return getUnitCost(unitsList.spearman, unitsConfig.getSpearman())
            .add(getUnitCost(unitsList.swordsman, unitsConfig.getSwordsman()))
            .add(getUnitCost(unitsList.archers, unitsConfig.getArcher()))
            .add(getUnitCost(unitsList.crossbowman, unitsConfig.getCrossbowman()))
    }

    private fun getUnitCost(number: Int, unit: ApplicationProperties.UnitsConfig): ResourcesValue {
        return ResourcesValue(
            (number * unit.getUnitsCost().gold).toDouble(),
            (number * unit.getUnitsCost().wood).toDouble(),
            (number * unit.getUnitsCost().stone).toDouble(),
            (number * unit.getUnitsCost().iron).toDouble(),
            (number * unit.getUnitsCost().food).toDouble(),
            (number * unit.getUnitsCost().faith).toDouble()
        )
    }

    override fun fillDto(dto: CityDto): CityDto {
        dto.recruitmentOrders = cityScheduledEvents.getRecruitmentOrders()
        return dto
    }
}
