package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.application.cities.buildings.Buildings
import com.graphzero.warmaker.application.ddd.AggregateRoot
import com.graphzero.warmaker.application.ddd.AggregateRootLogic
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.resources.Resources
import java.time.ZonedDateTime

/**
 * Aggregate root modeling City domain object
 */
open class City protected constructor(
    id: Long,
    resource: Resources,
    resourcesChangeDate: ZonedDateTime,
    protected val info: CityInfo,
    private val aggregateRoot: AggregateRootLogic = AggregateRootLogic(id),
    val buildings: Buildings
) : AggregateRoot {
    val resources: CityResources

    private fun cityIncome(): ResourcesIncomeRate {
        return ResourcesIncomeRate.of(buildings.resourceIncome)
    }

    fun collectDto(vararg cityDomain: CityDtoProvider): CityDto {
        val cityDto = CityDto(
            aggregateRoot.id!!,
            info,
            resources
        )
        for (x in cityDomain) {
            x.fillDto(cityDto)
        }
        return cityDto
    }

    override val id: Long
        get() = aggregateRoot.id!!

    override fun registerDomainEvent(event: DomainEvent<out WarMakerEventType>) {
        aggregateRoot.registerDomainEvent(event)
    }

    override fun clearEvents() {
        aggregateRoot.clearEvents()
    }

    override fun getDomainEvents(): List<DomainEvent<out WarMakerEventType>> {
        return aggregateRoot.getDomainEvents()
    }

    init {
        resources = CityResources(resource, cityIncome(), resourcesChangeDate)
        resources.updateResources()
    }
}
