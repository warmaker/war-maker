package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.cities.CityDomainEvent
import com.graphzero.warmaker.application.events.WarMakerBuildingEvent
import com.graphzero.warmaker.application.resources.ResourcesValue
import org.springframework.context.ApplicationEventPublisher
import java.time.ZonedDateTime
import java.util.*

/**
 * Domain event not connected to any of the publishing systems (like Spring ApplicationEvent)
 */
class StartedBuildingUpgradeEvent(
    source: Any, uuid: UUID,
    warMakerEventType: WarMakerBuildingEvent,
    cityId: Long,
    private val ownerId: Long,
    val executionTime: ZonedDateTime,
    val resourcesUsed: ResourcesValue
) : CityDomainEvent<WarMakerBuildingEvent>(source, uuid, warMakerEventType, cityId) {
    override fun dto(): UpgradeBuildingEventDto {
        return UpgradeBuildingEventDto(uuid, warMakerEventType, ownerId, executionTime, resourcesUsed)
    }

    fun toScheduledEvent(publisher: ApplicationEventPublisher): UpgradeBuildingScheduledEvent {
        return UpgradeBuildingScheduledEvent(
            source,
            publisher,
            executionTime,
            ownerId,
            this,
            warMakerEventType,
            uuid,
            resourcesUsed
        )
    }
}
