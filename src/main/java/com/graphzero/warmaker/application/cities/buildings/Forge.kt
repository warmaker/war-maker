package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.events.WarMakerBuildingEvent
import com.graphzero.warmaker.application.resources.ResourcesValue

class Forge(stats: InfrastructureBaseStats, level: Int, upgradingLevel: Int) :
    CityInfrastructure(stats, level, upgradingLevel, BuildingName.FORGE) {
    override fun calculatePointValue(): Long {
        return level * stats.basePointValue
    }

    override fun calculateToughness(): Long {
        return level * stats.basePointValue
    }

    override fun nextLevelCost(): ResourcesValue {
        return ResourcesValue(level * 500 + 1500.0,
            level * 400 + 1000.0,
            level * 40 + 500.0,
            level * 50 + 100.0,
            0.0,
            0.0)
    }

    override val upgradeEvent: WarMakerBuildingEvent
        get() = WarMakerBuildingEvent.UPGRADE_FORGE

    override fun canUpgrade(buildings: Buildings): Boolean {
        return buildings.cityHall.level > 10 && buildings.university.level > 5
    }

    override fun instance(level: Int): Forge {
        return Forge(stats, level, upgradingLevel)
    }

    override fun enhance() {}
}
