package com.graphzero.warmaker.application.cities.region

import com.graphzero.warmaker.application.cities.CityFactory
import com.graphzero.warmaker.application.cities.CityRepository
import com.graphzero.warmaker.application.cities.exceptions.CityNotFoundException
import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.domain.CityEntity
import io.vavr.control.Either
import org.springframework.stereotype.Service

@Service
class CityRegionService(
    private val cityFactory: CityFactory,
    private val cityRepository: CityRepository
) {
    /**
     * Method updating current resources of given City. You shouldn't return City without actual resources
     *
     * @param regionId
     * @param warPartiesInField
     * @return
     */
    fun getCity(
        regionId: Long,
        warPartiesInField: List<WarParty>
    ): Either<CityNotFoundException, CityRegion> {
        return cityRepository.cityEntityRepository
            .findByLocationId(regionId)
            .map { entity: CityEntity ->
                val cityRegion = cityFactory.buildCityRegion(entity, warPartiesInField)
                cityRepository.persistResourcesUpdate(entity, cityRegion.resources)
                Either.right<CityNotFoundException, CityRegion>(cityRegion)
            }
            .orElseGet { Either.left(CityNotFoundException("City not found")) }
    }
}
