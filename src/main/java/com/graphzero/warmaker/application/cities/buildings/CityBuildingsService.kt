package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.cities.City
import com.graphzero.warmaker.application.cities.CityFactory
import com.graphzero.warmaker.application.cities.CityRepository
import com.graphzero.warmaker.application.cities.exceptions.CityException
import com.graphzero.warmaker.application.cities.exceptions.CityNotFoundException
import com.graphzero.warmaker.domain.CityEntity
import com.graphzero.warmaker.shared.annotations.ApplicationService
import io.vavr.control.Either
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import org.springframework.transaction.annotation.Transactional
import java.util.function.Supplier

@ApplicationService
@Transactional
internal class CityBuildingsService(
    private val eventPublisher: ApplicationEventPublisher,
    private val eventsService: CityBuildingsEventsService,
    private val cityRepository: CityRepository,
    private val buildingsRepository: BuildingsRepository,
    private val cityFactory: CityFactory,
    private val buildingsFactory: BuildingsFactory
) {
    /**
     * Schedules upgrade while taking into account already queued commands
     *
     * @param cityId
     * @return
     */
    fun scheduleGoldMineUpgrade(cityId: Long): Either<CityException, StartedBuildingUpgradeEvent> {
        return Either
            .narrow<CityException, CityBuildings>(getCityBuildings(cityId))
            .flatMap { city: CityBuildings ->
                Either.narrow(city
                    .queueBuildingUpgrade(city.buildings.goldMine)
                    .map { result: BuildingUpgrade -> finalizeBuildingUpgrade(city) })
            }
    }

    fun scheduleSawMillUpgrade(cityId: Long): Either<CityException, StartedBuildingUpgradeEvent> {
        return Either
            .narrow<CityException, CityBuildings>(getCityBuildings(cityId))
            .flatMap { city: CityBuildings ->
                Either.narrow(city
                    .queueBuildingUpgrade(city.buildings.sawMill)
                    .map { result: BuildingUpgrade -> finalizeBuildingUpgrade(city) })
            }
    }

    fun scheduleOreMineUpgrade(cityId: Long): Either<CityException, StartedBuildingUpgradeEvent> {
        return Either
            .narrow<CityException, CityBuildings>(getCityBuildings(cityId))
            .flatMap { city: CityBuildings ->
                Either.narrow(city
                    .queueBuildingUpgrade(city.buildings.oreMine)
                    .map { result: BuildingUpgrade -> finalizeBuildingUpgrade(city) })
            }
    }

    fun scheduleIronworksUpgrade(cityId: Long): Either<CityException, StartedBuildingUpgradeEvent> {
        return Either
            .narrow<CityException, CityBuildings>(getCityBuildings(cityId))
            .flatMap { city: CityBuildings ->
                Either.narrow(city
                    .queueBuildingUpgrade(city.buildings.ironworks)
                    .map { result: BuildingUpgrade -> finalizeBuildingUpgrade(city) })
            }
    }

    fun scheduleBarracksUpgrade(cityId: Long): Either<CityException, StartedBuildingUpgradeEvent> {
        return Either
            .narrow<CityException, CityBuildings>(getCityBuildings(cityId))
            .flatMap { city: CityBuildings ->
                Either.narrow(city
                    .queueBuildingUpgrade(buildingsFactory.barracks(cityId))
                    .map { result: BuildingUpgrade -> finalizeBuildingUpgrade(city) })
            }
    }

    fun scheduleCityHallUpgrade(cityId: Long): Either<CityException, StartedBuildingUpgradeEvent> {
        return Either
            .narrow<CityException, CityBuildings>(getCityBuildings(cityId))
            .flatMap { city: CityBuildings ->
                Either.narrow(city
                    .queueBuildingUpgrade(city.buildings.cityHall)
                    .map { result: BuildingUpgrade -> finalizeBuildingUpgrade(city) })
            }
    }

    fun scheduleUniversityUpgrade(cityId: Long): Either<CityException, StartedBuildingUpgradeEvent> {
        return Either
            .narrow<CityException, CityBuildings>(getCityBuildings(cityId))
            .flatMap { city: CityBuildings ->
                Either.narrow(city
                    .queueBuildingUpgrade(city.buildings.university)
                    .map { result: BuildingUpgrade -> finalizeBuildingUpgrade(city) })
            }
    }

    fun scheduleForgeUpgrade(cityId: Long): Either<CityException, StartedBuildingUpgradeEvent> {
        return Either
            .narrow<CityException, CityBuildings>(getCityBuildings(cityId))
            .flatMap { city: CityBuildings ->
                Either.narrow(city
                    .queueBuildingUpgrade(city.buildings.forge)
                    .map { result: BuildingUpgrade -> finalizeBuildingUpgrade(city) })
            }
    }

    private fun finalizeBuildingUpgrade(city: CityBuildings): StartedBuildingUpgradeEvent {
        cityRepository.update(city)
        val latestBuildingUpgradeEvent = city.cityScheduledEvents.getLatestBuildingUpgradeEvent(
            eventPublisher
        )
        eventsService.scheduledEventsService.schedule(latestBuildingUpgradeEvent)
        return latestBuildingUpgradeEvent.toDomainEvent()
    }

    fun upgradeSawMill(cityId: Long): Either<CityNotFoundException, SawMill> {
        return getCityBuildings(cityId)
            .flatMap { city: CityBuildings ->
                val sawMill = city.upgradeResourceBuildingWithoutCheck(city.buildings.sawMill)
                persistBuildingUpdate(city, sawMill)
            }
    }

    fun upgradeOreMine(cityId: Long): Either<CityNotFoundException, OreMine> {
        return getCityBuildings(cityId)
            .flatMap { city: CityBuildings ->
                val oreMine = city.upgradeResourceBuildingWithoutCheck(city.buildings.oreMine)
                persistBuildingUpdate(city, oreMine)
            }
    }

    fun upgradeGoldMine(cityId: Long): Either<CityNotFoundException, GoldMine> {
        return getCityBuildings(cityId)
            .flatMap { city: CityBuildings ->
                val goldMine = city.upgradeResourceBuildingWithoutCheck(city.buildings.goldMine)
                persistBuildingUpdate(city, goldMine)
            }
    }

    fun upgradeIronworks(cityId: Long): Either<CityNotFoundException, IronWorks> {
        return getCityBuildings(cityId)
            .flatMap { city: CityBuildings ->
                val ironWorks = city.upgradeResourceBuildingWithoutCheck(city.buildings.ironworks)
                persistBuildingUpdate(city, ironWorks)
            }
    }

    fun upgradeBarracks(cityId: Long): Either<CityNotFoundException, Barracks> {
        return getCityBuildings(cityId)
            .flatMap { city: CityBuildings ->
                val barracks = city.upgradeBuildingWithoutCheck(
                    buildingsFactory.barracks(cityId)
                )
                persistBarracksUpdate(city, barracks)
            }
    }

    private fun persistBarracksUpdate(city: City, barracks: Barracks): Either<CityNotFoundException, out Barracks> {
        buildingsRepository.persistBarracksUpdate(city.buildings.id, barracks)
        log.debug("Upgraded [{}] for City [{}]", barracks.name.buildingName, city.id)
        return Either.right(barracks)
    }

    fun upgradeCityHall(cityId: Long): Either<CityNotFoundException, CityHall> {
        return getCityBuildings(cityId)
            .flatMap { city: CityBuildings ->
                val cityHall = city.upgradeBuildingWithoutCheck(city.buildings.cityHall)
                persistBuildingUpdate(city, cityHall)
            }
    }

    fun upgradeUniversity(cityId: Long): Either<CityNotFoundException, University> {
        return getCityBuildings(cityId)
            .flatMap { city: CityBuildings ->
                val university = city.upgradeBuildingWithoutCheck(city.buildings.university)
                persistBuildingUpdate(city, university)
            }
    }

    fun upgradeForge(cityId: Long): Either<CityNotFoundException, Forge> {
        return getCityBuildings(cityId)
            .flatMap { city: CityBuildings ->
                val forge = city.upgradeBuildingWithoutCheck(city.buildings.forge)
                persistBuildingUpdate(city, forge)
            }
    }

    /**
     * Method updating current resources of given City. You shouldn't return City without actual resources
     *
     * @param cityId
     * @return
     */
    fun getCityBuildings(cityId: Long): Either<CityNotFoundException, CityBuildings> {
        return cityRepository
            .findById(cityId)
            .map { cityEntity: CityEntity -> createCityBuildings(cityEntity) }
            .orElseGet { Either.left(CityNotFoundException("City not found")) }
    }

    private fun createCityBuildings(cityEntity: CityEntity): Either<CityNotFoundException, CityBuildings> {
        val cityBuildings = cityFactory.buildCityBuildings(cityEntity)
        cityRepository.persistResourcesUpdate(cityEntity, cityBuildings.resources)
        return Either.right(cityBuildings)
    }

    private fun <T : CityInfrastructure> persistBuildingUpdate(
        city: City,
        building: T
    ): Either<CityNotFoundException, T> {
        buildingsRepository.persistCityBuildingsUpdate(city.buildings.id, city.buildings)
        log.debug("Upgraded [{}] for City [{}]", building.name.buildingName, city.id)
        return Either.right(building)
    }

    companion object {
        private val log = LoggerFactory.getLogger(CityBuildingsService::class.java)
    }
}
