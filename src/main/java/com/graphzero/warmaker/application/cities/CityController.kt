package com.graphzero.warmaker.application.cities

import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/city")
internal class CityController(private val cityService: CityService) {
    private val log = LoggerFactory.getLogger(CityController::class.java)

    @GetMapping("/{id}")
    fun getCityDto(@PathVariable(value = "id") cityId: Long): ResponseEntity<CityDto> {
        log.debug("REST request to return city [{}]", cityId)
        val city = cityService.getCityDto(cityId).get()
        return ResponseEntity.ok(city)
    }

}
