package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.cities.buildings.Buildings.BuildingsUpgradeRequirementMatched
import com.graphzero.warmaker.application.resources.ResourcesValue

data class BuildingsDto(val goldMine: BuildingDto<GoldMine>,
                        val sawMill: BuildingDto<SawMill>,
                        val oreMine: BuildingDto<OreMine>,
                        val ironworks: BuildingDto<IronWorks>,
                        val barracks: BuildingDto<Barracks>,
                        val cityHall: BuildingDto<CityHall>,
                        val university: BuildingDto<University>,
                        val forge: BuildingDto<Forge>,
                        val buildingsUpgradeRequirementMatched: BuildingsUpgradeRequirementMatched?) {

    data class BuildingDto<T : CityInfrastructure>(
        val building: T,
        val upgradeCost: ResourcesValue,
        val name: String,
        val nextLevelBuildTime: Long,
        val upgradingLevel: Int = building.upgradingLevel
    )

}
