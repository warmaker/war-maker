package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEventDto
import com.graphzero.warmaker.application.resources.ResourcesValue
import java.time.ZonedDateTime
import java.util.*

class UpgradeBuildingEventDto(
    uuid: UUID,
    type: WarMakerEventType,
    val ownerId: Long,
    val executionTime: ZonedDateTime,
    val resourcesUsed: ResourcesValue?
) : DomainEventDto(uuid, type) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UpgradeBuildingEventDto

        if (ownerId != other.ownerId) return false
        if (executionTime != other.executionTime) return false
        if (resourcesUsed != other.resourcesUsed) return false

        return true
    }

    override fun hashCode(): Int {
        var result = ownerId.hashCode()
        result = 31 * result + executionTime.hashCode()
        result = 31 * result + (resourcesUsed?.hashCode() ?: 0)
        return result
    }
}
