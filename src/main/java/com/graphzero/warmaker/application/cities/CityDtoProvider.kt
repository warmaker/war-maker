package com.graphzero.warmaker.application.cities

interface CityDtoProvider {
    fun fillDto(dto: CityDto): CityDto
}
