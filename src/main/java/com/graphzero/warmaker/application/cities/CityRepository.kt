package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.domain.CityEntity
import com.graphzero.warmaker.repository.CityEntityRepository
import com.graphzero.warmaker.repository.ResourcesEntityRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class CityRepository(
    private val resourcesRepository: ResourcesEntityRepository,
    val cityEntityRepository: CityEntityRepository
) {
    fun persistResourcesUpdate(cityEntity: CityEntity, newResources: CityResources) {
        val resourcesEntity = cityEntity.storedResources
        val storedResources = newResources.storedResources
        resourcesEntity
            .denars(storedResources.denars)
            .wood(storedResources.wood)
            .stone(storedResources.stone)
            .steel(storedResources.steel)
            .food(storedResources.food)
            .faith(storedResources.faith)
            .changeDate(newResources.changeDate)
        resourcesRepository.save(resourcesEntity)
        cityEntityRepository.save(cityEntity)
    }

    fun update(city: City) {
        val cityEntity = cityEntityRepository.getOne(city.id)
        persistResourcesUpdate(cityEntity, city.resources)
        cityEntityRepository.save(cityEntity)
    }

    fun findById(id: Long): Optional<CityEntity> {
        return cityEntityRepository.findById(id)
    }
}
