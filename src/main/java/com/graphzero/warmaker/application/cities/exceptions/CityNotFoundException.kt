package com.graphzero.warmaker.application.cities.exceptions

class CityNotFoundException : CityException {
    constructor() : super(MESSAGE)

    constructor(customMessage: String) : super(customMessage)

    companion object {
        private const val MESSAGE = "City with given id couldn't be found."
    }
}
