package com.graphzero.warmaker.application.cities.region

import com.graphzero.warmaker.application.cities.City
import com.graphzero.warmaker.application.cities.CityDtoProvider
import com.graphzero.warmaker.application.cities.CityDto
import com.graphzero.warmaker.application.cities.CityInfo
import com.graphzero.warmaker.application.cities.buildings.*
import com.graphzero.warmaker.application.player.Player
import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.application.warparty.journey.WarPartyCommand
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneyConclusion
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneySaga
import com.graphzero.warmaker.application.world.*
import com.graphzero.warmaker.domain.enumeration.FieldType
import org.slf4j.LoggerFactory
import java.time.ZonedDateTime
import java.util.*

/**
 * Aggregate root modeling City domain object
 */
class CityRegion(
    coordinates: Coordinates,
    id: Long,
    info: CityInfo,
    buildings: Buildings,
    resource: Resources,
    resourcesChangeDate: ZonedDateTime,
    warPartiesInField: List<WarParty>
) : City(id = id, resource = resource, resourcesChangeDate = resourcesChangeDate, info, buildings = buildings), Region, CityDtoProvider {
    private val region: RegionFields = RegionFields(id, coordinates, 1.0, FieldType.CITY, warPartiesInField)
    val moveModifier: Double
        get() = region.moveModifier

    override fun isEnemyOf(player: Player): Boolean {
        return player.isEnemy(info.ownerId)
    }

    override val ownerId: Optional<Long>
        get() = Optional.of(info.ownerId)

    override fun handlePassThrough(warParty: WarParty): WarPartyJourneyConclusion {
        return WarPartyJourneyConclusion.MOVING_TO_NEXT_POSITION
    }

    override fun handleReachingDestination(warParty: WarParty, saga: WarPartyJourneySaga): WarPartyJourneyConclusion {
        return when (saga.command) {
            WarPartyCommand.GARRISON -> {
                log.debug("War Party [{}] moves to the location [{}] and waits there", warParty.id, id)
                resources.addLoot(warParty.loot())
                WarPartyJourneyConclusion.GARRISONED
            }
            else -> {
                log.debug("Unsupported command [{}] for War Party [{}] ", saga.command, warParty.id)
                WarPartyJourneyConclusion.RETURNING
            }
        }
    }

    override val coordinates: Coordinates?
        get() = region.coordinates

    override fun fillDto(dto: CityDto): CityDto {
        dto.coordinates = region.coordinates
        dto.fieldType = region.fieldType
        dto.moveModifier = 1.0
        return dto
    }

    companion object {
        private val log = LoggerFactory.getLogger(CityRegion::class.java)
    }

}
