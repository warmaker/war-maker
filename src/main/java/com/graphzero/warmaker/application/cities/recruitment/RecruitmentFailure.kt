package com.graphzero.warmaker.application.cities.recruitment

import com.graphzero.warmaker.application.cities.exceptions.CityException

class RecruitmentFailure private constructor(customMessage: String) : CityException(customMessage) {
    companion object {
        private const val NOT_ENOUGH_RESOURCES_MESSAGE = "There are not enough resources to recruits those units"
        private const val EMPTY_RECRUITMENT_ORDER = "Recruitment order cant be empty"

        fun notEnoughResources(): RecruitmentFailure {
            return RecruitmentFailure(NOT_ENOUGH_RESOURCES_MESSAGE)
        }

        fun emptyRecruitmentOrder(): RecruitmentFailure {
            return RecruitmentFailure(EMPTY_RECRUITMENT_ORDER)
        }
    }
}
