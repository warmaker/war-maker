package com.graphzero.warmaker.application.cities.recruitment

import com.graphzero.warmaker.application.army.StartedUnitsRecruitmentEvent
import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList

class RecruitmentResult(
    val recruitmentCost: ResourcesValue,
    val unitsChange: HumanUnitsList,
    val event: StartedUnitsRecruitmentEvent
)
