package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.cities.exceptions.CityException
import io.vavr.control.Either
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/city/buildings")
internal class CityBuildingsController(private val cityBuildingsService: CityBuildingsService) {
    private val log = LoggerFactory.getLogger(CityBuildingsController::class.java)
    @PostMapping("/{id}/saw-mill/up")
    fun upgradeSawMill(@PathVariable(value = "id") cityId: Long): ResponseEntity<*> {
        log.debug("REST Request to schedule SawMill upgrade for City [{}]", cityId)
        val upgradeSawMill: Either<out CityException, StartedBuildingUpgradeEvent> =
            cityBuildingsService.scheduleSawMillUpgrade(cityId)
        return if (upgradeSawMill.isRight) {
            ResponseEntity.ok(upgradeSawMill.get()!!.dto())
        } else {
            ResponseEntity.badRequest().body("error.cities.notEnoughResourcesSawMill")
        }
    }

    @PostMapping("/{id}/ore-mine/up")
    fun upgradeOreMine(@PathVariable(value = "id") cityId: Long): ResponseEntity<*> {
        log.debug("REST Request to schedule OreMine upgrade for City [{}]", cityId)
        val upgradeOreMine: Either<out CityException, StartedBuildingUpgradeEvent> =
            cityBuildingsService.scheduleOreMineUpgrade(cityId)
        return if (upgradeOreMine.isRight) {
            ResponseEntity.ok(upgradeOreMine.get()!!.dto())
        } else {
            ResponseEntity.badRequest().body("error.cities.notEnoughResourcesSawMill")
        }
    }

    @PostMapping("/{id}/gold-mine/up")
    fun upgradeGoldMine(@PathVariable(value = "id") cityId: Long): ResponseEntity<*> {
        log.debug("REST Request to schedule GoldMine upgrade for City [{}]", cityId)
        val upgradeGoldMine: Either<out CityException, StartedBuildingUpgradeEvent> =
            cityBuildingsService.scheduleGoldMineUpgrade(cityId)
        return if (upgradeGoldMine.isRight) {
            ResponseEntity.ok(upgradeGoldMine.get()!!.dto())
        } else {
            ResponseEntity.badRequest().body("error.cities.notEnoughResourcesSawMill")
        }
    }

    @PostMapping("/{id}/ironworks/up")
    fun upgradeIronworks(@PathVariable(value = "id") cityId: Long): ResponseEntity<*> {
        log.debug("REST Request to schedule Ironworks upgrade for City [{}]", cityId)
        val upgrade: Either<out CityException, StartedBuildingUpgradeEvent> =
            cityBuildingsService.scheduleIronworksUpgrade(cityId)
        return if (upgrade.isRight) {
            ResponseEntity.ok(upgrade.get()!!.dto())
        } else {
            ResponseEntity.badRequest().body("error.cities.notEnoughResourcesSawMill")
        }
    }

    @PostMapping("/{id}/barracks/up")
    fun upgradeBarracks(@PathVariable(value = "id") cityId: Long): ResponseEntity<*> {
        log.debug("REST Request to schedule Barracks upgrade for City [{}]", cityId)
        val upgrade: Either<out CityException, StartedBuildingUpgradeEvent> =
            cityBuildingsService.scheduleBarracksUpgrade(cityId)
        return if (upgrade.isRight) {
            ResponseEntity.ok(upgrade.get()!!.dto())
        } else {
            ResponseEntity.badRequest().body("error.cities.notEnoughResourcesSawMill")
        }
    }

    @PostMapping("/{id}/city-hall/up")
    fun upgradeTownHall(@PathVariable(value = "id") cityId: Long): ResponseEntity<*> {
        log.debug("REST Request to schedule City Hall upgrade for City [{}]", cityId)
        val upgrade: Either<out CityException, StartedBuildingUpgradeEvent> =
            cityBuildingsService.scheduleCityHallUpgrade(cityId)
        return if (upgrade.isRight) {
            ResponseEntity.ok(upgrade.get()!!.dto())
        } else {
            ResponseEntity.badRequest().body("error.cities.notEnoughResourcesSawMill")
        }
    }

    @PostMapping("/{id}/university/up")
    fun upgradeUniversity(@PathVariable(value = "id") cityId: Long): ResponseEntity<*> {
        log.debug("REST Request to schedule University upgrade for City [{}]", cityId)
        val upgrade: Either<out CityException, StartedBuildingUpgradeEvent> =
            cityBuildingsService.scheduleUniversityUpgrade(cityId)
        return if (upgrade.isRight) {
            ResponseEntity.ok(upgrade.get()!!.dto())
        } else {
            ResponseEntity.badRequest().body("error.cities.notEnoughResourcesSawMill")
        }
    }

    @PostMapping("/{id}/forge/up")
    fun upgradeForge(@PathVariable(value = "id") cityId: Long): ResponseEntity<*> {
        log.debug("REST Request to schedule Forge upgrade for City [{}]", cityId)
        val upgrade: Either<out CityException, StartedBuildingUpgradeEvent> =
            cityBuildingsService.scheduleForgeUpgrade(cityId)
        return if (upgrade.isRight) {
            ResponseEntity.ok(upgrade.get()!!.dto())
        } else {
            ResponseEntity.badRequest().body("error.cities.notEnoughResourcesSawMill")
        }
    }
}
