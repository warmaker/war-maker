package com.graphzero.warmaker.application.cities.recruitment

import com.graphzero.warmaker.application.cities.CityFactory
import com.graphzero.warmaker.application.cities.CityRepository
import com.graphzero.warmaker.application.cities.exceptions.CityException
import com.graphzero.warmaker.application.cities.exceptions.CityNotFoundException
import com.graphzero.warmaker.application.events.domain.DomainEventsPublisher
import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import com.graphzero.warmaker.domain.CityEntity
import io.vavr.control.Either
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
internal class CityRecruitmentService(
    private val cityFactory: CityFactory,
    private val cityRepository: CityRepository,
    private val eventsPublisher: DomainEventsPublisher
) {
    @Transactional
    fun startUnitsRecruitment(cityId: Long, units: HumanUnitsList): Either<CityException, ResourcesValue> {
        return Either
            .narrow<CityException, CityRecruitment>(getCityRecruitment(cityId))
            .flatMap { city: CityRecruitment ->
                Either.narrow(city.recruitUnits(units)
                    .map { result: ResourcesValue ->
                        cityRepository.update(city)
                        eventsPublisher.publish(city.getDomainEvents())
                        result
                    })
            }
    }

    /**
     * Method updating current resources of given City. You shouldn't return City without actual resources
     *
     * @param cityId
     * @return
     */
    private fun getCityRecruitment(cityId: Long): Either<CityNotFoundException, CityRecruitment> {
        return cityRepository
            .findById(cityId)
            .map { cityEntity: CityEntity ->
                val cityRecruitment = cityFactory.buildCityRecruitment(cityEntity)
                cityRepository.persistResourcesUpdate(cityEntity, cityRecruitment.resources)
                Either.right<CityNotFoundException, CityRecruitment>(cityRecruitment)
            }
            .orElseGet { Either.left(CityNotFoundException("City not found")) }
    }
}
