package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.events.WarMakerBuildingEvent
import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.domain.enumeration.ResourceType
import java.util.*

class GoldMine(stats: InfrastructureBaseStats, level: Int, upgradingLevel: Int) :
    CityInfrastructure(stats, level, upgradingLevel, BuildingName.GOLD_MINE), ResourceInfrastructure {
    override fun nextLevelCost(): ResourcesValue {
        return ResourcesValue(level * 300 + 1500.0,
            level * 300 + 1000.0,
            level * 40 + 500.0,
            level * 10 + 100.0,
            0.0,
            0.0)
    }

    override fun calculatePointValue(): Long {
        return level * stats.basePointValue
    }

    override fun calculateToughness(): Long {
        return level * stats.basePointValue
    }

    override val upgradeEvent: WarMakerBuildingEvent
        get() = WarMakerBuildingEvent.UPGRADE_GOLD_MINE

    override fun canUpgrade(buildings: Buildings): Boolean {
        return true
    }

    override fun instance(level: Int): GoldMine {
        return GoldMine(stats, level, upgradingLevel)
    }

    override fun enhance() {}
    override fun incomeModifier(): Map<ResourceType, Double> {
        return Collections.singletonMap(ResourceType.DENARS, level * 0.3)
    }
}
