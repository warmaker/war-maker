package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.application.army.StartedUnitsRecruitmentEvent
import com.graphzero.warmaker.application.army.StartedUnitsRecruitmentEvent.StartedUnitsRecruitmentEventDto
import com.graphzero.warmaker.application.cities.buildings.StartedBuildingUpgradeEvent
import com.graphzero.warmaker.application.cities.buildings.UpgradeBuildingEventDto
import com.graphzero.warmaker.application.cities.buildings.UpgradeBuildingScheduledEvent
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.time.TimeConfiguration
import org.springframework.context.ApplicationEventPublisher
import java.time.ZonedDateTime
import java.util.*
import java.util.stream.Collectors

class CityScheduledEvents {
    private val recruitmentOrders: List<StartedUnitsRecruitmentEvent>
    val currentStartedBuildingUpgradeEvents: MutableList<StartedBuildingUpgradeEvent>

    constructor() {
        recruitmentOrders = ArrayList()
        currentStartedBuildingUpgradeEvents = ArrayList()
    }

    constructor(
        recruitmentOrders: List<StartedUnitsRecruitmentEvent>,
        currentStartedBuildingUpgradeEvents: MutableList<StartedBuildingUpgradeEvent>
    ) {
        this.recruitmentOrders = recruitmentOrders
        this.currentStartedBuildingUpgradeEvents = currentStartedBuildingUpgradeEvents
    }

    fun addBuildingUpgradeEvent(event: StartedBuildingUpgradeEvent) {
        currentStartedBuildingUpgradeEvents.add(event)
    }

    val queuedBuildingsUpgrades: Map<WarMakerEventType, Long>
        get() = currentStartedBuildingUpgradeEvents
            .stream()
            .collect(
                Collectors.groupingBy(
                    { obj: StartedBuildingUpgradeEvent -> obj.warMakerEventType },
                    Collectors.counting()
                )
            )
    val lastBuildingFinishTime: ZonedDateTime
        get() = if (currentStartedBuildingUpgradeEvents.isEmpty()) ZonedDateTime.now(TimeConfiguration.serverClock) else currentStartedBuildingUpgradeEvents.stream()
            .sorted(Comparator.comparing { obj: StartedBuildingUpgradeEvent -> obj.executionTime })
            .skip((currentStartedBuildingUpgradeEvents.size - 1).toLong())
            .findFirst()
            .map { obj: StartedBuildingUpgradeEvent -> obj.executionTime }
            .orElse(ZonedDateTime.now(TimeConfiguration.serverClock))

    fun calculateRecruitmentFinishTime(event: StartedUnitsRecruitmentEvent): ZonedDateTime {
        return recruitmentOrders.stream()
            .max(Comparator.comparing { obj: StartedUnitsRecruitmentEvent -> obj.finishTime })
            .map { latestEvent: StartedUnitsRecruitmentEvent ->
                latestEvent.finishTime
                    .plus(event.recruitmentOrders.totalRecruitmentTime, event.recruitmentOrders.timeUnit)
            }
            .orElse(
                ZonedDateTime.now(TimeConfiguration.serverClock)
                    .plus(event.recruitmentOrders.totalRecruitmentTime, event.recruitmentOrders.timeUnit)
            )
    }

    fun getLatestBuildingUpgradeEvent(publisher: ApplicationEventPublisher): UpgradeBuildingScheduledEvent {
        return currentStartedBuildingUpgradeEvents[currentStartedBuildingUpgradeEvents.size - 1].toScheduledEvent(publisher)
    }

    fun getRecruitmentOrders(): List<StartedUnitsRecruitmentEventDto> {
        return recruitmentOrders.stream().map { obj: StartedUnitsRecruitmentEvent -> obj.dto() }
            .collect(Collectors.toList())
    }

    val startedBuildingUpgradeEventsDto: List<UpgradeBuildingEventDto>
        get() = currentStartedBuildingUpgradeEvents
            .stream()
            .map { obj: StartedBuildingUpgradeEvent -> obj.dto() }
            .collect(Collectors.toList())

    fun areUnitsRecruited(): Boolean {
        return recruitmentOrders.isNotEmpty()
    }
}
