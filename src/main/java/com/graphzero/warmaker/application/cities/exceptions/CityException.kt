package com.graphzero.warmaker.application.cities.exceptions

open class CityException(s: String) : Exception(s)
