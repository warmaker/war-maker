package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.cities.CityEvent
import com.graphzero.warmaker.application.events.WarMakerBuildingEvent
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.schedulable.AbstractSchedulableEvent
import com.graphzero.warmaker.application.events.schedulable.AbstractSchedulableEventDto
import com.graphzero.warmaker.application.resources.ResourcesValue
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import java.time.ZonedDateTime
import java.util.*

class UpgradeBuildingScheduledEvent(
    source: Any,
    @field:Transient private val publisher: ApplicationEventPublisher,
    executionTime: ZonedDateTime,
    ownerId: Long,
    val upgradeEvent: StartedBuildingUpgradeEvent,
    warMakerEventType: WarMakerBuildingEvent,
    uuid: UUID,
    val resourcesUsed: ResourcesValue
) : AbstractSchedulableEvent<WarMakerBuildingEvent>(source, ownerId, warMakerEventType, executionTime, uuid),
    CityEvent {

    fun toDomainEvent(): StartedBuildingUpgradeEvent {
        return StartedBuildingUpgradeEvent(
            source,
            uuid,
            warMakerEventType,
            cityId,
            ownerId,
            executionTime,
            resourcesUsed
        )
    }

    override fun execute() {
        log.debug("Publishing event: [{}] for user [{}]", uuid, ownerId)
        publisher.publishEvent(this)
    }

    override fun dto(): UpgradeCityInfrastructureCommandDto {
        return UpgradeCityInfrastructureCommandDto(ownerId, warMakerEventType)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UpgradeBuildingScheduledEvent

        if (publisher != other.publisher) return false
        if (upgradeEvent != other.upgradeEvent) return false
        if (resourcesUsed != other.resourcesUsed) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + publisher.hashCode()
        result = 31 * result + upgradeEvent.hashCode()
        result = 31 * result + resourcesUsed.hashCode()
        return result
    }


    override val cityId: Long
        get() = upgradeEvent.cityId

    class UpgradeCityInfrastructureCommandDto(ownerId: Long, type: WarMakerEventType) :
        AbstractSchedulableEventDto(ownerId, type)

    companion object {
        private val log = LoggerFactory.getLogger(UpgradeBuildingScheduledEvent::class.java)
    }
}
