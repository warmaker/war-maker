package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.domain.enumeration.CityType

class CityInfo(
    val ownerId: Long,
    val armyId: Long,
    val cityName: String,
    val cityType: CityType,
    val recruitmentModifier: Double,
    val buildSpeedModifier: Double?
) {
    override fun toString(): String {
        return "CityInfo{" +
                "ownerId=" + ownerId +
                ", cityName='" + cityName + '\'' +
                ", cityType=" + cityType +
                '}'
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CityInfo

        if (ownerId != other.ownerId) return false
        if (armyId != other.armyId) return false
        if (cityName != other.cityName) return false
        if (cityType != other.cityType) return false
        if (recruitmentModifier != other.recruitmentModifier) return false
        if (buildSpeedModifier != other.buildSpeedModifier) return false

        return true
    }

    override fun hashCode(): Int {
        var result = ownerId.hashCode()
        result = 31 * result + armyId.hashCode()
        result = 31 * result + cityName.hashCode()
        result = 31 * result + cityType.hashCode()
        result = 31 * result + recruitmentModifier.hashCode()
        result = 31 * result + (buildSpeedModifier?.hashCode() ?: 0)
        return result
    }
}
