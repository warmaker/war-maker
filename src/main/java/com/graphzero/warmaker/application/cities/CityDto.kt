package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.application.army.StartedUnitsRecruitmentEvent.StartedUnitsRecruitmentEventDto
import com.graphzero.warmaker.application.cities.buildings.BuildingsDto
import com.graphzero.warmaker.application.cities.buildings.UpgradeBuildingEventDto
import com.graphzero.warmaker.application.world.Coordinates
import com.graphzero.warmaker.domain.ResearchesEntity
import com.graphzero.warmaker.domain.enumeration.FieldType

data class CityDto(
    val id: Long = 0,
    val info: CityInfo? = null,
    val resources: CityResources? = null,
    var coordinates: Coordinates? = null,
    var fieldType: FieldType? = null,
    var moveModifier: Double = 0.0,
    var recruitmentOrders: List<StartedUnitsRecruitmentEventDto> = ArrayList(),
    var buildings: BuildingsDto? = null,
    var currentUpgradeBuildingEvents: List<UpgradeBuildingEventDto> = ArrayList(),
    var researches: ResearchesEntity? = null
)
