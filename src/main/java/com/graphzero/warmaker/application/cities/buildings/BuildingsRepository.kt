package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.repository.CityBuildingsEntityRepository
import org.springframework.stereotype.Repository

@Repository
internal class BuildingsRepository(private val cityBuildingsRepository: CityBuildingsEntityRepository) {
    fun persistCityBuildingsUpdate(id: Long, newBuildings: Buildings) {
        val buildingsEntity = cityBuildingsRepository.findById(id).get()
        buildingsEntity
            .goldMine(newBuildings.goldMine.level)
            .sawMill(newBuildings.sawMill.level)
            .oreMine(newBuildings.oreMine.level)
            .ironworks(newBuildings.ironworks.level)
            .cityHall(newBuildings.cityHall.level)
            .forge(newBuildings.forge.level)
            .university(newBuildings.university.level)
        cityBuildingsRepository.save(buildingsEntity)
    }

    fun persistBarracksUpdate(id: Long, barracks: Barracks) {
        val buildingsEntity = cityBuildingsRepository.findById(id).get()
        buildingsEntity
            .barracks(barracks.level)
        cityBuildingsRepository.save(buildingsEntity)
    }
}
