package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.domain.WarPartyEntity
import com.graphzero.warmaker.repository.CityEntityRepository
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/city")
internal class CityCrudController(private val cityRepository: CityEntityRepository) {
    @GetMapping("/{id}/war-parties")
    fun getWarParties(@PathVariable(value = "id") cityId: Long): ResponseEntity<List<WarPartyEntity>> {
        return ResponseEntity.ok(cityRepository.getWarParties(cityId))
    }
}
