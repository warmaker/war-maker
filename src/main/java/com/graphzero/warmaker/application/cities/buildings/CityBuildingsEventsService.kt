package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.events.WarMakerBuildingEvent
import com.graphzero.warmaker.application.events.schedulable.AbstractSchedulableEvent
import com.graphzero.warmaker.application.events.schedulable.ScheduledEventsService
import com.graphzero.warmaker.shared.annotations.ApplicationService
import java.util.stream.Collectors
import java.util.stream.Stream

@ApplicationService
class CityBuildingsEventsService(val scheduledEventsService: ScheduledEventsService) {
    fun listAllCityUpgradeBuildingEvents(cityId: Long, ownerId: Long): MutableList<UpgradeBuildingScheduledEvent> {
        return scheduledEventsService
            .findEventsForUser(ownerId)
            .stream()
            .filter { event: AbstractSchedulableEvent<*> ->
                Stream.of(*WarMakerBuildingEvent.values())
                    .anyMatch { e: WarMakerBuildingEvent -> e == event.warMakerEventType }
            }
            .filter { event: AbstractSchedulableEvent<*> ->
                val cm = event as UpgradeBuildingScheduledEvent
                cm.cityId == cityId
            }
            .map { e: AbstractSchedulableEvent<*> -> e as UpgradeBuildingScheduledEvent? }
            .collect(Collectors.toList())
    }
}
