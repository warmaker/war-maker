package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.cities.CityScheduledEvents
import com.graphzero.warmaker.application.events.WarMakerBuildingEvent
import com.graphzero.warmaker.application.resources.ResourcesValue
import java.time.temporal.ChronoUnit
import java.util.*

class CityHall(
    stats: InfrastructureBaseStats,
    level: Int,
    upgradingLevel: Int,
    private val cityId: Long,
    private val ownerId: Long,
    private val cityScheduledEvents: CityScheduledEvents
) : CityInfrastructure(stats, level, upgradingLevel, BuildingName.CITY_HALL) {

    fun <T : CityInfrastructure> scheduleBuildingUpgrade(
        building: T,
        totalBuildCost: ResourcesValue
    ): StartedBuildingUpgradeEvent {
        building.increaseLevel()
        val buildTime = calculateConstructionTime(building.futureLevel, building.stats.baseToughness.toInt())
        val totalExecutionTime = cityScheduledEvents.lastBuildingFinishTime.plus(buildTime, ChronoUnit.SECONDS)
        return StartedBuildingUpgradeEvent(
            this,
            UUID.randomUUID(),
            building.upgradeEvent,
            cityId,
            ownerId,
            totalExecutionTime,
            totalBuildCost
        )
    }

    fun calculateConstructionTime(buildingLevel: Int, buildingToughness: Int): Long {
        return (buildingLevel + 1) * buildingToughness - 2L * level
    }

    override fun nextLevelCost(): ResourcesValue {
        return ResourcesValue(level * 300 + 750.0, level * 300 + 250.0, level * 40 + 100.0, 0.0, 0.0, 0.0)
    }

    override fun calculatePointValue(): Long {
        return level * stats.basePointValue
    }

    override fun calculateToughness(): Long {
        return level * stats.basePointValue
    }

    override val upgradeEvent: WarMakerBuildingEvent
        get() = WarMakerBuildingEvent.UPGRADE_CITY_HALL

    override fun canUpgrade(buildings: Buildings): Boolean {
        return true
    }

    override fun instance(level: Int): CityHall {
        return CityHall(stats, level, upgradingLevel, cityId, ownerId, cityScheduledEvents)
    }

    override fun enhance() {}
}
