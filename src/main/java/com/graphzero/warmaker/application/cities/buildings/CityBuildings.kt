package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.cities.*
import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.domain.enumeration.ResourceType
import io.vavr.control.Either
import java.time.ZonedDateTime
import java.util.function.Consumer

/**
 * Aggregate root modeling City domain object
 */
class CityBuildings(
    id: Long,
    info: CityInfo,
    buildings: Buildings,
    resource: Resources,
    resourcesChangeDate: ZonedDateTime,
    val cityScheduledEvents: CityScheduledEvents
) : CityDtoProvider, City(id = id, resource = resource, resourcesChangeDate = resourcesChangeDate, info, buildings = buildings) {

    /**
     * Updates Building to target level with taking into consideration possible cost reduction or increase by e.g
     * researches
     */
    fun <T : CityInfrastructure> queueBuildingUpgrade(building: T): Either<BuildingUpgradeProhibited, BuildingUpgrade> {
        val totalBuildCost = discountedBuildCostForBuilding(building)
        return if (resources.hasEnough(totalBuildCost)) {
            resources.subtract(totalBuildCost)
            buildings
                .scheduleBuildingUpgrade(building, totalBuildCost)
                .map { event: StartedBuildingUpgradeEvent ->
                    cityScheduledEvents.addBuildingUpgradeEvent(event)
                    BuildingUpgrade(totalBuildCost, event.executionTime)
                }
        } else {
            Either.left(BuildingUpgradeProhibited(BuildingUpgradeProhibited.Reason.NOT_ENOUGH_RESOURCES))
        }
    }

    /**
     * Updates Resource Building without checking if resources are available. Used after building has finished building
     */
    fun <T> upgradeResourceBuildingWithoutCheck(building: T): T where T : CityInfrastructure, T : ResourceInfrastructure {
        building.increaseLevel()
        building.incomeModifier().entries.forEach(Consumer { resource: Map.Entry<ResourceType, Double> ->
            resources.raiseIncome(
                resource
            )
        })
        return building
    }

    /**
     * Updates Building without checking if resources are available. Used after building has finished building
     */
    fun <T : CityInfrastructure> upgradeBuildingWithoutCheck(building: T): T {
        building.increaseLevel()
        return building
    }

    fun discountedBuildCostForBuilding(building: CityInfrastructure): ResourcesValue {
        val baseBuildCost = building.resourceCostForLevel(building.futureLevel + 1)
        return getDiscount(baseBuildCost)
    }

    private fun getDiscount(baseBuildCost: ResourcesValue): ResourcesValue {
        return ResourcesValue(
            if (baseBuildCost.denars - 500 >= 0) baseBuildCost.denars - 500.0 else 0.0,
            if (baseBuildCost.wood - 250 >= 0) baseBuildCost.wood - 250.0 else 0.0,
            if (baseBuildCost.stone - 100 >= 0) baseBuildCost.stone - 100.0 else 0.0,
            if (baseBuildCost.steel - 1 >= 0) baseBuildCost.steel - 1.0 else 0.0,
            if (baseBuildCost.food - 1 >= 0) baseBuildCost.food - 1.0 else 0.0,
            if (baseBuildCost.faith - 1 >= 0) baseBuildCost.faith - 1.0 else 0.0
        )
    }

    override fun fillDto(dto: CityDto): CityDto {
        dto.buildings = BuildingsDto(
            goldMine = getBuildingDto(buildings.goldMine),
            sawMill = getBuildingDto(buildings.sawMill),
            oreMine = getBuildingDto(buildings.oreMine),
            ironworks = getBuildingDto(buildings.ironworks),
            cityHall = getBuildingDto(buildings.cityHall),
            university = getBuildingDto(buildings.university),
            forge = getBuildingDto(buildings.forge),
            barracks = getBuildingDto(buildings.barracks!!),
            buildingsUpgradeRequirementMatched = buildings.calculateBuildingUpgradeAvailability()
        )
        dto.currentUpgradeBuildingEvents = cityScheduledEvents.startedBuildingUpgradeEventsDto
        return dto
    }

    private fun <T : CityInfrastructure> getBuildingDto(building: T): BuildingsDto.BuildingDto<T> {
        return BuildingsDto.BuildingDto(
            building,
            discountedBuildCostForBuilding(building),
            building.name.buildingName,
            buildings.cityHall.calculateConstructionTime(
                building.futureLevel + 1,
                building.stats.baseToughness.toInt()
            )
        )
    }
}
