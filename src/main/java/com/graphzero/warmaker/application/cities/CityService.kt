package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.application.cities.exceptions.CityNotFoundException
import com.graphzero.warmaker.domain.CityEntity
import com.graphzero.warmaker.shared.annotations.ApplicationService
import io.vavr.control.Either
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.List

@Service
@Transactional
internal class CityService(
    private val cityFactory: CityFactory,
    private val cityRepository: CityRepository
) {
    /**
     * Method updating current resources of given City. You shouldn't return City without actual resources
     *
     * @param cityId
     * @return
     */
    fun getCityDto(cityId: Long): Either<CityNotFoundException, CityDto> {
        return cityRepository
            .findById(cityId)
            .map { cityEntity: CityEntity ->
                val cityRegion = cityFactory.buildCityRegion(cityEntity, listOf())
                val cityBuildings = cityFactory.buildCityBuildings(cityEntity)
                val cityRecruitment = cityFactory.buildCityRecruitment(cityEntity)
                cityRepository.persistResourcesUpdate(cityEntity, cityRegion.resources)
                val cityDto = cityRegion.collectDto(cityRegion, cityBuildings, cityRecruitment)
                Either.right<CityNotFoundException, CityDto>(cityDto)
            }
            .orElseGet { Either.left(CityNotFoundException("City not found")) }
    }
}
