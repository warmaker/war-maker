package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.cities.exceptions.CityException

class BuildingUpgradeProhibited(val reason: Reason) : CityException(
    reason.description
) {
    enum class Reason(val description: String) {
        NOT_ENOUGH_RESOURCES("There are not enough resources to build this building."), REQUIREMENTS_NOT_MATCHED("Requirements to upgrade this building are not matched.");
    }
}
