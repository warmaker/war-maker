package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.resources.ResourcesValue
import java.time.ZonedDateTime

class BuildingUpgrade(
    val upgradeCost: ResourcesValue,
    val executionTime: ZonedDateTime
)
