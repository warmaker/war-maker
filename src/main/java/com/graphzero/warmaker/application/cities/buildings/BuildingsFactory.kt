package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.cities.CityScheduledEvents
import com.graphzero.warmaker.application.cities.buildings.Buildings.BuildingsBuilder
import com.graphzero.warmaker.application.events.WarMakerBuildingEvent
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.config.ApplicationProperties
import com.graphzero.warmaker.config.ApplicationProperties.BuildingConfig
import com.graphzero.warmaker.domain.CityEntity
import com.graphzero.warmaker.repository.CityEntityRepository
import org.springframework.stereotype.Service
import java.util.*
import java.util.stream.Collectors

@Service
class BuildingsFactory(
    private val properties: ApplicationProperties,
    private val cityRepository: CityEntityRepository,
    private val cityBuildingsEventsService: CityBuildingsEventsService
) {
    fun buildings(cityEntity: CityEntity, cityScheduledEvents: CityScheduledEvents): Buildings {
        val buildingsUpgradeEvents = cityScheduledEvents.queuedBuildingsUpgrades
        val cityBuildings = cityEntity.buildings
        return BuildingsBuilder()
            .id(cityBuildings.id)
            .sawMill(sawMill(cityBuildings.sawMill, buildingsUpgradeEvents))
            .oreMine(oreMine(cityBuildings.oreMine, buildingsUpgradeEvents))
            .goldMine(goldMine(cityBuildings.goldMine, buildingsUpgradeEvents))
            .ironworks(ironworks(cityBuildings.ironworks, buildingsUpgradeEvents))
            .university(university(cityBuildings.university, buildingsUpgradeEvents))
            .forge(forge(cityBuildings.forge, buildingsUpgradeEvents))
            .cityHall(cityHall(cityBuildings.cityHall, cityEntity, cityScheduledEvents))
            .barracks(barracks(cityBuildings.barracks, cityEntity.id, cityEntity.army.id, cityScheduledEvents))
            .createBuildings()
    }

    fun barracks(cityId: Long): Barracks {
        val cityEntity = cityRepository.findById(cityId).get()
        val stats = getConfig(properties.getCities().getBuildings().getBarracks())
        val cityScheduledEvents = getBuildingsEvents(cityEntity)
        val barracksUpgrades =
            cityScheduledEvents.queuedBuildingsUpgrades.getOrDefault(WarMakerBuildingEvent.UPGRADE_BARRACKS, 0L).toInt()
        return Barracks(stats, cityEntity.buildings.barracks, barracksUpgrades)
    }

    fun getBuildingsEvents(entity: CityEntity): CityScheduledEvents {
        val cityCommands = cityBuildingsEventsService.listAllCityUpgradeBuildingEvents(entity.id, entity.owner.id)
            .stream()
            .map { obj: UpgradeBuildingScheduledEvent -> obj.toDomainEvent() }
            .collect(Collectors.toList())
        return CityScheduledEvents(ArrayList(), cityCommands)
    }

    private fun sawMill(level: Int, events: Map<WarMakerEventType, Long>): SawMill {
        val stats = getConfig(properties.getCities().getBuildings().getSawMill())
        val sawMillUpgrades = events.getOrDefault(WarMakerBuildingEvent.UPGRADE_SAWMILL, 0L).toInt()
        return SawMill(stats, level, sawMillUpgrades)
    }

    private fun oreMine(level: Int, events: Map<WarMakerEventType, Long>): OreMine {
        val stats = getConfig(properties.getCities().getBuildings().getOreMine())
        val oreMineUpgrades = events.getOrDefault(WarMakerBuildingEvent.UPGRADE_ORE_MINE, 0L).toInt()
        return OreMine(stats, level, oreMineUpgrades)
    }

    private fun goldMine(level: Int, events: Map<WarMakerEventType, Long>): GoldMine {
        val stats = getConfig(properties.getCities().getBuildings().getGoldMine())
        val goldMineUpgrades = events.getOrDefault(WarMakerBuildingEvent.UPGRADE_GOLD_MINE, 0L).toInt()
        return GoldMine(stats, level, goldMineUpgrades)
    }

    private fun ironworks(level: Int, events: Map<WarMakerEventType, Long>): IronWorks {
        val stats = getConfig(properties.getCities().getBuildings().getIronworks())
        val ironworksUpgrades = events.getOrDefault(WarMakerBuildingEvent.UPGRADE_IRONWORKS, 0L).toInt()
        return IronWorks(stats, level, ironworksUpgrades)
    }

    private fun university(level: Int, events: Map<WarMakerEventType, Long>): University {
        val stats = getConfig(properties.getCities().getBuildings().getUniversity())
        val universityUpgrades = events.getOrDefault(WarMakerBuildingEvent.UPGRADE_UNIVERSITY, 0L).toInt()
        return University(stats, level, universityUpgrades)
    }

    private fun forge(level: Int, events: Map<WarMakerEventType, Long>): Forge {
        val stats = getConfig(properties.getCities().getBuildings().getForge())
        val universityUpgrades = events.getOrDefault(WarMakerBuildingEvent.UPGRADE_FORGE, 0L).toInt()
        return Forge(stats, level, universityUpgrades)
    }

    private fun cityHall(level: Int, cityEntity: CityEntity, cityScheduledEvents: CityScheduledEvents): CityHall {
        val stats = getConfig(properties.getCities().getBuildings().getCityHall())
        val cityHallUpgrades =
            cityScheduledEvents.queuedBuildingsUpgrades.getOrDefault(WarMakerBuildingEvent.UPGRADE_CITY_HALL, 0L)
                .toInt()
        return CityHall(stats, level, cityHallUpgrades, cityEntity.id, cityEntity.owner.id, cityScheduledEvents)
    }

    private fun barracks(level: Int, cityId: Long, armyId: Long, cityScheduledEvents: CityScheduledEvents): Barracks {
        val stats = getConfig(properties.getCities().getBuildings().getBarracks())
        val barracksUpgrades =
            cityScheduledEvents.queuedBuildingsUpgrades.getOrDefault(WarMakerBuildingEvent.UPGRADE_BARRACKS, 0L).toInt()
        return Barracks(stats, level, barracksUpgrades)
    }

    private fun getConfig(config: BuildingConfig): InfrastructureBaseStats {
        return InfrastructureBaseStats(
            config.basePointValue.toLong(), config.baseToughness.toLong(), config.baseBuildSpeed.toDouble()
        )
    }
}
