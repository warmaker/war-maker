package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.events.WarMakerBuildingEvent
import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.domain.enumeration.ResourceType
import java.util.*

class SawMill(stats: InfrastructureBaseStats, level: Int, upgradingLevel: Int) :
    CityInfrastructure(stats, level, upgradingLevel, BuildingName.SAW_MILL), ResourceInfrastructure {
    override fun nextLevelCost(): ResourcesValue {
        return ResourcesValue(level * 500 + 1000.0,
            level * 300 + 700.0,
            level * 20 + 500.0,
            level * 10 + 100.0,
            0.0,
            0.0)
    }

    override fun calculatePointValue(): Long {
        return (level + 1) * stats.basePointValue
    }

    override fun calculateToughness(): Long {
        return (level + 1) * stats.baseToughness
    }

    override val upgradeEvent: WarMakerBuildingEvent
        get() = WarMakerBuildingEvent.UPGRADE_SAWMILL

    override fun canUpgrade(buildings: Buildings): Boolean {
        return true
    }

    override fun instance(level: Int): SawMill {
        return SawMill(stats, level, upgradingLevel)
    }

    override fun enhance() {}
    override fun incomeModifier(): Map<ResourceType, Double> {
        return Collections.singletonMap(ResourceType.WOOD, level * 0.2)
    }
}
