package com.graphzero.warmaker.application.cities

interface CityEvent {
    val cityId: Long
}
