package com.graphzero.warmaker.application.cities.recruitment

import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/city")
internal class RecruitmentController(private val cityService: CityRecruitmentService) {
    private val log = LoggerFactory.getLogger(RecruitmentController::class.java)

    @PostMapping("/{id}/recruit")
    fun upgradeGoldMine(
        @PathVariable(value = "id") cityId: Long,
        @RequestBody unitsList: HumanUnitsList
    ): ResponseEntity<*> {
        log.debug("REST Request to start recruiting units [{}] for city {}", unitsList, cityId)
        val resourcesValue = cityService.startUnitsRecruitment(cityId, unitsList)
        return if (resourcesValue.isRight) {
            ResponseEntity.ok(resourcesValue.get())
        } else {
            ResponseEntity.badRequest().body(resourcesValue.left!!.message)
        }
    }
}
