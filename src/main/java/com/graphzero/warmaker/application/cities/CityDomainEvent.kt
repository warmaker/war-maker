package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import java.util.*


abstract class CityDomainEvent<E : WarMakerEventType>(
    source: Any,
    uuid: UUID,
    warMakerEventType: E,
    override val cityId: Long
) : DomainEvent<E>(source, warMakerEventType, uuid), CityEvent
