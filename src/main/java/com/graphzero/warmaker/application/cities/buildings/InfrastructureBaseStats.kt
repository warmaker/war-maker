package com.graphzero.warmaker.application.cities.buildings

class InfrastructureBaseStats(
    val basePointValue: Long,
    val baseToughness: Long,
    val baseBuildSpeed: Double
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as InfrastructureBaseStats

        if (basePointValue != other.basePointValue) return false
        if (baseToughness != other.baseToughness) return false
        if (baseBuildSpeed != other.baseBuildSpeed) return false

        return true
    }

    override fun hashCode(): Int {
        var result = basePointValue.hashCode()
        result = 31 * result + baseToughness.hashCode()
        result = 31 * result + baseBuildSpeed.hashCode()
        return result
    }
}
