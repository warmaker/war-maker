package com.graphzero.warmaker.application.config

import org.jooq.conf.RenderQuotedNames
import org.jooq.conf.Settings
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class JooqConfig {

    @Bean
    fun jooqSettings(): Settings {
        return Settings().withRenderQuotedNames(RenderQuotedNames.NEVER)
    }

}
