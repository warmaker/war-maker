package com.graphzero.warmaker.application.events.schedulable

import com.fasterxml.jackson.annotation.JsonProperty
import com.graphzero.warmaker.application.events.WarMakerEventType

abstract class AbstractSchedulableEventDto(
    @get:JsonProperty("ownerId") val ownerId: Long, @get:JsonProperty(
        "type"
    ) val type: WarMakerEventType?
)
