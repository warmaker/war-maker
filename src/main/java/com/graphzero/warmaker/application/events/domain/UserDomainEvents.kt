package com.graphzero.warmaker.application.events.domain

import com.graphzero.warmaker.application.events.WarMakerEventType

abstract class UserDomainEvents<T : DomainEvent<WarMakerEventType>> protected constructor(private val recruitmentEvents: List<T>)
