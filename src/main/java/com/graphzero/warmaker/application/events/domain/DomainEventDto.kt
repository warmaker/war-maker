package com.graphzero.warmaker.application.events.domain

import com.google.gson.Gson
import com.graphzero.warmaker.application.events.WarMakerEventType
import java.util.*

abstract class DomainEventDto(val uuid: UUID, val type: WarMakerEventType) {
    @Transient
    private val gson = Gson()
    fun toJson(): String {
        return gson.toJson(this)
    }
}
