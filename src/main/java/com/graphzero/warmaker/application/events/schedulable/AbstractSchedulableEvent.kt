package com.graphzero.warmaker.application.events.schedulable

import com.graphzero.warmaker.application.events.WarMakerEventType
import org.springframework.context.ApplicationEvent
import java.time.ZonedDateTime
import java.util.*

/**
 * Event to be designed to be executed at specific time @executionTime
 *
 * @param <T>
</T> */
abstract class AbstractSchedulableEvent<E : WarMakerEventType>(
    source: Any,
    val ownerId: Long,
    val warMakerEventType: E,
    var executionTime: ZonedDateTime,
    val uuid: UUID
) : ApplicationEvent(source), Runnable {
    override fun run() {
        finishTask()
        execute()
    }

    private fun finishTask() {
        ScheduledEventsService.finishedTasks.incrementAndGet()
        ScheduledEventsService.remove(ownerId, uuid)
    }

    protected abstract fun execute()
    abstract fun dto(): AbstractSchedulableEventDto


    override fun toString(): String {
        return "AbstractSchedulableEvent{" +
                "ownerId=" + ownerId +
                ", getUuid=" + uuid +
                ", warMakerEventType=" + warMakerEventType +
                ", executionTime=" + executionTime +
                '}'
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AbstractSchedulableEvent<*>

        if (ownerId != other.ownerId) return false
        if (warMakerEventType != other.warMakerEventType) return false
        if (executionTime != other.executionTime) return false
        if (uuid != other.uuid) return false

        return true
    }

    override fun hashCode(): Int {
        var result = ownerId.hashCode()
        result = 31 * result + warMakerEventType.hashCode()
        result = 31 * result + executionTime.hashCode()
        result = 31 * result + uuid.hashCode()
        return result
    }
}
