package com.graphzero.warmaker.application.events

enum class WarMakerBuildingEvent : WarMakerEventType {
    UPGRADE_SAWMILL, UPGRADE_ORE_MINE, UPGRADE_GOLD_MINE, UPGRADE_IRONWORKS, UPGRADE_BARRACKS, UPGRADE_CITY_HALL, UPGRADE_UNIVERSITY, UPGRADE_FORGE
}
