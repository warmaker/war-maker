package com.graphzero.warmaker.application.events

enum class WarMakerArmyEvent : WarMakerEventType {
    UNITS_RECRUITED, FINISHED_UNITS_RECRUITMENT, WAR_PARTY_JOURNEY, WAR_PARTY_BATTLES, WAR_PARTY_ANNIHILATED, WAR_PARTY_RESOURCES_CHANGE
}
