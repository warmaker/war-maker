package com.graphzero.warmaker.application.events.domain

import com.graphzero.warmaker.application.events.WarMakerEventType
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import java.util.concurrent.ConcurrentHashMap
import java.util.function.Consumer

@Service // TODO - make published events async !!!
class DomainEventsPublisher(protected val publisher: ApplicationEventPublisher) {
    protected val tasksToComplete = ConcurrentHashMap<Long, UserDomainEvents<DomainEvent<WarMakerEventType>>>()

    fun publish(event: DomainEvent<out WarMakerEventType>) {
        publisher.publishEvent(event)
    }

    fun publish(events: Iterable<DomainEvent<out WarMakerEventType>>) {
        events.forEach(Consumer { event: DomainEvent<out WarMakerEventType> -> this.publish(event) })
    }
}
