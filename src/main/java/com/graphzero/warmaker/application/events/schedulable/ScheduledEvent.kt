package com.graphzero.warmaker.application.events.schedulable

import java.util.concurrent.ScheduledFuture

internal class ScheduledEvent<T : AbstractSchedulableEvent<*>>(val event: T, val future: ScheduledFuture<*>) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ScheduledEvent<*>

        if (event != other.event) return false
        if (future != other.future) return false

        return true
    }

    override fun hashCode(): Int {
        var result = event.hashCode()
        result = 31 * result + future.hashCode()
        return result
    }
}
