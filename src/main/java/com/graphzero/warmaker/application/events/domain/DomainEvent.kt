package com.graphzero.warmaker.application.events.domain

import com.graphzero.warmaker.application.events.WarMakerEventType
import org.springframework.context.ApplicationEvent
import java.util.*

/**
 * Event designed to be executed immediately
 */
abstract class DomainEvent<E : WarMakerEventType> : ApplicationEvent {
    @kotlin.jvm.JvmField
    val warMakerEventType: E
    val uuid: UUID

    constructor(source: Any, warMakerEventType: E, uuid: UUID) : super(source) {
        this.warMakerEventType = warMakerEventType
        this.uuid = uuid
    }

    constructor(source: Any, warMakerEventType: E) : super(source) {
        this.warMakerEventType = warMakerEventType
        uuid = UUID.randomUUID()
    }

    abstract fun dto(): DomainEventDto
}
