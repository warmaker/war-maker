package com.graphzero.warmaker.application.events.schedulable

import com.graphzero.warmaker.application.events.schedulable.ScheduledEventsService
import org.slf4j.LoggerFactory
import org.springframework.scheduling.TaskScheduler
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicLong
import java.util.stream.Collectors

@Service
class ScheduledEventsService(private val scheduler: TaskScheduler) {
    fun <T : AbstractSchedulableEvent<*>> schedule(event: T) {
        log.debug(
            "Scheduling: [{}] to start at [{}] for owner [{}]",
            event.uuid,
            event.executionTime,
            event.ownerId
        )
        synchronized(tasksToComplete) {
            tasksToComplete.computeIfPresent(event.ownerId) { aLong: Long, scheduledEvents: MutableList<ScheduledEvent<*>> ->
                val tScheduledEvent =
                    ScheduledEvent(event, scheduler.schedule(event, event.executionTime.toInstant()))
                scheduledEvents.add(tScheduledEvent)
                scheduledEvents
            }
            tasksToComplete.computeIfAbsent(event.ownerId) { aLong: Long ->
                val scheduledEvents: List<ScheduledEvent<*>> = ArrayList<ScheduledEvent<*>>(
                    listOf(
                        ScheduledEvent(
                            event,
                            scheduler.schedule(event, event.executionTime.toInstant())
                        )
                    )
                )
                Collections.synchronizedList(scheduledEvents)
            }
        }
    }

    fun findEventsForUser(userId: Long): List<AbstractSchedulableEvent<*>> {
        return tasksToComplete.getOrDefault(userId, ArrayList())
            .stream()
            .map { event: ScheduledEvent<*> -> event.event }
            .collect(Collectors.toList())
    }

    fun findEventsDtoForUser(userId: Long): List<AbstractSchedulableEventDto> {
        return tasksToComplete.getOrDefault(userId, ArrayList())
            .stream()
            .map { event: ScheduledEvent<*> -> event.event.dto() }
            .collect(Collectors.toList())
    }

    fun cancel(userId: Long, eventUuid: UUID): Boolean {
        log.debug("Cancelling: [{}] of user [{}]", eventUuid, userId)
        return Optional
            .ofNullable(tasksToComplete[userId])
            .map { scheduledEvents: List<ScheduledEvent<*>> ->
                scheduledEvents
                    .stream()
                    .filter { scheduledEvent: ScheduledEvent<*> -> scheduledEvent.event.uuid == eventUuid }
                    .findFirst()
                    .map { scheduledEvent: ScheduledEvent<*> ->
                        scheduledEvent.future.cancel(true)
                        tasksToComplete[userId]!!.remove(scheduledEvent)
                        true
                    }
                    .orElse(false)
            }
            .orElse(false)
    }

    private fun executingTasks(): Long {
        return tasksToComplete
            .values
            .stream()
            .mapToLong { obj: List<ScheduledEvent<*>> -> obj.size.toLong() }
            .sum()
    }

    @Scheduled(fixedRate = 10000)
    fun reportNumberOfExecutingTasks() {
        log.debug("Currently there are queued [{}] tasks", executingTasks())
    }

    companion object {
        val finishedTasks = AtomicLong(0)
        private val tasksToComplete = ConcurrentHashMap<Long, MutableList<ScheduledEvent<*>>>()
        private val log = LoggerFactory.getLogger(ScheduledEventsService::class.java)
        fun remove(userId: Long, eventUuid: UUID): Boolean {
            return Optional
                .ofNullable(tasksToComplete[userId])
                .map { scheduledEvents: List<ScheduledEvent<*>> ->
                    synchronized(
                        tasksToComplete[userId]!!
                    ) {
                        return@map scheduledEvents
                            .stream()
                            .filter { scheduledEvent: ScheduledEvent<*> -> scheduledEvent.event.uuid == eventUuid }
                            .findFirst()
                            .map { scheduledEvent: ScheduledEvent<*> ->
                                tasksToComplete[userId]!!.remove(scheduledEvent)
                                true
                            }
                            .orElse(false)
                    }
                }
                .orElse(false)
        }
    }
}
