package com.graphzero.warmaker.application.researches.scouting

import com.graphzero.warmaker.application.world.Coordinates

class ScoutingObservationPosition(xCoordinate: Int,
                                  yCoordinate: Int,
                                  val scoutingLevel: Int) {
    val coordinates: Coordinates = Coordinates(xCoordinate, yCoordinate)
}
