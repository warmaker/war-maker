package com.graphzero.warmaker.application.researches

import com.graphzero.warmaker.domain.ResearchesEntity
import com.graphzero.warmaker.repository.ResearchesEntityRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
internal interface ResearchesCrudRepository : ResearchesEntityRepository {
    @Query(
        "SELECT re " +
                "FROM ProfileEntity pe " +
                "  JOIN pe.researches re " +
                "WHERE pe.id = :playerId "
    )
    fun findByPlayerId(@Param("playerId") playerId: Long): ResearchesEntity?
}
