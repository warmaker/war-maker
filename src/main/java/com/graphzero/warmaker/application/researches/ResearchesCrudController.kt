package com.graphzero.warmaker.application.researches

import com.graphzero.warmaker.domain.ResearchesEntity
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/researches")
internal class ResearchesCrudController(private val researchesCrudRepository: ResearchesCrudRepository) {
    private val log = LoggerFactory.getLogger(ResearchesCrudController::class.java)
    @GetMapping("")
    fun getUserResearches(@RequestParam(value = "playerId") playerId: Long): ResponseEntity<ResearchesEntity> {
        log.debug("REST request to return researches for profile [{}]", playerId)
        val researches = researchesCrudRepository.findByPlayerId(playerId)
        return ResponseEntity.ok(researches)
    }
}
