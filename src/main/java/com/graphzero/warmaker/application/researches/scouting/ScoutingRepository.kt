package com.graphzero.warmaker.application.researches.scouting

import com.graphzero.warmaker.domain.FieldEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
open interface ScoutingRepository : JpaRepository<FieldEntity, Long> {
    @Query(
        "SELECT new com.graphzero.warmaker.application.researches.scouting.ScoutingObservationPosition(fe.xCoordinate, fe.yCoordinate, re.scouting) " +
                " FROM CityEntity c " +
                " JOIN c.location fe " +
                " JOIN c.owner pe " +
                " JOIN pe.researches re  " +
                " WHERE pe.id = :profileId"
    )
    fun findScoutingObservationPosition(@Param("profileId") profileId: Long): ScoutingObservationPosition
}
