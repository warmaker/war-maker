package com.graphzero.warmaker.application.researches.scouting

class ScoutingDetectRadius(val xCoordinateBounds: Bounding, val yCoordinatesBounds: Bounding) {
    class Bounding(val lowerBound: Int, val upperBound: Int) {
        override fun toString(): String {
            return "Bounding{" +
                    "lowerBound=" + lowerBound +
                    ", upperBound=" + upperBound +
                    '}'
        }
    }

    override fun toString(): String {
        return "ScoutingDetectRadius{" +
                "xCoordinateBounds=" + xCoordinateBounds +
                ", yCoordinatesBounds=" + yCoordinatesBounds +
                '}'
    }
}
