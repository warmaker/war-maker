package com.graphzero.warmaker.application.researches.scouting

import com.graphzero.warmaker.application.researches.scouting.ScoutingDetectRadius.Bounding
import com.graphzero.warmaker.application.world.WorldConstants
import kotlin.math.max

class Scouting {
    // FIX this since it doesn't really work
    fun calculateBaseScoutingBounds(scoutingObservationPosition: ScoutingObservationPosition): ScoutingDetectRadius {
        val playerCoordinates = scoutingObservationPosition.coordinates
        val scoutingLevel = scoutingObservationPosition.scoutingLevel
        val xCoordinateBounds = Bounding(
            bound(playerCoordinates.x, scoutingLevel), bound(
                playerCoordinates.x, -scoutingLevel
            )
        )
        val yCoordinateBounds = Bounding(
            bound(playerCoordinates.y, WorldConstants.DOUBLED_Y_COORDINATE * scoutingLevel), bound(
                playerCoordinates.y, -WorldConstants.DOUBLED_Y_COORDINATE * scoutingLevel
            )
        )
        return ScoutingDetectRadius(xCoordinateBounds, yCoordinateBounds)
    }

    private fun bound(coordinate: Int, scoutingLevel: Int): Int {
        return max(coordinate - scoutingLevel, 0)
    }
}
