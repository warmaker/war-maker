package com.graphzero.warmaker.application.player

import com.graphzero.warmaker.domain.ProfileEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
internal interface ProfileEntityCustomRepository : JpaRepository<ProfileEntity, Long> {
    @Query(
        " SELECT p " +
                "FROM ProfileEntity p " +
                "   LEFT OUTER JOIN FETCH p.relations r " +
                "WHERE p.id = :profileId "
    )
    fun findByIdEager(@Param("profileId") profileId: Long): Optional<ProfileEntity>
}
