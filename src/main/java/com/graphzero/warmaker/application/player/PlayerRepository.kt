package com.graphzero.warmaker.application.player

import com.graphzero.warmaker.domain.PlayerRelations
import com.graphzero.warmaker.domain.ProfileEntity
import com.graphzero.warmaker.domain.enumeration.RelationType
import com.graphzero.warmaker.repository.PlayerRelationsRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.util.stream.Collectors

@Repository
internal class PlayerRepository(
    private val profileEntityRepository: ProfileEntityCustomRepository,
    private val playerRelationsRepository: PlayerRelationsRepository
) {
    @Transactional
    fun persist(player: Player) {
        val relationOwner = profileEntityRepository.findByIdEager(player.id).get()
        val relationsInDb = relationOwner.relations.stream()
            .map { playerRelations: PlayerRelations -> playerRelations.targetPlayer.id }
            .collect(Collectors.toSet())
        val notPersistedEnemies = player.enemies.stream()
            .filter { enemy: Long -> !relationsInDb.contains(enemy) }
            .collect(Collectors.toSet())
        val notPersistedAllies = player.allies.stream()
            .filter { ally: Long -> !relationsInDb.contains(ally) }
            .collect(Collectors.toSet())
        val enemyPlayerRelations = createRelations(relationOwner, notPersistedEnemies, RelationType.ENEMY)
        val allyPlayerRelations = createRelations(relationOwner, notPersistedAllies, RelationType.ALLY)
        playerRelationsRepository.saveAll(enemyPlayerRelations)
        playerRelationsRepository.saveAll(allyPlayerRelations)
    }

    private fun createRelations(
        relationOwner: ProfileEntity,
        notPersistedRelations: Set<Long>,
        ally: RelationType
    ): Set<PlayerRelations> {
        return notPersistedRelations.stream()
            .map { aLong: Long ->
                val enemy = profileEntityRepository.getOne(aLong)
                PlayerRelations()
                    .owner(relationOwner)
                    .targetPlayer(enemy)
                    .relation(ally)
            }
            .collect(Collectors.toSet())
    }
}
