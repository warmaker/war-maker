package com.graphzero.warmaker.application.player

import com.graphzero.warmaker.domain.ProfileEntity
import com.graphzero.warmaker.repository.ProfileEntityRepository
import com.graphzero.warmaker.repository.WarPartyEntityRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class PlayerFactory(
    val warPartyEntityRepository: WarPartyEntityRepository,
    val profileEntityRepository: ProfileEntityRepository
) {
    fun getOwner(warPartyId: Long): Player {
        return warPartyEntityRepository.getOwner(warPartyId)
            .map { profileEntity: ProfileEntity -> toPlayer(profileEntity) }
            .or {
                profileEntityRepository
                    .findByAccountName(Player.BANDIT_NAME)
                    .map { profileEntity: ProfileEntity -> toPlayer(profileEntity) }
            }
            .orElseThrow { RuntimeException("Couldn't find player. Are Bandits defined?") }
    }

    @Transactional
    fun getPlayer(playerId: Long): Player {
        return profileEntityRepository.findById(playerId)
            .map { profileEntity: ProfileEntity -> toPlayer(profileEntity) }
            .or {
                profileEntityRepository
                    .findByAccountName(Player.BANDIT_NAME)
                    .map { profileEntity: ProfileEntity -> toPlayer(profileEntity) }
            }
            .orElseThrow { RuntimeException("Couldn't find player. Are Bandits defined?") }
    }

    private fun toPlayer(profileEntity: ProfileEntity): Player {
        return Player(profileEntity.id, profileEntity.accountName, profileEntity.relations)
    }
}
