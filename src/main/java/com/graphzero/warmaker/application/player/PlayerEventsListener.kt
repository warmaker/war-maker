package com.graphzero.warmaker.application.player

import com.graphzero.warmaker.application.kafka.MessageBoxKafkaProducer
import com.graphzero.warmaker.application.warparty.battle.WarPartyBattleEvent
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service

@Service
internal class PlayerEventsListener(private val kafkaProducer: MessageBoxKafkaProducer) {
    private val log = LoggerFactory.getLogger(PlayerEventsListener::class.java)

    @EventListener
    fun handleWarPartyBattleEvent(event: WarPartyBattleEvent) {
        log.debug("WarPartyBattleEvent received to send message about War Party [{}] battle", event.warParty.id)
        kafkaProducer.sendBattleMessage(event)
    }

    @EventListener
    fun handleWarDeclaredEvent(event: WarDeclaredEvent) {
        log.debug("WarDeclaredEvent received to send message about War declaration by [{}]", event.attackerId)
        kafkaProducer.sendWarDeclarationMessage(event)
    }
}
