package com.graphzero.warmaker.application.player

import com.graphzero.warmaker.application.events.domain.DomainEventsPublisher
import org.springframework.stereotype.Service

@Service
class PlayerService internal constructor(
    private val playerRepository: PlayerRepository,
    private val playerFactory: PlayerFactory,
    private val domainEventsPublisher: DomainEventsPublisher
) {
    fun declareWar(warDeclarationReq: WarDeclarationReq) {
        val attackingPlayer = playerFactory.getPlayer(warDeclarationReq.attackerId)
        val defendingPlayer = playerFactory.getPlayer(warDeclarationReq.defenderId)
        attackingPlayer.declareWar(defendingPlayer)
        playerRepository.persist(attackingPlayer)
        playerRepository.persist(defendingPlayer)
        domainEventsPublisher.publish(attackingPlayer.getDomainEvents())
        domainEventsPublisher.publish(defendingPlayer.getDomainEvents())
    }

    fun declareWar(attackerId: Long, defenderId: Long) {
        declareWar(WarDeclarationReq(attackerId, defenderId))
    }
}
