package com.graphzero.warmaker.application.player

import com.graphzero.warmaker.application.ddd.AggregateRoot
import com.graphzero.warmaker.application.ddd.AggregateRootLogic
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.domain.PlayerRelations
import com.graphzero.warmaker.domain.enumeration.RelationType
import io.vavr.control.Either
import java.util.stream.Collectors

class Player : AggregateRoot {
    private val aggregateRoot: AggregateRootLogic
    private val playerName: String
    val enemies: MutableList<Long>
    val allies: List<Long>

    constructor(id: Long, playerName: String, relations: Set<PlayerRelations>) {
        aggregateRoot = AggregateRootLogic(id)
        this.playerName = playerName
        val groupedRelations: Map<RelationType, List<PlayerRelations>> =
            relations.stream().collect(Collectors.groupingBy { obj: PlayerRelations -> obj.relation })
        enemies = collectRelation(groupedRelations, RelationType.ENEMY)
        allies = collectRelation(groupedRelations, RelationType.ALLY)
    }

    constructor(id: Long, playerName: String, enemies: MutableList<Long>, allies: List<Long>) {
        aggregateRoot = AggregateRootLogic(id)
        this.playerName = playerName
        this.enemies = enemies
        this.allies = allies
    }

    private fun collectRelation(
        groupedRelations: Map<RelationType, List<PlayerRelations>>,
        relationType: RelationType
    ): MutableList<Long> {
        return (groupedRelations[relationType] ?: emptyList())
            .stream()
            .map { relation: PlayerRelations -> relation.targetPlayer.id }
            .collect(Collectors.toList())
    }

    fun declareWar(player: Player): Either<WarDeclarationFailure, List<Long>> {
        return when {
            isEnemy(player.id) -> {
                Either.left(WarDeclarationFailure.WAR_ALREADY_DECLARED)
            }
            isAlly(player.id) -> {
                Either.left(WarDeclarationFailure.CANNOT_DECLARE_WAR_ON_ALLY)
            }
            else -> {
                declareWar(player.id)
                player.acceptWarDeclaration(id)
                Either.right(enemies)
            }
        }
    }

    fun isEnemy(playerId: Long): Boolean {
        return enemies.stream()
            .anyMatch { enemy: Long -> enemy == playerId }
    }

    private fun declareWar(enemyId: Long): Player {
        enemies.add(enemyId)
        registerDomainEvent(WarDeclaredEvent(this, id, enemyId))
        return this
    }

    private fun acceptWarDeclaration(declaringPlayer: Long): Player {
        enemies.add(declaringPlayer)
        return this
    }

    fun isAlly(playerId: Long): Boolean {
        return allies.stream().anyMatch { enemy: Long -> enemy == playerId }
    }

    val isBandit: Boolean
        get() = playerName == BANDIT_NAME

    enum class WarDeclarationFailure {
        CANNOT_DECLARE_WAR_ON_ALLY, WAR_ALREADY_DECLARED
    }

    override val id: Long
        get() = aggregateRoot.id!!

    override fun registerDomainEvent(event: DomainEvent<out WarMakerEventType>) {
        aggregateRoot.registerDomainEvent(event)
    }

    override fun clearEvents() {
        aggregateRoot.clearEvents()
    }

    override fun getDomainEvents(): List<DomainEvent<out WarMakerEventType>> {
        return aggregateRoot.getDomainEvents()
    }

    companion object {
        const val BANDIT_NAME = "bandit"
    }
}
