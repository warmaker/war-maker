package com.graphzero.warmaker.application.player

import com.fasterxml.jackson.annotation.JsonProperty

class WarDeclarationReq(
    @get:JsonProperty("attackerId") val attackerId: Long,
    @get:JsonProperty("defenderId") val defenderId: Long
)
