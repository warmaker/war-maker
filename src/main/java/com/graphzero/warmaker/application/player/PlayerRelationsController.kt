package com.graphzero.warmaker.application.player

import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/player/")
internal class PlayerRelationsController(private val playerService: PlayerService) {
    private val log = LoggerFactory.getLogger(PlayerRelationsController::class.java)
    @PostMapping("declare-war")
    fun declareWar(@RequestBody warDeclarationReq: WarDeclarationReq): ResponseEntity<Void> {
        log.debug(
            "REST request to declare war by [{}] on [{}]", warDeclarationReq.attackerId,
            warDeclarationReq.defenderId
        )
        playerService.declareWar(warDeclarationReq)
        return ResponseEntity.ok().build()
    }
}
