package com.graphzero.warmaker.application.player

import com.graphzero.warmaker.application.events.WarMakerEventType

enum class WarMakerPlayerEvent : WarMakerEventType {
    WAR_DECLARED
}
