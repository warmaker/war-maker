package com.graphzero.warmaker.application.player

import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.events.domain.DomainEventDto
import com.graphzero.warmaker.application.time.TimeConfiguration
import java.time.ZonedDateTime
import java.util.*

class WarDeclaredEvent(source: Any, val attackerId: Long, private val defenderId: Long) :
    DomainEvent<WarMakerPlayerEvent>(source, WarMakerPlayerEvent.WAR_DECLARED, UUID.randomUUID()) {
    private val declarationTime: ZonedDateTime = ZonedDateTime.now(TimeConfiguration.serverClock)
    override fun dto(): WarDeclaredEventDto {
        return WarDeclaredEventDto(attackerId, defenderId, declarationTime)
    }

    class WarDeclaredEventDto(
        val attackerId: Long,
        val defenderId: Long,
        declarationTime: ZonedDateTime
    ) : DomainEventDto(UUID.randomUUID(), WarMakerPlayerEvent.WAR_DECLARED) {
        private val declarationTime: String = declarationTime.toString()

    }

}
