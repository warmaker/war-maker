package com.graphzero.warmaker.application.ddd

import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent

class DomainEvents {
    val domainEvents: MutableList<DomainEvent<out WarMakerEventType>> = ArrayList()

    fun registerDomainEvent(event: DomainEvent<out WarMakerEventType>) {
        domainEvents.add(event)
    }

    fun clearEvents() {
        domainEvents.clear()
    }

}
