package com.graphzero.warmaker.application.ddd

import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import java.util.function.Consumer

interface AggregateRoot {
    val id: Long?
    fun getDomainEvents(): List<DomainEvent<out WarMakerEventType>>
    fun registerDomainEvent(event: DomainEvent<out WarMakerEventType>)
    fun clearEvents()
    fun registerDomainEvents(events: Collection<DomainEvent<out WarMakerEventType>>) {
        events.forEach(Consumer { event: DomainEvent<out WarMakerEventType> -> registerDomainEvent(event) })
    }
}
