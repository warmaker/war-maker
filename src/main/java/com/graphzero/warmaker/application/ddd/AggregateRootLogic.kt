package com.graphzero.warmaker.application.ddd

import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent

class AggregateRootLogic (override var id: Long?,
                          private val domainEvents: DomainEvents = DomainEvents()) : AggregateRoot {

    override fun registerDomainEvent(event: DomainEvent<out WarMakerEventType>) {
        domainEvents.registerDomainEvent(event)
    }

    override fun clearEvents() {
        domainEvents.clearEvents()
    }

    override fun getDomainEvents(): List<DomainEvent<out WarMakerEventType>> {
        return domainEvents.domainEvents
    }
}
