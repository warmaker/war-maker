/**
 * Data Access Objects used by WebSocket services.
 */
package com.graphzero.warmaker.web.websocket.dto;
