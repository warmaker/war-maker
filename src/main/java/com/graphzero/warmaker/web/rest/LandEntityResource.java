package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.LandEntity;
import com.graphzero.warmaker.repository.LandEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.LandEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class LandEntityResource {

    private final Logger log = LoggerFactory.getLogger(LandEntityResource.class);

    private static final String ENTITY_NAME = "landEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LandEntityRepository landEntityRepository;

    public LandEntityResource(LandEntityRepository landEntityRepository) {
        this.landEntityRepository = landEntityRepository;
    }

    /**
     * {@code POST  /land-entities} : Create a new landEntity.
     *
     * @param landEntity the landEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new landEntity, or with status {@code 400 (Bad Request)} if the landEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/land-entities")
    public ResponseEntity<LandEntity> createLandEntity(@Valid @RequestBody LandEntity landEntity) throws URISyntaxException {
        log.debug("REST request to save LandEntity : {}", landEntity);
        if (landEntity.getId() != null) {
            throw new BadRequestAlertException("A new landEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LandEntity result = landEntityRepository.save(landEntity);
        return ResponseEntity.created(new URI("/api/land-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /land-entities} : Updates an existing landEntity.
     *
     * @param landEntity the landEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated landEntity,
     * or with status {@code 400 (Bad Request)} if the landEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the landEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/land-entities")
    public ResponseEntity<LandEntity> updateLandEntity(@Valid @RequestBody LandEntity landEntity) throws URISyntaxException {
        log.debug("REST request to update LandEntity : {}", landEntity);
        if (landEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LandEntity result = landEntityRepository.save(landEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, landEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /land-entities} : get all the landEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of landEntities in body.
     */
    @GetMapping("/land-entities")
    public List<LandEntity> getAllLandEntities() {
        log.debug("REST request to get all LandEntities");
        return landEntityRepository.findAll();
    }

    /**
     * {@code GET  /land-entities/:id} : get the "id" landEntity.
     *
     * @param id the id of the landEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the landEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/land-entities/{id}")
    public ResponseEntity<LandEntity> getLandEntity(@PathVariable Long id) {
        log.debug("REST request to get LandEntity : {}", id);
        Optional<LandEntity> landEntity = landEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(landEntity);
    }

    /**
     * {@code DELETE  /land-entities/:id} : delete the "id" landEntity.
     *
     * @param id the id of the landEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/land-entities/{id}")
    public ResponseEntity<Void> deleteLandEntity(@PathVariable Long id) {
        log.debug("REST request to delete LandEntity : {}", id);
        landEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
