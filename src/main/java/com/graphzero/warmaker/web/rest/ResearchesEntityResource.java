package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.ResearchesEntity;
import com.graphzero.warmaker.repository.ResearchesEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.ResearchesEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ResearchesEntityResource {

    private final Logger log = LoggerFactory.getLogger(ResearchesEntityResource.class);

    private static final String ENTITY_NAME = "researchesEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ResearchesEntityRepository researchesEntityRepository;

    public ResearchesEntityResource(ResearchesEntityRepository researchesEntityRepository) {
        this.researchesEntityRepository = researchesEntityRepository;
    }

    /**
     * {@code POST  /researches-entities} : Create a new researchesEntity.
     *
     * @param researchesEntity the researchesEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new researchesEntity, or with status {@code 400 (Bad Request)} if the researchesEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/researches-entities")
    public ResponseEntity<ResearchesEntity> createResearchesEntity(@RequestBody ResearchesEntity researchesEntity) throws URISyntaxException {
        log.debug("REST request to save ResearchesEntity : {}", researchesEntity);
        if (researchesEntity.getId() != null) {
            throw new BadRequestAlertException("A new researchesEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ResearchesEntity result = researchesEntityRepository.save(researchesEntity);
        return ResponseEntity.created(new URI("/api/researches-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /researches-entities} : Updates an existing researchesEntity.
     *
     * @param researchesEntity the researchesEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated researchesEntity,
     * or with status {@code 400 (Bad Request)} if the researchesEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the researchesEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/researches-entities")
    public ResponseEntity<ResearchesEntity> updateResearchesEntity(@RequestBody ResearchesEntity researchesEntity) throws URISyntaxException {
        log.debug("REST request to update ResearchesEntity : {}", researchesEntity);
        if (researchesEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ResearchesEntity result = researchesEntityRepository.save(researchesEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, researchesEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /researches-entities} : get all the researchesEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of researchesEntities in body.
     */
    @GetMapping("/researches-entities")
    public List<ResearchesEntity> getAllResearchesEntities() {
        log.debug("REST request to get all ResearchesEntities");
        return researchesEntityRepository.findAll();
    }

    /**
     * {@code GET  /researches-entities/:id} : get the "id" researchesEntity.
     *
     * @param id the id of the researchesEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the researchesEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/researches-entities/{id}")
    public ResponseEntity<ResearchesEntity> getResearchesEntity(@PathVariable Long id) {
        log.debug("REST request to get ResearchesEntity : {}", id);
        Optional<ResearchesEntity> researchesEntity = researchesEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(researchesEntity);
    }

    /**
     * {@code DELETE  /researches-entities/:id} : delete the "id" researchesEntity.
     *
     * @param id the id of the researchesEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/researches-entities/{id}")
    public ResponseEntity<Void> deleteResearchesEntity(@PathVariable Long id) {
        log.debug("REST request to delete ResearchesEntity : {}", id);
        researchesEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
