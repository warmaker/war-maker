package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.PointOfInterestEntity;
import com.graphzero.warmaker.repository.PointOfInterestEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.PointOfInterestEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PointOfInterestEntityResource {

    private final Logger log = LoggerFactory.getLogger(PointOfInterestEntityResource.class);

    private static final String ENTITY_NAME = "pointOfInterestEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PointOfInterestEntityRepository pointOfInterestEntityRepository;

    public PointOfInterestEntityResource(PointOfInterestEntityRepository pointOfInterestEntityRepository) {
        this.pointOfInterestEntityRepository = pointOfInterestEntityRepository;
    }

    /**
     * {@code POST  /point-of-interest-entities} : Create a new pointOfInterestEntity.
     *
     * @param pointOfInterestEntity the pointOfInterestEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pointOfInterestEntity, or with status {@code 400 (Bad Request)} if the pointOfInterestEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/point-of-interest-entities")
    public ResponseEntity<PointOfInterestEntity> createPointOfInterestEntity(@RequestBody PointOfInterestEntity pointOfInterestEntity) throws URISyntaxException {
        log.debug("REST request to save PointOfInterestEntity : {}", pointOfInterestEntity);
        if (pointOfInterestEntity.getId() != null) {
            throw new BadRequestAlertException("A new pointOfInterestEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PointOfInterestEntity result = pointOfInterestEntityRepository.save(pointOfInterestEntity);
        return ResponseEntity.created(new URI("/api/point-of-interest-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /point-of-interest-entities} : Updates an existing pointOfInterestEntity.
     *
     * @param pointOfInterestEntity the pointOfInterestEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pointOfInterestEntity,
     * or with status {@code 400 (Bad Request)} if the pointOfInterestEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pointOfInterestEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/point-of-interest-entities")
    public ResponseEntity<PointOfInterestEntity> updatePointOfInterestEntity(@RequestBody PointOfInterestEntity pointOfInterestEntity) throws URISyntaxException {
        log.debug("REST request to update PointOfInterestEntity : {}", pointOfInterestEntity);
        if (pointOfInterestEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PointOfInterestEntity result = pointOfInterestEntityRepository.save(pointOfInterestEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, pointOfInterestEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /point-of-interest-entities} : get all the pointOfInterestEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pointOfInterestEntities in body.
     */
    @GetMapping("/point-of-interest-entities")
    public List<PointOfInterestEntity> getAllPointOfInterestEntities() {
        log.debug("REST request to get all PointOfInterestEntities");
        return pointOfInterestEntityRepository.findAll();
    }

    /**
     * {@code GET  /point-of-interest-entities/:id} : get the "id" pointOfInterestEntity.
     *
     * @param id the id of the pointOfInterestEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pointOfInterestEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/point-of-interest-entities/{id}")
    public ResponseEntity<PointOfInterestEntity> getPointOfInterestEntity(@PathVariable Long id) {
        log.debug("REST request to get PointOfInterestEntity : {}", id);
        Optional<PointOfInterestEntity> pointOfInterestEntity = pointOfInterestEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(pointOfInterestEntity);
    }

    /**
     * {@code DELETE  /point-of-interest-entities/:id} : delete the "id" pointOfInterestEntity.
     *
     * @param id the id of the pointOfInterestEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/point-of-interest-entities/{id}")
    public ResponseEntity<Void> deletePointOfInterestEntity(@PathVariable Long id) {
        log.debug("REST request to delete PointOfInterestEntity : {}", id);
        pointOfInterestEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
