package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.FortressEntity;
import com.graphzero.warmaker.repository.FortressEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.FortressEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FortressEntityResource {

    private final Logger log = LoggerFactory.getLogger(FortressEntityResource.class);

    private static final String ENTITY_NAME = "fortressEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FortressEntityRepository fortressEntityRepository;

    public FortressEntityResource(FortressEntityRepository fortressEntityRepository) {
        this.fortressEntityRepository = fortressEntityRepository;
    }

    /**
     * {@code POST  /fortress-entities} : Create a new fortressEntity.
     *
     * @param fortressEntity the fortressEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fortressEntity, or with status {@code 400 (Bad Request)} if the fortressEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fortress-entities")
    public ResponseEntity<FortressEntity> createFortressEntity(@RequestBody FortressEntity fortressEntity) throws URISyntaxException {
        log.debug("REST request to save FortressEntity : {}", fortressEntity);
        if (fortressEntity.getId() != null) {
            throw new BadRequestAlertException("A new fortressEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FortressEntity result = fortressEntityRepository.save(fortressEntity);
        return ResponseEntity.created(new URI("/api/fortress-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /fortress-entities} : Updates an existing fortressEntity.
     *
     * @param fortressEntity the fortressEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fortressEntity,
     * or with status {@code 400 (Bad Request)} if the fortressEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fortressEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/fortress-entities")
    public ResponseEntity<FortressEntity> updateFortressEntity(@RequestBody FortressEntity fortressEntity) throws URISyntaxException {
        log.debug("REST request to update FortressEntity : {}", fortressEntity);
        if (fortressEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FortressEntity result = fortressEntityRepository.save(fortressEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, fortressEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /fortress-entities} : get all the fortressEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fortressEntities in body.
     */
    @GetMapping("/fortress-entities")
    public List<FortressEntity> getAllFortressEntities() {
        log.debug("REST request to get all FortressEntities");
        return fortressEntityRepository.findAll();
    }

    /**
     * {@code GET  /fortress-entities/:id} : get the "id" fortressEntity.
     *
     * @param id the id of the fortressEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fortressEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/fortress-entities/{id}")
    public ResponseEntity<FortressEntity> getFortressEntity(@PathVariable Long id) {
        log.debug("REST request to get FortressEntity : {}", id);
        Optional<FortressEntity> fortressEntity = fortressEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fortressEntity);
    }

    /**
     * {@code DELETE  /fortress-entities/:id} : delete the "id" fortressEntity.
     *
     * @param id the id of the fortressEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/fortress-entities/{id}")
    public ResponseEntity<Void> deleteFortressEntity(@PathVariable Long id) {
        log.debug("REST request to delete FortressEntity : {}", id);
        fortressEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
