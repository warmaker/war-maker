package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.PlayerRelations;
import com.graphzero.warmaker.repository.PlayerRelationsRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.PlayerRelations}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PlayerRelationsResource {

    private final Logger log = LoggerFactory.getLogger(PlayerRelationsResource.class);

    private static final String ENTITY_NAME = "playerRelations";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PlayerRelationsRepository playerRelationsRepository;

    public PlayerRelationsResource(PlayerRelationsRepository playerRelationsRepository) {
        this.playerRelationsRepository = playerRelationsRepository;
    }

    /**
     * {@code POST  /player-relations} : Create a new playerRelations.
     *
     * @param playerRelations the playerRelations to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new playerRelations, or with status {@code 400 (Bad Request)} if the playerRelations has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/player-relations")
    public ResponseEntity<PlayerRelations> createPlayerRelations(@RequestBody PlayerRelations playerRelations) throws URISyntaxException {
        log.debug("REST request to save PlayerRelations : {}", playerRelations);
        if (playerRelations.getId() != null) {
            throw new BadRequestAlertException("A new playerRelations cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlayerRelations result = playerRelationsRepository.save(playerRelations);
        return ResponseEntity.created(new URI("/api/player-relations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /player-relations} : Updates an existing playerRelations.
     *
     * @param playerRelations the playerRelations to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated playerRelations,
     * or with status {@code 400 (Bad Request)} if the playerRelations is not valid,
     * or with status {@code 500 (Internal Server Error)} if the playerRelations couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/player-relations")
    public ResponseEntity<PlayerRelations> updatePlayerRelations(@RequestBody PlayerRelations playerRelations) throws URISyntaxException {
        log.debug("REST request to update PlayerRelations : {}", playerRelations);
        if (playerRelations.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PlayerRelations result = playerRelationsRepository.save(playerRelations);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, playerRelations.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /player-relations} : get all the playerRelations.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of playerRelations in body.
     */
    @GetMapping("/player-relations")
    public List<PlayerRelations> getAllPlayerRelations() {
        log.debug("REST request to get all PlayerRelations");
        return playerRelationsRepository.findAll();
    }

    /**
     * {@code GET  /player-relations/:id} : get the "id" playerRelations.
     *
     * @param id the id of the playerRelations to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the playerRelations, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/player-relations/{id}")
    public ResponseEntity<PlayerRelations> getPlayerRelations(@PathVariable Long id) {
        log.debug("REST request to get PlayerRelations : {}", id);
        Optional<PlayerRelations> playerRelations = playerRelationsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(playerRelations);
    }

    /**
     * {@code DELETE  /player-relations/:id} : delete the "id" playerRelations.
     *
     * @param id the id of the playerRelations to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/player-relations/{id}")
    public ResponseEntity<Void> deletePlayerRelations(@PathVariable Long id) {
        log.debug("REST request to delete PlayerRelations : {}", id);
        playerRelationsRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
