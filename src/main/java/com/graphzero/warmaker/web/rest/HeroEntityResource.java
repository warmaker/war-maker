package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.HeroEntity;
import com.graphzero.warmaker.repository.HeroEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.HeroEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class HeroEntityResource {

    private final Logger log = LoggerFactory.getLogger(HeroEntityResource.class);

    private static final String ENTITY_NAME = "heroEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HeroEntityRepository heroEntityRepository;

    public HeroEntityResource(HeroEntityRepository heroEntityRepository) {
        this.heroEntityRepository = heroEntityRepository;
    }

    /**
     * {@code POST  /hero-entities} : Create a new heroEntity.
     *
     * @param heroEntity the heroEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new heroEntity, or with status {@code 400 (Bad Request)} if the heroEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/hero-entities")
    public ResponseEntity<HeroEntity> createHeroEntity(@RequestBody HeroEntity heroEntity) throws URISyntaxException {
        log.debug("REST request to save HeroEntity : {}", heroEntity);
        if (heroEntity.getId() != null) {
            throw new BadRequestAlertException("A new heroEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HeroEntity result = heroEntityRepository.save(heroEntity);
        return ResponseEntity.created(new URI("/api/hero-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /hero-entities} : Updates an existing heroEntity.
     *
     * @param heroEntity the heroEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated heroEntity,
     * or with status {@code 400 (Bad Request)} if the heroEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the heroEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/hero-entities")
    public ResponseEntity<HeroEntity> updateHeroEntity(@RequestBody HeroEntity heroEntity) throws URISyntaxException {
        log.debug("REST request to update HeroEntity : {}", heroEntity);
        if (heroEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        HeroEntity result = heroEntityRepository.save(heroEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, heroEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /hero-entities} : get all the heroEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of heroEntities in body.
     */
    @GetMapping("/hero-entities")
    public List<HeroEntity> getAllHeroEntities() {
        log.debug("REST request to get all HeroEntities");
        return heroEntityRepository.findAll();
    }

    /**
     * {@code GET  /hero-entities/:id} : get the "id" heroEntity.
     *
     * @param id the id of the heroEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the heroEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/hero-entities/{id}")
    public ResponseEntity<HeroEntity> getHeroEntity(@PathVariable Long id) {
        log.debug("REST request to get HeroEntity : {}", id);
        Optional<HeroEntity> heroEntity = heroEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(heroEntity);
    }

    /**
     * {@code DELETE  /hero-entities/:id} : delete the "id" heroEntity.
     *
     * @param id the id of the heroEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/hero-entities/{id}")
    public ResponseEntity<Void> deleteHeroEntity(@PathVariable Long id) {
        log.debug("REST request to delete HeroEntity : {}", id);
        heroEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
