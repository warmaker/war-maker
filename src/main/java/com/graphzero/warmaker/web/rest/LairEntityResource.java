package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.LairEntity;
import com.graphzero.warmaker.repository.LairEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.LairEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class LairEntityResource {

    private final Logger log = LoggerFactory.getLogger(LairEntityResource.class);

    private static final String ENTITY_NAME = "lairEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LairEntityRepository lairEntityRepository;

    public LairEntityResource(LairEntityRepository lairEntityRepository) {
        this.lairEntityRepository = lairEntityRepository;
    }

    /**
     * {@code POST  /lair-entities} : Create a new lairEntity.
     *
     * @param lairEntity the lairEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new lairEntity, or with status {@code 400 (Bad Request)} if the lairEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/lair-entities")
    public ResponseEntity<LairEntity> createLairEntity(@Valid @RequestBody LairEntity lairEntity) throws URISyntaxException {
        log.debug("REST request to save LairEntity : {}", lairEntity);
        if (lairEntity.getId() != null) {
            throw new BadRequestAlertException("A new lairEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LairEntity result = lairEntityRepository.save(lairEntity);
        return ResponseEntity.created(new URI("/api/lair-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /lair-entities} : Updates an existing lairEntity.
     *
     * @param lairEntity the lairEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated lairEntity,
     * or with status {@code 400 (Bad Request)} if the lairEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the lairEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/lair-entities")
    public ResponseEntity<LairEntity> updateLairEntity(@Valid @RequestBody LairEntity lairEntity) throws URISyntaxException {
        log.debug("REST request to update LairEntity : {}", lairEntity);
        if (lairEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LairEntity result = lairEntityRepository.save(lairEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, lairEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /lair-entities} : get all the lairEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of lairEntities in body.
     */
    @GetMapping("/lair-entities")
    public List<LairEntity> getAllLairEntities() {
        log.debug("REST request to get all LairEntities");
        return lairEntityRepository.findAll();
    }

    /**
     * {@code GET  /lair-entities/:id} : get the "id" lairEntity.
     *
     * @param id the id of the lairEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the lairEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/lair-entities/{id}")
    public ResponseEntity<LairEntity> getLairEntity(@PathVariable Long id) {
        log.debug("REST request to get LairEntity : {}", id);
        Optional<LairEntity> lairEntity = lairEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(lairEntity);
    }

    /**
     * {@code DELETE  /lair-entities/:id} : delete the "id" lairEntity.
     *
     * @param id the id of the lairEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/lair-entities/{id}")
    public ResponseEntity<Void> deleteLairEntity(@PathVariable Long id) {
        log.debug("REST request to delete LairEntity : {}", id);
        lairEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
