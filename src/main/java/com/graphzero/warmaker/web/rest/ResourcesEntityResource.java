package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.ResourcesEntity;
import com.graphzero.warmaker.repository.ResourcesEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.ResourcesEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ResourcesEntityResource {

    private final Logger log = LoggerFactory.getLogger(ResourcesEntityResource.class);

    private static final String ENTITY_NAME = "resourcesEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ResourcesEntityRepository resourcesEntityRepository;

    public ResourcesEntityResource(ResourcesEntityRepository resourcesEntityRepository) {
        this.resourcesEntityRepository = resourcesEntityRepository;
    }

    /**
     * {@code POST  /resources-entities} : Create a new resourcesEntity.
     *
     * @param resourcesEntity the resourcesEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new resourcesEntity, or with status {@code 400 (Bad Request)} if the resourcesEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/resources-entities")
    public ResponseEntity<ResourcesEntity> createResourcesEntity(@RequestBody ResourcesEntity resourcesEntity) throws URISyntaxException {
        log.debug("REST request to save ResourcesEntity : {}", resourcesEntity);
        if (resourcesEntity.getId() != null) {
            throw new BadRequestAlertException("A new resourcesEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ResourcesEntity result = resourcesEntityRepository.save(resourcesEntity);
        return ResponseEntity.created(new URI("/api/resources-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /resources-entities} : Updates an existing resourcesEntity.
     *
     * @param resourcesEntity the resourcesEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated resourcesEntity,
     * or with status {@code 400 (Bad Request)} if the resourcesEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the resourcesEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/resources-entities")
    public ResponseEntity<ResourcesEntity> updateResourcesEntity(@RequestBody ResourcesEntity resourcesEntity) throws URISyntaxException {
        log.debug("REST request to update ResourcesEntity : {}", resourcesEntity);
        if (resourcesEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ResourcesEntity result = resourcesEntityRepository.save(resourcesEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, resourcesEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /resources-entities} : get all the resourcesEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of resourcesEntities in body.
     */
    @GetMapping("/resources-entities")
    public List<ResourcesEntity> getAllResourcesEntities() {
        log.debug("REST request to get all ResourcesEntities");
        return resourcesEntityRepository.findAll();
    }

    /**
     * {@code GET  /resources-entities/:id} : get the "id" resourcesEntity.
     *
     * @param id the id of the resourcesEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the resourcesEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/resources-entities/{id}")
    public ResponseEntity<ResourcesEntity> getResourcesEntity(@PathVariable Long id) {
        log.debug("REST request to get ResourcesEntity : {}", id);
        Optional<ResourcesEntity> resourcesEntity = resourcesEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(resourcesEntity);
    }

    /**
     * {@code DELETE  /resources-entities/:id} : delete the "id" resourcesEntity.
     *
     * @param id the id of the resourcesEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/resources-entities/{id}")
    public ResponseEntity<Void> deleteResourcesEntity(@PathVariable Long id) {
        log.debug("REST request to delete ResourcesEntity : {}", id);
        resourcesEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
