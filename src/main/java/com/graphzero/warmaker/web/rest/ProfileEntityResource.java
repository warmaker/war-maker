package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.ProfileEntity;
import com.graphzero.warmaker.repository.ProfileEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.ProfileEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ProfileEntityResource {

    private final Logger log = LoggerFactory.getLogger(ProfileEntityResource.class);

    private static final String ENTITY_NAME = "profileEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProfileEntityRepository profileEntityRepository;

    public ProfileEntityResource(ProfileEntityRepository profileEntityRepository) {
        this.profileEntityRepository = profileEntityRepository;
    }

    /**
     * {@code POST  /profile-entities} : Create a new profileEntity.
     *
     * @param profileEntity the profileEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new profileEntity, or with status {@code 400 (Bad Request)} if the profileEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/profile-entities")
    public ResponseEntity<ProfileEntity> createProfileEntity(@Valid @RequestBody ProfileEntity profileEntity) throws URISyntaxException {
        log.debug("REST request to save ProfileEntity : {}", profileEntity);
        if (profileEntity.getId() != null) {
            throw new BadRequestAlertException("A new profileEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProfileEntity result = profileEntityRepository.save(profileEntity);
        return ResponseEntity.created(new URI("/api/profile-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /profile-entities} : Updates an existing profileEntity.
     *
     * @param profileEntity the profileEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated profileEntity,
     * or with status {@code 400 (Bad Request)} if the profileEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the profileEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/profile-entities")
    public ResponseEntity<ProfileEntity> updateProfileEntity(@Valid @RequestBody ProfileEntity profileEntity) throws URISyntaxException {
        log.debug("REST request to update ProfileEntity : {}", profileEntity);
        if (profileEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProfileEntity result = profileEntityRepository.save(profileEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, profileEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /profile-entities} : get all the profileEntities.
     *
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of profileEntities in body.
     */
    @GetMapping("/profile-entities")
    public List<ProfileEntity> getAllProfileEntities(@RequestParam(required = false) String filter) {
        if ("clan-is-null".equals(filter)) {
            log.debug("REST request to get all ProfileEntitys where clan is null");
            return StreamSupport
                .stream(profileEntityRepository.findAll().spliterator(), false)
                .filter(profileEntity -> profileEntity.getClan() == null)
                .collect(Collectors.toList());
        }
        log.debug("REST request to get all ProfileEntities");
        return profileEntityRepository.findAll();
    }

    /**
     * {@code GET  /profile-entities/:id} : get the "id" profileEntity.
     *
     * @param id the id of the profileEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the profileEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/profile-entities/{id}")
    public ResponseEntity<ProfileEntity> getProfileEntity(@PathVariable Long id) {
        log.debug("REST request to get ProfileEntity : {}", id);
        Optional<ProfileEntity> profileEntity = profileEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(profileEntity);
    }

    /**
     * {@code DELETE  /profile-entities/:id} : delete the "id" profileEntity.
     *
     * @param id the id of the profileEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/profile-entities/{id}")
    public ResponseEntity<Void> deleteProfileEntity(@PathVariable Long id) {
        log.debug("REST request to delete ProfileEntity : {}", id);
        profileEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
