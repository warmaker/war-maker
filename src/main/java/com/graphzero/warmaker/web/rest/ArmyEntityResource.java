package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.ArmyEntity;
import com.graphzero.warmaker.repository.ArmyEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.ArmyEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ArmyEntityResource {

    private final Logger log = LoggerFactory.getLogger(ArmyEntityResource.class);

    private static final String ENTITY_NAME = "armyEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ArmyEntityRepository armyEntityRepository;

    public ArmyEntityResource(ArmyEntityRepository armyEntityRepository) {
        this.armyEntityRepository = armyEntityRepository;
    }

    /**
     * {@code POST  /army-entities} : Create a new armyEntity.
     *
     * @param armyEntity the armyEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new armyEntity, or with status {@code 400 (Bad Request)} if the armyEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/army-entities")
    public ResponseEntity<ArmyEntity> createArmyEntity(@RequestBody ArmyEntity armyEntity) throws URISyntaxException {
        log.debug("REST request to save ArmyEntity : {}", armyEntity);
        if (armyEntity.getId() != null) {
            throw new BadRequestAlertException("A new armyEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ArmyEntity result = armyEntityRepository.save(armyEntity);
        return ResponseEntity.created(new URI("/api/army-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /army-entities} : Updates an existing armyEntity.
     *
     * @param armyEntity the armyEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated armyEntity,
     * or with status {@code 400 (Bad Request)} if the armyEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the armyEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/army-entities")
    public ResponseEntity<ArmyEntity> updateArmyEntity(@RequestBody ArmyEntity armyEntity) throws URISyntaxException {
        log.debug("REST request to update ArmyEntity : {}", armyEntity);
        if (armyEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ArmyEntity result = armyEntityRepository.save(armyEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, armyEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /army-entities} : get all the armyEntities.
     *
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of armyEntities in body.
     */
    @GetMapping("/army-entities")
    public List<ArmyEntity> getAllArmyEntities(@RequestParam(required = false) String filter) {
        if ("city-is-null".equals(filter)) {
            log.debug("REST request to get all ArmyEntitys where city is null");
            return StreamSupport
                .stream(armyEntityRepository.findAll().spliterator(), false)
                .filter(armyEntity -> armyEntity.getCity() == null)
                .collect(Collectors.toList());
        }
        log.debug("REST request to get all ArmyEntities");
        return armyEntityRepository.findAll();
    }

    /**
     * {@code GET  /army-entities/:id} : get the "id" armyEntity.
     *
     * @param id the id of the armyEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the armyEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/army-entities/{id}")
    public ResponseEntity<ArmyEntity> getArmyEntity(@PathVariable Long id) {
        log.debug("REST request to get ArmyEntity : {}", id);
        Optional<ArmyEntity> armyEntity = armyEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(armyEntity);
    }

    /**
     * {@code DELETE  /army-entities/:id} : delete the "id" armyEntity.
     *
     * @param id the id of the armyEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/army-entities/{id}")
    public ResponseEntity<Void> deleteArmyEntity(@PathVariable Long id) {
        log.debug("REST request to delete ArmyEntity : {}", id);
        armyEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
