/**
 * View Models used by Spring MVC REST controllers.
 */
package com.graphzero.warmaker.web.rest.vm;
