package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.FieldEntity;
import com.graphzero.warmaker.repository.FieldEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.FieldEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FieldEntityResource {

    private final Logger log = LoggerFactory.getLogger(FieldEntityResource.class);

    private static final String ENTITY_NAME = "fieldEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FieldEntityRepository fieldEntityRepository;

    public FieldEntityResource(FieldEntityRepository fieldEntityRepository) {
        this.fieldEntityRepository = fieldEntityRepository;
    }

    /**
     * {@code POST  /field-entities} : Create a new fieldEntity.
     *
     * @param fieldEntity the fieldEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fieldEntity, or with status {@code 400 (Bad Request)} if the fieldEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/field-entities")
    public ResponseEntity<FieldEntity> createFieldEntity(@Valid @RequestBody FieldEntity fieldEntity) throws URISyntaxException {
        log.debug("REST request to save FieldEntity : {}", fieldEntity);
        if (fieldEntity.getId() != null) {
            throw new BadRequestAlertException("A new fieldEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FieldEntity result = fieldEntityRepository.save(fieldEntity);
        return ResponseEntity.created(new URI("/api/field-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /field-entities} : Updates an existing fieldEntity.
     *
     * @param fieldEntity the fieldEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fieldEntity,
     * or with status {@code 400 (Bad Request)} if the fieldEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fieldEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/field-entities")
    public ResponseEntity<FieldEntity> updateFieldEntity(@Valid @RequestBody FieldEntity fieldEntity) throws URISyntaxException {
        log.debug("REST request to update FieldEntity : {}", fieldEntity);
        if (fieldEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FieldEntity result = fieldEntityRepository.save(fieldEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, fieldEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /field-entities} : get all the fieldEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fieldEntities in body.
     */
    @GetMapping("/field-entities")
    public List<FieldEntity> getAllFieldEntities() {
        log.debug("REST request to get all FieldEntities");
        return fieldEntityRepository.findAll();
    }

    /**
     * {@code GET  /field-entities/:id} : get the "id" fieldEntity.
     *
     * @param id the id of the fieldEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fieldEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/field-entities/{id}")
    public ResponseEntity<FieldEntity> getFieldEntity(@PathVariable Long id) {
        log.debug("REST request to get FieldEntity : {}", id);
        Optional<FieldEntity> fieldEntity = fieldEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fieldEntity);
    }

    /**
     * {@code DELETE  /field-entities/:id} : delete the "id" fieldEntity.
     *
     * @param id the id of the fieldEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/field-entities/{id}")
    public ResponseEntity<Void> deleteFieldEntity(@PathVariable Long id) {
        log.debug("REST request to delete FieldEntity : {}", id);
        fieldEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
