package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.AllianceEntity;
import com.graphzero.warmaker.repository.AllianceEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.AllianceEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AllianceEntityResource {

    private final Logger log = LoggerFactory.getLogger(AllianceEntityResource.class);

    private static final String ENTITY_NAME = "allianceEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AllianceEntityRepository allianceEntityRepository;

    public AllianceEntityResource(AllianceEntityRepository allianceEntityRepository) {
        this.allianceEntityRepository = allianceEntityRepository;
    }

    /**
     * {@code POST  /alliance-entities} : Create a new allianceEntity.
     *
     * @param allianceEntity the allianceEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new allianceEntity, or with status {@code 400 (Bad Request)} if the allianceEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/alliance-entities")
    public ResponseEntity<AllianceEntity> createAllianceEntity(@RequestBody AllianceEntity allianceEntity) throws URISyntaxException {
        log.debug("REST request to save AllianceEntity : {}", allianceEntity);
        if (allianceEntity.getId() != null) {
            throw new BadRequestAlertException("A new allianceEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AllianceEntity result = allianceEntityRepository.save(allianceEntity);
        return ResponseEntity.created(new URI("/api/alliance-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /alliance-entities} : Updates an existing allianceEntity.
     *
     * @param allianceEntity the allianceEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated allianceEntity,
     * or with status {@code 400 (Bad Request)} if the allianceEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the allianceEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/alliance-entities")
    public ResponseEntity<AllianceEntity> updateAllianceEntity(@RequestBody AllianceEntity allianceEntity) throws URISyntaxException {
        log.debug("REST request to update AllianceEntity : {}", allianceEntity);
        if (allianceEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AllianceEntity result = allianceEntityRepository.save(allianceEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, allianceEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /alliance-entities} : get all the allianceEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of allianceEntities in body.
     */
    @GetMapping("/alliance-entities")
    public List<AllianceEntity> getAllAllianceEntities() {
        log.debug("REST request to get all AllianceEntities");
        return allianceEntityRepository.findAll();
    }

    /**
     * {@code GET  /alliance-entities/:id} : get the "id" allianceEntity.
     *
     * @param id the id of the allianceEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the allianceEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/alliance-entities/{id}")
    public ResponseEntity<AllianceEntity> getAllianceEntity(@PathVariable Long id) {
        log.debug("REST request to get AllianceEntity : {}", id);
        Optional<AllianceEntity> allianceEntity = allianceEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(allianceEntity);
    }

    /**
     * {@code DELETE  /alliance-entities/:id} : delete the "id" allianceEntity.
     *
     * @param id the id of the allianceEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/alliance-entities/{id}")
    public ResponseEntity<Void> deleteAllianceEntity(@PathVariable Long id) {
        log.debug("REST request to delete AllianceEntity : {}", id);
        allianceEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
