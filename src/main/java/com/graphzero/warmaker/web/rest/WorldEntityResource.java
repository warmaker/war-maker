package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.WorldEntity;
import com.graphzero.warmaker.repository.WorldEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.WorldEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class WorldEntityResource {

    private final Logger log = LoggerFactory.getLogger(WorldEntityResource.class);

    private static final String ENTITY_NAME = "worldEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WorldEntityRepository worldEntityRepository;

    public WorldEntityResource(WorldEntityRepository worldEntityRepository) {
        this.worldEntityRepository = worldEntityRepository;
    }

    /**
     * {@code POST  /world-entities} : Create a new worldEntity.
     *
     * @param worldEntity the worldEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new worldEntity, or with status {@code 400 (Bad Request)} if the worldEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/world-entities")
    public ResponseEntity<WorldEntity> createWorldEntity(@Valid @RequestBody WorldEntity worldEntity) throws URISyntaxException {
        log.debug("REST request to save WorldEntity : {}", worldEntity);
        if (worldEntity.getId() != null) {
            throw new BadRequestAlertException("A new worldEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WorldEntity result = worldEntityRepository.save(worldEntity);
        return ResponseEntity.created(new URI("/api/world-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /world-entities} : Updates an existing worldEntity.
     *
     * @param worldEntity the worldEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated worldEntity,
     * or with status {@code 400 (Bad Request)} if the worldEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the worldEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/world-entities")
    public ResponseEntity<WorldEntity> updateWorldEntity(@Valid @RequestBody WorldEntity worldEntity) throws URISyntaxException {
        log.debug("REST request to update WorldEntity : {}", worldEntity);
        if (worldEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WorldEntity result = worldEntityRepository.save(worldEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, worldEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /world-entities} : get all the worldEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of worldEntities in body.
     */
    @GetMapping("/world-entities")
    public List<WorldEntity> getAllWorldEntities() {
        log.debug("REST request to get all WorldEntities");
        return worldEntityRepository.findAll();
    }

    /**
     * {@code GET  /world-entities/:id} : get the "id" worldEntity.
     *
     * @param id the id of the worldEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the worldEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/world-entities/{id}")
    public ResponseEntity<WorldEntity> getWorldEntity(@PathVariable Long id) {
        log.debug("REST request to get WorldEntity : {}", id);
        Optional<WorldEntity> worldEntity = worldEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(worldEntity);
    }

    /**
     * {@code DELETE  /world-entities/:id} : delete the "id" worldEntity.
     *
     * @param id the id of the worldEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/world-entities/{id}")
    public ResponseEntity<Void> deleteWorldEntity(@PathVariable Long id) {
        log.debug("REST request to delete WorldEntity : {}", id);
        worldEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
