package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.CityEntity;
import com.graphzero.warmaker.repository.CityEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.CityEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CityEntityResource {

    private final Logger log = LoggerFactory.getLogger(CityEntityResource.class);

    private static final String ENTITY_NAME = "cityEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CityEntityRepository cityEntityRepository;

    public CityEntityResource(CityEntityRepository cityEntityRepository) {
        this.cityEntityRepository = cityEntityRepository;
    }

    /**
     * {@code POST  /city-entities} : Create a new cityEntity.
     *
     * @param cityEntity the cityEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cityEntity, or with status {@code 400 (Bad Request)} if the cityEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/city-entities")
    public ResponseEntity<CityEntity> createCityEntity(@Valid @RequestBody CityEntity cityEntity) throws URISyntaxException {
        log.debug("REST request to save CityEntity : {}", cityEntity);
        if (cityEntity.getId() != null) {
            throw new BadRequestAlertException("A new cityEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CityEntity result = cityEntityRepository.save(cityEntity);
        return ResponseEntity.created(new URI("/api/city-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /city-entities} : Updates an existing cityEntity.
     *
     * @param cityEntity the cityEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cityEntity,
     * or with status {@code 400 (Bad Request)} if the cityEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cityEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/city-entities")
    public ResponseEntity<CityEntity> updateCityEntity(@Valid @RequestBody CityEntity cityEntity) throws URISyntaxException {
        log.debug("REST request to update CityEntity : {}", cityEntity);
        if (cityEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CityEntity result = cityEntityRepository.save(cityEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cityEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /city-entities} : get all the cityEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cityEntities in body.
     */
    @GetMapping("/city-entities")
    public List<CityEntity> getAllCityEntities() {
        log.debug("REST request to get all CityEntities");
        return cityEntityRepository.findAll();
    }

    /**
     * {@code GET  /city-entities/:id} : get the "id" cityEntity.
     *
     * @param id the id of the cityEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cityEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/city-entities/{id}")
    public ResponseEntity<CityEntity> getCityEntity(@PathVariable Long id) {
        log.debug("REST request to get CityEntity : {}", id);
        Optional<CityEntity> cityEntity = cityEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(cityEntity);
    }

    /**
     * {@code DELETE  /city-entities/:id} : delete the "id" cityEntity.
     *
     * @param id the id of the cityEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/city-entities/{id}")
    public ResponseEntity<Void> deleteCityEntity(@PathVariable Long id) {
        log.debug("REST request to delete CityEntity : {}", id);
        cityEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
