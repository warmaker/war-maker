package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.CityBuildingsEntity;
import com.graphzero.warmaker.repository.CityBuildingsEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.CityBuildingsEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CityBuildingsEntityResource {

    private final Logger log = LoggerFactory.getLogger(CityBuildingsEntityResource.class);

    private static final String ENTITY_NAME = "cityBuildingsEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CityBuildingsEntityRepository cityBuildingsEntityRepository;

    public CityBuildingsEntityResource(CityBuildingsEntityRepository cityBuildingsEntityRepository) {
        this.cityBuildingsEntityRepository = cityBuildingsEntityRepository;
    }

    /**
     * {@code POST  /city-buildings-entities} : Create a new cityBuildingsEntity.
     *
     * @param cityBuildingsEntity the cityBuildingsEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cityBuildingsEntity, or with status {@code 400 (Bad Request)} if the cityBuildingsEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/city-buildings-entities")
    public ResponseEntity<CityBuildingsEntity> createCityBuildingsEntity(@RequestBody CityBuildingsEntity cityBuildingsEntity) throws URISyntaxException {
        log.debug("REST request to save CityBuildingsEntity : {}", cityBuildingsEntity);
        if (cityBuildingsEntity.getId() != null) {
            throw new BadRequestAlertException("A new cityBuildingsEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CityBuildingsEntity result = cityBuildingsEntityRepository.save(cityBuildingsEntity);
        return ResponseEntity.created(new URI("/api/city-buildings-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /city-buildings-entities} : Updates an existing cityBuildingsEntity.
     *
     * @param cityBuildingsEntity the cityBuildingsEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cityBuildingsEntity,
     * or with status {@code 400 (Bad Request)} if the cityBuildingsEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cityBuildingsEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/city-buildings-entities")
    public ResponseEntity<CityBuildingsEntity> updateCityBuildingsEntity(@RequestBody CityBuildingsEntity cityBuildingsEntity) throws URISyntaxException {
        log.debug("REST request to update CityBuildingsEntity : {}", cityBuildingsEntity);
        if (cityBuildingsEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CityBuildingsEntity result = cityBuildingsEntityRepository.save(cityBuildingsEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cityBuildingsEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /city-buildings-entities} : get all the cityBuildingsEntities.
     *
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cityBuildingsEntities in body.
     */
    @GetMapping("/city-buildings-entities")
    public List<CityBuildingsEntity> getAllCityBuildingsEntities(@RequestParam(required = false) String filter) {
        if ("city-is-null".equals(filter)) {
            log.debug("REST request to get all CityBuildingsEntitys where city is null");
            return StreamSupport
                .stream(cityBuildingsEntityRepository.findAll().spliterator(), false)
                .filter(cityBuildingsEntity -> cityBuildingsEntity.getCity() == null)
                .collect(Collectors.toList());
        }
        log.debug("REST request to get all CityBuildingsEntities");
        return cityBuildingsEntityRepository.findAll();
    }

    /**
     * {@code GET  /city-buildings-entities/:id} : get the "id" cityBuildingsEntity.
     *
     * @param id the id of the cityBuildingsEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cityBuildingsEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/city-buildings-entities/{id}")
    public ResponseEntity<CityBuildingsEntity> getCityBuildingsEntity(@PathVariable Long id) {
        log.debug("REST request to get CityBuildingsEntity : {}", id);
        Optional<CityBuildingsEntity> cityBuildingsEntity = cityBuildingsEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(cityBuildingsEntity);
    }

    /**
     * {@code DELETE  /city-buildings-entities/:id} : delete the "id" cityBuildingsEntity.
     *
     * @param id the id of the cityBuildingsEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/city-buildings-entities/{id}")
    public ResponseEntity<Void> deleteCityBuildingsEntity(@PathVariable Long id) {
        log.debug("REST request to delete CityBuildingsEntity : {}", id);
        cityBuildingsEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
