package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.WarPartyEntity;
import com.graphzero.warmaker.repository.WarPartyEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.WarPartyEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class WarPartyEntityResource {

    private final Logger log = LoggerFactory.getLogger(WarPartyEntityResource.class);

    private static final String ENTITY_NAME = "warPartyEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WarPartyEntityRepository warPartyEntityRepository;

    public WarPartyEntityResource(WarPartyEntityRepository warPartyEntityRepository) {
        this.warPartyEntityRepository = warPartyEntityRepository;
    }

    /**
     * {@code POST  /war-party-entities} : Create a new warPartyEntity.
     *
     * @param warPartyEntity the warPartyEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new warPartyEntity, or with status {@code 400 (Bad Request)} if the warPartyEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/war-party-entities")
    public ResponseEntity<WarPartyEntity> createWarPartyEntity(@Valid @RequestBody WarPartyEntity warPartyEntity) throws URISyntaxException {
        log.debug("REST request to save WarPartyEntity : {}", warPartyEntity);
        if (warPartyEntity.getId() != null) {
            throw new BadRequestAlertException("A new warPartyEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WarPartyEntity result = warPartyEntityRepository.save(warPartyEntity);
        return ResponseEntity.created(new URI("/api/war-party-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /war-party-entities} : Updates an existing warPartyEntity.
     *
     * @param warPartyEntity the warPartyEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated warPartyEntity,
     * or with status {@code 400 (Bad Request)} if the warPartyEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the warPartyEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/war-party-entities")
    public ResponseEntity<WarPartyEntity> updateWarPartyEntity(@Valid @RequestBody WarPartyEntity warPartyEntity) throws URISyntaxException {
        log.debug("REST request to update WarPartyEntity : {}", warPartyEntity);
        if (warPartyEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WarPartyEntity result = warPartyEntityRepository.save(warPartyEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, warPartyEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /war-party-entities} : get all the warPartyEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of warPartyEntities in body.
     */
    @GetMapping("/war-party-entities")
    public List<WarPartyEntity> getAllWarPartyEntities() {
        log.debug("REST request to get all WarPartyEntities");
        return warPartyEntityRepository.findAll();
    }

    /**
     * {@code GET  /war-party-entities/:id} : get the "id" warPartyEntity.
     *
     * @param id the id of the warPartyEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the warPartyEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/war-party-entities/{id}")
    public ResponseEntity<WarPartyEntity> getWarPartyEntity(@PathVariable Long id) {
        log.debug("REST request to get WarPartyEntity : {}", id);
        Optional<WarPartyEntity> warPartyEntity = warPartyEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(warPartyEntity);
    }

    /**
     * {@code DELETE  /war-party-entities/:id} : delete the "id" warPartyEntity.
     *
     * @param id the id of the warPartyEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/war-party-entities/{id}")
    public ResponseEntity<Void> deleteWarPartyEntity(@PathVariable Long id) {
        log.debug("REST request to delete WarPartyEntity : {}", id);
        warPartyEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
