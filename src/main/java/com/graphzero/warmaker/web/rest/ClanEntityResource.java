package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.ClanEntity;
import com.graphzero.warmaker.repository.ClanEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.ClanEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ClanEntityResource {

    private final Logger log = LoggerFactory.getLogger(ClanEntityResource.class);

    private static final String ENTITY_NAME = "clanEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClanEntityRepository clanEntityRepository;

    public ClanEntityResource(ClanEntityRepository clanEntityRepository) {
        this.clanEntityRepository = clanEntityRepository;
    }

    /**
     * {@code POST  /clan-entities} : Create a new clanEntity.
     *
     * @param clanEntity the clanEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clanEntity, or with status {@code 400 (Bad Request)} if the clanEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/clan-entities")
    public ResponseEntity<ClanEntity> createClanEntity(@RequestBody ClanEntity clanEntity) throws URISyntaxException {
        log.debug("REST request to save ClanEntity : {}", clanEntity);
        if (clanEntity.getId() != null) {
            throw new BadRequestAlertException("A new clanEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClanEntity result = clanEntityRepository.save(clanEntity);
        return ResponseEntity.created(new URI("/api/clan-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /clan-entities} : Updates an existing clanEntity.
     *
     * @param clanEntity the clanEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clanEntity,
     * or with status {@code 400 (Bad Request)} if the clanEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the clanEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/clan-entities")
    public ResponseEntity<ClanEntity> updateClanEntity(@RequestBody ClanEntity clanEntity) throws URISyntaxException {
        log.debug("REST request to update ClanEntity : {}", clanEntity);
        if (clanEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ClanEntity result = clanEntityRepository.save(clanEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, clanEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /clan-entities} : get all the clanEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of clanEntities in body.
     */
    @GetMapping("/clan-entities")
    public List<ClanEntity> getAllClanEntities() {
        log.debug("REST request to get all ClanEntities");
        return clanEntityRepository.findAll();
    }

    /**
     * {@code GET  /clan-entities/:id} : get the "id" clanEntity.
     *
     * @param id the id of the clanEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clanEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/clan-entities/{id}")
    public ResponseEntity<ClanEntity> getClanEntity(@PathVariable Long id) {
        log.debug("REST request to get ClanEntity : {}", id);
        Optional<ClanEntity> clanEntity = clanEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(clanEntity);
    }

    /**
     * {@code DELETE  /clan-entities/:id} : delete the "id" clanEntity.
     *
     * @param id the id of the clanEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/clan-entities/{id}")
    public ResponseEntity<Void> deleteClanEntity(@PathVariable Long id) {
        log.debug("REST request to delete ClanEntity : {}", id);
        clanEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
