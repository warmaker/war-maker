package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.domain.UnitsEntity;
import com.graphzero.warmaker.repository.UnitsEntityRepository;
import com.graphzero.warmaker.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.graphzero.warmaker.domain.UnitsEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class UnitsEntityResource {

    private final Logger log = LoggerFactory.getLogger(UnitsEntityResource.class);

    private static final String ENTITY_NAME = "unitsEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UnitsEntityRepository unitsEntityRepository;

    public UnitsEntityResource(UnitsEntityRepository unitsEntityRepository) {
        this.unitsEntityRepository = unitsEntityRepository;
    }

    /**
     * {@code POST  /units-entities} : Create a new unitsEntity.
     *
     * @param unitsEntity the unitsEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new unitsEntity, or with status {@code 400 (Bad Request)} if the unitsEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/units-entities")
    public ResponseEntity<UnitsEntity> createUnitsEntity(@RequestBody UnitsEntity unitsEntity) throws URISyntaxException {
        log.debug("REST request to save UnitsEntity : {}", unitsEntity);
        if (unitsEntity.getId() != null) {
            throw new BadRequestAlertException("A new unitsEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UnitsEntity result = unitsEntityRepository.save(unitsEntity);
        return ResponseEntity.created(new URI("/api/units-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /units-entities} : Updates an existing unitsEntity.
     *
     * @param unitsEntity the unitsEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated unitsEntity,
     * or with status {@code 400 (Bad Request)} if the unitsEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the unitsEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/units-entities")
    public ResponseEntity<UnitsEntity> updateUnitsEntity(@RequestBody UnitsEntity unitsEntity) throws URISyntaxException {
        log.debug("REST request to update UnitsEntity : {}", unitsEntity);
        if (unitsEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UnitsEntity result = unitsEntityRepository.save(unitsEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, unitsEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /units-entities} : get all the unitsEntities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of unitsEntities in body.
     */
    @GetMapping("/units-entities")
    public List<UnitsEntity> getAllUnitsEntities() {
        log.debug("REST request to get all UnitsEntities");
        return unitsEntityRepository.findAll();
    }

    /**
     * {@code GET  /units-entities/:id} : get the "id" unitsEntity.
     *
     * @param id the id of the unitsEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the unitsEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/units-entities/{id}")
    public ResponseEntity<UnitsEntity> getUnitsEntity(@PathVariable Long id) {
        log.debug("REST request to get UnitsEntity : {}", id);
        Optional<UnitsEntity> unitsEntity = unitsEntityRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(unitsEntity);
    }

    /**
     * {@code DELETE  /units-entities/:id} : delete the "id" unitsEntity.
     *
     * @param id the id of the unitsEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/units-entities/{id}")
    public ResponseEntity<Void> deleteUnitsEntity(@PathVariable Long id) {
        log.debug("REST request to delete UnitsEntity : {}", id);
        unitsEntityRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
