package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.HeroEntity;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the HeroEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HeroEntityRepository extends JpaRepository<HeroEntity, Long> {
}
