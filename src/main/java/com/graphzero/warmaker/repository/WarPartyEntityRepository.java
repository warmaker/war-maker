package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.FieldEntity;
import com.graphzero.warmaker.domain.ProfileEntity;
import com.graphzero.warmaker.domain.WarPartyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the WarPartyEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WarPartyEntityRepository extends JpaRepository<WarPartyEntity, Long> {
    @Query("SELECT o" +
        " FROM WarPartyEntity wp " +
        "    JOIN wp.armyEntity a " +
        "    JOIN a.city c " +
        "    JOIN c.owner o " +
        "    LEFT JOIN FETCH o.relations " +
        " WHERE wp.id = :warPartyId")
    Optional<ProfileEntity> getOwner(@Param("warPartyId") long warPartyId);

    List<WarPartyEntity> findByPosition(FieldEntity position);
}
