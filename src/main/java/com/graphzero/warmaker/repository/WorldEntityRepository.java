package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.WorldEntity;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the WorldEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WorldEntityRepository extends JpaRepository<WorldEntity, Long> {
}
