package com.graphzero.warmaker.repository;
import com.graphzero.warmaker.domain.LandEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the LandEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LandEntityRepository extends JpaRepository<LandEntity, Long> {
    Optional<LandEntity> findByLocationId(long fieldId);
}
