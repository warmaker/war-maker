package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.CityBuildingsEntity;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CityBuildingsEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CityBuildingsEntityRepository extends JpaRepository<CityBuildingsEntity, Long> {
}
