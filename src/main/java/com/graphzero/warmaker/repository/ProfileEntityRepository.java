package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.ProfileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the ProfileEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProfileEntityRepository extends JpaRepository<ProfileEntity, Long> {

    @Query("select profile_entity from ProfileEntity profile_entity where profile_entity.user.login = ?#{principal" +
            ".username}")
    List<ProfileEntity> findByUserIsCurrentUser();

    Optional<ProfileEntity> findByAccountName(String accountName);
}
