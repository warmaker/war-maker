package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.ResourcesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the ResourcesEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResourcesEntityRepository extends JpaRepository<ResourcesEntity, Long> {

    @Query(
        "SELECT re " +
        "FROM WarPartyEntity wpe " +
        "   JOIN wpe.resourcesCarried re " +
        "WHERE wpe.id = :warPartyId ")
    Optional<ResourcesEntity> findByWarPartyId(@Param("warPartyId") long warPartyId);
}
