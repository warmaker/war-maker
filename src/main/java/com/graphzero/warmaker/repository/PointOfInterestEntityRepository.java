package com.graphzero.warmaker.repository;
import com.graphzero.warmaker.domain.PointOfInterestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the PointOfInterestEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PointOfInterestEntityRepository extends JpaRepository<PointOfInterestEntity, Long> {
    Optional<PointOfInterestEntity> findByLocationId(long locationId);
}
