package com.graphzero.warmaker.repository;
import com.graphzero.warmaker.domain.LairEntity;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the LairEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LairEntityRepository extends JpaRepository<LairEntity, Long> {
    Optional<LairEntity> findByLocationId(long locationId);
}
