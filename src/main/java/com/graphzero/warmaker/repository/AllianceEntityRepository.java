package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.AllianceEntity;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AllianceEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AllianceEntityRepository extends JpaRepository<AllianceEntity, Long> {
}
