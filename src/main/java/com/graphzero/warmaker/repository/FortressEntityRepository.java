package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.FortressEntity;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FortressEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FortressEntityRepository extends JpaRepository<FortressEntity, Long> {
}
