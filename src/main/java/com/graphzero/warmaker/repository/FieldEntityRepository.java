package com.graphzero.warmaker.repository;
import com.graphzero.warmaker.domain.FieldEntity;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the FieldEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FieldEntityRepository extends JpaRepository<FieldEntity, Long> {
    @Query("SELECT f" +
        "   FROM FieldEntity f" +
        "   WHERE f.xCoordinate = :xCoordinate " +
        "       AND f.yCoordinate = :yCoordinate")
    FieldEntity findByXCoordinateAndYCoordinate(@Param("xCoordinate") int xCoordinate, @Param("yCoordinate") int yCoordinate);

    @Query("SELECT f " +
        "FROM FieldEntity f " +
        "ORDER BY f.xCoordinate ASC, f.yCoordinate ASC  ")
    List<FieldEntity> findAllOrderByCoordinates();
}
