package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.ClanEntity;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ClanEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClanEntityRepository extends JpaRepository<ClanEntity, Long> {
}
