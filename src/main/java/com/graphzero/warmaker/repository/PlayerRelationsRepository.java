package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.PlayerRelations;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the PlayerRelations entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlayerRelationsRepository extends JpaRepository<PlayerRelations, Long> {
}
