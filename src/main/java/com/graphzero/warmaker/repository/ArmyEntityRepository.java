package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.ArmyEntity;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ArmyEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ArmyEntityRepository extends JpaRepository<ArmyEntity, Long> {
}
