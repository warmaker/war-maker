package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.UnitsEntity;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the UnitsEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UnitsEntityRepository extends JpaRepository<UnitsEntity, Long> {
}
