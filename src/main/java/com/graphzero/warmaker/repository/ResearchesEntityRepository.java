package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.ResearchesEntity;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ResearchesEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResearchesEntityRepository extends JpaRepository<ResearchesEntity, Long> {
}
