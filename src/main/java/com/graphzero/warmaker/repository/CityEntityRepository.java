package com.graphzero.warmaker.repository;

import com.graphzero.warmaker.domain.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the CityEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CityEntityRepository extends JpaRepository<CityEntity, Long> {

    @Query("SELECT c.id " +
        " FROM CityEntity c " +
        " WHERE c.owner.id = :profileId")
    List<Long> findCityIdsByUserId(@Param("profileId") Long profileId);

    @Query("SELECT r " +
        " FROM CityEntity c " +
        "   LEFT JOIN c.owner o " +
        "   LEFT JOIN o.researches r" +
        " WHERE c.id = :cityId AND " +
        " o.id = :profileId")
    ResearchesEntity getResearchesForCity(@Param("profileId") Long profileId, @Param("cityId") Long cityId);

    @Query("SELECT a " +
        " FROM ArmyEntity a " +
        " WHERE a.city.id = :cityId ")
    Optional<ArmyEntity> getArmyForCity(@Param("cityId") Long cityId);


    @Query("SELECT wp " +
        " FROM CityEntity ce " +
        "   JOIN ce.army a" +
        "   JOIN a.warParties wp" +
        " WHERE ce.id = :cityId")
    List<WarPartyEntity> getWarParties(@Param("cityId") long cityId);

    @Query("SELECT b" +
        " FROM CityEntity ce " +
        "   JOIN ce.buildings b" +
        " WHERE ce.id = :cityId")
    Optional<CityBuildingsEntity> getCityBuildings(@Param("cityId") long cityId);

    Optional<CityEntity> findByLocationId(long locationId);
}
