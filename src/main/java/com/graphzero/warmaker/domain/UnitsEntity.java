package com.graphzero.warmaker.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A UnitsEntity.
 */
@Entity
@Table(name = "units_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UnitsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "spearmen")
    private Integer spearmen;

    @Column(name = "swordsmen")
    private Integer swordsmen;

    @Column(name = "archers")
    private Integer archers;

    @Column(name = "crossbowmen")
    private Integer crossbowmen;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSpearmen() {
        return spearmen;
    }

    public UnitsEntity spearmen(Integer spearmen) {
        this.spearmen = spearmen;
        return this;
    }

    public void setSpearmen(Integer spearmen) {
        this.spearmen = spearmen;
    }

    public Integer getSwordsmen() {
        return swordsmen;
    }

    public UnitsEntity swordsmen(Integer swordsmen) {
        this.swordsmen = swordsmen;
        return this;
    }

    public void setSwordsmen(Integer swordsmen) {
        this.swordsmen = swordsmen;
    }

    public Integer getArchers() {
        return archers;
    }

    public UnitsEntity archers(Integer archers) {
        this.archers = archers;
        return this;
    }

    public void setArchers(Integer archers) {
        this.archers = archers;
    }

    public Integer getCrossbowmen() {
        return crossbowmen;
    }

    public UnitsEntity crossbowmen(Integer crossbowmen) {
        this.crossbowmen = crossbowmen;
        return this;
    }

    public void setCrossbowmen(Integer crossbowmen) {
        this.crossbowmen = crossbowmen;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UnitsEntity)) {
            return false;
        }
        return id != null && id.equals(((UnitsEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UnitsEntity{" +
            "id=" + getId() +
            ", spearmen=" + getSpearmen() +
            ", swordsmen=" + getSwordsmen() +
            ", archers=" + getArchers() +
            ", crossbowmen=" + getCrossbowmen() +
            "}";
    }
}
