package com.graphzero.warmaker.domain.enumeration;

public enum ResourceType {
    DENARS, WOOD, STONE, STEEL, FOOD, FAITH
}
