package com.graphzero.warmaker.domain.enumeration;

/**
 * The FieldType enumeration.
 */
public enum FieldType {
    LAIR, POINT_OF_INTEREST, CITY, LAND
}
