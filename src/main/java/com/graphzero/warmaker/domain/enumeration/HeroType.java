package com.graphzero.warmaker.domain.enumeration;

/**
 * The HeroType enumeration.
 */
public enum HeroType {
    WARRIOR, SPY, MERCHANT, CLERIC
}
