package com.graphzero.warmaker.domain.enumeration;

/**
 * The CityType enumeration.
 */
public enum CityType {
    PLAYER, RENEGATES
}
