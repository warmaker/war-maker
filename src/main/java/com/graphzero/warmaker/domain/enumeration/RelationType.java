package com.graphzero.warmaker.domain.enumeration;

/**
 * The RelationType enumeration.
 */
public enum RelationType {
    ENEMY, ALLY, NEUTRAL
}
