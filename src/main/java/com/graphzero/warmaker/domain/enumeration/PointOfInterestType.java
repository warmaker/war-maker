package com.graphzero.warmaker.domain.enumeration;

/**
 * The PointOfInterestType enumeration.
 */
public enum PointOfInterestType {
    RIVER_CROSSING, FOREST, DESERT, RUINS
}
