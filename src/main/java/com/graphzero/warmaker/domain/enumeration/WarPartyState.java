package com.graphzero.warmaker.domain.enumeration;

/**
 * The WarPartyState enumeration.
 */
public enum WarPartyState {
    MOVING, STATIONED
}
