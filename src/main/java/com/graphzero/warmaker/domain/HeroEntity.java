package com.graphzero.warmaker.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

import com.graphzero.warmaker.domain.enumeration.HeroType;

/**
 * A HeroEntity.
 */
@Entity
@Table(name = "hero_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class HeroEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private HeroType type;

    @Column(name = "level")
    private Integer level;

    @OneToOne
    @JoinColumn(unique = true)
    private WarPartyEntity supportedWarParty;

    @OneToOne
    @JoinColumn(unique = true)
    private CityEntity supportedCity;

    @ManyToOne
    @JsonIgnoreProperties(value = "heroes", allowSetters = true)
    private ProfileEntity owner;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public HeroEntity name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HeroType getType() {
        return type;
    }

    public HeroEntity type(HeroType type) {
        this.type = type;
        return this;
    }

    public void setType(HeroType type) {
        this.type = type;
    }

    public Integer getLevel() {
        return level;
    }

    public HeroEntity level(Integer level) {
        this.level = level;
        return this;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public WarPartyEntity getSupportedWarParty() {
        return supportedWarParty;
    }

    public HeroEntity supportedWarParty(WarPartyEntity warPartyEntity) {
        this.supportedWarParty = warPartyEntity;
        return this;
    }

    public void setSupportedWarParty(WarPartyEntity warPartyEntity) {
        this.supportedWarParty = warPartyEntity;
    }

    public CityEntity getSupportedCity() {
        return supportedCity;
    }

    public HeroEntity supportedCity(CityEntity cityEntity) {
        this.supportedCity = cityEntity;
        return this;
    }

    public void setSupportedCity(CityEntity cityEntity) {
        this.supportedCity = cityEntity;
    }

    public ProfileEntity getOwner() {
        return owner;
    }

    public HeroEntity owner(ProfileEntity profileEntity) {
        this.owner = profileEntity;
        return this;
    }

    public void setOwner(ProfileEntity profileEntity) {
        this.owner = profileEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HeroEntity)) {
            return false;
        }
        return id != null && id.equals(((HeroEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HeroEntity{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", type='" + getType() + "'" +
            ", level=" + getLevel() +
            "}";
    }
}
