package com.graphzero.warmaker.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import com.graphzero.warmaker.domain.enumeration.CityType;

/**
 * A CityEntity.
 */
@Entity
@Table(name = "city_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CityEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "city_name")
    private String cityName;

    @Enumerated(EnumType.STRING)
    @Column(name = "city_type")
    private CityType cityType;

    @Column(name = "recruitment_modifier")
    private Double recruitmentModifier;

    @Column(name = "build_speed_modifier")
    private Double buildSpeedModifier;

    @OneToOne
    @JoinColumn(unique = true)
    private ArmyEntity army;

    @OneToOne
    @JoinColumn(unique = true)
    private CityBuildingsEntity buildings;

    @OneToOne
    @JoinColumn(unique = true)
    private ResourcesEntity storedResources;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private FieldEntity location;

    @ManyToOne
    @JsonIgnoreProperties(value = "ownedCities", allowSetters = true)
    private ProfileEntity owner;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public CityEntity cityName(String cityName) {
        this.cityName = cityName;
        return this;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public CityType getCityType() {
        return cityType;
    }

    public CityEntity cityType(CityType cityType) {
        this.cityType = cityType;
        return this;
    }

    public void setCityType(CityType cityType) {
        this.cityType = cityType;
    }

    public Double getRecruitmentModifier() {
        return recruitmentModifier;
    }

    public CityEntity recruitmentModifier(Double recruitmentModifier) {
        this.recruitmentModifier = recruitmentModifier;
        return this;
    }

    public void setRecruitmentModifier(Double recruitmentModifier) {
        this.recruitmentModifier = recruitmentModifier;
    }

    public Double getBuildSpeedModifier() {
        return buildSpeedModifier;
    }

    public CityEntity buildSpeedModifier(Double buildSpeedModifier) {
        this.buildSpeedModifier = buildSpeedModifier;
        return this;
    }

    public void setBuildSpeedModifier(Double buildSpeedModifier) {
        this.buildSpeedModifier = buildSpeedModifier;
    }

    public ArmyEntity getArmy() {
        return army;
    }

    public CityEntity army(ArmyEntity armyEntity) {
        this.army = armyEntity;
        return this;
    }

    public void setArmy(ArmyEntity armyEntity) {
        this.army = armyEntity;
    }

    public CityBuildingsEntity getBuildings() {
        return buildings;
    }

    public CityEntity buildings(CityBuildingsEntity cityBuildingsEntity) {
        this.buildings = cityBuildingsEntity;
        return this;
    }

    public void setBuildings(CityBuildingsEntity cityBuildingsEntity) {
        this.buildings = cityBuildingsEntity;
    }

    public ResourcesEntity getStoredResources() {
        return storedResources;
    }

    public CityEntity storedResources(ResourcesEntity resourcesEntity) {
        this.storedResources = resourcesEntity;
        return this;
    }

    public void setStoredResources(ResourcesEntity resourcesEntity) {
        this.storedResources = resourcesEntity;
    }

    public FieldEntity getLocation() {
        return location;
    }

    public CityEntity location(FieldEntity fieldEntity) {
        this.location = fieldEntity;
        return this;
    }

    public void setLocation(FieldEntity fieldEntity) {
        this.location = fieldEntity;
    }

    public ProfileEntity getOwner() {
        return owner;
    }

    public CityEntity owner(ProfileEntity profileEntity) {
        this.owner = profileEntity;
        return this;
    }

    public void setOwner(ProfileEntity profileEntity) {
        this.owner = profileEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CityEntity)) {
            return false;
        }
        return id != null && id.equals(((CityEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CityEntity{" +
            "id=" + getId() +
            ", cityName='" + getCityName() + "'" +
            ", cityType='" + getCityType() + "'" +
            ", recruitmentModifier=" + getRecruitmentModifier() +
            ", buildSpeedModifier=" + getBuildSpeedModifier() +
            "}";
    }
}
