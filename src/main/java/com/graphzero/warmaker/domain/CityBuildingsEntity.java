package com.graphzero.warmaker.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A CityBuildingsEntity.
 */
@Entity
@Table(name = "city_buildings_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CityBuildingsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "city_hall")
    private Integer cityHall;

    @Column(name = "barracks")
    private Integer barracks;

    @Column(name = "wall")
    private Integer wall;

    @Column(name = "gold_mine")
    private Integer goldMine;

    @Column(name = "saw_mill")
    private Integer sawMill;

    @Column(name = "ore_mine")
    private Integer oreMine;

    @Column(name = "ironworks")
    private Integer ironworks;

    @Column(name = "trading_post")
    private Integer tradingPost;

    @Column(name = "chapel")
    private Integer chapel;

    @Column(name = "university")
    private Integer university;

    @Column(name = "forge")
    private Integer forge;

    @OneToOne(mappedBy = "buildings")
    @JsonIgnore
    private CityEntity city;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCityHall() {
        return cityHall;
    }

    public CityBuildingsEntity cityHall(Integer cityHall) {
        this.cityHall = cityHall;
        return this;
    }

    public void setCityHall(Integer cityHall) {
        this.cityHall = cityHall;
    }

    public Integer getBarracks() {
        return barracks;
    }

    public CityBuildingsEntity barracks(Integer barracks) {
        this.barracks = barracks;
        return this;
    }

    public void setBarracks(Integer barracks) {
        this.barracks = barracks;
    }

    public Integer getWall() {
        return wall;
    }

    public CityBuildingsEntity wall(Integer wall) {
        this.wall = wall;
        return this;
    }

    public void setWall(Integer wall) {
        this.wall = wall;
    }

    public Integer getGoldMine() {
        return goldMine;
    }

    public CityBuildingsEntity goldMine(Integer goldMine) {
        this.goldMine = goldMine;
        return this;
    }

    public void setGoldMine(Integer goldMine) {
        this.goldMine = goldMine;
    }

    public Integer getSawMill() {
        return sawMill;
    }

    public CityBuildingsEntity sawMill(Integer sawMill) {
        this.sawMill = sawMill;
        return this;
    }

    public void setSawMill(Integer sawMill) {
        this.sawMill = sawMill;
    }

    public Integer getOreMine() {
        return oreMine;
    }

    public CityBuildingsEntity oreMine(Integer oreMine) {
        this.oreMine = oreMine;
        return this;
    }

    public void setOreMine(Integer oreMine) {
        this.oreMine = oreMine;
    }

    public Integer getIronworks() {
        return ironworks;
    }

    public CityBuildingsEntity ironworks(Integer ironworks) {
        this.ironworks = ironworks;
        return this;
    }

    public void setIronworks(Integer ironworks) {
        this.ironworks = ironworks;
    }

    public Integer getTradingPost() {
        return tradingPost;
    }

    public CityBuildingsEntity tradingPost(Integer tradingPost) {
        this.tradingPost = tradingPost;
        return this;
    }

    public void setTradingPost(Integer tradingPost) {
        this.tradingPost = tradingPost;
    }

    public Integer getChapel() {
        return chapel;
    }

    public CityBuildingsEntity chapel(Integer chapel) {
        this.chapel = chapel;
        return this;
    }

    public void setChapel(Integer chapel) {
        this.chapel = chapel;
    }

    public Integer getUniversity() {
        return university;
    }

    public CityBuildingsEntity university(Integer university) {
        this.university = university;
        return this;
    }

    public void setUniversity(Integer university) {
        this.university = university;
    }

    public Integer getForge() {
        return forge;
    }

    public CityBuildingsEntity forge(Integer forge) {
        this.forge = forge;
        return this;
    }

    public void setForge(Integer forge) {
        this.forge = forge;
    }

    public CityEntity getCity() {
        return city;
    }

    public CityBuildingsEntity city(CityEntity cityEntity) {
        this.city = cityEntity;
        return this;
    }

    public void setCity(CityEntity cityEntity) {
        this.city = cityEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CityBuildingsEntity)) {
            return false;
        }
        return id != null && id.equals(((CityBuildingsEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CityBuildingsEntity{" +
            "id=" + getId() +
            ", cityHall=" + getCityHall() +
            ", barracks=" + getBarracks() +
            ", wall=" + getWall() +
            ", goldMine=" + getGoldMine() +
            ", sawMill=" + getSawMill() +
            ", oreMine=" + getOreMine() +
            ", ironworks=" + getIronworks() +
            ", tradingPost=" + getTradingPost() +
            ", chapel=" + getChapel() +
            ", university=" + getUniversity() +
            ", forge=" + getForge() +
            "}";
    }
}
