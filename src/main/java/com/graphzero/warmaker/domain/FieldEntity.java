package com.graphzero.warmaker.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import com.graphzero.warmaker.domain.enumeration.FieldType;

/**
 * A FieldEntity.
 */
@Entity
@Table(name = "field_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FieldEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "x_coordinate", nullable = false)
    private Integer xCoordinate;

    @NotNull
    @Column(name = "y_coordinate", nullable = false)
    private Integer yCoordinate;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private FieldType type;

    @ManyToOne
    @JsonIgnoreProperties(value = "fields", allowSetters = true)
    private WorldEntity worldEntity;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getxCoordinate() {
        return xCoordinate;
    }

    public FieldEntity xCoordinate(Integer xCoordinate) {
        this.xCoordinate = xCoordinate;
        return this;
    }

    public void setxCoordinate(Integer xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public Integer getyCoordinate() {
        return yCoordinate;
    }

    public FieldEntity yCoordinate(Integer yCoordinate) {
        this.yCoordinate = yCoordinate;
        return this;
    }

    public void setyCoordinate(Integer yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public FieldType getType() {
        return type;
    }

    public FieldEntity type(FieldType type) {
        this.type = type;
        return this;
    }

    public void setType(FieldType type) {
        this.type = type;
    }

    public WorldEntity getWorldEntity() {
        return worldEntity;
    }

    public FieldEntity worldEntity(WorldEntity worldEntity) {
        this.worldEntity = worldEntity;
        return this;
    }

    public void setWorldEntity(WorldEntity worldEntity) {
        this.worldEntity = worldEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FieldEntity)) {
            return false;
        }
        return id != null && id.equals(((FieldEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FieldEntity{" +
            "id=" + getId() +
            ", xCoordinate=" + getxCoordinate() +
            ", yCoordinate=" + getyCoordinate() +
            ", type='" + getType() + "'" +
            "}";
    }
}
