package com.graphzero.warmaker.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

import com.graphzero.warmaker.domain.enumeration.PointOfInterestType;

/**
 * A PointOfInterestEntity.
 */
@Entity
@Table(name = "point_of_interest_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PointOfInterestEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private PointOfInterestType type;

    @OneToOne
    @JoinColumn(unique = true)
    private FortressEntity fortress;

    @OneToOne
    @JoinColumn(unique = true)
    private FieldEntity location;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PointOfInterestType getType() {
        return type;
    }

    public PointOfInterestEntity type(PointOfInterestType type) {
        this.type = type;
        return this;
    }

    public void setType(PointOfInterestType type) {
        this.type = type;
    }

    public FortressEntity getFortress() {
        return fortress;
    }

    public PointOfInterestEntity fortress(FortressEntity fortressEntity) {
        this.fortress = fortressEntity;
        return this;
    }

    public void setFortress(FortressEntity fortressEntity) {
        this.fortress = fortressEntity;
    }

    public FieldEntity getLocation() {
        return location;
    }

    public PointOfInterestEntity location(FieldEntity fieldEntity) {
        this.location = fieldEntity;
        return this;
    }

    public void setLocation(FieldEntity fieldEntity) {
        this.location = fieldEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PointOfInterestEntity)) {
            return false;
        }
        return id != null && id.equals(((PointOfInterestEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PointOfInterestEntity{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            "}";
    }
}
