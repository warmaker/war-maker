package com.graphzero.warmaker.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A WorldEntity.
 */
@Entity
@Table(name = "world_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WorldEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "world_name")
    private String worldName;

    @Column(name = "world_number")
    private Integer worldNumber;

    @Column(name = "height")
    private Integer height;

    @Column(name = "width")
    private Integer width;

    @OneToMany(mappedBy = "worldEntity")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<FieldEntity> fields = new HashSet<>();

    @OneToMany(mappedBy = "worldEntity")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ProfileEntity> participatingPlayers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWorldName() {
        return worldName;
    }

    public WorldEntity worldName(String worldName) {
        this.worldName = worldName;
        return this;
    }

    public void setWorldName(String worldName) {
        this.worldName = worldName;
    }

    public Integer getWorldNumber() {
        return worldNumber;
    }

    public WorldEntity worldNumber(Integer worldNumber) {
        this.worldNumber = worldNumber;
        return this;
    }

    public void setWorldNumber(Integer worldNumber) {
        this.worldNumber = worldNumber;
    }

    public Integer getHeight() {
        return height;
    }

    public WorldEntity height(Integer height) {
        this.height = height;
        return this;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public WorldEntity width(Integer width) {
        this.width = width;
        return this;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Set<FieldEntity> getFields() {
        return fields;
    }

    public WorldEntity fields(Set<FieldEntity> fieldEntities) {
        this.fields = fieldEntities;
        return this;
    }

    public WorldEntity addFields(FieldEntity fieldEntity) {
        this.fields.add(fieldEntity);
        fieldEntity.setWorldEntity(this);
        return this;
    }

    public WorldEntity removeFields(FieldEntity fieldEntity) {
        this.fields.remove(fieldEntity);
        fieldEntity.setWorldEntity(null);
        return this;
    }

    public void setFields(Set<FieldEntity> fieldEntities) {
        this.fields = fieldEntities;
    }

    public Set<ProfileEntity> getParticipatingPlayers() {
        return participatingPlayers;
    }

    public WorldEntity participatingPlayers(Set<ProfileEntity> profileEntities) {
        this.participatingPlayers = profileEntities;
        return this;
    }

    public WorldEntity addParticipatingPlayers(ProfileEntity profileEntity) {
        this.participatingPlayers.add(profileEntity);
        profileEntity.setWorldEntity(this);
        return this;
    }

    public WorldEntity removeParticipatingPlayers(ProfileEntity profileEntity) {
        this.participatingPlayers.remove(profileEntity);
        profileEntity.setWorldEntity(null);
        return this;
    }

    public void setParticipatingPlayers(Set<ProfileEntity> profileEntities) {
        this.participatingPlayers = profileEntities;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WorldEntity)) {
            return false;
        }
        return id != null && id.equals(((WorldEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "WorldEntity{" +
            "id=" + getId() +
            ", worldName='" + getWorldName() + "'" +
            ", worldNumber=" + getWorldNumber() +
            ", height=" + getHeight() +
            ", width=" + getWidth() +
            "}";
    }
}
