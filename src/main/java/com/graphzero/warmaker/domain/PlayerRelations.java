package com.graphzero.warmaker.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

import com.graphzero.warmaker.domain.enumeration.RelationType;

/**
 * A PlayerRelations.
 */
@Entity
@Table(name = "player_relations")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PlayerRelations implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "relation")
    private RelationType relation;

    @ManyToOne
    @JsonIgnoreProperties(value = "playerRelations", allowSetters = true)
    private ProfileEntity targetPlayer;

    @ManyToOne
    @JsonIgnoreProperties(value = "relations", allowSetters = true)
    private ProfileEntity owner;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RelationType getRelation() {
        return relation;
    }

    public PlayerRelations relation(RelationType relation) {
        this.relation = relation;
        return this;
    }

    public void setRelation(RelationType relation) {
        this.relation = relation;
    }

    public ProfileEntity getTargetPlayer() {
        return targetPlayer;
    }

    public PlayerRelations targetPlayer(ProfileEntity profileEntity) {
        this.targetPlayer = profileEntity;
        return this;
    }

    public void setTargetPlayer(ProfileEntity profileEntity) {
        this.targetPlayer = profileEntity;
    }

    public ProfileEntity getOwner() {
        return owner;
    }

    public PlayerRelations owner(ProfileEntity profileEntity) {
        this.owner = profileEntity;
        return this;
    }

    public void setOwner(ProfileEntity profileEntity) {
        this.owner = profileEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PlayerRelations)) {
            return false;
        }
        return id != null && id.equals(((PlayerRelations) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PlayerRelations{" +
            "id=" + getId() +
            ", relation='" + getRelation() + "'" +
            "}";
    }
}
