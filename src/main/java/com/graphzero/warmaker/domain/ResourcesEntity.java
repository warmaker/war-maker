package com.graphzero.warmaker.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A ResourcesEntity.
 */
@Entity
@Table(name = "resources_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResourcesEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "denars")
    private Double denars;

    @Column(name = "wood")
    private Double wood;

    @Column(name = "stone")
    private Double stone;

    @Column(name = "steel")
    private Double steel;

    @Column(name = "food")
    private Double food;

    @Column(name = "faith")
    private Double faith;

    @Column(name = "change_date")
    private ZonedDateTime changeDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getDenars() {
        return denars;
    }

    public ResourcesEntity denars(Double denars) {
        this.denars = denars;
        return this;
    }

    public void setDenars(Double denars) {
        this.denars = denars;
    }

    public Double getWood() {
        return wood;
    }

    public ResourcesEntity wood(Double wood) {
        this.wood = wood;
        return this;
    }

    public void setWood(Double wood) {
        this.wood = wood;
    }

    public Double getStone() {
        return stone;
    }

    public ResourcesEntity stone(Double stone) {
        this.stone = stone;
        return this;
    }

    public void setStone(Double stone) {
        this.stone = stone;
    }

    public Double getSteel() {
        return steel;
    }

    public ResourcesEntity steel(Double steel) {
        this.steel = steel;
        return this;
    }

    public void setSteel(Double steel) {
        this.steel = steel;
    }

    public Double getFood() {
        return food;
    }

    public ResourcesEntity food(Double food) {
        this.food = food;
        return this;
    }

    public void setFood(Double food) {
        this.food = food;
    }

    public Double getFaith() {
        return faith;
    }

    public ResourcesEntity faith(Double faith) {
        this.faith = faith;
        return this;
    }

    public void setFaith(Double faith) {
        this.faith = faith;
    }

    public ZonedDateTime getChangeDate() {
        return changeDate;
    }

    public ResourcesEntity changeDate(ZonedDateTime changeDate) {
        this.changeDate = changeDate;
        return this;
    }

    public void setChangeDate(ZonedDateTime changeDate) {
        this.changeDate = changeDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ResourcesEntity)) {
            return false;
        }
        return id != null && id.equals(((ResourcesEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ResourcesEntity{" +
            "id=" + getId() +
            ", denars=" + getDenars() +
            ", wood=" + getWood() +
            ", stone=" + getStone() +
            ", steel=" + getSteel() +
            ", food=" + getFood() +
            ", faith=" + getFaith() +
            ", changeDate='" + getChangeDate() + "'" +
            "}";
    }
}
