package com.graphzero.warmaker.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A ClanEntity.
 */
@Entity
@Table(name = "clan_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ClanEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "emblem", columnDefinition = "blob")
    private byte[] emblem;

    @Column(name = "emblem_content_type")
    private String emblemContentType;

    @OneToOne
    @JoinColumn(unique = true)
    private ProfileEntity members;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ClanEntity name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getEmblem() {
        return emblem;
    }

    public ClanEntity emblem(byte[] emblem) {
        this.emblem = emblem;
        return this;
    }

    public void setEmblem(byte[] emblem) {
        this.emblem = emblem;
    }

    public String getEmblemContentType() {
        return emblemContentType;
    }

    public ClanEntity emblemContentType(String emblemContentType) {
        this.emblemContentType = emblemContentType;
        return this;
    }

    public void setEmblemContentType(String emblemContentType) {
        this.emblemContentType = emblemContentType;
    }

    public ProfileEntity getMembers() {
        return members;
    }

    public ClanEntity members(ProfileEntity profileEntity) {
        this.members = profileEntity;
        return this;
    }

    public void setMembers(ProfileEntity profileEntity) {
        this.members = profileEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClanEntity)) {
            return false;
        }
        return id != null && id.equals(((ClanEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClanEntity{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", emblem='" + getEmblem() + "'" +
            ", emblemContentType='" + getEmblemContentType() + "'" +
            "}";
    }
}
