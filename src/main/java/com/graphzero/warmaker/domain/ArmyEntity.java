package com.graphzero.warmaker.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ArmyEntity.
 */
@Entity
@Table(name = "army_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ArmyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "food_req")
    private Integer foodReq;

    @OneToOne
    @JoinColumn(unique = true)
    private UnitsEntity units;

    @OneToMany(mappedBy = "armyEntity")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<WarPartyEntity> warParties = new HashSet<>();

    @OneToOne(mappedBy = "army")
    @JsonIgnore
    private CityEntity city;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFoodReq() {
        return foodReq;
    }

    public ArmyEntity foodReq(Integer foodReq) {
        this.foodReq = foodReq;
        return this;
    }

    public void setFoodReq(Integer foodReq) {
        this.foodReq = foodReq;
    }

    public UnitsEntity getUnits() {
        return units;
    }

    public ArmyEntity units(UnitsEntity unitsEntity) {
        this.units = unitsEntity;
        return this;
    }

    public void setUnits(UnitsEntity unitsEntity) {
        this.units = unitsEntity;
    }

    public Set<WarPartyEntity> getWarParties() {
        return warParties;
    }

    public ArmyEntity warParties(Set<WarPartyEntity> warPartyEntities) {
        this.warParties = warPartyEntities;
        return this;
    }

    public ArmyEntity addWarParties(WarPartyEntity warPartyEntity) {
        this.warParties.add(warPartyEntity);
        warPartyEntity.setArmyEntity(this);
        return this;
    }

    public ArmyEntity removeWarParties(WarPartyEntity warPartyEntity) {
        this.warParties.remove(warPartyEntity);
        warPartyEntity.setArmyEntity(null);
        return this;
    }

    public void setWarParties(Set<WarPartyEntity> warPartyEntities) {
        this.warParties = warPartyEntities;
    }

    public CityEntity getCity() {
        return city;
    }

    public ArmyEntity city(CityEntity cityEntity) {
        this.city = cityEntity;
        return this;
    }

    public void setCity(CityEntity cityEntity) {
        this.city = cityEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ArmyEntity)) {
            return false;
        }
        return id != null && id.equals(((ArmyEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ArmyEntity{" +
            "id=" + getId() +
            ", foodReq=" + getFoodReq() +
            "}";
    }
}
