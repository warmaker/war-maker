package com.graphzero.warmaker.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A ResearchesEntity.
 */
@Entity
@Table(name = "researches_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ResearchesEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "infantry_vitality")
    private Integer infantryVitality;

    @Column(name = "cavalry_vitality")
    private Integer cavalryVitality;

    @Column(name = "light_armor_value")
    private Integer lightArmorValue;

    @Column(name = "heavy_armor_value")
    private Integer heavyArmorValue;

    @Column(name = "melee_attack")
    private Integer meleeAttack;

    @Column(name = "ranged_attack")
    private Integer rangedAttack;

    @Column(name = "infantry_speed")
    private Integer infantrySpeed;

    @Column(name = "cavalry_speed")
    private Integer cavalrySpeed;

    @Column(name = "city_building_speed")
    private Integer cityBuildingSpeed;

    @Column(name = "recruitment_speed")
    private Integer recruitmentSpeed;

    @Column(name = "scouting")
    private Integer scouting;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getInfantryVitality() {
        return infantryVitality;
    }

    public ResearchesEntity infantryVitality(Integer infantryVitality) {
        this.infantryVitality = infantryVitality;
        return this;
    }

    public void setInfantryVitality(Integer infantryVitality) {
        this.infantryVitality = infantryVitality;
    }

    public Integer getCavalryVitality() {
        return cavalryVitality;
    }

    public ResearchesEntity cavalryVitality(Integer cavalryVitality) {
        this.cavalryVitality = cavalryVitality;
        return this;
    }

    public void setCavalryVitality(Integer cavalryVitality) {
        this.cavalryVitality = cavalryVitality;
    }

    public Integer getLightArmorValue() {
        return lightArmorValue;
    }

    public ResearchesEntity lightArmorValue(Integer lightArmorValue) {
        this.lightArmorValue = lightArmorValue;
        return this;
    }

    public void setLightArmorValue(Integer lightArmorValue) {
        this.lightArmorValue = lightArmorValue;
    }

    public Integer getHeavyArmorValue() {
        return heavyArmorValue;
    }

    public ResearchesEntity heavyArmorValue(Integer heavyArmorValue) {
        this.heavyArmorValue = heavyArmorValue;
        return this;
    }

    public void setHeavyArmorValue(Integer heavyArmorValue) {
        this.heavyArmorValue = heavyArmorValue;
    }

    public Integer getMeleeAttack() {
        return meleeAttack;
    }

    public ResearchesEntity meleeAttack(Integer meleeAttack) {
        this.meleeAttack = meleeAttack;
        return this;
    }

    public void setMeleeAttack(Integer meleeAttack) {
        this.meleeAttack = meleeAttack;
    }

    public Integer getRangedAttack() {
        return rangedAttack;
    }

    public ResearchesEntity rangedAttack(Integer rangedAttack) {
        this.rangedAttack = rangedAttack;
        return this;
    }

    public void setRangedAttack(Integer rangedAttack) {
        this.rangedAttack = rangedAttack;
    }

    public Integer getInfantrySpeed() {
        return infantrySpeed;
    }

    public ResearchesEntity infantrySpeed(Integer infantrySpeed) {
        this.infantrySpeed = infantrySpeed;
        return this;
    }

    public void setInfantrySpeed(Integer infantrySpeed) {
        this.infantrySpeed = infantrySpeed;
    }

    public Integer getCavalrySpeed() {
        return cavalrySpeed;
    }

    public ResearchesEntity cavalrySpeed(Integer cavalrySpeed) {
        this.cavalrySpeed = cavalrySpeed;
        return this;
    }

    public void setCavalrySpeed(Integer cavalrySpeed) {
        this.cavalrySpeed = cavalrySpeed;
    }

    public Integer getCityBuildingSpeed() {
        return cityBuildingSpeed;
    }

    public ResearchesEntity cityBuildingSpeed(Integer cityBuildingSpeed) {
        this.cityBuildingSpeed = cityBuildingSpeed;
        return this;
    }

    public void setCityBuildingSpeed(Integer cityBuildingSpeed) {
        this.cityBuildingSpeed = cityBuildingSpeed;
    }

    public Integer getRecruitmentSpeed() {
        return recruitmentSpeed;
    }

    public ResearchesEntity recruitmentSpeed(Integer recruitmentSpeed) {
        this.recruitmentSpeed = recruitmentSpeed;
        return this;
    }

    public void setRecruitmentSpeed(Integer recruitmentSpeed) {
        this.recruitmentSpeed = recruitmentSpeed;
    }

    public Integer getScouting() {
        return scouting;
    }

    public ResearchesEntity scouting(Integer scouting) {
        this.scouting = scouting;
        return this;
    }

    public void setScouting(Integer scouting) {
        this.scouting = scouting;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ResearchesEntity)) {
            return false;
        }
        return id != null && id.equals(((ResearchesEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ResearchesEntity{" +
            "id=" + getId() +
            ", infantryVitality=" + getInfantryVitality() +
            ", cavalryVitality=" + getCavalryVitality() +
            ", lightArmorValue=" + getLightArmorValue() +
            ", heavyArmorValue=" + getHeavyArmorValue() +
            ", meleeAttack=" + getMeleeAttack() +
            ", rangedAttack=" + getRangedAttack() +
            ", infantrySpeed=" + getInfantrySpeed() +
            ", cavalrySpeed=" + getCavalrySpeed() +
            ", cityBuildingSpeed=" + getCityBuildingSpeed() +
            ", recruitmentSpeed=" + getRecruitmentSpeed() +
            ", scouting=" + getScouting() +
            "}";
    }
}
