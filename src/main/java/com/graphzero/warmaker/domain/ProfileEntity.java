package com.graphzero.warmaker.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ProfileEntity.
 */
@Entity
@Table(name = "profile_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProfileEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "account_name")
    private String accountName;

    @Column(name = "email")
    private String email;

    @Column(name = "research_time_modifier")
    private Double researchTimeModifier;

    @OneToOne
    @JoinColumn(unique = true)
    private ResearchesEntity researches;

    @OneToOne
    @JoinColumn(unique = true)
    private AllianceEntity alignment;

    @OneToMany(mappedBy = "owner")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<CityEntity> ownedCities = new HashSet<>();

    @OneToMany(mappedBy = "owner")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<LandEntity> controlledLands = new HashSet<>();

    @OneToMany(mappedBy = "owner")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<HeroEntity> heroes = new HashSet<>();

    @OneToMany(mappedBy = "owner")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<PlayerRelations> relations = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "profileEntities", allowSetters = true)
    private User user;

    @OneToOne(mappedBy = "members")
    @JsonIgnore
    private ClanEntity clan;

    @ManyToOne
    @JsonIgnoreProperties(value = "participatingPlayers", allowSetters = true)
    private WorldEntity worldEntity;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountName() {
        return accountName;
    }

    public ProfileEntity accountName(String accountName) {
        this.accountName = accountName;
        return this;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getEmail() {
        return email;
    }

    public ProfileEntity email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getResearchTimeModifier() {
        return researchTimeModifier;
    }

    public ProfileEntity researchTimeModifier(Double researchTimeModifier) {
        this.researchTimeModifier = researchTimeModifier;
        return this;
    }

    public void setResearchTimeModifier(Double researchTimeModifier) {
        this.researchTimeModifier = researchTimeModifier;
    }

    public ResearchesEntity getResearches() {
        return researches;
    }

    public ProfileEntity researches(ResearchesEntity researchesEntity) {
        this.researches = researchesEntity;
        return this;
    }

    public void setResearches(ResearchesEntity researchesEntity) {
        this.researches = researchesEntity;
    }

    public AllianceEntity getAlignment() {
        return alignment;
    }

    public ProfileEntity alignment(AllianceEntity allianceEntity) {
        this.alignment = allianceEntity;
        return this;
    }

    public void setAlignment(AllianceEntity allianceEntity) {
        this.alignment = allianceEntity;
    }

    public Set<CityEntity> getOwnedCities() {
        return ownedCities;
    }

    public ProfileEntity ownedCities(Set<CityEntity> cityEntities) {
        this.ownedCities = cityEntities;
        return this;
    }

    public ProfileEntity addOwnedCities(CityEntity cityEntity) {
        this.ownedCities.add(cityEntity);
        cityEntity.setOwner(this);
        return this;
    }

    public ProfileEntity removeOwnedCities(CityEntity cityEntity) {
        this.ownedCities.remove(cityEntity);
        cityEntity.setOwner(null);
        return this;
    }

    public void setOwnedCities(Set<CityEntity> cityEntities) {
        this.ownedCities = cityEntities;
    }

    public Set<LandEntity> getControlledLands() {
        return controlledLands;
    }

    public ProfileEntity controlledLands(Set<LandEntity> landEntities) {
        this.controlledLands = landEntities;
        return this;
    }

    public ProfileEntity addControlledLands(LandEntity landEntity) {
        this.controlledLands.add(landEntity);
        landEntity.setOwner(this);
        return this;
    }

    public ProfileEntity removeControlledLands(LandEntity landEntity) {
        this.controlledLands.remove(landEntity);
        landEntity.setOwner(null);
        return this;
    }

    public void setControlledLands(Set<LandEntity> landEntities) {
        this.controlledLands = landEntities;
    }

    public Set<HeroEntity> getHeroes() {
        return heroes;
    }

    public ProfileEntity heroes(Set<HeroEntity> heroEntities) {
        this.heroes = heroEntities;
        return this;
    }

    public ProfileEntity addHeroes(HeroEntity heroEntity) {
        this.heroes.add(heroEntity);
        heroEntity.setOwner(this);
        return this;
    }

    public ProfileEntity removeHeroes(HeroEntity heroEntity) {
        this.heroes.remove(heroEntity);
        heroEntity.setOwner(null);
        return this;
    }

    public void setHeroes(Set<HeroEntity> heroEntities) {
        this.heroes = heroEntities;
    }

    public Set<PlayerRelations> getRelations() {
        return relations;
    }

    public ProfileEntity relations(Set<PlayerRelations> playerRelations) {
        this.relations = playerRelations;
        return this;
    }

    public ProfileEntity addRelations(PlayerRelations playerRelations) {
        this.relations.add(playerRelations);
        playerRelations.setOwner(this);
        return this;
    }

    public ProfileEntity removeRelations(PlayerRelations playerRelations) {
        this.relations.remove(playerRelations);
        playerRelations.setOwner(null);
        return this;
    }

    public void setRelations(Set<PlayerRelations> playerRelations) {
        this.relations = playerRelations;
    }

    public User getUser() {
        return user;
    }

    public ProfileEntity user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ClanEntity getClan() {
        return clan;
    }

    public ProfileEntity clan(ClanEntity clanEntity) {
        this.clan = clanEntity;
        return this;
    }

    public void setClan(ClanEntity clanEntity) {
        this.clan = clanEntity;
    }

    public WorldEntity getWorldEntity() {
        return worldEntity;
    }

    public ProfileEntity worldEntity(WorldEntity worldEntity) {
        this.worldEntity = worldEntity;
        return this;
    }

    public void setWorldEntity(WorldEntity worldEntity) {
        this.worldEntity = worldEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProfileEntity)) {
            return false;
        }
        return id != null && id.equals(((ProfileEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProfileEntity{" +
            "id=" + getId() +
            ", accountName='" + getAccountName() + "'" +
            ", email='" + getEmail() + "'" +
            ", researchTimeModifier=" + getResearchTimeModifier() +
            "}";
    }
}
