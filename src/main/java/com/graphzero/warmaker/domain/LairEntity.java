package com.graphzero.warmaker.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A LairEntity.
 */
@Entity
@Table(name = "lair_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class LairEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "lair_name")
    private String lairName;

    @OneToOne
    @JoinColumn(unique = true)
    private ResourcesEntity treasure;

    @OneToOne
    @JoinColumn(unique = true)
    private WarPartyEntity garrison;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private FieldEntity location;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLairName() {
        return lairName;
    }

    public LairEntity lairName(String lairName) {
        this.lairName = lairName;
        return this;
    }

    public void setLairName(String lairName) {
        this.lairName = lairName;
    }

    public ResourcesEntity getTreasure() {
        return treasure;
    }

    public LairEntity treasure(ResourcesEntity resourcesEntity) {
        this.treasure = resourcesEntity;
        return this;
    }

    public void setTreasure(ResourcesEntity resourcesEntity) {
        this.treasure = resourcesEntity;
    }

    public WarPartyEntity getGarrison() {
        return garrison;
    }

    public LairEntity garrison(WarPartyEntity warPartyEntity) {
        this.garrison = warPartyEntity;
        return this;
    }

    public void setGarrison(WarPartyEntity warPartyEntity) {
        this.garrison = warPartyEntity;
    }

    public FieldEntity getLocation() {
        return location;
    }

    public LairEntity location(FieldEntity fieldEntity) {
        this.location = fieldEntity;
        return this;
    }

    public void setLocation(FieldEntity fieldEntity) {
        this.location = fieldEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LairEntity)) {
            return false;
        }
        return id != null && id.equals(((LairEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LairEntity{" +
            "id=" + getId() +
            ", lairName='" + getLairName() + "'" +
            "}";
    }
}
