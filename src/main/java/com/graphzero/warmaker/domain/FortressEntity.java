package com.graphzero.warmaker.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A FortressEntity.
 */
@Entity
@Table(name = "fortress_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FortressEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "fortress_name")
    private String fortressName;

    @Column(name = "war_barracks")
    private Integer warBarracks;

    @Column(name = "wall")
    private Integer wall;

    @Column(name = "moat")
    private Integer moat;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFortressName() {
        return fortressName;
    }

    public FortressEntity fortressName(String fortressName) {
        this.fortressName = fortressName;
        return this;
    }

    public void setFortressName(String fortressName) {
        this.fortressName = fortressName;
    }

    public Integer getWarBarracks() {
        return warBarracks;
    }

    public FortressEntity warBarracks(Integer warBarracks) {
        this.warBarracks = warBarracks;
        return this;
    }

    public void setWarBarracks(Integer warBarracks) {
        this.warBarracks = warBarracks;
    }

    public Integer getWall() {
        return wall;
    }

    public FortressEntity wall(Integer wall) {
        this.wall = wall;
        return this;
    }

    public void setWall(Integer wall) {
        this.wall = wall;
    }

    public Integer getMoat() {
        return moat;
    }

    public FortressEntity moat(Integer moat) {
        this.moat = moat;
        return this;
    }

    public void setMoat(Integer moat) {
        this.moat = moat;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FortressEntity)) {
            return false;
        }
        return id != null && id.equals(((FortressEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FortressEntity{" +
            "id=" + getId() +
            ", fortressName='" + getFortressName() + "'" +
            ", warBarracks=" + getWarBarracks() +
            ", wall=" + getWall() +
            ", moat=" + getMoat() +
            "}";
    }
}
