package com.graphzero.warmaker.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import com.graphzero.warmaker.domain.enumeration.WarPartyState;

/**
 * A WarPartyEntity.
 */
@Entity
@Table(name = "war_party_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WarPartyEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "speed")
    private Double speed;

    @Column(name = "capacity")
    private Long capacity;

    @Enumerated(EnumType.STRING)
    @Column(name = "state")
    private WarPartyState state;

    @OneToOne
    @JoinColumn(unique = true)
    private ResourcesEntity resourcesCarried;

    @OneToOne
    @JoinColumn(unique = true)
    private UnitsEntity units;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "warPartyEntities", allowSetters = true)
    private FieldEntity position;

    @ManyToOne
    @JsonIgnoreProperties(value = "warParties", allowSetters = true)
    private ArmyEntity armyEntity;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getSpeed() {
        return speed;
    }

    public WarPartyEntity speed(Double speed) {
        this.speed = speed;
        return this;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Long getCapacity() {
        return capacity;
    }

    public WarPartyEntity capacity(Long capacity) {
        this.capacity = capacity;
        return this;
    }

    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    public WarPartyState getState() {
        return state;
    }

    public WarPartyEntity state(WarPartyState state) {
        this.state = state;
        return this;
    }

    public void setState(WarPartyState state) {
        this.state = state;
    }

    public ResourcesEntity getResourcesCarried() {
        return resourcesCarried;
    }

    public WarPartyEntity resourcesCarried(ResourcesEntity resourcesEntity) {
        this.resourcesCarried = resourcesEntity;
        return this;
    }

    public void setResourcesCarried(ResourcesEntity resourcesEntity) {
        this.resourcesCarried = resourcesEntity;
    }

    public UnitsEntity getUnits() {
        return units;
    }

    public WarPartyEntity units(UnitsEntity unitsEntity) {
        this.units = unitsEntity;
        return this;
    }

    public void setUnits(UnitsEntity unitsEntity) {
        this.units = unitsEntity;
    }

    public FieldEntity getPosition() {
        return position;
    }

    public WarPartyEntity position(FieldEntity fieldEntity) {
        this.position = fieldEntity;
        return this;
    }

    public void setPosition(FieldEntity fieldEntity) {
        this.position = fieldEntity;
    }

    public ArmyEntity getArmyEntity() {
        return armyEntity;
    }

    public WarPartyEntity armyEntity(ArmyEntity armyEntity) {
        this.armyEntity = armyEntity;
        return this;
    }

    public void setArmyEntity(ArmyEntity armyEntity) {
        this.armyEntity = armyEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WarPartyEntity)) {
            return false;
        }
        return id != null && id.equals(((WarPartyEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "WarPartyEntity{" +
            "id=" + getId() +
            ", speed=" + getSpeed() +
            ", capacity=" + getCapacity() +
            ", state='" + getState() + "'" +
            "}";
    }
}
