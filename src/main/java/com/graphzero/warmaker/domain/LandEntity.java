package com.graphzero.warmaker.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A LandEntity.
 */
@Entity
@Table(name = "land_entity")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class LandEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "road")
    private Integer road;

    @Column(name = "scout_tower")
    private Integer scoutTower;

    @OneToOne
    @JoinColumn(unique = true)
    private FortressEntity fortress;

    @OneToOne
    @JoinColumn(unique = true)
    private ArmyEntity army;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private FieldEntity location;

    @ManyToOne
    @JsonIgnoreProperties(value = "controlledLands", allowSetters = true)
    private ProfileEntity owner;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRoad() {
        return road;
    }

    public LandEntity road(Integer road) {
        this.road = road;
        return this;
    }

    public void setRoad(Integer road) {
        this.road = road;
    }

    public Integer getScoutTower() {
        return scoutTower;
    }

    public LandEntity scoutTower(Integer scoutTower) {
        this.scoutTower = scoutTower;
        return this;
    }

    public void setScoutTower(Integer scoutTower) {
        this.scoutTower = scoutTower;
    }

    public FortressEntity getFortress() {
        return fortress;
    }

    public LandEntity fortress(FortressEntity fortressEntity) {
        this.fortress = fortressEntity;
        return this;
    }

    public void setFortress(FortressEntity fortressEntity) {
        this.fortress = fortressEntity;
    }

    public ArmyEntity getArmy() {
        return army;
    }

    public LandEntity army(ArmyEntity armyEntity) {
        this.army = armyEntity;
        return this;
    }

    public void setArmy(ArmyEntity armyEntity) {
        this.army = armyEntity;
    }

    public FieldEntity getLocation() {
        return location;
    }

    public LandEntity location(FieldEntity fieldEntity) {
        this.location = fieldEntity;
        return this;
    }

    public void setLocation(FieldEntity fieldEntity) {
        this.location = fieldEntity;
    }

    public ProfileEntity getOwner() {
        return owner;
    }

    public LandEntity owner(ProfileEntity profileEntity) {
        this.owner = profileEntity;
        return this;
    }

    public void setOwner(ProfileEntity profileEntity) {
        this.owner = profileEntity;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LandEntity)) {
            return false;
        }
        return id != null && id.equals(((LandEntity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LandEntity{" +
            "id=" + getId() +
            ", road=" + getRoad() +
            ", scoutTower=" + getScoutTower() +
            "}";
    }
}
