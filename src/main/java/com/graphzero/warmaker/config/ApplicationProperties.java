package com.graphzero.warmaker.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Properties specific to War Maker.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@Validated
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    @Valid
    public final Cities cities = new Cities();
    @Valid
    public final Units units = new Units();
    @Valid
    public final World world = new World();

    public Cities getCities() {
        return cities;
    }

    public Units getUnits() {
        return units;
    }

    public World getWorld() {
        return world;
    }

    public static class Cities {
        @Valid
        public final Buildings buildings = new Buildings();

        public Buildings getBuildings() {
            return buildings;
        }

        public static class Buildings {
            @Valid
            public final SawMill sawMill = new SawMill();
            @Valid
            public final OreMine oreMine = new OreMine();
            @Valid
            public final GoldMine goldMine = new GoldMine();
            @Valid
            public final Ironworks ironworks = new Ironworks();
            @Valid
            public final Barracks barracks = new Barracks();
            @Valid
            public final CityHall cityHall = new CityHall();
            @Valid
            public final University university = new University();
            @Valid
            public final Forge forge = new Forge();

            public SawMill getSawMill() {
                return sawMill;
            }

            public OreMine getOreMine() {
                return oreMine;
            }

            public GoldMine getGoldMine() {
                return goldMine;
            }

            public Ironworks getIronworks() {
                return ironworks;
            }

            public Barracks getBarracks() {
                return barracks;
            }

            public CityHall getCityHall() {
                return cityHall;
            }

            public University getUniversity() {
                return university;
            }

            public Forge getForge() {
                return forge;
            }

            public static class SawMill extends BuildingConfig {
            }

            public static class OreMine extends BuildingConfig {
            }

            public static class GoldMine extends BuildingConfig {
            }

            public static class Ironworks extends BuildingConfig {
            }

            public static class Barracks extends BuildingConfig {
            }

            public static class CityHall extends BuildingConfig {
            }

            public static class University extends BuildingConfig {
            }

            public static class Forge extends BuildingConfig {
            }
        }
    }

    public static class Units {
        @Valid
        public final Spearman spearman = new Spearman();
        @Valid
        public final Swordsman swordsman = new Swordsman();
        @Valid
        public final Archer archer = new Archer();
        @Valid
        public final Crossbowman crossbowman = new Crossbowman();

        public Spearman getSpearman() {
            return spearman;
        }

        public Swordsman getSwordsman() {
            return swordsman;
        }

        public Archer getArcher() {
            return archer;
        }

        public Crossbowman getCrossbowman() {
            return crossbowman;
        }

        public static class Spearman extends UnitsConfig {
        }

        public static class Swordsman extends UnitsConfig {
        }

        public static class Archer extends UnitsConfig {
        }

        public static class Crossbowman extends UnitsConfig {
        }

    }

    public static class World {
        @Valid
        public final FieldModifiers fieldModifiers = new FieldModifiers();
        @Valid
        public final Generator generator = new Generator();

        public FieldModifiers getFieldModifiers() {
            return fieldModifiers;
        }

        public Generator getGenerator() {
            return generator;
        }

        public static class FieldModifiers {
            @NotNull
            protected double land;
            @NotNull
            protected double lair;
            @NotNull
            protected double city;
            @NotNull
            protected double pointOfInterest;

            public double getLand() {
                return land;
            }

            public double getLair() {
                return lair;
            }

            public double getCity() {
                return city;
            }

            public double getPointOfInterest() {
                return pointOfInterest;
            }

            public void setLand(double land) {
                this.land = land;
            }

            public void setLair(double lair) {
                this.lair = lair;
            }

            public void setCity(double city) {
                this.city = city;
            }

            public void setPointOfInterest(double pointOfInterest) {
                this.pointOfInterest = pointOfInterest;
            }
        }

        public static class Generator {
            @NotNull
            protected boolean regenerate;

            public boolean regenerate() {
                return regenerate;
            }

            public void setRegenerate(boolean regenerate) {
                this.regenerate = regenerate;
            }
        }
    }

    public abstract static class UnitsConfig {
        @NotNull
        protected int meleeAttack;
        @NotNull
        protected int rangedAttack;
        @NotNull
        protected int meleeArmorPenetration;
        @NotNull
        protected int rangedArmorPenetration;
        @NotNull
        protected int health;
        @NotNull
        protected int armor;
        @NotNull
        protected int speed;
        @NotNull
        protected int recruitmentTime;
        @Valid
        public final UnitsCost unitsCost = new UnitsCost();

        public static class UnitsCost {
            @NotNull
            protected int gold;
            @NotNull
            protected int wood;
            @NotNull
            protected int stone;
            @NotNull
            protected int iron;
            @NotNull
            protected int food;
            @NotNull
            protected int faith;

            public int getGold() {
                return gold;
            }

            public void setGold(int gold) {
                this.gold = gold;
            }

            public int getWood() {
                return wood;
            }

            public void setWood(int wood) {
                this.wood = wood;
            }

            public int getStone() {
                return stone;
            }

            public void setStone(int stone) {
                this.stone = stone;
            }

            public int getIron() {
                return iron;
            }

            public void setIron(int iron) {
                this.iron = iron;
            }

            public int getFood() {
                return food;
            }

            public void setFood(int food) {
                this.food = food;
            }

            public int getFaith() {
                return faith;
            }

            public void setFaith(int faith) {
                this.faith = faith;
            }
        }

        public int getMeleeAttack() {
            return meleeAttack;
        }

        public void setMeleeAttack(int meleeAttack) {
            this.meleeAttack = meleeAttack;
        }

        public int getRangedAttack() {
            return rangedAttack;
        }

        public void setRangedAttack(int rangedAttack) {
            this.rangedAttack = rangedAttack;
        }

        public int getMeleeArmorPenetration() {
            return meleeArmorPenetration;
        }

        public void setMeleeArmorPenetration(int meleeArmorPenetration) {
            this.meleeArmorPenetration = meleeArmorPenetration;
        }

        public int getRangedArmorPenetration() {
            return rangedArmorPenetration;
        }

        public void setRangedArmorPenetration(int rangedArmorPenetration) {
            this.rangedArmorPenetration = rangedArmorPenetration;
        }

        public int getHealth() {
            return health;
        }

        public void setHealth(int health) {
            this.health = health;
        }

        public int getArmor() {
            return armor;
        }

        public void setArmor(int armor) {
            this.armor = armor;
        }

        public int getSpeed() {
            return speed;
        }

        public void setSpeed(int speed) {
            this.speed = speed;
        }

        public int getRecruitmentTime() {
            return recruitmentTime;
        }

        public void setRecruitmentTime(int recruitmentTime) {
            this.recruitmentTime = recruitmentTime;
        }

        public UnitsCost getUnitsCost() {
            return unitsCost;
        }
    }

    public abstract static class BuildingConfig {
        @NotNull
        protected int basePointValue;
        @NotNull
        protected int baseToughness;
        @NotNull
        protected int baseBuildSpeed;

        public int getBasePointValue() {
            return basePointValue;
        }

        public void setBasePointValue(int basePointValue) {
            this.basePointValue = basePointValue;
        }

        public int getBaseToughness() {
            return baseToughness;
        }

        public void setBaseToughness(int baseToughness) {
            this.baseToughness = baseToughness;
        }

        public int getBaseBuildSpeed() {
            return baseBuildSpeed;
        }

        public void setBaseBuildSpeed(int baseBuildSpeed) {
            this.baseBuildSpeed = baseBuildSpeed;
        }
    }
}
