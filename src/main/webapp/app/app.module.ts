import './vendor';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WarMakerCoreModule } from 'app/core/core.module';
import { WarMakerSharedModule } from 'app/shared/shared.module';

import { AccountModule } from './account/account.module';
import { WarMakerAppRoutingModule } from './app-routing.module';
import { WarMakerEntityModule } from './entities/entity.module';
import { ErrorComponent } from './layouts/error/error.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { MainComponent } from './layouts/main/main.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { WarMakerHomeModule } from './war-maker/home/home.module';
import { PlayerInfoModule } from './war-maker/player-info/player-info.module';
import { SharedModule } from './war-maker/shared/shared.module';

// jhipster-needle-angular-add-module-import JHipster will add new module here
@NgModule({
  imports: [
    BrowserModule,
    WarMakerSharedModule,
    WarMakerCoreModule,
    WarMakerHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    AccountModule,
    WarMakerEntityModule,
    WarMakerAppRoutingModule,
    BrowserAnimationsModule,
    PlayerInfoModule,
    SharedModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class WarMakerAppModule {}
