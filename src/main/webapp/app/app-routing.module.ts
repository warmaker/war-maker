import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute } from './layouts/error/error.route';
import { navbarRoute } from './layouts/navbar/navbar.route';
import { Authority } from 'app/shared/constants/authority.constants';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

const LAYOUT_ROUTES = [navbarRoute, ...errorRoute];

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'admin',
          data: {
            authorities: [Authority.ADMIN],
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./admin/admin-routing.module').then(m => m.AdminRoutingModule),
        },
        {
          path: 'account',
          loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
        },
        {
          path: 'main',
          loadChildren: () => import('./war-maker/main/main.module').then(m => m.MainModule),
        },
        {
          path: 'message-box',
          loadChildren: () => import('./war-maker/message-box/message-box.module').then(m => m.MessageBoxModule),
        },
        {
          path: 'world-map',
          loadChildren: () => import('./war-maker/world-map/world-map.module').then(m => m.WorldMapModule),
        },
        {
          path: 'army',
          loadChildren: () => import('./war-maker/army/army.module').then(m => m.ArmyModule),
        },
        {
          path: 'university',
          loadChildren: () => import('./war-maker/university/university.module').then(m => m.UniversityModule),
        },
        ...LAYOUT_ROUTES,
      ],
      { enableTracing: false }
    ),
  ],
  exports: [RouterModule],
})
export class WarMakerAppRoutingModule {}
