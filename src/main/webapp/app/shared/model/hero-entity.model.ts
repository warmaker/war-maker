import { IWarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { ICityEntity } from 'app/shared/model/city-entity.model';
import { IProfileEntity } from 'app/shared/model/profile-entity.model';
import { HeroType } from 'app/shared/model/enumerations/hero-type.model';

export interface IHeroEntity {
  id?: number;
  name?: string;
  type?: HeroType;
  level?: number;
  supportedWarParty?: IWarPartyEntity;
  supportedCity?: ICityEntity;
  owner?: IProfileEntity;
}

export class HeroEntity implements IHeroEntity {
  constructor(
    public id?: number,
    public name?: string,
    public type?: HeroType,
    public level?: number,
    public supportedWarParty?: IWarPartyEntity,
    public supportedCity?: ICityEntity,
    public owner?: IProfileEntity
  ) {}
}
