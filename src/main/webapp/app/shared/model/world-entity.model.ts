import { IFieldEntity } from 'app/shared/model/field-entity.model';
import { IProfileEntity } from 'app/shared/model/profile-entity.model';

export interface IWorldEntity {
  id?: number;
  worldName?: string;
  worldNumber?: number;
  height?: number;
  width?: number;
  fields?: IFieldEntity[];
  participatingPlayers?: IProfileEntity[];
}

export class WorldEntity implements IWorldEntity {
  constructor(
    public id?: number,
    public worldName?: string,
    public worldNumber?: number,
    public height?: number,
    public width?: number,
    public fields?: IFieldEntity[],
    public participatingPlayers?: IProfileEntity[]
  ) {}
}
