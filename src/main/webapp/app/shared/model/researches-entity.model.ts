export interface IResearchesEntity {
  id?: number;
  infantryVitality?: number;
  cavalryVitality?: number;
  lightArmorValue?: number;
  heavyArmorValue?: number;
  meleeAttack?: number;
  rangedAttack?: number;
  infantrySpeed?: number;
  cavalrySpeed?: number;
  cityBuildingSpeed?: number;
  recruitmentSpeed?: number;
  scouting?: number;
}

export class ResearchesEntity implements IResearchesEntity {
  constructor(
    public id?: number,
    public infantryVitality?: number,
    public cavalryVitality?: number,
    public lightArmorValue?: number,
    public heavyArmorValue?: number,
    public meleeAttack?: number,
    public rangedAttack?: number,
    public infantrySpeed?: number,
    public cavalrySpeed?: number,
    public cityBuildingSpeed?: number,
    public recruitmentSpeed?: number,
    public scouting?: number
  ) {}
}
