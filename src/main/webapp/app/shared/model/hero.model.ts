import { IWarParty } from 'app/shared/model//war-party.model';
import { ICity } from 'app/shared/model//city.model';
import { IProfile } from 'app/shared/model//profile.model';

export const enum HeroType {
    WARRIOR = 'WARRIOR',
    SPY = 'SPY',
    MERCHANT = 'MERCHANT',
    CLERIC = 'CLERIC'
}

export interface IHero {
    id?: number;
    name?: string;
    type?: HeroType;
    level?: number;
    supportedWarParty?: IWarParty;
    supportedCity?: ICity;
    owner?: IProfile;
}

export class Hero implements IHero {
    constructor(
        public id?: number,
        public name?: string,
        public type?: HeroType,
        public level?: number,
        public supportedWarParty?: IWarParty,
        public supportedCity?: ICity,
        public owner?: IProfile
    ) {}
}
