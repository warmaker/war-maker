export interface IUnitsEntity {
  id?: number;
  spearmen?: number;
  swordsmen?: number;
  archers?: number;
  crossbowmen?: number;
}

export class UnitsEntity implements IUnitsEntity {
  constructor(
    public id?: number,
    public spearmen?: number,
    public swordsmen?: number,
    public archers?: number,
    public crossbowmen?: number
  ) {}
}
