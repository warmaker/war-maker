export const enum HeroType {
  WARRIOR = 'WARRIOR',

  SPY = 'SPY',

  MERCHANT = 'MERCHANT',

  CLERIC = 'CLERIC',
}
