export const enum FieldType {
  LAIR = 'LAIR',

  POINT_OF_INTEREST = 'POINT_OF_INTEREST',

  CITY = 'CITY',

  LAND = 'LAND',
}
