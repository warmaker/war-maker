export const enum PointOfInterestType {
  RIVER_CROSSING = 'RIVER_CROSSING',

  FOREST = 'FOREST',

  DESERT = 'DESERT',

  RUINS = 'RUINS',
}
