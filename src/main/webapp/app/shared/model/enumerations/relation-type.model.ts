export const enum RelationType {
  ENEMY = 'ENEMY',

  ALLY = 'ALLY',

  NEUTRAL = 'NEUTRAL',
}
