import { IWorld } from 'app/shared/model//world.model';

export const enum FieldType {
    LAIR = 'LAIR',
    POINT_OF_INTEREST = 'POINT_OF_INTEREST',
    CITY = 'CITY',
    LAND = 'LAND'
}

export interface IField {
    id?: number;
    xCoordinate?: number;
    yCoordinate?: number;
    type?: FieldType;
    world?: IWorld;
}

export class Field implements IField {
    constructor(
        public id?: number,
        public xCoordinate?: number,
        public yCoordinate?: number,
        public type?: FieldType,
        public world?: IWorld
    ) {}
}
