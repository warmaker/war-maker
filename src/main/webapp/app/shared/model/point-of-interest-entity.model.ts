import { IFortressEntity } from 'app/shared/model/fortress-entity.model';
import { IFieldEntity } from 'app/shared/model/field-entity.model';
import { PointOfInterestType } from 'app/shared/model/enumerations/point-of-interest-type.model';

export interface IPointOfInterestEntity {
  id?: number;
  type?: PointOfInterestType;
  fortress?: IFortressEntity;
  location?: IFieldEntity;
}

export class PointOfInterestEntity implements IPointOfInterestEntity {
  constructor(public id?: number, public type?: PointOfInterestType, public fortress?: IFortressEntity, public location?: IFieldEntity) {}
}
