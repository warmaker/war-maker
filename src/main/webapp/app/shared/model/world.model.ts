import { IField } from 'app/shared/model//field.model';
import { IProfile } from 'app/shared/model//profile.model';

export interface IWorld {
    id?: number;
    worldName?: string;
    worldNumber?: number;
    height?: number;
    width?: number;
    fields?: IField[];
    participatingPlayers?: IProfile[];
}

export class World implements IWorld {
    constructor(
        public id?: number,
        public worldName?: string,
        public worldNumber?: number,
        public height?: number,
        public width?: number,
        public fields?: IField[],
        public participatingPlayers?: IProfile[]
    ) {}
}
