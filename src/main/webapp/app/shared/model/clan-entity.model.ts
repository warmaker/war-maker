import { IProfileEntity } from 'app/shared/model/profile-entity.model';

export interface IClanEntity {
  id?: number;
  name?: string;
  emblemContentType?: string;
  emblem?: any;
  members?: IProfileEntity;
}

export class ClanEntity implements IClanEntity {
  constructor(
    public id?: number,
    public name?: string,
    public emblemContentType?: string,
    public emblem?: any,
    public members?: IProfileEntity
  ) {}
}
