export interface IAlliance {
    id?: number;
    name?: string;
}

export class Alliance implements IAlliance {
    constructor(public id?: number, public name?: string) {}
}
