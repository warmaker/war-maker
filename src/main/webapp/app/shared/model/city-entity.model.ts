import { IArmyEntity } from 'app/shared/model/army-entity.model';
import { ICityBuildingsEntity } from 'app/shared/model/city-buildings-entity.model';
import { IResourcesEntity } from 'app/shared/model/resources-entity.model';
import { IFieldEntity } from 'app/shared/model/field-entity.model';
import { IProfileEntity } from 'app/shared/model/profile-entity.model';
import { CityType } from 'app/shared/model/enumerations/city-type.model';

export interface ICityEntity {
  id?: number;
  cityName?: string;
  cityType?: CityType;
  recruitmentModifier?: number;
  buildSpeedModifier?: number;
  army?: IArmyEntity;
  buildings?: ICityBuildingsEntity;
  storedResources?: IResourcesEntity;
  location?: IFieldEntity;
  owner?: IProfileEntity;
}

export class CityEntity implements ICityEntity {
  constructor(
    public id?: number,
    public cityName?: string,
    public cityType?: CityType,
    public recruitmentModifier?: number,
    public buildSpeedModifier?: number,
    public army?: IArmyEntity,
    public buildings?: ICityBuildingsEntity,
    public storedResources?: IResourcesEntity,
    public location?: IFieldEntity,
    public owner?: IProfileEntity
  ) {}
}
