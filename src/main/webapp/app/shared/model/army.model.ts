import { IUnits } from 'app/shared/model//units.model';
import { IWarParty } from 'app/shared/model//war-party.model';
import { ICity } from 'app/shared/model//city.model';

export interface IArmy {
    id?: number;
    foodReq?: number;
    units?: IUnits;
    warParties?: IWarParty[];
    city?: ICity;
}

export class Army implements IArmy {
    constructor(public id?: number, public foodReq?: number, public units?: IUnits, public warParties?: IWarParty[], public city?: ICity) {}
}
