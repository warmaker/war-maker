export interface IFortress {
    id?: number;
    fortressName?: string;
    warBarracks?: number;
    wall?: number;
    moat?: number;
}

export class Fortress implements IFortress {
    constructor(
        public id?: number,
        public fortressName?: string,
        public warBarracks?: number,
        public wall?: number,
        public moat?: number
    ) {}
}
