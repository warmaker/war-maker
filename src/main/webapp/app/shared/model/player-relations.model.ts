import { IProfileEntity } from 'app/shared/model/profile-entity.model';
import { RelationType } from 'app/shared/model/enumerations/relation-type.model';

export interface IPlayerRelations {
  id?: number;
  relation?: RelationType;
  targetPlayer?: IProfileEntity;
  owner?: IProfileEntity;
}

export class PlayerRelations implements IPlayerRelations {
  constructor(public id?: number, public relation?: RelationType, public targetPlayer?: IProfileEntity, public owner?: IProfileEntity) {}
}
