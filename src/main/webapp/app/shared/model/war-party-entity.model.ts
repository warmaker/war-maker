import { IResourcesEntity } from 'app/shared/model/resources-entity.model';
import { IUnitsEntity } from 'app/shared/model/units-entity.model';
import { IFieldEntity } from 'app/shared/model/field-entity.model';
import { IArmyEntity } from 'app/shared/model/army-entity.model';
import { WarPartyState } from 'app/shared/model/enumerations/war-party-state.model';

export interface IWarPartyEntity {
  id?: number;
  speed?: number;
  capacity?: number;
  state?: WarPartyState;
  resourcesCarried?: IResourcesEntity;
  units?: IUnitsEntity;
  position?: IFieldEntity;
  armyEntity?: IArmyEntity;
}

export class WarPartyEntity implements IWarPartyEntity {
  constructor(
    public id?: number,
    public speed?: number,
    public capacity?: number,
    public state?: WarPartyState,
    public resourcesCarried?: IResourcesEntity,
    public units?: IUnitsEntity,
    public position?: IFieldEntity,
    public armyEntity?: IArmyEntity
  ) {}
}
