import { IWorld } from 'app/shared/model//world.model';
import { IResearches } from 'app/shared/model//researches.model';
import { IAlliance } from 'app/shared/model//alliance.model';
import { ICity } from 'app/shared/model//city.model';
import { ILand } from 'app/shared/model//land.model';
import { IHero } from 'app/shared/model//hero.model';
import { IUser } from 'app/core/user/user.model';
import { IClan } from 'app/shared/model//clan.model';

export interface IProfile {
    id?: number;
    accountName?: string;
    email?: string;
    researchTimeModifier?: number;
    world?: IWorld;
    researches?: IResearches;
    alliance?: IAlliance;
    ownedCities?: ICity[];
    controlledLands?: ILand[];
    heroes?: IHero[];
    user?: IUser;
    clan?: IClan;
}

export class Profile implements IProfile {
    constructor(
        public id?: number,
        public accountName?: string,
        public email?: string,
        public researchTimeModifier?: number,
        public world?: IWorld,
        public researches?: IResearches,
        public alliance?: IAlliance,
        public ownedCities?: ICity[],
        public controlledLands?: ILand[],
        public heroes?: IHero[],
        public user?: IUser,
        public clan?: IClan
    ) {}
}
