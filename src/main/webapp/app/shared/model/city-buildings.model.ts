import { ICity } from 'app/shared/model//city.model';

export interface ICityBuildings {
    id?: number;
    cityHall?: number;
    barracks?: number;
    wall?: number;
    sawMill?: number;
    oreMine?: number;
    ironworks?: number;
    tradingPost?: number;
    chapel?: number;
    city?: ICity;
}

export class CityBuildings implements ICityBuildings {
    constructor(
        public id?: number,
        public cityHall?: number,
        public barracks?: number,
        public wall?: number,
        public sawMill?: number,
        public oreMine?: number,
        public ironworks?: number,
        public tradingPost?: number,
        public chapel?: number,
        public city?: ICity
    ) {}
}
