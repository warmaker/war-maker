export interface IFortressEntity {
  id?: number;
  fortressName?: string;
  warBarracks?: number;
  wall?: number;
  moat?: number;
}

export class FortressEntity implements IFortressEntity {
  constructor(public id?: number, public fortressName?: string, public warBarracks?: number, public wall?: number, public moat?: number) {}
}
