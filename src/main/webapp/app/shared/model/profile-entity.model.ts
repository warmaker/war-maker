import { IResearchesEntity } from 'app/shared/model/researches-entity.model';
import { IAllianceEntity } from 'app/shared/model/alliance-entity.model';
import { ICityEntity } from 'app/shared/model/city-entity.model';
import { ILandEntity } from 'app/shared/model/land-entity.model';
import { IHeroEntity } from 'app/shared/model/hero-entity.model';
import { IPlayerRelations } from 'app/shared/model/player-relations.model';
import { IUser } from 'app/core/user/user.model';
import { IClanEntity } from 'app/shared/model/clan-entity.model';
import { IWorldEntity } from 'app/shared/model/world-entity.model';

export interface IProfileEntity {
  id?: number;
  accountName?: string;
  email?: string;
  researchTimeModifier?: number;
  researches?: IResearchesEntity;
  alignment?: IAllianceEntity;
  ownedCities?: ICityEntity[];
  controlledLands?: ILandEntity[];
  heroes?: IHeroEntity[];
  relations?: IPlayerRelations[];
  user?: IUser;
  clan?: IClanEntity;
  worldEntity?: IWorldEntity;
}

export class ProfileEntity implements IProfileEntity {
  constructor(
    public id?: number,
    public accountName?: string,
    public email?: string,
    public researchTimeModifier?: number,
    public researches?: IResearchesEntity,
    public alignment?: IAllianceEntity,
    public ownedCities?: ICityEntity[],
    public controlledLands?: ILandEntity[],
    public heroes?: IHeroEntity[],
    public relations?: IPlayerRelations[],
    public user?: IUser,
    public clan?: IClanEntity,
    public worldEntity?: IWorldEntity
  ) {}
}
