export interface IUnits {
    id?: number;
    spearmen?: number;
    swordsmen?: number;
    archers?: number;
    crossbowmen?: number;
}

export class Units implements IUnits {
    constructor(
        public id?: number,
        public spearmen?: number,
        public swordsmen?: number,
        public archers?: number,
        public crossbowmen?: number
    ) {}
}
