import { IFortress } from 'app/shared/model//fortress.model';
import { IArmy } from 'app/shared/model//army.model';
import { IField } from 'app/shared/model//field.model';
import { IProfile } from 'app/shared/model//profile.model';

export interface ILand {
    id?: number;
    road?: number;
    scoutTower?: number;
    fortress?: IFortress;
    army?: IArmy;
    location?: IField;
    owner?: IProfile;
}

export class Land implements ILand {
    constructor(
        public id?: number,
        public road?: number,
        public scoutTower?: number,
        public fortress?: IFortress,
        public army?: IArmy,
        public location?: IField,
        public owner?: IProfile
    ) {}
}
