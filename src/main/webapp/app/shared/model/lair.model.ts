import { IResources } from 'app/shared/model//resources.model';
import { IWarParty } from 'app/shared/model//war-party.model';
import { IField } from 'app/shared/model//field.model';

export interface ILair {
    id?: number;
    lairName?: string;
    treasure?: IResources;
    garrison?: IWarParty;
    location?: IField;
}

export class Lair implements ILair {
    constructor(
        public id?: number,
        public lairName?: string,
        public treasure?: IResources,
        public garrison?: IWarParty,
        public location?: IField
    ) {}
}
