import { Moment } from 'moment';

export interface IResources {
    id?: number;
    denars?: number;
    wood?: number;
    stone?: number;
    steel?: number;
    food?: number;
    faith?: number;
    changeDate?: Moment;
}

export class Resources implements IResources {
    constructor(
        public id?: number,
        public denars?: number,
        public wood?: number,
        public stone?: number,
        public steel?: number,
        public food?: number,
        public faith?: number,
        public changeDate?: Moment
    ) {}
}
