import { IArmy } from 'app/shared/model//army.model';
import { IResources } from 'app/shared/model//resources.model';
import { IUnits } from 'app/shared/model//units.model';
import { IField } from 'app/shared/model//field.model';

export const enum WarPartyState {
    MOVING = 'MOVING',
    STATIONED = 'STATIONED'
}

export interface IWarParty {
    id?: number;
    speed?: number;
    capacity?: number;
    state?: WarPartyState;
    army?: IArmy;
    resourcesCarried?: IResources;
    units?: IUnits;
    position?: IField;
}

export class WarParty implements IWarParty {
    constructor(
        public id?: number,
        public speed?: number,
        public capacity?: number,
        public state?: WarPartyState,
        public army?: IArmy,
        public resourcesCarried?: IResources,
        public units?: IUnits,
        public position?: IField
    ) {}
}
