import { IFortressEntity } from 'app/shared/model/fortress-entity.model';
import { IArmyEntity } from 'app/shared/model/army-entity.model';
import { IFieldEntity } from 'app/shared/model/field-entity.model';
import { IProfileEntity } from 'app/shared/model/profile-entity.model';

export interface ILandEntity {
  id?: number;
  road?: number;
  scoutTower?: number;
  fortress?: IFortressEntity;
  army?: IArmyEntity;
  location?: IFieldEntity;
  owner?: IProfileEntity;
}

export class LandEntity implements ILandEntity {
  constructor(
    public id?: number,
    public road?: number,
    public scoutTower?: number,
    public fortress?: IFortressEntity,
    public army?: IArmyEntity,
    public location?: IFieldEntity,
    public owner?: IProfileEntity
  ) {}
}
