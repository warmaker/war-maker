import { IResourcesEntity } from 'app/shared/model/resources-entity.model';
import { IWarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { IFieldEntity } from 'app/shared/model/field-entity.model';

export interface ILairEntity {
  id?: number;
  lairName?: string;
  treasure?: IResourcesEntity;
  garrison?: IWarPartyEntity;
  location?: IFieldEntity;
}

export class LairEntity implements ILairEntity {
  constructor(
    public id?: number,
    public lairName?: string,
    public treasure?: IResourcesEntity,
    public garrison?: IWarPartyEntity,
    public location?: IFieldEntity
  ) {}
}
