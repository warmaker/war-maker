export interface IAllianceEntity {
  id?: number;
  name?: string;
}

export class AllianceEntity implements IAllianceEntity {
  constructor(public id?: number, public name?: string) {}
}
