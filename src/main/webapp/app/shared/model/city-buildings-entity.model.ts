import { ICityEntity } from 'app/shared/model/city-entity.model';

export interface ICityBuildingsEntity {
  id?: number;
  cityHall?: number;
  barracks?: number;
  wall?: number;
  goldMine?: number;
  sawMill?: number;
  oreMine?: number;
  ironworks?: number;
  tradingPost?: number;
  chapel?: number;
  university?: number;
  forge?: number;
  city?: ICityEntity;
}

export class CityBuildingsEntity implements ICityBuildingsEntity {
  constructor(
    public id?: number,
    public cityHall?: number,
    public barracks?: number,
    public wall?: number,
    public goldMine?: number,
    public sawMill?: number,
    public oreMine?: number,
    public ironworks?: number,
    public tradingPost?: number,
    public chapel?: number,
    public university?: number,
    public forge?: number,
    public city?: ICityEntity
  ) {}
}
