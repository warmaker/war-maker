import { IFortress } from 'app/shared/model//fortress.model';
import { IField } from 'app/shared/model//field.model';

export const enum PointOfInterestType {
    RIVER_CROSSING = 'RIVER_CROSSING',
    FOREST = 'FOREST',
    DESERT = 'DESERT',
    RUINS = 'RUINS'
}

export interface IPointOfInterest {
    id?: number;
    type?: PointOfInterestType;
    fortress?: IFortress;
    location?: IField;
}

export class PointOfInterest implements IPointOfInterest {
    constructor(public id?: number, public type?: PointOfInterestType, public fortress?: IFortress, public location?: IField) {}
}
