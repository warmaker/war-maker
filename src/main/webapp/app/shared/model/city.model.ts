import { IArmy } from 'app/shared/model//army.model';
import { ICityBuildings } from 'app/shared/model//city-buildings.model';
import { IResources } from 'app/shared/model//resources.model';
import { IField } from 'app/shared/model//field.model';
import { IProfile } from 'app/shared/model//profile.model';

export const enum CityType {
    PLAYER = 'PLAYER',
    RENEGATES = 'RENEGATES'
}

export interface ICity {
    id?: number;
    cityName?: string;
    cityType?: CityType;
    recruitmentModifier?: number;
    buildSpeedModifier?: number;
    army?: IArmy;
    buildings?: ICityBuildings;
    storedResources?: IResources;
    location?: IField;
    owner?: IProfile;
}

export class City implements ICity {
    constructor(
        public id?: number,
        public cityName?: string,
        public cityType?: CityType,
        public recruitmentModifier?: number,
        public buildSpeedModifier?: number,
        public army?: IArmy,
        public buildings?: ICityBuildings,
        public storedResources?: IResources,
        public location?: IField,
        public owner?: IProfile
    ) {}
}
