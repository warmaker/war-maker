import { IUnitsEntity } from 'app/shared/model/units-entity.model';
import { IWarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { ICityEntity } from 'app/shared/model/city-entity.model';

export interface IArmyEntity {
  id?: number;
  foodReq?: number;
  units?: IUnitsEntity;
  warParties?: IWarPartyEntity[];
  city?: ICityEntity;
}

export class ArmyEntity implements IArmyEntity {
  constructor(
    public id?: number,
    public foodReq?: number,
    public units?: IUnitsEntity,
    public warParties?: IWarPartyEntity[],
    public city?: ICityEntity
  ) {}
}
