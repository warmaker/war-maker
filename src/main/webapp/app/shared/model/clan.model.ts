import { IProfile } from 'app/shared/model//profile.model';

export interface IClan {
    id?: number;
    name?: string;
    emblemContentType?: string;
    emblem?: any;
    members?: IProfile;
}

export class Clan implements IClan {
    constructor(
        public id?: number,
        public name?: string,
        public emblemContentType?: string,
        public emblem?: any,
        public members?: IProfile
    ) {}
}
