import { IWorldEntity } from 'app/shared/model/world-entity.model';
import { FieldType } from 'app/shared/model/enumerations/field-type.model';

export interface IFieldEntity {
  id?: number;
  xCoordinate?: number;
  yCoordinate?: number;
  type?: FieldType;
  worldEntity?: IWorldEntity;
}

export class FieldEntity implements IFieldEntity {
  constructor(
    public id?: number,
    public xCoordinate?: number,
    public yCoordinate?: number,
    public type?: FieldType,
    public worldEntity?: IWorldEntity
  ) {}
}
