import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IResearchesEntity, ResearchesEntity } from 'app/shared/model/researches-entity.model';
import { ResearchesEntityService } from './researches-entity.service';

@Component({
  selector: 'jhi-researches-entity-update',
  templateUrl: './researches-entity-update.component.html',
})
export class ResearchesEntityUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    infantryVitality: [],
    cavalryVitality: [],
    lightArmorValue: [],
    heavyArmorValue: [],
    meleeAttack: [],
    rangedAttack: [],
    infantrySpeed: [],
    cavalrySpeed: [],
    cityBuildingSpeed: [],
    recruitmentSpeed: [],
    scouting: [],
  });

  constructor(
    protected researchesEntityService: ResearchesEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ researchesEntity }) => {
      this.updateForm(researchesEntity);
    });
  }

  updateForm(researchesEntity: IResearchesEntity): void {
    this.editForm.patchValue({
      id: researchesEntity.id,
      infantryVitality: researchesEntity.infantryVitality,
      cavalryVitality: researchesEntity.cavalryVitality,
      lightArmorValue: researchesEntity.lightArmorValue,
      heavyArmorValue: researchesEntity.heavyArmorValue,
      meleeAttack: researchesEntity.meleeAttack,
      rangedAttack: researchesEntity.rangedAttack,
      infantrySpeed: researchesEntity.infantrySpeed,
      cavalrySpeed: researchesEntity.cavalrySpeed,
      cityBuildingSpeed: researchesEntity.cityBuildingSpeed,
      recruitmentSpeed: researchesEntity.recruitmentSpeed,
      scouting: researchesEntity.scouting,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const researchesEntity = this.createFromForm();
    if (researchesEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.researchesEntityService.update(researchesEntity));
    } else {
      this.subscribeToSaveResponse(this.researchesEntityService.create(researchesEntity));
    }
  }

  private createFromForm(): IResearchesEntity {
    return {
      ...new ResearchesEntity(),
      id: this.editForm.get(['id'])!.value,
      infantryVitality: this.editForm.get(['infantryVitality'])!.value,
      cavalryVitality: this.editForm.get(['cavalryVitality'])!.value,
      lightArmorValue: this.editForm.get(['lightArmorValue'])!.value,
      heavyArmorValue: this.editForm.get(['heavyArmorValue'])!.value,
      meleeAttack: this.editForm.get(['meleeAttack'])!.value,
      rangedAttack: this.editForm.get(['rangedAttack'])!.value,
      infantrySpeed: this.editForm.get(['infantrySpeed'])!.value,
      cavalrySpeed: this.editForm.get(['cavalrySpeed'])!.value,
      cityBuildingSpeed: this.editForm.get(['cityBuildingSpeed'])!.value,
      recruitmentSpeed: this.editForm.get(['recruitmentSpeed'])!.value,
      scouting: this.editForm.get(['scouting'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IResearchesEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
