import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { ResearchesEntityComponent } from './researches-entity.component';
import { ResearchesEntityDetailComponent } from './researches-entity-detail.component';
import { ResearchesEntityUpdateComponent } from './researches-entity-update.component';
import { ResearchesEntityDeleteDialogComponent } from './researches-entity-delete-dialog.component';
import { researchesEntityRoute } from './researches-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(researchesEntityRoute)],
  declarations: [
    ResearchesEntityComponent,
    ResearchesEntityDetailComponent,
    ResearchesEntityUpdateComponent,
    ResearchesEntityDeleteDialogComponent,
  ],
  entryComponents: [ResearchesEntityDeleteDialogComponent],
})
export class WarMakerResearchesEntityModule {}
