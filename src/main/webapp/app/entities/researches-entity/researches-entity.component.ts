import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IResearchesEntity } from 'app/shared/model/researches-entity.model';
import { ResearchesEntityService } from './researches-entity.service';
import { ResearchesEntityDeleteDialogComponent } from './researches-entity-delete-dialog.component';

@Component({
  selector: 'jhi-researches-entity',
  templateUrl: './researches-entity.component.html',
})
export class ResearchesEntityComponent implements OnInit, OnDestroy {
  researchesEntities?: IResearchesEntity[];
  eventSubscriber?: Subscription;

  constructor(
    protected researchesEntityService: ResearchesEntityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.researchesEntityService.query().subscribe((res: HttpResponse<IResearchesEntity[]>) => (this.researchesEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInResearchesEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IResearchesEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInResearchesEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('researchesEntityListModification', () => this.loadAll());
  }

  delete(researchesEntity: IResearchesEntity): void {
    const modalRef = this.modalService.open(ResearchesEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.researchesEntity = researchesEntity;
  }
}
