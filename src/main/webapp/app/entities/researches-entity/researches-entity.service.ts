import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IResearchesEntity } from 'app/shared/model/researches-entity.model';

type EntityResponseType = HttpResponse<IResearchesEntity>;
type EntityArrayResponseType = HttpResponse<IResearchesEntity[]>;

@Injectable({ providedIn: 'root' })
export class ResearchesEntityService {
  public resourceUrl = SERVER_API_URL + 'api/researches-entities';

  constructor(protected http: HttpClient) {}

  create(researchesEntity: IResearchesEntity): Observable<EntityResponseType> {
    return this.http.post<IResearchesEntity>(this.resourceUrl, researchesEntity, { observe: 'response' });
  }

  update(researchesEntity: IResearchesEntity): Observable<EntityResponseType> {
    return this.http.put<IResearchesEntity>(this.resourceUrl, researchesEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IResearchesEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IResearchesEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
