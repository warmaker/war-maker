import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IResearchesEntity } from 'app/shared/model/researches-entity.model';

@Component({
  selector: 'jhi-researches-entity-detail',
  templateUrl: './researches-entity-detail.component.html',
})
export class ResearchesEntityDetailComponent implements OnInit {
  researchesEntity: IResearchesEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ researchesEntity }) => (this.researchesEntity = researchesEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
