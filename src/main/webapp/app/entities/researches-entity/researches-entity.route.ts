import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IResearchesEntity, ResearchesEntity } from 'app/shared/model/researches-entity.model';
import { ResearchesEntityService } from './researches-entity.service';
import { ResearchesEntityComponent } from './researches-entity.component';
import { ResearchesEntityDetailComponent } from './researches-entity-detail.component';
import { ResearchesEntityUpdateComponent } from './researches-entity-update.component';

@Injectable({ providedIn: 'root' })
export class ResearchesEntityResolve implements Resolve<IResearchesEntity> {
  constructor(private service: ResearchesEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IResearchesEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((researchesEntity: HttpResponse<ResearchesEntity>) => {
          if (researchesEntity.body) {
            return of(researchesEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ResearchesEntity());
  }
}

export const researchesEntityRoute: Routes = [
  {
    path: '',
    component: ResearchesEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.researchesEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ResearchesEntityDetailComponent,
    resolve: {
      researchesEntity: ResearchesEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.researchesEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ResearchesEntityUpdateComponent,
    resolve: {
      researchesEntity: ResearchesEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.researchesEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ResearchesEntityUpdateComponent,
    resolve: {
      researchesEntity: ResearchesEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.researchesEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
