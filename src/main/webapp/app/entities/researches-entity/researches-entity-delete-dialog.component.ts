import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IResearchesEntity } from 'app/shared/model/researches-entity.model';
import { ResearchesEntityService } from './researches-entity.service';

@Component({
  templateUrl: './researches-entity-delete-dialog.component.html',
})
export class ResearchesEntityDeleteDialogComponent {
  researchesEntity?: IResearchesEntity;

  constructor(
    protected researchesEntityService: ResearchesEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.researchesEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('researchesEntityListModification');
      this.activeModal.close();
    });
  }
}
