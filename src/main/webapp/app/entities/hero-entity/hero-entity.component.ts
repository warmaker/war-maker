import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IHeroEntity } from 'app/shared/model/hero-entity.model';
import { HeroEntityService } from './hero-entity.service';
import { HeroEntityDeleteDialogComponent } from './hero-entity-delete-dialog.component';

@Component({
  selector: 'jhi-hero-entity',
  templateUrl: './hero-entity.component.html',
})
export class HeroEntityComponent implements OnInit, OnDestroy {
  heroEntities?: IHeroEntity[];
  eventSubscriber?: Subscription;

  constructor(protected heroEntityService: HeroEntityService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.heroEntityService.query().subscribe((res: HttpResponse<IHeroEntity[]>) => (this.heroEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInHeroEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IHeroEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInHeroEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('heroEntityListModification', () => this.loadAll());
  }

  delete(heroEntity: IHeroEntity): void {
    const modalRef = this.modalService.open(HeroEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.heroEntity = heroEntity;
  }
}
