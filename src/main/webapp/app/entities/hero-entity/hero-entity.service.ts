import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IHeroEntity } from 'app/shared/model/hero-entity.model';

type EntityResponseType = HttpResponse<IHeroEntity>;
type EntityArrayResponseType = HttpResponse<IHeroEntity[]>;

@Injectable({ providedIn: 'root' })
export class HeroEntityService {
  public resourceUrl = SERVER_API_URL + 'api/hero-entities';

  constructor(protected http: HttpClient) {}

  create(heroEntity: IHeroEntity): Observable<EntityResponseType> {
    return this.http.post<IHeroEntity>(this.resourceUrl, heroEntity, { observe: 'response' });
  }

  update(heroEntity: IHeroEntity): Observable<EntityResponseType> {
    return this.http.put<IHeroEntity>(this.resourceUrl, heroEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IHeroEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IHeroEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
