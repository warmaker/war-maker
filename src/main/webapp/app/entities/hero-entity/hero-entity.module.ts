import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { HeroEntityComponent } from './hero-entity.component';
import { HeroEntityDetailComponent } from './hero-entity-detail.component';
import { HeroEntityUpdateComponent } from './hero-entity-update.component';
import { HeroEntityDeleteDialogComponent } from './hero-entity-delete-dialog.component';
import { heroEntityRoute } from './hero-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(heroEntityRoute)],
  declarations: [HeroEntityComponent, HeroEntityDetailComponent, HeroEntityUpdateComponent, HeroEntityDeleteDialogComponent],
  entryComponents: [HeroEntityDeleteDialogComponent],
})
export class WarMakerHeroEntityModule {}
