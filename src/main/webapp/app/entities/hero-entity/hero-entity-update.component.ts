import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IHeroEntity, HeroEntity } from 'app/shared/model/hero-entity.model';
import { HeroEntityService } from './hero-entity.service';
import { IWarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { WarPartyEntityService } from 'app/entities/war-party-entity/war-party-entity.service';
import { ICityEntity } from 'app/shared/model/city-entity.model';
import { CityEntityService } from 'app/entities/city-entity/city-entity.service';
import { IProfileEntity } from 'app/shared/model/profile-entity.model';
import { ProfileEntityService } from 'app/entities/profile-entity/profile-entity.service';

type SelectableEntity = IWarPartyEntity | ICityEntity | IProfileEntity;

@Component({
  selector: 'jhi-hero-entity-update',
  templateUrl: './hero-entity-update.component.html',
})
export class HeroEntityUpdateComponent implements OnInit {
  isSaving = false;
  supportedwarparties: IWarPartyEntity[] = [];
  supportedcities: ICityEntity[] = [];
  profileentities: IProfileEntity[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    type: [],
    level: [],
    supportedWarParty: [],
    supportedCity: [],
    owner: [],
  });

  constructor(
    protected heroEntityService: HeroEntityService,
    protected warPartyEntityService: WarPartyEntityService,
    protected cityEntityService: CityEntityService,
    protected profileEntityService: ProfileEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ heroEntity }) => {
      this.updateForm(heroEntity);

      this.warPartyEntityService
        .query({ filter: 'heroentity-is-null' })
        .pipe(
          map((res: HttpResponse<IWarPartyEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IWarPartyEntity[]) => {
          if (!heroEntity.supportedWarParty || !heroEntity.supportedWarParty.id) {
            this.supportedwarparties = resBody;
          } else {
            this.warPartyEntityService
              .find(heroEntity.supportedWarParty.id)
              .pipe(
                map((subRes: HttpResponse<IWarPartyEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IWarPartyEntity[]) => (this.supportedwarparties = concatRes));
          }
        });

      this.cityEntityService
        .query({ filter: 'heroentity-is-null' })
        .pipe(
          map((res: HttpResponse<ICityEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: ICityEntity[]) => {
          if (!heroEntity.supportedCity || !heroEntity.supportedCity.id) {
            this.supportedcities = resBody;
          } else {
            this.cityEntityService
              .find(heroEntity.supportedCity.id)
              .pipe(
                map((subRes: HttpResponse<ICityEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: ICityEntity[]) => (this.supportedcities = concatRes));
          }
        });

      this.profileEntityService.query().subscribe((res: HttpResponse<IProfileEntity[]>) => (this.profileentities = res.body || []));
    });
  }

  updateForm(heroEntity: IHeroEntity): void {
    this.editForm.patchValue({
      id: heroEntity.id,
      name: heroEntity.name,
      type: heroEntity.type,
      level: heroEntity.level,
      supportedWarParty: heroEntity.supportedWarParty,
      supportedCity: heroEntity.supportedCity,
      owner: heroEntity.owner,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const heroEntity = this.createFromForm();
    if (heroEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.heroEntityService.update(heroEntity));
    } else {
      this.subscribeToSaveResponse(this.heroEntityService.create(heroEntity));
    }
  }

  private createFromForm(): IHeroEntity {
    return {
      ...new HeroEntity(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      type: this.editForm.get(['type'])!.value,
      level: this.editForm.get(['level'])!.value,
      supportedWarParty: this.editForm.get(['supportedWarParty'])!.value,
      supportedCity: this.editForm.get(['supportedCity'])!.value,
      owner: this.editForm.get(['owner'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHeroEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
