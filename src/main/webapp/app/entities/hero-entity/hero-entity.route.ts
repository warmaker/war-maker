import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IHeroEntity, HeroEntity } from 'app/shared/model/hero-entity.model';
import { HeroEntityService } from './hero-entity.service';
import { HeroEntityComponent } from './hero-entity.component';
import { HeroEntityDetailComponent } from './hero-entity-detail.component';
import { HeroEntityUpdateComponent } from './hero-entity-update.component';

@Injectable({ providedIn: 'root' })
export class HeroEntityResolve implements Resolve<IHeroEntity> {
  constructor(private service: HeroEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IHeroEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((heroEntity: HttpResponse<HeroEntity>) => {
          if (heroEntity.body) {
            return of(heroEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new HeroEntity());
  }
}

export const heroEntityRoute: Routes = [
  {
    path: '',
    component: HeroEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.heroEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: HeroEntityDetailComponent,
    resolve: {
      heroEntity: HeroEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.heroEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: HeroEntityUpdateComponent,
    resolve: {
      heroEntity: HeroEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.heroEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: HeroEntityUpdateComponent,
    resolve: {
      heroEntity: HeroEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.heroEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
