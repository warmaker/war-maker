import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHeroEntity } from 'app/shared/model/hero-entity.model';
import { HeroEntityService } from './hero-entity.service';

@Component({
  templateUrl: './hero-entity-delete-dialog.component.html',
})
export class HeroEntityDeleteDialogComponent {
  heroEntity?: IHeroEntity;

  constructor(
    protected heroEntityService: HeroEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.heroEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('heroEntityListModification');
      this.activeModal.close();
    });
  }
}
