import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IHeroEntity } from 'app/shared/model/hero-entity.model';

@Component({
  selector: 'jhi-hero-entity-detail',
  templateUrl: './hero-entity-detail.component.html',
})
export class HeroEntityDetailComponent implements OnInit {
  heroEntity: IHeroEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ heroEntity }) => (this.heroEntity = heroEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
