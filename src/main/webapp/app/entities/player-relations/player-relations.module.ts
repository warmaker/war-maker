import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { PlayerRelationsComponent } from './player-relations.component';
import { PlayerRelationsDetailComponent } from './player-relations-detail.component';
import { PlayerRelationsUpdateComponent } from './player-relations-update.component';
import { PlayerRelationsDeleteDialogComponent } from './player-relations-delete-dialog.component';
import { playerRelationsRoute } from './player-relations.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(playerRelationsRoute)],
  declarations: [
    PlayerRelationsComponent,
    PlayerRelationsDetailComponent,
    PlayerRelationsUpdateComponent,
    PlayerRelationsDeleteDialogComponent,
  ],
  entryComponents: [PlayerRelationsDeleteDialogComponent],
})
export class WarMakerPlayerRelationsModule {}
