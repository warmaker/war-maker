import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPlayerRelations } from 'app/shared/model/player-relations.model';

@Component({
  selector: 'jhi-player-relations-detail',
  templateUrl: './player-relations-detail.component.html',
})
export class PlayerRelationsDetailComponent implements OnInit {
  playerRelations: IPlayerRelations | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ playerRelations }) => (this.playerRelations = playerRelations));
  }

  previousState(): void {
    window.history.back();
  }
}
