import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPlayerRelations, PlayerRelations } from 'app/shared/model/player-relations.model';
import { PlayerRelationsService } from './player-relations.service';
import { PlayerRelationsComponent } from './player-relations.component';
import { PlayerRelationsDetailComponent } from './player-relations-detail.component';
import { PlayerRelationsUpdateComponent } from './player-relations-update.component';

@Injectable({ providedIn: 'root' })
export class PlayerRelationsResolve implements Resolve<IPlayerRelations> {
  constructor(private service: PlayerRelationsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPlayerRelations> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((playerRelations: HttpResponse<PlayerRelations>) => {
          if (playerRelations.body) {
            return of(playerRelations.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PlayerRelations());
  }
}

export const playerRelationsRoute: Routes = [
  {
    path: '',
    component: PlayerRelationsComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.playerRelations.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PlayerRelationsDetailComponent,
    resolve: {
      playerRelations: PlayerRelationsResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.playerRelations.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PlayerRelationsUpdateComponent,
    resolve: {
      playerRelations: PlayerRelationsResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.playerRelations.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PlayerRelationsUpdateComponent,
    resolve: {
      playerRelations: PlayerRelationsResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.playerRelations.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
