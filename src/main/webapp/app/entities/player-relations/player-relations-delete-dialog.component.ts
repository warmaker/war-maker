import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPlayerRelations } from 'app/shared/model/player-relations.model';
import { PlayerRelationsService } from './player-relations.service';

@Component({
  templateUrl: './player-relations-delete-dialog.component.html',
})
export class PlayerRelationsDeleteDialogComponent {
  playerRelations?: IPlayerRelations;

  constructor(
    protected playerRelationsService: PlayerRelationsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.playerRelationsService.delete(id).subscribe(() => {
      this.eventManager.broadcast('playerRelationsListModification');
      this.activeModal.close();
    });
  }
}
