import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPlayerRelations } from 'app/shared/model/player-relations.model';
import { PlayerRelationsService } from './player-relations.service';
import { PlayerRelationsDeleteDialogComponent } from './player-relations-delete-dialog.component';

@Component({
  selector: 'jhi-player-relations',
  templateUrl: './player-relations.component.html',
})
export class PlayerRelationsComponent implements OnInit, OnDestroy {
  playerRelations?: IPlayerRelations[];
  eventSubscriber?: Subscription;

  constructor(
    protected playerRelationsService: PlayerRelationsService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.playerRelationsService.query().subscribe((res: HttpResponse<IPlayerRelations[]>) => (this.playerRelations = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPlayerRelations();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPlayerRelations): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPlayerRelations(): void {
    this.eventSubscriber = this.eventManager.subscribe('playerRelationsListModification', () => this.loadAll());
  }

  delete(playerRelations: IPlayerRelations): void {
    const modalRef = this.modalService.open(PlayerRelationsDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.playerRelations = playerRelations;
  }
}
