import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPlayerRelations, PlayerRelations } from 'app/shared/model/player-relations.model';
import { PlayerRelationsService } from './player-relations.service';
import { IProfileEntity } from 'app/shared/model/profile-entity.model';
import { ProfileEntityService } from 'app/entities/profile-entity/profile-entity.service';

@Component({
  selector: 'jhi-player-relations-update',
  templateUrl: './player-relations-update.component.html',
})
export class PlayerRelationsUpdateComponent implements OnInit {
  isSaving = false;
  profileentities: IProfileEntity[] = [];

  editForm = this.fb.group({
    id: [],
    relation: [],
    targetPlayer: [],
    owner: [],
  });

  constructor(
    protected playerRelationsService: PlayerRelationsService,
    protected profileEntityService: ProfileEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ playerRelations }) => {
      this.updateForm(playerRelations);

      this.profileEntityService.query().subscribe((res: HttpResponse<IProfileEntity[]>) => (this.profileentities = res.body || []));
    });
  }

  updateForm(playerRelations: IPlayerRelations): void {
    this.editForm.patchValue({
      id: playerRelations.id,
      relation: playerRelations.relation,
      targetPlayer: playerRelations.targetPlayer,
      owner: playerRelations.owner,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const playerRelations = this.createFromForm();
    if (playerRelations.id !== undefined) {
      this.subscribeToSaveResponse(this.playerRelationsService.update(playerRelations));
    } else {
      this.subscribeToSaveResponse(this.playerRelationsService.create(playerRelations));
    }
  }

  private createFromForm(): IPlayerRelations {
    return {
      ...new PlayerRelations(),
      id: this.editForm.get(['id'])!.value,
      relation: this.editForm.get(['relation'])!.value,
      targetPlayer: this.editForm.get(['targetPlayer'])!.value,
      owner: this.editForm.get(['owner'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPlayerRelations>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IProfileEntity): any {
    return item.id;
  }
}
