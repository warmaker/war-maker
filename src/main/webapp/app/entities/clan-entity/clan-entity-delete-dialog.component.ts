import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IClanEntity } from 'app/shared/model/clan-entity.model';
import { ClanEntityService } from './clan-entity.service';

@Component({
  templateUrl: './clan-entity-delete-dialog.component.html',
})
export class ClanEntityDeleteDialogComponent {
  clanEntity?: IClanEntity;

  constructor(
    protected clanEntityService: ClanEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.clanEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('clanEntityListModification');
      this.activeModal.close();
    });
  }
}
