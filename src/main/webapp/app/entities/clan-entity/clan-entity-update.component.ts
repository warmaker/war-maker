import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IClanEntity, ClanEntity } from 'app/shared/model/clan-entity.model';
import { ClanEntityService } from './clan-entity.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { IProfileEntity } from 'app/shared/model/profile-entity.model';
import { ProfileEntityService } from 'app/entities/profile-entity/profile-entity.service';

@Component({
  selector: 'jhi-clan-entity-update',
  templateUrl: './clan-entity-update.component.html',
})
export class ClanEntityUpdateComponent implements OnInit {
  isSaving = false;
  members: IProfileEntity[] = [];

  editForm = this.fb.group({
    id: [],
    name: [],
    emblem: [],
    emblemContentType: [],
    members: [],
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected clanEntityService: ClanEntityService,
    protected profileEntityService: ProfileEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ clanEntity }) => {
      this.updateForm(clanEntity);

      this.profileEntityService
        .query({ filter: 'clan-is-null' })
        .pipe(
          map((res: HttpResponse<IProfileEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IProfileEntity[]) => {
          if (!clanEntity.members || !clanEntity.members.id) {
            this.members = resBody;
          } else {
            this.profileEntityService
              .find(clanEntity.members.id)
              .pipe(
                map((subRes: HttpResponse<IProfileEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IProfileEntity[]) => (this.members = concatRes));
          }
        });
    });
  }

  updateForm(clanEntity: IClanEntity): void {
    this.editForm.patchValue({
      id: clanEntity.id,
      name: clanEntity.name,
      emblem: clanEntity.emblem,
      emblemContentType: clanEntity.emblemContentType,
      members: clanEntity.members,
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: any, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('warMakerApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const clanEntity = this.createFromForm();
    if (clanEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.clanEntityService.update(clanEntity));
    } else {
      this.subscribeToSaveResponse(this.clanEntityService.create(clanEntity));
    }
  }

  private createFromForm(): IClanEntity {
    return {
      ...new ClanEntity(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      emblemContentType: this.editForm.get(['emblemContentType'])!.value,
      emblem: this.editForm.get(['emblem'])!.value,
      members: this.editForm.get(['members'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IClanEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IProfileEntity): any {
    return item.id;
  }
}
