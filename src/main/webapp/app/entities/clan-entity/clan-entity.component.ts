import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IClanEntity } from 'app/shared/model/clan-entity.model';
import { ClanEntityService } from './clan-entity.service';
import { ClanEntityDeleteDialogComponent } from './clan-entity-delete-dialog.component';

@Component({
  selector: 'jhi-clan-entity',
  templateUrl: './clan-entity.component.html',
})
export class ClanEntityComponent implements OnInit, OnDestroy {
  clanEntities?: IClanEntity[];
  eventSubscriber?: Subscription;

  constructor(
    protected clanEntityService: ClanEntityService,
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.clanEntityService.query().subscribe((res: HttpResponse<IClanEntity[]>) => (this.clanEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInClanEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IClanEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType = '', base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInClanEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('clanEntityListModification', () => this.loadAll());
  }

  delete(clanEntity: IClanEntity): void {
    const modalRef = this.modalService.open(ClanEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.clanEntity = clanEntity;
  }
}
