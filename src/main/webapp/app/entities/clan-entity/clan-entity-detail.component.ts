import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IClanEntity } from 'app/shared/model/clan-entity.model';

@Component({
  selector: 'jhi-clan-entity-detail',
  templateUrl: './clan-entity-detail.component.html',
})
export class ClanEntityDetailComponent implements OnInit {
  clanEntity: IClanEntity | null = null;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ clanEntity }) => (this.clanEntity = clanEntity));
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType = '', base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  previousState(): void {
    window.history.back();
  }
}
