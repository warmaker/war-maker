import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IClanEntity, ClanEntity } from 'app/shared/model/clan-entity.model';
import { ClanEntityService } from './clan-entity.service';
import { ClanEntityComponent } from './clan-entity.component';
import { ClanEntityDetailComponent } from './clan-entity-detail.component';
import { ClanEntityUpdateComponent } from './clan-entity-update.component';

@Injectable({ providedIn: 'root' })
export class ClanEntityResolve implements Resolve<IClanEntity> {
  constructor(private service: ClanEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IClanEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((clanEntity: HttpResponse<ClanEntity>) => {
          if (clanEntity.body) {
            return of(clanEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ClanEntity());
  }
}

export const clanEntityRoute: Routes = [
  {
    path: '',
    component: ClanEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.clanEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ClanEntityDetailComponent,
    resolve: {
      clanEntity: ClanEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.clanEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ClanEntityUpdateComponent,
    resolve: {
      clanEntity: ClanEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.clanEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ClanEntityUpdateComponent,
    resolve: {
      clanEntity: ClanEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.clanEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
