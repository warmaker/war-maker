import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { ClanEntityComponent } from './clan-entity.component';
import { ClanEntityDetailComponent } from './clan-entity-detail.component';
import { ClanEntityUpdateComponent } from './clan-entity-update.component';
import { ClanEntityDeleteDialogComponent } from './clan-entity-delete-dialog.component';
import { clanEntityRoute } from './clan-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(clanEntityRoute)],
  declarations: [ClanEntityComponent, ClanEntityDetailComponent, ClanEntityUpdateComponent, ClanEntityDeleteDialogComponent],
  entryComponents: [ClanEntityDeleteDialogComponent],
})
export class WarMakerClanEntityModule {}
