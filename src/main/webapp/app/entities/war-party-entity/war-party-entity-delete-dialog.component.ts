import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { WarPartyEntityService } from './war-party-entity.service';

@Component({
  templateUrl: './war-party-entity-delete-dialog.component.html',
})
export class WarPartyEntityDeleteDialogComponent {
  warPartyEntity?: IWarPartyEntity;

  constructor(
    protected warPartyEntityService: WarPartyEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.warPartyEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('warPartyEntityListModification');
      this.activeModal.close();
    });
  }
}
