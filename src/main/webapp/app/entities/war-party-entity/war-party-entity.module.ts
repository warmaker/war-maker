import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { WarPartyEntityComponent } from './war-party-entity.component';
import { WarPartyEntityDetailComponent } from './war-party-entity-detail.component';
import { WarPartyEntityUpdateComponent } from './war-party-entity-update.component';
import { WarPartyEntityDeleteDialogComponent } from './war-party-entity-delete-dialog.component';
import { warPartyEntityRoute } from './war-party-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(warPartyEntityRoute)],
  declarations: [
    WarPartyEntityComponent,
    WarPartyEntityDetailComponent,
    WarPartyEntityUpdateComponent,
    WarPartyEntityDeleteDialogComponent,
  ],
  entryComponents: [WarPartyEntityDeleteDialogComponent],
})
export class WarMakerWarPartyEntityModule {}
