import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IWarPartyEntity, WarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { WarPartyEntityService } from './war-party-entity.service';
import { WarPartyEntityComponent } from './war-party-entity.component';
import { WarPartyEntityDetailComponent } from './war-party-entity-detail.component';
import { WarPartyEntityUpdateComponent } from './war-party-entity-update.component';

@Injectable({ providedIn: 'root' })
export class WarPartyEntityResolve implements Resolve<IWarPartyEntity> {
  constructor(private service: WarPartyEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWarPartyEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((warPartyEntity: HttpResponse<WarPartyEntity>) => {
          if (warPartyEntity.body) {
            return of(warPartyEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new WarPartyEntity());
  }
}

export const warPartyEntityRoute: Routes = [
  {
    path: '',
    component: WarPartyEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.warPartyEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: WarPartyEntityDetailComponent,
    resolve: {
      warPartyEntity: WarPartyEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.warPartyEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: WarPartyEntityUpdateComponent,
    resolve: {
      warPartyEntity: WarPartyEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.warPartyEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: WarPartyEntityUpdateComponent,
    resolve: {
      warPartyEntity: WarPartyEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.warPartyEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
