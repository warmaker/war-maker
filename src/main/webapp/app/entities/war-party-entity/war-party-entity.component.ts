import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { WarPartyEntityService } from './war-party-entity.service';
import { WarPartyEntityDeleteDialogComponent } from './war-party-entity-delete-dialog.component';

@Component({
  selector: 'jhi-war-party-entity',
  templateUrl: './war-party-entity.component.html',
})
export class WarPartyEntityComponent implements OnInit, OnDestroy {
  warPartyEntities?: IWarPartyEntity[];
  eventSubscriber?: Subscription;

  constructor(
    protected warPartyEntityService: WarPartyEntityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.warPartyEntityService.query().subscribe((res: HttpResponse<IWarPartyEntity[]>) => (this.warPartyEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInWarPartyEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IWarPartyEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInWarPartyEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('warPartyEntityListModification', () => this.loadAll());
  }

  delete(warPartyEntity: IWarPartyEntity): void {
    const modalRef = this.modalService.open(WarPartyEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.warPartyEntity = warPartyEntity;
  }
}
