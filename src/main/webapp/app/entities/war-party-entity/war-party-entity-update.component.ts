import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IWarPartyEntity, WarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { WarPartyEntityService } from './war-party-entity.service';
import { IResourcesEntity } from 'app/shared/model/resources-entity.model';
import { ResourcesEntityService } from 'app/entities/resources-entity/resources-entity.service';
import { IUnitsEntity } from 'app/shared/model/units-entity.model';
import { UnitsEntityService } from 'app/entities/units-entity/units-entity.service';
import { IFieldEntity } from 'app/shared/model/field-entity.model';
import { FieldEntityService } from 'app/entities/field-entity/field-entity.service';
import { IArmyEntity } from 'app/shared/model/army-entity.model';
import { ArmyEntityService } from 'app/entities/army-entity/army-entity.service';

type SelectableEntity = IResourcesEntity | IUnitsEntity | IFieldEntity | IArmyEntity;

@Component({
  selector: 'jhi-war-party-entity-update',
  templateUrl: './war-party-entity-update.component.html',
})
export class WarPartyEntityUpdateComponent implements OnInit {
  isSaving = false;
  resourcescarrieds: IResourcesEntity[] = [];
  units: IUnitsEntity[] = [];
  fieldentities: IFieldEntity[] = [];
  armyentities: IArmyEntity[] = [];

  editForm = this.fb.group({
    id: [],
    speed: [],
    capacity: [],
    state: [],
    resourcesCarried: [],
    units: [],
    position: [null, Validators.required],
    armyEntity: [],
  });

  constructor(
    protected warPartyEntityService: WarPartyEntityService,
    protected resourcesEntityService: ResourcesEntityService,
    protected unitsEntityService: UnitsEntityService,
    protected fieldEntityService: FieldEntityService,
    protected armyEntityService: ArmyEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ warPartyEntity }) => {
      this.updateForm(warPartyEntity);

      this.resourcesEntityService
        .query({ filter: 'warpartyentity-is-null' })
        .pipe(
          map((res: HttpResponse<IResourcesEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IResourcesEntity[]) => {
          if (!warPartyEntity.resourcesCarried || !warPartyEntity.resourcesCarried.id) {
            this.resourcescarrieds = resBody;
          } else {
            this.resourcesEntityService
              .find(warPartyEntity.resourcesCarried.id)
              .pipe(
                map((subRes: HttpResponse<IResourcesEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IResourcesEntity[]) => (this.resourcescarrieds = concatRes));
          }
        });

      this.unitsEntityService
        .query({ filter: 'warpartyentity-is-null' })
        .pipe(
          map((res: HttpResponse<IUnitsEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IUnitsEntity[]) => {
          if (!warPartyEntity.units || !warPartyEntity.units.id) {
            this.units = resBody;
          } else {
            this.unitsEntityService
              .find(warPartyEntity.units.id)
              .pipe(
                map((subRes: HttpResponse<IUnitsEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IUnitsEntity[]) => (this.units = concatRes));
          }
        });

      this.fieldEntityService.query().subscribe((res: HttpResponse<IFieldEntity[]>) => (this.fieldentities = res.body || []));

      this.armyEntityService.query().subscribe((res: HttpResponse<IArmyEntity[]>) => (this.armyentities = res.body || []));
    });
  }

  updateForm(warPartyEntity: IWarPartyEntity): void {
    this.editForm.patchValue({
      id: warPartyEntity.id,
      speed: warPartyEntity.speed,
      capacity: warPartyEntity.capacity,
      state: warPartyEntity.state,
      resourcesCarried: warPartyEntity.resourcesCarried,
      units: warPartyEntity.units,
      position: warPartyEntity.position,
      armyEntity: warPartyEntity.armyEntity,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const warPartyEntity = this.createFromForm();
    if (warPartyEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.warPartyEntityService.update(warPartyEntity));
    } else {
      this.subscribeToSaveResponse(this.warPartyEntityService.create(warPartyEntity));
    }
  }

  private createFromForm(): IWarPartyEntity {
    return {
      ...new WarPartyEntity(),
      id: this.editForm.get(['id'])!.value,
      speed: this.editForm.get(['speed'])!.value,
      capacity: this.editForm.get(['capacity'])!.value,
      state: this.editForm.get(['state'])!.value,
      resourcesCarried: this.editForm.get(['resourcesCarried'])!.value,
      units: this.editForm.get(['units'])!.value,
      position: this.editForm.get(['position'])!.value,
      armyEntity: this.editForm.get(['armyEntity'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWarPartyEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
