import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWarPartyEntity } from 'app/shared/model/war-party-entity.model';

@Component({
  selector: 'jhi-war-party-entity-detail',
  templateUrl: './war-party-entity-detail.component.html',
})
export class WarPartyEntityDetailComponent implements OnInit {
  warPartyEntity: IWarPartyEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ warPartyEntity }) => (this.warPartyEntity = warPartyEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
