import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAllianceEntity, AllianceEntity } from 'app/shared/model/alliance-entity.model';
import { AllianceEntityService } from './alliance-entity.service';

@Component({
  selector: 'jhi-alliance-entity-update',
  templateUrl: './alliance-entity-update.component.html',
})
export class AllianceEntityUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
  });

  constructor(protected allianceEntityService: AllianceEntityService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ allianceEntity }) => {
      this.updateForm(allianceEntity);
    });
  }

  updateForm(allianceEntity: IAllianceEntity): void {
    this.editForm.patchValue({
      id: allianceEntity.id,
      name: allianceEntity.name,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const allianceEntity = this.createFromForm();
    if (allianceEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.allianceEntityService.update(allianceEntity));
    } else {
      this.subscribeToSaveResponse(this.allianceEntityService.create(allianceEntity));
    }
  }

  private createFromForm(): IAllianceEntity {
    return {
      ...new AllianceEntity(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAllianceEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
