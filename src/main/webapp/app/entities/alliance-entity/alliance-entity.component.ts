import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAllianceEntity } from 'app/shared/model/alliance-entity.model';
import { AllianceEntityService } from './alliance-entity.service';
import { AllianceEntityDeleteDialogComponent } from './alliance-entity-delete-dialog.component';

@Component({
  selector: 'jhi-alliance-entity',
  templateUrl: './alliance-entity.component.html',
})
export class AllianceEntityComponent implements OnInit, OnDestroy {
  allianceEntities?: IAllianceEntity[];
  eventSubscriber?: Subscription;

  constructor(
    protected allianceEntityService: AllianceEntityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.allianceEntityService.query().subscribe((res: HttpResponse<IAllianceEntity[]>) => (this.allianceEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAllianceEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAllianceEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAllianceEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('allianceEntityListModification', () => this.loadAll());
  }

  delete(allianceEntity: IAllianceEntity): void {
    const modalRef = this.modalService.open(AllianceEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.allianceEntity = allianceEntity;
  }
}
