import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { AllianceEntityComponent } from './alliance-entity.component';
import { AllianceEntityDetailComponent } from './alliance-entity-detail.component';
import { AllianceEntityUpdateComponent } from './alliance-entity-update.component';
import { AllianceEntityDeleteDialogComponent } from './alliance-entity-delete-dialog.component';
import { allianceEntityRoute } from './alliance-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(allianceEntityRoute)],
  declarations: [
    AllianceEntityComponent,
    AllianceEntityDetailComponent,
    AllianceEntityUpdateComponent,
    AllianceEntityDeleteDialogComponent,
  ],
  entryComponents: [AllianceEntityDeleteDialogComponent],
})
export class WarMakerAllianceEntityModule {}
