import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAllianceEntity, AllianceEntity } from 'app/shared/model/alliance-entity.model';
import { AllianceEntityService } from './alliance-entity.service';
import { AllianceEntityComponent } from './alliance-entity.component';
import { AllianceEntityDetailComponent } from './alliance-entity-detail.component';
import { AllianceEntityUpdateComponent } from './alliance-entity-update.component';

@Injectable({ providedIn: 'root' })
export class AllianceEntityResolve implements Resolve<IAllianceEntity> {
  constructor(private service: AllianceEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAllianceEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((allianceEntity: HttpResponse<AllianceEntity>) => {
          if (allianceEntity.body) {
            return of(allianceEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AllianceEntity());
  }
}

export const allianceEntityRoute: Routes = [
  {
    path: '',
    component: AllianceEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.allianceEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AllianceEntityDetailComponent,
    resolve: {
      allianceEntity: AllianceEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.allianceEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AllianceEntityUpdateComponent,
    resolve: {
      allianceEntity: AllianceEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.allianceEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AllianceEntityUpdateComponent,
    resolve: {
      allianceEntity: AllianceEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.allianceEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
