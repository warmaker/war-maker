import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAllianceEntity } from 'app/shared/model/alliance-entity.model';

@Component({
  selector: 'jhi-alliance-entity-detail',
  templateUrl: './alliance-entity-detail.component.html',
})
export class AllianceEntityDetailComponent implements OnInit {
  allianceEntity: IAllianceEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ allianceEntity }) => (this.allianceEntity = allianceEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
