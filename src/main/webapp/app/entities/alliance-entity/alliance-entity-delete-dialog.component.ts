import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAllianceEntity } from 'app/shared/model/alliance-entity.model';
import { AllianceEntityService } from './alliance-entity.service';

@Component({
  templateUrl: './alliance-entity-delete-dialog.component.html',
})
export class AllianceEntityDeleteDialogComponent {
  allianceEntity?: IAllianceEntity;

  constructor(
    protected allianceEntityService: AllianceEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.allianceEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('allianceEntityListModification');
      this.activeModal.close();
    });
  }
}
