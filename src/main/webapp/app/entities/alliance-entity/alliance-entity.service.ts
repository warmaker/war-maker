import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAllianceEntity } from 'app/shared/model/alliance-entity.model';

type EntityResponseType = HttpResponse<IAllianceEntity>;
type EntityArrayResponseType = HttpResponse<IAllianceEntity[]>;

@Injectable({ providedIn: 'root' })
export class AllianceEntityService {
  public resourceUrl = SERVER_API_URL + 'api/alliance-entities';

  constructor(protected http: HttpClient) {}

  create(allianceEntity: IAllianceEntity): Observable<EntityResponseType> {
    return this.http.post<IAllianceEntity>(this.resourceUrl, allianceEntity, { observe: 'response' });
  }

  update(allianceEntity: IAllianceEntity): Observable<EntityResponseType> {
    return this.http.put<IAllianceEntity>(this.resourceUrl, allianceEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAllianceEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAllianceEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
