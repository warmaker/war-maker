import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPointOfInterestEntity } from 'app/shared/model/point-of-interest-entity.model';
import { PointOfInterestEntityService } from './point-of-interest-entity.service';

@Component({
  templateUrl: './point-of-interest-entity-delete-dialog.component.html',
})
export class PointOfInterestEntityDeleteDialogComponent {
  pointOfInterestEntity?: IPointOfInterestEntity;

  constructor(
    protected pointOfInterestEntityService: PointOfInterestEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.pointOfInterestEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('pointOfInterestEntityListModification');
      this.activeModal.close();
    });
  }
}
