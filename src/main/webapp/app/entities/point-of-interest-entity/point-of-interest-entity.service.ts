import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IPointOfInterestEntity } from 'app/shared/model/point-of-interest-entity.model';

type EntityResponseType = HttpResponse<IPointOfInterestEntity>;
type EntityArrayResponseType = HttpResponse<IPointOfInterestEntity[]>;

@Injectable({ providedIn: 'root' })
export class PointOfInterestEntityService {
  public resourceUrl = SERVER_API_URL + 'api/point-of-interest-entities';

  constructor(protected http: HttpClient) {}

  create(pointOfInterestEntity: IPointOfInterestEntity): Observable<EntityResponseType> {
    return this.http.post<IPointOfInterestEntity>(this.resourceUrl, pointOfInterestEntity, { observe: 'response' });
  }

  update(pointOfInterestEntity: IPointOfInterestEntity): Observable<EntityResponseType> {
    return this.http.put<IPointOfInterestEntity>(this.resourceUrl, pointOfInterestEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPointOfInterestEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPointOfInterestEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
