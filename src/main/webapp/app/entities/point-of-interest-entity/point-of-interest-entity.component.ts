import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPointOfInterestEntity } from 'app/shared/model/point-of-interest-entity.model';
import { PointOfInterestEntityService } from './point-of-interest-entity.service';
import { PointOfInterestEntityDeleteDialogComponent } from './point-of-interest-entity-delete-dialog.component';

@Component({
  selector: 'jhi-point-of-interest-entity',
  templateUrl: './point-of-interest-entity.component.html',
})
export class PointOfInterestEntityComponent implements OnInit, OnDestroy {
  pointOfInterestEntities?: IPointOfInterestEntity[];
  eventSubscriber?: Subscription;

  constructor(
    protected pointOfInterestEntityService: PointOfInterestEntityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.pointOfInterestEntityService
      .query()
      .subscribe((res: HttpResponse<IPointOfInterestEntity[]>) => (this.pointOfInterestEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPointOfInterestEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPointOfInterestEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPointOfInterestEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('pointOfInterestEntityListModification', () => this.loadAll());
  }

  delete(pointOfInterestEntity: IPointOfInterestEntity): void {
    const modalRef = this.modalService.open(PointOfInterestEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pointOfInterestEntity = pointOfInterestEntity;
  }
}
