import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { PointOfInterestEntityComponent } from './point-of-interest-entity.component';
import { PointOfInterestEntityDetailComponent } from './point-of-interest-entity-detail.component';
import { PointOfInterestEntityUpdateComponent } from './point-of-interest-entity-update.component';
import { PointOfInterestEntityDeleteDialogComponent } from './point-of-interest-entity-delete-dialog.component';
import { pointOfInterestEntityRoute } from './point-of-interest-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(pointOfInterestEntityRoute)],
  declarations: [
    PointOfInterestEntityComponent,
    PointOfInterestEntityDetailComponent,
    PointOfInterestEntityUpdateComponent,
    PointOfInterestEntityDeleteDialogComponent,
  ],
  entryComponents: [PointOfInterestEntityDeleteDialogComponent],
})
export class WarMakerPointOfInterestEntityModule {}
