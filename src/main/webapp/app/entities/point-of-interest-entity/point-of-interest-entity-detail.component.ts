import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPointOfInterestEntity } from 'app/shared/model/point-of-interest-entity.model';

@Component({
  selector: 'jhi-point-of-interest-entity-detail',
  templateUrl: './point-of-interest-entity-detail.component.html',
})
export class PointOfInterestEntityDetailComponent implements OnInit {
  pointOfInterestEntity: IPointOfInterestEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ pointOfInterestEntity }) => (this.pointOfInterestEntity = pointOfInterestEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
