import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IPointOfInterestEntity, PointOfInterestEntity } from 'app/shared/model/point-of-interest-entity.model';
import { PointOfInterestEntityService } from './point-of-interest-entity.service';
import { PointOfInterestEntityComponent } from './point-of-interest-entity.component';
import { PointOfInterestEntityDetailComponent } from './point-of-interest-entity-detail.component';
import { PointOfInterestEntityUpdateComponent } from './point-of-interest-entity-update.component';

@Injectable({ providedIn: 'root' })
export class PointOfInterestEntityResolve implements Resolve<IPointOfInterestEntity> {
  constructor(private service: PointOfInterestEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPointOfInterestEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((pointOfInterestEntity: HttpResponse<PointOfInterestEntity>) => {
          if (pointOfInterestEntity.body) {
            return of(pointOfInterestEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PointOfInterestEntity());
  }
}

export const pointOfInterestEntityRoute: Routes = [
  {
    path: '',
    component: PointOfInterestEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.pointOfInterestEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PointOfInterestEntityDetailComponent,
    resolve: {
      pointOfInterestEntity: PointOfInterestEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.pointOfInterestEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PointOfInterestEntityUpdateComponent,
    resolve: {
      pointOfInterestEntity: PointOfInterestEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.pointOfInterestEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PointOfInterestEntityUpdateComponent,
    resolve: {
      pointOfInterestEntity: PointOfInterestEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.pointOfInterestEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
