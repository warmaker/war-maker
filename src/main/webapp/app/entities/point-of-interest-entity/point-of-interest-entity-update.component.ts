import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IPointOfInterestEntity, PointOfInterestEntity } from 'app/shared/model/point-of-interest-entity.model';
import { PointOfInterestEntityService } from './point-of-interest-entity.service';
import { IFortressEntity } from 'app/shared/model/fortress-entity.model';
import { FortressEntityService } from 'app/entities/fortress-entity/fortress-entity.service';
import { IFieldEntity } from 'app/shared/model/field-entity.model';
import { FieldEntityService } from 'app/entities/field-entity/field-entity.service';

type SelectableEntity = IFortressEntity | IFieldEntity;

@Component({
  selector: 'jhi-point-of-interest-entity-update',
  templateUrl: './point-of-interest-entity-update.component.html',
})
export class PointOfInterestEntityUpdateComponent implements OnInit {
  isSaving = false;
  fortresses: IFortressEntity[] = [];
  locations: IFieldEntity[] = [];

  editForm = this.fb.group({
    id: [],
    type: [],
    fortress: [],
    location: [],
  });

  constructor(
    protected pointOfInterestEntityService: PointOfInterestEntityService,
    protected fortressEntityService: FortressEntityService,
    protected fieldEntityService: FieldEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ pointOfInterestEntity }) => {
      this.updateForm(pointOfInterestEntity);

      this.fortressEntityService
        .query({ filter: 'pointofinterestentity-is-null' })
        .pipe(
          map((res: HttpResponse<IFortressEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IFortressEntity[]) => {
          if (!pointOfInterestEntity.fortress || !pointOfInterestEntity.fortress.id) {
            this.fortresses = resBody;
          } else {
            this.fortressEntityService
              .find(pointOfInterestEntity.fortress.id)
              .pipe(
                map((subRes: HttpResponse<IFortressEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IFortressEntity[]) => (this.fortresses = concatRes));
          }
        });

      this.fieldEntityService
        .query({ filter: 'pointofinterestentity-is-null' })
        .pipe(
          map((res: HttpResponse<IFieldEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IFieldEntity[]) => {
          if (!pointOfInterestEntity.location || !pointOfInterestEntity.location.id) {
            this.locations = resBody;
          } else {
            this.fieldEntityService
              .find(pointOfInterestEntity.location.id)
              .pipe(
                map((subRes: HttpResponse<IFieldEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IFieldEntity[]) => (this.locations = concatRes));
          }
        });
    });
  }

  updateForm(pointOfInterestEntity: IPointOfInterestEntity): void {
    this.editForm.patchValue({
      id: pointOfInterestEntity.id,
      type: pointOfInterestEntity.type,
      fortress: pointOfInterestEntity.fortress,
      location: pointOfInterestEntity.location,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const pointOfInterestEntity = this.createFromForm();
    if (pointOfInterestEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.pointOfInterestEntityService.update(pointOfInterestEntity));
    } else {
      this.subscribeToSaveResponse(this.pointOfInterestEntityService.create(pointOfInterestEntity));
    }
  }

  private createFromForm(): IPointOfInterestEntity {
    return {
      ...new PointOfInterestEntity(),
      id: this.editForm.get(['id'])!.value,
      type: this.editForm.get(['type'])!.value,
      fortress: this.editForm.get(['fortress'])!.value,
      location: this.editForm.get(['location'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPointOfInterestEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
