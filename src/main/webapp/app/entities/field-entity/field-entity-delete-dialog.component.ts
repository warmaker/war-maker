import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFieldEntity } from 'app/shared/model/field-entity.model';
import { FieldEntityService } from './field-entity.service';

@Component({
  templateUrl: './field-entity-delete-dialog.component.html',
})
export class FieldEntityDeleteDialogComponent {
  fieldEntity?: IFieldEntity;

  constructor(
    protected fieldEntityService: FieldEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fieldEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fieldEntityListModification');
      this.activeModal.close();
    });
  }
}
