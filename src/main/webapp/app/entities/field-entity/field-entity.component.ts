import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFieldEntity } from 'app/shared/model/field-entity.model';
import { FieldEntityService } from './field-entity.service';
import { FieldEntityDeleteDialogComponent } from './field-entity-delete-dialog.component';

@Component({
  selector: 'jhi-field-entity',
  templateUrl: './field-entity.component.html',
})
export class FieldEntityComponent implements OnInit, OnDestroy {
  fieldEntities?: IFieldEntity[];
  eventSubscriber?: Subscription;

  constructor(
    protected fieldEntityService: FieldEntityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.fieldEntityService.query().subscribe((res: HttpResponse<IFieldEntity[]>) => (this.fieldEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFieldEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFieldEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFieldEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('fieldEntityListModification', () => this.loadAll());
  }

  delete(fieldEntity: IFieldEntity): void {
    const modalRef = this.modalService.open(FieldEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fieldEntity = fieldEntity;
  }
}
