import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFieldEntity } from 'app/shared/model/field-entity.model';

type EntityResponseType = HttpResponse<IFieldEntity>;
type EntityArrayResponseType = HttpResponse<IFieldEntity[]>;

@Injectable({ providedIn: 'root' })
export class FieldEntityService {
  public resourceUrl = SERVER_API_URL + 'api/field-entities';

  constructor(protected http: HttpClient) {}

  create(fieldEntity: IFieldEntity): Observable<EntityResponseType> {
    return this.http.post<IFieldEntity>(this.resourceUrl, fieldEntity, { observe: 'response' });
  }

  update(fieldEntity: IFieldEntity): Observable<EntityResponseType> {
    return this.http.put<IFieldEntity>(this.resourceUrl, fieldEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFieldEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFieldEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
