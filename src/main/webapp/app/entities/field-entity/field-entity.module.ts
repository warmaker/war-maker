import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { FieldEntityComponent } from './field-entity.component';
import { FieldEntityDetailComponent } from './field-entity-detail.component';
import { FieldEntityUpdateComponent } from './field-entity-update.component';
import { FieldEntityDeleteDialogComponent } from './field-entity-delete-dialog.component';
import { fieldEntityRoute } from './field-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(fieldEntityRoute)],
  declarations: [FieldEntityComponent, FieldEntityDetailComponent, FieldEntityUpdateComponent, FieldEntityDeleteDialogComponent],
  entryComponents: [FieldEntityDeleteDialogComponent],
})
export class WarMakerFieldEntityModule {}
