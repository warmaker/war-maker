import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFieldEntity, FieldEntity } from 'app/shared/model/field-entity.model';
import { FieldEntityService } from './field-entity.service';
import { FieldEntityComponent } from './field-entity.component';
import { FieldEntityDetailComponent } from './field-entity-detail.component';
import { FieldEntityUpdateComponent } from './field-entity-update.component';

@Injectable({ providedIn: 'root' })
export class FieldEntityResolve implements Resolve<IFieldEntity> {
  constructor(private service: FieldEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFieldEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fieldEntity: HttpResponse<FieldEntity>) => {
          if (fieldEntity.body) {
            return of(fieldEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FieldEntity());
  }
}

export const fieldEntityRoute: Routes = [
  {
    path: '',
    component: FieldEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.fieldEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FieldEntityDetailComponent,
    resolve: {
      fieldEntity: FieldEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.fieldEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FieldEntityUpdateComponent,
    resolve: {
      fieldEntity: FieldEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.fieldEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FieldEntityUpdateComponent,
    resolve: {
      fieldEntity: FieldEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.fieldEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
