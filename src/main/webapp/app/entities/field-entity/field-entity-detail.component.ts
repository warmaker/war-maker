import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFieldEntity } from 'app/shared/model/field-entity.model';

@Component({
  selector: 'jhi-field-entity-detail',
  templateUrl: './field-entity-detail.component.html',
})
export class FieldEntityDetailComponent implements OnInit {
  fieldEntity: IFieldEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fieldEntity }) => (this.fieldEntity = fieldEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
