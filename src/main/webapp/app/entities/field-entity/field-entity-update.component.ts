import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFieldEntity, FieldEntity } from 'app/shared/model/field-entity.model';
import { FieldEntityService } from './field-entity.service';
import { IWorldEntity } from 'app/shared/model/world-entity.model';
import { WorldEntityService } from 'app/entities/world-entity/world-entity.service';

@Component({
  selector: 'jhi-field-entity-update',
  templateUrl: './field-entity-update.component.html',
})
export class FieldEntityUpdateComponent implements OnInit {
  isSaving = false;
  worldentities: IWorldEntity[] = [];

  editForm = this.fb.group({
    id: [],
    xCoordinate: [null, [Validators.required]],
    yCoordinate: [null, [Validators.required]],
    type: [null, [Validators.required]],
    worldEntity: [],
  });

  constructor(
    protected fieldEntityService: FieldEntityService,
    protected worldEntityService: WorldEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fieldEntity }) => {
      this.updateForm(fieldEntity);

      this.worldEntityService.query().subscribe((res: HttpResponse<IWorldEntity[]>) => (this.worldentities = res.body || []));
    });
  }

  updateForm(fieldEntity: IFieldEntity): void {
    this.editForm.patchValue({
      id: fieldEntity.id,
      xCoordinate: fieldEntity.xCoordinate,
      yCoordinate: fieldEntity.yCoordinate,
      type: fieldEntity.type,
      worldEntity: fieldEntity.worldEntity,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fieldEntity = this.createFromForm();
    if (fieldEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.fieldEntityService.update(fieldEntity));
    } else {
      this.subscribeToSaveResponse(this.fieldEntityService.create(fieldEntity));
    }
  }

  private createFromForm(): IFieldEntity {
    return {
      ...new FieldEntity(),
      id: this.editForm.get(['id'])!.value,
      xCoordinate: this.editForm.get(['xCoordinate'])!.value,
      yCoordinate: this.editForm.get(['yCoordinate'])!.value,
      type: this.editForm.get(['type'])!.value,
      worldEntity: this.editForm.get(['worldEntity'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFieldEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IWorldEntity): any {
    return item.id;
  }
}
