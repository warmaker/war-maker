import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ICityEntity, CityEntity } from 'app/shared/model/city-entity.model';
import { CityEntityService } from './city-entity.service';
import { IArmyEntity } from 'app/shared/model/army-entity.model';
import { ArmyEntityService } from 'app/entities/army-entity/army-entity.service';
import { ICityBuildingsEntity } from 'app/shared/model/city-buildings-entity.model';
import { CityBuildingsEntityService } from 'app/entities/city-buildings-entity/city-buildings-entity.service';
import { IResourcesEntity } from 'app/shared/model/resources-entity.model';
import { ResourcesEntityService } from 'app/entities/resources-entity/resources-entity.service';
import { IFieldEntity } from 'app/shared/model/field-entity.model';
import { FieldEntityService } from 'app/entities/field-entity/field-entity.service';
import { IProfileEntity } from 'app/shared/model/profile-entity.model';
import { ProfileEntityService } from 'app/entities/profile-entity/profile-entity.service';

type SelectableEntity = IArmyEntity | ICityBuildingsEntity | IResourcesEntity | IFieldEntity | IProfileEntity;

@Component({
  selector: 'jhi-city-entity-update',
  templateUrl: './city-entity-update.component.html',
})
export class CityEntityUpdateComponent implements OnInit {
  isSaving = false;
  armies: IArmyEntity[] = [];
  buildings: ICityBuildingsEntity[] = [];
  storedresources: IResourcesEntity[] = [];
  locations: IFieldEntity[] = [];
  profileentities: IProfileEntity[] = [];

  editForm = this.fb.group({
    id: [],
    cityName: [],
    cityType: [],
    recruitmentModifier: [],
    buildSpeedModifier: [],
    army: [],
    buildings: [],
    storedResources: [],
    location: [null, Validators.required],
    owner: [],
  });

  constructor(
    protected cityEntityService: CityEntityService,
    protected armyEntityService: ArmyEntityService,
    protected cityBuildingsEntityService: CityBuildingsEntityService,
    protected resourcesEntityService: ResourcesEntityService,
    protected fieldEntityService: FieldEntityService,
    protected profileEntityService: ProfileEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cityEntity }) => {
      this.updateForm(cityEntity);

      this.armyEntityService
        .query({ filter: 'city-is-null' })
        .pipe(
          map((res: HttpResponse<IArmyEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IArmyEntity[]) => {
          if (!cityEntity.army || !cityEntity.army.id) {
            this.armies = resBody;
          } else {
            this.armyEntityService
              .find(cityEntity.army.id)
              .pipe(
                map((subRes: HttpResponse<IArmyEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IArmyEntity[]) => (this.armies = concatRes));
          }
        });

      this.cityBuildingsEntityService
        .query({ filter: 'city-is-null' })
        .pipe(
          map((res: HttpResponse<ICityBuildingsEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: ICityBuildingsEntity[]) => {
          if (!cityEntity.buildings || !cityEntity.buildings.id) {
            this.buildings = resBody;
          } else {
            this.cityBuildingsEntityService
              .find(cityEntity.buildings.id)
              .pipe(
                map((subRes: HttpResponse<ICityBuildingsEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: ICityBuildingsEntity[]) => (this.buildings = concatRes));
          }
        });

      this.resourcesEntityService
        .query({ filter: 'cityentity-is-null' })
        .pipe(
          map((res: HttpResponse<IResourcesEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IResourcesEntity[]) => {
          if (!cityEntity.storedResources || !cityEntity.storedResources.id) {
            this.storedresources = resBody;
          } else {
            this.resourcesEntityService
              .find(cityEntity.storedResources.id)
              .pipe(
                map((subRes: HttpResponse<IResourcesEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IResourcesEntity[]) => (this.storedresources = concatRes));
          }
        });

      this.fieldEntityService
        .query({ filter: 'cityentity-is-null' })
        .pipe(
          map((res: HttpResponse<IFieldEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IFieldEntity[]) => {
          if (!cityEntity.location || !cityEntity.location.id) {
            this.locations = resBody;
          } else {
            this.fieldEntityService
              .find(cityEntity.location.id)
              .pipe(
                map((subRes: HttpResponse<IFieldEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IFieldEntity[]) => (this.locations = concatRes));
          }
        });

      this.profileEntityService.query().subscribe((res: HttpResponse<IProfileEntity[]>) => (this.profileentities = res.body || []));
    });
  }

  updateForm(cityEntity: ICityEntity): void {
    this.editForm.patchValue({
      id: cityEntity.id,
      cityName: cityEntity.cityName,
      cityType: cityEntity.cityType,
      recruitmentModifier: cityEntity.recruitmentModifier,
      buildSpeedModifier: cityEntity.buildSpeedModifier,
      army: cityEntity.army,
      buildings: cityEntity.buildings,
      storedResources: cityEntity.storedResources,
      location: cityEntity.location,
      owner: cityEntity.owner,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cityEntity = this.createFromForm();
    if (cityEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.cityEntityService.update(cityEntity));
    } else {
      this.subscribeToSaveResponse(this.cityEntityService.create(cityEntity));
    }
  }

  private createFromForm(): ICityEntity {
    return {
      ...new CityEntity(),
      id: this.editForm.get(['id'])!.value,
      cityName: this.editForm.get(['cityName'])!.value,
      cityType: this.editForm.get(['cityType'])!.value,
      recruitmentModifier: this.editForm.get(['recruitmentModifier'])!.value,
      buildSpeedModifier: this.editForm.get(['buildSpeedModifier'])!.value,
      army: this.editForm.get(['army'])!.value,
      buildings: this.editForm.get(['buildings'])!.value,
      storedResources: this.editForm.get(['storedResources'])!.value,
      location: this.editForm.get(['location'])!.value,
      owner: this.editForm.get(['owner'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICityEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
