import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICityEntity, CityEntity } from 'app/shared/model/city-entity.model';
import { CityEntityService } from './city-entity.service';
import { CityEntityComponent } from './city-entity.component';
import { CityEntityDetailComponent } from './city-entity-detail.component';
import { CityEntityUpdateComponent } from './city-entity-update.component';

@Injectable({ providedIn: 'root' })
export class CityEntityResolve implements Resolve<ICityEntity> {
  constructor(private service: CityEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICityEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((cityEntity: HttpResponse<CityEntity>) => {
          if (cityEntity.body) {
            return of(cityEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CityEntity());
  }
}

export const cityEntityRoute: Routes = [
  {
    path: '',
    component: CityEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.cityEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CityEntityDetailComponent,
    resolve: {
      cityEntity: CityEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.cityEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CityEntityUpdateComponent,
    resolve: {
      cityEntity: CityEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.cityEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CityEntityUpdateComponent,
    resolve: {
      cityEntity: CityEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.cityEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
