import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { CityEntityComponent } from './city-entity.component';
import { CityEntityDetailComponent } from './city-entity-detail.component';
import { CityEntityUpdateComponent } from './city-entity-update.component';
import { CityEntityDeleteDialogComponent } from './city-entity-delete-dialog.component';
import { cityEntityRoute } from './city-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(cityEntityRoute)],
  declarations: [CityEntityComponent, CityEntityDetailComponent, CityEntityUpdateComponent, CityEntityDeleteDialogComponent],
  entryComponents: [CityEntityDeleteDialogComponent],
})
export class WarMakerCityEntityModule {}
