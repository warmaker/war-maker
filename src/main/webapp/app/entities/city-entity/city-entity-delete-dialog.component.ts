import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICityEntity } from 'app/shared/model/city-entity.model';
import { CityEntityService } from './city-entity.service';

@Component({
  templateUrl: './city-entity-delete-dialog.component.html',
})
export class CityEntityDeleteDialogComponent {
  cityEntity?: ICityEntity;

  constructor(
    protected cityEntityService: CityEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.cityEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('cityEntityListModification');
      this.activeModal.close();
    });
  }
}
