import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICityEntity } from 'app/shared/model/city-entity.model';
import { CityEntityService } from './city-entity.service';
import { CityEntityDeleteDialogComponent } from './city-entity-delete-dialog.component';

@Component({
  selector: 'jhi-city-entity',
  templateUrl: './city-entity.component.html',
})
export class CityEntityComponent implements OnInit, OnDestroy {
  cityEntities?: ICityEntity[];
  eventSubscriber?: Subscription;

  constructor(protected cityEntityService: CityEntityService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.cityEntityService.query().subscribe((res: HttpResponse<ICityEntity[]>) => (this.cityEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCityEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICityEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCityEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('cityEntityListModification', () => this.loadAll());
  }

  delete(cityEntity: ICityEntity): void {
    const modalRef = this.modalService.open(CityEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.cityEntity = cityEntity;
  }
}
