import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICityEntity } from 'app/shared/model/city-entity.model';

@Component({
  selector: 'jhi-city-entity-detail',
  templateUrl: './city-entity-detail.component.html',
})
export class CityEntityDetailComponent implements OnInit {
  cityEntity: ICityEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cityEntity }) => (this.cityEntity = cityEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
