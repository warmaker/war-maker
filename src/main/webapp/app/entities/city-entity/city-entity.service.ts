import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICityEntity } from 'app/shared/model/city-entity.model';

type EntityResponseType = HttpResponse<ICityEntity>;
type EntityArrayResponseType = HttpResponse<ICityEntity[]>;

@Injectable({ providedIn: 'root' })
export class CityEntityService {
  public resourceUrl = SERVER_API_URL + 'api/city-entities';

  constructor(protected http: HttpClient) {}

  create(cityEntity: ICityEntity): Observable<EntityResponseType> {
    return this.http.post<ICityEntity>(this.resourceUrl, cityEntity, { observe: 'response' });
  }

  update(cityEntity: ICityEntity): Observable<EntityResponseType> {
    return this.http.put<ICityEntity>(this.resourceUrl, cityEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICityEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICityEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
