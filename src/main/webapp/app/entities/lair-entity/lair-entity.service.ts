import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ILairEntity } from 'app/shared/model/lair-entity.model';

type EntityResponseType = HttpResponse<ILairEntity>;
type EntityArrayResponseType = HttpResponse<ILairEntity[]>;

@Injectable({ providedIn: 'root' })
export class LairEntityService {
  public resourceUrl = SERVER_API_URL + 'api/lair-entities';

  constructor(protected http: HttpClient) {}

  create(lairEntity: ILairEntity): Observable<EntityResponseType> {
    return this.http.post<ILairEntity>(this.resourceUrl, lairEntity, { observe: 'response' });
  }

  update(lairEntity: ILairEntity): Observable<EntityResponseType> {
    return this.http.put<ILairEntity>(this.resourceUrl, lairEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ILairEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ILairEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
