import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { LairEntityComponent } from './lair-entity.component';
import { LairEntityDetailComponent } from './lair-entity-detail.component';
import { LairEntityUpdateComponent } from './lair-entity-update.component';
import { LairEntityDeleteDialogComponent } from './lair-entity-delete-dialog.component';
import { lairEntityRoute } from './lair-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(lairEntityRoute)],
  declarations: [LairEntityComponent, LairEntityDetailComponent, LairEntityUpdateComponent, LairEntityDeleteDialogComponent],
  entryComponents: [LairEntityDeleteDialogComponent],
})
export class WarMakerLairEntityModule {}
