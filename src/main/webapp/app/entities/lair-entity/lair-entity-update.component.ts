import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ILairEntity, LairEntity } from 'app/shared/model/lair-entity.model';
import { LairEntityService } from './lair-entity.service';
import { IResourcesEntity } from 'app/shared/model/resources-entity.model';
import { ResourcesEntityService } from 'app/entities/resources-entity/resources-entity.service';
import { IWarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { WarPartyEntityService } from 'app/entities/war-party-entity/war-party-entity.service';
import { IFieldEntity } from 'app/shared/model/field-entity.model';
import { FieldEntityService } from 'app/entities/field-entity/field-entity.service';

type SelectableEntity = IResourcesEntity | IWarPartyEntity | IFieldEntity;

@Component({
  selector: 'jhi-lair-entity-update',
  templateUrl: './lair-entity-update.component.html',
})
export class LairEntityUpdateComponent implements OnInit {
  isSaving = false;
  treasures: IResourcesEntity[] = [];
  garrisons: IWarPartyEntity[] = [];
  locations: IFieldEntity[] = [];

  editForm = this.fb.group({
    id: [],
    lairName: [],
    treasure: [],
    garrison: [],
    location: [null, Validators.required],
  });

  constructor(
    protected lairEntityService: LairEntityService,
    protected resourcesEntityService: ResourcesEntityService,
    protected warPartyEntityService: WarPartyEntityService,
    protected fieldEntityService: FieldEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ lairEntity }) => {
      this.updateForm(lairEntity);

      this.resourcesEntityService
        .query({ filter: 'lairentity-is-null' })
        .pipe(
          map((res: HttpResponse<IResourcesEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IResourcesEntity[]) => {
          if (!lairEntity.treasure || !lairEntity.treasure.id) {
            this.treasures = resBody;
          } else {
            this.resourcesEntityService
              .find(lairEntity.treasure.id)
              .pipe(
                map((subRes: HttpResponse<IResourcesEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IResourcesEntity[]) => (this.treasures = concatRes));
          }
        });

      this.warPartyEntityService
        .query({ filter: 'lairentity-is-null' })
        .pipe(
          map((res: HttpResponse<IWarPartyEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IWarPartyEntity[]) => {
          if (!lairEntity.garrison || !lairEntity.garrison.id) {
            this.garrisons = resBody;
          } else {
            this.warPartyEntityService
              .find(lairEntity.garrison.id)
              .pipe(
                map((subRes: HttpResponse<IWarPartyEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IWarPartyEntity[]) => (this.garrisons = concatRes));
          }
        });

      this.fieldEntityService
        .query({ filter: 'lairentity-is-null' })
        .pipe(
          map((res: HttpResponse<IFieldEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IFieldEntity[]) => {
          if (!lairEntity.location || !lairEntity.location.id) {
            this.locations = resBody;
          } else {
            this.fieldEntityService
              .find(lairEntity.location.id)
              .pipe(
                map((subRes: HttpResponse<IFieldEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IFieldEntity[]) => (this.locations = concatRes));
          }
        });
    });
  }

  updateForm(lairEntity: ILairEntity): void {
    this.editForm.patchValue({
      id: lairEntity.id,
      lairName: lairEntity.lairName,
      treasure: lairEntity.treasure,
      garrison: lairEntity.garrison,
      location: lairEntity.location,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const lairEntity = this.createFromForm();
    if (lairEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.lairEntityService.update(lairEntity));
    } else {
      this.subscribeToSaveResponse(this.lairEntityService.create(lairEntity));
    }
  }

  private createFromForm(): ILairEntity {
    return {
      ...new LairEntity(),
      id: this.editForm.get(['id'])!.value,
      lairName: this.editForm.get(['lairName'])!.value,
      treasure: this.editForm.get(['treasure'])!.value,
      garrison: this.editForm.get(['garrison'])!.value,
      location: this.editForm.get(['location'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILairEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
