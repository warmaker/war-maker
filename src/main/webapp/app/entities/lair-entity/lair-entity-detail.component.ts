import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILairEntity } from 'app/shared/model/lair-entity.model';

@Component({
  selector: 'jhi-lair-entity-detail',
  templateUrl: './lair-entity-detail.component.html',
})
export class LairEntityDetailComponent implements OnInit {
  lairEntity: ILairEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ lairEntity }) => (this.lairEntity = lairEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
