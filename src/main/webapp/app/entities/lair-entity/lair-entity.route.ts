import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ILairEntity, LairEntity } from 'app/shared/model/lair-entity.model';
import { LairEntityService } from './lair-entity.service';
import { LairEntityComponent } from './lair-entity.component';
import { LairEntityDetailComponent } from './lair-entity-detail.component';
import { LairEntityUpdateComponent } from './lair-entity-update.component';

@Injectable({ providedIn: 'root' })
export class LairEntityResolve implements Resolve<ILairEntity> {
  constructor(private service: LairEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ILairEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((lairEntity: HttpResponse<LairEntity>) => {
          if (lairEntity.body) {
            return of(lairEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new LairEntity());
  }
}

export const lairEntityRoute: Routes = [
  {
    path: '',
    component: LairEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.lairEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: LairEntityDetailComponent,
    resolve: {
      lairEntity: LairEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.lairEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: LairEntityUpdateComponent,
    resolve: {
      lairEntity: LairEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.lairEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: LairEntityUpdateComponent,
    resolve: {
      lairEntity: LairEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.lairEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
