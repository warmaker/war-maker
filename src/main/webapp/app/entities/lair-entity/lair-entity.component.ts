import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ILairEntity } from 'app/shared/model/lair-entity.model';
import { LairEntityService } from './lair-entity.service';
import { LairEntityDeleteDialogComponent } from './lair-entity-delete-dialog.component';

@Component({
  selector: 'jhi-lair-entity',
  templateUrl: './lair-entity.component.html',
})
export class LairEntityComponent implements OnInit, OnDestroy {
  lairEntities?: ILairEntity[];
  eventSubscriber?: Subscription;

  constructor(protected lairEntityService: LairEntityService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.lairEntityService.query().subscribe((res: HttpResponse<ILairEntity[]>) => (this.lairEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInLairEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ILairEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInLairEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('lairEntityListModification', () => this.loadAll());
  }

  delete(lairEntity: ILairEntity): void {
    const modalRef = this.modalService.open(LairEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.lairEntity = lairEntity;
  }
}
