import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ILairEntity } from 'app/shared/model/lair-entity.model';
import { LairEntityService } from './lair-entity.service';

@Component({
  templateUrl: './lair-entity-delete-dialog.component.html',
})
export class LairEntityDeleteDialogComponent {
  lairEntity?: ILairEntity;

  constructor(
    protected lairEntityService: LairEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.lairEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('lairEntityListModification');
      this.activeModal.close();
    });
  }
}
