import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFortressEntity, FortressEntity } from 'app/shared/model/fortress-entity.model';
import { FortressEntityService } from './fortress-entity.service';

@Component({
  selector: 'jhi-fortress-entity-update',
  templateUrl: './fortress-entity-update.component.html',
})
export class FortressEntityUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    fortressName: [],
    warBarracks: [],
    wall: [],
    moat: [],
  });

  constructor(protected fortressEntityService: FortressEntityService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fortressEntity }) => {
      this.updateForm(fortressEntity);
    });
  }

  updateForm(fortressEntity: IFortressEntity): void {
    this.editForm.patchValue({
      id: fortressEntity.id,
      fortressName: fortressEntity.fortressName,
      warBarracks: fortressEntity.warBarracks,
      wall: fortressEntity.wall,
      moat: fortressEntity.moat,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const fortressEntity = this.createFromForm();
    if (fortressEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.fortressEntityService.update(fortressEntity));
    } else {
      this.subscribeToSaveResponse(this.fortressEntityService.create(fortressEntity));
    }
  }

  private createFromForm(): IFortressEntity {
    return {
      ...new FortressEntity(),
      id: this.editForm.get(['id'])!.value,
      fortressName: this.editForm.get(['fortressName'])!.value,
      warBarracks: this.editForm.get(['warBarracks'])!.value,
      wall: this.editForm.get(['wall'])!.value,
      moat: this.editForm.get(['moat'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFortressEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
