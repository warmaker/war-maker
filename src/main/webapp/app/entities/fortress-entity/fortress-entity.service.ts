import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFortressEntity } from 'app/shared/model/fortress-entity.model';

type EntityResponseType = HttpResponse<IFortressEntity>;
type EntityArrayResponseType = HttpResponse<IFortressEntity[]>;

@Injectable({ providedIn: 'root' })
export class FortressEntityService {
  public resourceUrl = SERVER_API_URL + 'api/fortress-entities';

  constructor(protected http: HttpClient) {}

  create(fortressEntity: IFortressEntity): Observable<EntityResponseType> {
    return this.http.post<IFortressEntity>(this.resourceUrl, fortressEntity, { observe: 'response' });
  }

  update(fortressEntity: IFortressEntity): Observable<EntityResponseType> {
    return this.http.put<IFortressEntity>(this.resourceUrl, fortressEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFortressEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFortressEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
