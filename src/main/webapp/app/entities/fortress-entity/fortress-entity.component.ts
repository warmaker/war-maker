import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFortressEntity } from 'app/shared/model/fortress-entity.model';
import { FortressEntityService } from './fortress-entity.service';
import { FortressEntityDeleteDialogComponent } from './fortress-entity-delete-dialog.component';

@Component({
  selector: 'jhi-fortress-entity',
  templateUrl: './fortress-entity.component.html',
})
export class FortressEntityComponent implements OnInit, OnDestroy {
  fortressEntities?: IFortressEntity[];
  eventSubscriber?: Subscription;

  constructor(
    protected fortressEntityService: FortressEntityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.fortressEntityService.query().subscribe((res: HttpResponse<IFortressEntity[]>) => (this.fortressEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFortressEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFortressEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFortressEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('fortressEntityListModification', () => this.loadAll());
  }

  delete(fortressEntity: IFortressEntity): void {
    const modalRef = this.modalService.open(FortressEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fortressEntity = fortressEntity;
  }
}
