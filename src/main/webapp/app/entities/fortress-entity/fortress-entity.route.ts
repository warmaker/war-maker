import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFortressEntity, FortressEntity } from 'app/shared/model/fortress-entity.model';
import { FortressEntityService } from './fortress-entity.service';
import { FortressEntityComponent } from './fortress-entity.component';
import { FortressEntityDetailComponent } from './fortress-entity-detail.component';
import { FortressEntityUpdateComponent } from './fortress-entity-update.component';

@Injectable({ providedIn: 'root' })
export class FortressEntityResolve implements Resolve<IFortressEntity> {
  constructor(private service: FortressEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFortressEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fortressEntity: HttpResponse<FortressEntity>) => {
          if (fortressEntity.body) {
            return of(fortressEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FortressEntity());
  }
}

export const fortressEntityRoute: Routes = [
  {
    path: '',
    component: FortressEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.fortressEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FortressEntityDetailComponent,
    resolve: {
      fortressEntity: FortressEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.fortressEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FortressEntityUpdateComponent,
    resolve: {
      fortressEntity: FortressEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.fortressEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FortressEntityUpdateComponent,
    resolve: {
      fortressEntity: FortressEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.fortressEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
