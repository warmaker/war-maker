import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFortressEntity } from 'app/shared/model/fortress-entity.model';
import { FortressEntityService } from './fortress-entity.service';

@Component({
  templateUrl: './fortress-entity-delete-dialog.component.html',
})
export class FortressEntityDeleteDialogComponent {
  fortressEntity?: IFortressEntity;

  constructor(
    protected fortressEntityService: FortressEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.fortressEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('fortressEntityListModification');
      this.activeModal.close();
    });
  }
}
