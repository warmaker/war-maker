import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { FortressEntityComponent } from './fortress-entity.component';
import { FortressEntityDetailComponent } from './fortress-entity-detail.component';
import { FortressEntityUpdateComponent } from './fortress-entity-update.component';
import { FortressEntityDeleteDialogComponent } from './fortress-entity-delete-dialog.component';
import { fortressEntityRoute } from './fortress-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(fortressEntityRoute)],
  declarations: [
    FortressEntityComponent,
    FortressEntityDetailComponent,
    FortressEntityUpdateComponent,
    FortressEntityDeleteDialogComponent,
  ],
  entryComponents: [FortressEntityDeleteDialogComponent],
})
export class WarMakerFortressEntityModule {}
