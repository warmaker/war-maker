import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFortressEntity } from 'app/shared/model/fortress-entity.model';

@Component({
  selector: 'jhi-fortress-entity-detail',
  templateUrl: './fortress-entity-detail.component.html',
})
export class FortressEntityDetailComponent implements OnInit {
  fortressEntity: IFortressEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fortressEntity }) => (this.fortressEntity = fortressEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
