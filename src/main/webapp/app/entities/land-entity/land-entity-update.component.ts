import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ILandEntity, LandEntity } from 'app/shared/model/land-entity.model';
import { LandEntityService } from './land-entity.service';
import { IFortressEntity } from 'app/shared/model/fortress-entity.model';
import { FortressEntityService } from 'app/entities/fortress-entity/fortress-entity.service';
import { IArmyEntity } from 'app/shared/model/army-entity.model';
import { ArmyEntityService } from 'app/entities/army-entity/army-entity.service';
import { IFieldEntity } from 'app/shared/model/field-entity.model';
import { FieldEntityService } from 'app/entities/field-entity/field-entity.service';
import { IProfileEntity } from 'app/shared/model/profile-entity.model';
import { ProfileEntityService } from 'app/entities/profile-entity/profile-entity.service';

type SelectableEntity = IFortressEntity | IArmyEntity | IFieldEntity | IProfileEntity;

@Component({
  selector: 'jhi-land-entity-update',
  templateUrl: './land-entity-update.component.html',
})
export class LandEntityUpdateComponent implements OnInit {
  isSaving = false;
  fortresses: IFortressEntity[] = [];
  armies: IArmyEntity[] = [];
  locations: IFieldEntity[] = [];
  profileentities: IProfileEntity[] = [];

  editForm = this.fb.group({
    id: [],
    road: [],
    scoutTower: [],
    fortress: [],
    army: [],
    location: [null, Validators.required],
    owner: [],
  });

  constructor(
    protected landEntityService: LandEntityService,
    protected fortressEntityService: FortressEntityService,
    protected armyEntityService: ArmyEntityService,
    protected fieldEntityService: FieldEntityService,
    protected profileEntityService: ProfileEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ landEntity }) => {
      this.updateForm(landEntity);

      this.fortressEntityService
        .query({ filter: 'landentity-is-null' })
        .pipe(
          map((res: HttpResponse<IFortressEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IFortressEntity[]) => {
          if (!landEntity.fortress || !landEntity.fortress.id) {
            this.fortresses = resBody;
          } else {
            this.fortressEntityService
              .find(landEntity.fortress.id)
              .pipe(
                map((subRes: HttpResponse<IFortressEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IFortressEntity[]) => (this.fortresses = concatRes));
          }
        });

      this.armyEntityService
        .query({ filter: 'landentity-is-null' })
        .pipe(
          map((res: HttpResponse<IArmyEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IArmyEntity[]) => {
          if (!landEntity.army || !landEntity.army.id) {
            this.armies = resBody;
          } else {
            this.armyEntityService
              .find(landEntity.army.id)
              .pipe(
                map((subRes: HttpResponse<IArmyEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IArmyEntity[]) => (this.armies = concatRes));
          }
        });

      this.fieldEntityService
        .query({ filter: 'landentity-is-null' })
        .pipe(
          map((res: HttpResponse<IFieldEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IFieldEntity[]) => {
          if (!landEntity.location || !landEntity.location.id) {
            this.locations = resBody;
          } else {
            this.fieldEntityService
              .find(landEntity.location.id)
              .pipe(
                map((subRes: HttpResponse<IFieldEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IFieldEntity[]) => (this.locations = concatRes));
          }
        });

      this.profileEntityService.query().subscribe((res: HttpResponse<IProfileEntity[]>) => (this.profileentities = res.body || []));
    });
  }

  updateForm(landEntity: ILandEntity): void {
    this.editForm.patchValue({
      id: landEntity.id,
      road: landEntity.road,
      scoutTower: landEntity.scoutTower,
      fortress: landEntity.fortress,
      army: landEntity.army,
      location: landEntity.location,
      owner: landEntity.owner,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const landEntity = this.createFromForm();
    if (landEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.landEntityService.update(landEntity));
    } else {
      this.subscribeToSaveResponse(this.landEntityService.create(landEntity));
    }
  }

  private createFromForm(): ILandEntity {
    return {
      ...new LandEntity(),
      id: this.editForm.get(['id'])!.value,
      road: this.editForm.get(['road'])!.value,
      scoutTower: this.editForm.get(['scoutTower'])!.value,
      fortress: this.editForm.get(['fortress'])!.value,
      army: this.editForm.get(['army'])!.value,
      location: this.editForm.get(['location'])!.value,
      owner: this.editForm.get(['owner'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILandEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
