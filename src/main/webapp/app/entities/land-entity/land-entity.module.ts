import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { LandEntityComponent } from './land-entity.component';
import { LandEntityDetailComponent } from './land-entity-detail.component';
import { LandEntityUpdateComponent } from './land-entity-update.component';
import { LandEntityDeleteDialogComponent } from './land-entity-delete-dialog.component';
import { landEntityRoute } from './land-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(landEntityRoute)],
  declarations: [LandEntityComponent, LandEntityDetailComponent, LandEntityUpdateComponent, LandEntityDeleteDialogComponent],
  entryComponents: [LandEntityDeleteDialogComponent],
})
export class WarMakerLandEntityModule {}
