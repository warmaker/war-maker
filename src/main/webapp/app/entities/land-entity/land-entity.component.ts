import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ILandEntity } from 'app/shared/model/land-entity.model';
import { LandEntityService } from './land-entity.service';
import { LandEntityDeleteDialogComponent } from './land-entity-delete-dialog.component';

@Component({
  selector: 'jhi-land-entity',
  templateUrl: './land-entity.component.html',
})
export class LandEntityComponent implements OnInit, OnDestroy {
  landEntities?: ILandEntity[];
  eventSubscriber?: Subscription;

  constructor(protected landEntityService: LandEntityService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.landEntityService.query().subscribe((res: HttpResponse<ILandEntity[]>) => (this.landEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInLandEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ILandEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInLandEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('landEntityListModification', () => this.loadAll());
  }

  delete(landEntity: ILandEntity): void {
    const modalRef = this.modalService.open(LandEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.landEntity = landEntity;
  }
}
