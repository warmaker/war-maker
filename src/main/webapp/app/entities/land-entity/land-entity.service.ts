import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ILandEntity } from 'app/shared/model/land-entity.model';

type EntityResponseType = HttpResponse<ILandEntity>;
type EntityArrayResponseType = HttpResponse<ILandEntity[]>;

@Injectable({ providedIn: 'root' })
export class LandEntityService {
  public resourceUrl = SERVER_API_URL + 'api/land-entities';

  constructor(protected http: HttpClient) {}

  create(landEntity: ILandEntity): Observable<EntityResponseType> {
    return this.http.post<ILandEntity>(this.resourceUrl, landEntity, { observe: 'response' });
  }

  update(landEntity: ILandEntity): Observable<EntityResponseType> {
    return this.http.put<ILandEntity>(this.resourceUrl, landEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ILandEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ILandEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
