import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ILandEntity } from 'app/shared/model/land-entity.model';
import { LandEntityService } from './land-entity.service';

@Component({
  templateUrl: './land-entity-delete-dialog.component.html',
})
export class LandEntityDeleteDialogComponent {
  landEntity?: ILandEntity;

  constructor(
    protected landEntityService: LandEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.landEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('landEntityListModification');
      this.activeModal.close();
    });
  }
}
