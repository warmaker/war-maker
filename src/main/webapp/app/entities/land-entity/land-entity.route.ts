import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ILandEntity, LandEntity } from 'app/shared/model/land-entity.model';
import { LandEntityService } from './land-entity.service';
import { LandEntityComponent } from './land-entity.component';
import { LandEntityDetailComponent } from './land-entity-detail.component';
import { LandEntityUpdateComponent } from './land-entity-update.component';

@Injectable({ providedIn: 'root' })
export class LandEntityResolve implements Resolve<ILandEntity> {
  constructor(private service: LandEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ILandEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((landEntity: HttpResponse<LandEntity>) => {
          if (landEntity.body) {
            return of(landEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new LandEntity());
  }
}

export const landEntityRoute: Routes = [
  {
    path: '',
    component: LandEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.landEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: LandEntityDetailComponent,
    resolve: {
      landEntity: LandEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.landEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: LandEntityUpdateComponent,
    resolve: {
      landEntity: LandEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.landEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: LandEntityUpdateComponent,
    resolve: {
      landEntity: LandEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.landEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
