import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILandEntity } from 'app/shared/model/land-entity.model';

@Component({
  selector: 'jhi-land-entity-detail',
  templateUrl: './land-entity-detail.component.html',
})
export class LandEntityDetailComponent implements OnInit {
  landEntity: ILandEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ landEntity }) => (this.landEntity = landEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
