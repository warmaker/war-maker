import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IUnitsEntity } from 'app/shared/model/units-entity.model';

type EntityResponseType = HttpResponse<IUnitsEntity>;
type EntityArrayResponseType = HttpResponse<IUnitsEntity[]>;

@Injectable({ providedIn: 'root' })
export class UnitsEntityService {
  public resourceUrl = SERVER_API_URL + 'api/units-entities';

  constructor(protected http: HttpClient) {}

  create(unitsEntity: IUnitsEntity): Observable<EntityResponseType> {
    return this.http.post<IUnitsEntity>(this.resourceUrl, unitsEntity, { observe: 'response' });
  }

  update(unitsEntity: IUnitsEntity): Observable<EntityResponseType> {
    return this.http.put<IUnitsEntity>(this.resourceUrl, unitsEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUnitsEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUnitsEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
