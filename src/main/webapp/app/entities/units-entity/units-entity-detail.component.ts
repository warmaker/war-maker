import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUnitsEntity } from 'app/shared/model/units-entity.model';

@Component({
  selector: 'jhi-units-entity-detail',
  templateUrl: './units-entity-detail.component.html',
})
export class UnitsEntityDetailComponent implements OnInit {
  unitsEntity: IUnitsEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ unitsEntity }) => (this.unitsEntity = unitsEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
