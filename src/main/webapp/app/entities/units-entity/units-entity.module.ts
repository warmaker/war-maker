import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { UnitsEntityComponent } from './units-entity.component';
import { UnitsEntityDetailComponent } from './units-entity-detail.component';
import { UnitsEntityUpdateComponent } from './units-entity-update.component';
import { UnitsEntityDeleteDialogComponent } from './units-entity-delete-dialog.component';
import { unitsEntityRoute } from './units-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(unitsEntityRoute)],
  declarations: [UnitsEntityComponent, UnitsEntityDetailComponent, UnitsEntityUpdateComponent, UnitsEntityDeleteDialogComponent],
  entryComponents: [UnitsEntityDeleteDialogComponent],
})
export class WarMakerUnitsEntityModule {}
