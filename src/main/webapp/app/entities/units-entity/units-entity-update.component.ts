import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IUnitsEntity, UnitsEntity } from 'app/shared/model/units-entity.model';
import { UnitsEntityService } from './units-entity.service';

@Component({
  selector: 'jhi-units-entity-update',
  templateUrl: './units-entity-update.component.html',
})
export class UnitsEntityUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    spearmen: [],
    swordsmen: [],
    archers: [],
    crossbowmen: [],
  });

  constructor(protected unitsEntityService: UnitsEntityService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ unitsEntity }) => {
      this.updateForm(unitsEntity);
    });
  }

  updateForm(unitsEntity: IUnitsEntity): void {
    this.editForm.patchValue({
      id: unitsEntity.id,
      spearmen: unitsEntity.spearmen,
      swordsmen: unitsEntity.swordsmen,
      archers: unitsEntity.archers,
      crossbowmen: unitsEntity.crossbowmen,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const unitsEntity = this.createFromForm();
    if (unitsEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.unitsEntityService.update(unitsEntity));
    } else {
      this.subscribeToSaveResponse(this.unitsEntityService.create(unitsEntity));
    }
  }

  private createFromForm(): IUnitsEntity {
    return {
      ...new UnitsEntity(),
      id: this.editForm.get(['id'])!.value,
      spearmen: this.editForm.get(['spearmen'])!.value,
      swordsmen: this.editForm.get(['swordsmen'])!.value,
      archers: this.editForm.get(['archers'])!.value,
      crossbowmen: this.editForm.get(['crossbowmen'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUnitsEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
