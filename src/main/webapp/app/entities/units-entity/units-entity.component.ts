import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IUnitsEntity } from 'app/shared/model/units-entity.model';
import { UnitsEntityService } from './units-entity.service';
import { UnitsEntityDeleteDialogComponent } from './units-entity-delete-dialog.component';

@Component({
  selector: 'jhi-units-entity',
  templateUrl: './units-entity.component.html',
})
export class UnitsEntityComponent implements OnInit, OnDestroy {
  unitsEntities?: IUnitsEntity[];
  eventSubscriber?: Subscription;

  constructor(
    protected unitsEntityService: UnitsEntityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.unitsEntityService.query().subscribe((res: HttpResponse<IUnitsEntity[]>) => (this.unitsEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInUnitsEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IUnitsEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInUnitsEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('unitsEntityListModification', () => this.loadAll());
  }

  delete(unitsEntity: IUnitsEntity): void {
    const modalRef = this.modalService.open(UnitsEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.unitsEntity = unitsEntity;
  }
}
