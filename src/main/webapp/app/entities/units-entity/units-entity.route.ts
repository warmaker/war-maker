import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IUnitsEntity, UnitsEntity } from 'app/shared/model/units-entity.model';
import { UnitsEntityService } from './units-entity.service';
import { UnitsEntityComponent } from './units-entity.component';
import { UnitsEntityDetailComponent } from './units-entity-detail.component';
import { UnitsEntityUpdateComponent } from './units-entity-update.component';

@Injectable({ providedIn: 'root' })
export class UnitsEntityResolve implements Resolve<IUnitsEntity> {
  constructor(private service: UnitsEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUnitsEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((unitsEntity: HttpResponse<UnitsEntity>) => {
          if (unitsEntity.body) {
            return of(unitsEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new UnitsEntity());
  }
}

export const unitsEntityRoute: Routes = [
  {
    path: '',
    component: UnitsEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.unitsEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: UnitsEntityDetailComponent,
    resolve: {
      unitsEntity: UnitsEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.unitsEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: UnitsEntityUpdateComponent,
    resolve: {
      unitsEntity: UnitsEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.unitsEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: UnitsEntityUpdateComponent,
    resolve: {
      unitsEntity: UnitsEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.unitsEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
