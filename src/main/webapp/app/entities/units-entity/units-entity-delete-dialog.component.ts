import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUnitsEntity } from 'app/shared/model/units-entity.model';
import { UnitsEntityService } from './units-entity.service';

@Component({
  templateUrl: './units-entity-delete-dialog.component.html',
})
export class UnitsEntityDeleteDialogComponent {
  unitsEntity?: IUnitsEntity;

  constructor(
    protected unitsEntityService: UnitsEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.unitsEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('unitsEntityListModification');
      this.activeModal.close();
    });
  }
}
