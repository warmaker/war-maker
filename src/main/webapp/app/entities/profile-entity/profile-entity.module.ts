import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { ProfileEntityComponent } from './profile-entity.component';
import { ProfileEntityDetailComponent } from './profile-entity-detail.component';
import { ProfileEntityUpdateComponent } from './profile-entity-update.component';
import { ProfileEntityDeleteDialogComponent } from './profile-entity-delete-dialog.component';
import { profileEntityRoute } from './profile-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(profileEntityRoute)],
  declarations: [ProfileEntityComponent, ProfileEntityDetailComponent, ProfileEntityUpdateComponent, ProfileEntityDeleteDialogComponent],
  entryComponents: [ProfileEntityDeleteDialogComponent],
})
export class WarMakerProfileEntityModule {}
