import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProfileEntity } from 'app/shared/model/profile-entity.model';
import { ProfileEntityService } from './profile-entity.service';
import { ProfileEntityDeleteDialogComponent } from './profile-entity-delete-dialog.component';

@Component({
  selector: 'jhi-profile-entity',
  templateUrl: './profile-entity.component.html',
})
export class ProfileEntityComponent implements OnInit, OnDestroy {
  profileEntities?: IProfileEntity[];
  eventSubscriber?: Subscription;

  constructor(
    protected profileEntityService: ProfileEntityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.profileEntityService.query().subscribe((res: HttpResponse<IProfileEntity[]>) => (this.profileEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProfileEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProfileEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInProfileEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('profileEntityListModification', () => this.loadAll());
  }

  delete(profileEntity: IProfileEntity): void {
    const modalRef = this.modalService.open(ProfileEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.profileEntity = profileEntity;
  }
}
