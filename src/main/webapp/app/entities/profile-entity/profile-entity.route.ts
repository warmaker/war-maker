import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IProfileEntity, ProfileEntity } from 'app/shared/model/profile-entity.model';
import { ProfileEntityService } from './profile-entity.service';
import { ProfileEntityComponent } from './profile-entity.component';
import { ProfileEntityDetailComponent } from './profile-entity-detail.component';
import { ProfileEntityUpdateComponent } from './profile-entity-update.component';

@Injectable({ providedIn: 'root' })
export class ProfileEntityResolve implements Resolve<IProfileEntity> {
  constructor(private service: ProfileEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProfileEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((profileEntity: HttpResponse<ProfileEntity>) => {
          if (profileEntity.body) {
            return of(profileEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ProfileEntity());
  }
}

export const profileEntityRoute: Routes = [
  {
    path: '',
    component: ProfileEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.profileEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProfileEntityDetailComponent,
    resolve: {
      profileEntity: ProfileEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.profileEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProfileEntityUpdateComponent,
    resolve: {
      profileEntity: ProfileEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.profileEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProfileEntityUpdateComponent,
    resolve: {
      profileEntity: ProfileEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.profileEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
