import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProfileEntity } from 'app/shared/model/profile-entity.model';

type EntityResponseType = HttpResponse<IProfileEntity>;
type EntityArrayResponseType = HttpResponse<IProfileEntity[]>;

@Injectable({ providedIn: 'root' })
export class ProfileEntityService {
  public resourceUrl = SERVER_API_URL + 'api/profile-entities';

  constructor(protected http: HttpClient) {}

  create(profileEntity: IProfileEntity): Observable<EntityResponseType> {
    return this.http.post<IProfileEntity>(this.resourceUrl, profileEntity, { observe: 'response' });
  }

  update(profileEntity: IProfileEntity): Observable<EntityResponseType> {
    return this.http.put<IProfileEntity>(this.resourceUrl, profileEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProfileEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProfileEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
