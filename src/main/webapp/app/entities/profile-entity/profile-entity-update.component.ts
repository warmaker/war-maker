import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IProfileEntity, ProfileEntity } from 'app/shared/model/profile-entity.model';
import { ProfileEntityService } from './profile-entity.service';
import { IResearchesEntity } from 'app/shared/model/researches-entity.model';
import { ResearchesEntityService } from 'app/entities/researches-entity/researches-entity.service';
import { IAllianceEntity } from 'app/shared/model/alliance-entity.model';
import { AllianceEntityService } from 'app/entities/alliance-entity/alliance-entity.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IWorldEntity } from 'app/shared/model/world-entity.model';
import { WorldEntityService } from 'app/entities/world-entity/world-entity.service';

type SelectableEntity = IResearchesEntity | IAllianceEntity | IUser | IWorldEntity;

@Component({
  selector: 'jhi-profile-entity-update',
  templateUrl: './profile-entity-update.component.html',
})
export class ProfileEntityUpdateComponent implements OnInit {
  isSaving = false;
  researches: IResearchesEntity[] = [];
  alignments: IAllianceEntity[] = [];
  users: IUser[] = [];
  worldentities: IWorldEntity[] = [];

  editForm = this.fb.group({
    id: [],
    accountName: [],
    email: [],
    researchTimeModifier: [],
    researches: [],
    alignment: [],
    user: [null, Validators.required],
    worldEntity: [],
  });

  constructor(
    protected profileEntityService: ProfileEntityService,
    protected researchesEntityService: ResearchesEntityService,
    protected allianceEntityService: AllianceEntityService,
    protected userService: UserService,
    protected worldEntityService: WorldEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ profileEntity }) => {
      this.updateForm(profileEntity);

      this.researchesEntityService
        .query({ filter: 'profileentity-is-null' })
        .pipe(
          map((res: HttpResponse<IResearchesEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IResearchesEntity[]) => {
          if (!profileEntity.researches || !profileEntity.researches.id) {
            this.researches = resBody;
          } else {
            this.researchesEntityService
              .find(profileEntity.researches.id)
              .pipe(
                map((subRes: HttpResponse<IResearchesEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IResearchesEntity[]) => (this.researches = concatRes));
          }
        });

      this.allianceEntityService
        .query({ filter: 'profileentity-is-null' })
        .pipe(
          map((res: HttpResponse<IAllianceEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IAllianceEntity[]) => {
          if (!profileEntity.alignment || !profileEntity.alignment.id) {
            this.alignments = resBody;
          } else {
            this.allianceEntityService
              .find(profileEntity.alignment.id)
              .pipe(
                map((subRes: HttpResponse<IAllianceEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IAllianceEntity[]) => (this.alignments = concatRes));
          }
        });

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));

      this.worldEntityService.query().subscribe((res: HttpResponse<IWorldEntity[]>) => (this.worldentities = res.body || []));
    });
  }

  updateForm(profileEntity: IProfileEntity): void {
    this.editForm.patchValue({
      id: profileEntity.id,
      accountName: profileEntity.accountName,
      email: profileEntity.email,
      researchTimeModifier: profileEntity.researchTimeModifier,
      researches: profileEntity.researches,
      alignment: profileEntity.alignment,
      user: profileEntity.user,
      worldEntity: profileEntity.worldEntity,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const profileEntity = this.createFromForm();
    if (profileEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.profileEntityService.update(profileEntity));
    } else {
      this.subscribeToSaveResponse(this.profileEntityService.create(profileEntity));
    }
  }

  private createFromForm(): IProfileEntity {
    return {
      ...new ProfileEntity(),
      id: this.editForm.get(['id'])!.value,
      accountName: this.editForm.get(['accountName'])!.value,
      email: this.editForm.get(['email'])!.value,
      researchTimeModifier: this.editForm.get(['researchTimeModifier'])!.value,
      researches: this.editForm.get(['researches'])!.value,
      alignment: this.editForm.get(['alignment'])!.value,
      user: this.editForm.get(['user'])!.value,
      worldEntity: this.editForm.get(['worldEntity'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProfileEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
