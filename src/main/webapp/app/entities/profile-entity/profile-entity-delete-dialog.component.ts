import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProfileEntity } from 'app/shared/model/profile-entity.model';
import { ProfileEntityService } from './profile-entity.service';

@Component({
  templateUrl: './profile-entity-delete-dialog.component.html',
})
export class ProfileEntityDeleteDialogComponent {
  profileEntity?: IProfileEntity;

  constructor(
    protected profileEntityService: ProfileEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.profileEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('profileEntityListModification');
      this.activeModal.close();
    });
  }
}
