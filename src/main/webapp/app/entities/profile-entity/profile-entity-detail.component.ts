import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProfileEntity } from 'app/shared/model/profile-entity.model';

@Component({
  selector: 'jhi-profile-entity-detail',
  templateUrl: './profile-entity-detail.component.html',
})
export class ProfileEntityDetailComponent implements OnInit {
  profileEntity: IProfileEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ profileEntity }) => (this.profileEntity = profileEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
