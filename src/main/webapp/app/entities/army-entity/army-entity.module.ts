import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { ArmyEntityComponent } from './army-entity.component';
import { ArmyEntityDetailComponent } from './army-entity-detail.component';
import { ArmyEntityUpdateComponent } from './army-entity-update.component';
import { ArmyEntityDeleteDialogComponent } from './army-entity-delete-dialog.component';
import { armyEntityRoute } from './army-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(armyEntityRoute)],
  declarations: [ArmyEntityComponent, ArmyEntityDetailComponent, ArmyEntityUpdateComponent, ArmyEntityDeleteDialogComponent],
  entryComponents: [ArmyEntityDeleteDialogComponent],
})
export class WarMakerArmyEntityModule {}
