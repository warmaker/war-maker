import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IArmyEntity } from 'app/shared/model/army-entity.model';
import { ArmyEntityService } from './army-entity.service';
import { ArmyEntityDeleteDialogComponent } from './army-entity-delete-dialog.component';

@Component({
  selector: 'jhi-army-entity',
  templateUrl: './army-entity.component.html',
})
export class ArmyEntityComponent implements OnInit, OnDestroy {
  armyEntities?: IArmyEntity[];
  eventSubscriber?: Subscription;

  constructor(protected armyEntityService: ArmyEntityService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.armyEntityService.query().subscribe((res: HttpResponse<IArmyEntity[]>) => (this.armyEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInArmyEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IArmyEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInArmyEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('armyEntityListModification', () => this.loadAll());
  }

  delete(armyEntity: IArmyEntity): void {
    const modalRef = this.modalService.open(ArmyEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.armyEntity = armyEntity;
  }
}
