import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IArmyEntity } from 'app/shared/model/army-entity.model';
import { ArmyEntityService } from './army-entity.service';

@Component({
  templateUrl: './army-entity-delete-dialog.component.html',
})
export class ArmyEntityDeleteDialogComponent {
  armyEntity?: IArmyEntity;

  constructor(
    protected armyEntityService: ArmyEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.armyEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('armyEntityListModification');
      this.activeModal.close();
    });
  }
}
