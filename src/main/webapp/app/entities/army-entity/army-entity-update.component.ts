import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IArmyEntity, ArmyEntity } from 'app/shared/model/army-entity.model';
import { ArmyEntityService } from './army-entity.service';
import { IUnitsEntity } from 'app/shared/model/units-entity.model';
import { UnitsEntityService } from 'app/entities/units-entity/units-entity.service';

@Component({
  selector: 'jhi-army-entity-update',
  templateUrl: './army-entity-update.component.html',
})
export class ArmyEntityUpdateComponent implements OnInit {
  isSaving = false;
  units: IUnitsEntity[] = [];

  editForm = this.fb.group({
    id: [],
    foodReq: [],
    units: [],
  });

  constructor(
    protected armyEntityService: ArmyEntityService,
    protected unitsEntityService: UnitsEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ armyEntity }) => {
      this.updateForm(armyEntity);

      this.unitsEntityService
        .query({ filter: 'armyentity-is-null' })
        .pipe(
          map((res: HttpResponse<IUnitsEntity[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IUnitsEntity[]) => {
          if (!armyEntity.units || !armyEntity.units.id) {
            this.units = resBody;
          } else {
            this.unitsEntityService
              .find(armyEntity.units.id)
              .pipe(
                map((subRes: HttpResponse<IUnitsEntity>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IUnitsEntity[]) => (this.units = concatRes));
          }
        });
    });
  }

  updateForm(armyEntity: IArmyEntity): void {
    this.editForm.patchValue({
      id: armyEntity.id,
      foodReq: armyEntity.foodReq,
      units: armyEntity.units,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const armyEntity = this.createFromForm();
    if (armyEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.armyEntityService.update(armyEntity));
    } else {
      this.subscribeToSaveResponse(this.armyEntityService.create(armyEntity));
    }
  }

  private createFromForm(): IArmyEntity {
    return {
      ...new ArmyEntity(),
      id: this.editForm.get(['id'])!.value,
      foodReq: this.editForm.get(['foodReq'])!.value,
      units: this.editForm.get(['units'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IArmyEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUnitsEntity): any {
    return item.id;
  }
}
