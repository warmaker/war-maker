import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IArmyEntity } from 'app/shared/model/army-entity.model';

type EntityResponseType = HttpResponse<IArmyEntity>;
type EntityArrayResponseType = HttpResponse<IArmyEntity[]>;

@Injectable({ providedIn: 'root' })
export class ArmyEntityService {
  public resourceUrl = SERVER_API_URL + 'api/army-entities';

  constructor(protected http: HttpClient) {}

  create(armyEntity: IArmyEntity): Observable<EntityResponseType> {
    return this.http.post<IArmyEntity>(this.resourceUrl, armyEntity, { observe: 'response' });
  }

  update(armyEntity: IArmyEntity): Observable<EntityResponseType> {
    return this.http.put<IArmyEntity>(this.resourceUrl, armyEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IArmyEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IArmyEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
