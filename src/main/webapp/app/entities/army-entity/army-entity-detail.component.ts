import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IArmyEntity } from 'app/shared/model/army-entity.model';

@Component({
  selector: 'jhi-army-entity-detail',
  templateUrl: './army-entity-detail.component.html',
})
export class ArmyEntityDetailComponent implements OnInit {
  armyEntity: IArmyEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ armyEntity }) => (this.armyEntity = armyEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
