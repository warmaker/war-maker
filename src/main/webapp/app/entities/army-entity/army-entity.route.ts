import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IArmyEntity, ArmyEntity } from 'app/shared/model/army-entity.model';
import { ArmyEntityService } from './army-entity.service';
import { ArmyEntityComponent } from './army-entity.component';
import { ArmyEntityDetailComponent } from './army-entity-detail.component';
import { ArmyEntityUpdateComponent } from './army-entity-update.component';

@Injectable({ providedIn: 'root' })
export class ArmyEntityResolve implements Resolve<IArmyEntity> {
  constructor(private service: ArmyEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IArmyEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((armyEntity: HttpResponse<ArmyEntity>) => {
          if (armyEntity.body) {
            return of(armyEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ArmyEntity());
  }
}

export const armyEntityRoute: Routes = [
  {
    path: '',
    component: ArmyEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.armyEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ArmyEntityDetailComponent,
    resolve: {
      armyEntity: ArmyEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.armyEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ArmyEntityUpdateComponent,
    resolve: {
      armyEntity: ArmyEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.armyEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ArmyEntityUpdateComponent,
    resolve: {
      armyEntity: ArmyEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.armyEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
