import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'world-entity',
        loadChildren: () => import('./world-entity/world-entity.module').then(m => m.WarMakerWorldEntityModule),
      },
      {
        path: 'field-entity',
        loadChildren: () => import('./field-entity/field-entity.module').then(m => m.WarMakerFieldEntityModule),
      },
      {
        path: 'land-entity',
        loadChildren: () => import('./land-entity/land-entity.module').then(m => m.WarMakerLandEntityModule),
      },
      {
        path: 'point-of-interest-entity',
        loadChildren: () =>
          import('./point-of-interest-entity/point-of-interest-entity.module').then(m => m.WarMakerPointOfInterestEntityModule),
      },
      {
        path: 'lair-entity',
        loadChildren: () => import('./lair-entity/lair-entity.module').then(m => m.WarMakerLairEntityModule),
      },
      {
        path: 'city-entity',
        loadChildren: () => import('./city-entity/city-entity.module').then(m => m.WarMakerCityEntityModule),
      },
      {
        path: 'fortress-entity',
        loadChildren: () => import('./fortress-entity/fortress-entity.module').then(m => m.WarMakerFortressEntityModule),
      },
      {
        path: 'resources-entity',
        loadChildren: () => import('./resources-entity/resources-entity.module').then(m => m.WarMakerResourcesEntityModule),
      },
      {
        path: 'city-buildings-entity',
        loadChildren: () => import('./city-buildings-entity/city-buildings-entity.module').then(m => m.WarMakerCityBuildingsEntityModule),
      },
      {
        path: 'alliance-entity',
        loadChildren: () => import('./alliance-entity/alliance-entity.module').then(m => m.WarMakerAllianceEntityModule),
      },
      {
        path: 'clan-entity',
        loadChildren: () => import('./clan-entity/clan-entity.module').then(m => m.WarMakerClanEntityModule),
      },
      {
        path: 'profile-entity',
        loadChildren: () => import('./profile-entity/profile-entity.module').then(m => m.WarMakerProfileEntityModule),
      },
      {
        path: 'hero-entity',
        loadChildren: () => import('./hero-entity/hero-entity.module').then(m => m.WarMakerHeroEntityModule),
      },
      {
        path: 'army-entity',
        loadChildren: () => import('./army-entity/army-entity.module').then(m => m.WarMakerArmyEntityModule),
      },
      {
        path: 'war-party-entity',
        loadChildren: () => import('./war-party-entity/war-party-entity.module').then(m => m.WarMakerWarPartyEntityModule),
      },
      {
        path: 'researches-entity',
        loadChildren: () => import('./researches-entity/researches-entity.module').then(m => m.WarMakerResearchesEntityModule),
      },
      {
        path: 'units-entity',
        loadChildren: () => import('./units-entity/units-entity.module').then(m => m.WarMakerUnitsEntityModule),
      },
      {
        path: 'player-relations',
        loadChildren: () => import('./player-relations/player-relations.module').then(m => m.WarMakerPlayerRelationsModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class WarMakerEntityModule {}
