import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICityBuildingsEntity } from 'app/shared/model/city-buildings-entity.model';

@Component({
  selector: 'jhi-city-buildings-entity-detail',
  templateUrl: './city-buildings-entity-detail.component.html',
})
export class CityBuildingsEntityDetailComponent implements OnInit {
  cityBuildingsEntity: ICityBuildingsEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cityBuildingsEntity }) => (this.cityBuildingsEntity = cityBuildingsEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
