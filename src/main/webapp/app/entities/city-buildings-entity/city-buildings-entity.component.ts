import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICityBuildingsEntity } from 'app/shared/model/city-buildings-entity.model';
import { CityBuildingsEntityService } from './city-buildings-entity.service';
import { CityBuildingsEntityDeleteDialogComponent } from './city-buildings-entity-delete-dialog.component';

@Component({
  selector: 'jhi-city-buildings-entity',
  templateUrl: './city-buildings-entity.component.html',
})
export class CityBuildingsEntityComponent implements OnInit, OnDestroy {
  cityBuildingsEntities?: ICityBuildingsEntity[];
  eventSubscriber?: Subscription;

  constructor(
    protected cityBuildingsEntityService: CityBuildingsEntityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.cityBuildingsEntityService
      .query()
      .subscribe((res: HttpResponse<ICityBuildingsEntity[]>) => (this.cityBuildingsEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCityBuildingsEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICityBuildingsEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCityBuildingsEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('cityBuildingsEntityListModification', () => this.loadAll());
  }

  delete(cityBuildingsEntity: ICityBuildingsEntity): void {
    const modalRef = this.modalService.open(CityBuildingsEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.cityBuildingsEntity = cityBuildingsEntity;
  }
}
