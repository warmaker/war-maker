import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { CityBuildingsEntityComponent } from './city-buildings-entity.component';
import { CityBuildingsEntityDetailComponent } from './city-buildings-entity-detail.component';
import { CityBuildingsEntityUpdateComponent } from './city-buildings-entity-update.component';
import { CityBuildingsEntityDeleteDialogComponent } from './city-buildings-entity-delete-dialog.component';
import { cityBuildingsEntityRoute } from './city-buildings-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(cityBuildingsEntityRoute)],
  declarations: [
    CityBuildingsEntityComponent,
    CityBuildingsEntityDetailComponent,
    CityBuildingsEntityUpdateComponent,
    CityBuildingsEntityDeleteDialogComponent,
  ],
  entryComponents: [CityBuildingsEntityDeleteDialogComponent],
})
export class WarMakerCityBuildingsEntityModule {}
