import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICityBuildingsEntity, CityBuildingsEntity } from 'app/shared/model/city-buildings-entity.model';
import { CityBuildingsEntityService } from './city-buildings-entity.service';
import { CityBuildingsEntityComponent } from './city-buildings-entity.component';
import { CityBuildingsEntityDetailComponent } from './city-buildings-entity-detail.component';
import { CityBuildingsEntityUpdateComponent } from './city-buildings-entity-update.component';

@Injectable({ providedIn: 'root' })
export class CityBuildingsEntityResolve implements Resolve<ICityBuildingsEntity> {
  constructor(private service: CityBuildingsEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICityBuildingsEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((cityBuildingsEntity: HttpResponse<CityBuildingsEntity>) => {
          if (cityBuildingsEntity.body) {
            return of(cityBuildingsEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CityBuildingsEntity());
  }
}

export const cityBuildingsEntityRoute: Routes = [
  {
    path: '',
    component: CityBuildingsEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.cityBuildingsEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CityBuildingsEntityDetailComponent,
    resolve: {
      cityBuildingsEntity: CityBuildingsEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.cityBuildingsEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CityBuildingsEntityUpdateComponent,
    resolve: {
      cityBuildingsEntity: CityBuildingsEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.cityBuildingsEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CityBuildingsEntityUpdateComponent,
    resolve: {
      cityBuildingsEntity: CityBuildingsEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.cityBuildingsEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
