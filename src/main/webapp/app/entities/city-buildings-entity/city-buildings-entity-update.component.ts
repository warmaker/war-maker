import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICityBuildingsEntity, CityBuildingsEntity } from 'app/shared/model/city-buildings-entity.model';
import { CityBuildingsEntityService } from './city-buildings-entity.service';

@Component({
  selector: 'jhi-city-buildings-entity-update',
  templateUrl: './city-buildings-entity-update.component.html',
})
export class CityBuildingsEntityUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    cityHall: [],
    barracks: [],
    wall: [],
    goldMine: [],
    sawMill: [],
    oreMine: [],
    ironworks: [],
    tradingPost: [],
    chapel: [],
    university: [],
    forge: [],
  });

  constructor(
    protected cityBuildingsEntityService: CityBuildingsEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cityBuildingsEntity }) => {
      this.updateForm(cityBuildingsEntity);
    });
  }

  updateForm(cityBuildingsEntity: ICityBuildingsEntity): void {
    this.editForm.patchValue({
      id: cityBuildingsEntity.id,
      cityHall: cityBuildingsEntity.cityHall,
      barracks: cityBuildingsEntity.barracks,
      wall: cityBuildingsEntity.wall,
      goldMine: cityBuildingsEntity.goldMine,
      sawMill: cityBuildingsEntity.sawMill,
      oreMine: cityBuildingsEntity.oreMine,
      ironworks: cityBuildingsEntity.ironworks,
      tradingPost: cityBuildingsEntity.tradingPost,
      chapel: cityBuildingsEntity.chapel,
      university: cityBuildingsEntity.university,
      forge: cityBuildingsEntity.forge,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cityBuildingsEntity = this.createFromForm();
    if (cityBuildingsEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.cityBuildingsEntityService.update(cityBuildingsEntity));
    } else {
      this.subscribeToSaveResponse(this.cityBuildingsEntityService.create(cityBuildingsEntity));
    }
  }

  private createFromForm(): ICityBuildingsEntity {
    return {
      ...new CityBuildingsEntity(),
      id: this.editForm.get(['id'])!.value,
      cityHall: this.editForm.get(['cityHall'])!.value,
      barracks: this.editForm.get(['barracks'])!.value,
      wall: this.editForm.get(['wall'])!.value,
      goldMine: this.editForm.get(['goldMine'])!.value,
      sawMill: this.editForm.get(['sawMill'])!.value,
      oreMine: this.editForm.get(['oreMine'])!.value,
      ironworks: this.editForm.get(['ironworks'])!.value,
      tradingPost: this.editForm.get(['tradingPost'])!.value,
      chapel: this.editForm.get(['chapel'])!.value,
      university: this.editForm.get(['university'])!.value,
      forge: this.editForm.get(['forge'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICityBuildingsEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
