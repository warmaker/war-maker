import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICityBuildingsEntity } from 'app/shared/model/city-buildings-entity.model';
import { CityBuildingsEntityService } from './city-buildings-entity.service';

@Component({
  templateUrl: './city-buildings-entity-delete-dialog.component.html',
})
export class CityBuildingsEntityDeleteDialogComponent {
  cityBuildingsEntity?: ICityBuildingsEntity;

  constructor(
    protected cityBuildingsEntityService: CityBuildingsEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.cityBuildingsEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('cityBuildingsEntityListModification');
      this.activeModal.close();
    });
  }
}
