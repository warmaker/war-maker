import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IResourcesEntity } from 'app/shared/model/resources-entity.model';
import { ResourcesEntityService } from './resources-entity.service';
import { ResourcesEntityDeleteDialogComponent } from './resources-entity-delete-dialog.component';

@Component({
  selector: 'jhi-resources-entity',
  templateUrl: './resources-entity.component.html',
})
export class ResourcesEntityComponent implements OnInit, OnDestroy {
  resourcesEntities?: IResourcesEntity[];
  eventSubscriber?: Subscription;

  constructor(
    protected resourcesEntityService: ResourcesEntityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.resourcesEntityService.query().subscribe((res: HttpResponse<IResourcesEntity[]>) => (this.resourcesEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInResourcesEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IResourcesEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInResourcesEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('resourcesEntityListModification', () => this.loadAll());
  }

  delete(resourcesEntity: IResourcesEntity): void {
    const modalRef = this.modalService.open(ResourcesEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.resourcesEntity = resourcesEntity;
  }
}
