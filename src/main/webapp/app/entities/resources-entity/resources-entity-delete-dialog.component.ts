import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IResourcesEntity } from 'app/shared/model/resources-entity.model';
import { ResourcesEntityService } from './resources-entity.service';

@Component({
  templateUrl: './resources-entity-delete-dialog.component.html',
})
export class ResourcesEntityDeleteDialogComponent {
  resourcesEntity?: IResourcesEntity;

  constructor(
    protected resourcesEntityService: ResourcesEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.resourcesEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('resourcesEntityListModification');
      this.activeModal.close();
    });
  }
}
