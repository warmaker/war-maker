import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IResourcesEntity } from 'app/shared/model/resources-entity.model';

@Component({
  selector: 'jhi-resources-entity-detail',
  templateUrl: './resources-entity-detail.component.html',
})
export class ResourcesEntityDetailComponent implements OnInit {
  resourcesEntity: IResourcesEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ resourcesEntity }) => (this.resourcesEntity = resourcesEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
