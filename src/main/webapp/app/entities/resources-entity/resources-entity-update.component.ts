import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IResourcesEntity, ResourcesEntity } from 'app/shared/model/resources-entity.model';
import { ResourcesEntityService } from './resources-entity.service';

@Component({
  selector: 'jhi-resources-entity-update',
  templateUrl: './resources-entity-update.component.html',
})
export class ResourcesEntityUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    denars: [],
    wood: [],
    stone: [],
    steel: [],
    food: [],
    faith: [],
    changeDate: [],
  });

  constructor(
    protected resourcesEntityService: ResourcesEntityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ resourcesEntity }) => {
      if (!resourcesEntity.id) {
        const today = moment().startOf('day');
        resourcesEntity.changeDate = today;
      }

      this.updateForm(resourcesEntity);
    });
  }

  updateForm(resourcesEntity: IResourcesEntity): void {
    this.editForm.patchValue({
      id: resourcesEntity.id,
      denars: resourcesEntity.denars,
      wood: resourcesEntity.wood,
      stone: resourcesEntity.stone,
      steel: resourcesEntity.steel,
      food: resourcesEntity.food,
      faith: resourcesEntity.faith,
      changeDate: resourcesEntity.changeDate ? resourcesEntity.changeDate.format(DATE_TIME_FORMAT) : null,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const resourcesEntity = this.createFromForm();
    if (resourcesEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.resourcesEntityService.update(resourcesEntity));
    } else {
      this.subscribeToSaveResponse(this.resourcesEntityService.create(resourcesEntity));
    }
  }

  private createFromForm(): IResourcesEntity {
    return {
      ...new ResourcesEntity(),
      id: this.editForm.get(['id'])!.value,
      denars: this.editForm.get(['denars'])!.value,
      wood: this.editForm.get(['wood'])!.value,
      stone: this.editForm.get(['stone'])!.value,
      steel: this.editForm.get(['steel'])!.value,
      food: this.editForm.get(['food'])!.value,
      faith: this.editForm.get(['faith'])!.value,
      changeDate: this.editForm.get(['changeDate'])!.value ? moment(this.editForm.get(['changeDate'])!.value, DATE_TIME_FORMAT) : undefined,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IResourcesEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
