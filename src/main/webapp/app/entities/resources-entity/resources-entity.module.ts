import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { ResourcesEntityComponent } from './resources-entity.component';
import { ResourcesEntityDetailComponent } from './resources-entity-detail.component';
import { ResourcesEntityUpdateComponent } from './resources-entity-update.component';
import { ResourcesEntityDeleteDialogComponent } from './resources-entity-delete-dialog.component';
import { resourcesEntityRoute } from './resources-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(resourcesEntityRoute)],
  declarations: [
    ResourcesEntityComponent,
    ResourcesEntityDetailComponent,
    ResourcesEntityUpdateComponent,
    ResourcesEntityDeleteDialogComponent,
  ],
  entryComponents: [ResourcesEntityDeleteDialogComponent],
})
export class WarMakerResourcesEntityModule {}
