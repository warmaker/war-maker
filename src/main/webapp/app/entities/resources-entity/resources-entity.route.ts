import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IResourcesEntity, ResourcesEntity } from 'app/shared/model/resources-entity.model';
import { ResourcesEntityService } from './resources-entity.service';
import { ResourcesEntityComponent } from './resources-entity.component';
import { ResourcesEntityDetailComponent } from './resources-entity-detail.component';
import { ResourcesEntityUpdateComponent } from './resources-entity-update.component';

@Injectable({ providedIn: 'root' })
export class ResourcesEntityResolve implements Resolve<IResourcesEntity> {
  constructor(private service: ResourcesEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IResourcesEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((resourcesEntity: HttpResponse<ResourcesEntity>) => {
          if (resourcesEntity.body) {
            return of(resourcesEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ResourcesEntity());
  }
}

export const resourcesEntityRoute: Routes = [
  {
    path: '',
    component: ResourcesEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.resourcesEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ResourcesEntityDetailComponent,
    resolve: {
      resourcesEntity: ResourcesEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.resourcesEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ResourcesEntityUpdateComponent,
    resolve: {
      resourcesEntity: ResourcesEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.resourcesEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ResourcesEntityUpdateComponent,
    resolve: {
      resourcesEntity: ResourcesEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.resourcesEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
