import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IWorldEntity, WorldEntity } from 'app/shared/model/world-entity.model';
import { WorldEntityService } from './world-entity.service';

@Component({
  selector: 'jhi-world-entity-update',
  templateUrl: './world-entity-update.component.html',
})
export class WorldEntityUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    worldName: [],
    worldNumber: [],
    height: [],
    width: [],
  });

  constructor(protected worldEntityService: WorldEntityService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ worldEntity }) => {
      this.updateForm(worldEntity);
    });
  }

  updateForm(worldEntity: IWorldEntity): void {
    this.editForm.patchValue({
      id: worldEntity.id,
      worldName: worldEntity.worldName,
      worldNumber: worldEntity.worldNumber,
      height: worldEntity.height,
      width: worldEntity.width,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const worldEntity = this.createFromForm();
    if (worldEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.worldEntityService.update(worldEntity));
    } else {
      this.subscribeToSaveResponse(this.worldEntityService.create(worldEntity));
    }
  }

  private createFromForm(): IWorldEntity {
    return {
      ...new WorldEntity(),
      id: this.editForm.get(['id'])!.value,
      worldName: this.editForm.get(['worldName'])!.value,
      worldNumber: this.editForm.get(['worldNumber'])!.value,
      height: this.editForm.get(['height'])!.value,
      width: this.editForm.get(['width'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWorldEntity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
