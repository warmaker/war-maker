import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from 'app/shared/shared.module';
import { WorldEntityComponent } from './world-entity.component';
import { WorldEntityDetailComponent } from './world-entity-detail.component';
import { WorldEntityUpdateComponent } from './world-entity-update.component';
import { WorldEntityDeleteDialogComponent } from './world-entity-delete-dialog.component';
import { worldEntityRoute } from './world-entity.route';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild(worldEntityRoute)],
  declarations: [WorldEntityComponent, WorldEntityDetailComponent, WorldEntityUpdateComponent, WorldEntityDeleteDialogComponent],
  entryComponents: [WorldEntityDeleteDialogComponent],
})
export class WarMakerWorldEntityModule {}
