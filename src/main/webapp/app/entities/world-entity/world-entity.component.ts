import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWorldEntity } from 'app/shared/model/world-entity.model';
import { WorldEntityService } from './world-entity.service';
import { WorldEntityDeleteDialogComponent } from './world-entity-delete-dialog.component';

@Component({
  selector: 'jhi-world-entity',
  templateUrl: './world-entity.component.html',
})
export class WorldEntityComponent implements OnInit, OnDestroy {
  worldEntities?: IWorldEntity[];
  eventSubscriber?: Subscription;

  constructor(
    protected worldEntityService: WorldEntityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.worldEntityService.query().subscribe((res: HttpResponse<IWorldEntity[]>) => (this.worldEntities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInWorldEntities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IWorldEntity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInWorldEntities(): void {
    this.eventSubscriber = this.eventManager.subscribe('worldEntityListModification', () => this.loadAll());
  }

  delete(worldEntity: IWorldEntity): void {
    const modalRef = this.modalService.open(WorldEntityDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.worldEntity = worldEntity;
  }
}
