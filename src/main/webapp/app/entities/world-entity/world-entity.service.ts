import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IWorldEntity } from 'app/shared/model/world-entity.model';

type EntityResponseType = HttpResponse<IWorldEntity>;
type EntityArrayResponseType = HttpResponse<IWorldEntity[]>;

@Injectable({ providedIn: 'root' })
export class WorldEntityService {
  public resourceUrl = SERVER_API_URL + 'api/world-entities';

  constructor(protected http: HttpClient) {}

  create(worldEntity: IWorldEntity): Observable<EntityResponseType> {
    return this.http.post<IWorldEntity>(this.resourceUrl, worldEntity, { observe: 'response' });
  }

  update(worldEntity: IWorldEntity): Observable<EntityResponseType> {
    return this.http.put<IWorldEntity>(this.resourceUrl, worldEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IWorldEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IWorldEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
