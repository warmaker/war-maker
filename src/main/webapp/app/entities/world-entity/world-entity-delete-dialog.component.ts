import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWorldEntity } from 'app/shared/model/world-entity.model';
import { WorldEntityService } from './world-entity.service';

@Component({
  templateUrl: './world-entity-delete-dialog.component.html',
})
export class WorldEntityDeleteDialogComponent {
  worldEntity?: IWorldEntity;

  constructor(
    protected worldEntityService: WorldEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.worldEntityService.delete(id).subscribe(() => {
      this.eventManager.broadcast('worldEntityListModification');
      this.activeModal.close();
    });
  }
}
