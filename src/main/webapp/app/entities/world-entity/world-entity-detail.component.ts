import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWorldEntity } from 'app/shared/model/world-entity.model';

@Component({
  selector: 'jhi-world-entity-detail',
  templateUrl: './world-entity-detail.component.html',
})
export class WorldEntityDetailComponent implements OnInit {
  worldEntity: IWorldEntity | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ worldEntity }) => (this.worldEntity = worldEntity));
  }

  previousState(): void {
    window.history.back();
  }
}
