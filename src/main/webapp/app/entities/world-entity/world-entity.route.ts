import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IWorldEntity, WorldEntity } from 'app/shared/model/world-entity.model';
import { WorldEntityService } from './world-entity.service';
import { WorldEntityComponent } from './world-entity.component';
import { WorldEntityDetailComponent } from './world-entity-detail.component';
import { WorldEntityUpdateComponent } from './world-entity-update.component';

@Injectable({ providedIn: 'root' })
export class WorldEntityResolve implements Resolve<IWorldEntity> {
  constructor(private service: WorldEntityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWorldEntity> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((worldEntity: HttpResponse<WorldEntity>) => {
          if (worldEntity.body) {
            return of(worldEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new WorldEntity());
  }
}

export const worldEntityRoute: Routes = [
  {
    path: '',
    component: WorldEntityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.worldEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: WorldEntityDetailComponent,
    resolve: {
      worldEntity: WorldEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.worldEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: WorldEntityUpdateComponent,
    resolve: {
      worldEntity: WorldEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.worldEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: WorldEntityUpdateComponent,
    resolve: {
      worldEntity: WorldEntityResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'warMakerApp.worldEntity.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
