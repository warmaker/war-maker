import { Component, OnInit } from '@angular/core';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';

@Component({
    selector: 'war-player-info',
    templateUrl: './player-info.component.html',
    styles: []
})
export class PlayerInfoComponent implements OnInit {
    constructor(public userDataService: UserDataService) {}

    ngOnInit(): void {}
}
