import { Component, OnInit } from '@angular/core';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';

import { WarMakerMainPageEventsService } from './war-maker-main-page-events.service';

@Component({
    selector: 'war-main-page',
    templateUrl: './main-page.component.html',
    providers: [WarMakerMainPageEventsService],
    styleUrls: []
})
export class MainPageComponent implements OnInit {
    public profileId: number;
    public cityId: number;
    public hasPlayerChoosedCity = false;

    constructor(private userDataService: UserDataService) { }

    ngOnInit(): void {
        this.userDataService.getLatestCity()
            .subscribe((it) => {
                this.cityId = it.id;
                this.profileId = it.info.ownerId;
                this.hasPlayerChoosedCity = true;
            });
    }
}
