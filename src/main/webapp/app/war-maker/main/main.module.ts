import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CityViewComponent } from './city-view/city-view.component';
import { MainPageComponent } from './main-page.component';
import { RouterModule } from '@angular/router';
import { MAIN_ROUTES } from './main.route';
import { ResourcesViewComponent } from './city-view/resources-view/resources-view.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BuildingsViewComponent } from './city-view/buildings-view/buildings-view.component';
import { BuildingTileComponent } from './city-view/buildings-view/building-tile/building-tile.component';
import { EventsViewerComponent } from './events-viewer/events-viewer.component';
import { UserDataService } from '../shared/tracking/user-data.service';
import { ArmyEventsComponent } from './events-viewer/army-events/army-events.component';
import { CityEventsComponent } from './events-viewer/city-events/city-events.component';
import { WarMakerSharedModule } from 'app/shared/shared.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [CommonModule, WarMakerSharedModule, RouterModule.forChild(MAIN_ROUTES), NgbModule, SharedModule],
    declarations: [
        CityViewComponent,
        MainPageComponent,
        ResourcesViewComponent,
        BuildingsViewComponent,
        BuildingTileComponent,
        EventsViewerComponent,
        ArmyEventsComponent,
        CityEventsComponent
    ],
    providers: [UserDataService]
})
export class MainModule {}
