import { Component, Input, OnInit } from '@angular/core';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';

import { CityResponse } from './value/city-response';

@Component({
    selector: 'war-city-view',
    templateUrl: './city-view.component.html',
    styleUrls: ['./city-view.scss']
})
export class CityViewComponent implements OnInit {
    @Input() protected cityId: number;
    public city: CityResponse | null = null;

    constructor(private userDataService: UserDataService) {}

    ngOnInit(): void {
        this.city = this.userDataService.getCurrentCity();
    }
}
