export abstract class CityInfrastructure {
    public stats: InfrastructureBaseStats;
    public level: number;
    public pointValue: number;
    public toughness: number;
    public name: string;
}

export class InfrastructureBaseStats {
    public basePointValue: number;
    public baseToughness: number;
    public baseBuildSpeed: number;
}
