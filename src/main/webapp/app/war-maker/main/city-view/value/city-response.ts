import { BuildingDto } from './building-dto';

export interface CityResponse {
    id: number;
    coordinates: CoordinatesDto;
    fieldType: string;
    moveModifier: number;
    info: Info;
    resources: Resources;
    buildings: Buildings;
    researches: Researches;
    recruitmentOrders?: RecruitmentOrdersEntity[] | null;
    currentUpgradeBuildingEvents?: UpgradeBuildingCommand[] | null;
}

export interface CoordinatesDto {
    x: number;
    y: number;
}

export interface Info {
    ownerId: number;
    armyId: number;
    cityName?: null;
    cityType?: null;
    recruitmentModifier: number;
    buildSpeedModifier: number;
}

export interface Resources {
    storedResources: ResourcesValue;
    incomeRate: IncomeRate;
    changeDate: string;
}

export class ResourcesValue {
    constructor(
        public denars: number,
        public wood: number,
        public stone: number,
        public steel: number,
        public food: number,
        public faith: number,
        public id: number
    ) {}

    public static minus(from: ResourcesValue, resources: ResourcesValue): ResourcesValue {
        const res = new ResourcesValue(0, 0, 0, 0, 0, 0, 0);
        res.denars = from.denars - resources.denars;
        res.wood = from.wood - resources.wood;
        res.stone = from.stone - resources.stone;
        res.steel = from.steel - resources.steel;
        res.food = from.food - resources.food;
        res.faith = from.faith - resources.faith;
        return res;
    }

    public static empty(): ResourcesValue {
        return new ResourcesValue(0, 0, 0, 0, 0, 0, 0);
    }

}

export interface IncomeRate {
    denarsIncome: number;
    woodIncome: number;
    stoneIncome: number;
    steelIncome: number;
    foodIncome: number;
    faithIncome: number;
    timeFormat: string;
}

export interface BuildingsUpgradeRequirementMatched {
  canUpgradeSawmill: boolean;
  canUpgradeOreMine: boolean;
  canUpgradeGoldMine: boolean;
  canUpgradeIronworks: boolean;
  canUpgradeCityHall: boolean;
  canUpgradeUniversity: boolean;
  canUpgradeForge: boolean;
  canUpgradeBarracks: boolean;
}

export interface Buildings {
    goldMine?: BuildingDto;
    sawMill?: BuildingDto;
    oreMine?: BuildingDto;
    ironworks?: BuildingDto;
    barracks?: BuildingDto;
    cityHall?: BuildingDto;
    university?: BuildingDto;
    forge?: BuildingDto;
    buildingsUpgradeRequirementMatched: BuildingsUpgradeRequirementMatched;
}

export interface Building {
    stats: BuildingStats;
    level: number;
    pointValue: number;
    toughness: number;
    upgradingLevel: number;
}

export interface UpgradeCost {
    denars: number;
    wood: number;
    stone: number;
    steel: number;
    food: number;
    faith: number;
}

export interface BuildingStats {
    basePointValue: number;
    baseToughness: number;
    baseBuildSpeed: number;
}

export interface ResourceIncome {
    STONE: number;
    STEEL: number;
    DENARS: number;
    WOOD: number;
}

export interface Researches {
    id: number;
    infantryVitality: number;
    cavalryVitality: number;
    lightArmorValue: number;
    heavyArmorValue: number;
    meleeAttack: number;
    rangedAttack: number;
    infantrySpeed: number;
    cavalrySpeed: number;
    cityBuildingSpeed: number;
    recruitmentSpeed: number;
    scouting: number;
}

export interface RecruitmentOrdersEntity {
    uuid: string;
    type: string;
    recruitmentOrders: RecruitmentOrders;
    orderTime: string;
    finishTime: string;
    armyId: number;
    lastReadTime: string;
}

export interface RecruitmentOrders {
    recruitSpearman: RecruitUnitOrder;
    recruitSwordsman: RecruitUnitOrder;
    recruitArchers: RecruitUnitOrder;
    recruitCrossbowman: RecruitUnitOrder;
    timeUnit: string;
    finished: boolean;
}

export interface RecruitUnitOrder {
    numberOfUnitsToRecruit: number;
    timeToRecruitOneUnit: number;
    timeSpendOnRecruiting: number;
    remainingRecruitmentTime: number;
    totalRecruitmentTime: number;
    finished: boolean;
    empty: boolean;
}

export class UpgradeBuildingCommand {
    uuid: string;
    type: string;
    resourcesUsed: ResourcesValue;
    ownerId: number;
    executionTime: string;
}
