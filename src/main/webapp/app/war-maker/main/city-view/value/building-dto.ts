import { Building, UpgradeCost } from './city-response';

export interface BuildingDto {
    building: Building;
    upgradingLevel: number;
    upgradeCost: UpgradeCost;
    name: string;
    buildTime: number;
    nextLevelBuildTime: number;
}