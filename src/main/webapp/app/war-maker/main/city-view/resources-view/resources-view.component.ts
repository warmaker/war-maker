import { Component, Input, OnInit } from '@angular/core';
import { interval } from 'rxjs';

import { TimeFormat } from './resources-dto';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';
import { Resources, ResourcesValue } from '../value/city-response';
import { WarMakerMainPageEventsService } from '../../war-maker-main-page-events.service';
import { RESOURCES_IMAGE_PATHS } from 'app/war-maker/shared/constants/resource-images';

@Component({
    selector: 'war-resources-view',
    templateUrl: './resources-view.component.html',
    styleUrls: ['./resources-view.scss']
})
export class ResourcesViewComponent implements OnInit {
    readonly resourcesImagePaths = RESOURCES_IMAGE_PATHS;
    @Input() private cityId: number;
    resources: Resources;
    timeFormat = TimeFormat;

    constructor(private userDataService: UserDataService,
                private eventsService: WarMakerMainPageEventsService) { }

    ngOnInit(): void {
        const currentCity = this.userDataService.getCurrentCity();
        if (currentCity) {
            this.resources = currentCity.resources;
            this.updateResources();
        }
        this.eventsService.buildingUpgraded.subscribe(event => {
            this.handleResourceChange(event.resourcesUsed);
        })
    }

    updateResources(): void {
        let updateInterval;
        let divination;
        switch (this.resources.incomeRate.timeFormat) {
            case this.timeFormat.MINUTES:
                updateInterval = 60000;
                divination = 12;
                break;
            case this.timeFormat.SECONDS:
                updateInterval = 1000;
                divination = 0.2;
                break;
            default:
                updateInterval = 60000;
                divination = 12;
                break;
        }
        this.update(updateInterval, divination);
    }

    public handleResourceChange(resourceChange: ResourcesValue): void {
        this.resources.storedResources = ResourcesValue.minus(this.resources.storedResources, resourceChange);
    }

    private update(updateInterval: number, divination: number): void {
        interval(updateInterval / divination).subscribe(() => {
            this.resources.storedResources.denars += this.resources.incomeRate.denarsIncome / divination;
            this.resources.storedResources.wood += this.resources.incomeRate.woodIncome / divination;
            this.resources.storedResources.stone += this.resources.incomeRate.stoneIncome / divination;
            this.resources.storedResources.steel += this.resources.incomeRate.steelIncome / divination;
            this.resources.storedResources.food += this.resources.incomeRate.foodIncome / divination;
            this.resources.storedResources.faith += this.resources.incomeRate.faithIncome / divination;
        });
    }
}
