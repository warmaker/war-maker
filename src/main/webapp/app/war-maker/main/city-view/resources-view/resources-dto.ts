export enum TimeFormat {
    MINUTES = 'MINUTES',
    SECONDS = 'SECONDS'
}
