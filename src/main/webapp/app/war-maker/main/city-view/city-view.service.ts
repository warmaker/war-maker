import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CityResponse } from './value/city-response';
import { WarPartyEntity } from 'app/shared/model/war-party-entity.model';

@Injectable({
    providedIn: 'root'
})
export class CityViewService {
    public resourceUrl = SERVER_API_URL + 'api/city';

    constructor(private http: HttpClient) {}

    findCity(cityId: number): Observable<CityResponse> {
        return this.http.get<CityResponse>(`${this.resourceUrl}/${cityId}`);
    }

    findWarParties(cityId: number): Observable<WarPartyEntity[]> {
        return this.http.get<WarPartyEntity[]>(`${this.resourceUrl}/${cityId}/war-parties`);
    }
}
