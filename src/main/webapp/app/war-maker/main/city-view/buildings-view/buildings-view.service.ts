import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { UpgradeBuildingCommand } from '../value/city-response';

@Injectable({
    providedIn: 'root'
})
export class BuildingsViewService {
    public resourceUrl = SERVER_API_URL + 'api/city/buildings';

    constructor(private http: HttpClient) {}

    upgradeSawMill(cityId: number): Observable<UpgradeBuildingCommand> {
        return this.http.post<UpgradeBuildingCommand>(`${this.resourceUrl}/${cityId}/saw-mill/up`, { observe: 'response' });
    }

    upgradeOreMine(cityId: number): Observable<UpgradeBuildingCommand> {
        return this.http.post<UpgradeBuildingCommand>(`${this.resourceUrl}/${cityId}/ore-mine/up`, { observe: 'response' });
    }

    upgradeGoldMine(cityId: number): Observable<UpgradeBuildingCommand> {
        return this.http.post<UpgradeBuildingCommand>(`${this.resourceUrl}/${cityId}/gold-mine/up`, { observe: 'response' });
    }

    upgradeIronworks(cityId: number): Observable<UpgradeBuildingCommand> {
        return this.http.post<UpgradeBuildingCommand>(`${this.resourceUrl}/${cityId}/ironworks/up`, { observe: 'response' });
    }

    upgradeBarracks(cityId: number): Observable<UpgradeBuildingCommand> {
        return this.http.post<UpgradeBuildingCommand>(`${this.resourceUrl}/${cityId}/barracks/up`, { observe: 'response' });
    }

    upgradeCityHall(cityId: number): Observable<UpgradeBuildingCommand> {
        return this.http.post<UpgradeBuildingCommand>(`${this.resourceUrl}/${cityId}/city-hall/up`, { observe: 'response' });
    }

    upgradeUniversity(cityId: number): Observable<UpgradeBuildingCommand> {
        return this.http.post<UpgradeBuildingCommand>(`${this.resourceUrl}/${cityId}/university/up`, { observe: 'response' });
    }

    upgradeForge(cityId: number): Observable<UpgradeBuildingCommand> {
        return this.http.post<UpgradeBuildingCommand>(`${this.resourceUrl}/${cityId}/forge/up`, { observe: 'response' });
    }
}
