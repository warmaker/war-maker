import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { BuildingDto } from '../../value/building-dto';
import { RESOURCES_IMAGE_PATHS } from 'app/war-maker/shared/constants/resource-images';

@Component({
    selector: 'war-building-tile',
    templateUrl: './building-tile.component.html',
    styleUrls: ['./building-tile.scss']
})
export class BuildingTileComponent implements OnInit {
    readonly resourcesImagePaths = RESOURCES_IMAGE_PATHS;
    isCollapsed = true;
    src: string;
    @Input() routerUrl: string;
    @Input() buildingDto: BuildingDto;
    @Input() imageSrc: string;
    @Input() upgradeRequirementMet: boolean;
    @Input() buildingName: string;
    @Output() buildingUpgrade = new EventEmitter<Number>();

    constructor() {}

    ngOnInit(): void {
        this.src = this.imageSrc;
    }

    upgradeBuilding(): void {
        this.buildingUpgrade.emit(this.buildingDto.building.level);
    }
}
