import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';
import { JhiAlertService } from 'ng-jhipster';

import { WarMakerMainPageEventsService } from '../../war-maker-main-page-events.service';
import { BuildingDto } from '../value/building-dto';
import { Buildings, UpgradeBuildingCommand } from '../value/city-response';
import { BuildingsViewService } from './buildings-view.service';

@Component({
    selector: 'war-buildings-view',
    templateUrl: './buildings-view.component.html',
    styleUrls: ['./buildings-view.scss']
})
export class BuildingsViewComponent implements OnInit {
    isCollapsed = true;
    buildings: Buildings;
    @Input() cityId: number;

    constructor(
        private buildingsService: BuildingsViewService,
        private eventsService: WarMakerMainPageEventsService,
        private alertService: JhiAlertService,
        private userDataService: UserDataService
    ) {}

    ngOnInit(): void {
        const currentCity = this.userDataService.getCurrentCity();
        if (currentCity) {
            this.buildings = currentCity.buildings;
        }
    }

    public upgradeGoldMine(building: BuildingDto): void {
        this.buildingsService.upgradeGoldMine(this.cityId).subscribe(
            res => this.onBuildingUpgradeSuccess(res, building),
            (error: HttpErrorResponse) => {
                this.alertService.error(error.error, null, '');
            }
        );
    }

    public upgradeSawMill(building: BuildingDto): void {
        this.buildingsService.upgradeSawMill(this.cityId).subscribe(
            res => this.onBuildingUpgradeSuccess(res, building),
            (error: HttpErrorResponse) => {
                this.alertService.error(error.error, null, '');
            }
        );
    }

    public upgradeOreMine(building: BuildingDto): void {
        this.buildingsService.upgradeOreMine(this.cityId).subscribe(
            res => this.onBuildingUpgradeSuccess(res, building),
            (error: HttpErrorResponse) => {
                this.alertService.error(error.error, null, '');
            }
        );
    }

    public upgradeIronworks(building: BuildingDto): void {
        this.buildingsService.upgradeIronworks(this.cityId).subscribe(
            res => this.onBuildingUpgradeSuccess(res, building),
            (error: HttpErrorResponse) => {
                this.alertService.error(error.error, null, '');
            }
        );
    }

    public upgradeBarracks(building: BuildingDto): void {
        this.buildingsService.upgradeBarracks(this.cityId).subscribe(
            res => this.onBuildingUpgradeSuccess(res, building),
            (error: HttpErrorResponse) => {
                this.alertService.error(error.error, null, '');
            }
        );
    }

    public upgradeCityHall(building: BuildingDto): void {
        this.buildingsService.upgradeCityHall(this.cityId).subscribe(
            res => this.onBuildingUpgradeSuccess(res, building),
            (error: HttpErrorResponse) => {
                this.alertService.error(error.error, null, '');
            }
        );
    }

    public upgradeUniversity(building: BuildingDto): void {
        this.buildingsService.upgradeUniversity(this.cityId).subscribe(
            res => this.onBuildingUpgradeSuccess(res, building),
            (error: HttpErrorResponse) => {
                this.alertService.error(error.error, null, '');
            }
        );
    }

    public upgradeForge(building: BuildingDto): void {
        this.buildingsService.upgradeForge(this.cityId).subscribe(
            res => this.onBuildingUpgradeSuccess(res, building),
            (error: HttpErrorResponse) => {
                this.alertService.error(error.error, null, '');
            }
        );
    }

    protected onBuildingUpgradeSuccess(res: UpgradeBuildingCommand, building: BuildingDto): void {
        this.alertService.success('success', null, '');
        building.building.upgradingLevel++;
        this.eventsService.buildingUpgraded.next(res);
        this.isCollapsed = false;
        this.userDataService.getLatestCity().subscribe(cityResponse => {
            this.buildings = cityResponse.buildings;
        });
    }
}
