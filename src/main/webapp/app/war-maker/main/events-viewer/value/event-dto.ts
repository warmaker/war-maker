import { EventType } from './event-type.enum';

export interface EventDto {
    ownerId: number;
    type: EventType;
}
