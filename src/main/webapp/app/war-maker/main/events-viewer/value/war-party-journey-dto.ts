import { EventDto } from './event-dto';
import { Coordinates } from '../../../world-map/map/value/coordinates';

export interface WarPartyJourneyDto extends EventDto {
    movePath?: (Coordinates)[] | null;
    warPartyId: number;
    command: string;
    returning: boolean;
    nextStopArrivalTime: string;
    nextStop: Coordinates;
    currentPosition: Coordinates;
}