import { Component, OnInit } from '@angular/core';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';

import { UpgradeBuildingCommand } from '../../city-view/value/city-response';
import { WarMakerMainPageEventsService } from '../../war-maker-main-page-events.service';

@Component({
    selector: 'war-city-events',
    templateUrl: './city-events.component.html',
    styles: []
})
export class CityEventsComponent implements OnInit {
    upgradeBuildingCommands: UpgradeBuildingCommand[];

    constructor(private userDataService: UserDataService,
                private eventsService: WarMakerMainPageEventsService) {}

    ngOnInit(): void {
        this.getCityEvents();
        this.eventsService.buildingUpgraded.subscribe(event => {
            this.upgradeBuildingCommands.push(event);
        });
    }

    private getCityEvents(): void {
        const currentCity = this.userDataService.getCurrentCity();
        if (currentCity && currentCity.currentUpgradeBuildingEvents) {
            this.upgradeBuildingCommands = currentCity.currentUpgradeBuildingEvents;
        }
    }
}
