import { Component, OnInit } from '@angular/core';
import { WAR_MAKER_DATE_FORMAT } from 'app/war-maker/shared/constants/date-constants';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';
import * as moment from 'moment';

import { RecruitmentOrdersEntity } from '../../city-view/value/city-response';

@Component({
    selector: 'war-army-events',
    templateUrl: './army-events.component.html',
    styles: []
})
export class ArmyEventsComponent implements OnInit {
    armyEvents: RecruitmentOrdersEntity[];

    constructor(private userDataService: UserDataService) { }

    ngOnInit(): void {
        this.getArmyEvents();
    }

    private getArmyEvents(): void {
        const currentCity = this.userDataService.getCurrentCity();
        if (currentCity && currentCity.recruitmentOrders) {
            this.armyEvents = currentCity.recruitmentOrders.map(event => {
                event.finishTime = moment(event.finishTime).format(WAR_MAKER_DATE_FORMAT);
                return event;
            });
        }
    }
}
