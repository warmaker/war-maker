import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Subscription } from 'rxjs';

import { MainPageService } from '../main-page.service';
import { WarMakerMainPageEventsService } from '../war-maker-main-page-events.service';
import { EventType } from './value/event-type.enum';
import { EventDto } from './value/event-dto';
import { WarPartyJourneyDto } from './value/war-party-journey-dto';
import { WAR_MAKER_DATE_FORMAT } from 'app/war-maker/shared/constants/date-constants';

@Component({
    selector: 'war-events-viewer',
    templateUrl: './events-viewer.component.html',
    styles: []
})
export class EventsViewerComponent implements OnInit, OnDestroy {
    profileEvents: WarPartyJourneyDto[];
    eventType = EventType;
    buildingsSubscription: Subscription;
    @Input() profileId: number;

    constructor(private mainPageService: MainPageService, private eventsService: WarMakerMainPageEventsService) {}

    ngOnInit(): void {
        this.getProfileEvents();
        this.buildingsSubscription = this.eventsService.buildingUpgraded.subscribe(() => this.getProfileEvents());
    }

    ngOnDestroy(): void {
        if (this.buildingsSubscription) {
            this.buildingsSubscription.unsubscribe();
        }
    }

    private getProfileEvents(): void {
        this.mainPageService.getWarPartyJourneyEvents(this.profileId).subscribe(res => {
            this.profileEvents = res
                .map(event => {
                    event.nextStopArrivalTime = moment(event.nextStopArrivalTime).format(WAR_MAKER_DATE_FORMAT);
                    return event;
            });
        });
    }

    isWarPartyJourneyEvent = (event: EventDto): event is WarPartyJourneyDto => {
        return event.type === this.eventType.WAR_PARTY_JOURNEY;
    }

}
