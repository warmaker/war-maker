import { Route } from '@angular/router';
import { MainPageComponent } from './main-page.component';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

const mainRoute: Route = {
    path: '',
    component: MainPageComponent,
    canActivate: [UserRouteAccessService],
    data: {
        pageTitle: 'warMaker.navbar.main'
    }
};

export const MAIN_ROUTES = [mainRoute];
