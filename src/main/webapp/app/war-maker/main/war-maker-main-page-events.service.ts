import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { UpgradeBuildingCommand } from './city-view/value/city-response';

@Injectable()
export class WarMakerMainPageEventsService {
    public buildingUpgraded: Subject<UpgradeBuildingCommand> = new Subject();

    constructor() {}
}
