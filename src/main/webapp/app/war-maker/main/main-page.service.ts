import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { EventDto } from './events-viewer/value/event-dto';
import { WarPartyJourneyDto } from './events-viewer/value/war-party-journey-dto';
import { EventType } from './events-viewer/value/event-type.enum';

@Injectable({
    providedIn: 'root'
})
export class MainPageService {
    public resourceUrl = SERVER_API_URL + 'api/profiles';
    public armyResourceUrl = SERVER_API_URL + 'api/army';

    constructor(private http: HttpClient) {}

    getCities(profileId: number): Observable<number[]> {
        return this.http.get<number[]>(`${this.resourceUrl}/${profileId}/cities`);
    }

    getEvents(profileId: number): Observable<WarPartyJourneyDto[] | EventDto[]> {
        return this.http.get<EventDto[]>(`${this.resourceUrl}/${profileId}/events`);
    }

    getWarPartyJourneyEvents(profileId: number): Observable<WarPartyJourneyDto[]> {
        return this.http
            .get<EventDto[]>(`${this.resourceUrl}/${profileId}/events`)
            .pipe(map(x => x.filter(this.isWarPartyJourneyEvent)));
    }

    private isWarPartyJourneyEvent = (event: EventDto): event is WarPartyJourneyDto => {
        return event.type === EventType.WAR_PARTY_JOURNEY;
    };
}
