import { Component, OnInit } from '@angular/core';
import { mergeMap } from 'rxjs/operators';

import { UserDataService } from '../shared/tracking/user-data.service';
import { MessageBoxService } from './message-box.service';
import { PlayerMessages } from './player-messages';

@Component({
    selector: 'war-message-box',
    templateUrl: './message-box.component.html',
    styleUrls: ['./message-box.component.scss']
})
export class MessageBoxComponent implements OnInit {
    playerMessages: PlayerMessages;

    constructor(private messageBoxService: MessageBoxService,
                public userDataService: UserDataService) {}

    ngOnInit(): void {
        this.userDataService.dataLoaded
            .pipe(mergeMap(() => this.messageBoxService.getMessages(this.userDataService.getCurrentProfile())))
            .subscribe(res => (this.playerMessages = res));
    }
}
