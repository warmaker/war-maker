import { Moment } from 'moment';
import { HumanUnitsListDto } from '../army/army/army-dto';

export interface PlayerMessage {
}


export interface PlayerMessages {
  id: string;
  playerId: number;
  battleMessages?: (BattleMessage)[] | null;
  attackingWarDeclarationMessages?: (WarDeclarationMessage)[] | null;
  defendingWarDeclarationMessages?: (WarDeclarationMessage)[] | null;
}

export interface WarDeclarationMessage extends PlayerMessage {
  id: string;
  declarationTime: string;
  attacker: PlayerParticipating; 
  defender: PlayerParticipating;
}

export interface PlayerParticipating {
  id: string;
  playerId: number;
}


export interface BattleMessage extends PlayerMessage {
    battleTime: Moment;
    id: string;
    attackerWarPartyId: number;
    defenderWarPartyId: number;
    attackerId: number;
    defenderId: number;
    attackerLoses: HumanUnitsListDto;
    defenderLoses: HumanUnitsListDto;
    player?: null;
}
