import { Component, Input, OnInit } from '@angular/core';

import { BattleMessage } from '../../player-messages';

@Component({
    selector: 'war-player-info-display',
    templateUrl: './player-info-display.component.html',
    styleUrls: ['./player-info-display.component.scss']
})
export class PlayerInfoDisplayComponent implements OnInit {
    @Input() battleMessage: BattleMessage;
    infoToDisplay: string;

    constructor() {}

    ngOnInit(): void {
        if (this.battleMessage.defenderId === -1) {
            this.infoToDisplay = 'Bandit';
        } else {
            this.infoToDisplay = this.battleMessage.defenderId + '';
        }
    }
}
