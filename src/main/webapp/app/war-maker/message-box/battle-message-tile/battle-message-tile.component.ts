import { Component, Input, OnInit } from '@angular/core';

import { BattleMessage } from '../player-messages';

@Component({
    selector: 'war-battle-message-tile',
    templateUrl: './battle-message-tile.component.html',
    styleUrls: ['./battle-message-tile.component.scss']
})
export class BattleMessageTileComponent implements OnInit {
    @Input() public message: BattleMessage;
    public isCollapsed = true;

    constructor() {}

    ngOnInit(): void {}
}
