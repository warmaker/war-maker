import { Component, OnInit, Input } from '@angular/core';
import { WarDeclarationMessage } from '../player-messages';

@Component({
    selector: 'war-war-defending-tile',
    templateUrl: './war-defending-tile.component.html',
    styleUrls: ['./war-defending-tile.component.scss']
})
export class WarDefendingTileComponent implements OnInit {
    @Input() public message: WarDeclarationMessage;
    public isCollapsed = false;
    
    constructor() { }


    ngOnInit(): void {
    }

}
