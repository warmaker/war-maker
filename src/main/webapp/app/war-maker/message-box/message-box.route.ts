import { Route } from '@angular/router';
import { MessageBoxComponent } from './message-box.component';

const messageBoxRoute: Route = {
    path: '',
    component: MessageBoxComponent,
    resolve: {},
    data: {
        pageTitle: 'warMaker.navbar.envelope'
    }
};

export const MESSAGE_BOX_ROUTES = [messageBoxRoute];
