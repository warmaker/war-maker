import { Component, OnInit, Input } from '@angular/core';
import { WarDeclarationMessage } from '../player-messages';

@Component({
    selector: 'war-war-declaration-tile',
    templateUrl: './war-declaration-tile.component.html',
    styleUrls: ['./war-declaration-tile.component.scss']
})
export class WarDeclarationTileComponent implements OnInit {
    @Input() public message: WarDeclarationMessage;
    public isCollapsed = false;

    constructor() { }

    ngOnInit(): void {
    }

}
