import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { PlayerMessages } from './player-messages';

@Injectable()
export class MessageBoxService {
    public resourceUrl = SERVER_API_URL + 'services/messagebox/api/messages';

    constructor(private http: HttpClient) {}

    getMessages(profileId: number): Observable<PlayerMessages> {
        return this.http.get<PlayerMessages>(`${this.resourceUrl}/${profileId}`).pipe(
            map(res => {
                if (res) {
                    if (res.battleMessages) {
                        res.battleMessages = res.battleMessages.sort((a, b) =>
                            moment(a.battleTime).isBefore(moment(b.battleTime)) ? 1 : -1
                        );
                    }
                }
                return res;
            })
        );
    }
}
