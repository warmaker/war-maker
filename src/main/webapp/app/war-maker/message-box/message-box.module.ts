import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared/shared.module';
import { BattleMessageTileComponent } from './battle-message-tile/battle-message-tile.component';
import { MessageBoxComponent } from './message-box.component';
import { MESSAGE_BOX_ROUTES } from './message-box.route';
import { MessageBoxService } from './message-box.service';
import { WarMakerSharedModule } from 'app/shared/shared.module';
import { PlayerInfoDisplayComponent } from './battle-message-tile/player-info-display/player-info-display.component';
import { WarDeclarationTileComponent } from './war-declaration-tile/war-declaration-tile.component';
import { WarDefendingTileComponent } from './war-defending-tile/war-defending-tile.component';

@NgModule({
    imports: [CommonModule, RouterModule.forChild(MESSAGE_BOX_ROUTES), NgbModule, SharedModule, WarMakerSharedModule],
    providers: [MessageBoxService],
    declarations: [MessageBoxComponent, BattleMessageTileComponent, PlayerInfoDisplayComponent, WarDeclarationTileComponent, WarDefendingTileComponent]
})
export class MessageBoxModule {}
