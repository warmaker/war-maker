export class ArmyDto {
    constructor(public id: number, public units: ArmyUnitsDto) {}
}

export class ArmyUnitsDto {
    constructor(
        public id: number,
        public available: HumanUnitsListDto,
        public stationed: HumanUnitsListDto,
        public fielded: HumanUnitsListDto
    ) {}
}

export class HumanUnitsListDto {
    constructor(
        public spearman: number,
        public swordsman: number,
        public axeman: number,
        public enforcer: number,
        public forestWarrior: number,
        public archers: number,
        public crossbowman: number,
        public ranger: number,
        public lightCavalry: number,
        public lancers: number,
        public heavyCavalry: number
    ) {}

    static empty(): HumanUnitsListDto {
        return new HumanUnitsListDto(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    }
}
