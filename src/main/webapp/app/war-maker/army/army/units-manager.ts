import { ResourcesValue } from 'app/war-maker/main/city-view/value/city-response';

import { ArmyDto } from './army-dto';

export enum UnitType {
    SPEARMAN = 'spearman',
    SWORDSMAN = 'swordsman',
    AXEMAN = 'axeman',
    ENFORCER = 'enforcer',
    FOREST_WARRIOR = 'forestWarrior',
    LIGHT_CAVALRY = 'lightCavalry',
    LANCERS = 'lancers',
    HEAVY_CAVALRY = 'heavyCavalry',
    SPY = 'spy',
    ARCHER = 'archers',
    CROSSBOWMAN = 'crossbowman',
    RANGER = 'ranger'
}

export class Unit {
    constructor(
        public type: UnitType,
        public displayedName: string,
        public availableUnitsCount: number,
        public stationedUnitsCount: number,
        public fieldedUnitsCount: number,
        public cost: ResourcesValue,
        public imageSrc: string
    ) {}
}

export class UnitsCost {
    spearman: ResourcesValue;
    swordsman: ResourcesValue;
    archer: ResourcesValue;
    crossbowman: ResourcesValue;

    constructor() {
        this.spearman = new ResourcesValue(200, 50, 0, 10, 0, 0, 0);
        this.swordsman = new ResourcesValue(600, 20, 0, 100, 0, 0, 0);
        this.archer = new ResourcesValue(600, 100, 0, 50, 0, 0, 0);
        this.crossbowman = new ResourcesValue(1000, 20, 0, 200, 0, 0, 0);
    }
}

export class UnitsIcons {
    spearman: string;
    swordsman: string;
    axeman: string;
    enforcer: string;
    forestWarrior: string;
    lightCavalry: string;
    lancers: string;
    heavyCavalry: string;
    spy: string;
    archers: string;
    crossbowman: string;
    ranger: string;

    constructor() {
        this.spearman = '/content/images/units/spear-hook.svg';
        this.swordsman = './content/images/units/two-handed-sword.svg';
        this.axeman = './content/images/units/fire-axe.svg';
        this.enforcer = './content/images/units/flanged-mace.svg';
        this.forestWarrior = './content/images/units/sacrificial-dagger.svg';
        this.lightCavalry = './content/images/units/cavalry.svg';
        this.lancers = './content/images/units/chariot.svg';
        this.heavyCavalry = './content/images/units/mounted-knight.svg';
        this.spy = './content/images/units/spyglass.svg';
        this.archers = './content/images/units/high-shot.svg';
        this.crossbowman = './content/images/units/crossbow.svg';
        this.ranger = './content/images/units/wolf-trap.svg';
    }
}

export class UnitsManager {
    private unitsCost = new UnitsCost();
    private unitsIcons = new UnitsIcons();

    constructor(private currentArmy: ArmyDto) {}

    public getInfantryUnits(): Unit[] {
        return [
            new Unit(
                UnitType.SPEARMAN,
                'Spearman',
                this.currentArmy.units.available.spearman,
                this.currentArmy.units.stationed.spearman,
                this.currentArmy.units.fielded.spearman,
                this.unitsCost.spearman,
                this.unitsIcons.spearman
            ),
            new Unit(
                UnitType.SWORDSMAN,
                'Swordsman',
                this.currentArmy.units.available.swordsman,
                this.currentArmy.units.stationed.swordsman,
                this.currentArmy.units.fielded.swordsman,
                this.unitsCost.swordsman,
                this.unitsIcons.swordsman
            ),
            // new Unit(
            //     UnitType.AXEMAN,
            //     'Axeman',
            //     this.currentArmy.units.available.axeman,
            //     this.currentArmy.units.stationed.axeman,
            //     this.currentArmy.units.fielded.axeman,
            //     ResourcesValue.empty(),
            //     this.unitsIcons.axeman
            // ),
            // new Unit(
            //     UnitType.ENFORCER,
            //     'Enforcer',
            //     this.currentArmy.units.available.enforcer,
            //     this.currentArmy.units.stationed.enforcer,
            //     this.currentArmy.units.fielded.enforcer,
            //     ResourcesValue.empty(),
            //     this.unitsIcons.enforcer
            // ),
            // new Unit(
            //     UnitType.FOREST_WARRIOR,
            //     'Forest Warrior',
            //     this.currentArmy.units.available.forestWarrior,
            //     this.currentArmy.units.stationed.forestWarrior,
            //     this.currentArmy.units.fielded.forestWarrior,
            //     ResourcesValue.empty(),
            //     this.unitsIcons.forestWarrior
            // )
        ];
    }

    public getCavalryUnits(): Unit[] {
        return [
            // new Unit(
            //     UnitType.LIGHT_CAVALRY,
            //     'Light Cavalry',
            //     this.currentArmy.units.available.lightCavalry,
            //     this.currentArmy.units.stationed.lightCavalry,
            //     this.currentArmy.units.fielded.lightCavalry,
            //     ResourcesValue.empty(),
            //     this.unitsIcons.lightCavalry
            // ),
            // new Unit(
            //     UnitType.HEAVY_CAVALRY,
            //     'Heavy Cavalry',
            //     this.currentArmy.units.available.heavyCavalry,
            //     this.currentArmy.units.stationed.heavyCavalry,
            //     this.currentArmy.units.fielded.heavyCavalry,
            //     ResourcesValue.empty(),
            //     this.unitsIcons.heavyCavalry
            // )
        ];
    }

    public getSpecialUnits(): Unit[] {
        return [
            // new Unit(
            //     UnitType.SPY,
            //     'Spy',
            //     this.currentArmy.units.available.archers,
            //     this.currentArmy.units.stationed.archers,
            //     this.currentArmy.units.fielded.archers,
            //     ResourcesValue.empty(),
            //     this.unitsIcons.spy
            // )
        ];
    }

    public getRangedUnits(): Unit[] {
        return [
            new Unit(
                UnitType.ARCHER,
                'Archer',
                this.currentArmy.units.available.archers,
                this.currentArmy.units.stationed.archers,
                this.currentArmy.units.fielded.archers,
                this.unitsCost.archer,
                this.unitsIcons.archers
            ),
            new Unit(
                UnitType.CROSSBOWMAN,
                'Crossbowman',
                this.currentArmy.units.available.crossbowman,
                this.currentArmy.units.stationed.crossbowman,
                this.currentArmy.units.fielded.crossbowman,
                this.unitsCost.crossbowman,
                this.unitsIcons.crossbowman
            ),
            // new Unit(
            //     UnitType.RANGER,
            //     'Ranger',
            //     this.currentArmy.units.available.ranger,
            //     this.currentArmy.units.stationed.ranger,
            //     this.currentArmy.units.fielded.ranger,
            //     ResourcesValue.empty(),
            //     this.unitsIcons.ranger
            // )
        ];
    }
}
