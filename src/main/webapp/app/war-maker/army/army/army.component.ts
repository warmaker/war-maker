import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';
import { mergeMap } from 'rxjs/operators';

import { ArmyService } from '../army.service';
import { ArmyDto, HumanUnitsListDto } from './army-dto';
import { Unit, UnitsManager } from './units-manager';
import { UnitsRecruitmentCalculatorService } from './units-recruitment-calculator.service';
import { RESOURCES_IMAGE_PATHS } from 'app/war-maker/shared/constants/resource-images';

@Component({
    selector: 'war-army',
    templateUrl: './army.component.html',
    providers: [UnitsRecruitmentCalculatorService],
    styleUrls: ['./army.scss']
})
export class ArmyComponent implements OnInit {
    readonly resourcesImagePaths = RESOURCES_IMAGE_PATHS;
    
    cityId: number;
    currentArmy: ArmyDto;
    unitsToRecruit: HumanUnitsListDto;
    infantryUnits: Unit[];
    specialUnits: Unit[];
    rangedUnits: Unit[];
    cavalryUnits: Unit[];

    unitsManager: UnitsManager;

    constructor(
        private userDataService: UserDataService,
        private armyService: ArmyService,
        public unitsRecruitmentCalculatorService: UnitsRecruitmentCalculatorService
    ) {}

    ngOnInit(): void {
        this.userDataService.dataLoaded.subscribe(() => {
            this.userDataService.dataLoaded
                .pipe(
                    mergeMap(() => {
                        this.cityId = this.userDataService.cityId;
                        return this.armyService.getCityArmy(this.userDataService.cityId);
                    })
                )
                .subscribe(res => {
                    this.loadArmy(res);
                });
        });
    }

    private loadArmy(res: HttpResponse<ArmyDto>): void {
        if (res.body) {
            this.currentArmy = res.body;
        }
        this.unitsToRecruit = HumanUnitsListDto.empty();
        this.unitsManager = new UnitsManager(this.currentArmy);
        this.infantryUnits = this.unitsManager.getInfantryUnits();
        this.specialUnits = this.unitsManager.getSpecialUnits();
        this.rangedUnits = this.unitsManager.getRangedUnits();
        this.cavalryUnits = this.unitsManager.getCavalryUnits();
    }
}
