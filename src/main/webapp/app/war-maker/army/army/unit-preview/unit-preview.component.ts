import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ResourcesValue } from 'app/war-maker/main/city-view/value/city-response';
import { RESOURCES_IMAGE_PATHS } from 'app/war-maker/shared/constants/resource-images';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';
import { JhiAlertService } from 'ng-jhipster';
import { Options } from 'ng5-slider';
import { mergeMap } from 'rxjs/operators';
import { ArmyService } from '../../army.service';
import { HumanUnitsListDto } from '../army-dto';
import { Unit } from '../units-manager';
import { UnitsRecruitmentCalculatorService } from '../units-recruitment-calculator.service';


@Component({
    selector: 'war-unit-preview',
    templateUrl: './unit-preview.component.html',
    styleUrls: ['./unit-preview.component.scss']
})
export class UnitPreviewComponent implements OnInit {
    readonly resourcesImagePaths = RESOURCES_IMAGE_PATHS;
    maxUnits: Options;
    @Input() unit: Unit;
    @Input() cityId: number;
    @Output() recruitedUnits = new EventEmitter<number>();

    numberOfRecruitedUnits = 0;
    previousValueOfRecruitedUnits = 0;

    constructor(
        private armyService: ArmyService,
        private unitsRecruitmentCalculatorService: UnitsRecruitmentCalculatorService,
        private userDataService: UserDataService,
        private alertService: JhiAlertService
    ) { }

    ngOnInit(): void {
        this.unitsRecruitmentCalculatorService
            .maxUnits
            .get(this.unit.type)
            .subscribe(
                it => {
                    it.ceil += this.numberOfRecruitedUnits;
                    return this.maxUnits = it;
                }
            )
    }

    recruit(): void {
        const units = HumanUnitsListDto.empty();
        units[this.unit.type] = this.numberOfRecruitedUnits;
        this.armyService.recruit(units, this.cityId)
            .pipe(
                mergeMap(() => this.userDataService.getLatestCity())
            )
            .subscribe(
                () => {
                    this.numberOfRecruitedUnits = 0;
                    this.alertService.success('warMaker.army.recruitmentSuccess');
                },
                (err) => {
                    this.alertService.error(err);
                });
    }

    sliderChanged(): void {
        const change = -(this.previousValueOfRecruitedUnits - this.numberOfRecruitedUnits);
        this.previousValueOfRecruitedUnits = this.numberOfRecruitedUnits;
        const resourcesCost = new ResourcesValue(
            change * this.unit.cost.denars,
            change * this.unit.cost.wood,
            change * this.unit.cost.stone,
            change * this.unit.cost.steel,
            change * this.unit.cost.food,
            change * this.unit.cost.faith,
            -1
        );
        this.unitsRecruitmentCalculatorService.calculateChange(this.unit.type, resourcesCost);
    }
}