import { Injectable, OnInit } from '@angular/core';
import { ResourcesValue } from 'app/war-maker/main/city-view/value/city-response';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';
import { Options } from 'ng5-slider';

import { UnitsCost, UnitType } from './units-manager';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';

const defaultConfig: Options = {
    floor: 0,
    ceil: 0
};

@Injectable()
export class UnitsRecruitmentCalculatorService implements OnInit {
    availableResources: Observable<ResourcesValue>;
    availableResources1: ResourcesValue;
    maxUnits: Map<UnitType, BehaviorSubject<Options>> = new Map([
        [UnitType.SPEARMAN, new BehaviorSubject(defaultConfig)],
        [UnitType.SWORDSMAN, new BehaviorSubject(defaultConfig)],
        [UnitType.ARCHER, new BehaviorSubject(defaultConfig)],
        [UnitType.CROSSBOWMAN, new BehaviorSubject(defaultConfig)]
    ]);
    usedResources = ResourcesValue.empty();

    recruitedSpearman: number;
    recruitedSwordsman: number;
    recruitedArchers: number;
    recruitedCrossbowman: number;

    updateUnits = new ReplaySubject<UnitType>(1);

    private readonly unitsCost = new UnitsCost();

    constructor(private userDataService: UserDataService) {
        this.availableResources = this.userDataService.dataChanged
            .pipe(
                map(
                    () => {
                        const newLocal = JSON.parse(JSON.stringify(this.userDataService?.chosenCity?.resources.storedResources)) as ResourcesValue;
                        this.availableResources1 = newLocal;
                        this.maxUnits.get(UnitType.SPEARMAN).next(this.getSliderConfig(this.unitsCost.spearman))
                        this.maxUnits.get(UnitType.SWORDSMAN).next(this.getSliderConfig(this.unitsCost.swordsman))
                        this.maxUnits.get(UnitType.ARCHER).next(this.getSliderConfig(this.unitsCost.archer))
                        this.maxUnits.get(UnitType.CROSSBOWMAN).next(this.getSliderConfig(this.unitsCost.crossbowman))
                        return newLocal;
                    }
                )
            );
    }


    ngOnInit(): void { }

    private getMaxUnits(unitType: UnitType): Options {
        switch (unitType) {
            case UnitType.SPEARMAN:
                return this.getSliderConfig(this.unitsCost.spearman);
            case UnitType.SWORDSMAN:
                return this.getSliderConfig(this.unitsCost.swordsman);
            case UnitType.ARCHER:
                return this.getSliderConfig(this.unitsCost.archer);
            case UnitType.CROSSBOWMAN:
                return this.getSliderConfig(this.unitsCost.crossbowman);
        }
        return {
            floor: 0,
            ceil: 0
        };
    }

    private getSliderConfig(unitCost: ResourcesValue): Options {
        return {
            floor: 0,
            ceil: Math.min(...this.getUnitRequirementCost(unitCost))
        }
    }

    private getUnitRequirementCost(unitCost: ResourcesValue): number[] {
        return [
            this.floor(unitCost.denars === 0 ? Number.MAX_VALUE : (this.availableResources1.denars - this.usedResources.denars) / unitCost.denars),
            this.floor(unitCost.wood === 0 ? Number.MAX_VALUE : (this.availableResources1.wood - this.usedResources.wood) / unitCost.wood),
            this.floor(unitCost.stone === 0 ? Number.MAX_VALUE : (this.availableResources1.stone - this.usedResources.stone) / unitCost.stone),
            this.floor(unitCost.steel === 0 ? Number.MAX_VALUE : (this.availableResources1.steel - this.usedResources.steel) / unitCost.steel),
            this.floor(unitCost.food === 0 ? Number.MAX_VALUE : (this.availableResources1.food - this.usedResources.food) / unitCost.food),
            this.floor(unitCost.faith === 0 ? Number.MAX_VALUE : (this.availableResources1.faith - this.usedResources.faith) / unitCost.faith)
        ]
    }

    private floor(value: number): number {
        return Math.floor(value);
    }

    calculateChange(unitType: UnitType, resources: ResourcesValue): void {
        this.usedResources.denars += resources.denars;
        this.usedResources.wood += resources.wood;
        this.usedResources.stone += resources.stone;
        this.usedResources.steel += resources.steel;
        this.usedResources.food += resources.food;
        this.usedResources.faith += resources.faith;
        Array.from(this.maxUnits.entries())
            .filter(it => it[0] !== unitType)
            .forEach(it => it[1].next(this.getMaxUnits(it[0])))
    }
}
