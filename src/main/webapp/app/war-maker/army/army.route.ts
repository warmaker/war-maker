import { Route } from '@angular/router';

import { ArmyComponent } from './army/army.component';

const armyRoute: Route = {
    path: '',
    component: ArmyComponent,
    resolve: {},
    data: {
        pageTitle: 'warMaker.navbar.army'
    }
};

export const ARMY_ROUTES = [armyRoute];
