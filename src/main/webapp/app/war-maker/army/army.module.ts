import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArmyComponent } from './army/army.component';
import { ARMY_ROUTES } from './army.route';
import { RouterModule } from '@angular/router';
import { UnitPreviewComponent } from './army/unit-preview/unit-preview.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WarMakerSharedModule } from 'app/shared/shared.module';
import { Ng5SliderModule } from 'ng5-slider';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        WarMakerSharedModule,
        RouterModule.forChild(ARMY_ROUTES),
        NgbModule,
        Ng5SliderModule,
        SharedModule
    ],
    declarations: [
        ArmyComponent,
        UnitPreviewComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ArmyModule { }
