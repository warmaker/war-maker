import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ArmyDto, HumanUnitsListDto } from './army/army-dto';
import { createRequestOption } from 'app/shared/util/request-util';
import { IWarPartyEntity } from 'app/shared/model/war-party-entity.model';

@Injectable({
    providedIn: 'root'
})
export class ArmyService {
    private armyResourceUrl = SERVER_API_URL + 'api/army';
    private cityResourceUrl = SERVER_API_URL + 'api/city';

    constructor(private http: HttpClient) {}

    getCityArmy(cityId: number): Observable<HttpResponse<ArmyDto>> {
        return this.http.get<ArmyDto>(`${this.armyResourceUrl}/${cityId}`, { observe: 'response' });
    }

    recruit(units: HumanUnitsListDto, cityId: number): Observable<HttpResponse<any>> {
        return this.http.post(`${this.cityResourceUrl}/${cityId}/recruit`, units, { observe: 'response' });
    }

    createWarParty(armyId: number, units: HumanUnitsListDto): Observable<HttpResponse<IWarPartyEntity>> {
        const params = createRequestOption({armyId});
        return this.http.post<IWarPartyEntity>(`${this.armyResourceUrl}/create-war-party`, units, { observe: 'response', params });
    }
}
