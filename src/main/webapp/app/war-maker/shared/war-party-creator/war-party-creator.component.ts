import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { IWarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { ArmyService } from 'app/war-maker/army/army.service';
import { HumanUnitsListDto } from 'app/war-maker/army/army/army-dto';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';
import { WorldMapDataProviderService } from 'app/war-maker/world-map/map/world-map-data-provider.service';


@Component({
    selector: 'war-war-party-creator',
    templateUrl: './war-party-creator.component.html',
    styles: []
})
export class WarPartyCreatorComponent implements OnInit {
    public unitsList = HumanUnitsListDto.empty();

    constructor(
        private userDataService: UserDataService,
        private worldMapDataProviderService: WorldMapDataProviderService,
        private armyService: ArmyService
    ) {}

    ngOnInit(): void {
    }

    createWarParty(toolTip: NgbTooltip): void {
        this.armyService.createWarParty(this.userDataService.chosenCity!.info.armyId, this.unitsList).subscribe(res => {
            this.addWarParty(res);
            toolTip.open();
        });
    }

    private addWarParty(res: HttpResponse<IWarPartyEntity>): void {
        const newWarParty = res.body;
        if (newWarParty) {
            this.worldMapDataProviderService.refreshWarPartiesInfo();
        } else {
            throw new Error("Couldn't get new War Party from server.");
        }
    }
}
