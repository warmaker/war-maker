export class ResourcesImagePaths {
    constructor(
        public denars: string,
        public wood: string,
        public stone: string,
        public steel: string,
        public food: string,
        public faith: string
    ) {}
}

export const RESOURCES_IMAGE_PATHS: ResourcesImagePaths = new ResourcesImagePaths(
    './content/images/resources/two-coins.svg',
    './content/images/resources/wood-beam.svg',
    './content/images/resources/stone-block.svg',
    './content/images/resources/metal-bar.svg',
    './content/images/resources/wheat.svg',
    './content/images/resources/crucifix.svg'
);
