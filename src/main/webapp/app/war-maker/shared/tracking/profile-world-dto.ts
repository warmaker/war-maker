export class ProfileWorldDto {
    constructor(public profileId: number,
                public profileName: string) {}
}
