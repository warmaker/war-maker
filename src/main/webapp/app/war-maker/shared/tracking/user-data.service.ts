import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { AccountService } from 'app/core/auth/account.service';
import { MainPageService } from 'app/war-maker/main/main-page.service';
import { Observable, ReplaySubject } from 'rxjs';
import { filter, mergeMap, tap } from 'rxjs/operators';

import { CityViewService } from '../../main/city-view/city-view.service';
import { CityResponse } from '../../main/city-view/value/city-response';
import { ProfileWorldDto } from './profile-world-dto';

@Injectable({
    providedIn: 'root'
})
export class UserDataService {
    dataLoaded = new ReplaySubject<boolean>(1);
    dataChanged = new ReplaySubject<boolean>(1);
    dataLoadedValue = false;
    profileId: number;
    cityId: number;
    chosenCity: CityResponse | null = null;
    private resourceUrl = SERVER_API_URL + 'api/profiles';

    constructor(
        private accountService: AccountService,
        private http: HttpClient,
        private cityService: CityViewService,
        private mainPageService: MainPageService
    ) {
        this.accountService
            .getAuthenticationState()
            .pipe(filter(account => account !== null))
            .pipe(
                mergeMap(account => {
                    return this.http.get<ProfileWorldDto[]>(`${this.resourceUrl}/by-user-login/${account?.login}`, {
                        observe: 'response'
                    });
                })
            )
            .pipe(
                mergeMap((res: HttpResponse<ProfileWorldDto[]>) => {
                    const responseBody = res.body![0];
                    this.chooseProfile(responseBody.profileId);
                    return this.mainPageService.getCities(responseBody.profileId);
                })
            )
            .pipe(
                mergeMap(res => {
                    this.cityId = res[0];
                    return this.cityService.findCity(res[0]);
                })
            )
            .subscribe(res => {
                this.chosenCity = res;
                this.dataLoadedValue = true;
                this.dataLoaded.next(true);
                this.dataChanged.next(true);
            });
    }

    getLatestCity(): Observable<CityResponse> {
        return this.dataLoaded
            .pipe(
                mergeMap(() => {
                    return this.cityService.findCity(this.cityId);
                })
            )
            .pipe(tap(it => {
                this.chosenCity = it;
                this.dataChanged.next(true)
                return it;
            }));
    }

    chooseProfile(id: number): void {
        this.profileId = id;
    }

    getCurrentCity(): CityResponse | null {
        return this.chosenCity;
    }

    getCurrentProfile(): number {
        return this.profileId;
    }
}
