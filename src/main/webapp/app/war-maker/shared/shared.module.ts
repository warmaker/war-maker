import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SecondsFormatterPipe } from './pipes/seconds-formatter.pipe';
import { ZonedDateTimeFormatterPipe } from './pipes/zoned-date-time-formatter.pipe';
import { SecondsToHoursPipe } from './pipes/seconds-to-minutes.pipe';
import { WarPartyCreatorComponent } from './war-party-creator/war-party-creator.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
    exports: [
        FormsModule,
        NgbModule,
        SecondsFormatterPipe,
        ZonedDateTimeFormatterPipe,
        SecondsToHoursPipe,
        WarPartyCreatorComponent
    ],
    declarations: [SecondsFormatterPipe, ZonedDateTimeFormatterPipe, SecondsToHoursPipe, WarPartyCreatorComponent],
    imports: [
        FormsModule,
        CommonModule,
        NgbModule
    ]
})
export class SharedModule { }
