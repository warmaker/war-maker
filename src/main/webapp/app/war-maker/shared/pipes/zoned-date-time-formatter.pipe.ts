import { Pipe, PipeTransform } from '@angular/core';
import { WAR_MAKER_WORD_DATE_FORMAT } from 'app/war-maker/shared/constants/date-constants';
import * as moment from 'moment';
import { Moment } from 'moment';

@Pipe({
    name: 'zonedDateTimeFormatter'
})
export class ZonedDateTimeFormatterPipe implements PipeTransform {
    transform(zonedDateTime: string | Moment): string {
        return moment(zonedDateTime).format(WAR_MAKER_WORD_DATE_FORMAT);
    }
}
