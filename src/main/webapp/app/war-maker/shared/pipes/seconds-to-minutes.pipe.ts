import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'secondsToHours'
})
export class SecondsToHoursPipe implements PipeTransform {
    transform(value: number): number {
        return value * 60 * 60;
    }
}
