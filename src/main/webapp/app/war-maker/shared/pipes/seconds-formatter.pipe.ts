import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'secondsFormatter'
})
export class SecondsFormatterPipe implements PipeTransform {
    transform(seconds: number): string {
        return moment.utc(seconds * 1000).format('HH:mm:ss');
    }
}
