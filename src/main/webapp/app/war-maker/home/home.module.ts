import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WarMakerSharedModule } from '../../shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [WarMakerSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent],
})
export class WarMakerHomeModule {}
