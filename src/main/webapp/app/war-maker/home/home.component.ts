import { Component, OnDestroy, OnInit } from '@angular/core';
import { AccountService } from '../../core/auth/account.service';
import { LoginModalService } from '../../core/login/login-modal.service';
import { Account } from '../../core/user/account.model';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';

import { UserDataService } from '../shared/tracking/user-data.service';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
    account: Account | null;
    authSubscription?: Subscription;

    constructor(
        private accountService: AccountService,
        private loginModalService: LoginModalService,
        private router: Router,
        private userDataService: UserDataService
    ) { }

    ngOnInit(): void {
        this.authSubscription = this.accountService
            .getAuthenticationState()
            .pipe(filter(account => account !== null))
            .subscribe(account => {
                this.userDataService.dataLoaded.subscribe(() => {
                    this.router.navigate(['main']);
                });
                return (this.account = account);
            });
    }

    ngOnDestroy(): void {
        if (this.authSubscription) {
            this.authSubscription.unsubscribe();
        }
    }

    isAuthenticated(): boolean {
        return this.accountService.isAuthenticated();
    }

    login(): void {
        this.loginModalService.open();
    }
}
