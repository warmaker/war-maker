import { Route } from '@angular/router';
import { UniversityComponent } from './university/university.component';


const UNIVERSITY_ROUTE: Route = {
    path: '',
    component: UniversityComponent,
    resolve: {},
    data: {
        pageTitle: 'warMaker.navbar.university'
    }
};

export const UNIVERSITY_ROUTES = [UNIVERSITY_ROUTE];
