import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { WarMakerSharedModule } from 'app/shared/shared.module';
import { UNIVERSITY_ROUTES } from './university.route';
import { UniversityComponent } from './university/university.component';
import { UniversityService } from './university/university.service';


@NgModule({
    declarations: [UniversityComponent],
    imports: [
        CommonModule,
        WarMakerSharedModule,
        RouterModule.forChild(UNIVERSITY_ROUTES)
    ],
    providers: [UniversityService]
})
export class UniversityModule { }
