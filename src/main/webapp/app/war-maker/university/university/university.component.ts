import { Component, OnInit } from '@angular/core';
import { ResearchesEntity } from 'app/shared/model/researches-entity.model';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';
import { finalize } from 'rxjs/operators';
import { UniversityService } from './university.service';

@Component({
    selector: 'war-university',
    templateUrl: './university.component.html',
    styleUrls: ['./university.component.scss']
})
export class UniversityComponent implements OnInit {
    researches: ResearchesEntity;
    requestPending = false;

    constructor(private universityService: UniversityService,
                private userData: UserDataService) { }

    ngOnInit(): void {
        this.requestPending = true;
        this.universityService
            .getPlayerWorld(this.userData.getCurrentProfile())
            .pipe(finalize(() => this.requestPending = false))
            .subscribe(res => this.researches = res.body);
    }

    upgradeTechnology(): void {

    }

}
