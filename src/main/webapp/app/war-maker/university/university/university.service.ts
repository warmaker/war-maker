import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { ResearchesEntity } from 'app/shared/model/researches-entity.model';
import { Observable } from 'rxjs';

@Injectable()
export class UniversityService {
    private researchesResourceUrl = SERVER_API_URL + 'api/researches';

    constructor(private http: HttpClient) { }


    getPlayerWorld(playerId: number): Observable<HttpResponse<ResearchesEntity>> {
        const params = new HttpParams().append('playerId', playerId.toString());
        return this.http.get<ResearchesEntity>(this.researchesResourceUrl, { observe: 'response', params });
    }

}
