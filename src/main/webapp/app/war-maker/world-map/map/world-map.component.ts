import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';
import { WarDeclareService } from './war-declare/war-declare.service';
import { WorldMapDataProviderService } from './world-map-data-provider.service';

@Component({
    selector: 'war-world-map',
    templateUrl: './world-map.component.html',
    styles: [],
    providers: [WarDeclareService]
})
export class WorldMapComponent implements OnInit, OnDestroy {
    constructor(public userDataService: UserDataService,
                public worldMapDataProviderService: WorldMapDataProviderService) {}

    ngOnDestroy(): void {
        this.worldMapDataProviderService.ngOnDestroy();
    }

    ngOnInit(): void {
    }
}
