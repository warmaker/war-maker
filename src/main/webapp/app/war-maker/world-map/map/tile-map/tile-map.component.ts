import { Component, OnInit } from '@angular/core';
import { UserActionsService } from '../user-actions.service';

import { WorldMapDataProviderService } from '../world-map-data-provider.service';

@Component({
    selector: 'war-tile-map',
    templateUrl: './tile-map.component.html',
    styleUrls: ['./tile-map.scss']
})
export class TileMapComponent implements OnInit {
    public static readonly WAR_PARTY_INFO_MENU_ID = 'war-party-info-menu'; 
    public static readonly SCOUTED_FIELD_MENU_ID = 'scouted-field-menu'; 

    public readonly WAR_PARTY_INFO_MENU_COMPONENT_ID = TileMapComponent.WAR_PARTY_INFO_MENU_ID; 
    public readonly SCOUTED_FIELD_MENU_COMPONENT_ID = TileMapComponent.SCOUTED_FIELD_MENU_ID; 

    constructor(public worldMapDataProviderService: WorldMapDataProviderService,
                public userActions: UserActionsService) { }

    ngOnInit(): void {
        this.worldMapDataProviderService.createWorld();
    }

}
