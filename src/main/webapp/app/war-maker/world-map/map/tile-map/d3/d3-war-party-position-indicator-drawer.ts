import { Renderer2 } from '@angular/core';
import { WarPartyState } from 'app/shared/model/enumerations/war-party-state.model';
import * as d3 from 'd3';

import { UserActionsService } from '../../user-actions.service';
import { Coordinates } from '../../value/coordinates';
import { FieldWithWarParty } from '../../value/field-with-war-party';
import { DynamicWorldMapData } from '../../world-map-data-provider.service';
import { TileMapComponent } from '../tile-map.component';
import { D3HoverableIconsDrawer } from './d3-event-publisher';
import { D3ImageDrawer, IconsNames } from './d3-image-drawer';
import { EagerlyInitializedComponents } from './d3-map/eagerly-initialized-components';

export class D3WarPartyPositionIndicatorDrawer extends D3HoverableIconsDrawer {
    private blockWarPartyIndicator = false;
    private drawnIcons: d3.Selection<SVGPathElement, FieldWithWarParty, SVGSVGElement, unknown> = null;

    constructor(renderer: Renderer2,
                svg: d3.Selection<SVGSVGElement, unknown, HTMLElement, any>,
                userActions: UserActionsService,
                private dynamicWorldMapData: DynamicWorldMapData,
                private eagerlyInitializedComponents: EagerlyInitializedComponents) {
        super(renderer, svg, userActions);
        this.subscribeToWarPartiesChange();
    }

    private subscribeToWarPartiesChange(): void {
        this.dynamicWorldMapData.warPartiesOnMap.subscribe(warPartiesChange => {
            const fieldWithWarParties: FieldWithWarParty[] = [];
            warPartiesChange.forEach(wp => {
                const field = fieldWithWarParties.filter(f => f.field.coordinates.equals(Coordinates.of(wp.position)));
                if (field.length > 0) {
                    field[0].warParties.push(wp);
                } else {
                    fieldWithWarParties.push(
                        new FieldWithWarParty(this.eagerlyInitializedComponents.findD3HexFieldByCoordinates(Coordinates.of(wp.position)), [wp])
                    );
                }
            });
            this.drawCurrentPositionIndicators(fieldWithWarParties);
        });
    }

    private drawCurrentPositionIndicators(currentWarPartiesJourneys: FieldWithWarParty[]): void {
        const square = this.createIconSymbol();
        this.clearOldIcons();
        this.drawnIcons = this.svg
            .selectAll('div')
            .data(currentWarPartiesJourneys)
            .enter()
            .append('g')
            .append('path')
            .attr('d', square)
            .attr('stroke', 'black')
            .attr('class', 'pointer')
            .attr('stroke-width', 0)
            .style('fill', d => {
                switch (d.warParties[0].state) {
                    case WarPartyState.STATIONED:
                        return D3ImageDrawer.getOtherIcon(IconsNames.STATIONED);
                    case WarPartyState.MOVING:
                        return D3ImageDrawer.getOtherIcon(IconsNames.ATTACK);
                    default:
                        return D3ImageDrawer.getOtherIcon(IconsNames.STATIONED);
                }
            })
            .attr(
                'transform', d => this.getTransformation(d.field)
            )
            .on('click', () => {
                d3.event.preventDefault();
                d3.event.stopPropagation();
                this.blockWarPartyIndicator = !this.blockWarPartyIndicator;
                if (!this.blockWarPartyIndicator) {
                    this.renderer.setStyle(document.getElementById(TileMapComponent.WAR_PARTY_INFO_MENU_ID), 'display', 'none');
                }
            })
            .on('mouseover', (d: FieldWithWarParty, e: string | number, group) => {
                d3.event.preventDefault();
                d3.event.stopPropagation();
                this.squareHovered(d, group[e]);
            })
            .on('mouseout', () => {
                d3.event.preventDefault();
                d3.event.stopPropagation();
                this.triangleNotHovered();
            });
    }

    private clearOldIcons(): void {
        if (this.drawnIcons) {
            this.drawnIcons.remove();
        };
    }

    private triangleNotHovered(): void {
        if (!this.blockWarPartyIndicator) {
            this.renderer.setStyle(document.getElementById(TileMapComponent.WAR_PARTY_INFO_MENU_ID), 'display', 'none');
        }
    }

    private squareHovered(d: FieldWithWarParty, path: SVGPathElement): void {
        this.revealElement(TileMapComponent.WAR_PARTY_INFO_MENU_ID, path);
        this.userActions.warPartyIconHovered(d.warParties);
    }
}
