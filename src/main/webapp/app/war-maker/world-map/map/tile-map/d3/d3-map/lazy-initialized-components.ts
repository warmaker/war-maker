import { Renderer2 } from '@angular/core';
import * as d3 from 'd3';
import { UserActionsService } from '../../../user-actions.service';
import { DynamicWorldMapData } from '../../../world-map-data-provider.service';
import { D3ColorManager } from '../d3-color-manager';
import { D3ImageDrawer } from '../d3-image-drawer';
import { D3LineDrawer } from '../d3-line-drawer';
import { D3ScoutedWarPartiesDrawer } from '../d3-scouted-war-parties-drawer';
import { D3WarPartyPositionIndicatorDrawer } from '../d3-war-party-position-indicator-drawer';
import { EagerlyInitializedComponents } from './eagerly-initialized-components';


export class LazyInitializedComponents {
    private constructor(public d3ImageDrawer: D3ImageDrawer,
                        public d3LineDrawer: D3LineDrawer,
                        public d3ScoutedWarPartiesDrawer: D3ScoutedWarPartiesDrawer,
                        public d3WarPartyPositionDrawer: D3WarPartyPositionIndicatorDrawer,
                        public d3ColorManager: D3ColorManager) { }


    static init(svg: d3.Selection<SVGSVGElement, any, HTMLElement, any>,
                renderer: Renderer2,
                userActions: UserActionsService,
                eagerlyInitializedComponents: EagerlyInitializedComponents,
                dynamicWorldMapData: DynamicWorldMapData): LazyInitializedComponents {
        const d3ImageDrawer = new D3ImageDrawer(svg);
        const d3LineDrawer = new D3LineDrawer(svg,
            userActions,
            dynamicWorldMapData,
            eagerlyInitializedComponents);
        const d3ScoutedWarPartiesDrawer = new D3ScoutedWarPartiesDrawer(
            eagerlyInitializedComponents.worldData,
            renderer,
            svg,
            userActions
        );
        const d3WarPartyPositionDrawer = new D3WarPartyPositionIndicatorDrawer(
            renderer,
            svg,
            userActions,
            dynamicWorldMapData,
            eagerlyInitializedComponents
        );
        const d3ColorManager = new D3ColorManager(eagerlyInitializedComponents, userActions);
        d3ImageDrawer.createImagePatterns();
        d3ColorManager.colorSpecialPositions();
        d3ScoutedWarPartiesDrawer.drawScoutedWarPartiesIndicators();
        return new LazyInitializedComponents(d3ImageDrawer, d3LineDrawer, d3ScoutedWarPartiesDrawer, d3WarPartyPositionDrawer, d3ColorManager);
    }
}
