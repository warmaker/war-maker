import { CityResponse } from 'app/war-maker/main/city-view/value/city-response';
import { Coordinates } from '../../../value/coordinates';
import { D3HexField } from '../../../value/d3-hex-field';
import { WorldFieldView, WorldFieldViewDto } from '../../../value/world-field-view-dto';
import { PathFinder } from '../../path-finder/path-finder';

export class EagerlyInitializedComponents {
    private constructor(public worldData: D3HexField[][],
                        public pathFinder: PathFinder,
                        public currentPosition: Coordinates,
                        public scoutedRing: Coordinates[]) { }

    public static init(worldFieldData: WorldFieldViewDto[][], city: CityResponse): EagerlyInitializedComponents {
        const currentPosition = new Coordinates(city.coordinates.x, city.coordinates.y);
        const worldData = this.createD3HexFields(city, worldFieldData);
        const pathFinder = new PathFinder(worldData);
        const scoutedRing = pathFinder.cubeRing(currentPosition, 3);
        EagerlyInitializedComponents.findScoutedRingHexFields(worldData, scoutedRing);
        return new EagerlyInitializedComponents(worldData, pathFinder, currentPosition, scoutedRing);
    }

    private static findScoutedRingHexFields(worldData: D3HexField[][], scoutedRing: Coordinates[]): void {
        worldData.flat()
                 .forEach(it => it.belongsToScoutedRing = scoutedRing.find(scoutedRing1 => scoutedRing1.equals(it.coordinates)) != null);
    }

    private static createD3HexFields(city: CityResponse, res: WorldFieldViewDto[][]): D3HexField[][] {
        const currentPosition = Coordinates.from(city.coordinates);
        return res.map(row => row.map(field => {
                const coordinates = new Coordinates(field.xCoordinate, field.yCoordinate);
                const d3HexField = new D3HexField(coordinates, WorldFieldView.from(field), false, coordinates.equals(currentPosition)); // TODO fix scouting ring
                return d3HexField;
            })
        );
    }

    findD3HexFieldByCoordinates = (coordinats: Coordinates): D3HexField => {
        for (const rows of this.worldData) {
            if (rows[0].coordinates.x !== coordinats.x) {
                continue;
            } else {
                const foundElement = rows.find(elem => elem.coordinates.equals(coordinats));
                if (foundElement) {
                    return foundElement;
                } else {
                    // throw new Error('Field with given coordinates was not found in the row');
                    return null;
                }
            }
        }
        // throw new Error('Field with given coordinates was not found');
        return null;
    };


    getCurrentPosition(city: CityResponse): D3HexField {
        const coord = new Coordinates(city.coordinates.x, city.coordinates.y);
        return this.findD3HexFieldByCoordinates(coord);
    }
}
