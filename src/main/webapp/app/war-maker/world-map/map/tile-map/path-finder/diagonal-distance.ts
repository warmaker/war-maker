import { D3HexField } from '../../value/d3-hex-field';
import { Heurestic, DistanceCalculator } from './nodes-data';

export class DiagonalDistance implements Heurestic, DistanceCalculator {
    getDistance(start: D3HexField, goal: D3HexField): number {
        const x1 = start.coordinates.x;
        const y1 = start.coordinates.y;
        const x2 = goal.coordinates.x;
        const y2 = goal.coordinates.y;

        if (x1 === x2 || y1 === y2) {
            return 1;
        } else {
            return Math.sqrt(2);
        }
    }

    getHeurestic(start: D3HexField, goal: D3HexField): number {
        const D = 1;
        const D2 = 1;
        const dx = Math.abs(start.coordinates.x - goal.coordinates.x);
        const dy = Math.abs(start.coordinates.y - goal.coordinates.y);
        return D * (dx + dy) + (D2 - 2 * D) * Math.min(dx, dy);
    }
}
