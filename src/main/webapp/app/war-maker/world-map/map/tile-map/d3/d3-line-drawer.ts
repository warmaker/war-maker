import * as d3 from 'd3';
import { curveLinear, Line } from 'd3';
import { UserActionsService } from '../../user-actions.service';
import { Coordinates } from '../../value/coordinates';
import { D3HexField } from '../../value/d3-hex-field';
import { DynamicWorldMapData } from '../../world-map-data-provider.service';
import { D3Drawer } from './d3-drawer';
import { EagerlyInitializedComponents } from './d3-map/eagerly-initialized-components';


export class D3LineDrawer extends D3Drawer {
    public currentDrawnPath: d3.Selection<SVGPathElement, unknown, HTMLElement, any>;
    private readonly lineFunction: Line<D3HexField> = this.curveLine();

    pathToSelectedField: Coordinates[];

    constructor(svg: d3.Selection<SVGSVGElement, unknown, HTMLElement, any>,
                private userActions: UserActionsService,
                private dynamicWorldMapData: DynamicWorldMapData,
                private eagerlyInitializedComponents: EagerlyInitializedComponents) {
        super(svg);
        this.subscribeToJourneysChanges();
        this.subscribeToUserActions();
    }

    private subscribeToUserActions(): void {
        this.userActions.clickedFieldChanged().subscribe(field => {
            const selectedField = this.userActions.getSelectedField();
            const path = this.eagerlyInitializedComponents.pathFinder.getShortestPath(selectedField, field);
            this.pathToSelectedField = path.map(f => f.coordinates).reverse();
            this.eraseCurrentPath();
            this.drawAnimatedLinePath(path);
        });
    }

    private subscribeToJourneysChanges(): void {
        this.dynamicWorldMapData.warPartyJourneys.subscribe(warPartiesJournyesChange => {
            warPartiesJournyesChange.forEach(journey => {
                if (journey.movePath) {
                    this.drawStaticAnimatedLinePath(
                        journey.movePath.map(this.eagerlyInitializedComponents.findD3HexFieldByCoordinates),
                        '#a70000'
                    );
                }
            });
        });
    }

    public drawAnimatedLinePath(path: D3HexField[], color = '#d0d0d0'): d3.Selection<SVGPathElement, unknown, HTMLElement, any> {
        this.currentDrawnPath = this.createLine(path, color);
        this.animateLine();
        return this.currentDrawnPath;
    }

    public drawStaticAnimatedLinePath(path: D3HexField[],color = '#d0d0d0'): d3.Selection<SVGPathElement, unknown, HTMLElement, any> {
        const line = this.createLine(path, color);
        this.animateStaticLine(line);
        return line;
    }

    private createLine(path: D3HexField[], color: string): d3.Selection<SVGPathElement, unknown, HTMLElement, any> {
        const line = this.lineFunction(path);
        if (line) {
            return this.svg.select('g')
                .append('svg:a')
                .attr('href', '')
                .append('path')
                .attr('d', line)
                .attr('stroke', color)
                .style('stroke-dasharray', '10, 4')
                .attr('stroke-width', 4)
                .attr('fill', 'none');
        } else {
            throw new Error('Line function returned null');
        }
    }

    private curveLine(): Line<D3HexField> {
        return d3
            .line<D3HexField>()
            .x(d => (d.xPixel))
            .y(d => (d.yPixel))
            .curve(curveLinear);
    }

    private animateLine(): void {
        function animate(this: any): void {
            d3.select(this)
                .transition()
                .ease(d3.easeLinear)
                .duration(1000)
                .styleTween('stroke-dashoffset', function (): (t: number) => string {
                    return d3.interpolateString(0, 14);
                })
                .on('end', animate);
        }
        this.currentDrawnPath.each(animate);
    }

    private animateStaticLine(line: d3.Selection<SVGPathElement, unknown, HTMLElement, any>): void {
        function animate(this: any): void {
            d3.select(this)
                .transition()
                .ease(d3.easeLinear)
                .duration(1000)
                .styleTween('stroke-dashoffset', function (): (t: number) => string {
                    return d3.interpolateString(0, 14);
                })
                .on('end', animate);
        }
        line.each(animate);
    }

    public eraseCurrentPath(): boolean {
        if (this.currentDrawnPath) {
            this.currentDrawnPath.remove();
            return true;
        }
        return false;
    }
}
