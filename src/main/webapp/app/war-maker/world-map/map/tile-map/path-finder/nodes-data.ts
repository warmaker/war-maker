import { D3HexField } from '../../value/d3-hex-field';

export interface Heurestic {
    getHeurestic(start: D3HexField, goal: D3HexField): number;
}

export interface DistanceCalculator {
    getDistance(start: D3HexField, goal: D3HexField): number;
}

export class NodesData {
    private _level: D3HexField[][];
    private distanceCalculator: DistanceCalculator;
    private heuresticCalculator: Heurestic;

    constructor(level: D3HexField[][], distanceCalculator: DistanceCalculator, heuresticCalculator: Heurestic) {
        this._level = level;
        this.distanceCalculator = distanceCalculator;
        this.heuresticCalculator = heuresticCalculator;
    }

    getDistance(start: D3HexField, goal: D3HexField): number {
        return this.distanceCalculator.getDistance(start, goal);
    }

    getHeurestic(start: D3HexField, goal: D3HexField): number {
        return this.heuresticCalculator.getHeurestic(start, goal);
    }

    doubleWidthNeighbor(hex: D3HexField, direction: number): D3HexField | null {
        const doublewidthDirections = [
            [+2,  0], [+1, -1], [-1, -1], 
            [-2,  0], [-1, +1], [+1, +1], 
        ]
        const dir = doublewidthDirections[direction]
        return this.findByCoordinates(hex.coordinates.x  + dir[0], hex.coordinates.y  + dir[1]); 
    }

    findByCoordinates(x: number, y: number): D3HexField | null{
        const isYOdd = y % 2 === 1;
        if (isYOdd && (x - 1)/2 < 0 || y < 0 ) return null;
        if (!isYOdd && x/2 < 0 || y < 0 ) return null;
        if ( isYOdd ) {
            return this.level[y][(x-1)/2];
        } else {
            return this.level[y][x/2];
        }
    }

    getNeighbors(node: D3HexField): D3HexField[] {
        const neighbors: D3HexField[] = [];
        
        for (let index = 0; index < 6; index++) {
            const neighbour = this.doubleWidthNeighbor(node, index);
            if (neighbour) {
                neighbors.push(neighbour);
            }
        }

        return neighbors;
    }

    public get level(): D3HexField[][] {
        return this._level;
    }

    public flatLevel(): D3HexField[] {
        return this._level.reduce(function(prev, next): D3HexField[] {
            return prev.concat(next);
        });
    }
}
