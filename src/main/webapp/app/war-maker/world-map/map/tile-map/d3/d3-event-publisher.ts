import { D3Drawer } from "./d3-drawer";
import { UserActionsService } from '../../user-actions.service';
import {Symbol} from 'd3';
import * as d3 from "d3";
import { D3HexField } from "../../value/d3-hex-field";
import { Renderer2 } from "@angular/core";
import { D3WorldMap } from "./d3-map/d3-world-map";

export abstract class D3HoverableIconsDrawer extends D3Drawer {
    protected readonly squareSize = 3500;

    constructor(protected renderer: Renderer2, 
                protected svg: d3.Selection<SVGSVGElement, unknown, HTMLElement, any>,
                protected userActions: UserActionsService ) {
        super(svg);
    }

    protected createIconSymbol(): Symbol<any, any> {
        return d3
            .symbol()
            .type(d3.symbolSquare)
            .size(this.squareSize);
    }

    protected getTransformation(d: D3HexField): string {
        const cx = d.xPixel;
        const cy = d.yPixel - D3WorldMap.HEX_CONFIG.height / 2 - 25;
        return `translate(${cx},${cy})`;
    }

    protected revealElement(elementId: string, path: SVGPathElement): void {
        const coordinates = path.getBoundingClientRect();
        this.renderer.setStyle(document.getElementById(elementId), 'display', 'block');
        this.renderer.setStyle(document.getElementById(elementId), 'top', coordinates.top - 10 + 'px');
        this.renderer.setStyle(document.getElementById(elementId), 'left', coordinates.left - 350 + 'px');
    }

}