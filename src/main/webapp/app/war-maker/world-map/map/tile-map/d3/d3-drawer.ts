import { D3Class } from './d3-class';

export abstract class D3Drawer extends D3Class {

    constructor(protected svg: d3.Selection<SVGSVGElement, unknown, HTMLElement, any>) {
        super();
    }

}