import * as d3 from 'd3';
import { UserActionsService } from '../../user-actions.service';
import { D3HexField } from '../../value/d3-hex-field';
import { D3Class } from './d3-class';
import { EagerlyInitializedComponents } from './d3-map/eagerly-initialized-components';


export class D3ColorManager extends D3Class {
    static readonly defaultHexStrokeColor = '#28ae60';
    static readonly enemyStrokeColor = '#ff7961';
    static readonly defaultCurrentPositionStrokeColor = '#FFFF00';
    private static readonly defaultHoverStrokeColor = '#f2b759';
    private readonly scoutingRingHexStrokeColor = '#3d4361';
    private currentField: D3HexField;

    constructor(private eagerlyInitializedComponents: EagerlyInitializedComponents,
        private userActions: UserActionsService) {
        super();
    }

    static resetStrokeColor(): d3.ValueFn<d3.BaseType, D3HexField, void> {
        return function (d): void {
            d3.select(this)
                .selectAll('path')
                .style('stroke', d.hexColor);
        };
    }

    static changeStrokeColor(): d3.ValueFn<d3.BaseType, D3HexField, void> {
        return function (): void {
            d3.select(this)
                .selectAll('path')
                .style('stroke', D3ColorManager.defaultHoverStrokeColor);
        };
    }

    colorSpecialPositions(): void {
        this.colorCurrentPosition();
        this.raiseEnemyFields();
        this.createScoutingRing();
    }

    private colorCurrentPosition(): void {
        this.userActions.selectedFieldChanged()
            .subscribe(field => {
                if (this.currentField) {
                    this.colorHex(this.currentField, this.currentField.hexColor);
                }
                this.currentField = field;
                this.colorHex(field);
            });
    }

    private raiseEnemyFields(): void {
        this.eagerlyInitializedComponents.worldData.flat()
            .filter(field => field.fieldData.isEnemy())
            .forEach(enemyField => {
                d3.select(enemyField.svgElement).raise();
            });
    }

    private createScoutingRing(): void {
        this.eagerlyInitializedComponents.scoutedRing.map(it => this.eagerlyInitializedComponents.findD3HexFieldByCoordinates(it))
            .filter(it => it)
            .forEach(it => this.colorHex(it, this.scoutingRingHexStrokeColor));
    }

    colorHex(field: D3HexField, strokeColor = D3ColorManager.defaultCurrentPositionStrokeColor): void {
        d3.select(field.svgElement)
            .selectAll('path')
            .style('stroke', () => strokeColor);
    }
}
