import { FibonacciHeap } from '@tyriar/fibonacci-heap';
import * as Collections from 'typescript-collections';
import { Coordinates, Hex } from '../../value/coordinates';

import { D3HexField } from '../../value/d3-hex-field';
import { DiagonalDistance } from './diagonal-distance';
import { NodesData } from './nodes-data';

export class PathFinder {
    private nodesData: NodesData;

    constructor(level: D3HexField[][]) {
        const diagonalCalculator = new DiagonalDistance();
        this.nodesData = new NodesData(level, diagonalCalculator, diagonalCalculator);
    }

    public getShortestPath(start: D3HexField, goal: D3HexField): D3HexField[] {
        const openSet = new FibonacciHeap<number, D3HexField>();
        const cameFrom = new Collections.Dictionary<D3HexField, D3HexField>();
        const gScore = new Collections.Dictionary<D3HexField, number>();
        openSet.insert(0, start);
        gScore.setValue(start, 0);
        while (!openSet.isEmpty()) {
            const minimum = openSet.extractMinimum();
            if (minimum && minimum.value) {
                const current = minimum.value;
                if (current.coordinates.x === goal.coordinates.x && current.coordinates.y === goal.coordinates.y) {
                    return this.reconstructPath(cameFrom, current);
                }
                this.nodesData.getNeighbors(current).forEach(neighbor => {
                    let gScoreValue = gScore.getValue(current);
                    let gScoreNeighborValue = gScore.getValue(neighbor);
                    if (!gScoreNeighborValue) {
                        gScoreNeighborValue = 0;
                    }
                    if (!gScoreValue) {
                        gScoreValue = 0;
                    }
                    const tentativeGScore =
                        gScoreValue + neighbor.fieldData.modifier * this.nodesData.getDistance(current, neighbor);
                    if (!gScore.containsKey(neighbor) || tentativeGScore < gScoreNeighborValue) {
                        gScore.setValue(neighbor, tentativeGScore);
                        const priority = tentativeGScore + this.nodesData.getHeurestic(neighbor, goal);
                        openSet.insert(priority, neighbor);
                        cameFrom.setValue(neighbor, current);
                    }
                });
            } else {
                throw new Error('Path finder failure');
            }
        }
        return [];
    }

    private reconstructPath(cameFrom: Collections.Dictionary<D3HexField, D3HexField>,
                            current: D3HexField): D3HexField[] {
        const totalPath = [current];
        while (cameFrom.getValue(current)) {
            const gScoreValue = cameFrom.getValue(current);
            if (gScoreValue) {
                current = gScoreValue;
                totalPath.push(current);
            }
        }
        return totalPath;
    }

    public cubeRing(center: Coordinates, radius = 1): Coordinates[] {
        center = new Coordinates(center.x, center.y / 2);
        const results: Hex[] = [];
        let cube = Coordinates.qoffsetToCube(-1, center).add(Hex.direction(4).scale(radius));
        for(let i = 0; i < 6; i++ ){
            for (let k = 0; k < radius; k++) {
                results.push(cube);
                cube = cube.neighbor(i);
            }
        }
        return results.map(it => Coordinates.qoffsetFromCube(-1, it)).map(it => new Coordinates(it.x * 2, it.y));
    }
}
