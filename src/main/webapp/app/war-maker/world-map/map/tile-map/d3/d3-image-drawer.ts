import { FieldType } from 'app/shared/model/field.model';

import { D3HexField } from '../../value/d3-hex-field';
import { D3Drawer } from './d3-drawer';

enum FieldsImagePaths {
    CITY = '../../../../../content/images/PNG/Tiles/Medieval/medieval_smallCastle.png',
    LAIR = '../../../../../content/images/PNG/Tiles/Medieval/medieval_cabin.png',
    POINT_OF_INTEREST = '../../../../../content/images/PNG/Tiles/Medieval/medieval_archway.png',
    LAND = '../../../../../content/images/PNG/Tiles/Terrain/Grass/grass_05.png',
    LAND_1 = '../../../../../content/images/PNG/Tiles/Terrain/Grass/grass_10.png',
    LAND_2 = '../../../../../content/images/PNG/Tiles/Terrain/Grass/grass_11.png',
    LAND_3 = '../../../../../content/images/PNG/Tiles/Terrain/Grass/grass_12.png',
    LAND_4 = '../../../../../content/images/PNG/Tiles/Terrain/Grass/grass_13.png'
}

enum IconsImagePaths {
    ATTACK = '../../../../../../content/images/world-map/axe-sword.svg',
    STATIONED = '../../../../../../content/images/world-map/condor-emblem.svg',
    SCOUTED_FIELD = '../../../../../../content/images/world-map/spyglass.svg',
}

export enum IconsNames {
    ATTACK = 'ATTACK',
    STATIONED = 'STATIONED',
    SCOUTED_FIELD = 'SCOUTED_FIELD'
}

export class D3ImageDrawer extends D3Drawer {
    private readonly imageHeight = '140px';
    private readonly imageWidth = '120px';

    constructor(svg: d3.Selection<SVGSVGElement, unknown, HTMLElement, any>) {
        super(svg);
    }

    static matchImageWithField(type: FieldType): string {
        switch (type) {
            case FieldType.CITY:
                return FieldsImagePaths.CITY;
            case FieldType.LAND:
                return FieldsImagePaths.LAND;
            case FieldType.LAIR:
                return FieldsImagePaths.LAIR;
            case FieldType.POINT_OF_INTEREST:
                return FieldsImagePaths.POINT_OF_INTEREST;
        }
    }

    static getOtherIcon(d: string): string {
        return 'url(#' + d + ')';
    }

    private static getLandImageId(numb: number): string {
        return FieldType.LAND + '_' + numb;
    }

    static getFieldImageId(d: D3HexField): string {
        if (d.fieldData.fieldType !== FieldType.LAND) {
            return 'url(#' + d.fieldData.fieldType + ')';
        } else {
            return 'url(#' + this.getLandImageId(0) + ')';
        }
    }
    
    createImagePatterns(): void {
        this.drawFieldPatterns();
        this.drawIconPatterns();
    }

    private drawFieldPatterns(): void {
        const data = [
            { image: D3ImageDrawer.matchImageWithField(FieldType.CITY), type: FieldType.CITY },
            { image: D3ImageDrawer.matchImageWithField(FieldType.LAIR), type: FieldType.LAIR },
            {
                image: D3ImageDrawer.matchImageWithField(FieldType.POINT_OF_INTEREST),
                type: FieldType.POINT_OF_INTEREST
            },
            { image: FieldsImagePaths.LAND_1, type: D3ImageDrawer.getLandImageId(0) },
            { image: FieldsImagePaths.LAND_2, type: D3ImageDrawer.getLandImageId(1) },
            { image: FieldsImagePaths.LAND_3, type: D3ImageDrawer.getLandImageId(2) },
            { image: FieldsImagePaths.LAND_4, type: D3ImageDrawer.getLandImageId(3) }
        ];
        this.createGrayscaleFilter();
        this.svg
            .selectAll('.image')
            .data(data)
            .enter()
            .append('pattern')
            .attr('id', d => d.type)
            .attr('class', 'svg-image')
            .attr('x', '0')
            .attr('y', '0')
            .attr('height', '1')
            .attr('width', '1')
            .append('image')
            .attr('height', this.imageHeight)
            .attr('width', this.imageWidth)
            .attr('xlink:href', d => d.image);

    }

    private createGrayscaleFilter(): void {
        this.svg
            .selectAll('.image')
            .data(['grayscale-filter'])
            .enter()
            .append('filter')
            .attr('id', d => d)
            .append('feColorMatrix')
            .attr('type', 'saturate')
            .attr('values', 0.1 + '');
    }

    private drawIconPatterns(): void {
        const iconData = [
            { image: IconsImagePaths.ATTACK, type: IconsNames.ATTACK },
            { image: IconsImagePaths.STATIONED, type: IconsNames.STATIONED },
            { image: IconsImagePaths.SCOUTED_FIELD, type: IconsNames.SCOUTED_FIELD },
            ];
        const image1 = this.svg.selectAll('.image1').data(iconData);
        image1
            .enter()
            .append('pattern')
            .attr('id', d => d.type)
            .attr('class', 'svg-image')
            .attr('height', '1')
            .attr('width', '1')
            .append('image')
            .attr('x', '6')
            .attr('y', '5')
            .attr('height', '50px')
            .attr('width', '50px')
            .attr('xlink:href', d => d.image);
    }

}
