import { D3HexField } from '../../value/d3-hex-field';

export class D3DebugTools {
    constructor(
        private svg: d3.Selection<SVGSVGElement, unknown, HTMLElement, any>,
        private hexGroup: d3.Selection<SVGGElement, D3HexField, null, undefined>,
        private worldData: D3HexField[]
    ) {}

    scaleHexesToFitBox(): void {
        const transitionDuration = 400;
        const svgNode = this.svg.node();
        const hexGroupNode = this.hexGroup.node();
        if (svgNode && hexGroupNode) {
            const svgBox = svgNode.getBoundingClientRect();
            const hexBox = hexGroupNode.getBBox();
            const scale = svgBox.height / hexBox.height;
            const centerX = svgBox.width / 2 - (hexBox.width * scale) / 2;
            this.hexGroup
                .attr('transform', `matrix(0, 0, 0, 0, ${centerX}, 0)`)
                .transition()
                .duration(transitionDuration)
                .attr('transform', `matrix(${scale}, 0, 0, ${scale}, ${centerX}, 0)`);
        }
    }

    showCoordinates(): void {
        this.hexGroup
            .selectAll('text')
            .data(this.worldData)
            .enter()
            .append('text')
            .attr('x', d => d.xPixel)
            .attr('y', d => d.yPixel)
            .text(d => d.coordinates.x + ' ' + d.coordinates.y)
            .attr('font-size', '30px')
            .attr('fill', 'white');
    }
}
