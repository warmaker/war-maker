import { Renderer2 } from '@angular/core';
import * as d3 from 'd3';

import { UserActionsService } from '../../user-actions.service';
import { D3HexField } from '../../value/d3-hex-field';
import { TileMapComponent } from '../tile-map.component';
import { D3HoverableIconsDrawer } from './d3-event-publisher';
import { D3ImageDrawer, IconsNames } from './d3-image-drawer';

export class D3ScoutedWarPartiesDrawer extends D3HoverableIconsDrawer {
    constructor(private worldData: D3HexField[][],
                renderer: Renderer2,
                svg: d3.Selection<SVGSVGElement, unknown, HTMLElement, any>,
                userActions: UserActionsService) {
        super(renderer, svg, userActions);
    }

    public drawScoutedWarPartiesIndicators(): void {
        const scoutedWarParties = this.findScoutedWarParties();
        const square = this.createIconSymbol();
        this.svg
            .selectAll('div')
            .data(scoutedWarParties)
            .enter()
            .append('g')
            .append('path')
            .attr('d', square)
            .attr('stroke', 'black')
            .attr('class', 'pointer')
            .attr('stroke-width', 0)
            .style('fill', () => D3ImageDrawer.getOtherIcon(IconsNames.SCOUTED_FIELD))
            .attr(
                'transform', d => this.getTransformation(d)
            )
            .on('click', () => {
                d3.event.preventDefault();
                d3.event.stopPropagation();
                this.renderer.setStyle(document.getElementById(TileMapComponent.SCOUTED_FIELD_MENU_ID), 'display', 'none');
            })
            .on('mouseover', (d: D3HexField, e: string | number, group) => {
                d3.event.preventDefault();
                d3.event.stopPropagation();
                this.squareHovered(d, group[e]);
            })
            .on('mouseout', () => {
                d3.event.preventDefault();
                d3.event.stopPropagation();
            });
    }


    private findScoutedWarParties(): D3HexField[] {
        return (([] as D3HexField[]).concat.apply([], this.worldData) as D3HexField[])
            .filter(it => it.fieldData.scoutedWarParties && !it.isCurrentPosition);
    }

    private squareHovered(d: D3HexField, path: SVGPathElement): void {
        this.revealElement(TileMapComponent.SCOUTED_FIELD_MENU_ID, path);
        this.userActions.scoutedFieldIconHovered(d.fieldData.scoutedWarParties);
    }
}
