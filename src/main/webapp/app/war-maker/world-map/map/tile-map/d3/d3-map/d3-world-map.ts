import { Renderer2 } from '@angular/core';
import { CityResponse } from 'app/war-maker/main/city-view/value/city-response';
import * as d3 from 'd3';
import { UserActionsService } from '../../../user-actions.service';
import { D3HexField } from '../../../value/d3-hex-field';
import { WorldFieldViewDto } from '../../../value/world-field-view-dto';
import { DynamicWorldMapData } from '../../../world-map-data-provider.service';
import { D3ColorManager } from '../d3-color-manager';
import { D3ImageDrawer } from '../d3-image-drawer';
import { EagerlyInitializedComponents } from './eagerly-initialized-components';
import { LazyInitializedComponents } from './lazy-initialized-components';
import { D3DebugTools } from '../debug-tools';


class D3Fields {
    constructor(
        public svg: d3.Selection<SVGSVGElement, any, HTMLElement, any>,
        public hexGroup: d3.Selection<SVGGElement, D3HexField, null, undefined>
    ) { }
}

class HexConfig {
    constructor(
        public readonly h = Math.sqrt(3) / 3,
        public size = 70,
        public width = Math.sqrt(3) * size,
        public height = 2 * 70
    ) { }
}

class Point {
    constructor(public x: number,
        public y: number) { }

    spread(): [number, number] {
        return [this.x, this.y];
    }
}

export class D3WorldMap {
    /**
     * Y coordinate is doubled to make algorithms easier, take a look at "Doubled coordinates" https://www.redblobgames.com/grids/hexagons/#coordinates
     */
    public static readonly DOUBLED_X_COORDINATE = 2;
    public static readonly HEX_CONFIG = new HexConfig();
    private static readonly HEX_PATH = D3WorldMap.buildHexPath();
    private static readonly MAP_WIDTH = 1280;
    private static readonly MAP_HEIGHT = 780;
    private readonly grayFilter = 'filter: grayscale(100%); filter: url("#grayscale-filter");';

    d3Fields: D3Fields;
    lazyInitializedComponents: LazyInitializedComponents;

    protected constructor(
        private eagerlyInitializedComponents: EagerlyInitializedComponents,
        private userActions: UserActionsService
    ) { }

    static create(
        renderer: Renderer2,
        worldFieldData: WorldFieldViewDto[][],
        dynamicWorldMapData: DynamicWorldMapData,
        userActions: UserActionsService,
        city: CityResponse): D3WorldMap {

        const eagerlyInitializedComponents = EagerlyInitializedComponents.init(worldFieldData, city);
        const worldMap = new D3WorldMap(eagerlyInitializedComponents, userActions);
        worldMap.d3Fields = worldMap.createSvgArea();
        worldMap.createHexMap();

        userActions.initialize(eagerlyInitializedComponents.getCurrentPosition(city));

        worldMap.lazyInitializedComponents = LazyInitializedComponents.init(worldMap.d3Fields.svg, renderer, userActions, eagerlyInitializedComponents, dynamicWorldMapData);
        return worldMap;
    }

    private static pointyHexCorner(center: Point, i: number): Point {
        const size = D3WorldMap.HEX_CONFIG.size;
        const angleDeg = 60 * i - 30;
        const angleRad = Math.PI / 180 * angleDeg;
        return new Point(center.x + size * Math.cos(angleRad),
            center.y + size * Math.sin(angleRad))
    }

    private static pointyHexToPixel(coordinates: Point): Point {
        const x = (D3WorldMap.HEX_CONFIG.size + 5) * Math.sqrt(3) / 2 * coordinates.x;
        const y = (D3WorldMap.HEX_CONFIG.size + 5) * 3 / 2 * coordinates.y;
        return new Point(x, y);
    }

    private static buildHexPath(x = 0, y = 0): string {
        const newLocal = d3
            .line()
            .x(d => d[0])
            .y(d => d[1])
            .curve(d3.curveCardinalClosed.tension(0.9))([
                D3WorldMap.pointyHexCorner(new Point(x, y), 0).spread(),
                D3WorldMap.pointyHexCorner(new Point(x, y), 1).spread(),
                D3WorldMap.pointyHexCorner(new Point(x, y), 2).spread(),
                D3WorldMap.pointyHexCorner(new Point(x, y), 3).spread(),
                D3WorldMap.pointyHexCorner(new Point(x, y), 4).spread(),
                D3WorldMap.pointyHexCorner(new Point(x, y), 5).spread(),
            ]);
        if (newLocal) {
            return newLocal;
        } else {
            throw new Error('D3 line returned null');
        }
    }

    private createSvgArea(): D3Fields {
        this.clearSVGData();
        const svg = d3
            .select<SVGSVGElement, null>('#worldMap')
            .attr('width', D3WorldMap.MAP_WIDTH)
            .attr('height', D3WorldMap.MAP_HEIGHT);
        const hexGroup = d3.select<SVGGElement, D3HexField>(svg.node()!)
            .append('g');
        const zoom = d3
            .zoom<SVGSVGElement, any>()
            .scaleExtent([1, 8])
            .on('zoom', () => this.zoomed());
        svg.call(zoom);
        return new D3Fields(svg, hexGroup);
    }

    private createHexMap(): void {
        const worldData = this.eagerlyInitializedComponents.worldData.flat();
        const that = this;
        const svgAHex = this.d3Fields.hexGroup
            .selectAll<SVGPathElement, D3HexField>('.hex')
            .data(worldData)
            .enter()
            .append<SVGAElement>('svg:a')
            .attr('class', function (d): string {
                d.svgElement = this;
                return 'hex';
            })
            .attr('href', '')
            .on('click', d => {
                d3.event.preventDefault();
                d3.event.stopPropagation();
                this.hexClicked(d);
            })
            .on('mouseover', D3ColorManager.changeStrokeColor())
            .on('mouseout', D3ColorManager.resetStrokeColor());
        svgAHex
            .append('path')
            .attr('d', () => D3WorldMap.HEX_PATH)
            .attr('id', d => d.fieldData.fieldType + d.coordinates.x + d.coordinates.y)
            .style('stroke', d => d.hexColor)
            .attr('stroke-width', 10)
            .attr('transform', d => this.transformElement(d));
        svgAHex
            .append<SVGAElement>('path')
            .attr('d', () => D3WorldMap.HEX_PATH)
            .attr('id', d => d.fieldData.fieldType + d.coordinates.x + d.coordinates.y + '_image')
            .style('fill', function (d): string {
                if (!d.fieldData.isScouted) {
                    this.setAttribute("style", that.grayFilter)
                }
                return D3ImageDrawer.getFieldImageId(d);
            })
            .attr('transform', d => this.transformElement(d));
    }

    private transformElement(d: D3HexField): string {
        const coordinates = D3WorldMap.pointyHexToPixel(new Point(d.coordinates.x, d.coordinates.y))
        d.xPixel = coordinates.x;
        d.yPixel = coordinates.y;
        return `translate(${coordinates.x}, ${coordinates.y})`;
    }

    clearSVGData(): void {
        d3.select('#worldMap')
            .html("");
    }

    zoomed(): void {
        this.d3Fields.svg.selectAll('g').attr('transform', d3.event.transform);
        this.userActions.zoomedMap();
    }

    private enableDebug(worldMap: D3WorldMap): void {
        const debugTools = new D3DebugTools(worldMap.d3Fields.svg, worldMap.d3Fields.hexGroup, worldMap.eagerlyInitializedComponents.worldData.flatMap(it => it));
        debugTools.showCoordinates();
    }

    private hexClicked(field: D3HexField): void {
        this.userActions.fieldClickedOn(field);
    }

}


