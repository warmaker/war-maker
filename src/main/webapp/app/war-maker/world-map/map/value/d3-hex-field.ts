import { makeString } from 'typescript-collections/dist/lib/util';
import { D3ColorManager } from '../tile-map/d3/d3-color-manager';
import { D3ImageDrawer } from '../tile-map/d3/d3-image-drawer';
import { Coordinates } from './coordinates';
import { WorldFieldView } from './world-field-view-dto';


export class D3HexField {
    public image: string;
    public svgElement: SVGAElement;
    public xPixel = 0;
    public yPixel = 0;
    public hexColor: string;

    constructor(public coordinates: Coordinates,
                public fieldData: WorldFieldView,
                public belongsToScoutedRing = false,
                public isCurrentPosition = false) {
        this.image = D3ImageDrawer.matchImageWithField(this.fieldData.fieldType);
        this.hexColor = this.findColor();
    }

    toString(): string {
        return makeString(this);
    }

    private findColor(): string {
        if (this.fieldData.isEnemy()) {
            return D3ColorManager.enemyStrokeColor;
        }
        if (this.isCurrentPosition) {
            return D3ColorManager.defaultCurrentPositionStrokeColor;
        }
        return D3ColorManager.defaultHexStrokeColor;
    }

    unsetAsCurrentPosition(): void {
        this.isCurrentPosition = false;
        this.hexColor = D3ColorManager.defaultHexStrokeColor;
    }

    setAsCurrentPosition(): void {
        this.isCurrentPosition = true;
        this.hexColor = D3ColorManager.defaultCurrentPositionStrokeColor;
    }

}
