import { FieldType } from 'app/shared/model/field.model';
import { RelationType } from 'app/shared/model/enumerations/relation-type.model';

export interface WorldFieldViewDto {
    xCoordinate: number,
    yCoordinate: number,
    modifier: number,
    fieldType: FieldType,
    relation: RelationType,
    isScouted: boolean,
    ownerId?: number,
    canDeclareWar?: boolean,
    canAttack?: boolean,
    scoutedWarParties?: number[]
}

export class WorldFieldView implements WorldFieldViewDto {
    constructor(
        public xCoordinate: number,
        public yCoordinate: number,
        public modifier: number,
        public fieldType: FieldType,
        public relation: RelationType,
        public isScouted: boolean,
        public ownerId?: number,
        public canDeclareWar?: boolean,
        public canAttack?: boolean,
        public scoutedWarParties?: number[]
    ) {}

    static from(it: WorldFieldViewDto): WorldFieldView {
        return new WorldFieldView(
            it.xCoordinate,
            it.yCoordinate,
            it.modifier,
            it.fieldType,
            it.relation,
            it.isScouted,
            it.ownerId,
            it.canDeclareWar,
            it.canAttack,
            it.scoutedWarParties
        )
    }

    isEnemy(): boolean {
        return this.relation === RelationType.ENEMY;
    }
}
