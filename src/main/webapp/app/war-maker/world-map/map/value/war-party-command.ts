export enum WarPartyCommand {
    GARRISON = 'GARRISON',
    ATTACK = 'ATTACK',
    MOVE = 'MOVE'
}
