import { IWarPartyEntity } from 'app/shared/model/war-party-entity.model';

import { D3HexField } from './d3-hex-field';

export class FieldWithWarParty {
    constructor(public field: D3HexField, public warParties: IWarPartyEntity[]) {}
}
