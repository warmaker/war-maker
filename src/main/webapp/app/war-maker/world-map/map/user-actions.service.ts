import { Injectable } from '@angular/core';
import { IWarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { D3HexField } from './value/d3-hex-field';

@Injectable()
export class UserActionsService {
    private selectedField: BehaviorSubject<D3HexField>;
    private fieldClicked: BehaviorSubject<D3HexField>;
    public zoomed: Subject<boolean> = new Subject();
    public warPartyHovered = new Subject<IWarPartyEntity[]>();
    public scoutedFieldHovered = new Subject<number[]>();

    initialize(startingPosition: D3HexField): void {
        this.selectedField = new BehaviorSubject(startingPosition);
        this.selectedField.next(startingPosition);
        this.fieldClicked = new BehaviorSubject(startingPosition);
    }

    updateSelectedField(): void {
        this.selectedField.value.unsetAsCurrentPosition()
        this.fieldClicked.value.setAsCurrentPosition();
        this.selectedField.next(this.fieldClicked.value);
    }

    getSelectedField(): D3HexField {
        return this.selectedField.value;
    }

    getClickedField(): D3HexField {
        return this.fieldClicked.value;
    }

    selectedFieldChanged(): Observable<D3HexField> {
        return this.selectedField;
    }

    clickedFieldChanged(): Observable<D3HexField> {
        return this.fieldClicked;
    }

    zoomedMap(): void {
        this.zoomed.next(true);
    }

    warPartyIconHovered(warParty: IWarPartyEntity[]): void {
        this.warPartyHovered.next(warParty);
    }

    scoutedFieldIconHovered(ids: number[]): void {
        this.scoutedFieldHovered.next(ids);
    }

    fieldClickedOn(field: D3HexField): void {
        this.fieldClicked.next(field);
    }

}