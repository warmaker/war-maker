import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import * as d3 from 'd3';
import { UserActionsService } from '../user-actions.service';

import { D3HexField } from '../value/d3-hex-field';

@Injectable()
export class WorldMapActionMenuService {
    private renderer: Renderer2;

    constructor(private userActions: UserActionsService, 
                rendererFactory: RendererFactory2) {
        this.renderer = rendererFactory.createRenderer(null, null);
        this.userActions.clickedFieldChanged().subscribe(field => this.fieldClicked(field));
        this.userActions.zoomed.subscribe(() => this.transformFieldMenu());
    }

    private fieldClicked(field: D3HexField): void {
        if (document.getElementById('menu-container')) {
            this.renderer.setStyle(document.getElementById('menu-container'), 'display', 'none');
            this.renderer.setStyle(document.getElementById('menu-container'), 'top', field.yPixel + 10 + 'px');
            this.renderer.setStyle(document.getElementById('menu-container'), 'left', field.xPixel + 10 + 'px');
            this.renderer.setStyle(document.getElementById('menu-container'), 'display', 'block');
        }
    }

    private transformFieldMenu(): void {
        const startTranslateState = `translate(${d3.event.transform.x}px,${d3.event.transform.y}px)`;
        const translateInterpolator = d3.interpolateString(startTranslateState, startTranslateState);
        d3.select('#menu-container')
            .transition()
            .styleTween('transform', () => translateInterpolator);
    }

}