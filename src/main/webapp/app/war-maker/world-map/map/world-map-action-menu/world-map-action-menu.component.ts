import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService } from 'ng-jhipster';
import { from } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { UserActionsService } from '../user-actions.service';
import { WarPartyCommand } from '../value/war-party-command';
import { WarDeclareService } from '../war-declare/war-declare.service';
import { WarPartyPickerComponent } from '../war-party-picker/war-party-picker.component';
import { WorldMapDataProviderService } from '../world-map-data-provider.service';
import { WorldMapActionMenuService } from './world-map-action-menu.service';

@Component({
    selector: 'war-world-map-action-menu',
    templateUrl: './world-map-action-menu.component.html',
    styleUrls: ['./world-map-action-menu.component.scss', '../tile-map/tile-map.scss'],
    providers: [WorldMapActionMenuService]
})
export class WorldMapActionMenuComponent implements OnInit {

    constructor(
        public worldMapDataProviderService: WorldMapDataProviderService,
        public userActions: UserActionsService,
        private warDeclareService: WarDeclareService,
        private alertService: JhiAlertService,
        private modalService: NgbModal,
        private worldMapActionMenuService: WorldMapActionMenuService) { }

    ngOnInit(): void {
    }

    declareWar(): void {
        const ownerId = this.userActions.getClickedField().fieldData.ownerId;
        if (ownerId) {
            this.warDeclareService
                .declareWar(ownerId)
                .pipe(concatMap(() => this.worldMapDataProviderService.reloadPlayerWorld()))
                .subscribe(() => {
                    return this.alertService.success('War declared');
                });
        } else {
            throw new Error('Not yet implemented');
        }
    }

    select(): void {
        this.worldMapDataProviderService.selectField();
    }

    attack(): void {
        this.openWarPartyPickedCommand(WarPartyCommand.ATTACK);
    }

    garrison(): void {
        this.openWarPartyPickedCommand(WarPartyCommand.GARRISON);
    }

    private openWarPartyPickedCommand(command: WarPartyCommand): void {
        const ref = this.modalService.open(WarPartyPickerComponent);
        ref.componentInstance.currentPath = this.worldMapDataProviderService.d3WorldMap.lazyInitializedComponents.d3LineDrawer.pathToSelectedField;
        ref.componentInstance.command = command;
        from(ref.result).subscribe(
            () => this.worldMapDataProviderService.refreshWarPartiesInfo(),
            () => { }
        );
    }
}
