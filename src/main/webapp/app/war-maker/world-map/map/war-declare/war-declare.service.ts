import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';
import { Observable } from 'rxjs';

@Injectable()
export class WarDeclareService {
    public resourceUrl = SERVER_API_URL + 'api/player/';

    constructor(private userDataService: UserDataService, private http: HttpClient) {}

    declareWar(defenderId: number): Observable<HttpResponse<any>> {
        const requestDto = { attackerId: this.userDataService.profileId, defenderId };
        return this.http.post<HttpResponse<any>>(
            this.resourceUrl + 'declare-war',
            requestDto,
            { observe: 'response' }
        );
    }
}
