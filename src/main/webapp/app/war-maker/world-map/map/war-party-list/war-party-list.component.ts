import { Component, Input, OnInit } from '@angular/core';

import type { IWarPartyEntity} from 'app/shared/model/war-party-entity.model';
import { UnitsIcons } from 'app/war-maker/army/army/units-manager';

@Component({
    selector: 'war-war-party-list',
    templateUrl: './war-party-list.component.html',
    styleUrls: ['./war-party-list.component.scss']
})
export class WarPartyListComponent implements OnInit {
    @Input() warParty: IWarPartyEntity;
    isCollapsed = true;
    unitsCount: number;
    unitsIcons = new UnitsIcons();
    
    constructor() {
    }

    ngOnInit(): void {
        if (this.warParty.units) {
            this.unitsCount = (this.warParty.units.spearmen ? this.warParty.units.spearmen  : 0)+
                            (this.warParty.units.swordsmen ? this.warParty.units.swordsmen : 0) +
                            (this.warParty.units.archers ? this.warParty.units.archers : 0) +
                            (this.warParty.units.crossbowmen ? this.warParty.units.crossbowmen : 0);
        } else {
            throw new Error('Units are not defined for War Party');
        }
    }
}

