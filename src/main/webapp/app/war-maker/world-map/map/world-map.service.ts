import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { WorldFieldViewDto } from './value/world-field-view-dto';
import { WarPartyCommand } from './value/war-party-command';
import { Coordinates } from './value/coordinates';
import { WarPartyJourneyDto } from 'app/war-maker/main/events-viewer/value/war-party-journey-dto';

@Injectable({
    providedIn: 'root'
})
export class WorldMapService {
    public resourceUrl = SERVER_API_URL + 'api/world';
    public warPartyResourceUrl = SERVER_API_URL + 'api/war-party/';

    constructor(private http: HttpClient) {}

    getPlayerWorld(playerId: number): Observable<HttpResponse<WorldFieldViewDto[][]>> {
        const params = new HttpParams().append('playerId', playerId.toString());
        return this.http.get<WorldFieldViewDto[][]>(this.resourceUrl, { observe: 'response', params });
    }

    moveWarParty(warPartyId: number, path: Coordinates[], command: WarPartyCommand): Observable<HttpResponse<WarPartyJourneyDto>> {
        const slicedPath = path.slice(1);
        return this.http.post<WarPartyJourneyDto>(this.warPartyResourceUrl + 'move', { warPartyId, path : slicedPath, command }, { observe: 'response' });
    }
}
