import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { WarPartyState } from 'app/shared/model/enumerations/war-party-state.model';

@Component({
    selector: 'war-units-sending-list',
    templateUrl: './units-sending-list.component.html',
    styles: []
})
export class UnitsSendingListComponent implements OnInit {
    public isCollapsed = true;
    public isPartySendable = true;
    @Input() warParty: WarPartyEntity;
    @Output() warPartySend = new EventEmitter<number>();

    constructor() { }

    ngOnInit(): void {
        this.isPartySendable = this.warParty.state === WarPartyState.MOVING;
    }

    public confirm(warPartyId: number | undefined): void {
        this.warPartySend.emit(warPartyId);
    }

}
