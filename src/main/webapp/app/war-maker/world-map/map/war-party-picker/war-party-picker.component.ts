import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService } from 'ng-jhipster';
import { Coordinates } from '../value/coordinates';
import { WarPartyCommand } from '../value/war-party-command';
import { WorldMapDataProviderService } from '../world-map-data-provider.service';
import { WorldMapService } from '../world-map.service';


@Component({
    selector: 'war-war-party-picker',
    templateUrl: './war-party-picker.component.html',
    styleUrls: ['./war-party-picker.scss']
})
export class WarPartyPickerComponent{
    isCollapsed = true;
    @Input() command: WarPartyCommand;
    @Input() currentPath: Coordinates[];

    constructor(public activeModal: NgbActiveModal,
                private worldMapService: WorldMapService,
                public worldMapDataProviderService: WorldMapDataProviderService,
                private alertService: JhiAlertService) { }

    public moveWarParty(warPartyId: number): void {
        this.worldMapService
            .moveWarParty(warPartyId, this.currentPath, this.command)
            .subscribe(
                () => {
                    this.activeModal.close();
                    return this.alertService.success('warMaker.worldMap.warPartySend');
                },
                () => this.alertService.error('warMaker.worldMap.warPartySendError')
            );
    }
}
