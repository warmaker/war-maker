import { HttpResponse } from '@angular/common/http';
import { Injectable, OnDestroy, Renderer2, RendererFactory2 } from '@angular/core';
import { WarPartyState } from 'app/shared/model/enumerations/war-party-state.model';
import { WarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { CityViewService } from 'app/war-maker/main/city-view/city-view.service';
import { CityResponse } from 'app/war-maker/main/city-view/value/city-response';
import { WarPartyJourneyDto } from 'app/war-maker/main/events-viewer/value/war-party-journey-dto';
import { MainPageService } from 'app/war-maker/main/main-page.service';
import { UserDataService } from 'app/war-maker/shared/tracking/user-data.service';
import { BehaviorSubject, forkJoin, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { D3WorldMap } from './tile-map/d3/d3-map/d3-world-map';
import { UserActionsService } from './user-actions.service';
import { Coordinates } from './value/coordinates';
import { WorldFieldViewDto } from './value/world-field-view-dto';
import { WorldMapService } from './world-map.service';

export class DynamicWorldMapData {
    constructor(public readonly warPartiesOnMap: Observable<WarPartyEntity[]>, 
                public readonly warPartyJourneys: Observable<WarPartyJourneyDto[]>) {}
}

@Injectable()
export class WorldMapDataProviderService implements OnDestroy {
    d3WorldMap: D3WorldMap;
    private city: CityResponse;
    private renderer: Renderer2;
    private warPartiesOnMap: BehaviorSubject<WarPartyEntity[]> = new BehaviorSubject([]);
    private warPartyJourneys: BehaviorSubject<WarPartyJourneyDto[]> = new BehaviorSubject([]);
    public warPartiesForSelectedField: WarPartyEntity[] = [];
    public initializationPending = true;

    constructor(private cityService: CityViewService,
                private mainPageService: MainPageService,
                private worldMapService: WorldMapService,
                private userDataService: UserDataService,
                private userActions: UserActionsService,
                rendererFactory: RendererFactory2) {
        this.renderer = rendererFactory.createRenderer(null, null);
    }

    ngOnDestroy(): void {
        this.warPartiesOnMap = new BehaviorSubject([]);
        this.warPartyJourneys = new BehaviorSubject([]);
        this.warPartiesForSelectedField = [];
        this.d3WorldMap = null;
    }

    createWorld(): void {
        this.initializationPending = true;
        this.city = this.userDataService.getCurrentCity()!;
        forkJoin(
            this.reloadPlayerWorld(),
            this.getWarPartyJourneyEvents(),
            this.getWarPartiesOnMap()
        ).subscribe(res => {
            this.warPartyJourneys.next(res[1]);
            this.warPartiesOnMap.next(res[2]);
            this.warPartiesForSelectedField = this.findWarPartiesForSelectedField();
            this.initializationPending = false;
        })
    }

    selectField(): void {
        this.userActions.updateSelectedField();
        this.warPartiesForSelectedField = this.findWarPartiesForSelectedField();
    }

    private findWarPartiesForSelectedField(): WarPartyEntity[] {
        return this.warPartiesOnMap.value.filter(warParty => {
            if (warParty.position) {
                return Coordinates.of(warParty.position).equals(this.userActions.getSelectedField().coordinates) && warParty.state !== WarPartyState.MOVING;
            } else {
                throw new Error('Position of War Party is not defined');
            }
        });
    }

    refreshWarPartiesInfo(): void {
        forkJoin(this.getWarPartyJourneyEvents(), this.getWarPartiesOnMap())
            .subscribe(res => {
                this.warPartyJourneys.next(res[0]);
                this.warPartiesOnMap.next(res[1]);
            });
    }

    reloadPlayerWorld(): Observable<HttpResponse<WorldFieldViewDto[][]>> {
        return this.worldMapService
            .getPlayerWorld(this.userDataService.getCurrentProfile())
            .pipe(tap(res => {
                this.d3WorldMap = D3WorldMap.create(
                    this.renderer,
                    res.body!,
                    new DynamicWorldMapData(this.warPartiesOnMap, this.warPartyJourneys),
                    this.userActions,
                    this.city
                );
            }));
    }

    private getWarPartiesOnMap(): Observable<WarPartyEntity[]> {
        return this.cityService.findWarParties(this.city.id);
    }

    private getWarPartyJourneyEvents(): Observable<WarPartyJourneyDto[]> {
        return this.mainPageService.getWarPartyJourneyEvents(this.userDataService.getCurrentProfile());
    }
}
