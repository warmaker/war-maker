import { Route } from '@angular/router';

import { WorldMapComponent } from './map/world-map.component';

const WORLD_MAP_ROUTE: Route = {
    path: '',
    component: WorldMapComponent,
    resolve: {},
    data: {
        pageTitle: 'warMaker.navbar.worldMap'
    }
};

export const WORLD_MAP_ROUTES = [WORLD_MAP_ROUTE];
