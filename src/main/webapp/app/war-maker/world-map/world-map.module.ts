import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WarMakerSharedModule } from 'app/shared/shared.module';
import { JhiAlertService } from 'ng-jhipster';
import { SharedModule } from '../shared/shared.module';
import { TileMapComponent } from './map/tile-map/tile-map.component';
import { UserActionsService } from './map/user-actions.service';
import { WarPartyListComponent } from './map/war-party-list/war-party-list.component';
import { UnitsSendingListComponent } from './map/war-party-picker/units-sending-list/units-sending-list.component';
import { WarPartyPickerComponent } from './map/war-party-picker/war-party-picker.component';
import { WorldMapActionMenuComponent } from './map/world-map-action-menu/world-map-action-menu.component';
import { WorldMapDataProviderService } from './map/world-map-data-provider.service';
import { WorldMapComponent } from './map/world-map.component';
import { WORLD_MAP_ROUTES } from './world-map.route';


@NgModule({
    imports: [
        CommonModule,
        WarMakerSharedModule,
        RouterModule.forChild(WORLD_MAP_ROUTES),
        SharedModule,
        NgbModule
    ],
    exports: [TileMapComponent],
    declarations: [
        WorldMapComponent,
        TileMapComponent,
        WarPartyPickerComponent,
        UnitsSendingListComponent,
        WarPartyListComponent,
        WorldMapActionMenuComponent
    ],
    providers: [WorldMapDataProviderService, UserActionsService, JhiAlertService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WorldMapModule {}
