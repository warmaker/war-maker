package com.graphzero.warmaker.generators

import com.graphzero.warmaker.application.warparty.battle.WarPartyBattle
import com.graphzero.warmaker.domain.WarPartyEntity
import com.graphzero.warmaker.utils.TestConfigLoader

class WarPartyBattleGenerator {
    private val playerGenerator = PlayerGenerator()
    private val applicationProperties = TestConfigLoader.testConfig

    @JvmOverloads
    fun from(warPartyId: Long = 1L): WarPartyBattle {
        return WarPartyBattle(
            AggregateRootLogicGenerator.from(warPartyId),
            HumanUnitsListGenerator.units(100),
            playerGenerator.getPlayer(),
            applicationProperties.units
        )
    }

    fun from(warPartyEntity: WarPartyEntity): WarPartyBattle {
        return WarPartyBattle(
            AggregateRootLogicGenerator.from(warPartyEntity.id),
            HumanUnitsListGenerator.units(warPartyEntity.units),
            playerGenerator.getPlayer(),
            applicationProperties.units
        )
    }
}
