package com.graphzero.warmaker.generators

import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.domain.UnitsEntity

object HumanUnitsListGenerator {
    fun units(numberOfUnits: Int): HumanUnits {
        return HumanUnits(1, numberOfUnits, numberOfUnits, numberOfUnits, numberOfUnits)
    }

    fun units(unitsEntity: UnitsEntity): HumanUnits {
        return HumanUnits(unitsEntity.id, unitsEntity)
    }
}
