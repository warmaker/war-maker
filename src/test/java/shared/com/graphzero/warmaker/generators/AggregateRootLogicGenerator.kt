package com.graphzero.warmaker.generators

import com.graphzero.warmaker.application.ddd.AggregateRootLogic

object AggregateRootLogicGenerator {
    fun basic(): AggregateRootLogic {
        return AggregateRootLogic(1L)
    }

    fun from(id: Long): AggregateRootLogic {
        return AggregateRootLogic(id)
    }
}
