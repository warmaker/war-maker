package com.graphzero.warmaker.generators

import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.resources.Resources.Companion.emptyResources
import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.application.warparty.builders.WarPartyBuilder
import com.graphzero.warmaker.application.world.Coordinates
import com.graphzero.warmaker.domain.enumeration.WarPartyState
import com.graphzero.warmaker.utils.TestConfigLoader
import java.time.ZonedDateTime
import java.util.*

class WarPartyGenerator {
    private val playerGenerator = PlayerGenerator()

    val mediumTestWarParty: WarPartyBuilder
        get() = WarPartyBuilder()
            .id(1L)
            .state(WarPartyState.STATIONED)
            .unitsList(HumanUnitsListGenerator.units(50))
            .carriedResources(Resources(1, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0))
            .location(Coordinates(0, 0))
            .fieldEntryDate(ZonedDateTime.now())
            .owner(playerGenerator.getPlayer())
            .unitsConfig(
                TestConfigLoader.testConfig
                    .getUnits()
            )

    val smallTestWarParty: WarPartyBuilder
        get() = WarPartyBuilder()
            .id(2L)
            .state(WarPartyState.STATIONED)
            .unitsList(HumanUnitsListGenerator.units(10))
            .carriedResources(Resources(1, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0))
            .location(Coordinates(0, 0))
            .fieldEntryDate(ZonedDateTime.now())
            .owner(playerGenerator.getPlayer())
            .unitsConfig(
                TestConfigLoader.testConfig
                    .getUnits()
            )

    val bigTestWarParty: WarPartyBuilder
        get() = WarPartyBuilder()
            .id(3L)
            .state(WarPartyState.STATIONED)
            .unitsList(HumanUnitsListGenerator.units(1000))
            .carriedResources(Resources(1, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0, 1000.0))
            .location(Coordinates(0, 0))
            .fieldEntryDate(ZonedDateTime.now())
            .owner(playerGenerator.getPlayer())
            .unitsConfig(
                TestConfigLoader.testConfig
                    .getUnits()
            )
    val mixedWarPartiesInField: MutableList<WarParty>
        get() = ArrayList(
            listOf(
                WarPartyBuilder().id(1L)
                    .state(WarPartyState.STATIONED)
                    .unitsList(HumanUnitsListGenerator.units(10))
                    .carriedResources(emptyResources())
                    .location(Coordinates(1, 1))
                    .fieldEntryDate(ZonedDateTime.now())
                    .owner(playerGenerator.enemyPlayer)
                    .unitsConfig(TestConfigLoader.testConfig.getUnits())
                    .build(),
                WarPartyBuilder().id(2L)
                    .state(WarPartyState.STATIONED)
                    .unitsList(HumanUnitsListGenerator.units(5))
                    .carriedResources(emptyResources())
                    .location(Coordinates(1, 1))
                    .fieldEntryDate(ZonedDateTime.now())
                    .owner(playerGenerator.neutralPlayer)
                    .unitsConfig(TestConfigLoader.testConfig.getUnits())
                    .build()
            )
        )

    val neutralPartiesInField: List<WarParty>
        get() = ArrayList(
            listOf(
                WarPartyBuilder().id(1L)
                    .state(WarPartyState.STATIONED)
                    .unitsList(HumanUnitsListGenerator.units(10))
                    .carriedResources(emptyResources())
                    .location(Coordinates(1, 1))
                    .fieldEntryDate(ZonedDateTime.now())
                    .owner(playerGenerator.neutralPlayer)
                    .unitsConfig(TestConfigLoader.testConfig.getUnits())
                    .build(),
                WarPartyBuilder().id(2L)
                    .state(WarPartyState.STATIONED)
                    .unitsList(HumanUnitsListGenerator.units(5))
                    .carriedResources(emptyResources())
                    .location(Coordinates(1, 1))
                    .fieldEntryDate(ZonedDateTime.now())
                    .owner(playerGenerator.neutralPlayer)
                    .unitsConfig(TestConfigLoader.testConfig.getUnits())
                    .build()
            )
        )
}
