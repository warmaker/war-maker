package com.graphzero.warmaker.generators

import com.graphzero.warmaker.application.player.Player
import com.graphzero.warmaker.domain.PlayerRelations
import com.graphzero.warmaker.domain.ProfileEntity
import com.graphzero.warmaker.domain.enumeration.RelationType
import java.util.Set

class PlayerGenerator {
    private val player = ProfileEntity()
    private val enemy = ProfileEntity()
    private val ally = ProfileEntity()
    private val neutral = ProfileEntity()
    private val bandit = ProfileEntity()

    fun getPlayer(): Player {
        val enemyPlayer = PlayerRelations()
            .owner(player)
            .targetPlayer(enemy)
            .relation(RelationType.ENEMY)
        enemyPlayer.id = 1111L
        val allyPlayer = PlayerRelations()
            .owner(player)
            .targetPlayer(ally)
            .relation(RelationType.ALLY)
        enemyPlayer.id = 1112L
        return Player(player.id, "player", setOf(enemyPlayer, allyPlayer))
    }

    val enemyPlayer: Player
        get() {
            val playerEnemyRelation = PlayerRelations()
                .owner(enemy)
                .targetPlayer(player)
                .relation(RelationType.ENEMY)
            playerEnemyRelation.id = 2222L
            val allyEnemyRelation = PlayerRelations()
                .owner(enemy)
                .targetPlayer(ally)
                .relation(RelationType.ENEMY)
            allyEnemyRelation.id = 2223L
            return Player(enemy.id, "enemy-player", setOf(playerEnemyRelation, allyEnemyRelation))
        }

    val allyPlayer: Player
        get() {
            val enemyAllyRelation = PlayerRelations()
                .owner(ally)
                .targetPlayer(enemy)
                .relation(RelationType.ENEMY)
            enemyAllyRelation.id = 3333L
            val playerAllyRelation = PlayerRelations()
                .owner(ally)
                .targetPlayer(player)
                .relation(RelationType.ALLY)
            playerAllyRelation.id = 3334L
            return Player(ally.id, "ally-player", setOf(enemyAllyRelation, playerAllyRelation))
        }

    val banditPlayer: Player
        get() = Player(-1, Player.BANDIT_NAME, emptySet())

    val neutralPlayer: Player
        get() = Player(neutral.id, "neutral-player", setOf())

    init {
        bandit.id = -1L
        player.id = 1L
        enemy.id = 2L
        ally.id = 3L
        neutral.id = 4L
    }
}
