package com.graphzero.warmaker.utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.graphzero.warmaker.config.ApplicationProperties
import java.io.File
import java.io.IOException

object TestConfigLoader {
    private val mapper = ObjectMapper(YAMLFactory())
    val testConfig: ApplicationProperties
        get() = try {
            mapper.readValue(
                File("src/test/resources/config/unit-test-application.yml"),
                ApplicationProperties::class.java
            )
        } catch (e: IOException) {
            throw RuntimeException("Failed to load application config.")
        }
    val unitsConfig: ApplicationProperties.Units
        get() = testConfig.getUnits()
}
