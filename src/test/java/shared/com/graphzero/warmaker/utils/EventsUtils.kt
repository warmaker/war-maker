package com.graphzero.warmaker.utils

import com.graphzero.warmaker.application.ddd.AggregateRoot
import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.warparty.battle.WarPartyBattleEvent
import com.graphzero.warmaker.application.warparty.value.WarPartyResourcesChangeEvent
import org.assertj.core.api.Assertions.assertThat
import java.util.*
import java.util.stream.Collectors

object EventsUtils {
    fun getBattlesEvents(aggregateRoot: AggregateRoot?): List<WarPartyBattleEvent> {
        return getBattlesEvents(aggregateRoot!!.getDomainEvents())
    }

    fun getOneBattleEvent(aggregateRoot: AggregateRoot?): Optional<WarPartyBattleEvent> {
        val battleEvents = getBattlesEvents(aggregateRoot)
        return Optional.ofNullable(battleEvents[0])
    }

    fun <T> getOneEvent(aggregateRoot: AggregateRoot, clazz: Class<T>): T {
        val events = getEvents(aggregateRoot.getDomainEvents(), clazz)
        assertThat(events).hasSizeBetween(1, 1)
        return events[0]
    }

    fun getOneBattleEvent(events: List<DomainEvent<out WarMakerEventType>>): Optional<WarPartyBattleEvent> {
        val battleEvents = getBattlesEvents(events)
        return Optional.ofNullable(battleEvents[0])
    }

    private fun getBattlesEvents(events: List<DomainEvent<out WarMakerEventType>>): MutableList<WarPartyBattleEvent> {
        return events.stream()
            .filter { domainEvent: DomainEvent<out WarMakerEventType> ->
                domainEvent.warMakerEventType == WarMakerArmyEvent.WAR_PARTY_BATTLES
            }
            .peek { domainEvent: DomainEvent<out WarMakerEventType> ->
                assertThat(domainEvent).isInstanceOf(
                    WarPartyBattleEvent::class.java
                )
            }
            .map { domainEvent: DomainEvent<out WarMakerEventType> -> domainEvent as WarPartyBattleEvent? }
            .collect(Collectors.toList())
    }

    private fun <T> getEvents(events: List<DomainEvent<out WarMakerEventType>>, clazz: Class<T>): List<T> {
        return events.filterIsInstance(clazz)
    }

    fun getWarPartyResourcesChangeEvent(events: List<DomainEvent<out WarMakerEventType>>): MutableList<WarPartyResourcesChangeEvent> {
        return events.stream()
            .filter { domainEvent: DomainEvent<out WarMakerEventType> ->
                domainEvent.warMakerEventType == WarMakerArmyEvent.WAR_PARTY_RESOURCES_CHANGE
            }
            .peek { domainEvent: DomainEvent<out WarMakerEventType> ->
                assertThat(domainEvent).isInstanceOf(
                    WarPartyResourcesChangeEvent::class.java
                )
            }
            .map { domainEvent: DomainEvent<out WarMakerEventType> -> domainEvent as WarPartyResourcesChangeEvent? }
            .collect(Collectors.toList())
    }

    private fun getWarPartyResourcesChangeEvent(aggregateRoot: AggregateRoot): List<WarPartyResourcesChangeEvent> {
        return getWarPartyResourcesChangeEvent(aggregateRoot.getDomainEvents())
    }

    fun getOneResourcesChangeEvent(aggregateRoot: AggregateRoot): Optional<WarPartyResourcesChangeEvent> {
        val resourcesChangeEvents = getWarPartyResourcesChangeEvent(aggregateRoot)
        return Optional.ofNullable(resourcesChangeEvents[0])
    }
}
