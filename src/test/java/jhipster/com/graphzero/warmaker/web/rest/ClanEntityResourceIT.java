package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.ClanEntity;
import com.graphzero.warmaker.repository.ClanEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ClanEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ClanEntityResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final byte[] DEFAULT_EMBLEM = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_EMBLEM = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_EMBLEM_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_EMBLEM_CONTENT_TYPE = "image/png";

    @Autowired
    private ClanEntityRepository clanEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restClanEntityMockMvc;

    private ClanEntity clanEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClanEntity createEntity(EntityManager em) {
        ClanEntity clanEntity = new ClanEntity()
            .name(DEFAULT_NAME)
            .emblem(DEFAULT_EMBLEM)
            .emblemContentType(DEFAULT_EMBLEM_CONTENT_TYPE);
        return clanEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClanEntity createUpdatedEntity(EntityManager em) {
        ClanEntity clanEntity = new ClanEntity()
            .name(UPDATED_NAME)
            .emblem(UPDATED_EMBLEM)
            .emblemContentType(UPDATED_EMBLEM_CONTENT_TYPE);
        return clanEntity;
    }

    @BeforeEach
    public void initTest() {
        clanEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createClanEntity() throws Exception {
        int databaseSizeBeforeCreate = clanEntityRepository.findAll().size();
        // Create the ClanEntity
        restClanEntityMockMvc.perform(post("/api/clan-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(clanEntity)))
            .andExpect(status().isCreated());

        // Validate the ClanEntity in the database
        List<ClanEntity> clanEntityList = clanEntityRepository.findAll();
        assertThat(clanEntityList).hasSize(databaseSizeBeforeCreate + 1);
        ClanEntity testClanEntity = clanEntityList.get(clanEntityList.size() - 1);
        assertThat(testClanEntity.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testClanEntity.getEmblem()).isEqualTo(DEFAULT_EMBLEM);
        assertThat(testClanEntity.getEmblemContentType()).isEqualTo(DEFAULT_EMBLEM_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createClanEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clanEntityRepository.findAll().size();

        // Create the ClanEntity with an existing ID
        clanEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClanEntityMockMvc.perform(post("/api/clan-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(clanEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ClanEntity in the database
        List<ClanEntity> clanEntityList = clanEntityRepository.findAll();
        assertThat(clanEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllClanEntities() throws Exception {
        // Initialize the database
        clanEntityRepository.saveAndFlush(clanEntity);

        // Get all the clanEntityList
        restClanEntityMockMvc.perform(get("/api/clan-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clanEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].emblemContentType").value(hasItem(DEFAULT_EMBLEM_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].emblem").value(hasItem(Base64Utils.encodeToString(DEFAULT_EMBLEM))));
    }
    
    @Test
    @Transactional
    public void getClanEntity() throws Exception {
        // Initialize the database
        clanEntityRepository.saveAndFlush(clanEntity);

        // Get the clanEntity
        restClanEntityMockMvc.perform(get("/api/clan-entities/{id}", clanEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(clanEntity.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.emblemContentType").value(DEFAULT_EMBLEM_CONTENT_TYPE))
            .andExpect(jsonPath("$.emblem").value(Base64Utils.encodeToString(DEFAULT_EMBLEM)));
    }
    @Test
    @Transactional
    public void getNonExistingClanEntity() throws Exception {
        // Get the clanEntity
        restClanEntityMockMvc.perform(get("/api/clan-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClanEntity() throws Exception {
        // Initialize the database
        clanEntityRepository.saveAndFlush(clanEntity);

        int databaseSizeBeforeUpdate = clanEntityRepository.findAll().size();

        // Update the clanEntity
        ClanEntity updatedClanEntity = clanEntityRepository.findById(clanEntity.getId()).get();
        // Disconnect from session so that the updates on updatedClanEntity are not directly saved in db
        em.detach(updatedClanEntity);
        updatedClanEntity
            .name(UPDATED_NAME)
            .emblem(UPDATED_EMBLEM)
            .emblemContentType(UPDATED_EMBLEM_CONTENT_TYPE);

        restClanEntityMockMvc.perform(put("/api/clan-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedClanEntity)))
            .andExpect(status().isOk());

        // Validate the ClanEntity in the database
        List<ClanEntity> clanEntityList = clanEntityRepository.findAll();
        assertThat(clanEntityList).hasSize(databaseSizeBeforeUpdate);
        ClanEntity testClanEntity = clanEntityList.get(clanEntityList.size() - 1);
        assertThat(testClanEntity.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testClanEntity.getEmblem()).isEqualTo(UPDATED_EMBLEM);
        assertThat(testClanEntity.getEmblemContentType()).isEqualTo(UPDATED_EMBLEM_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingClanEntity() throws Exception {
        int databaseSizeBeforeUpdate = clanEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClanEntityMockMvc.perform(put("/api/clan-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(clanEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ClanEntity in the database
        List<ClanEntity> clanEntityList = clanEntityRepository.findAll();
        assertThat(clanEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteClanEntity() throws Exception {
        // Initialize the database
        clanEntityRepository.saveAndFlush(clanEntity);

        int databaseSizeBeforeDelete = clanEntityRepository.findAll().size();

        // Delete the clanEntity
        restClanEntityMockMvc.perform(delete("/api/clan-entities/{id}", clanEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ClanEntity> clanEntityList = clanEntityRepository.findAll();
        assertThat(clanEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
