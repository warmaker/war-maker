package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.PointOfInterestEntity;
import com.graphzero.warmaker.repository.PointOfInterestEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.graphzero.warmaker.domain.enumeration.PointOfInterestType;
/**
 * Integration tests for the {@link PointOfInterestEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PointOfInterestEntityResourceIT {

    private static final PointOfInterestType DEFAULT_TYPE = PointOfInterestType.RIVER_CROSSING;
    private static final PointOfInterestType UPDATED_TYPE = PointOfInterestType.FOREST;

    @Autowired
    private PointOfInterestEntityRepository pointOfInterestEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPointOfInterestEntityMockMvc;

    private PointOfInterestEntity pointOfInterestEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PointOfInterestEntity createEntity(EntityManager em) {
        PointOfInterestEntity pointOfInterestEntity = new PointOfInterestEntity()
            .type(DEFAULT_TYPE);
        return pointOfInterestEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PointOfInterestEntity createUpdatedEntity(EntityManager em) {
        PointOfInterestEntity pointOfInterestEntity = new PointOfInterestEntity()
            .type(UPDATED_TYPE);
        return pointOfInterestEntity;
    }

    @BeforeEach
    public void initTest() {
        pointOfInterestEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createPointOfInterestEntity() throws Exception {
        int databaseSizeBeforeCreate = pointOfInterestEntityRepository.findAll().size();
        // Create the PointOfInterestEntity
        restPointOfInterestEntityMockMvc.perform(post("/api/point-of-interest-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pointOfInterestEntity)))
            .andExpect(status().isCreated());

        // Validate the PointOfInterestEntity in the database
        List<PointOfInterestEntity> pointOfInterestEntityList = pointOfInterestEntityRepository.findAll();
        assertThat(pointOfInterestEntityList).hasSize(databaseSizeBeforeCreate + 1);
        PointOfInterestEntity testPointOfInterestEntity = pointOfInterestEntityList.get(pointOfInterestEntityList.size() - 1);
        assertThat(testPointOfInterestEntity.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createPointOfInterestEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pointOfInterestEntityRepository.findAll().size();

        // Create the PointOfInterestEntity with an existing ID
        pointOfInterestEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPointOfInterestEntityMockMvc.perform(post("/api/point-of-interest-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pointOfInterestEntity)))
            .andExpect(status().isBadRequest());

        // Validate the PointOfInterestEntity in the database
        List<PointOfInterestEntity> pointOfInterestEntityList = pointOfInterestEntityRepository.findAll();
        assertThat(pointOfInterestEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPointOfInterestEntities() throws Exception {
        // Initialize the database
        pointOfInterestEntityRepository.saveAndFlush(pointOfInterestEntity);

        // Get all the pointOfInterestEntityList
        restPointOfInterestEntityMockMvc.perform(get("/api/point-of-interest-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pointOfInterestEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }
    
    @Test
    @Transactional
    public void getPointOfInterestEntity() throws Exception {
        // Initialize the database
        pointOfInterestEntityRepository.saveAndFlush(pointOfInterestEntity);

        // Get the pointOfInterestEntity
        restPointOfInterestEntityMockMvc.perform(get("/api/point-of-interest-entities/{id}", pointOfInterestEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pointOfInterestEntity.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingPointOfInterestEntity() throws Exception {
        // Get the pointOfInterestEntity
        restPointOfInterestEntityMockMvc.perform(get("/api/point-of-interest-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePointOfInterestEntity() throws Exception {
        // Initialize the database
        pointOfInterestEntityRepository.saveAndFlush(pointOfInterestEntity);

        int databaseSizeBeforeUpdate = pointOfInterestEntityRepository.findAll().size();

        // Update the pointOfInterestEntity
        PointOfInterestEntity updatedPointOfInterestEntity = pointOfInterestEntityRepository.findById(pointOfInterestEntity.getId()).get();
        // Disconnect from session so that the updates on updatedPointOfInterestEntity are not directly saved in db
        em.detach(updatedPointOfInterestEntity);
        updatedPointOfInterestEntity
            .type(UPDATED_TYPE);

        restPointOfInterestEntityMockMvc.perform(put("/api/point-of-interest-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPointOfInterestEntity)))
            .andExpect(status().isOk());

        // Validate the PointOfInterestEntity in the database
        List<PointOfInterestEntity> pointOfInterestEntityList = pointOfInterestEntityRepository.findAll();
        assertThat(pointOfInterestEntityList).hasSize(databaseSizeBeforeUpdate);
        PointOfInterestEntity testPointOfInterestEntity = pointOfInterestEntityList.get(pointOfInterestEntityList.size() - 1);
        assertThat(testPointOfInterestEntity.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingPointOfInterestEntity() throws Exception {
        int databaseSizeBeforeUpdate = pointOfInterestEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPointOfInterestEntityMockMvc.perform(put("/api/point-of-interest-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pointOfInterestEntity)))
            .andExpect(status().isBadRequest());

        // Validate the PointOfInterestEntity in the database
        List<PointOfInterestEntity> pointOfInterestEntityList = pointOfInterestEntityRepository.findAll();
        assertThat(pointOfInterestEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePointOfInterestEntity() throws Exception {
        // Initialize the database
        pointOfInterestEntityRepository.saveAndFlush(pointOfInterestEntity);

        int databaseSizeBeforeDelete = pointOfInterestEntityRepository.findAll().size();

        // Delete the pointOfInterestEntity
        restPointOfInterestEntityMockMvc.perform(delete("/api/point-of-interest-entities/{id}", pointOfInterestEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PointOfInterestEntity> pointOfInterestEntityList = pointOfInterestEntityRepository.findAll();
        assertThat(pointOfInterestEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
