package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.LairEntity;
import com.graphzero.warmaker.domain.FieldEntity;
import com.graphzero.warmaker.repository.LairEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LairEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LairEntityResourceIT {

    private static final String DEFAULT_LAIR_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAIR_NAME = "BBBBBBBBBB";

    @Autowired
    private LairEntityRepository lairEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLairEntityMockMvc;

    private LairEntity lairEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LairEntity createEntity(EntityManager em) {
        LairEntity lairEntity = new LairEntity()
            .lairName(DEFAULT_LAIR_NAME);
        // Add required entity
        FieldEntity fieldEntity;
        if (TestUtil.findAll(em, FieldEntity.class).isEmpty()) {
            fieldEntity = FieldEntityResourceIT.createEntity(em);
            em.persist(fieldEntity);
            em.flush();
        } else {
            fieldEntity = TestUtil.findAll(em, FieldEntity.class).get(0);
        }
        lairEntity.setLocation(fieldEntity);
        return lairEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LairEntity createUpdatedEntity(EntityManager em) {
        LairEntity lairEntity = new LairEntity()
            .lairName(UPDATED_LAIR_NAME);
        // Add required entity
        FieldEntity fieldEntity;
        if (TestUtil.findAll(em, FieldEntity.class).isEmpty()) {
            fieldEntity = FieldEntityResourceIT.createUpdatedEntity(em);
            em.persist(fieldEntity);
            em.flush();
        } else {
            fieldEntity = TestUtil.findAll(em, FieldEntity.class).get(0);
        }
        lairEntity.setLocation(fieldEntity);
        return lairEntity;
    }

    @BeforeEach
    public void initTest() {
        lairEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createLairEntity() throws Exception {
        int databaseSizeBeforeCreate = lairEntityRepository.findAll().size();
        // Create the LairEntity
        restLairEntityMockMvc.perform(post("/api/lair-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lairEntity)))
            .andExpect(status().isCreated());

        // Validate the LairEntity in the database
        List<LairEntity> lairEntityList = lairEntityRepository.findAll();
        assertThat(lairEntityList).hasSize(databaseSizeBeforeCreate + 1);
        LairEntity testLairEntity = lairEntityList.get(lairEntityList.size() - 1);
        assertThat(testLairEntity.getLairName()).isEqualTo(DEFAULT_LAIR_NAME);
    }

    @Test
    @Transactional
    public void createLairEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = lairEntityRepository.findAll().size();

        // Create the LairEntity with an existing ID
        lairEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLairEntityMockMvc.perform(post("/api/lair-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lairEntity)))
            .andExpect(status().isBadRequest());

        // Validate the LairEntity in the database
        List<LairEntity> lairEntityList = lairEntityRepository.findAll();
        assertThat(lairEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllLairEntities() throws Exception {
        // Initialize the database
        lairEntityRepository.saveAndFlush(lairEntity);

        // Get all the lairEntityList
        restLairEntityMockMvc.perform(get("/api/lair-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lairEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].lairName").value(hasItem(DEFAULT_LAIR_NAME)));
    }
    
    @Test
    @Transactional
    public void getLairEntity() throws Exception {
        // Initialize the database
        lairEntityRepository.saveAndFlush(lairEntity);

        // Get the lairEntity
        restLairEntityMockMvc.perform(get("/api/lair-entities/{id}", lairEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(lairEntity.getId().intValue()))
            .andExpect(jsonPath("$.lairName").value(DEFAULT_LAIR_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingLairEntity() throws Exception {
        // Get the lairEntity
        restLairEntityMockMvc.perform(get("/api/lair-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLairEntity() throws Exception {
        // Initialize the database
        lairEntityRepository.saveAndFlush(lairEntity);

        int databaseSizeBeforeUpdate = lairEntityRepository.findAll().size();

        // Update the lairEntity
        LairEntity updatedLairEntity = lairEntityRepository.findById(lairEntity.getId()).get();
        // Disconnect from session so that the updates on updatedLairEntity are not directly saved in db
        em.detach(updatedLairEntity);
        updatedLairEntity
            .lairName(UPDATED_LAIR_NAME);

        restLairEntityMockMvc.perform(put("/api/lair-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLairEntity)))
            .andExpect(status().isOk());

        // Validate the LairEntity in the database
        List<LairEntity> lairEntityList = lairEntityRepository.findAll();
        assertThat(lairEntityList).hasSize(databaseSizeBeforeUpdate);
        LairEntity testLairEntity = lairEntityList.get(lairEntityList.size() - 1);
        assertThat(testLairEntity.getLairName()).isEqualTo(UPDATED_LAIR_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingLairEntity() throws Exception {
        int databaseSizeBeforeUpdate = lairEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLairEntityMockMvc.perform(put("/api/lair-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(lairEntity)))
            .andExpect(status().isBadRequest());

        // Validate the LairEntity in the database
        List<LairEntity> lairEntityList = lairEntityRepository.findAll();
        assertThat(lairEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLairEntity() throws Exception {
        // Initialize the database
        lairEntityRepository.saveAndFlush(lairEntity);

        int databaseSizeBeforeDelete = lairEntityRepository.findAll().size();

        // Delete the lairEntity
        restLairEntityMockMvc.perform(delete("/api/lair-entities/{id}", lairEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LairEntity> lairEntityList = lairEntityRepository.findAll();
        assertThat(lairEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
