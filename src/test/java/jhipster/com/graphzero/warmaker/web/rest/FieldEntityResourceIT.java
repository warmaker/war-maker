package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.FieldEntity;
import com.graphzero.warmaker.repository.FieldEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.graphzero.warmaker.domain.enumeration.FieldType;
/**
 * Integration tests for the {@link FieldEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FieldEntityResourceIT {

    private static final Integer DEFAULT_X_COORDINATE = 1;
    private static final Integer UPDATED_X_COORDINATE = 2;

    private static final Integer DEFAULT_Y_COORDINATE = 1;
    private static final Integer UPDATED_Y_COORDINATE = 2;

    private static final FieldType DEFAULT_TYPE = FieldType.LAIR;
    private static final FieldType UPDATED_TYPE = FieldType.POINT_OF_INTEREST;

    @Autowired
    private FieldEntityRepository fieldEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFieldEntityMockMvc;

    private FieldEntity fieldEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FieldEntity createEntity(EntityManager em) {
        FieldEntity fieldEntity = new FieldEntity()
            .xCoordinate(DEFAULT_X_COORDINATE)
            .yCoordinate(DEFAULT_Y_COORDINATE)
            .type(DEFAULT_TYPE);
        return fieldEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FieldEntity createUpdatedEntity(EntityManager em) {
        FieldEntity fieldEntity = new FieldEntity()
            .xCoordinate(UPDATED_X_COORDINATE)
            .yCoordinate(UPDATED_Y_COORDINATE)
            .type(UPDATED_TYPE);
        return fieldEntity;
    }

    @BeforeEach
    public void initTest() {
        fieldEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createFieldEntity() throws Exception {
        int databaseSizeBeforeCreate = fieldEntityRepository.findAll().size();
        // Create the FieldEntity
        restFieldEntityMockMvc.perform(post("/api/field-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fieldEntity)))
            .andExpect(status().isCreated());

        // Validate the FieldEntity in the database
        List<FieldEntity> fieldEntityList = fieldEntityRepository.findAll();
        assertThat(fieldEntityList).hasSize(databaseSizeBeforeCreate + 1);
        FieldEntity testFieldEntity = fieldEntityList.get(fieldEntityList.size() - 1);
        assertThat(testFieldEntity.getxCoordinate()).isEqualTo(DEFAULT_X_COORDINATE);
        assertThat(testFieldEntity.getyCoordinate()).isEqualTo(DEFAULT_Y_COORDINATE);
        assertThat(testFieldEntity.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createFieldEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fieldEntityRepository.findAll().size();

        // Create the FieldEntity with an existing ID
        fieldEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFieldEntityMockMvc.perform(post("/api/field-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fieldEntity)))
            .andExpect(status().isBadRequest());

        // Validate the FieldEntity in the database
        List<FieldEntity> fieldEntityList = fieldEntityRepository.findAll();
        assertThat(fieldEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkxCoordinateIsRequired() throws Exception {
        int databaseSizeBeforeTest = fieldEntityRepository.findAll().size();
        // set the field null
        fieldEntity.setxCoordinate(null);

        // Create the FieldEntity, which fails.


        restFieldEntityMockMvc.perform(post("/api/field-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fieldEntity)))
            .andExpect(status().isBadRequest());

        List<FieldEntity> fieldEntityList = fieldEntityRepository.findAll();
        assertThat(fieldEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkyCoordinateIsRequired() throws Exception {
        int databaseSizeBeforeTest = fieldEntityRepository.findAll().size();
        // set the field null
        fieldEntity.setyCoordinate(null);

        // Create the FieldEntity, which fails.


        restFieldEntityMockMvc.perform(post("/api/field-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fieldEntity)))
            .andExpect(status().isBadRequest());

        List<FieldEntity> fieldEntityList = fieldEntityRepository.findAll();
        assertThat(fieldEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = fieldEntityRepository.findAll().size();
        // set the field null
        fieldEntity.setType(null);

        // Create the FieldEntity, which fails.


        restFieldEntityMockMvc.perform(post("/api/field-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fieldEntity)))
            .andExpect(status().isBadRequest());

        List<FieldEntity> fieldEntityList = fieldEntityRepository.findAll();
        assertThat(fieldEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFieldEntities() throws Exception {
        // Initialize the database
        fieldEntityRepository.saveAndFlush(fieldEntity);

        // Get all the fieldEntityList
        restFieldEntityMockMvc.perform(get("/api/field-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fieldEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].xCoordinate").value(hasItem(DEFAULT_X_COORDINATE)))
            .andExpect(jsonPath("$.[*].yCoordinate").value(hasItem(DEFAULT_Y_COORDINATE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }
    
    @Test
    @Transactional
    public void getFieldEntity() throws Exception {
        // Initialize the database
        fieldEntityRepository.saveAndFlush(fieldEntity);

        // Get the fieldEntity
        restFieldEntityMockMvc.perform(get("/api/field-entities/{id}", fieldEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fieldEntity.getId().intValue()))
            .andExpect(jsonPath("$.xCoordinate").value(DEFAULT_X_COORDINATE))
            .andExpect(jsonPath("$.yCoordinate").value(DEFAULT_Y_COORDINATE))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingFieldEntity() throws Exception {
        // Get the fieldEntity
        restFieldEntityMockMvc.perform(get("/api/field-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFieldEntity() throws Exception {
        // Initialize the database
        fieldEntityRepository.saveAndFlush(fieldEntity);

        int databaseSizeBeforeUpdate = fieldEntityRepository.findAll().size();

        // Update the fieldEntity
        FieldEntity updatedFieldEntity = fieldEntityRepository.findById(fieldEntity.getId()).get();
        // Disconnect from session so that the updates on updatedFieldEntity are not directly saved in db
        em.detach(updatedFieldEntity);
        updatedFieldEntity
            .xCoordinate(UPDATED_X_COORDINATE)
            .yCoordinate(UPDATED_Y_COORDINATE)
            .type(UPDATED_TYPE);

        restFieldEntityMockMvc.perform(put("/api/field-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFieldEntity)))
            .andExpect(status().isOk());

        // Validate the FieldEntity in the database
        List<FieldEntity> fieldEntityList = fieldEntityRepository.findAll();
        assertThat(fieldEntityList).hasSize(databaseSizeBeforeUpdate);
        FieldEntity testFieldEntity = fieldEntityList.get(fieldEntityList.size() - 1);
        assertThat(testFieldEntity.getxCoordinate()).isEqualTo(UPDATED_X_COORDINATE);
        assertThat(testFieldEntity.getyCoordinate()).isEqualTo(UPDATED_Y_COORDINATE);
        assertThat(testFieldEntity.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingFieldEntity() throws Exception {
        int databaseSizeBeforeUpdate = fieldEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFieldEntityMockMvc.perform(put("/api/field-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fieldEntity)))
            .andExpect(status().isBadRequest());

        // Validate the FieldEntity in the database
        List<FieldEntity> fieldEntityList = fieldEntityRepository.findAll();
        assertThat(fieldEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFieldEntity() throws Exception {
        // Initialize the database
        fieldEntityRepository.saveAndFlush(fieldEntity);

        int databaseSizeBeforeDelete = fieldEntityRepository.findAll().size();

        // Delete the fieldEntity
        restFieldEntityMockMvc.perform(delete("/api/field-entities/{id}", fieldEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FieldEntity> fieldEntityList = fieldEntityRepository.findAll();
        assertThat(fieldEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
