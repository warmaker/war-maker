package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.ResearchesEntity;
import com.graphzero.warmaker.repository.ResearchesEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ResearchesEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ResearchesEntityResourceIT {

    private static final Integer DEFAULT_INFANTRY_VITALITY = 1;
    private static final Integer UPDATED_INFANTRY_VITALITY = 2;

    private static final Integer DEFAULT_CAVALRY_VITALITY = 1;
    private static final Integer UPDATED_CAVALRY_VITALITY = 2;

    private static final Integer DEFAULT_LIGHT_ARMOR_VALUE = 1;
    private static final Integer UPDATED_LIGHT_ARMOR_VALUE = 2;

    private static final Integer DEFAULT_HEAVY_ARMOR_VALUE = 1;
    private static final Integer UPDATED_HEAVY_ARMOR_VALUE = 2;

    private static final Integer DEFAULT_MELEE_ATTACK = 1;
    private static final Integer UPDATED_MELEE_ATTACK = 2;

    private static final Integer DEFAULT_RANGED_ATTACK = 1;
    private static final Integer UPDATED_RANGED_ATTACK = 2;

    private static final Integer DEFAULT_INFANTRY_SPEED = 1;
    private static final Integer UPDATED_INFANTRY_SPEED = 2;

    private static final Integer DEFAULT_CAVALRY_SPEED = 1;
    private static final Integer UPDATED_CAVALRY_SPEED = 2;

    private static final Integer DEFAULT_CITY_BUILDING_SPEED = 1;
    private static final Integer UPDATED_CITY_BUILDING_SPEED = 2;

    private static final Integer DEFAULT_RECRUITMENT_SPEED = 1;
    private static final Integer UPDATED_RECRUITMENT_SPEED = 2;

    private static final Integer DEFAULT_SCOUTING = 1;
    private static final Integer UPDATED_SCOUTING = 2;

    @Autowired
    private ResearchesEntityRepository researchesEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restResearchesEntityMockMvc;

    private ResearchesEntity researchesEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ResearchesEntity createEntity(EntityManager em) {
        ResearchesEntity researchesEntity = new ResearchesEntity()
            .infantryVitality(DEFAULT_INFANTRY_VITALITY)
            .cavalryVitality(DEFAULT_CAVALRY_VITALITY)
            .lightArmorValue(DEFAULT_LIGHT_ARMOR_VALUE)
            .heavyArmorValue(DEFAULT_HEAVY_ARMOR_VALUE)
            .meleeAttack(DEFAULT_MELEE_ATTACK)
            .rangedAttack(DEFAULT_RANGED_ATTACK)
            .infantrySpeed(DEFAULT_INFANTRY_SPEED)
            .cavalrySpeed(DEFAULT_CAVALRY_SPEED)
            .cityBuildingSpeed(DEFAULT_CITY_BUILDING_SPEED)
            .recruitmentSpeed(DEFAULT_RECRUITMENT_SPEED)
            .scouting(DEFAULT_SCOUTING);
        return researchesEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ResearchesEntity createUpdatedEntity(EntityManager em) {
        ResearchesEntity researchesEntity = new ResearchesEntity()
            .infantryVitality(UPDATED_INFANTRY_VITALITY)
            .cavalryVitality(UPDATED_CAVALRY_VITALITY)
            .lightArmorValue(UPDATED_LIGHT_ARMOR_VALUE)
            .heavyArmorValue(UPDATED_HEAVY_ARMOR_VALUE)
            .meleeAttack(UPDATED_MELEE_ATTACK)
            .rangedAttack(UPDATED_RANGED_ATTACK)
            .infantrySpeed(UPDATED_INFANTRY_SPEED)
            .cavalrySpeed(UPDATED_CAVALRY_SPEED)
            .cityBuildingSpeed(UPDATED_CITY_BUILDING_SPEED)
            .recruitmentSpeed(UPDATED_RECRUITMENT_SPEED)
            .scouting(UPDATED_SCOUTING);
        return researchesEntity;
    }

    @BeforeEach
    public void initTest() {
        researchesEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createResearchesEntity() throws Exception {
        int databaseSizeBeforeCreate = researchesEntityRepository.findAll().size();
        // Create the ResearchesEntity
        restResearchesEntityMockMvc.perform(post("/api/researches-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(researchesEntity)))
            .andExpect(status().isCreated());

        // Validate the ResearchesEntity in the database
        List<ResearchesEntity> researchesEntityList = researchesEntityRepository.findAll();
        assertThat(researchesEntityList).hasSize(databaseSizeBeforeCreate + 1);
        ResearchesEntity testResearchesEntity = researchesEntityList.get(researchesEntityList.size() - 1);
        assertThat(testResearchesEntity.getInfantryVitality()).isEqualTo(DEFAULT_INFANTRY_VITALITY);
        assertThat(testResearchesEntity.getCavalryVitality()).isEqualTo(DEFAULT_CAVALRY_VITALITY);
        assertThat(testResearchesEntity.getLightArmorValue()).isEqualTo(DEFAULT_LIGHT_ARMOR_VALUE);
        assertThat(testResearchesEntity.getHeavyArmorValue()).isEqualTo(DEFAULT_HEAVY_ARMOR_VALUE);
        assertThat(testResearchesEntity.getMeleeAttack()).isEqualTo(DEFAULT_MELEE_ATTACK);
        assertThat(testResearchesEntity.getRangedAttack()).isEqualTo(DEFAULT_RANGED_ATTACK);
        assertThat(testResearchesEntity.getInfantrySpeed()).isEqualTo(DEFAULT_INFANTRY_SPEED);
        assertThat(testResearchesEntity.getCavalrySpeed()).isEqualTo(DEFAULT_CAVALRY_SPEED);
        assertThat(testResearchesEntity.getCityBuildingSpeed()).isEqualTo(DEFAULT_CITY_BUILDING_SPEED);
        assertThat(testResearchesEntity.getRecruitmentSpeed()).isEqualTo(DEFAULT_RECRUITMENT_SPEED);
        assertThat(testResearchesEntity.getScouting()).isEqualTo(DEFAULT_SCOUTING);
    }

    @Test
    @Transactional
    public void createResearchesEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = researchesEntityRepository.findAll().size();

        // Create the ResearchesEntity with an existing ID
        researchesEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResearchesEntityMockMvc.perform(post("/api/researches-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(researchesEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ResearchesEntity in the database
        List<ResearchesEntity> researchesEntityList = researchesEntityRepository.findAll();
        assertThat(researchesEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllResearchesEntities() throws Exception {
        // Initialize the database
        researchesEntityRepository.saveAndFlush(researchesEntity);

        // Get all the researchesEntityList
        restResearchesEntityMockMvc.perform(get("/api/researches-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(researchesEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].infantryVitality").value(hasItem(DEFAULT_INFANTRY_VITALITY)))
            .andExpect(jsonPath("$.[*].cavalryVitality").value(hasItem(DEFAULT_CAVALRY_VITALITY)))
            .andExpect(jsonPath("$.[*].lightArmorValue").value(hasItem(DEFAULT_LIGHT_ARMOR_VALUE)))
            .andExpect(jsonPath("$.[*].heavyArmorValue").value(hasItem(DEFAULT_HEAVY_ARMOR_VALUE)))
            .andExpect(jsonPath("$.[*].meleeAttack").value(hasItem(DEFAULT_MELEE_ATTACK)))
            .andExpect(jsonPath("$.[*].rangedAttack").value(hasItem(DEFAULT_RANGED_ATTACK)))
            .andExpect(jsonPath("$.[*].infantrySpeed").value(hasItem(DEFAULT_INFANTRY_SPEED)))
            .andExpect(jsonPath("$.[*].cavalrySpeed").value(hasItem(DEFAULT_CAVALRY_SPEED)))
            .andExpect(jsonPath("$.[*].cityBuildingSpeed").value(hasItem(DEFAULT_CITY_BUILDING_SPEED)))
            .andExpect(jsonPath("$.[*].recruitmentSpeed").value(hasItem(DEFAULT_RECRUITMENT_SPEED)))
            .andExpect(jsonPath("$.[*].scouting").value(hasItem(DEFAULT_SCOUTING)));
    }
    
    @Test
    @Transactional
    public void getResearchesEntity() throws Exception {
        // Initialize the database
        researchesEntityRepository.saveAndFlush(researchesEntity);

        // Get the researchesEntity
        restResearchesEntityMockMvc.perform(get("/api/researches-entities/{id}", researchesEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(researchesEntity.getId().intValue()))
            .andExpect(jsonPath("$.infantryVitality").value(DEFAULT_INFANTRY_VITALITY))
            .andExpect(jsonPath("$.cavalryVitality").value(DEFAULT_CAVALRY_VITALITY))
            .andExpect(jsonPath("$.lightArmorValue").value(DEFAULT_LIGHT_ARMOR_VALUE))
            .andExpect(jsonPath("$.heavyArmorValue").value(DEFAULT_HEAVY_ARMOR_VALUE))
            .andExpect(jsonPath("$.meleeAttack").value(DEFAULT_MELEE_ATTACK))
            .andExpect(jsonPath("$.rangedAttack").value(DEFAULT_RANGED_ATTACK))
            .andExpect(jsonPath("$.infantrySpeed").value(DEFAULT_INFANTRY_SPEED))
            .andExpect(jsonPath("$.cavalrySpeed").value(DEFAULT_CAVALRY_SPEED))
            .andExpect(jsonPath("$.cityBuildingSpeed").value(DEFAULT_CITY_BUILDING_SPEED))
            .andExpect(jsonPath("$.recruitmentSpeed").value(DEFAULT_RECRUITMENT_SPEED))
            .andExpect(jsonPath("$.scouting").value(DEFAULT_SCOUTING));
    }
    @Test
    @Transactional
    public void getNonExistingResearchesEntity() throws Exception {
        // Get the researchesEntity
        restResearchesEntityMockMvc.perform(get("/api/researches-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResearchesEntity() throws Exception {
        // Initialize the database
        researchesEntityRepository.saveAndFlush(researchesEntity);

        int databaseSizeBeforeUpdate = researchesEntityRepository.findAll().size();

        // Update the researchesEntity
        ResearchesEntity updatedResearchesEntity = researchesEntityRepository.findById(researchesEntity.getId()).get();
        // Disconnect from session so that the updates on updatedResearchesEntity are not directly saved in db
        em.detach(updatedResearchesEntity);
        updatedResearchesEntity
            .infantryVitality(UPDATED_INFANTRY_VITALITY)
            .cavalryVitality(UPDATED_CAVALRY_VITALITY)
            .lightArmorValue(UPDATED_LIGHT_ARMOR_VALUE)
            .heavyArmorValue(UPDATED_HEAVY_ARMOR_VALUE)
            .meleeAttack(UPDATED_MELEE_ATTACK)
            .rangedAttack(UPDATED_RANGED_ATTACK)
            .infantrySpeed(UPDATED_INFANTRY_SPEED)
            .cavalrySpeed(UPDATED_CAVALRY_SPEED)
            .cityBuildingSpeed(UPDATED_CITY_BUILDING_SPEED)
            .recruitmentSpeed(UPDATED_RECRUITMENT_SPEED)
            .scouting(UPDATED_SCOUTING);

        restResearchesEntityMockMvc.perform(put("/api/researches-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedResearchesEntity)))
            .andExpect(status().isOk());

        // Validate the ResearchesEntity in the database
        List<ResearchesEntity> researchesEntityList = researchesEntityRepository.findAll();
        assertThat(researchesEntityList).hasSize(databaseSizeBeforeUpdate);
        ResearchesEntity testResearchesEntity = researchesEntityList.get(researchesEntityList.size() - 1);
        assertThat(testResearchesEntity.getInfantryVitality()).isEqualTo(UPDATED_INFANTRY_VITALITY);
        assertThat(testResearchesEntity.getCavalryVitality()).isEqualTo(UPDATED_CAVALRY_VITALITY);
        assertThat(testResearchesEntity.getLightArmorValue()).isEqualTo(UPDATED_LIGHT_ARMOR_VALUE);
        assertThat(testResearchesEntity.getHeavyArmorValue()).isEqualTo(UPDATED_HEAVY_ARMOR_VALUE);
        assertThat(testResearchesEntity.getMeleeAttack()).isEqualTo(UPDATED_MELEE_ATTACK);
        assertThat(testResearchesEntity.getRangedAttack()).isEqualTo(UPDATED_RANGED_ATTACK);
        assertThat(testResearchesEntity.getInfantrySpeed()).isEqualTo(UPDATED_INFANTRY_SPEED);
        assertThat(testResearchesEntity.getCavalrySpeed()).isEqualTo(UPDATED_CAVALRY_SPEED);
        assertThat(testResearchesEntity.getCityBuildingSpeed()).isEqualTo(UPDATED_CITY_BUILDING_SPEED);
        assertThat(testResearchesEntity.getRecruitmentSpeed()).isEqualTo(UPDATED_RECRUITMENT_SPEED);
        assertThat(testResearchesEntity.getScouting()).isEqualTo(UPDATED_SCOUTING);
    }

    @Test
    @Transactional
    public void updateNonExistingResearchesEntity() throws Exception {
        int databaseSizeBeforeUpdate = researchesEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restResearchesEntityMockMvc.perform(put("/api/researches-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(researchesEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ResearchesEntity in the database
        List<ResearchesEntity> researchesEntityList = researchesEntityRepository.findAll();
        assertThat(researchesEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteResearchesEntity() throws Exception {
        // Initialize the database
        researchesEntityRepository.saveAndFlush(researchesEntity);

        int databaseSizeBeforeDelete = researchesEntityRepository.findAll().size();

        // Delete the researchesEntity
        restResearchesEntityMockMvc.perform(delete("/api/researches-entities/{id}", researchesEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ResearchesEntity> researchesEntityList = researchesEntityRepository.findAll();
        assertThat(researchesEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
