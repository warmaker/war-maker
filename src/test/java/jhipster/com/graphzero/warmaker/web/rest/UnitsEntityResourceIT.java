package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.UnitsEntity;
import com.graphzero.warmaker.repository.UnitsEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UnitsEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class UnitsEntityResourceIT {

    private static final Integer DEFAULT_SPEARMEN = 1;
    private static final Integer UPDATED_SPEARMEN = 2;

    private static final Integer DEFAULT_SWORDSMEN = 1;
    private static final Integer UPDATED_SWORDSMEN = 2;

    private static final Integer DEFAULT_ARCHERS = 1;
    private static final Integer UPDATED_ARCHERS = 2;

    private static final Integer DEFAULT_CROSSBOWMEN = 1;
    private static final Integer UPDATED_CROSSBOWMEN = 2;

    @Autowired
    private UnitsEntityRepository unitsEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUnitsEntityMockMvc;

    private UnitsEntity unitsEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UnitsEntity createEntity(EntityManager em) {
        UnitsEntity unitsEntity = new UnitsEntity()
            .spearmen(DEFAULT_SPEARMEN)
            .swordsmen(DEFAULT_SWORDSMEN)
            .archers(DEFAULT_ARCHERS)
            .crossbowmen(DEFAULT_CROSSBOWMEN);
        return unitsEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UnitsEntity createUpdatedEntity(EntityManager em) {
        UnitsEntity unitsEntity = new UnitsEntity()
            .spearmen(UPDATED_SPEARMEN)
            .swordsmen(UPDATED_SWORDSMEN)
            .archers(UPDATED_ARCHERS)
            .crossbowmen(UPDATED_CROSSBOWMEN);
        return unitsEntity;
    }

    @BeforeEach
    public void initTest() {
        unitsEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createUnitsEntity() throws Exception {
        int databaseSizeBeforeCreate = unitsEntityRepository.findAll().size();
        // Create the UnitsEntity
        restUnitsEntityMockMvc.perform(post("/api/units-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(unitsEntity)))
            .andExpect(status().isCreated());

        // Validate the UnitsEntity in the database
        List<UnitsEntity> unitsEntityList = unitsEntityRepository.findAll();
        assertThat(unitsEntityList).hasSize(databaseSizeBeforeCreate + 1);
        UnitsEntity testUnitsEntity = unitsEntityList.get(unitsEntityList.size() - 1);
        assertThat(testUnitsEntity.getSpearmen()).isEqualTo(DEFAULT_SPEARMEN);
        assertThat(testUnitsEntity.getSwordsmen()).isEqualTo(DEFAULT_SWORDSMEN);
        assertThat(testUnitsEntity.getArchers()).isEqualTo(DEFAULT_ARCHERS);
        assertThat(testUnitsEntity.getCrossbowmen()).isEqualTo(DEFAULT_CROSSBOWMEN);
    }

    @Test
    @Transactional
    public void createUnitsEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = unitsEntityRepository.findAll().size();

        // Create the UnitsEntity with an existing ID
        unitsEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUnitsEntityMockMvc.perform(post("/api/units-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(unitsEntity)))
            .andExpect(status().isBadRequest());

        // Validate the UnitsEntity in the database
        List<UnitsEntity> unitsEntityList = unitsEntityRepository.findAll();
        assertThat(unitsEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUnitsEntities() throws Exception {
        // Initialize the database
        unitsEntityRepository.saveAndFlush(unitsEntity);

        // Get all the unitsEntityList
        restUnitsEntityMockMvc.perform(get("/api/units-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(unitsEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].spearmen").value(hasItem(DEFAULT_SPEARMEN)))
            .andExpect(jsonPath("$.[*].swordsmen").value(hasItem(DEFAULT_SWORDSMEN)))
            .andExpect(jsonPath("$.[*].archers").value(hasItem(DEFAULT_ARCHERS)))
            .andExpect(jsonPath("$.[*].crossbowmen").value(hasItem(DEFAULT_CROSSBOWMEN)));
    }
    
    @Test
    @Transactional
    public void getUnitsEntity() throws Exception {
        // Initialize the database
        unitsEntityRepository.saveAndFlush(unitsEntity);

        // Get the unitsEntity
        restUnitsEntityMockMvc.perform(get("/api/units-entities/{id}", unitsEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(unitsEntity.getId().intValue()))
            .andExpect(jsonPath("$.spearmen").value(DEFAULT_SPEARMEN))
            .andExpect(jsonPath("$.swordsmen").value(DEFAULT_SWORDSMEN))
            .andExpect(jsonPath("$.archers").value(DEFAULT_ARCHERS))
            .andExpect(jsonPath("$.crossbowmen").value(DEFAULT_CROSSBOWMEN));
    }
    @Test
    @Transactional
    public void getNonExistingUnitsEntity() throws Exception {
        // Get the unitsEntity
        restUnitsEntityMockMvc.perform(get("/api/units-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUnitsEntity() throws Exception {
        // Initialize the database
        unitsEntityRepository.saveAndFlush(unitsEntity);

        int databaseSizeBeforeUpdate = unitsEntityRepository.findAll().size();

        // Update the unitsEntity
        UnitsEntity updatedUnitsEntity = unitsEntityRepository.findById(unitsEntity.getId()).get();
        // Disconnect from session so that the updates on updatedUnitsEntity are not directly saved in db
        em.detach(updatedUnitsEntity);
        updatedUnitsEntity
            .spearmen(UPDATED_SPEARMEN)
            .swordsmen(UPDATED_SWORDSMEN)
            .archers(UPDATED_ARCHERS)
            .crossbowmen(UPDATED_CROSSBOWMEN);

        restUnitsEntityMockMvc.perform(put("/api/units-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedUnitsEntity)))
            .andExpect(status().isOk());

        // Validate the UnitsEntity in the database
        List<UnitsEntity> unitsEntityList = unitsEntityRepository.findAll();
        assertThat(unitsEntityList).hasSize(databaseSizeBeforeUpdate);
        UnitsEntity testUnitsEntity = unitsEntityList.get(unitsEntityList.size() - 1);
        assertThat(testUnitsEntity.getSpearmen()).isEqualTo(UPDATED_SPEARMEN);
        assertThat(testUnitsEntity.getSwordsmen()).isEqualTo(UPDATED_SWORDSMEN);
        assertThat(testUnitsEntity.getArchers()).isEqualTo(UPDATED_ARCHERS);
        assertThat(testUnitsEntity.getCrossbowmen()).isEqualTo(UPDATED_CROSSBOWMEN);
    }

    @Test
    @Transactional
    public void updateNonExistingUnitsEntity() throws Exception {
        int databaseSizeBeforeUpdate = unitsEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUnitsEntityMockMvc.perform(put("/api/units-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(unitsEntity)))
            .andExpect(status().isBadRequest());

        // Validate the UnitsEntity in the database
        List<UnitsEntity> unitsEntityList = unitsEntityRepository.findAll();
        assertThat(unitsEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUnitsEntity() throws Exception {
        // Initialize the database
        unitsEntityRepository.saveAndFlush(unitsEntity);

        int databaseSizeBeforeDelete = unitsEntityRepository.findAll().size();

        // Delete the unitsEntity
        restUnitsEntityMockMvc.perform(delete("/api/units-entities/{id}", unitsEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UnitsEntity> unitsEntityList = unitsEntityRepository.findAll();
        assertThat(unitsEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
