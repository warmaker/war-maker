package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.CityEntity;
import com.graphzero.warmaker.domain.FieldEntity;
import com.graphzero.warmaker.repository.CityEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.graphzero.warmaker.domain.enumeration.CityType;
/**
 * Integration tests for the {@link CityEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CityEntityResourceIT {

    private static final String DEFAULT_CITY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CITY_NAME = "BBBBBBBBBB";

    private static final CityType DEFAULT_CITY_TYPE = CityType.PLAYER;
    private static final CityType UPDATED_CITY_TYPE = CityType.RENEGATES;

    private static final Double DEFAULT_RECRUITMENT_MODIFIER = 1D;
    private static final Double UPDATED_RECRUITMENT_MODIFIER = 2D;

    private static final Double DEFAULT_BUILD_SPEED_MODIFIER = 1D;
    private static final Double UPDATED_BUILD_SPEED_MODIFIER = 2D;

    @Autowired
    private CityEntityRepository cityEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCityEntityMockMvc;

    private CityEntity cityEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CityEntity createEntity(EntityManager em) {
        CityEntity cityEntity = new CityEntity()
            .cityName(DEFAULT_CITY_NAME)
            .cityType(DEFAULT_CITY_TYPE)
            .recruitmentModifier(DEFAULT_RECRUITMENT_MODIFIER)
            .buildSpeedModifier(DEFAULT_BUILD_SPEED_MODIFIER);
        // Add required entity
        FieldEntity fieldEntity;
        if (TestUtil.findAll(em, FieldEntity.class).isEmpty()) {
            fieldEntity = FieldEntityResourceIT.createEntity(em);
            em.persist(fieldEntity);
            em.flush();
        } else {
            fieldEntity = TestUtil.findAll(em, FieldEntity.class).get(0);
        }
        cityEntity.setLocation(fieldEntity);
        return cityEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CityEntity createUpdatedEntity(EntityManager em) {
        CityEntity cityEntity = new CityEntity()
            .cityName(UPDATED_CITY_NAME)
            .cityType(UPDATED_CITY_TYPE)
            .recruitmentModifier(UPDATED_RECRUITMENT_MODIFIER)
            .buildSpeedModifier(UPDATED_BUILD_SPEED_MODIFIER);
        // Add required entity
        FieldEntity fieldEntity;
        if (TestUtil.findAll(em, FieldEntity.class).isEmpty()) {
            fieldEntity = FieldEntityResourceIT.createUpdatedEntity(em);
            em.persist(fieldEntity);
            em.flush();
        } else {
            fieldEntity = TestUtil.findAll(em, FieldEntity.class).get(0);
        }
        cityEntity.setLocation(fieldEntity);
        return cityEntity;
    }

    @BeforeEach
    public void initTest() {
        cityEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createCityEntity() throws Exception {
        int databaseSizeBeforeCreate = cityEntityRepository.findAll().size();
        // Create the CityEntity
        restCityEntityMockMvc.perform(post("/api/city-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cityEntity)))
            .andExpect(status().isCreated());

        // Validate the CityEntity in the database
        List<CityEntity> cityEntityList = cityEntityRepository.findAll();
        assertThat(cityEntityList).hasSize(databaseSizeBeforeCreate + 1);
        CityEntity testCityEntity = cityEntityList.get(cityEntityList.size() - 1);
        assertThat(testCityEntity.getCityName()).isEqualTo(DEFAULT_CITY_NAME);
        assertThat(testCityEntity.getCityType()).isEqualTo(DEFAULT_CITY_TYPE);
        assertThat(testCityEntity.getRecruitmentModifier()).isEqualTo(DEFAULT_RECRUITMENT_MODIFIER);
        assertThat(testCityEntity.getBuildSpeedModifier()).isEqualTo(DEFAULT_BUILD_SPEED_MODIFIER);
    }

    @Test
    @Transactional
    public void createCityEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cityEntityRepository.findAll().size();

        // Create the CityEntity with an existing ID
        cityEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCityEntityMockMvc.perform(post("/api/city-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cityEntity)))
            .andExpect(status().isBadRequest());

        // Validate the CityEntity in the database
        List<CityEntity> cityEntityList = cityEntityRepository.findAll();
        assertThat(cityEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCityEntities() throws Exception {
        // Initialize the database
        cityEntityRepository.saveAndFlush(cityEntity);

        // Get all the cityEntityList
        restCityEntityMockMvc.perform(get("/api/city-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cityEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].cityName").value(hasItem(DEFAULT_CITY_NAME)))
            .andExpect(jsonPath("$.[*].cityType").value(hasItem(DEFAULT_CITY_TYPE.toString())))
            .andExpect(jsonPath("$.[*].recruitmentModifier").value(hasItem(DEFAULT_RECRUITMENT_MODIFIER.doubleValue())))
            .andExpect(jsonPath("$.[*].buildSpeedModifier").value(hasItem(DEFAULT_BUILD_SPEED_MODIFIER.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getCityEntity() throws Exception {
        // Initialize the database
        cityEntityRepository.saveAndFlush(cityEntity);

        // Get the cityEntity
        restCityEntityMockMvc.perform(get("/api/city-entities/{id}", cityEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(cityEntity.getId().intValue()))
            .andExpect(jsonPath("$.cityName").value(DEFAULT_CITY_NAME))
            .andExpect(jsonPath("$.cityType").value(DEFAULT_CITY_TYPE.toString()))
            .andExpect(jsonPath("$.recruitmentModifier").value(DEFAULT_RECRUITMENT_MODIFIER.doubleValue()))
            .andExpect(jsonPath("$.buildSpeedModifier").value(DEFAULT_BUILD_SPEED_MODIFIER.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingCityEntity() throws Exception {
        // Get the cityEntity
        restCityEntityMockMvc.perform(get("/api/city-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCityEntity() throws Exception {
        // Initialize the database
        cityEntityRepository.saveAndFlush(cityEntity);

        int databaseSizeBeforeUpdate = cityEntityRepository.findAll().size();

        // Update the cityEntity
        CityEntity updatedCityEntity = cityEntityRepository.findById(cityEntity.getId()).get();
        // Disconnect from session so that the updates on updatedCityEntity are not directly saved in db
        em.detach(updatedCityEntity);
        updatedCityEntity
            .cityName(UPDATED_CITY_NAME)
            .cityType(UPDATED_CITY_TYPE)
            .recruitmentModifier(UPDATED_RECRUITMENT_MODIFIER)
            .buildSpeedModifier(UPDATED_BUILD_SPEED_MODIFIER);

        restCityEntityMockMvc.perform(put("/api/city-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCityEntity)))
            .andExpect(status().isOk());

        // Validate the CityEntity in the database
        List<CityEntity> cityEntityList = cityEntityRepository.findAll();
        assertThat(cityEntityList).hasSize(databaseSizeBeforeUpdate);
        CityEntity testCityEntity = cityEntityList.get(cityEntityList.size() - 1);
        assertThat(testCityEntity.getCityName()).isEqualTo(UPDATED_CITY_NAME);
        assertThat(testCityEntity.getCityType()).isEqualTo(UPDATED_CITY_TYPE);
        assertThat(testCityEntity.getRecruitmentModifier()).isEqualTo(UPDATED_RECRUITMENT_MODIFIER);
        assertThat(testCityEntity.getBuildSpeedModifier()).isEqualTo(UPDATED_BUILD_SPEED_MODIFIER);
    }

    @Test
    @Transactional
    public void updateNonExistingCityEntity() throws Exception {
        int databaseSizeBeforeUpdate = cityEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCityEntityMockMvc.perform(put("/api/city-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cityEntity)))
            .andExpect(status().isBadRequest());

        // Validate the CityEntity in the database
        List<CityEntity> cityEntityList = cityEntityRepository.findAll();
        assertThat(cityEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCityEntity() throws Exception {
        // Initialize the database
        cityEntityRepository.saveAndFlush(cityEntity);

        int databaseSizeBeforeDelete = cityEntityRepository.findAll().size();

        // Delete the cityEntity
        restCityEntityMockMvc.perform(delete("/api/city-entities/{id}", cityEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CityEntity> cityEntityList = cityEntityRepository.findAll();
        assertThat(cityEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
