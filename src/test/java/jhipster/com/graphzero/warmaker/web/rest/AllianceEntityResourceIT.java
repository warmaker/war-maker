package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.AllianceEntity;
import com.graphzero.warmaker.repository.AllianceEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AllianceEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AllianceEntityResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private AllianceEntityRepository allianceEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAllianceEntityMockMvc;

    private AllianceEntity allianceEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AllianceEntity createEntity(EntityManager em) {
        AllianceEntity allianceEntity = new AllianceEntity()
            .name(DEFAULT_NAME);
        return allianceEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AllianceEntity createUpdatedEntity(EntityManager em) {
        AllianceEntity allianceEntity = new AllianceEntity()
            .name(UPDATED_NAME);
        return allianceEntity;
    }

    @BeforeEach
    public void initTest() {
        allianceEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createAllianceEntity() throws Exception {
        int databaseSizeBeforeCreate = allianceEntityRepository.findAll().size();
        // Create the AllianceEntity
        restAllianceEntityMockMvc.perform(post("/api/alliance-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(allianceEntity)))
            .andExpect(status().isCreated());

        // Validate the AllianceEntity in the database
        List<AllianceEntity> allianceEntityList = allianceEntityRepository.findAll();
        assertThat(allianceEntityList).hasSize(databaseSizeBeforeCreate + 1);
        AllianceEntity testAllianceEntity = allianceEntityList.get(allianceEntityList.size() - 1);
        assertThat(testAllianceEntity.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createAllianceEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = allianceEntityRepository.findAll().size();

        // Create the AllianceEntity with an existing ID
        allianceEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAllianceEntityMockMvc.perform(post("/api/alliance-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(allianceEntity)))
            .andExpect(status().isBadRequest());

        // Validate the AllianceEntity in the database
        List<AllianceEntity> allianceEntityList = allianceEntityRepository.findAll();
        assertThat(allianceEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAllianceEntities() throws Exception {
        // Initialize the database
        allianceEntityRepository.saveAndFlush(allianceEntity);

        // Get all the allianceEntityList
        restAllianceEntityMockMvc.perform(get("/api/alliance-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(allianceEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getAllianceEntity() throws Exception {
        // Initialize the database
        allianceEntityRepository.saveAndFlush(allianceEntity);

        // Get the allianceEntity
        restAllianceEntityMockMvc.perform(get("/api/alliance-entities/{id}", allianceEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(allianceEntity.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingAllianceEntity() throws Exception {
        // Get the allianceEntity
        restAllianceEntityMockMvc.perform(get("/api/alliance-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAllianceEntity() throws Exception {
        // Initialize the database
        allianceEntityRepository.saveAndFlush(allianceEntity);

        int databaseSizeBeforeUpdate = allianceEntityRepository.findAll().size();

        // Update the allianceEntity
        AllianceEntity updatedAllianceEntity = allianceEntityRepository.findById(allianceEntity.getId()).get();
        // Disconnect from session so that the updates on updatedAllianceEntity are not directly saved in db
        em.detach(updatedAllianceEntity);
        updatedAllianceEntity
            .name(UPDATED_NAME);

        restAllianceEntityMockMvc.perform(put("/api/alliance-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAllianceEntity)))
            .andExpect(status().isOk());

        // Validate the AllianceEntity in the database
        List<AllianceEntity> allianceEntityList = allianceEntityRepository.findAll();
        assertThat(allianceEntityList).hasSize(databaseSizeBeforeUpdate);
        AllianceEntity testAllianceEntity = allianceEntityList.get(allianceEntityList.size() - 1);
        assertThat(testAllianceEntity.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingAllianceEntity() throws Exception {
        int databaseSizeBeforeUpdate = allianceEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAllianceEntityMockMvc.perform(put("/api/alliance-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(allianceEntity)))
            .andExpect(status().isBadRequest());

        // Validate the AllianceEntity in the database
        List<AllianceEntity> allianceEntityList = allianceEntityRepository.findAll();
        assertThat(allianceEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAllianceEntity() throws Exception {
        // Initialize the database
        allianceEntityRepository.saveAndFlush(allianceEntity);

        int databaseSizeBeforeDelete = allianceEntityRepository.findAll().size();

        // Delete the allianceEntity
        restAllianceEntityMockMvc.perform(delete("/api/alliance-entities/{id}", allianceEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AllianceEntity> allianceEntityList = allianceEntityRepository.findAll();
        assertThat(allianceEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
