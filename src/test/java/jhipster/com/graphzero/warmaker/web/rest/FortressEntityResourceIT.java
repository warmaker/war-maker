package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.FortressEntity;
import com.graphzero.warmaker.repository.FortressEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FortressEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FortressEntityResourceIT {

    private static final String DEFAULT_FORTRESS_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FORTRESS_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_WAR_BARRACKS = 1;
    private static final Integer UPDATED_WAR_BARRACKS = 2;

    private static final Integer DEFAULT_WALL = 1;
    private static final Integer UPDATED_WALL = 2;

    private static final Integer DEFAULT_MOAT = 1;
    private static final Integer UPDATED_MOAT = 2;

    @Autowired
    private FortressEntityRepository fortressEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFortressEntityMockMvc;

    private FortressEntity fortressEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FortressEntity createEntity(EntityManager em) {
        FortressEntity fortressEntity = new FortressEntity()
            .fortressName(DEFAULT_FORTRESS_NAME)
            .warBarracks(DEFAULT_WAR_BARRACKS)
            .wall(DEFAULT_WALL)
            .moat(DEFAULT_MOAT);
        return fortressEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FortressEntity createUpdatedEntity(EntityManager em) {
        FortressEntity fortressEntity = new FortressEntity()
            .fortressName(UPDATED_FORTRESS_NAME)
            .warBarracks(UPDATED_WAR_BARRACKS)
            .wall(UPDATED_WALL)
            .moat(UPDATED_MOAT);
        return fortressEntity;
    }

    @BeforeEach
    public void initTest() {
        fortressEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createFortressEntity() throws Exception {
        int databaseSizeBeforeCreate = fortressEntityRepository.findAll().size();
        // Create the FortressEntity
        restFortressEntityMockMvc.perform(post("/api/fortress-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fortressEntity)))
            .andExpect(status().isCreated());

        // Validate the FortressEntity in the database
        List<FortressEntity> fortressEntityList = fortressEntityRepository.findAll();
        assertThat(fortressEntityList).hasSize(databaseSizeBeforeCreate + 1);
        FortressEntity testFortressEntity = fortressEntityList.get(fortressEntityList.size() - 1);
        assertThat(testFortressEntity.getFortressName()).isEqualTo(DEFAULT_FORTRESS_NAME);
        assertThat(testFortressEntity.getWarBarracks()).isEqualTo(DEFAULT_WAR_BARRACKS);
        assertThat(testFortressEntity.getWall()).isEqualTo(DEFAULT_WALL);
        assertThat(testFortressEntity.getMoat()).isEqualTo(DEFAULT_MOAT);
    }

    @Test
    @Transactional
    public void createFortressEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fortressEntityRepository.findAll().size();

        // Create the FortressEntity with an existing ID
        fortressEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFortressEntityMockMvc.perform(post("/api/fortress-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fortressEntity)))
            .andExpect(status().isBadRequest());

        // Validate the FortressEntity in the database
        List<FortressEntity> fortressEntityList = fortressEntityRepository.findAll();
        assertThat(fortressEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFortressEntities() throws Exception {
        // Initialize the database
        fortressEntityRepository.saveAndFlush(fortressEntity);

        // Get all the fortressEntityList
        restFortressEntityMockMvc.perform(get("/api/fortress-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fortressEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].fortressName").value(hasItem(DEFAULT_FORTRESS_NAME)))
            .andExpect(jsonPath("$.[*].warBarracks").value(hasItem(DEFAULT_WAR_BARRACKS)))
            .andExpect(jsonPath("$.[*].wall").value(hasItem(DEFAULT_WALL)))
            .andExpect(jsonPath("$.[*].moat").value(hasItem(DEFAULT_MOAT)));
    }
    
    @Test
    @Transactional
    public void getFortressEntity() throws Exception {
        // Initialize the database
        fortressEntityRepository.saveAndFlush(fortressEntity);

        // Get the fortressEntity
        restFortressEntityMockMvc.perform(get("/api/fortress-entities/{id}", fortressEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fortressEntity.getId().intValue()))
            .andExpect(jsonPath("$.fortressName").value(DEFAULT_FORTRESS_NAME))
            .andExpect(jsonPath("$.warBarracks").value(DEFAULT_WAR_BARRACKS))
            .andExpect(jsonPath("$.wall").value(DEFAULT_WALL))
            .andExpect(jsonPath("$.moat").value(DEFAULT_MOAT));
    }
    @Test
    @Transactional
    public void getNonExistingFortressEntity() throws Exception {
        // Get the fortressEntity
        restFortressEntityMockMvc.perform(get("/api/fortress-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFortressEntity() throws Exception {
        // Initialize the database
        fortressEntityRepository.saveAndFlush(fortressEntity);

        int databaseSizeBeforeUpdate = fortressEntityRepository.findAll().size();

        // Update the fortressEntity
        FortressEntity updatedFortressEntity = fortressEntityRepository.findById(fortressEntity.getId()).get();
        // Disconnect from session so that the updates on updatedFortressEntity are not directly saved in db
        em.detach(updatedFortressEntity);
        updatedFortressEntity
            .fortressName(UPDATED_FORTRESS_NAME)
            .warBarracks(UPDATED_WAR_BARRACKS)
            .wall(UPDATED_WALL)
            .moat(UPDATED_MOAT);

        restFortressEntityMockMvc.perform(put("/api/fortress-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFortressEntity)))
            .andExpect(status().isOk());

        // Validate the FortressEntity in the database
        List<FortressEntity> fortressEntityList = fortressEntityRepository.findAll();
        assertThat(fortressEntityList).hasSize(databaseSizeBeforeUpdate);
        FortressEntity testFortressEntity = fortressEntityList.get(fortressEntityList.size() - 1);
        assertThat(testFortressEntity.getFortressName()).isEqualTo(UPDATED_FORTRESS_NAME);
        assertThat(testFortressEntity.getWarBarracks()).isEqualTo(UPDATED_WAR_BARRACKS);
        assertThat(testFortressEntity.getWall()).isEqualTo(UPDATED_WALL);
        assertThat(testFortressEntity.getMoat()).isEqualTo(UPDATED_MOAT);
    }

    @Test
    @Transactional
    public void updateNonExistingFortressEntity() throws Exception {
        int databaseSizeBeforeUpdate = fortressEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFortressEntityMockMvc.perform(put("/api/fortress-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fortressEntity)))
            .andExpect(status().isBadRequest());

        // Validate the FortressEntity in the database
        List<FortressEntity> fortressEntityList = fortressEntityRepository.findAll();
        assertThat(fortressEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFortressEntity() throws Exception {
        // Initialize the database
        fortressEntityRepository.saveAndFlush(fortressEntity);

        int databaseSizeBeforeDelete = fortressEntityRepository.findAll().size();

        // Delete the fortressEntity
        restFortressEntityMockMvc.perform(delete("/api/fortress-entities/{id}", fortressEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FortressEntity> fortressEntityList = fortressEntityRepository.findAll();
        assertThat(fortressEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
