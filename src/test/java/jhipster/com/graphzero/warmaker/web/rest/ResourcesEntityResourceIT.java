package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.ResourcesEntity;
import com.graphzero.warmaker.repository.ResourcesEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.graphzero.warmaker.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ResourcesEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ResourcesEntityResourceIT {

    private static final Double DEFAULT_DENARS = 1D;
    private static final Double UPDATED_DENARS = 2D;

    private static final Double DEFAULT_WOOD = 1D;
    private static final Double UPDATED_WOOD = 2D;

    private static final Double DEFAULT_STONE = 1D;
    private static final Double UPDATED_STONE = 2D;

    private static final Double DEFAULT_STEEL = 1D;
    private static final Double UPDATED_STEEL = 2D;

    private static final Double DEFAULT_FOOD = 1D;
    private static final Double UPDATED_FOOD = 2D;

    private static final Double DEFAULT_FAITH = 1D;
    private static final Double UPDATED_FAITH = 2D;

    private static final ZonedDateTime DEFAULT_CHANGE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CHANGE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private ResourcesEntityRepository resourcesEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restResourcesEntityMockMvc;

    private ResourcesEntity resourcesEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ResourcesEntity createEntity(EntityManager em) {
        ResourcesEntity resourcesEntity = new ResourcesEntity()
            .denars(DEFAULT_DENARS)
            .wood(DEFAULT_WOOD)
            .stone(DEFAULT_STONE)
            .steel(DEFAULT_STEEL)
            .food(DEFAULT_FOOD)
            .faith(DEFAULT_FAITH)
            .changeDate(DEFAULT_CHANGE_DATE);
        return resourcesEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ResourcesEntity createUpdatedEntity(EntityManager em) {
        ResourcesEntity resourcesEntity = new ResourcesEntity()
            .denars(UPDATED_DENARS)
            .wood(UPDATED_WOOD)
            .stone(UPDATED_STONE)
            .steel(UPDATED_STEEL)
            .food(UPDATED_FOOD)
            .faith(UPDATED_FAITH)
            .changeDate(UPDATED_CHANGE_DATE);
        return resourcesEntity;
    }

    @BeforeEach
    public void initTest() {
        resourcesEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createResourcesEntity() throws Exception {
        int databaseSizeBeforeCreate = resourcesEntityRepository.findAll().size();
        // Create the ResourcesEntity
        restResourcesEntityMockMvc.perform(post("/api/resources-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(resourcesEntity)))
            .andExpect(status().isCreated());

        // Validate the ResourcesEntity in the database
        List<ResourcesEntity> resourcesEntityList = resourcesEntityRepository.findAll();
        assertThat(resourcesEntityList).hasSize(databaseSizeBeforeCreate + 1);
        ResourcesEntity testResourcesEntity = resourcesEntityList.get(resourcesEntityList.size() - 1);
        assertThat(testResourcesEntity.getDenars()).isEqualTo(DEFAULT_DENARS);
        assertThat(testResourcesEntity.getWood()).isEqualTo(DEFAULT_WOOD);
        assertThat(testResourcesEntity.getStone()).isEqualTo(DEFAULT_STONE);
        assertThat(testResourcesEntity.getSteel()).isEqualTo(DEFAULT_STEEL);
        assertThat(testResourcesEntity.getFood()).isEqualTo(DEFAULT_FOOD);
        assertThat(testResourcesEntity.getFaith()).isEqualTo(DEFAULT_FAITH);
        assertThat(testResourcesEntity.getChangeDate()).isEqualTo(DEFAULT_CHANGE_DATE);
    }

    @Test
    @Transactional
    public void createResourcesEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = resourcesEntityRepository.findAll().size();

        // Create the ResourcesEntity with an existing ID
        resourcesEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResourcesEntityMockMvc.perform(post("/api/resources-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(resourcesEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ResourcesEntity in the database
        List<ResourcesEntity> resourcesEntityList = resourcesEntityRepository.findAll();
        assertThat(resourcesEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllResourcesEntities() throws Exception {
        // Initialize the database
        resourcesEntityRepository.saveAndFlush(resourcesEntity);

        // Get all the resourcesEntityList
        restResourcesEntityMockMvc.perform(get("/api/resources-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resourcesEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].denars").value(hasItem(DEFAULT_DENARS.doubleValue())))
            .andExpect(jsonPath("$.[*].wood").value(hasItem(DEFAULT_WOOD.doubleValue())))
            .andExpect(jsonPath("$.[*].stone").value(hasItem(DEFAULT_STONE.doubleValue())))
            .andExpect(jsonPath("$.[*].steel").value(hasItem(DEFAULT_STEEL.doubleValue())))
            .andExpect(jsonPath("$.[*].food").value(hasItem(DEFAULT_FOOD.doubleValue())))
            .andExpect(jsonPath("$.[*].faith").value(hasItem(DEFAULT_FAITH.doubleValue())))
            .andExpect(jsonPath("$.[*].changeDate").value(hasItem(sameInstant(DEFAULT_CHANGE_DATE))));
    }
    
    @Test
    @Transactional
    public void getResourcesEntity() throws Exception {
        // Initialize the database
        resourcesEntityRepository.saveAndFlush(resourcesEntity);

        // Get the resourcesEntity
        restResourcesEntityMockMvc.perform(get("/api/resources-entities/{id}", resourcesEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(resourcesEntity.getId().intValue()))
            .andExpect(jsonPath("$.denars").value(DEFAULT_DENARS.doubleValue()))
            .andExpect(jsonPath("$.wood").value(DEFAULT_WOOD.doubleValue()))
            .andExpect(jsonPath("$.stone").value(DEFAULT_STONE.doubleValue()))
            .andExpect(jsonPath("$.steel").value(DEFAULT_STEEL.doubleValue()))
            .andExpect(jsonPath("$.food").value(DEFAULT_FOOD.doubleValue()))
            .andExpect(jsonPath("$.faith").value(DEFAULT_FAITH.doubleValue()))
            .andExpect(jsonPath("$.changeDate").value(sameInstant(DEFAULT_CHANGE_DATE)));
    }
    @Test
    @Transactional
    public void getNonExistingResourcesEntity() throws Exception {
        // Get the resourcesEntity
        restResourcesEntityMockMvc.perform(get("/api/resources-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResourcesEntity() throws Exception {
        // Initialize the database
        resourcesEntityRepository.saveAndFlush(resourcesEntity);

        int databaseSizeBeforeUpdate = resourcesEntityRepository.findAll().size();

        // Update the resourcesEntity
        ResourcesEntity updatedResourcesEntity = resourcesEntityRepository.findById(resourcesEntity.getId()).get();
        // Disconnect from session so that the updates on updatedResourcesEntity are not directly saved in db
        em.detach(updatedResourcesEntity);
        updatedResourcesEntity
            .denars(UPDATED_DENARS)
            .wood(UPDATED_WOOD)
            .stone(UPDATED_STONE)
            .steel(UPDATED_STEEL)
            .food(UPDATED_FOOD)
            .faith(UPDATED_FAITH)
            .changeDate(UPDATED_CHANGE_DATE);

        restResourcesEntityMockMvc.perform(put("/api/resources-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedResourcesEntity)))
            .andExpect(status().isOk());

        // Validate the ResourcesEntity in the database
        List<ResourcesEntity> resourcesEntityList = resourcesEntityRepository.findAll();
        assertThat(resourcesEntityList).hasSize(databaseSizeBeforeUpdate);
        ResourcesEntity testResourcesEntity = resourcesEntityList.get(resourcesEntityList.size() - 1);
        assertThat(testResourcesEntity.getDenars()).isEqualTo(UPDATED_DENARS);
        assertThat(testResourcesEntity.getWood()).isEqualTo(UPDATED_WOOD);
        assertThat(testResourcesEntity.getStone()).isEqualTo(UPDATED_STONE);
        assertThat(testResourcesEntity.getSteel()).isEqualTo(UPDATED_STEEL);
        assertThat(testResourcesEntity.getFood()).isEqualTo(UPDATED_FOOD);
        assertThat(testResourcesEntity.getFaith()).isEqualTo(UPDATED_FAITH);
        assertThat(testResourcesEntity.getChangeDate()).isEqualTo(UPDATED_CHANGE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingResourcesEntity() throws Exception {
        int databaseSizeBeforeUpdate = resourcesEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restResourcesEntityMockMvc.perform(put("/api/resources-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(resourcesEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ResourcesEntity in the database
        List<ResourcesEntity> resourcesEntityList = resourcesEntityRepository.findAll();
        assertThat(resourcesEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteResourcesEntity() throws Exception {
        // Initialize the database
        resourcesEntityRepository.saveAndFlush(resourcesEntity);

        int databaseSizeBeforeDelete = resourcesEntityRepository.findAll().size();

        // Delete the resourcesEntity
        restResourcesEntityMockMvc.perform(delete("/api/resources-entities/{id}", resourcesEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ResourcesEntity> resourcesEntityList = resourcesEntityRepository.findAll();
        assertThat(resourcesEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
