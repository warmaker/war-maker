package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.PlayerRelations;
import com.graphzero.warmaker.repository.PlayerRelationsRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.graphzero.warmaker.domain.enumeration.RelationType;
/**
 * Integration tests for the {@link PlayerRelationsResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PlayerRelationsResourceIT {

    private static final RelationType DEFAULT_RELATION = RelationType.ENEMY;
    private static final RelationType UPDATED_RELATION = RelationType.ALLY;

    @Autowired
    private PlayerRelationsRepository playerRelationsRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPlayerRelationsMockMvc;

    private PlayerRelations playerRelations;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PlayerRelations createEntity(EntityManager em) {
        PlayerRelations playerRelations = new PlayerRelations()
            .relation(DEFAULT_RELATION);
        return playerRelations;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PlayerRelations createUpdatedEntity(EntityManager em) {
        PlayerRelations playerRelations = new PlayerRelations()
            .relation(UPDATED_RELATION);
        return playerRelations;
    }

    @BeforeEach
    public void initTest() {
        playerRelations = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlayerRelations() throws Exception {
        int databaseSizeBeforeCreate = playerRelationsRepository.findAll().size();
        // Create the PlayerRelations
        restPlayerRelationsMockMvc.perform(post("/api/player-relations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(playerRelations)))
            .andExpect(status().isCreated());

        // Validate the PlayerRelations in the database
        List<PlayerRelations> playerRelationsList = playerRelationsRepository.findAll();
        assertThat(playerRelationsList).hasSize(databaseSizeBeforeCreate + 1);
        PlayerRelations testPlayerRelations = playerRelationsList.get(playerRelationsList.size() - 1);
        assertThat(testPlayerRelations.getRelation()).isEqualTo(DEFAULT_RELATION);
    }

    @Test
    @Transactional
    public void createPlayerRelationsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = playerRelationsRepository.findAll().size();

        // Create the PlayerRelations with an existing ID
        playerRelations.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlayerRelationsMockMvc.perform(post("/api/player-relations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(playerRelations)))
            .andExpect(status().isBadRequest());

        // Validate the PlayerRelations in the database
        List<PlayerRelations> playerRelationsList = playerRelationsRepository.findAll();
        assertThat(playerRelationsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPlayerRelations() throws Exception {
        // Initialize the database
        playerRelationsRepository.saveAndFlush(playerRelations);

        // Get all the playerRelationsList
        restPlayerRelationsMockMvc.perform(get("/api/player-relations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(playerRelations.getId().intValue())))
            .andExpect(jsonPath("$.[*].relation").value(hasItem(DEFAULT_RELATION.toString())));
    }
    
    @Test
    @Transactional
    public void getPlayerRelations() throws Exception {
        // Initialize the database
        playerRelationsRepository.saveAndFlush(playerRelations);

        // Get the playerRelations
        restPlayerRelationsMockMvc.perform(get("/api/player-relations/{id}", playerRelations.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(playerRelations.getId().intValue()))
            .andExpect(jsonPath("$.relation").value(DEFAULT_RELATION.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingPlayerRelations() throws Exception {
        // Get the playerRelations
        restPlayerRelationsMockMvc.perform(get("/api/player-relations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlayerRelations() throws Exception {
        // Initialize the database
        playerRelationsRepository.saveAndFlush(playerRelations);

        int databaseSizeBeforeUpdate = playerRelationsRepository.findAll().size();

        // Update the playerRelations
        PlayerRelations updatedPlayerRelations = playerRelationsRepository.findById(playerRelations.getId()).get();
        // Disconnect from session so that the updates on updatedPlayerRelations are not directly saved in db
        em.detach(updatedPlayerRelations);
        updatedPlayerRelations
            .relation(UPDATED_RELATION);

        restPlayerRelationsMockMvc.perform(put("/api/player-relations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPlayerRelations)))
            .andExpect(status().isOk());

        // Validate the PlayerRelations in the database
        List<PlayerRelations> playerRelationsList = playerRelationsRepository.findAll();
        assertThat(playerRelationsList).hasSize(databaseSizeBeforeUpdate);
        PlayerRelations testPlayerRelations = playerRelationsList.get(playerRelationsList.size() - 1);
        assertThat(testPlayerRelations.getRelation()).isEqualTo(UPDATED_RELATION);
    }

    @Test
    @Transactional
    public void updateNonExistingPlayerRelations() throws Exception {
        int databaseSizeBeforeUpdate = playerRelationsRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPlayerRelationsMockMvc.perform(put("/api/player-relations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(playerRelations)))
            .andExpect(status().isBadRequest());

        // Validate the PlayerRelations in the database
        List<PlayerRelations> playerRelationsList = playerRelationsRepository.findAll();
        assertThat(playerRelationsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePlayerRelations() throws Exception {
        // Initialize the database
        playerRelationsRepository.saveAndFlush(playerRelations);

        int databaseSizeBeforeDelete = playerRelationsRepository.findAll().size();

        // Delete the playerRelations
        restPlayerRelationsMockMvc.perform(delete("/api/player-relations/{id}", playerRelations.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PlayerRelations> playerRelationsList = playerRelationsRepository.findAll();
        assertThat(playerRelationsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
