package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.LandEntity;
import com.graphzero.warmaker.domain.FieldEntity;
import com.graphzero.warmaker.repository.LandEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LandEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LandEntityResourceIT {

    private static final Integer DEFAULT_ROAD = 1;
    private static final Integer UPDATED_ROAD = 2;

    private static final Integer DEFAULT_SCOUT_TOWER = 1;
    private static final Integer UPDATED_SCOUT_TOWER = 2;

    @Autowired
    private LandEntityRepository landEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLandEntityMockMvc;

    private LandEntity landEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LandEntity createEntity(EntityManager em) {
        LandEntity landEntity = new LandEntity()
            .road(DEFAULT_ROAD)
            .scoutTower(DEFAULT_SCOUT_TOWER);
        // Add required entity
        FieldEntity fieldEntity;
        if (TestUtil.findAll(em, FieldEntity.class).isEmpty()) {
            fieldEntity = FieldEntityResourceIT.createEntity(em);
            em.persist(fieldEntity);
            em.flush();
        } else {
            fieldEntity = TestUtil.findAll(em, FieldEntity.class).get(0);
        }
        landEntity.setLocation(fieldEntity);
        return landEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LandEntity createUpdatedEntity(EntityManager em) {
        LandEntity landEntity = new LandEntity()
            .road(UPDATED_ROAD)
            .scoutTower(UPDATED_SCOUT_TOWER);
        // Add required entity
        FieldEntity fieldEntity;
        if (TestUtil.findAll(em, FieldEntity.class).isEmpty()) {
            fieldEntity = FieldEntityResourceIT.createUpdatedEntity(em);
            em.persist(fieldEntity);
            em.flush();
        } else {
            fieldEntity = TestUtil.findAll(em, FieldEntity.class).get(0);
        }
        landEntity.setLocation(fieldEntity);
        return landEntity;
    }

    @BeforeEach
    public void initTest() {
        landEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createLandEntity() throws Exception {
        int databaseSizeBeforeCreate = landEntityRepository.findAll().size();
        // Create the LandEntity
        restLandEntityMockMvc.perform(post("/api/land-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(landEntity)))
            .andExpect(status().isCreated());

        // Validate the LandEntity in the database
        List<LandEntity> landEntityList = landEntityRepository.findAll();
        assertThat(landEntityList).hasSize(databaseSizeBeforeCreate + 1);
        LandEntity testLandEntity = landEntityList.get(landEntityList.size() - 1);
        assertThat(testLandEntity.getRoad()).isEqualTo(DEFAULT_ROAD);
        assertThat(testLandEntity.getScoutTower()).isEqualTo(DEFAULT_SCOUT_TOWER);
    }

    @Test
    @Transactional
    public void createLandEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = landEntityRepository.findAll().size();

        // Create the LandEntity with an existing ID
        landEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLandEntityMockMvc.perform(post("/api/land-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(landEntity)))
            .andExpect(status().isBadRequest());

        // Validate the LandEntity in the database
        List<LandEntity> landEntityList = landEntityRepository.findAll();
        assertThat(landEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllLandEntities() throws Exception {
        // Initialize the database
        landEntityRepository.saveAndFlush(landEntity);

        // Get all the landEntityList
        restLandEntityMockMvc.perform(get("/api/land-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(landEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].road").value(hasItem(DEFAULT_ROAD)))
            .andExpect(jsonPath("$.[*].scoutTower").value(hasItem(DEFAULT_SCOUT_TOWER)));
    }
    
    @Test
    @Transactional
    public void getLandEntity() throws Exception {
        // Initialize the database
        landEntityRepository.saveAndFlush(landEntity);

        // Get the landEntity
        restLandEntityMockMvc.perform(get("/api/land-entities/{id}", landEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(landEntity.getId().intValue()))
            .andExpect(jsonPath("$.road").value(DEFAULT_ROAD))
            .andExpect(jsonPath("$.scoutTower").value(DEFAULT_SCOUT_TOWER));
    }
    @Test
    @Transactional
    public void getNonExistingLandEntity() throws Exception {
        // Get the landEntity
        restLandEntityMockMvc.perform(get("/api/land-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLandEntity() throws Exception {
        // Initialize the database
        landEntityRepository.saveAndFlush(landEntity);

        int databaseSizeBeforeUpdate = landEntityRepository.findAll().size();

        // Update the landEntity
        LandEntity updatedLandEntity = landEntityRepository.findById(landEntity.getId()).get();
        // Disconnect from session so that the updates on updatedLandEntity are not directly saved in db
        em.detach(updatedLandEntity);
        updatedLandEntity
            .road(UPDATED_ROAD)
            .scoutTower(UPDATED_SCOUT_TOWER);

        restLandEntityMockMvc.perform(put("/api/land-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLandEntity)))
            .andExpect(status().isOk());

        // Validate the LandEntity in the database
        List<LandEntity> landEntityList = landEntityRepository.findAll();
        assertThat(landEntityList).hasSize(databaseSizeBeforeUpdate);
        LandEntity testLandEntity = landEntityList.get(landEntityList.size() - 1);
        assertThat(testLandEntity.getRoad()).isEqualTo(UPDATED_ROAD);
        assertThat(testLandEntity.getScoutTower()).isEqualTo(UPDATED_SCOUT_TOWER);
    }

    @Test
    @Transactional
    public void updateNonExistingLandEntity() throws Exception {
        int databaseSizeBeforeUpdate = landEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLandEntityMockMvc.perform(put("/api/land-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(landEntity)))
            .andExpect(status().isBadRequest());

        // Validate the LandEntity in the database
        List<LandEntity> landEntityList = landEntityRepository.findAll();
        assertThat(landEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLandEntity() throws Exception {
        // Initialize the database
        landEntityRepository.saveAndFlush(landEntity);

        int databaseSizeBeforeDelete = landEntityRepository.findAll().size();

        // Delete the landEntity
        restLandEntityMockMvc.perform(delete("/api/land-entities/{id}", landEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LandEntity> landEntityList = landEntityRepository.findAll();
        assertThat(landEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
