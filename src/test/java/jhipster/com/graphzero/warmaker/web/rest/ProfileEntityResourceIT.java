package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.ProfileEntity;
import com.graphzero.warmaker.domain.User;
import com.graphzero.warmaker.repository.ProfileEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProfileEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ProfileEntityResourceIT {

    private static final String DEFAULT_ACCOUNT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final Double DEFAULT_RESEARCH_TIME_MODIFIER = 1D;
    private static final Double UPDATED_RESEARCH_TIME_MODIFIER = 2D;

    @Autowired
    private ProfileEntityRepository profileEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProfileEntityMockMvc;

    private ProfileEntity profileEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProfileEntity createEntity(EntityManager em) {
        ProfileEntity profileEntity = new ProfileEntity()
            .accountName(DEFAULT_ACCOUNT_NAME)
            .email(DEFAULT_EMAIL)
            .researchTimeModifier(DEFAULT_RESEARCH_TIME_MODIFIER);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        profileEntity.setUser(user);
        return profileEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProfileEntity createUpdatedEntity(EntityManager em) {
        ProfileEntity profileEntity = new ProfileEntity()
            .accountName(UPDATED_ACCOUNT_NAME)
            .email(UPDATED_EMAIL)
            .researchTimeModifier(UPDATED_RESEARCH_TIME_MODIFIER);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        profileEntity.setUser(user);
        return profileEntity;
    }

    @BeforeEach
    public void initTest() {
        profileEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createProfileEntity() throws Exception {
        int databaseSizeBeforeCreate = profileEntityRepository.findAll().size();
        // Create the ProfileEntity
        restProfileEntityMockMvc.perform(post("/api/profile-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profileEntity)))
            .andExpect(status().isCreated());

        // Validate the ProfileEntity in the database
        List<ProfileEntity> profileEntityList = profileEntityRepository.findAll();
        assertThat(profileEntityList).hasSize(databaseSizeBeforeCreate + 1);
        ProfileEntity testProfileEntity = profileEntityList.get(profileEntityList.size() - 1);
        assertThat(testProfileEntity.getAccountName()).isEqualTo(DEFAULT_ACCOUNT_NAME);
        assertThat(testProfileEntity.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testProfileEntity.getResearchTimeModifier()).isEqualTo(DEFAULT_RESEARCH_TIME_MODIFIER);
    }

    @Test
    @Transactional
    public void createProfileEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = profileEntityRepository.findAll().size();

        // Create the ProfileEntity with an existing ID
        profileEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProfileEntityMockMvc.perform(post("/api/profile-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profileEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ProfileEntity in the database
        List<ProfileEntity> profileEntityList = profileEntityRepository.findAll();
        assertThat(profileEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProfileEntities() throws Exception {
        // Initialize the database
        profileEntityRepository.saveAndFlush(profileEntity);

        // Get all the profileEntityList
        restProfileEntityMockMvc.perform(get("/api/profile-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(profileEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].accountName").value(hasItem(DEFAULT_ACCOUNT_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].researchTimeModifier").value(hasItem(DEFAULT_RESEARCH_TIME_MODIFIER.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getProfileEntity() throws Exception {
        // Initialize the database
        profileEntityRepository.saveAndFlush(profileEntity);

        // Get the profileEntity
        restProfileEntityMockMvc.perform(get("/api/profile-entities/{id}", profileEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(profileEntity.getId().intValue()))
            .andExpect(jsonPath("$.accountName").value(DEFAULT_ACCOUNT_NAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.researchTimeModifier").value(DEFAULT_RESEARCH_TIME_MODIFIER.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingProfileEntity() throws Exception {
        // Get the profileEntity
        restProfileEntityMockMvc.perform(get("/api/profile-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProfileEntity() throws Exception {
        // Initialize the database
        profileEntityRepository.saveAndFlush(profileEntity);

        int databaseSizeBeforeUpdate = profileEntityRepository.findAll().size();

        // Update the profileEntity
        ProfileEntity updatedProfileEntity = profileEntityRepository.findById(profileEntity.getId()).get();
        // Disconnect from session so that the updates on updatedProfileEntity are not directly saved in db
        em.detach(updatedProfileEntity);
        updatedProfileEntity
            .accountName(UPDATED_ACCOUNT_NAME)
            .email(UPDATED_EMAIL)
            .researchTimeModifier(UPDATED_RESEARCH_TIME_MODIFIER);

        restProfileEntityMockMvc.perform(put("/api/profile-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedProfileEntity)))
            .andExpect(status().isOk());

        // Validate the ProfileEntity in the database
        List<ProfileEntity> profileEntityList = profileEntityRepository.findAll();
        assertThat(profileEntityList).hasSize(databaseSizeBeforeUpdate);
        ProfileEntity testProfileEntity = profileEntityList.get(profileEntityList.size() - 1);
        assertThat(testProfileEntity.getAccountName()).isEqualTo(UPDATED_ACCOUNT_NAME);
        assertThat(testProfileEntity.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testProfileEntity.getResearchTimeModifier()).isEqualTo(UPDATED_RESEARCH_TIME_MODIFIER);
    }

    @Test
    @Transactional
    public void updateNonExistingProfileEntity() throws Exception {
        int databaseSizeBeforeUpdate = profileEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProfileEntityMockMvc.perform(put("/api/profile-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(profileEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ProfileEntity in the database
        List<ProfileEntity> profileEntityList = profileEntityRepository.findAll();
        assertThat(profileEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProfileEntity() throws Exception {
        // Initialize the database
        profileEntityRepository.saveAndFlush(profileEntity);

        int databaseSizeBeforeDelete = profileEntityRepository.findAll().size();

        // Delete the profileEntity
        restProfileEntityMockMvc.perform(delete("/api/profile-entities/{id}", profileEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProfileEntity> profileEntityList = profileEntityRepository.findAll();
        assertThat(profileEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
