package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.HeroEntity;
import com.graphzero.warmaker.repository.HeroEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.graphzero.warmaker.domain.enumeration.HeroType;
/**
 * Integration tests for the {@link HeroEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class HeroEntityResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final HeroType DEFAULT_TYPE = HeroType.WARRIOR;
    private static final HeroType UPDATED_TYPE = HeroType.SPY;

    private static final Integer DEFAULT_LEVEL = 1;
    private static final Integer UPDATED_LEVEL = 2;

    @Autowired
    private HeroEntityRepository heroEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHeroEntityMockMvc;

    private HeroEntity heroEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HeroEntity createEntity(EntityManager em) {
        HeroEntity heroEntity = new HeroEntity()
            .name(DEFAULT_NAME)
            .type(DEFAULT_TYPE)
            .level(DEFAULT_LEVEL);
        return heroEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HeroEntity createUpdatedEntity(EntityManager em) {
        HeroEntity heroEntity = new HeroEntity()
            .name(UPDATED_NAME)
            .type(UPDATED_TYPE)
            .level(UPDATED_LEVEL);
        return heroEntity;
    }

    @BeforeEach
    public void initTest() {
        heroEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createHeroEntity() throws Exception {
        int databaseSizeBeforeCreate = heroEntityRepository.findAll().size();
        // Create the HeroEntity
        restHeroEntityMockMvc.perform(post("/api/hero-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(heroEntity)))
            .andExpect(status().isCreated());

        // Validate the HeroEntity in the database
        List<HeroEntity> heroEntityList = heroEntityRepository.findAll();
        assertThat(heroEntityList).hasSize(databaseSizeBeforeCreate + 1);
        HeroEntity testHeroEntity = heroEntityList.get(heroEntityList.size() - 1);
        assertThat(testHeroEntity.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testHeroEntity.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testHeroEntity.getLevel()).isEqualTo(DEFAULT_LEVEL);
    }

    @Test
    @Transactional
    public void createHeroEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = heroEntityRepository.findAll().size();

        // Create the HeroEntity with an existing ID
        heroEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHeroEntityMockMvc.perform(post("/api/hero-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(heroEntity)))
            .andExpect(status().isBadRequest());

        // Validate the HeroEntity in the database
        List<HeroEntity> heroEntityList = heroEntityRepository.findAll();
        assertThat(heroEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllHeroEntities() throws Exception {
        // Initialize the database
        heroEntityRepository.saveAndFlush(heroEntity);

        // Get all the heroEntityList
        restHeroEntityMockMvc.perform(get("/api/hero-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(heroEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)));
    }
    
    @Test
    @Transactional
    public void getHeroEntity() throws Exception {
        // Initialize the database
        heroEntityRepository.saveAndFlush(heroEntity);

        // Get the heroEntity
        restHeroEntityMockMvc.perform(get("/api/hero-entities/{id}", heroEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(heroEntity.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL));
    }
    @Test
    @Transactional
    public void getNonExistingHeroEntity() throws Exception {
        // Get the heroEntity
        restHeroEntityMockMvc.perform(get("/api/hero-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHeroEntity() throws Exception {
        // Initialize the database
        heroEntityRepository.saveAndFlush(heroEntity);

        int databaseSizeBeforeUpdate = heroEntityRepository.findAll().size();

        // Update the heroEntity
        HeroEntity updatedHeroEntity = heroEntityRepository.findById(heroEntity.getId()).get();
        // Disconnect from session so that the updates on updatedHeroEntity are not directly saved in db
        em.detach(updatedHeroEntity);
        updatedHeroEntity
            .name(UPDATED_NAME)
            .type(UPDATED_TYPE)
            .level(UPDATED_LEVEL);

        restHeroEntityMockMvc.perform(put("/api/hero-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedHeroEntity)))
            .andExpect(status().isOk());

        // Validate the HeroEntity in the database
        List<HeroEntity> heroEntityList = heroEntityRepository.findAll();
        assertThat(heroEntityList).hasSize(databaseSizeBeforeUpdate);
        HeroEntity testHeroEntity = heroEntityList.get(heroEntityList.size() - 1);
        assertThat(testHeroEntity.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testHeroEntity.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testHeroEntity.getLevel()).isEqualTo(UPDATED_LEVEL);
    }

    @Test
    @Transactional
    public void updateNonExistingHeroEntity() throws Exception {
        int databaseSizeBeforeUpdate = heroEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHeroEntityMockMvc.perform(put("/api/hero-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(heroEntity)))
            .andExpect(status().isBadRequest());

        // Validate the HeroEntity in the database
        List<HeroEntity> heroEntityList = heroEntityRepository.findAll();
        assertThat(heroEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteHeroEntity() throws Exception {
        // Initialize the database
        heroEntityRepository.saveAndFlush(heroEntity);

        int databaseSizeBeforeDelete = heroEntityRepository.findAll().size();

        // Delete the heroEntity
        restHeroEntityMockMvc.perform(delete("/api/hero-entities/{id}", heroEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HeroEntity> heroEntityList = heroEntityRepository.findAll();
        assertThat(heroEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
