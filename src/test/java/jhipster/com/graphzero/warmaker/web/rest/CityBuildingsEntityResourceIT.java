package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.CityBuildingsEntity;
import com.graphzero.warmaker.repository.CityBuildingsEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CityBuildingsEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CityBuildingsEntityResourceIT {

    private static final Integer DEFAULT_CITY_HALL = 1;
    private static final Integer UPDATED_CITY_HALL = 2;

    private static final Integer DEFAULT_BARRACKS = 1;
    private static final Integer UPDATED_BARRACKS = 2;

    private static final Integer DEFAULT_WALL = 1;
    private static final Integer UPDATED_WALL = 2;

    private static final Integer DEFAULT_GOLD_MINE = 1;
    private static final Integer UPDATED_GOLD_MINE = 2;

    private static final Integer DEFAULT_SAW_MILL = 1;
    private static final Integer UPDATED_SAW_MILL = 2;

    private static final Integer DEFAULT_ORE_MINE = 1;
    private static final Integer UPDATED_ORE_MINE = 2;

    private static final Integer DEFAULT_IRONWORKS = 1;
    private static final Integer UPDATED_IRONWORKS = 2;

    private static final Integer DEFAULT_TRADING_POST = 1;
    private static final Integer UPDATED_TRADING_POST = 2;

    private static final Integer DEFAULT_CHAPEL = 1;
    private static final Integer UPDATED_CHAPEL = 2;

    private static final Integer DEFAULT_UNIVERSITY = 1;
    private static final Integer UPDATED_UNIVERSITY = 2;

    private static final Integer DEFAULT_FORGE = 1;
    private static final Integer UPDATED_FORGE = 2;

    @Autowired
    private CityBuildingsEntityRepository cityBuildingsEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCityBuildingsEntityMockMvc;

    private CityBuildingsEntity cityBuildingsEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CityBuildingsEntity createEntity(EntityManager em) {
        CityBuildingsEntity cityBuildingsEntity = new CityBuildingsEntity()
            .cityHall(DEFAULT_CITY_HALL)
            .barracks(DEFAULT_BARRACKS)
            .wall(DEFAULT_WALL)
            .goldMine(DEFAULT_GOLD_MINE)
            .sawMill(DEFAULT_SAW_MILL)
            .oreMine(DEFAULT_ORE_MINE)
            .ironworks(DEFAULT_IRONWORKS)
            .tradingPost(DEFAULT_TRADING_POST)
            .chapel(DEFAULT_CHAPEL)
            .university(DEFAULT_UNIVERSITY)
            .forge(DEFAULT_FORGE);
        return cityBuildingsEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CityBuildingsEntity createUpdatedEntity(EntityManager em) {
        CityBuildingsEntity cityBuildingsEntity = new CityBuildingsEntity()
            .cityHall(UPDATED_CITY_HALL)
            .barracks(UPDATED_BARRACKS)
            .wall(UPDATED_WALL)
            .goldMine(UPDATED_GOLD_MINE)
            .sawMill(UPDATED_SAW_MILL)
            .oreMine(UPDATED_ORE_MINE)
            .ironworks(UPDATED_IRONWORKS)
            .tradingPost(UPDATED_TRADING_POST)
            .chapel(UPDATED_CHAPEL)
            .university(UPDATED_UNIVERSITY)
            .forge(UPDATED_FORGE);
        return cityBuildingsEntity;
    }

    @BeforeEach
    public void initTest() {
        cityBuildingsEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createCityBuildingsEntity() throws Exception {
        int databaseSizeBeforeCreate = cityBuildingsEntityRepository.findAll().size();
        // Create the CityBuildingsEntity
        restCityBuildingsEntityMockMvc.perform(post("/api/city-buildings-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cityBuildingsEntity)))
            .andExpect(status().isCreated());

        // Validate the CityBuildingsEntity in the database
        List<CityBuildingsEntity> cityBuildingsEntityList = cityBuildingsEntityRepository.findAll();
        assertThat(cityBuildingsEntityList).hasSize(databaseSizeBeforeCreate + 1);
        CityBuildingsEntity testCityBuildingsEntity = cityBuildingsEntityList.get(cityBuildingsEntityList.size() - 1);
        assertThat(testCityBuildingsEntity.getCityHall()).isEqualTo(DEFAULT_CITY_HALL);
        assertThat(testCityBuildingsEntity.getBarracks()).isEqualTo(DEFAULT_BARRACKS);
        assertThat(testCityBuildingsEntity.getWall()).isEqualTo(DEFAULT_WALL);
        assertThat(testCityBuildingsEntity.getGoldMine()).isEqualTo(DEFAULT_GOLD_MINE);
        assertThat(testCityBuildingsEntity.getSawMill()).isEqualTo(DEFAULT_SAW_MILL);
        assertThat(testCityBuildingsEntity.getOreMine()).isEqualTo(DEFAULT_ORE_MINE);
        assertThat(testCityBuildingsEntity.getIronworks()).isEqualTo(DEFAULT_IRONWORKS);
        assertThat(testCityBuildingsEntity.getTradingPost()).isEqualTo(DEFAULT_TRADING_POST);
        assertThat(testCityBuildingsEntity.getChapel()).isEqualTo(DEFAULT_CHAPEL);
        assertThat(testCityBuildingsEntity.getUniversity()).isEqualTo(DEFAULT_UNIVERSITY);
        assertThat(testCityBuildingsEntity.getForge()).isEqualTo(DEFAULT_FORGE);
    }

    @Test
    @Transactional
    public void createCityBuildingsEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cityBuildingsEntityRepository.findAll().size();

        // Create the CityBuildingsEntity with an existing ID
        cityBuildingsEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCityBuildingsEntityMockMvc.perform(post("/api/city-buildings-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cityBuildingsEntity)))
            .andExpect(status().isBadRequest());

        // Validate the CityBuildingsEntity in the database
        List<CityBuildingsEntity> cityBuildingsEntityList = cityBuildingsEntityRepository.findAll();
        assertThat(cityBuildingsEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCityBuildingsEntities() throws Exception {
        // Initialize the database
        cityBuildingsEntityRepository.saveAndFlush(cityBuildingsEntity);

        // Get all the cityBuildingsEntityList
        restCityBuildingsEntityMockMvc.perform(get("/api/city-buildings-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cityBuildingsEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].cityHall").value(hasItem(DEFAULT_CITY_HALL)))
            .andExpect(jsonPath("$.[*].barracks").value(hasItem(DEFAULT_BARRACKS)))
            .andExpect(jsonPath("$.[*].wall").value(hasItem(DEFAULT_WALL)))
            .andExpect(jsonPath("$.[*].goldMine").value(hasItem(DEFAULT_GOLD_MINE)))
            .andExpect(jsonPath("$.[*].sawMill").value(hasItem(DEFAULT_SAW_MILL)))
            .andExpect(jsonPath("$.[*].oreMine").value(hasItem(DEFAULT_ORE_MINE)))
            .andExpect(jsonPath("$.[*].ironworks").value(hasItem(DEFAULT_IRONWORKS)))
            .andExpect(jsonPath("$.[*].tradingPost").value(hasItem(DEFAULT_TRADING_POST)))
            .andExpect(jsonPath("$.[*].chapel").value(hasItem(DEFAULT_CHAPEL)))
            .andExpect(jsonPath("$.[*].university").value(hasItem(DEFAULT_UNIVERSITY)))
            .andExpect(jsonPath("$.[*].forge").value(hasItem(DEFAULT_FORGE)));
    }
    
    @Test
    @Transactional
    public void getCityBuildingsEntity() throws Exception {
        // Initialize the database
        cityBuildingsEntityRepository.saveAndFlush(cityBuildingsEntity);

        // Get the cityBuildingsEntity
        restCityBuildingsEntityMockMvc.perform(get("/api/city-buildings-entities/{id}", cityBuildingsEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(cityBuildingsEntity.getId().intValue()))
            .andExpect(jsonPath("$.cityHall").value(DEFAULT_CITY_HALL))
            .andExpect(jsonPath("$.barracks").value(DEFAULT_BARRACKS))
            .andExpect(jsonPath("$.wall").value(DEFAULT_WALL))
            .andExpect(jsonPath("$.goldMine").value(DEFAULT_GOLD_MINE))
            .andExpect(jsonPath("$.sawMill").value(DEFAULT_SAW_MILL))
            .andExpect(jsonPath("$.oreMine").value(DEFAULT_ORE_MINE))
            .andExpect(jsonPath("$.ironworks").value(DEFAULT_IRONWORKS))
            .andExpect(jsonPath("$.tradingPost").value(DEFAULT_TRADING_POST))
            .andExpect(jsonPath("$.chapel").value(DEFAULT_CHAPEL))
            .andExpect(jsonPath("$.university").value(DEFAULT_UNIVERSITY))
            .andExpect(jsonPath("$.forge").value(DEFAULT_FORGE));
    }
    @Test
    @Transactional
    public void getNonExistingCityBuildingsEntity() throws Exception {
        // Get the cityBuildingsEntity
        restCityBuildingsEntityMockMvc.perform(get("/api/city-buildings-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCityBuildingsEntity() throws Exception {
        // Initialize the database
        cityBuildingsEntityRepository.saveAndFlush(cityBuildingsEntity);

        int databaseSizeBeforeUpdate = cityBuildingsEntityRepository.findAll().size();

        // Update the cityBuildingsEntity
        CityBuildingsEntity updatedCityBuildingsEntity = cityBuildingsEntityRepository.findById(cityBuildingsEntity.getId()).get();
        // Disconnect from session so that the updates on updatedCityBuildingsEntity are not directly saved in db
        em.detach(updatedCityBuildingsEntity);
        updatedCityBuildingsEntity
            .cityHall(UPDATED_CITY_HALL)
            .barracks(UPDATED_BARRACKS)
            .wall(UPDATED_WALL)
            .goldMine(UPDATED_GOLD_MINE)
            .sawMill(UPDATED_SAW_MILL)
            .oreMine(UPDATED_ORE_MINE)
            .ironworks(UPDATED_IRONWORKS)
            .tradingPost(UPDATED_TRADING_POST)
            .chapel(UPDATED_CHAPEL)
            .university(UPDATED_UNIVERSITY)
            .forge(UPDATED_FORGE);

        restCityBuildingsEntityMockMvc.perform(put("/api/city-buildings-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCityBuildingsEntity)))
            .andExpect(status().isOk());

        // Validate the CityBuildingsEntity in the database
        List<CityBuildingsEntity> cityBuildingsEntityList = cityBuildingsEntityRepository.findAll();
        assertThat(cityBuildingsEntityList).hasSize(databaseSizeBeforeUpdate);
        CityBuildingsEntity testCityBuildingsEntity = cityBuildingsEntityList.get(cityBuildingsEntityList.size() - 1);
        assertThat(testCityBuildingsEntity.getCityHall()).isEqualTo(UPDATED_CITY_HALL);
        assertThat(testCityBuildingsEntity.getBarracks()).isEqualTo(UPDATED_BARRACKS);
        assertThat(testCityBuildingsEntity.getWall()).isEqualTo(UPDATED_WALL);
        assertThat(testCityBuildingsEntity.getGoldMine()).isEqualTo(UPDATED_GOLD_MINE);
        assertThat(testCityBuildingsEntity.getSawMill()).isEqualTo(UPDATED_SAW_MILL);
        assertThat(testCityBuildingsEntity.getOreMine()).isEqualTo(UPDATED_ORE_MINE);
        assertThat(testCityBuildingsEntity.getIronworks()).isEqualTo(UPDATED_IRONWORKS);
        assertThat(testCityBuildingsEntity.getTradingPost()).isEqualTo(UPDATED_TRADING_POST);
        assertThat(testCityBuildingsEntity.getChapel()).isEqualTo(UPDATED_CHAPEL);
        assertThat(testCityBuildingsEntity.getUniversity()).isEqualTo(UPDATED_UNIVERSITY);
        assertThat(testCityBuildingsEntity.getForge()).isEqualTo(UPDATED_FORGE);
    }

    @Test
    @Transactional
    public void updateNonExistingCityBuildingsEntity() throws Exception {
        int databaseSizeBeforeUpdate = cityBuildingsEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCityBuildingsEntityMockMvc.perform(put("/api/city-buildings-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(cityBuildingsEntity)))
            .andExpect(status().isBadRequest());

        // Validate the CityBuildingsEntity in the database
        List<CityBuildingsEntity> cityBuildingsEntityList = cityBuildingsEntityRepository.findAll();
        assertThat(cityBuildingsEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCityBuildingsEntity() throws Exception {
        // Initialize the database
        cityBuildingsEntityRepository.saveAndFlush(cityBuildingsEntity);

        int databaseSizeBeforeDelete = cityBuildingsEntityRepository.findAll().size();

        // Delete the cityBuildingsEntity
        restCityBuildingsEntityMockMvc.perform(delete("/api/city-buildings-entities/{id}", cityBuildingsEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CityBuildingsEntity> cityBuildingsEntityList = cityBuildingsEntityRepository.findAll();
        assertThat(cityBuildingsEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
