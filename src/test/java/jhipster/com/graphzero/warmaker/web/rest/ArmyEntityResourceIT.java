package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.ArmyEntity;
import com.graphzero.warmaker.repository.ArmyEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ArmyEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ArmyEntityResourceIT {

    private static final Integer DEFAULT_FOOD_REQ = 1;
    private static final Integer UPDATED_FOOD_REQ = 2;

    @Autowired
    private ArmyEntityRepository armyEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restArmyEntityMockMvc;

    private ArmyEntity armyEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ArmyEntity createEntity(EntityManager em) {
        ArmyEntity armyEntity = new ArmyEntity()
            .foodReq(DEFAULT_FOOD_REQ);
        return armyEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ArmyEntity createUpdatedEntity(EntityManager em) {
        ArmyEntity armyEntity = new ArmyEntity()
            .foodReq(UPDATED_FOOD_REQ);
        return armyEntity;
    }

    @BeforeEach
    public void initTest() {
        armyEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createArmyEntity() throws Exception {
        int databaseSizeBeforeCreate = armyEntityRepository.findAll().size();
        // Create the ArmyEntity
        restArmyEntityMockMvc.perform(post("/api/army-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(armyEntity)))
            .andExpect(status().isCreated());

        // Validate the ArmyEntity in the database
        List<ArmyEntity> armyEntityList = armyEntityRepository.findAll();
        assertThat(armyEntityList).hasSize(databaseSizeBeforeCreate + 1);
        ArmyEntity testArmyEntity = armyEntityList.get(armyEntityList.size() - 1);
        assertThat(testArmyEntity.getFoodReq()).isEqualTo(DEFAULT_FOOD_REQ);
    }

    @Test
    @Transactional
    public void createArmyEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = armyEntityRepository.findAll().size();

        // Create the ArmyEntity with an existing ID
        armyEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restArmyEntityMockMvc.perform(post("/api/army-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(armyEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ArmyEntity in the database
        List<ArmyEntity> armyEntityList = armyEntityRepository.findAll();
        assertThat(armyEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllArmyEntities() throws Exception {
        // Initialize the database
        armyEntityRepository.saveAndFlush(armyEntity);

        // Get all the armyEntityList
        restArmyEntityMockMvc.perform(get("/api/army-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(armyEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].foodReq").value(hasItem(DEFAULT_FOOD_REQ)));
    }
    
    @Test
    @Transactional
    public void getArmyEntity() throws Exception {
        // Initialize the database
        armyEntityRepository.saveAndFlush(armyEntity);

        // Get the armyEntity
        restArmyEntityMockMvc.perform(get("/api/army-entities/{id}", armyEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(armyEntity.getId().intValue()))
            .andExpect(jsonPath("$.foodReq").value(DEFAULT_FOOD_REQ));
    }
    @Test
    @Transactional
    public void getNonExistingArmyEntity() throws Exception {
        // Get the armyEntity
        restArmyEntityMockMvc.perform(get("/api/army-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateArmyEntity() throws Exception {
        // Initialize the database
        armyEntityRepository.saveAndFlush(armyEntity);

        int databaseSizeBeforeUpdate = armyEntityRepository.findAll().size();

        // Update the armyEntity
        ArmyEntity updatedArmyEntity = armyEntityRepository.findById(armyEntity.getId()).get();
        // Disconnect from session so that the updates on updatedArmyEntity are not directly saved in db
        em.detach(updatedArmyEntity);
        updatedArmyEntity
            .foodReq(UPDATED_FOOD_REQ);

        restArmyEntityMockMvc.perform(put("/api/army-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedArmyEntity)))
            .andExpect(status().isOk());

        // Validate the ArmyEntity in the database
        List<ArmyEntity> armyEntityList = armyEntityRepository.findAll();
        assertThat(armyEntityList).hasSize(databaseSizeBeforeUpdate);
        ArmyEntity testArmyEntity = armyEntityList.get(armyEntityList.size() - 1);
        assertThat(testArmyEntity.getFoodReq()).isEqualTo(UPDATED_FOOD_REQ);
    }

    @Test
    @Transactional
    public void updateNonExistingArmyEntity() throws Exception {
        int databaseSizeBeforeUpdate = armyEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restArmyEntityMockMvc.perform(put("/api/army-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(armyEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ArmyEntity in the database
        List<ArmyEntity> armyEntityList = armyEntityRepository.findAll();
        assertThat(armyEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteArmyEntity() throws Exception {
        // Initialize the database
        armyEntityRepository.saveAndFlush(armyEntity);

        int databaseSizeBeforeDelete = armyEntityRepository.findAll().size();

        // Delete the armyEntity
        restArmyEntityMockMvc.perform(delete("/api/army-entities/{id}", armyEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ArmyEntity> armyEntityList = armyEntityRepository.findAll();
        assertThat(armyEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
