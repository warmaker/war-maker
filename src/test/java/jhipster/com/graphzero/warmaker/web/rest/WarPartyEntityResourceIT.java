package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.WarPartyEntity;
import com.graphzero.warmaker.domain.FieldEntity;
import com.graphzero.warmaker.repository.WarPartyEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.graphzero.warmaker.domain.enumeration.WarPartyState;
/**
 * Integration tests for the {@link WarPartyEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class WarPartyEntityResourceIT {

    private static final Double DEFAULT_SPEED = 1D;
    private static final Double UPDATED_SPEED = 2D;

    private static final Long DEFAULT_CAPACITY = 1L;
    private static final Long UPDATED_CAPACITY = 2L;

    private static final WarPartyState DEFAULT_STATE = WarPartyState.MOVING;
    private static final WarPartyState UPDATED_STATE = WarPartyState.STATIONED;

    @Autowired
    private WarPartyEntityRepository warPartyEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restWarPartyEntityMockMvc;

    private WarPartyEntity warPartyEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WarPartyEntity createEntity(EntityManager em) {
        WarPartyEntity warPartyEntity = new WarPartyEntity()
            .speed(DEFAULT_SPEED)
            .capacity(DEFAULT_CAPACITY)
            .state(DEFAULT_STATE);
        // Add required entity
        FieldEntity fieldEntity;
        if (TestUtil.findAll(em, FieldEntity.class).isEmpty()) {
            fieldEntity = FieldEntityResourceIT.createEntity(em);
            em.persist(fieldEntity);
            em.flush();
        } else {
            fieldEntity = TestUtil.findAll(em, FieldEntity.class).get(0);
        }
        warPartyEntity.setPosition(fieldEntity);
        return warPartyEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WarPartyEntity createUpdatedEntity(EntityManager em) {
        WarPartyEntity warPartyEntity = new WarPartyEntity()
            .speed(UPDATED_SPEED)
            .capacity(UPDATED_CAPACITY)
            .state(UPDATED_STATE);
        // Add required entity
        FieldEntity fieldEntity;
        if (TestUtil.findAll(em, FieldEntity.class).isEmpty()) {
            fieldEntity = FieldEntityResourceIT.createUpdatedEntity(em);
            em.persist(fieldEntity);
            em.flush();
        } else {
            fieldEntity = TestUtil.findAll(em, FieldEntity.class).get(0);
        }
        warPartyEntity.setPosition(fieldEntity);
        return warPartyEntity;
    }

    @BeforeEach
    public void initTest() {
        warPartyEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createWarPartyEntity() throws Exception {
        int databaseSizeBeforeCreate = warPartyEntityRepository.findAll().size();
        // Create the WarPartyEntity
        restWarPartyEntityMockMvc.perform(post("/api/war-party-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(warPartyEntity)))
            .andExpect(status().isCreated());

        // Validate the WarPartyEntity in the database
        List<WarPartyEntity> warPartyEntityList = warPartyEntityRepository.findAll();
        assertThat(warPartyEntityList).hasSize(databaseSizeBeforeCreate + 1);
        WarPartyEntity testWarPartyEntity = warPartyEntityList.get(warPartyEntityList.size() - 1);
        assertThat(testWarPartyEntity.getSpeed()).isEqualTo(DEFAULT_SPEED);
        assertThat(testWarPartyEntity.getCapacity()).isEqualTo(DEFAULT_CAPACITY);
        assertThat(testWarPartyEntity.getState()).isEqualTo(DEFAULT_STATE);
    }

    @Test
    @Transactional
    public void createWarPartyEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = warPartyEntityRepository.findAll().size();

        // Create the WarPartyEntity with an existing ID
        warPartyEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWarPartyEntityMockMvc.perform(post("/api/war-party-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(warPartyEntity)))
            .andExpect(status().isBadRequest());

        // Validate the WarPartyEntity in the database
        List<WarPartyEntity> warPartyEntityList = warPartyEntityRepository.findAll();
        assertThat(warPartyEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllWarPartyEntities() throws Exception {
        // Initialize the database
        warPartyEntityRepository.saveAndFlush(warPartyEntity);

        // Get all the warPartyEntityList
        restWarPartyEntityMockMvc.perform(get("/api/war-party-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(warPartyEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].speed").value(hasItem(DEFAULT_SPEED.doubleValue())))
            .andExpect(jsonPath("$.[*].capacity").value(hasItem(DEFAULT_CAPACITY.intValue())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())));
    }
    
    @Test
    @Transactional
    public void getWarPartyEntity() throws Exception {
        // Initialize the database
        warPartyEntityRepository.saveAndFlush(warPartyEntity);

        // Get the warPartyEntity
        restWarPartyEntityMockMvc.perform(get("/api/war-party-entities/{id}", warPartyEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(warPartyEntity.getId().intValue()))
            .andExpect(jsonPath("$.speed").value(DEFAULT_SPEED.doubleValue()))
            .andExpect(jsonPath("$.capacity").value(DEFAULT_CAPACITY.intValue()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingWarPartyEntity() throws Exception {
        // Get the warPartyEntity
        restWarPartyEntityMockMvc.perform(get("/api/war-party-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWarPartyEntity() throws Exception {
        // Initialize the database
        warPartyEntityRepository.saveAndFlush(warPartyEntity);

        int databaseSizeBeforeUpdate = warPartyEntityRepository.findAll().size();

        // Update the warPartyEntity
        WarPartyEntity updatedWarPartyEntity = warPartyEntityRepository.findById(warPartyEntity.getId()).get();
        // Disconnect from session so that the updates on updatedWarPartyEntity are not directly saved in db
        em.detach(updatedWarPartyEntity);
        updatedWarPartyEntity
            .speed(UPDATED_SPEED)
            .capacity(UPDATED_CAPACITY)
            .state(UPDATED_STATE);

        restWarPartyEntityMockMvc.perform(put("/api/war-party-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedWarPartyEntity)))
            .andExpect(status().isOk());

        // Validate the WarPartyEntity in the database
        List<WarPartyEntity> warPartyEntityList = warPartyEntityRepository.findAll();
        assertThat(warPartyEntityList).hasSize(databaseSizeBeforeUpdate);
        WarPartyEntity testWarPartyEntity = warPartyEntityList.get(warPartyEntityList.size() - 1);
        assertThat(testWarPartyEntity.getSpeed()).isEqualTo(UPDATED_SPEED);
        assertThat(testWarPartyEntity.getCapacity()).isEqualTo(UPDATED_CAPACITY);
        assertThat(testWarPartyEntity.getState()).isEqualTo(UPDATED_STATE);
    }

    @Test
    @Transactional
    public void updateNonExistingWarPartyEntity() throws Exception {
        int databaseSizeBeforeUpdate = warPartyEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWarPartyEntityMockMvc.perform(put("/api/war-party-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(warPartyEntity)))
            .andExpect(status().isBadRequest());

        // Validate the WarPartyEntity in the database
        List<WarPartyEntity> warPartyEntityList = warPartyEntityRepository.findAll();
        assertThat(warPartyEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteWarPartyEntity() throws Exception {
        // Initialize the database
        warPartyEntityRepository.saveAndFlush(warPartyEntity);

        int databaseSizeBeforeDelete = warPartyEntityRepository.findAll().size();

        // Delete the warPartyEntity
        restWarPartyEntityMockMvc.perform(delete("/api/war-party-entities/{id}", warPartyEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<WarPartyEntity> warPartyEntityList = warPartyEntityRepository.findAll();
        assertThat(warPartyEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
