package com.graphzero.warmaker.web.rest;

import com.graphzero.warmaker.WarMakerApp;
import com.graphzero.warmaker.domain.WorldEntity;
import com.graphzero.warmaker.domain.FieldEntity;
import com.graphzero.warmaker.repository.WorldEntityRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link WorldEntityResource} REST controller.
 */
@SpringBootTest(classes = WarMakerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class WorldEntityResourceIT {

    private static final String DEFAULT_WORLD_NAME = "AAAAAAAAAA";
    private static final String UPDATED_WORLD_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_WORLD_NUMBER = 1;
    private static final Integer UPDATED_WORLD_NUMBER = 2;

    private static final Integer DEFAULT_HEIGHT = 1;
    private static final Integer UPDATED_HEIGHT = 2;

    private static final Integer DEFAULT_WIDTH = 1;
    private static final Integer UPDATED_WIDTH = 2;

    @Autowired
    private WorldEntityRepository worldEntityRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restWorldEntityMockMvc;

    private WorldEntity worldEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WorldEntity createEntity(EntityManager em) {
        WorldEntity worldEntity = new WorldEntity()
            .worldName(DEFAULT_WORLD_NAME)
            .worldNumber(DEFAULT_WORLD_NUMBER)
            .height(DEFAULT_HEIGHT)
            .width(DEFAULT_WIDTH);
        // Add required entity
        FieldEntity fieldEntity;
        if (TestUtil.findAll(em, FieldEntity.class).isEmpty()) {
            fieldEntity = FieldEntityResourceIT.createEntity(em);
            em.persist(fieldEntity);
            em.flush();
        } else {
            fieldEntity = TestUtil.findAll(em, FieldEntity.class).get(0);
        }
        worldEntity.getFields().add(fieldEntity);
        return worldEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WorldEntity createUpdatedEntity(EntityManager em) {
        WorldEntity worldEntity = new WorldEntity()
            .worldName(UPDATED_WORLD_NAME)
            .worldNumber(UPDATED_WORLD_NUMBER)
            .height(UPDATED_HEIGHT)
            .width(UPDATED_WIDTH);
        // Add required entity
        FieldEntity fieldEntity;
        if (TestUtil.findAll(em, FieldEntity.class).isEmpty()) {
            fieldEntity = FieldEntityResourceIT.createUpdatedEntity(em);
            em.persist(fieldEntity);
            em.flush();
        } else {
            fieldEntity = TestUtil.findAll(em, FieldEntity.class).get(0);
        }
        worldEntity.getFields().add(fieldEntity);
        return worldEntity;
    }

    @BeforeEach
    public void initTest() {
        worldEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createWorldEntity() throws Exception {
        int databaseSizeBeforeCreate = worldEntityRepository.findAll().size();
        // Create the WorldEntity
        restWorldEntityMockMvc.perform(post("/api/world-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(worldEntity)))
            .andExpect(status().isCreated());

        // Validate the WorldEntity in the database
        List<WorldEntity> worldEntityList = worldEntityRepository.findAll();
        assertThat(worldEntityList).hasSize(databaseSizeBeforeCreate + 1);
        WorldEntity testWorldEntity = worldEntityList.get(worldEntityList.size() - 1);
        assertThat(testWorldEntity.getWorldName()).isEqualTo(DEFAULT_WORLD_NAME);
        assertThat(testWorldEntity.getWorldNumber()).isEqualTo(DEFAULT_WORLD_NUMBER);
        assertThat(testWorldEntity.getHeight()).isEqualTo(DEFAULT_HEIGHT);
        assertThat(testWorldEntity.getWidth()).isEqualTo(DEFAULT_WIDTH);
    }

    @Test
    @Transactional
    public void createWorldEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = worldEntityRepository.findAll().size();

        // Create the WorldEntity with an existing ID
        worldEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWorldEntityMockMvc.perform(post("/api/world-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(worldEntity)))
            .andExpect(status().isBadRequest());

        // Validate the WorldEntity in the database
        List<WorldEntity> worldEntityList = worldEntityRepository.findAll();
        assertThat(worldEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllWorldEntities() throws Exception {
        // Initialize the database
        worldEntityRepository.saveAndFlush(worldEntity);

        // Get all the worldEntityList
        restWorldEntityMockMvc.perform(get("/api/world-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(worldEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].worldName").value(hasItem(DEFAULT_WORLD_NAME)))
            .andExpect(jsonPath("$.[*].worldNumber").value(hasItem(DEFAULT_WORLD_NUMBER)))
            .andExpect(jsonPath("$.[*].height").value(hasItem(DEFAULT_HEIGHT)))
            .andExpect(jsonPath("$.[*].width").value(hasItem(DEFAULT_WIDTH)));
    }
    
    @Test
    @Transactional
    public void getWorldEntity() throws Exception {
        // Initialize the database
        worldEntityRepository.saveAndFlush(worldEntity);

        // Get the worldEntity
        restWorldEntityMockMvc.perform(get("/api/world-entities/{id}", worldEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(worldEntity.getId().intValue()))
            .andExpect(jsonPath("$.worldName").value(DEFAULT_WORLD_NAME))
            .andExpect(jsonPath("$.worldNumber").value(DEFAULT_WORLD_NUMBER))
            .andExpect(jsonPath("$.height").value(DEFAULT_HEIGHT))
            .andExpect(jsonPath("$.width").value(DEFAULT_WIDTH));
    }
    @Test
    @Transactional
    public void getNonExistingWorldEntity() throws Exception {
        // Get the worldEntity
        restWorldEntityMockMvc.perform(get("/api/world-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWorldEntity() throws Exception {
        // Initialize the database
        worldEntityRepository.saveAndFlush(worldEntity);

        int databaseSizeBeforeUpdate = worldEntityRepository.findAll().size();

        // Update the worldEntity
        WorldEntity updatedWorldEntity = worldEntityRepository.findById(worldEntity.getId()).get();
        // Disconnect from session so that the updates on updatedWorldEntity are not directly saved in db
        em.detach(updatedWorldEntity);
        updatedWorldEntity
            .worldName(UPDATED_WORLD_NAME)
            .worldNumber(UPDATED_WORLD_NUMBER)
            .height(UPDATED_HEIGHT)
            .width(UPDATED_WIDTH);

        restWorldEntityMockMvc.perform(put("/api/world-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedWorldEntity)))
            .andExpect(status().isOk());

        // Validate the WorldEntity in the database
        List<WorldEntity> worldEntityList = worldEntityRepository.findAll();
        assertThat(worldEntityList).hasSize(databaseSizeBeforeUpdate);
        WorldEntity testWorldEntity = worldEntityList.get(worldEntityList.size() - 1);
        assertThat(testWorldEntity.getWorldName()).isEqualTo(UPDATED_WORLD_NAME);
        assertThat(testWorldEntity.getWorldNumber()).isEqualTo(UPDATED_WORLD_NUMBER);
        assertThat(testWorldEntity.getHeight()).isEqualTo(UPDATED_HEIGHT);
        assertThat(testWorldEntity.getWidth()).isEqualTo(UPDATED_WIDTH);
    }

    @Test
    @Transactional
    public void updateNonExistingWorldEntity() throws Exception {
        int databaseSizeBeforeUpdate = worldEntityRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWorldEntityMockMvc.perform(put("/api/world-entities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(worldEntity)))
            .andExpect(status().isBadRequest());

        // Validate the WorldEntity in the database
        List<WorldEntity> worldEntityList = worldEntityRepository.findAll();
        assertThat(worldEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteWorldEntity() throws Exception {
        // Initialize the database
        worldEntityRepository.saveAndFlush(worldEntity);

        int databaseSizeBeforeDelete = worldEntityRepository.findAll().size();

        // Delete the worldEntity
        restWorldEntityMockMvc.perform(delete("/api/world-entities/{id}", worldEntity.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<WorldEntity> worldEntityList = worldEntityRepository.findAll();
        assertThat(worldEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
