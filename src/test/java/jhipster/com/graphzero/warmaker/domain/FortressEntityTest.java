package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class FortressEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FortressEntity.class);
        FortressEntity fortressEntity1 = new FortressEntity();
        fortressEntity1.setId(1L);
        FortressEntity fortressEntity2 = new FortressEntity();
        fortressEntity2.setId(fortressEntity1.getId());
        assertThat(fortressEntity1).isEqualTo(fortressEntity2);
        fortressEntity2.setId(2L);
        assertThat(fortressEntity1).isNotEqualTo(fortressEntity2);
        fortressEntity1.setId(null);
        assertThat(fortressEntity1).isNotEqualTo(fortressEntity2);
    }
}
