package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class LairEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LairEntity.class);
        LairEntity lairEntity1 = new LairEntity();
        lairEntity1.setId(1L);
        LairEntity lairEntity2 = new LairEntity();
        lairEntity2.setId(lairEntity1.getId());
        assertThat(lairEntity1).isEqualTo(lairEntity2);
        lairEntity2.setId(2L);
        assertThat(lairEntity1).isNotEqualTo(lairEntity2);
        lairEntity1.setId(null);
        assertThat(lairEntity1).isNotEqualTo(lairEntity2);
    }
}
