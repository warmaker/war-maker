package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class FieldEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FieldEntity.class);
        FieldEntity fieldEntity1 = new FieldEntity();
        fieldEntity1.setId(1L);
        FieldEntity fieldEntity2 = new FieldEntity();
        fieldEntity2.setId(fieldEntity1.getId());
        assertThat(fieldEntity1).isEqualTo(fieldEntity2);
        fieldEntity2.setId(2L);
        assertThat(fieldEntity1).isNotEqualTo(fieldEntity2);
        fieldEntity1.setId(null);
        assertThat(fieldEntity1).isNotEqualTo(fieldEntity2);
    }
}
