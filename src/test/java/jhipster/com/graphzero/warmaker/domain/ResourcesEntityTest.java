package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class ResourcesEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ResourcesEntity.class);
        ResourcesEntity resourcesEntity1 = new ResourcesEntity();
        resourcesEntity1.setId(1L);
        ResourcesEntity resourcesEntity2 = new ResourcesEntity();
        resourcesEntity2.setId(resourcesEntity1.getId());
        assertThat(resourcesEntity1).isEqualTo(resourcesEntity2);
        resourcesEntity2.setId(2L);
        assertThat(resourcesEntity1).isNotEqualTo(resourcesEntity2);
        resourcesEntity1.setId(null);
        assertThat(resourcesEntity1).isNotEqualTo(resourcesEntity2);
    }
}
