package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class AllianceEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AllianceEntity.class);
        AllianceEntity allianceEntity1 = new AllianceEntity();
        allianceEntity1.setId(1L);
        AllianceEntity allianceEntity2 = new AllianceEntity();
        allianceEntity2.setId(allianceEntity1.getId());
        assertThat(allianceEntity1).isEqualTo(allianceEntity2);
        allianceEntity2.setId(2L);
        assertThat(allianceEntity1).isNotEqualTo(allianceEntity2);
        allianceEntity1.setId(null);
        assertThat(allianceEntity1).isNotEqualTo(allianceEntity2);
    }
}
