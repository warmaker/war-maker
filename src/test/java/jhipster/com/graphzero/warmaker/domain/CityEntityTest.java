package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class CityEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CityEntity.class);
        CityEntity cityEntity1 = new CityEntity();
        cityEntity1.setId(1L);
        CityEntity cityEntity2 = new CityEntity();
        cityEntity2.setId(cityEntity1.getId());
        assertThat(cityEntity1).isEqualTo(cityEntity2);
        cityEntity2.setId(2L);
        assertThat(cityEntity1).isNotEqualTo(cityEntity2);
        cityEntity1.setId(null);
        assertThat(cityEntity1).isNotEqualTo(cityEntity2);
    }
}
