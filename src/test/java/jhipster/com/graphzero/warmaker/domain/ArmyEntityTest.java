package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class ArmyEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ArmyEntity.class);
        ArmyEntity armyEntity1 = new ArmyEntity();
        armyEntity1.setId(1L);
        ArmyEntity armyEntity2 = new ArmyEntity();
        armyEntity2.setId(armyEntity1.getId());
        assertThat(armyEntity1).isEqualTo(armyEntity2);
        armyEntity2.setId(2L);
        assertThat(armyEntity1).isNotEqualTo(armyEntity2);
        armyEntity1.setId(null);
        assertThat(armyEntity1).isNotEqualTo(armyEntity2);
    }
}
