package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class PlayerRelationsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlayerRelations.class);
        PlayerRelations playerRelations1 = new PlayerRelations();
        playerRelations1.setId(1L);
        PlayerRelations playerRelations2 = new PlayerRelations();
        playerRelations2.setId(playerRelations1.getId());
        assertThat(playerRelations1).isEqualTo(playerRelations2);
        playerRelations2.setId(2L);
        assertThat(playerRelations1).isNotEqualTo(playerRelations2);
        playerRelations1.setId(null);
        assertThat(playerRelations1).isNotEqualTo(playerRelations2);
    }
}
