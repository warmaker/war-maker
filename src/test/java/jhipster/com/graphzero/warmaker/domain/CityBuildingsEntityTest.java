package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class CityBuildingsEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CityBuildingsEntity.class);
        CityBuildingsEntity cityBuildingsEntity1 = new CityBuildingsEntity();
        cityBuildingsEntity1.setId(1L);
        CityBuildingsEntity cityBuildingsEntity2 = new CityBuildingsEntity();
        cityBuildingsEntity2.setId(cityBuildingsEntity1.getId());
        assertThat(cityBuildingsEntity1).isEqualTo(cityBuildingsEntity2);
        cityBuildingsEntity2.setId(2L);
        assertThat(cityBuildingsEntity1).isNotEqualTo(cityBuildingsEntity2);
        cityBuildingsEntity1.setId(null);
        assertThat(cityBuildingsEntity1).isNotEqualTo(cityBuildingsEntity2);
    }
}
