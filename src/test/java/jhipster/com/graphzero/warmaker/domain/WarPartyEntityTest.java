package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class WarPartyEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WarPartyEntity.class);
        WarPartyEntity warPartyEntity1 = new WarPartyEntity();
        warPartyEntity1.setId(1L);
        WarPartyEntity warPartyEntity2 = new WarPartyEntity();
        warPartyEntity2.setId(warPartyEntity1.getId());
        assertThat(warPartyEntity1).isEqualTo(warPartyEntity2);
        warPartyEntity2.setId(2L);
        assertThat(warPartyEntity1).isNotEqualTo(warPartyEntity2);
        warPartyEntity1.setId(null);
        assertThat(warPartyEntity1).isNotEqualTo(warPartyEntity2);
    }
}
