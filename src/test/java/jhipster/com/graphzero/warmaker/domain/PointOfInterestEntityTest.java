package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class PointOfInterestEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PointOfInterestEntity.class);
        PointOfInterestEntity pointOfInterestEntity1 = new PointOfInterestEntity();
        pointOfInterestEntity1.setId(1L);
        PointOfInterestEntity pointOfInterestEntity2 = new PointOfInterestEntity();
        pointOfInterestEntity2.setId(pointOfInterestEntity1.getId());
        assertThat(pointOfInterestEntity1).isEqualTo(pointOfInterestEntity2);
        pointOfInterestEntity2.setId(2L);
        assertThat(pointOfInterestEntity1).isNotEqualTo(pointOfInterestEntity2);
        pointOfInterestEntity1.setId(null);
        assertThat(pointOfInterestEntity1).isNotEqualTo(pointOfInterestEntity2);
    }
}
