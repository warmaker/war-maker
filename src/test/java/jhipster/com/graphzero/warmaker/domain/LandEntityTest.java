package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class LandEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LandEntity.class);
        LandEntity landEntity1 = new LandEntity();
        landEntity1.setId(1L);
        LandEntity landEntity2 = new LandEntity();
        landEntity2.setId(landEntity1.getId());
        assertThat(landEntity1).isEqualTo(landEntity2);
        landEntity2.setId(2L);
        assertThat(landEntity1).isNotEqualTo(landEntity2);
        landEntity1.setId(null);
        assertThat(landEntity1).isNotEqualTo(landEntity2);
    }
}
