package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class ResearchesEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ResearchesEntity.class);
        ResearchesEntity researchesEntity1 = new ResearchesEntity();
        researchesEntity1.setId(1L);
        ResearchesEntity researchesEntity2 = new ResearchesEntity();
        researchesEntity2.setId(researchesEntity1.getId());
        assertThat(researchesEntity1).isEqualTo(researchesEntity2);
        researchesEntity2.setId(2L);
        assertThat(researchesEntity1).isNotEqualTo(researchesEntity2);
        researchesEntity1.setId(null);
        assertThat(researchesEntity1).isNotEqualTo(researchesEntity2);
    }
}
