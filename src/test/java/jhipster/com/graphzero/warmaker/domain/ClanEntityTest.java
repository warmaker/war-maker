package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class ClanEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClanEntity.class);
        ClanEntity clanEntity1 = new ClanEntity();
        clanEntity1.setId(1L);
        ClanEntity clanEntity2 = new ClanEntity();
        clanEntity2.setId(clanEntity1.getId());
        assertThat(clanEntity1).isEqualTo(clanEntity2);
        clanEntity2.setId(2L);
        assertThat(clanEntity1).isNotEqualTo(clanEntity2);
        clanEntity1.setId(null);
        assertThat(clanEntity1).isNotEqualTo(clanEntity2);
    }
}
