package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class HeroEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HeroEntity.class);
        HeroEntity heroEntity1 = new HeroEntity();
        heroEntity1.setId(1L);
        HeroEntity heroEntity2 = new HeroEntity();
        heroEntity2.setId(heroEntity1.getId());
        assertThat(heroEntity1).isEqualTo(heroEntity2);
        heroEntity2.setId(2L);
        assertThat(heroEntity1).isNotEqualTo(heroEntity2);
        heroEntity1.setId(null);
        assertThat(heroEntity1).isNotEqualTo(heroEntity2);
    }
}
