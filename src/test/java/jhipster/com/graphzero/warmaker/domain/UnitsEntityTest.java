package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class UnitsEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UnitsEntity.class);
        UnitsEntity unitsEntity1 = new UnitsEntity();
        unitsEntity1.setId(1L);
        UnitsEntity unitsEntity2 = new UnitsEntity();
        unitsEntity2.setId(unitsEntity1.getId());
        assertThat(unitsEntity1).isEqualTo(unitsEntity2);
        unitsEntity2.setId(2L);
        assertThat(unitsEntity1).isNotEqualTo(unitsEntity2);
        unitsEntity1.setId(null);
        assertThat(unitsEntity1).isNotEqualTo(unitsEntity2);
    }
}
