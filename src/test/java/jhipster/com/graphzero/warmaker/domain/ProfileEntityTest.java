package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class ProfileEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProfileEntity.class);
        ProfileEntity profileEntity1 = new ProfileEntity();
        profileEntity1.setId(1L);
        ProfileEntity profileEntity2 = new ProfileEntity();
        profileEntity2.setId(profileEntity1.getId());
        assertThat(profileEntity1).isEqualTo(profileEntity2);
        profileEntity2.setId(2L);
        assertThat(profileEntity1).isNotEqualTo(profileEntity2);
        profileEntity1.setId(null);
        assertThat(profileEntity1).isNotEqualTo(profileEntity2);
    }
}
