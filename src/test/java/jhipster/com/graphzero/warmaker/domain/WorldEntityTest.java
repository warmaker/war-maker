package com.graphzero.warmaker.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.graphzero.warmaker.web.rest.TestUtil;

public class WorldEntityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WorldEntity.class);
        WorldEntity worldEntity1 = new WorldEntity();
        worldEntity1.setId(1L);
        WorldEntity worldEntity2 = new WorldEntity();
        worldEntity2.setId(worldEntity1.getId());
        assertThat(worldEntity1).isEqualTo(worldEntity2);
        worldEntity2.setId(2L);
        assertThat(worldEntity1).isNotEqualTo(worldEntity2);
        worldEntity1.setId(null);
        assertThat(worldEntity1).isNotEqualTo(worldEntity2);
    }
}
