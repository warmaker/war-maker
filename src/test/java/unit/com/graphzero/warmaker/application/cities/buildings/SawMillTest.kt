package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.UnitTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@UnitTest
internal class SawMillTest {
    @Test
    @DisplayName("Should properly increase stats")
    fun test1() {
        // given
        val level = 0
        val basePointValue: Long = 10
        val baseToughness: Long = 15
        val baseBuildSpeed: Long = 1
        val sawMill =
            SawMill(InfrastructureBaseStats(basePointValue, baseToughness, baseBuildSpeed.toDouble()), level, 0)
        // when
        sawMill.increaseLevel()
        // then
        Assertions.assertEquals(level + 1, sawMill.level)
        org.assertj.core.api.Assertions.assertThat(sawMill.pointValue).isGreaterThan(basePointValue)
        org.assertj.core.api.Assertions.assertThat(sawMill.toughness).isGreaterThan(baseToughness.toDouble())
    }
}
