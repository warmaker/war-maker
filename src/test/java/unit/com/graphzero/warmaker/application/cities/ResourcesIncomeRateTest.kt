package com.graphzero.warmaker.application.cities

import com.google.common.collect.ImmutableMap
import com.google.common.collect.Maps
import com.graphzero.warmaker.UnitTest
import com.graphzero.warmaker.application.cities.ResourcesIncomeRate.Companion.of
import com.graphzero.warmaker.domain.enumeration.ResourceType
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@UnitTest
internal class ResourcesIncomeRateTest {
    @Test
    @DisplayName("Should create correct object from income map")
    fun test1() {
        // given
        // when
        val incomeRate = basicResourcesIncome()
        // then
        Assertions.assertThat(incomeRate.denarsIncome).isEqualTo(500.0)
        Assertions.assertThat(incomeRate.woodIncome).isEqualTo(300.0)
        Assertions.assertThat(incomeRate.stoneIncome).isEqualTo(200.0)
        Assertions.assertThat(incomeRate.steelIncome).isEqualTo(100.0)
        Assertions.assertThat(incomeRate.foodIncome).isEqualTo(50.0)
        Assertions.assertThat(incomeRate.faithIncome).isEqualTo(10.0)
    }

    companion object {
        fun basicResourcesIncome(): ResourcesIncomeRate {
            val income = Maps.newHashMap(
                ImmutableMap.of(
                    ResourceType.DENARS, 500.0,
                    ResourceType.WOOD, 300.0,
                    ResourceType.STONE, 200.0,
                    ResourceType.STEEL, 100.0,
                    ResourceType.FOOD, 50.0
                )
            )
            income[ResourceType.FAITH] = 10.0
            return of(income)
        }
    }
}
