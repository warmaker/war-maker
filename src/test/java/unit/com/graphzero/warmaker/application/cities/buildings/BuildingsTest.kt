package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.UnitTest
import com.graphzero.warmaker.application.cities.CityScheduledEvents
import com.graphzero.warmaker.application.resources.ResourcesValue.Companion.empty
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@UnitTest
internal class BuildingsTest {
    private val cityGenerator = CityGenerator()

    @Test
    @DisplayName("Shouldn't update barracks if City Hall level is too low")
    fun scheduleBuildingUpgrade() {
        // given
        val buildings = cityGenerator.testBuildings
            .cityHall(CityHall(CityGenerator.baseStats, 1, 0, 1, -1, CityScheduledEvents()))
            .createBuildings()
        // when
        val result = buildings.scheduleBuildingUpgrade(buildings.barracks!!, empty())
        // then
        Assertions.assertThat(result.isLeft).isTrue
        Assertions.assertThat(result.left.reason).isEqualTo(BuildingUpgradeProhibited.Reason.REQUIREMENTS_NOT_MATCHED)
    }
}
