package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.UnitTest
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneySagaGenerator
import com.graphzero.warmaker.generators.PlayerGenerator
import com.graphzero.warmaker.generators.WarPartyGenerator
import com.graphzero.warmaker.utils.EventsUtils
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.provider.Arguments
import java.util.stream.Stream

@UnitTest
internal class LairTest {
    private val warPartyGenerator = WarPartyGenerator()
    @Test
    @DisplayName("Battling in a lair while reaching destination should generate correct events for War Party")
    fun battleShouldCreateEventsForWarParty() {
        // given
        val lair = RegionsGenerator.testLair
        val warParty = warPartyGenerator.mediumTestWarParty.build()
        val saga = WarPartyJourneySagaGenerator.getAttackSaga(
            1,
            warParty.id,
            listOfNotNull(Coordinates(0, 0), lair.coordinates)
        )
        saga.moveToNextStop()
        // when
        val warPartyJourneyConclusion = warParty.move(lair, saga)
        // then
        Assertions.assertThat(warParty.getDomainEvents().size).isEqualTo(2)
        val battleEvent = EventsUtils.getOneBattleEvent(warParty).get()
        Assertions.assertThat(battleEvent.uuid).isNotNull
    }

    @Test
    @DisplayName("Battling in a lair while reaching destination should generate correct events for Lair.")
    fun battleShouldCreateEventsForLair() {
        // given
        val lair = RegionsGenerator.testLair
        val warParty = warPartyGenerator.mediumTestWarParty.build()
        val saga = WarPartyJourneySagaGenerator.getAttackSaga(
            1,
            warParty.id,
            listOfNotNull(Coordinates(0, 0), lair.coordinates)
        )
        saga.moveToNextStop()
        // when
        val warPartyJourneyConclusion = warParty.move(lair, saga)
        // then
        Assertions.assertThat(lair.getDomainEvents().size).isEqualTo(3)
        val battleEvent = EventsUtils.getOneBattleEvent(lair).get()
        Assertions.assertThat(battleEvent.uuid).isNotNull
    }

    companion object {
        private val playerGenerator = PlayerGenerator()
        private fun players(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(playerGenerator.neutralPlayer),
                Arguments.of(playerGenerator.getPlayer()),
                Arguments.of(playerGenerator.enemyPlayer),
                Arguments.of(playerGenerator.allyPlayer)
            )
        }
    }
}
