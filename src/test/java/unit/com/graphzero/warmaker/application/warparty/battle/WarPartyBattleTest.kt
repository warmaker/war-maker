package com.graphzero.warmaker.application.warparty.battle

import com.graphzero.warmaker.UnitTest
import com.graphzero.warmaker.application.ddd.AggregateRootLogic
import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.application.warparty.value.WarPartyAnnihilatedEvent
import com.graphzero.warmaker.generators.PlayerGenerator
import com.graphzero.warmaker.utils.EventsUtils
import com.graphzero.warmaker.utils.TestConfigLoader
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@UnitTest
internal class WarPartyBattleTest {

    @Test
    @DisplayName("Battle should generate correct domain events for defender war party")
    fun battleShouldGenerateCorrectDomainEventsForDefenderWarParty() {
        // given
        val attacker = createWarPartyBattle(1L, 1, 100, 100, 15, 5)
        val defender = createWarPartyBattle(2L, 2, 10, 0, 150, 50)
        // when
        defender.battle(attacker, null)
        // then
        assertThat(defender.getDomainEvents()).hasSize(2)
        assertThat(defender.getDomainEvents().stream().map { it.warMakerEventType })
            .contains(WarMakerArmyEvent.WAR_PARTY_BATTLES)
        assertThat(defender.getDomainEvents().stream().map { it.warMakerEventType })
            .contains(WarMakerArmyEvent.WAR_PARTY_ANNIHILATED)
    }

    @Test
    @DisplayName("Battle should generate correct domain events for attacker war party")
    fun battleShouldGenerateCorrectDomainEventsForAttackerWarParty() {
        // given
        val attacker = createWarPartyBattle(1L, 1, 100, 100, 15, 5)
        val defender = createWarPartyBattle(2L, 2, 10, 0, 150, 50)
        // when
        defender.battle(attacker, null)
        // then
        assertThat(attacker.getDomainEvents()).hasSize(1)
        val warPartyBattleEvent = attacker.getDomainEvents()[0]
        assertThat(warPartyBattleEvent).isInstanceOf(WarPartyBattleEvent::class.java)
        assertThat(warPartyBattleEvent.warMakerEventType).isEqualTo(WarMakerArmyEvent.WAR_PARTY_BATTLES)
    }

    @Test
    @DisplayName("War party should generate correct units loses")
    fun warPartyShouldGenerateCorrectUnitsLoses() {
        // given
        val attacker = createWarPartyBattle(1L, 1, 100, 100, 15, 5)
        val defender = createWarPartyBattle(2L, 2, 10, 0, 150, 50)
        // when
        defender.battle(attacker, null)
        // then
        val loses = EventsUtils.getOneBattleEvent(attacker).get().unitsLoses
        assertThat(loses.spearman).isBetween(1, 100)
        assertThat(loses.swordsman).isBetween(1, 100)
        assertThat(loses.archers).isZero
        assertThat(loses.crossbowman).isZero
    }

    @Test
    @DisplayName("Draw should result in annihilation")
    fun drawShouldResultInAnnihilation() {
        // given
        val attacker = createWarPartyBattle(1L, 1, 10, 10, 10, 10)
        val defender = createWarPartyBattle(1L, 1, 10, 10, 10, 10)
        // when
        val battleReport = defender.battle(attacker, null)
        // then
        assertThat(battleReport.attackerLoses).isEqualTo(attacker.initialUnits.value())
        assertThat(battleReport.defenderLoses).isEqualTo(defender.initialUnits.value())
        assertThat(battleReport.battleResult).isEqualTo(BattleResult.DRAW)
        EventsUtils.getOneEvent(attacker, WarPartyAnnihilatedEvent::class.java)
        EventsUtils.getOneEvent(defender, WarPartyAnnihilatedEvent::class.java)
    }

    @Test
    @DisplayName("War party should generate correct remaining units")
    fun warPartyShouldGenerateCorrectRemainingUnits() {
        // given
        val attacker = createWarPartyBattle(1L, 1, 100, 100, 15, 5)
        val defender = createWarPartyBattle(2L, 2, 10, 0, 150, 50)
        val attackerUnits = attacker.initialUnits.units.copy()
        val defenderUnits = defender.initialUnits.units.copy()
        // when
        defender.battle(attacker, null)
        // then
        val attackerBattleEvent = EventsUtils.getOneBattleEvent(attacker).get()
        val expectedRemainingUnits = attackerUnits.minus(attackerBattleEvent.unitsLoses)
        assertThat(attackerBattleEvent.remainingUnits).isEqualTo(expectedRemainingUnits)
        val defenderBattleEvent = EventsUtils.getOneBattleEvent(defender).get()
        val defenderExpectedRemainingUnits = defenderUnits.minus(defenderBattleEvent.unitsLoses)
        assertThat(defenderBattleEvent.remainingUnits).isEqualTo(defenderExpectedRemainingUnits)
    }

    companion object {
        val playerGenerator = PlayerGenerator()
        fun createWarPartyBattle(
            id: Long,
            unitsId: Int,
            spearman: Int,
            swordsman: Int,
            archers: Int,
            crossbowman: Int
        ): WarPartyBattle {
            return WarPartyBattle(
                AggregateRootLogic(id),
                HumanUnits(unitsId.toLong(), spearman, swordsman, archers, crossbowman),
                playerGenerator.enemyPlayer,
                TestConfigLoader.testConfig.getUnits()
            )
        }
    }
}
