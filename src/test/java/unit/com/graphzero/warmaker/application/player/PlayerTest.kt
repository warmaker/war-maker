package com.graphzero.warmaker.application.player

import com.graphzero.warmaker.generators.PlayerGenerator
import com.graphzero.warmaker.UnitTest
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.provider.Arguments
import java.util.stream.Stream

@UnitTest
internal class PlayerTest {
    @Test
    @DisplayName("Should correctly group enemies")
    fun groupingTest() {
        // given
        val player = playerGenerator.enemyPlayer
        // when && then
        Assertions.assertThat(player.enemies.size).isEqualTo(2)
        Assertions.assertThat(player.enemies).contains(playerGenerator.getPlayer().id)
        Assertions.assertThat(player.enemies).contains(playerGenerator.allyPlayer.id)
    }

    @Test
    @DisplayName("Should correctly group allies")
    fun groupingTest1() {
        // given
        val player = playerGenerator.getPlayer()
        // when && then
        Assertions.assertThat(player.allies.size).isEqualTo(1)
        Assertions.assertThat(player.allies).contains(playerGenerator.allyPlayer.id)
    }

    // given
    @get:DisplayName("Should correctly identify enemies")
    @get:Test
    val isEnemyTest:
    // when && then
            Unit
        get() {
            // given
            val player = playerGenerator.enemyPlayer
            // when && then
            Assertions.assertThat(player.enemies.size).isEqualTo(2)
            Assertions.assertThat(player.isEnemy(playerGenerator.getPlayer().id)).isTrue
            Assertions.assertThat(player.isEnemy(playerGenerator.allyPlayer.id)).isTrue
        }

    // given
    @get:DisplayName("Should correctly identify allies")
    @get:Test
    val isAllyTest:
    // when && then
            Unit
        get() {
            // given
            val player = playerGenerator.getPlayer()
            // when && then
            Assertions.assertThat(player.allies.size).isEqualTo(1)
            Assertions.assertThat(player.isAlly(playerGenerator.allyPlayer.id)).isTrue
        }

    companion object {
        private val playerGenerator = PlayerGenerator()
        private fun players(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(playerGenerator.neutralPlayer),
                Arguments.of(playerGenerator.getPlayer()),
                Arguments.of(playerGenerator.enemyPlayer),
                Arguments.of(playerGenerator.allyPlayer)
            )
        }
    }
}
