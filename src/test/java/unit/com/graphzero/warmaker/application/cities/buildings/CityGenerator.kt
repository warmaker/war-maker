package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.application.cities.CityInfo
import com.graphzero.warmaker.application.cities.CityScheduledEvents
import com.graphzero.warmaker.application.cities.buildings.Buildings.BuildingsBuilder
import com.graphzero.warmaker.application.cities.region.CityRegion
import com.graphzero.warmaker.application.player.Player
import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.world.Coordinates
import com.graphzero.warmaker.domain.enumeration.CityType
import java.time.ZonedDateTime
import java.util.List

class CityGenerator {
    val cityBuildings: CityBuildings
        get() = CityBuildings(
            1L,
            CityInfo(2, 3, "Test City", CityType.PLAYER, 1.0, 1.0),
            testBuildings.createBuildings(),
            Resources(5, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0),
            ZonedDateTime.now(),
            CityScheduledEvents()
        )

    val cityRegion: CityRegion
        get() = CityRegion(
            Coordinates(0, 0),
            1L,
            CityInfo(2, 3, "Test City", CityType.PLAYER, 1.0, 1.0),
            testBuildings.createBuildings(),
            Resources(5, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0),
            ZonedDateTime.now(),
            listOf()
        )

    fun getCityRegion(player: Player?): CityRegion {
        return CityRegion(
            Coordinates(0, 0),
            1L,
            CityInfo(player!!.id, 3, "Test City", CityType.PLAYER, 1.0, 1.0),
            testBuildings.createBuildings(),
            Resources(5, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0),
            ZonedDateTime.now(),
            listOf()
        )
    }

    val testBuildings: BuildingsBuilder
        get() = BuildingsBuilder()
            .id(4)
            .sawMill(SawMill(baseStats, 1, 0))
            .oreMine(OreMine(baseStats, 1, 0))
            .goldMine(GoldMine(baseStats, 0, 0))
            .ironworks(IronWorks(baseStats, 0, 0))
            .cityHall(CityHall(baseStats, 1, 0, 1, 1, CityScheduledEvents()))
            .university(University(baseStats, 0, 0))
            .forge(Forge(baseStats, 0, 0))
            .barracks(Barracks(baseStats, 0, 0))

    companion object {
        val baseStats = InfrastructureBaseStats(1, 2, 3.0)
    }
}
