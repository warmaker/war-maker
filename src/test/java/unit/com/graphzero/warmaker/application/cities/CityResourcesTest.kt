package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.UnitTest
import com.graphzero.warmaker.application.resources.ResourcesValue
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.Month
import java.time.ZoneId

@UnitTest
internal class CityResourcesTest {
    @Test
    fun updateResources() {
        // given
        val cityResources = getCityResources(500)
        // when
        // then
    }

    @Test
    fun raiseIncome() {
        // given
        val cityResources = getCityResources(500)
        // when
        // then
    }

    @Test
    @DisplayName("Should have enough resources.")
    fun hasEnough() {
        // given
        val cityResources = getCityResources(500)
        // when
        val hasEnough = cityResources.hasEnough(ResourcesValue(499.0, 499.0, 499.0, 499.0, 499.0, 499.0))
        // then
        Assertions.assertThat(hasEnough).isTrue
    }

    @Test
    @DisplayName("Should have enough resources.")
    fun hasEnough1() {
        // given
        val cityResources = getCityResources(500)
        // when
        val hasEnough = cityResources.hasEnough(ResourcesValue(500.0, 500.0, 500.0, 500.0, 500.0, 500.0))
        // then
        Assertions.assertThat(hasEnough).isTrue
    }

    @Test
    @DisplayName("Shouldn't have enough resources.")
    fun hasEnough2() {
        // given
        val cityResources = getCityResources(500)
        // when
        val hasEnough = cityResources.hasEnough(ResourcesValue(500.0, 500.0, 501.0, 500.0, 500.0, 500.0))
        // then
        Assertions.assertThat(hasEnough).isFalse
    }

    @Test
    @DisplayName("Should substract resources.")
    fun substract() {
        // given
        val cityResources = getCityResources(500)
        // when
        cityResources.subtract(ResourcesValue(500.0, 500.0, 500.0, 500.0, 500.0, 500.0))
        // then
        Assertions.assertThat(cityResources.storedResources.denars).isEqualTo(0.0)
        Assertions.assertThat(cityResources.storedResources.wood).isEqualTo(0.0)
        Assertions.assertThat(cityResources.storedResources.stone).isEqualTo(0.0)
        Assertions.assertThat(cityResources.storedResources.steel).isEqualTo(0.0)
        Assertions.assertThat(cityResources.storedResources.food).isEqualTo(0.0)
    }

    @Test
    @DisplayName("Shouldn't substract resources.")
    fun substract1() {
        // given
        val resourcesAmount: Long = 500
        val amount1: Long = 501
        val cityResources = getCityResources(resourcesAmount)
        // when
        val substract = cityResources.subtract(
            ResourcesValue(
                amount1.toDouble(),
                amount1.toDouble(),
                amount1.toDouble(),
                amount1.toDouble(),
                amount1.toDouble(),
                amount1.toDouble()
            )
        )
        // then
        Assertions.assertThat(substract).isFalse
        Assertions.assertThat(cityResources.storedResources.denars).isEqualTo(resourcesAmount.toDouble())
        Assertions.assertThat(cityResources.storedResources.wood).isEqualTo(resourcesAmount.toDouble())
        Assertions.assertThat(cityResources.storedResources.stone).isEqualTo(resourcesAmount.toDouble())
        Assertions.assertThat(cityResources.storedResources.steel).isEqualTo(resourcesAmount.toDouble())
        Assertions.assertThat(cityResources.storedResources.food).isEqualTo(resourcesAmount.toDouble())
    }

    @Test
    fun addLoot() {
        // given
        val cityResources = getCityResources(500)
        // when
        // then
    }

    private fun getCityResources(amount: Long): CityResources {
        val ldt = LocalDateTime.of(2016, Month.AUGUST, 22, 14, 30)
        return CityResources(
            ResourcesTest.constantResources(amount), ResourcesIncomeRateTest.basicResourcesIncome(),
            ldt.atZone(ZoneId.of("Europe/Paris"))
        )
    }
}
