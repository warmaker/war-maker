package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.UnitTest
import com.graphzero.warmaker.application.army.value.RecruitUnitOrder
import com.graphzero.warmaker.application.army.value.RecruitmentOrders
import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.time.TimeConfiguration
import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.application.warparty.value.HumanUnits.Companion.concatOf
import com.graphzero.warmaker.application.world.Coordinates
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.List

@UnitTest
internal class ArmyTest {
    @Test
    @DisplayName("Should do nothing when no events are supplied")
    fun recruit() {
        // given
        val amountOfUnits = 100
        val army = Army(1, getHumanUnitsList(amountOfUnits), ArrayList(), Coordinates(1, 1))
        // when
        // then
        Assertions.assertThat(army.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits)
    }

    @Test
    @DisplayName("Should do nothing when event order is to recruit 0 units")
    fun recruit1() {
        // given
        val amountOfUnits = 100
        val recruitmentTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        val army = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(getRecruitmentOrder(1, 0, recruitmentTime)),
            recruitmentTime.plus(100, ChronoUnit.SECONDS), Coordinates(1, 1)
        )
        // when
        // then
        Assertions.assertThat(army.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits)
    }

    @Test
    @DisplayName("Should recruit all units after many more seconds passed")
    fun recruit2() {
        // given
        val amountOfUnits = 100
        val unitsToRecruit = 10
        val recruitmentTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        val army = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(getRecruitmentOrder(1, unitsToRecruit, recruitmentTime)),
            recruitmentTime.plus(10000, ChronoUnit.SECONDS), Coordinates(1, 1)
        )
        // when
        // then
        Assertions.assertThat(army.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits + unitsToRecruit)
        Assertions.assertThat(army.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits + unitsToRecruit)
        Assertions.assertThat(army.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits + unitsToRecruit)
        Assertions.assertThat(army.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits + unitsToRecruit)
    }

    @Test
    @DisplayName("Should recruit some units after exact seconds passed")
    fun recruit3() {
        // given
        val amountOfUnits = 100
        val unitsToRecruit = 10
        val recruitmentTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        val army = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(getRecruitmentOrder(1, unitsToRecruit, recruitmentTime)),
            recruitmentTime.plus(400, ChronoUnit.SECONDS), Coordinates(1, 1)
        )
        // when
        // then
        Assertions.assertThat(army.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits + unitsToRecruit)
        Assertions.assertThat(army.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits + unitsToRecruit)
        Assertions.assertThat(army.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits + unitsToRecruit)
        Assertions.assertThat(army.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits + unitsToRecruit)
    }

    @Test
    @DisplayName("Should recruit one units after many more seconds passed")
    fun recruit4() {
        // given
        val amountOfUnits = 100
        val unitsToRecruit = 10
        val recruitmentTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        val army = Army(
            1,
            getHumanUnitsList(amountOfUnits),
            listOf(getRecruitmentOrderOnlyArchers(1, unitsToRecruit, recruitmentTime)),
            recruitmentTime.plus(400, ChronoUnit.SECONDS),
            Coordinates(1, 1)
        )
        // when
        // then
        Assertions.assertThat(army.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits + unitsToRecruit)
        Assertions.assertThat(army.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits)
    }

    @Test
    @DisplayName("Should recruit some units when not enough seconds are passed")
    fun recruit5() {
        // given
        val amountOfUnits = 100
        val unitsToRecruit = 10
        val recruitmentTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        val army = Army(
            1,
            getHumanUnitsList(amountOfUnits),
            listOf(getRecruitmentOrderOnlyArchers(1, unitsToRecruit, recruitmentTime)),
            recruitmentTime.plus(60, ChronoUnit.SECONDS),
            Coordinates(1, 1)
        )
        // when
        // then
        Assertions.assertThat(army.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits + 6)
        Assertions.assertThat(army.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits)
    }

    @Test
    @DisplayName("Should recruit overlapping units")
    fun recruit6() {
        // given
        val amountOfUnits = 100
        val unitsToRecruit = 10
        val recruitmentTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        val event = getRecruitmentOrderOnlySpearmanAndSwordsman(1, unitsToRecruit, 6, recruitmentTime)
        val army = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(event),
            recruitmentTime.plus(160, ChronoUnit.SECONDS), Coordinates(1, 1)
        )
        // when
        // then
        Assertions.assertThat(army.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits + unitsToRecruit)
        Assertions.assertThat(army.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits + 6)
        Assertions.assertThat(army.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits)
        Assertions.assertThat(event.recruitmentOrders.recruitSwordsman.isFinished).isTrue
        Assertions.assertThat(event.recruitmentOrders.recruitSwordsman.timeSpendOnRecruiting).isEqualTo(60)
        Assertions.assertThat(event.recruitmentOrders.recruitSwordsman.remainingRecruitmentTime).isEqualTo(0)
    }

    @Test
    @DisplayName("Should recruit incomplete overlapping units")
    fun recruit6v1() {
        // given
        val amountOfUnits = 100
        val unitsToRecruit = 10
        val recruitmentTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        val event = getRecruitmentOrderOnlySpearmanAndSwordsman(1, unitsToRecruit, 10, recruitmentTime)
        val army = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(event),
            recruitmentTime.plus(160, ChronoUnit.SECONDS), Coordinates(1, 1)
        )
        // when
        // then
        Assertions.assertThat(army.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits + unitsToRecruit)
        Assertions.assertThat(army.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits + 6)
        Assertions.assertThat(army.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits)
        Assertions.assertThat(event.recruitmentOrders.recruitSwordsman.isFinished).isFalse
        Assertions.assertThat(event.recruitmentOrders.recruitSwordsman.timeSpendOnRecruiting).isEqualTo(60)
        Assertions.assertThat(event.recruitmentOrders.recruitSwordsman.remainingRecruitmentTime).isEqualTo(40)
    }

    @Test
    @DisplayName("Should take into account already passed seconds ")
    fun recruit7() {
        // given
        val amountOfUnits = 100
        val unitsToRecruit = 1
        var recruitmentTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        val event = getRecruitmentOrderOnlySpearman(1, unitsToRecruit, recruitmentTime)
        val army = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(event),
            recruitmentTime, Coordinates(1, 1)
        )
        recruitmentTime = recruitmentTime.plus(2, ChronoUnit.SECONDS)
        val army1 = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(event),
            recruitmentTime, Coordinates(1, 1)
        )
        recruitmentTime = recruitmentTime.plus(2, ChronoUnit.SECONDS)
        val army2 = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(event),
            recruitmentTime, Coordinates(1, 1)
        )
        recruitmentTime = recruitmentTime.plus(2, ChronoUnit.SECONDS)
        val army3 = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(event),
            recruitmentTime, Coordinates(1, 1)
        )
        recruitmentTime = recruitmentTime.plus(2, ChronoUnit.SECONDS)
        val army4 = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(event),
            recruitmentTime, Coordinates(1, 1)
        )
        recruitmentTime = recruitmentTime.plus(2, ChronoUnit.SECONDS)
        val army5 = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(event),
            recruitmentTime, Coordinates(1, 1)
        )
        // when
        // then
        Assertions.assertThat(army5.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits + 1)
        Assertions.assertThat(army5.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army5.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits)
        Assertions.assertThat(army5.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits)
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.isFinished).isTrue
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.timeSpendOnRecruiting).isEqualTo(10)
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.remainingRecruitmentTime).isEqualTo(0)
    }

    @Test
    @DisplayName("Should proccess two events sequentially ")
    fun recruit8() {
        // given
        val amountOfUnits = 100
        val unitsToRecruit = 10
        val recruitmentTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        val event = getRecruitmentOrderOnlySpearman(1, unitsToRecruit, recruitmentTime)
        val event1 = getRecruitmentOrderOnlySpearman(1, unitsToRecruit, recruitmentTime)
        val army = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(event, event1),
            recruitmentTime.plus(150, ChronoUnit.SECONDS), Coordinates(1, 1)
        )
        // when
        // then
        Assertions.assertThat(army.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits + 15)
        Assertions.assertThat(army.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits)
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.isFinished).isTrue
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.timeSpendOnRecruiting).isEqualTo(100)
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.remainingRecruitmentTime).isEqualTo(0)
        Assertions.assertThat(event1.recruitmentOrders.recruitSpearman.isFinished).isFalse
        Assertions.assertThat(event1.recruitmentOrders.recruitSpearman.timeSpendOnRecruiting).isEqualTo(50)
        Assertions.assertThat(event1.recruitmentOrders.recruitSpearman.remainingRecruitmentTime).isEqualTo(50)
    }

    @Test
    @DisplayName("Shouldn't start next event when first isn't finished ")
    fun recruit9() {
        // given
        val amountOfUnits = 100
        val unitsToRecruit = 10
        val recruitmentTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        val event = getRecruitmentOrderOnlySpearman(1, unitsToRecruit, recruitmentTime)
        val event1 = getRecruitmentOrderOnlySpearman(1, unitsToRecruit, recruitmentTime)
        val army = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(event, event1),
            recruitmentTime.plus(50, ChronoUnit.SECONDS), Coordinates(1, 1)
        )
        // when
        // then
        Assertions.assertThat(army.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits + 5)
        Assertions.assertThat(army.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits)
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.isFinished).isFalse
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.timeSpendOnRecruiting).isEqualTo(50)
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.remainingRecruitmentTime).isEqualTo(50)
        Assertions.assertThat(event1.recruitmentOrders.recruitSpearman.isFinished).isFalse
        Assertions.assertThat(event1.recruitmentOrders.recruitSpearman.timeSpendOnRecruiting).isEqualTo(0)
        Assertions.assertThat(event1.recruitmentOrders.recruitSpearman.remainingRecruitmentTime).isEqualTo(100)
    }

    @Test
    @DisplayName("Should create finished recruiting event ")
    fun recruit10() {
        // given
        val amountOfUnits = 100
        val unitsToRecruit = 10
        val recruitmentTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        val event = getRecruitmentOrderOnlySpearman(1, unitsToRecruit, recruitmentTime)
        val event1 = getRecruitmentOrderOnlySpearman(1, unitsToRecruit, recruitmentTime)
        val army = Army(
            1,
            getHumanUnitsList(amountOfUnits), listOf(event, event1),
            recruitmentTime.plus(150, ChronoUnit.SECONDS), Coordinates(1, 1)
        )
        // when
        // then
        Assertions.assertThat(army.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits + 15)
        Assertions.assertThat(army.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits)
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.isFinished).isTrue
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.timeSpendOnRecruiting).isEqualTo(100)
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.remainingRecruitmentTime).isEqualTo(0)
        Assertions.assertThat(event1.recruitmentOrders.recruitSpearman.isFinished).isFalse
        Assertions.assertThat(event1.recruitmentOrders.recruitSpearman.timeSpendOnRecruiting).isEqualTo(50)
        Assertions.assertThat(event1.recruitmentOrders.recruitSpearman.remainingRecruitmentTime).isEqualTo(50)
        Assertions.assertThat(army.getDomainEvents().size).isEqualTo(1)
        Assertions.assertThat(army.getDomainEvents()[0].warMakerEventType)
            .isEqualTo(WarMakerArmyEvent.FINISHED_UNITS_RECRUITMENT)
    }

    @Test
    @DisplayName("Should continue recruiting after recreating army ")
    fun recruit11() {
        // given
        val amountOfUnits = 100
        val unitsToRecruit = 15
        val recruitmentTime = ZonedDateTime.now(TimeConfiguration.serverClock)
        val recruitmentTime1 = ZonedDateTime.now(TimeConfiguration.serverClock).plus(25, ChronoUnit.SECONDS)
        val event = getRecruitmentOrderOnlySpearman(1, unitsToRecruit, recruitmentTime)
        val army = Army(
            1,
            getHumanUnitsList(amountOfUnits),
            listOf(event),
            recruitmentTime.plus(50, ChronoUnit.SECONDS),
            Coordinates(1, 1)
        )
        // when
        Assertions.assertThat(army.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits + 5)
        Assertions.assertThat(army.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits)
        Assertions.assertThat(army.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits)
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.isFinished).isFalse
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.timeSpendOnRecruiting).isEqualTo(50)
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.remainingRecruitmentTime).isEqualTo(100)

        // then
        val army1 = Army(
            1,
            army.armyUnits, listOf(event),
            recruitmentTime.plus(200, ChronoUnit.SECONDS), Coordinates(1, 1)
        )
        Assertions.assertThat(army1.armyUnits.availableUnits.units.spearman).isEqualTo(amountOfUnits + 15)
        Assertions.assertThat(army1.armyUnits.availableUnits.units.swordsman).isEqualTo(amountOfUnits)
        Assertions.assertThat(army1.armyUnits.availableUnits.units.archers).isEqualTo(amountOfUnits)
        Assertions.assertThat(army1.armyUnits.availableUnits.units.crossbowman).isEqualTo(amountOfUnits)
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.isFinished).isTrue
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.timeSpendOnRecruiting).isEqualTo(150)
        Assertions.assertThat(event.recruitmentOrders.recruitSpearman.remainingRecruitmentTime).isEqualTo(0)
    }

    private fun getHumanUnitsList(amount: Int): ArmyUnits {
        return ArmyUnits(
            1,
            HumanUnits(1L, amount, amount, amount, amount),
            concatOf(1, listOf(HumanUnits(1, amount, amount, amount, amount))),
            concatOf(1, listOf(HumanUnits(1, amount, amount, amount, amount)))
        )
    }

    private fun getRecruitmentOrder(
        armyId: Int,
        unitsToRecruit: Int,
        lastReadTime: ZonedDateTime
    ): StartedUnitsRecruitmentEvent {
        return StartedUnitsRecruitmentEvent(
            this, UUID.randomUUID(), 1, armyId.toLong(), RecruitmentOrders(
                RecruitUnitOrder(unitsToRecruit, 10),
                RecruitUnitOrder(unitsToRecruit, 10),
                RecruitUnitOrder(unitsToRecruit, 10),
                RecruitUnitOrder(unitsToRecruit, 10)
            ), lastReadTime
        )
    }

    private fun getRecruitmentOrderOnlyArchers(
        armyId: Int,
        unitsToRecruit: Int,
        lastReadTime: ZonedDateTime
    ): StartedUnitsRecruitmentEvent {
        return StartedUnitsRecruitmentEvent(
            this, UUID.randomUUID(), 1, armyId.toLong(), RecruitmentOrders(
                RecruitUnitOrder(0, 10),
                RecruitUnitOrder(0, 10),
                RecruitUnitOrder(unitsToRecruit, 10),
                RecruitUnitOrder(0, 10)
            ), lastReadTime
        )
    }

    private fun getRecruitmentOrderOnlySpearman(
        armyId: Int,
        unitsToRecruit: Int,
        lastReadTime: ZonedDateTime
    ): StartedUnitsRecruitmentEvent {
        return StartedUnitsRecruitmentEvent(
            this, UUID.randomUUID(), 1, armyId.toLong(), RecruitmentOrders(
                RecruitUnitOrder(unitsToRecruit, 10),
                RecruitUnitOrder(0, 10),
                RecruitUnitOrder(0, 10),
                RecruitUnitOrder(0, 10)
            ), lastReadTime
        )
    }

    private fun getRecruitmentOrderOnlySpearmanAndSwordsman(
        armyId: Int,
        unitsToRecruit1: Int,
        unitsToRecruit2: Int,
        lastReadTime: ZonedDateTime
    ): StartedUnitsRecruitmentEvent {
        return StartedUnitsRecruitmentEvent(
            this, UUID.randomUUID(), 1, armyId.toLong(), RecruitmentOrders(
                RecruitUnitOrder(unitsToRecruit1, 10),
                RecruitUnitOrder(unitsToRecruit2, 10),
                RecruitUnitOrder(0, 10),
                RecruitUnitOrder(0, 10)
            ), lastReadTime
        )
    }
}
