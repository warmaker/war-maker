package com.graphzero.warmaker.application.warparty

import com.google.common.collect.Lists
import com.graphzero.warmaker.UnitTest
import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.resources.ResourcesValue.Companion.of
import com.graphzero.warmaker.application.warparty.journey.WarPartyCommand
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneyConclusion
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneySaga
import com.graphzero.warmaker.application.world.Coordinates
import com.graphzero.warmaker.application.world.RegionsGenerator
import com.graphzero.warmaker.generators.WarPartyGenerator
import com.graphzero.warmaker.utils.EventsUtils
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.springframework.context.ApplicationEventPublisher
import java.time.ZonedDateTime

@UnitTest
internal class WarPartyTest {
    private val warPartyGenerator = WarPartyGenerator()
    private val regionsGenerator = RegionsGenerator

    @Test
    @DisplayName("Should execute simple move and do battle with lair")
    fun Move() {
        // given
        val warParty = warPartyGenerator.mediumTestWarParty.build()
        val saga = getTestSaga(warParty.id)
        // when
        val conclusion = warParty.move(RegionsGenerator.testLair, saga)
        // then
        Assertions.assertThat(conclusion).isEqualTo(WarPartyJourneyConclusion.MOVING_TO_NEXT_POSITION)
        Assertions.assertThat(warParty.location.x).isEqualTo(1)
        Assertions.assertThat(warParty.location.y).isEqualTo(1)
        Assertions.assertThat(saga.actualCoordinates.x).isEqualTo(1)
        Assertions.assertThat(saga.actualCoordinates.y).isEqualTo(1)
    }

    @Test
    @DisplayName("Should generate correct domain events for defender war party")
    fun shouldGenerateCorrectDomainEventsForDefenderWarParty() {
        // given
        val defenderWarParty = warPartyGenerator.mediumTestWarParty.build()
        val attackerWarParty = warPartyGenerator.smallTestWarParty.build()
        // when
        defenderWarParty.battle(attackerWarParty)
        // then
        Assertions.assertThat(defenderWarParty.getDomainEvents()).hasSize(2)
        val defenderWarPartyEvents = defenderWarParty.getDomainEvents()
        Assertions.assertThat(defenderWarPartyEvents.stream().map { it.warMakerEventType })
            .contains(WarMakerArmyEvent.WAR_PARTY_BATTLES)
        Assertions.assertThat(defenderWarPartyEvents.stream().map{ it.warMakerEventType })
            .contains(WarMakerArmyEvent.WAR_PARTY_RESOURCES_CHANGE)
    }

    @Test
    @DisplayName("Should generate correct domain events for attacker war party")
    fun shouldGenerateCorrectDomainEventsForAttackerWarParty() {
        // given
        val defenderWarParty = warPartyGenerator.mediumTestWarParty.build()
        val attackerWarParty = warPartyGenerator.smallTestWarParty.build()
        // when
        defenderWarParty.battle(attackerWarParty)
        // then
        Assertions.assertThat(attackerWarParty.getDomainEvents()).hasSize(3)
        Assertions.assertThat(attackerWarParty.getDomainEvents().stream().map{ it.warMakerEventType })
            .contains(WarMakerArmyEvent.WAR_PARTY_BATTLES)
        Assertions.assertThat(attackerWarParty.getDomainEvents().stream().map{ it.warMakerEventType })
            .contains(WarMakerArmyEvent.WAR_PARTY_ANNIHILATED)
        Assertions.assertThat(attackerWarParty.getDomainEvents().stream().map{ it.warMakerEventType })
            .contains(WarMakerArmyEvent.WAR_PARTY_RESOURCES_CHANGE)
    }

    @Test
    @DisplayName("Should generate correct resources change event")
    fun shouldGenerateCorrectResourcesChangeEvent() {
        // given
        val defenderWarParty = warPartyGenerator.mediumTestWarParty.build()
        val attackerWarParty = warPartyGenerator.smallTestWarParty.build()
        // when
        defenderWarParty.battle(attackerWarParty)
        // then
        val resourceChangeEventOpt = EventsUtils.getOneResourcesChangeEvent(defenderWarParty)
        Assertions.assertThat(resourceChangeEventOpt).isPresent
        val resourcesChangeEvent = resourceChangeEventOpt.get()
        val resourcesChange = resourcesChangeEvent.resourcesChange
        Assertions.assertThat(resourcesChangeEvent.warPartyId).isEqualTo(defenderWarParty.id)
        Assertions.assertThat(resourcesChange).isNotNull
    }

    @Test
    @DisplayName("Should generate correct resources change for defender event after battle when defender wins")
    fun shouldGenerateCorrectResourcesChangeForDefenderEventAfterBattleWhenDefenderWins() {
        // given
        val defenderWarParty = warPartyGenerator.mediumTestWarParty.build()
        val attackerWarParty = warPartyGenerator.smallTestWarParty.build()
        val attackerResourcesBeforeBattle = of(attackerWarParty.carriedResources)
        // when
        defenderWarParty.battle(attackerWarParty)
        // then
        val resourcesChange = EventsUtils.getOneResourcesChangeEvent(defenderWarParty).get().resourcesChange
        Assertions.assertThat(resourcesChange.denars).isEqualTo(attackerResourcesBeforeBattle.denars)
        Assertions.assertThat(resourcesChange.wood).isEqualTo(attackerResourcesBeforeBattle.wood)
        Assertions.assertThat(resourcesChange.stone).isEqualTo(attackerResourcesBeforeBattle.stone)
        Assertions.assertThat(resourcesChange.steel).isEqualTo(attackerResourcesBeforeBattle.steel)
        Assertions.assertThat(resourcesChange.food).isEqualTo(attackerResourcesBeforeBattle.food)
    }

    @Test
    @DisplayName("Should generate correct resources change event for attacker after battle when attacker wins")
    fun shouldGenerateCorrectResourcesChangeForAttackerEventAfterBattleWhenAttackerWins() {
        // given
        val defenderWarParty = warPartyGenerator.smallTestWarParty.build()
        val attackerWarParty = warPartyGenerator.mediumTestWarParty.build()
        val defenderResourcesBeforeBattle = of(defenderWarParty.carriedResources)
        // when
        defenderWarParty.battle(attackerWarParty)
        // then
        val resourcesChange = EventsUtils.getOneResourcesChangeEvent(attackerWarParty).get().resourcesChange
        Assertions.assertThat(resourcesChange.denars).isEqualTo(defenderResourcesBeforeBattle.denars)
        Assertions.assertThat(resourcesChange.wood).isEqualTo(defenderResourcesBeforeBattle.wood)
        Assertions.assertThat(resourcesChange.stone).isEqualTo(defenderResourcesBeforeBattle.stone)
        Assertions.assertThat(resourcesChange.steel).isEqualTo(defenderResourcesBeforeBattle.steel)
        Assertions.assertThat(resourcesChange.food).isEqualTo(defenderResourcesBeforeBattle.food)
    }

    private fun getTestSaga(warPartyId: Long): WarPartyJourneySaga {
        val coordinates = Lists.newArrayList(Coordinates(0, 0), Coordinates(1, 1))
        return WarPartyJourneySaga(
            this,
            mock(ApplicationEventPublisher::class.java),
            ZonedDateTime.now(),
            1,
            warPartyId,
            coordinates,
            WarPartyCommand.ATTACK,
            false
        )
    }
}
