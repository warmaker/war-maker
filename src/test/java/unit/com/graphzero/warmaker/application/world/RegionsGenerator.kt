package com.graphzero.warmaker.application.world

import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.generators.PlayerGenerator
import com.graphzero.warmaker.generators.WarPartyGenerator
import java.util.*

object RegionsGenerator {
    private val warPartyGenerator = WarPartyGenerator()
    private val playerGenerator = PlayerGenerator()

    internal val testLair: Lair
        get() = Lair(
            1L,
            Coordinates(1, 1),
            1.0,
            Resources(1L, 10.0, 10.0, 10.0, 10.0, 10.0, 0.0),
            warPartyGenerator.smallTestWarParty.owner(playerGenerator.banditPlayer).build(),
            ArrayList()
        )
}
