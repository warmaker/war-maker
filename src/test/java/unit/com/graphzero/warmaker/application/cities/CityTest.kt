package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.UnitTest
import com.graphzero.warmaker.application.cities.buildings.BuildingsDto
import com.graphzero.warmaker.application.cities.buildings.CityGenerator
import com.graphzero.warmaker.application.player.Player
import com.graphzero.warmaker.application.world.Coordinates
import com.graphzero.warmaker.domain.enumeration.FieldType
import com.graphzero.warmaker.generators.PlayerGenerator
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@UnitTest
internal class CityTest {
    private val cityGenerator = CityGenerator()

    @Test
    @DisplayName("Should create dto with proper city data")
    fun shouldCreateDtoWithProperCityData() {
        // given
        val city = cityGenerator.cityRegion
        // when
        val dto = city.fillDto(CityDto())
        // then
        Assertions.assertThat(dto.coordinates).isEqualTo(Coordinates(city.coordinates!!.x, city.coordinates!!.y))
        Assertions.assertThat(dto.fieldType).isEqualTo(FieldType.CITY)
        Assertions.assertThat(dto.moveModifier).isEqualTo(city.moveModifier)
    }

    @Test
    @DisplayName("Should create dto with proper nested data")
    fun shouldCreateDtoWithProperNestedData() {
        // given
        val city = cityGenerator.cityBuildings
        // when
        val dto = city.collectDto(city)
        // then
        Assertions.assertThat(dto.resources).isEqualTo(city.resources)
    }

    @Test
    @DisplayName("City of enemy should be enemy")
    fun cityOfEnemyShouldBeEnemy() {
        // given
        val player = playerGenerator.getPlayer()
        val enemyCity = cityGenerator.getCityRegion(playerGenerator.enemyPlayer)
        // when
        val isCityEnemy = enemyCity.isEnemyOf(player)
        // then
        Assertions.assertThat(isCityEnemy).isTrue
    }

    @ParameterizedTest
    @MethodSource("players")
    @DisplayName("City of not enemy should not be enemy")
    fun cityOfNotEnemyShouldNotBeEnemy(player: Player) {
        // given
        val enemyCity = cityGenerator.getCityRegion(player)
        // when
        val isCityEnemy = enemyCity.isEnemyOf(player)
        // then
        Assertions.assertThat(isCityEnemy).isFalse
    }

    companion object {
        private val playerGenerator = PlayerGenerator()

        @JvmStatic
        fun players(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(playerGenerator.neutralPlayer),
                Arguments.of(playerGenerator.getPlayer()),
                Arguments.of(playerGenerator.allyPlayer)
            )
        }
    }
}
