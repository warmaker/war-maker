package com.graphzero.warmaker.application.warparty.journey

import com.graphzero.warmaker.UnitTest
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@UnitTest
internal class WarPartyJourneySagaTest {
    @Test
    @DisplayName("Moving to next position should work")
    fun shouldChangePosition() {
        // given
        val testPath = WarPartyJourneySagaGenerator.testMovePath
        val saga = WarPartyJourneySagaGenerator.getJourneySaga(1L, 2L, testPath)
        // when
        val position = saga.moveToNextStop()
        // then
        Assertions.assertThat(position).isEqualTo(1)
        Assertions.assertThat(saga.currentPosition).isEqualTo(testPath[position])
        Assertions.assertThat(saga.hasReachedItsDestination()).isEqualTo(false)
    }

    @Test
    @DisplayName("should move to next position even if it was called too many times ")
    fun shouldChangePositionWhenIncorrentlyUsed() {
        // given
        val testPath = WarPartyJourneySagaGenerator.testMovePath
        val saga = WarPartyJourneySagaGenerator.getJourneySaga(1L, 2L, testPath)
        // when
        val position0 = saga.moveToNextStop()
        val position1 = saga.moveToNextStop()
        val position2 = saga.moveToNextStop()
        val badPosition1 = saga.moveToNextStop()
        val badPosition2 = saga.moveToNextStop()
        // then
        Assertions.assertThat(badPosition2).isEqualTo(2)
        Assertions.assertThat(saga.currentPosition).isEqualTo(testPath[badPosition2])
        Assertions.assertThat(saga.hasReachedItsDestination()).isEqualTo(true)
    }

    @Test
    @DisplayName("Should correctly initialize Saga")
    fun shouldInitialize() {
        // given
        val testPath = WarPartyJourneySagaGenerator.testMovePath
        val saga = WarPartyJourneySagaGenerator.getJourneySaga(1L, 2L, testPath)
        // when && then
        Assertions.assertThat(saga.currentPosition).isEqualTo(testPath[0])
        Assertions.assertThat(saga.hasReachedItsDestination()).isEqualTo(false)
    }

    @Test
    @DisplayName("Should reach destination")
    fun shouldReachDestination() {
        // given
        val testPath = WarPartyJourneySagaGenerator.testMovePath
        val saga = WarPartyJourneySagaGenerator.getJourneySaga(1L, 2L, testPath)
        // when
        saga.moveToNextStop()
        val position = saga.moveToNextStop()
        // then
        Assertions.assertThat(position).isEqualTo(2)
        Assertions.assertThat(saga.currentPosition).isEqualTo(testPath[position])
        Assertions.assertThat(saga.hasReachedItsDestination()).isEqualTo(true)
    }

    @Test
    @DisplayName("Should correctly return next stop")
    fun shouldCorrectlyReturnNextStop() {
        // given
        val testPath = WarPartyJourneySagaGenerator.testMovePath
        val saga = WarPartyJourneySagaGenerator.getJourneySaga(1L, 2L, testPath)
        // when
        val nextStop0 = saga.nextStop
        saga.moveToNextStop()
        val nextStop1 = saga.nextStop
        saga.moveToNextStop()
        val nextStop2 = saga.nextStop
        // then
        Assertions.assertThat(nextStop2).isEqualTo(testPath[2])
        Assertions.assertThat(saga.hasReachedItsDestination()).isEqualTo(true)
        Assertions.assertThat(saga.currentPosition).isEqualTo(testPath[2])
    }
}
