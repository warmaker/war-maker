package com.graphzero.warmaker.application.world

import com.google.common.collect.Lists
import com.graphzero.warmaker.UnitTest
import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.events.WarMakerEventType
import com.graphzero.warmaker.application.events.domain.DomainEvent
import com.graphzero.warmaker.application.warparty.WarParty
import com.graphzero.warmaker.application.warparty.journey.WarPartyCommand
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneyConclusion
import com.graphzero.warmaker.application.warparty.journey.WarPartyJourneySaga
import com.graphzero.warmaker.application.warparty.value.WarPartyAnnihilatedEvent
import com.graphzero.warmaker.generators.PlayerGenerator
import com.graphzero.warmaker.generators.WarPartyGenerator
import com.graphzero.warmaker.utils.EventsUtils
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.springframework.context.ApplicationEventPublisher
import java.time.ZonedDateTime
import java.util.*
import java.util.stream.Collectors

@UnitTest
internal class LandTest {
    private val warPartyGenerator = WarPartyGenerator()
    private val playerGenerator = PlayerGenerator()
    @Test
    @DisplayName("War Party should fight single enemy in field while passing through")
    fun handlePassThrough() {
        // given
        val land = Land(
            1, Coordinates(1, 1), 1.0, warPartyGenerator
                .mixedWarPartiesInField
        )
        val visitingWarParty = warPartyGenerator.mediumTestWarParty.build()
        // when
        val passThroughConclusion = land
            .enter(visitingWarParty, getPassThroughSaga(visitingWarParty.id))
        // then
        Assertions.assertThat(passThroughConclusion)
            .isEqualTo(WarPartyJourneyConclusion.MOVING_TO_NEXT_POSITION)
        val warPartyBattleEvent = EventsUtils.getOneBattleEvent(visitingWarParty).get()
        Assertions.assertThat(warPartyBattleEvent.warParty.id).isEqualTo(visitingWarParty.id)
    }

    @Test
    @DisplayName("War Party should fight all enemies in field while passing through")
    fun handlePassThrough1() {
        // given
        val warPartiesInField = warPartyGenerator.mixedWarPartiesInField
        warPartiesInField.add(
            warPartyGenerator.smallTestWarParty
                .owner(playerGenerator.enemyPlayer).build()
        )
        val land = Land(1, Coordinates(1, 1), 1.0, warPartiesInField)
        val visitingWarParty = warPartyGenerator.mediumTestWarParty.build()
        // when
        val passThroughConclusion = land
            .enter(visitingWarParty, getPassThroughSaga(visitingWarParty.id))
        // then
        Assertions.assertThat(passThroughConclusion)
            .isEqualTo(WarPartyJourneyConclusion.MOVING_TO_NEXT_POSITION)
        Assertions.assertThat(visitingWarParty.getDomainEvents().size).isEqualTo(4)
        val warPartyBattleEvents = EventsUtils.getBattlesEvents(visitingWarParty)
        Assertions.assertThat(warPartyBattleEvents[0].warParty.id).isEqualTo(visitingWarParty.id)
        Assertions.assertThat(warPartyBattleEvents[1].warParty.id).isEqualTo(visitingWarParty.id)
    }

    @Test
    @DisplayName("War Party should get annihilated by bigger force")
    fun handlePassThrough2() {
        // given
        val warPartiesInField = warPartyGenerator.mixedWarPartiesInField
        val enemyWarParty = warPartyGenerator.bigTestWarParty
            .owner(playerGenerator.enemyPlayer).build()
        warPartiesInField.add(enemyWarParty)
        val land = Land(1, Coordinates(1, 1), 1.0, warPartiesInField)
        val visitingWarParty = warPartyGenerator.mediumTestWarParty.build()
        // when
        val passThroughConclusion = land
            .enter(visitingWarParty, getPassThroughSaga(visitingWarParty.id))
        // then
        Assertions.assertThat(passThroughConclusion).isEqualTo(WarPartyJourneyConclusion.ANNIHILATED)
        Assertions.assertThat(visitingWarParty.getDomainEvents().size).isEqualTo(5)
        val warPartyBattleEvents = EventsUtils.getBattlesEvents(visitingWarParty)
        Assertions.assertThat(warPartyBattleEvents[0].warParty.id).isEqualTo(visitingWarParty.id)
        Assertions.assertThat(
            visitingWarParty.getDomainEvents().stream().map { it.warMakerEventType }
                .map { warMakerEventType: Any? -> warMakerEventType as WarMakerArmyEvent? }
                .collect(Collectors.toList())).contains(WarMakerArmyEvent.WAR_PARTY_ANNIHILATED)
        val annihilationEvent = visitingWarParty.getDomainEvents().stream()
            .filter { domainEvent: DomainEvent<out WarMakerEventType> -> domainEvent.warMakerEventType == WarMakerArmyEvent.WAR_PARTY_ANNIHILATED }
            .findFirst().get()
        Assertions.assertThat(annihilationEvent).isInstanceOf(WarPartyAnnihilatedEvent::class.java)
        Assertions.assertThat((annihilationEvent as WarPartyAnnihilatedEvent).warPartyId)
            .isEqualTo(visitingWarParty.id)
    }

    @Test
    @DisplayName("War Party shouldn't fight neutral players")
    fun handlePassThrough3() {
        // given
        val land = Land(
            1, Coordinates(1, 1), 1.0, warPartyGenerator
                .neutralPartiesInField
        )
        val visitingWarParty = warPartyGenerator.mediumTestWarParty.build()
        // when
        val passThroughConclusion = land
            .enter(visitingWarParty, getPassThroughSaga(visitingWarParty.id))
        // then
        Assertions.assertThat(passThroughConclusion)
            .isEqualTo(WarPartyJourneyConclusion.MOVING_TO_NEXT_POSITION)
        Assertions.assertThat(visitingWarParty.getDomainEvents().size).isEqualTo(0)
    }

    @Test
    @DisplayName("Should save all domain events that happened in land")
    fun handleReachingDestination() {
        // given
        val warPartiesInField = ArrayList<WarParty>()
        warPartiesInField.add(
            warPartyGenerator.smallTestWarParty
                .owner(playerGenerator.enemyPlayer).build()
        )
        val land = Land(1, Coordinates(1, 1), 1.0, warPartiesInField)
        val visitingWarParty = warPartyGenerator.bigTestWarParty.build()
        // when
        land.enter(visitingWarParty, getPassThroughSaga(visitingWarParty.id))
        // then
        Assertions.assertThat(land.getDomainEvents().size).isEqualTo(3)
        Assertions.assertThat(
            land.getDomainEvents()
                .map { it.warMakerEventType }
                .map { warMakerEventType: Any? -> warMakerEventType as WarMakerArmyEvent? })
            .contains(WarMakerArmyEvent.WAR_PARTY_ANNIHILATED)
        val event = land.getDomainEvents().stream()
            .filter { domainEvent: DomainEvent<out WarMakerEventType> -> domainEvent.warMakerEventType == WarMakerArmyEvent.WAR_PARTY_ANNIHILATED }
            .findFirst().get()
        Assertions.assertThat(event).isInstanceOf(WarPartyAnnihilatedEvent::class.java)
        Assertions.assertThat((event as WarPartyAnnihilatedEvent).warPartyId)
            .isEqualTo(warPartiesInField[0].id)
    }

    private fun getPassThroughSaga(warPartyId: Long): WarPartyJourneySaga {
        val coordinates = Lists
            .newArrayList(Coordinates(0, 1), Coordinates(1, 1), Coordinates(1, 2))
        return WarPartyJourneySaga(
            this, mock(ApplicationEventPublisher::class.java), ZonedDateTime
                .now(), 1, warPartyId, coordinates, WarPartyCommand.ATTACK, false
        )
    }
}
