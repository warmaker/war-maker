package com.graphzero.warmaker.application.warparty.journey

import com.graphzero.warmaker.application.world.Coordinates
import org.assertj.core.util.Lists
import org.mockito.Mockito.mock
import org.springframework.context.ApplicationEventPublisher
import java.time.ZonedDateTime

object WarPartyJourneySagaGenerator {

    fun getJourneySaga(
        ownerId: Long,
        warPartyEntityId: Long,
        coordinates: List<Coordinates>
    ): WarPartyJourneySaga {
        return getJourneySaga(ownerId, warPartyEntityId, coordinates, mock(ApplicationEventPublisher::class.java))
    }

    fun getJourneySaga(
        ownerId: Long,
        warPartyEntityId: Long,
        coordinates: List<Coordinates>,
        publisher: ApplicationEventPublisher
    ): WarPartyJourneySaga {
        return WarPartyJourneySaga(
            "test source", publisher, ZonedDateTime
                .now(), ownerId, warPartyEntityId, coordinates, WarPartyCommand.MOVE, false
        )
    }

    fun getAttackSaga(
        ownerId: Long,
        warPartyEntityId: Long,
        coordinates: List<Coordinates>
    ): WarPartyJourneySaga {
        return WarPartyJourneySaga(
            "test source", mock(ApplicationEventPublisher::class.java), ZonedDateTime
                .now(), ownerId, warPartyEntityId, coordinates, WarPartyCommand.ATTACK, false
        )
    }

    val testMovePath: List<Coordinates>
        get() = Lists.newArrayList(Coordinates(1, 1), Coordinates(1, 2), Coordinates(2, 2))
}
