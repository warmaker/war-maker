package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.UnitTest
import com.graphzero.warmaker.application.resources.Resources
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@UnitTest
internal class ResourcesTest {
    @Test
    @DisplayName("Should correctly erase all resources while plundering.")
    fun plunder() {
        // given
        val resources = constantResources(100)
        // when
        resources.plunder()
        // then
        Assertions.assertThat(resources.denars).isEqualTo(0.0)
        Assertions.assertThat(resources.wood).isEqualTo(0.0)
        Assertions.assertThat(resources.stone).isEqualTo(0.0)
        Assertions.assertThat(resources.steel).isEqualTo(0.0)
        Assertions.assertThat(resources.food).isEqualTo(0.0)
        Assertions.assertThat(resources.faith).isEqualTo(0.0)
    }

    @Test
    @DisplayName("Should correctly add resources while collecting.")
    fun collect() {
        // given
        val resources = constantResources(100)
        // when
        resources.collect(constantResources(100))
        // then
        Assertions.assertThat(resources.denars).isEqualTo(200.0)
        Assertions.assertThat(resources.wood).isEqualTo(200.0)
        Assertions.assertThat(resources.stone).isEqualTo(200.0)
        Assertions.assertThat(resources.steel).isEqualTo(200.0)
        Assertions.assertThat(resources.food).isEqualTo(200.0)
    }

    companion object {
        fun emptyResources(): Resources {
            return Resources(0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        }

        fun constantResources(amount: Long): Resources {
            return Resources(
                0,
                amount.toDouble(),
                amount.toDouble(),
                amount.toDouble(),
                amount.toDouble(),
                amount.toDouble(),
                amount.toDouble()
            )
        }
    }
}
