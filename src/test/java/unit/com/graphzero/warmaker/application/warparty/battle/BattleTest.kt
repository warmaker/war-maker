package com.graphzero.warmaker.application.warparty.battle

import com.graphzero.warmaker.UnitTest
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@UnitTest
internal class BattleTest {

    @Test
    @DisplayName("Attacker should win in simple scenario")
    fun test1() {
        // given
        val attacker: WarPartyBattle = WarPartyBattleTest.createWarPartyBattle(1L, 1, 100, 100, 15, 5)
        val defender: WarPartyBattle = WarPartyBattleTest.createWarPartyBattle(2L, 2, 10, 0, 150, 50)
        // when
        val battleResult = defender.battle(attacker, null)
        // then
        Assertions.assertThat(battleResult.battleResult).isEqualTo(BattleResult.ATTACKER_WINS)
        Assertions.assertThat(battleResult.attackerLoses.spearman).isBetween(1, 100)
        Assertions.assertThat(battleResult.attackerLoses.swordsman).isBetween(1, 100)
        Assertions.assertThat(battleResult.attackerLoses.archers).isBetween(0, 15)
        Assertions.assertThat(battleResult.attackerLoses.crossbowman).isBetween(0, 5)
        Assertions.assertThat(battleResult.defenderLoses.spearman).isBetween(1, 10)
        Assertions.assertThat(battleResult.defenderLoses.swordsman).isEqualTo(0)
        Assertions.assertThat(battleResult.defenderLoses.archers).isBetween(1, 150)
        Assertions.assertThat(battleResult.defenderLoses.crossbowman).isBetween(1, 50)
    }
}
