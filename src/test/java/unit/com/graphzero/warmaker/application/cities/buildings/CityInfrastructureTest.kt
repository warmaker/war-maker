package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.UnitTest
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@UnitTest
internal class CityInfrastructureTest {
    @Test
    @DisplayName("Should properly calculate resource value for level")
    fun resourceCostForLevel() {
        // given
        val level = 1
        val higherLevel = level + 1
        val sawMill = SawMill(infrastructureBaseStats(), level, 0)
        // when
        val higherLevelSawMill = SawMill(infrastructureBaseStats(), higherLevel - 1, 0)
        // then
        Assertions.assertThat(sawMill.resourceCostForLevel(higherLevel)).isEqualTo(higherLevelSawMill.nextLevelCost())
    }

    @Test
    @DisplayName("Should properly calculate resource value for higher level")
    fun resourceCostForLevel1() {
        // given
        val level = 1
        val higherLevel = level + 5
        val sawMill = SawMill(infrastructureBaseStats(), level, 0)
        // when
        val higherLevelSawMill = SawMill(infrastructureBaseStats(), higherLevel - 1, 0)
        // then
        Assertions.assertThat(sawMill.resourceCostForLevel(higherLevel)).isEqualTo(higherLevelSawMill.nextLevelCost())
    }

    @Test
    @DisplayName("Should properly add up resource cost while calculating cost for lower level")
    fun resourceCostForLevel2() {
        // given
        val level = 10
        val lowerLevel = level - 5
        val sawMill = SawMill(infrastructureBaseStats(), level, 0)
        // when
        val lowerLevelSawMill = SawMill(infrastructureBaseStats(), lowerLevel - 1, 0)
        // then
        Assertions.assertThat(sawMill.resourceCostForLevel(lowerLevel)).isEqualTo(lowerLevelSawMill.nextLevelCost())
    }

    private fun infrastructureBaseStats(): InfrastructureBaseStats {
        val basePointValue = 10
        val baseToughness = 15
        val baseBuildSpeed = 1
        return InfrastructureBaseStats(basePointValue.toLong(), baseToughness.toLong(), baseBuildSpeed.toDouble())
    }
}
