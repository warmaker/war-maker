package com.graphzero.warmaker.application.cities.recruitment

import com.graphzero.warmaker.UnitTest
import com.graphzero.warmaker.application.cities.CityInfo
import com.graphzero.warmaker.application.cities.CityScheduledEvents
import com.graphzero.warmaker.application.cities.buildings.CityGenerator
import com.graphzero.warmaker.application.resources.Resources
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import com.graphzero.warmaker.domain.enumeration.CityType
import com.graphzero.warmaker.utils.TestConfigLoader
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.io.IOException
import java.time.ZonedDateTime

@UnitTest
internal class CityRecruitmentTest {
    @Test
    @DisplayName("Should correctly calculate gold base units cost")
    @Throws(
        IOException::class
    )
    fun calculateGoldBaseUnitsCost() {
        // given
        val unitsConfig = TestConfigLoader.testConfig.getUnits()
        val cityRecruitment = cityRecruitment
        val unitsList = HumanUnitsList(10, 15, 20, 25)
        // when
        val resourceCost = cityRecruitment.calculateBaseUnitsCost(unitsList)
        // then
        Assertions.assertThat(resourceCost.denars).isEqualTo(
            (
                    unitsList.spearman * unitsConfig.getSpearman()
                        .getUnitsCost().gold + unitsList.swordsman * unitsConfig.getSwordsman()
                        .getUnitsCost().gold + unitsList.archers * unitsConfig.getArcher()
                        .getUnitsCost().gold + unitsList.crossbowman * unitsConfig.getCrossbowman().getUnitsCost().gold
                    ).toDouble()
        )
    }

    @Test
    @DisplayName("Should correctly calculate wood base units cost")
    @Throws(
        IOException::class
    )
    fun calculateWoodBaseUnitsCost() {
        // given
        val unitsConfig = TestConfigLoader.testConfig.getUnits()
        val unitsList = HumanUnitsList(10, 15, 20, 25)
        val cityRecruitment = cityRecruitment
        // when
        val resourceCost = cityRecruitment.calculateBaseUnitsCost(unitsList)
        // then
        Assertions.assertThat(resourceCost.wood).isEqualTo(
            (
                    unitsList.spearman * unitsConfig.getSpearman()
                        .getUnitsCost().wood + unitsList.swordsman * unitsConfig.getSwordsman()
                        .getUnitsCost().wood + unitsList.archers * unitsConfig.getArcher()
                        .getUnitsCost().wood + unitsList.crossbowman * unitsConfig.getCrossbowman().getUnitsCost().wood
                    ).toDouble()
        )
    }

    @Test
    @DisplayName("Should correctly calculate stone base units cost")
    @Throws(
        IOException::class
    )
    fun calculateStoneBaseUnitsCost() {
        // given
        val unitsConfig = TestConfigLoader.testConfig.getUnits()
        val cityRecruitment = cityRecruitment
        // when
        val unitsList = HumanUnitsList(10, 15, 20, 25)
        val resourceCost = cityRecruitment.calculateBaseUnitsCost(unitsList)
        // then
        Assertions.assertThat(resourceCost.stone).isEqualTo(
            (
                    unitsList.spearman * unitsConfig.getSpearman()
                        .getUnitsCost().stone + unitsList.swordsman * unitsConfig.getSwordsman()
                        .getUnitsCost().stone + unitsList.archers * unitsConfig.getArcher()
                        .getUnitsCost().stone + unitsList.crossbowman * unitsConfig.getCrossbowman()
                        .getUnitsCost().stone
                    ).toDouble()
        )
    }

    @Test
    @DisplayName("Should correctly calculate iron base units cost")
    @Throws(
        IOException::class
    )
    fun calculateIronBaseUnitsCost() {
        // given
        val unitsConfig = TestConfigLoader.testConfig.getUnits()
        val cityRecruitment = cityRecruitment
        val unitsList = HumanUnitsList(10, 15, 20, 25)
        // when
        val resourceCost = cityRecruitment.calculateBaseUnitsCost(unitsList)
        // then
        Assertions.assertThat(resourceCost.steel).isEqualTo(
            (
                    unitsList.spearman * unitsConfig.getSpearman()
                        .getUnitsCost().iron + unitsList.swordsman * unitsConfig.getSwordsman()
                        .getUnitsCost().iron + unitsList.archers * unitsConfig.getArcher()
                        .getUnitsCost().iron + unitsList.crossbowman * unitsConfig.getCrossbowman().getUnitsCost().iron
                    ).toDouble()
        )
    }

    @Test
    @DisplayName("Should correctly calculate faith base units cost")
    @Throws(
        IOException::class
    )
    fun calculateFaithBaseUnitsCost() {
        // given
        val unitsConfig = TestConfigLoader.testConfig.getUnits()
        val cityRecruitment = cityRecruitment
        val unitsList = HumanUnitsList(10, 15, 20, 25)
        // when
        val resourceCost = cityRecruitment.calculateBaseUnitsCost(unitsList)
        // then
        Assertions.assertThat(resourceCost.faith).isEqualTo(
            (
                    unitsList.spearman * unitsConfig.getSpearman()
                        .getUnitsCost().faith + unitsList.swordsman * unitsConfig.getSwordsman()
                        .getUnitsCost().faith + unitsList.archers * unitsConfig.getArcher()
                        .getUnitsCost().faith + unitsList.crossbowman * unitsConfig.getCrossbowman()
                        .getUnitsCost().faith
                    ).toDouble()
        )
    }

    @Test
    @DisplayName("Should correctly calculate food base units cost")
    @Throws(
        IOException::class
    )
    fun calculateFoodBaseUnitsCost() {
        // given
        val unitsConfig = TestConfigLoader.testConfig.getUnits()
        val cityRecruitment = cityRecruitment
        val unitsList = HumanUnitsList(10, 15, 20, 25)
        // when
        val resourceCost = cityRecruitment.calculateBaseUnitsCost(unitsList)
        // then
        Assertions.assertThat(resourceCost.food).isEqualTo(
            (
                    unitsList.spearman * unitsConfig.getSpearman()
                        .getUnitsCost().food + unitsList.swordsman * unitsConfig.getSwordsman()
                        .getUnitsCost().food + unitsList.archers * unitsConfig.getArcher()
                        .getUnitsCost().food + unitsList.crossbowman * unitsConfig.getCrossbowman().getUnitsCost().food
                    ).toDouble()
        )
    }

    companion object {
        val cityRecruitment: CityRecruitment
            get() = CityRecruitment(
                1L,
                CityInfo(2, 3, "Test City", CityType.PLAYER, 1.0, 1.0),
                CityGenerator().testBuildings.createBuildings(),
                Resources(5, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0),
                ZonedDateTime.now(),
                CityScheduledEvents(),
                TestConfigLoader.testConfig.getUnits()
            )
    }
}
