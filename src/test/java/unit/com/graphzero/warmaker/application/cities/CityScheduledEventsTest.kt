package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.UnitTest
import com.graphzero.warmaker.application.army.StartedUnitsRecruitmentEvent
import com.graphzero.warmaker.application.army.value.RecruitUnitOrder
import com.graphzero.warmaker.application.army.value.RecruitmentOrders
import com.graphzero.warmaker.application.cities.buildings.StartedBuildingUpgradeEvent
import com.graphzero.warmaker.application.events.WarMakerBuildingEvent
import com.graphzero.warmaker.application.resources.ResourcesValue.Companion.empty
import com.graphzero.warmaker.application.time.TimeConfiguration
import com.graphzero.warmaker.domain.ArmyEntity
import com.graphzero.warmaker.domain.CityEntity
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.List

@UnitTest
internal class CityScheduledEventsTest {
    private val formatter = DateTimeFormatter.ISO_ZONED_DATE_TIME
    @Test
    @DisplayName("Should add building event")
    fun shouldAddBuildingEvent() {
        // given
        val cityEvents = CityScheduledEvents()
        // when
        cityEvents.addBuildingUpgradeEvent(startedBuildingUpgradeEvent)
        // then
        Assertions.assertThat(cityEvents.startedBuildingUpgradeEventsDto).hasSize(1)
    }

    @Test
    @DisplayName("Should add multiple building events")
    fun shouldAddMultipleBuildingEvent() {
        // given
        val cityEvents = CityScheduledEvents()
        // when
        cityEvents.addBuildingUpgradeEvent(startedBuildingUpgradeEvent)
        cityEvents.addBuildingUpgradeEvent(startedBuildingUpgradeEvent)
        cityEvents.addBuildingUpgradeEvent(startedBuildingUpgradeEvent)
        // then
        Assertions.assertThat(cityEvents.startedBuildingUpgradeEventsDto).hasSize(3)
    }

    @Test
    @DisplayName("Should get map of queued buildings")
    fun shouldGetMapOfQueuedBuildings() {
        // given
        val cityEvents = CityScheduledEvents()
        cityEvents.addBuildingUpgradeEvent(getStartedBuildingUpgradeEvent(WarMakerBuildingEvent.UPGRADE_CITY_HALL))
        cityEvents.addBuildingUpgradeEvent(getStartedBuildingUpgradeEvent(WarMakerBuildingEvent.UPGRADE_BARRACKS))
        cityEvents.addBuildingUpgradeEvent(getStartedBuildingUpgradeEvent(WarMakerBuildingEvent.UPGRADE_FORGE))
        cityEvents.addBuildingUpgradeEvent(getStartedBuildingUpgradeEvent(WarMakerBuildingEvent.UPGRADE_BARRACKS))
        cityEvents.addBuildingUpgradeEvent(getStartedBuildingUpgradeEvent(WarMakerBuildingEvent.UPGRADE_BARRACKS))
        cityEvents.addBuildingUpgradeEvent(getStartedBuildingUpgradeEvent(WarMakerBuildingEvent.UPGRADE_CITY_HALL))
        // when
        val queuedBuildingsUpgrades = cityEvents.queuedBuildingsUpgrades
        // then
        Assertions.assertThat(queuedBuildingsUpgrades[WarMakerBuildingEvent.UPGRADE_FORGE]).isEqualTo(1)
        Assertions.assertThat(queuedBuildingsUpgrades[WarMakerBuildingEvent.UPGRADE_CITY_HALL]).isEqualTo(2)
        Assertions.assertThat(queuedBuildingsUpgrades[WarMakerBuildingEvent.UPGRADE_BARRACKS]).isEqualTo(3)
    }

    @Test
    @DisplayName("Should get latest building time")
    fun shouldGetLatestBuildingTime() {
        // given
        val cityEvents = CityScheduledEvents()
        val latestDate = "2020-01-03T12:05:30+02:00[Europe/Warsaw]"
        cityEvents.addBuildingUpgradeEvent(
            getStartedBuildingUpgradeEvent(
                WarMakerBuildingEvent.UPGRADE_CITY_HALL,
                "2010-01-03T10:15:30+02:00[Europe/Warsaw]"
            )
        )
        cityEvents.addBuildingUpgradeEvent(
            getStartedBuildingUpgradeEvent(
                WarMakerBuildingEvent.UPGRADE_BARRACKS,
                "2020-01-02T07:15:30+02:00[Europe/Warsaw]"
            )
        )
        cityEvents.addBuildingUpgradeEvent(
            getStartedBuildingUpgradeEvent(
                WarMakerBuildingEvent.UPGRADE_CITY_HALL,
                "2020-01-03T10:15:30+02:00[Europe/Warsaw]"
            )
        )
        cityEvents.addBuildingUpgradeEvent(
            getStartedBuildingUpgradeEvent(
                WarMakerBuildingEvent.UPGRADE_BARRACKS,
                "2020-01-03T10:15:31+02:00[Europe/Warsaw]"
            )
        )
        cityEvents.addBuildingUpgradeEvent(
            getStartedBuildingUpgradeEvent(
                WarMakerBuildingEvent.UPGRADE_FORGE,
                "2020-01-03T10:16:30+02:00[Europe/Warsaw]"
            )
        )
        cityEvents.addBuildingUpgradeEvent(
            getStartedBuildingUpgradeEvent(
                WarMakerBuildingEvent.UPGRADE_BARRACKS,
                latestDate
            )
        )
        // when
        val lastBuildingFinishTime = cityEvents.lastBuildingFinishTime
        // then
        Assertions.assertThat(lastBuildingFinishTime).isEqualTo(ZonedDateTime.parse(latestDate, formatter))
    }

    @Test
    @DisplayName("Should get latest building time when there are no events")
    fun shouldGetLatestBuildingTimeWhenThereAreNoEvents() {
        // given
        val cityEvents = CityScheduledEvents()
        // when
        val lastBuildingFinishTime = cityEvents.lastBuildingFinishTime
        // then
        Assertions.assertThat(lastBuildingFinishTime).isBetween(
            ZonedDateTime.now(TimeConfiguration.serverClock).minus(1, ChronoUnit.SECONDS),
            ZonedDateTime.now(TimeConfiguration.serverClock).plus(1, ChronoUnit.SECONDS)
        )
    }

    @Test
    @DisplayName("Should properly calculate latest recruitment finish time")
    fun shouldGetLatestRecruitmentFinishTime() {
        // given
        val testTime = ZonedDateTime.now()
        val lastEventRecruitmentDuration = 120
        val recruitmentEvent1 = getRecruitmentEvent(testTime.plus(40, ChronoUnit.SECONDS))
        val recruitmentEvent2 = getRecruitmentEvent(testTime.plus(80, ChronoUnit.SECONDS))
        val recruitmentEvent3 =
            getRecruitmentEvent(testTime.plus(lastEventRecruitmentDuration.toLong(), ChronoUnit.SECONDS))
        val cityEvents =
            CityScheduledEvents(listOf(recruitmentEvent1, recruitmentEvent2, recruitmentEvent3), ArrayList())
        // when
        val recruitmentEvent = recruitmentEvent
        val recruitmentFinishTime = cityEvents.calculateRecruitmentFinishTime(recruitmentEvent)
        // then
        Assertions.assertThat(recruitmentFinishTime)
            .isBetween(
                testTime.plus(
                    lastEventRecruitmentDuration + recruitmentEvent.recruitmentOrders.totalRecruitmentTime - TEST_TIME_DELTA,
                    ChronoUnit.SECONDS
                ),
                ZonedDateTime.now(TimeConfiguration.serverClock).plus(
                    lastEventRecruitmentDuration + recruitmentEvent.recruitmentOrders.totalRecruitmentTime + TEST_TIME_DELTA,
                    ChronoUnit.SECONDS
                )
            )
    }

    @Test
    @DisplayName("Should properly calculate latest recruitment finish time when there is no events")
    fun shouldGetLatestRecruitmentFinishTimeWithNoEvents() {
        // given
        val testTime = ZonedDateTime.now()
        val cityEvents = CityScheduledEvents(ArrayList(), ArrayList())
        // when
        val recruitmentEvent = recruitmentEvent
        val recruitmentFinishTime = cityEvents.calculateRecruitmentFinishTime(recruitmentEvent)
        // then
        Assertions.assertThat(recruitmentFinishTime)
            .isBetween(
                testTime.plus(
                    recruitmentEvent.recruitmentOrders.totalRecruitmentTime - TEST_TIME_DELTA,
                    ChronoUnit.SECONDS
                ),
                ZonedDateTime.now(TimeConfiguration.serverClock)
                    .plus(recruitmentEvent.recruitmentOrders.totalRecruitmentTime + TEST_TIME_DELTA, ChronoUnit.SECONDS)
            )
    }

    private val recruitmentEvent: StartedUnitsRecruitmentEvent
        private get() = StartedUnitsRecruitmentEvent(
            this,
            UUID.randomUUID(),
            1,
            2,
            RecruitmentOrders(
                RecruitUnitOrder(10, 1),
                RecruitUnitOrder(10, 1),
                RecruitUnitOrder(10, 1),
                RecruitUnitOrder(10, 1)
            ),
            ZonedDateTime.now()
        )
    private val startedBuildingUpgradeEvent: StartedBuildingUpgradeEvent
        private get() = StartedBuildingUpgradeEvent(
            this,
            UUID.randomUUID(),
            WarMakerBuildingEvent.UPGRADE_BARRACKS,
            1,
            2,
            ZonedDateTime.now(),
            empty()
        )

    private fun getStartedBuildingUpgradeEvent(eventType: WarMakerBuildingEvent): StartedBuildingUpgradeEvent {
        return StartedBuildingUpgradeEvent(this, UUID.randomUUID(), eventType, 1, 2, ZonedDateTime.now(), empty())
    }

    private fun getStartedBuildingUpgradeEvent(
        eventType: WarMakerBuildingEvent,
        executionTime: String
    ): StartedBuildingUpgradeEvent {
        return StartedBuildingUpgradeEvent(
            this,
            UUID.randomUUID(),
            eventType,
            1,
            2,
            ZonedDateTime.parse(executionTime, formatter),
            empty()
        )
    }

    companion object {
        private const val TEST_TIME_DELTA = 5
        fun getRecruitmentEvent(finishTime: ZonedDateTime?): StartedUnitsRecruitmentEvent {
            return StartedUnitsRecruitmentEvent(
                "",
                UUID.randomUUID(),
                1,
                2,
                RecruitmentOrders(
                    RecruitUnitOrder(10, 1),
                    RecruitUnitOrder(10, 1),
                    RecruitUnitOrder(10, 1),
                    RecruitUnitOrder(10, 1)
                ),
                ZonedDateTime.now()
            )
                .setFinishTime(finishTime!!)
        }

        fun getRecruitmentEvent(
            finishTime: ZonedDateTime?,
            entity: CityEntity?,
            armyEntity: ArmyEntity
        ): StartedUnitsRecruitmentEvent {
            return StartedUnitsRecruitmentEvent(
                "",
                UUID.randomUUID(),
                entity!!.id,
                armyEntity.id,
                RecruitmentOrders(
                    RecruitUnitOrder(10, 1),
                    RecruitUnitOrder(10, 1),
                    RecruitUnitOrder(10, 1),
                    RecruitUnitOrder(10, 1)
                ),
                ZonedDateTime.now()
            )
                .setFinishTime(finishTime!!)
        }
    }
}
