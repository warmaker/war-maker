package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.IntegrationTest
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import com.graphzero.warmaker.util.usecase.UseCaseBuilder
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

@IntegrationTest
internal class ArmyServiceIntTest {
    @Autowired
    private lateinit var armyService: ArmyService

    @Autowired
    private lateinit var  useCaseBuilder: UseCaseBuilder

    // given
    @get:DisplayName("Should get city army dto")
    @get:Test
    val cityArmy: Unit
        // when
        // then
        get() {
            // given
            val city = useCaseBuilder.city().build()
            val army = city.army
            // when
            val cityArmy = armyService.getCityArmy(city.id)
            // then
            Assertions.assertThat(cityArmy.id).isEqualTo(army.id)
        }

    @Test
    @DisplayName("Should create war party")
    fun createWarParty() {
        // given
        val city = useCaseBuilder.city().build()
        val army = city.army
        // when
        val unitsToRecruit = HumanUnitsList(10, 20, 30, 40)
        val createdWarParty = armyService.createWarParty(army.id, unitsToRecruit).get()
        // then
        Assertions.assertThat(createdWarParty.id).isNotNull
        Assertions.assertThat(createdWarParty.armyEntity).isEqualTo(army)
        Assertions.assertThat(createdWarParty.units.spearmen).isEqualTo(10)
        Assertions.assertThat(createdWarParty.units.swordsmen).isEqualTo(20)
        Assertions.assertThat(createdWarParty.units.archers).isEqualTo(30)
        Assertions.assertThat(createdWarParty.units.crossbowmen).isEqualTo(40)
        Assertions.assertThat(army.units.spearmen).isEqualTo(90)
        Assertions.assertThat(army.units.swordsmen).isEqualTo(80)
        Assertions.assertThat(army.units.archers).isEqualTo(70)
        Assertions.assertThat(army.units.crossbowmen).isEqualTo(60)
    }

    @Test
    @DisplayName("Should remove war party if army exists")
    fun shouldRemoveWarPartyIfArmyExists() {
        // given
        val city = useCaseBuilder.city().build()
        // when
        // then
        Assertions.assertThat(city.id).isNotNull
    }
}
