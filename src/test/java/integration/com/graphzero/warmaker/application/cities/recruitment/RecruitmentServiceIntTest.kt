package com.graphzero.warmaker.application.cities.recruitment

import com.graphzero.warmaker.IntegrationTest
import com.graphzero.warmaker.application.warparty.value.HumanUnitsList
import com.graphzero.warmaker.util.usecase.UseCaseBuilder
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

@IntegrationTest
internal class RecruitmentServiceIntTest {

    @Autowired
    private lateinit var useCaseBuilder: UseCaseBuilder

    @Autowired
    private lateinit var recruitmentService: CityRecruitmentService
    @Test
    @DisplayName("Should recruit units in simple scenario")
    fun recruitUnits() {
        // given
        val cityEntity = useCaseBuilder.city().withResources(5000.0).build()
        val currentDenars = cityEntity.storedResources.denars.toInt()
        // when
        val unitsToRecruit = HumanUnitsList(10, 0, 0, 0)
        val either = recruitmentService.startUnitsRecruitment(cityEntity.id, unitsToRecruit)
        // then
        Assertions.assertThat(either.isRight).isTrue
        Assertions.assertThat(cityEntity.storedResources.denars).isLessThan(currentDenars.toDouble())
    }
}
