package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.IntegrationTest
import com.graphzero.warmaker.application.army.ArmyEventsRepository
import com.graphzero.warmaker.application.army.StartedUnitsRecruitmentEvent
import com.graphzero.warmaker.application.world.Coordinates
import com.graphzero.warmaker.domain.enumeration.FieldType
import com.graphzero.warmaker.util.usecase.UseCaseBuilder
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.ZonedDateTime

@IntegrationTest
internal class CityServiceIntTest {
    @Autowired
    private lateinit var cityService: CityService

    @Autowired
    private lateinit var  useCaseBuilder: UseCaseBuilder

    @Autowired
    private lateinit var armyEventsRepository: ArmyEventsRepository

    @Test
    @DisplayName("Should get CityDto")
    fun shouldGetCityDto() {
        // given
        val cityEntity = useCaseBuilder.city().build()
        // when
        val dto = cityService.getCityDto(cityEntity.id).get()
        // then
        Assertions.assertThat(dto.id).isEqualTo(cityEntity.id)
        Assertions.assertThat(dto.coordinates).isEqualTo(Coordinates(0, 0))
        Assertions.assertThat(dto.fieldType).isEqualTo(FieldType.CITY)
        Assertions.assertThat(dto.moveModifier).isEqualTo(1.0)
        val expectedCityInfo =
            CityInfo(cityEntity.owner.id, cityEntity.army.id, cityEntity.cityName, cityEntity.cityType, 1.0, 1.0)
        Assertions.assertThat(dto.info).isEqualTo(expectedCityInfo)
        Assertions.assertThat(dto.buildings).isNotNull // TODO improve this assertion
        Assertions.assertThat(dto.recruitmentOrders).hasSize(0)
    }

    @Test
    @DisplayName("Dto should return recruitment result")
    fun dtoShouldReturnRecruitmentResult() {
        // given
        val cityEntity = useCaseBuilder.city().build()
        val recruitmentEvent: StartedUnitsRecruitmentEvent = CityScheduledEventsTest.getRecruitmentEvent(
            ZonedDateTime.now(), cityEntity, cityEntity.army
        )
        armyEventsRepository.addEvent(recruitmentEvent)
        // when
        val dto = cityService.getCityDto(cityEntity.id).get()
        // then
        Assertions.assertThat(dto.recruitmentOrders).hasSize(1)
        Assertions.assertThat(dto.recruitmentOrders[0]).isEqualTo(recruitmentEvent.dto())
    }
}
