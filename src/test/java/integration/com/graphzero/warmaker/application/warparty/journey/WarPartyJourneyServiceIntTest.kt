package com.graphzero.warmaker.application.warparty.journey

import com.graphzero.warmaker.IntegrationTest
import com.graphzero.warmaker.application.events.WarMakerArmyEvent
import com.graphzero.warmaker.application.warparty.value.WarPartyNotFound
import com.graphzero.warmaker.application.world.Coordinates
import com.graphzero.warmaker.repository.WarPartyEntityRepository
import com.graphzero.warmaker.util.usecase.UseCaseBuilder
import com.graphzero.warmaker.utils.EventsUtils
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.util.Lists
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher

@IntegrationTest
internal class WarPartyJourneyServiceIntTest {
    @Autowired
    private lateinit var warPartyJourneyService: WarPartyJourneyService

    @Autowired
    private lateinit var  useCaseBuilder: UseCaseBuilder

    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    @Autowired
    private lateinit var warPartyEntityRepository: WarPartyEntityRepository

    @Test
    @DisplayName("Should start War Party journey")
    fun startWarPartyJourney() {
        // given
        val worldMap = useCaseBuilder.worldMap().build()
        val city = worldMap.playerCity
        val owner = city.owner
        val testedWarParty = city.army.warParties.iterator().next()

        // when
        val saga = warPartyJourneyService
            .startWarPartyJourney(
                WarPartyJourneyReq(
                    testedWarParty.id,
                    Lists.newArrayList(Coordinates(1, 1), Coordinates(1, 2)),
                    WarPartyCommand.ATTACK
                ), false
            )

        // then
        assertThat(saga.isSuccess).isTrue
        assertThat(saga.get().actualCoordinates).isEqualTo(Coordinates(1, 1))
        assertThat(saga.get().hasReachedItsDestination()).isEqualTo(false)
        assertThat(saga.get().command).isEqualTo(WarPartyCommand.ATTACK)
        assertThat(saga.get().warMakerEventType).isEqualTo(WarMakerArmyEvent.WAR_PARTY_JOURNEY)
        assertThat(saga.get().ownerId).isEqualTo(owner.id)
    }

    @Test
    @DisplayName("Should move War Party in simple scenario")
    @Throws(
        WarPartyNotFound::class
    )
    fun moveWarParty() {
        // given
        val worldMap = useCaseBuilder.worldMap().build()
        val city = worldMap.playerCity
        val owner = city.owner
        val testedWarParty = city.army.warParties.iterator().next()
        val coordinates = Lists.newArrayList(Coordinates(0, 0), Coordinates(1, 1))

        // when
        val movementResult = warPartyJourneyService
            .moveWarParty(
                WarPartyJourneySagaGenerator.getJourneySaga(
                    owner.id,
                    testedWarParty.id,
                    coordinates,
                    publisher
                )
            )

        // then
        assertThat(movementResult.warPartyJourneyConclusion)
            .isEqualTo(WarPartyJourneyConclusion.MOVING_TO_NEXT_POSITION)
    }

    @Test
    @DisplayName("War Party should battle with enemy War Party stationed in Land")
    @Throws(
        WarPartyNotFound::class
    )
    fun warPartyShouldBattleWithEnemyWarPartyStationedInLand() {
        // given
        val worldMapBuilder = useCaseBuilder.worldMap()
        worldMapBuilder.playerCity
            .army
            .withArmyUnits(1000)
            .warPartyCase
            .withWarPartyUnits(500)
        worldMapBuilder
            .enemyCity
            .army
            .warPartyCase
            .withWarPartyAt(1, 1)
        val worldMap = worldMapBuilder.build()
        val city = worldMap.playerCity
        val owner = city.owner
        val testedWarParty = city.army.warParties.iterator().next()
        val coordinates = Lists.newArrayList(
            Coordinates(1, 1),
            Coordinates(2, 1)
        )
        // when
        val conclusion = warPartyJourneyService
            .moveWarParty(
                WarPartyJourneySagaGenerator.getJourneySaga(
                    owner.id,
                    testedWarParty.id,
                    coordinates,
                    publisher
                )
            )
        // then
        assertThat(conclusion.warPartyJourneyConclusion)
            .isEqualTo(WarPartyJourneyConclusion.MOVING_TO_NEXT_POSITION)
        assertThat(conclusion.eventsHappened!!.size).isEqualTo(2)
        val battleEvent = EventsUtils.getOneBattleEvent(conclusion.eventsHappened)
        val resourcesChangeEvent = EventsUtils.getWarPartyResourcesChangeEvent(
            conclusion.eventsHappened
        )
        assertThat(battleEvent).isPresent
        assertThat(resourcesChangeEvent).hasSize(1)
    }

    @Test
    @DisplayName("Battle casualties should not be zero")
    @Throws(
        WarPartyNotFound::class
    )
    fun warPartyShouldHaveLessUnitsAfterBattleWithEnemyWarParty() {
        // given
        val worldMapBuilder = useCaseBuilder.worldMap()
        worldMapBuilder.playerCity
            .army
            .withArmyUnits(200)
            .warPartyCase
            .withWarPartyUnits(60)
        worldMapBuilder
            .enemyCity
            .army
            .warPartyCase
            .withWarPartyAt(1, 1)
        val worldMap = worldMapBuilder.build()
        val city = worldMap.playerCity
        val owner = city.owner
        val testedWarParty = city.army.warParties.iterator().next()
        val coordinates = Lists.newArrayList(
            Coordinates(1, 1),
            Coordinates(2, 1)
        )
        // when
        val conclusion = warPartyJourneyService
            .moveWarParty(
                WarPartyJourneySagaGenerator.getJourneySaga(
                    owner.id,
                    testedWarParty.id,
                    coordinates,
                    publisher
                )
            )
        // then
        assertThat(conclusion.warPartyJourneyConclusion)
            .isEqualTo(WarPartyJourneyConclusion.MOVING_TO_NEXT_POSITION)
        assertThat(conclusion.eventsHappened!!.size).isEqualTo(2)
        val battleEvent = EventsUtils.getOneBattleEvent(conclusion.eventsHappened).get()
        val resourcesChangeEvent = EventsUtils.getWarPartyResourcesChangeEvent(
            conclusion.eventsHappened
        )
        assertThat(battleEvent).isNotNull
        assertThat(resourcesChangeEvent).hasSize(1)
        assertThat(battleEvent.unitsLoses.spearman).isNotZero
        assertThat(battleEvent.unitsLoses.swordsman).isNotZero
        assertThat(battleEvent.unitsLoses.archers).isNotZero
        assertThat(battleEvent.unitsLoses.crossbowman).isNotZero
    }
}
