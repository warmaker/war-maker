package com.graphzero.warmaker.application.army

import com.graphzero.warmaker.IntegrationTest
import com.graphzero.warmaker.application.resources.ResourcesValue
import com.graphzero.warmaker.application.resources.ResourcesValue.Companion.of
import com.graphzero.warmaker.application.warparty.battle.BattleReport
import com.graphzero.warmaker.application.warparty.battle.BattleResult
import com.graphzero.warmaker.application.warparty.battle.WarPartyBattle
import com.graphzero.warmaker.application.warparty.battle.WarPartyBattleEvent
import com.graphzero.warmaker.application.warparty.value.HumanUnits
import com.graphzero.warmaker.application.warparty.value.WarPartyResourcesChangeEvent
import com.graphzero.warmaker.config.ApplicationProperties
import com.graphzero.warmaker.generators.AggregateRootLogicGenerator
import com.graphzero.warmaker.generators.HumanUnitsListGenerator
import com.graphzero.warmaker.generators.PlayerGenerator
import com.graphzero.warmaker.repository.WarPartyEntityRepository
import com.graphzero.warmaker.util.usecase.UseCaseBuilder
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.ZonedDateTime

@IntegrationTest
internal class ArmyEventsListenerIntTest(@Autowired val useCaseBuilder: UseCaseBuilder,
                                              @Autowired val armyEventsListener: ArmyEventsListener,
                                              @Autowired val applicationProperties: ApplicationProperties,
                                              @Autowired val warPartyEntityRepository: WarPartyEntityRepository) {

    private val playerGenerator = PlayerGenerator()

    @Test
    @DisplayName("War Party battle event should lower units count")
    fun warPartyBattleEventShouldLowerUnitsCount() {
        // given
        val initialAttackerUnitsCount = 100
        val attackingWarParty = useCaseBuilder.warParty()
            .withWarPartyUnits(initialAttackerUnitsCount)
            .build()
        val defendingWarParty = useCaseBuilder.warParty()
            .withWarPartyUnits(30)
            .build()
        val attackerForces = HumanUnits.of(attackingWarParty.units)
        val defenderForces = HumanUnits.of(defendingWarParty.units)
        val attacker = WarPartyBattle(
            AggregateRootLogicGenerator.from(attackingWarParty.id),
            attackerForces,
            playerGenerator.getPlayer(),
            applicationProperties.getUnits()
        )
        val defender = WarPartyBattle(
            AggregateRootLogicGenerator.from(defendingWarParty.id),
            defenderForces,
            playerGenerator.enemyPlayer,
            applicationProperties.getUnits()
        )
        val attackerLosesCount = 40
        val attackerLoses = HumanUnitsListGenerator.units(attackerLosesCount).units
        val battleReport = BattleReport.of(
            ZonedDateTime.now(),
            BattleResult.ATTACKER_WINS,
            attackerLoses,
            HumanUnitsListGenerator.units(30).units,
            attacker,
            defender
        )
        val remainingUnits = HumanUnitsListGenerator.units(initialAttackerUnitsCount - attackerLosesCount).units
        // when
        val battleEvent = WarPartyBattleEvent(this, attacker, attackerLoses, remainingUnits, battleReport)
        armyEventsListener.handleWarPartyBattle(battleEvent)
        // then
        val attackingWarPartyUnitsAfterBattle = warPartyEntityRepository.findById(
            attackingWarParty.id
        ).get().units
        Assertions.assertThat(attackingWarPartyUnitsAfterBattle.spearmen).isEqualTo(remainingUnits.spearman)
        Assertions.assertThat(attackingWarPartyUnitsAfterBattle.swordsmen).isEqualTo(remainingUnits.swordsman)
        Assertions.assertThat(attackingWarPartyUnitsAfterBattle.archers).isEqualTo(remainingUnits.archers)
        Assertions.assertThat(attackingWarPartyUnitsAfterBattle.crossbowmen).isEqualTo(remainingUnits.crossbowman)
    }

    @Test
    @DisplayName("War Party won battle event should add resources")
    fun warPartyWonBattleEventShouldAddResources() {
        // given
        val attackingWarParty = useCaseBuilder.warParty()
            .withWarPartyUnits(200)
            .build() // when
        val resourcesCarriedBeforeBattle = of(attackingWarParty.resourcesCarried)
        val warPartyResourcesChange =
            WarPartyResourcesChangeEvent(
                this,
                attackingWarParty.id,
                ResourcesValue(1000.0, 100.0, 100.0, 100.0, 100.0, 100.0)
            )
        armyEventsListener.handleWarPartyResourcesChange(warPartyResourcesChange)
        // then
        val carriedResourcesAfterBattle = warPartyEntityRepository.findById(
            attackingWarParty.id
        ).get().resourcesCarried
        Assertions.assertThat(carriedResourcesAfterBattle.denars).isGreaterThan(resourcesCarriedBeforeBattle.denars)
        Assertions.assertThat(carriedResourcesAfterBattle.wood).isGreaterThan(resourcesCarriedBeforeBattle.wood)
        Assertions.assertThat(carriedResourcesAfterBattle.steel).isGreaterThan(resourcesCarriedBeforeBattle.steel)
        Assertions.assertThat(carriedResourcesAfterBattle.stone).isGreaterThan(resourcesCarriedBeforeBattle.stone)
        Assertions.assertThat(carriedResourcesAfterBattle.food).isGreaterThan(resourcesCarriedBeforeBattle.food)
    }
}
