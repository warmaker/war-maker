package com.graphzero.warmaker.application

import com.graphzero.warmaker.IntegrationTest
import com.graphzero.warmaker.WarMakerApp
import com.graphzero.warmaker.util.usecase.UseCaseBuilder
import com.graphzero.warmaker.web.rest.errors.ExceptionTranslator
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders

@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [WarMakerApp::class])
@IntegrationTest
abstract class AbstractWarMakerControllerIntTest<T> {
    protected lateinit var restMvc: MockMvc

    @Autowired
    protected lateinit var jacksonMessageConverter: MappingJackson2HttpMessageConverter

    @Autowired
    protected lateinit var pageableArgumentResolver: PageableHandlerMethodArgumentResolver

    @Autowired
    protected lateinit var exceptionTranslator: ExceptionTranslator

    @Autowired
    protected lateinit var useCaseBuilder: UseCaseBuilder

    @BeforeEach
    protected fun setUp() {
        MockitoAnnotations.initMocks(this)
        restMvc = MockMvcBuilders.standaloneSetup(testedController)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build()
    }

    protected fun addQuotes(s: String): String {
        return "\"" + s + "\""
    }

    internal abstract val testedController: T
}
