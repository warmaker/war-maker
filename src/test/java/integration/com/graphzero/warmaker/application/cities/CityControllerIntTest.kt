package com.graphzero.warmaker.application.cities

import com.graphzero.warmaker.application.AbstractWarMakerControllerIntTest
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

internal class CityControllerIntTest: AbstractWarMakerControllerIntTest<CityController>() {

    @Autowired
    override lateinit var testedController: CityController

    @Test
    @DisplayName("Should return basic city info")
    @Throws(
        Exception::class
    )
    fun shouldReturnBasicCityInfo() {
        // given
        val worldMap = useCaseBuilder.worldMap().build()
        val city = worldMap.playerCity
        // when && then
        restMvc.perform(
            get("/api/city/${city.id}").contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.resources.incomeRate").isNotEmpty)
    }

}
