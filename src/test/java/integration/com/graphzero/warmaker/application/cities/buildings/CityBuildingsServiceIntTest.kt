package com.graphzero.warmaker.application.cities.buildings

import com.graphzero.warmaker.IntegrationTest
import com.graphzero.warmaker.application.resources.ResourcesValue.Companion.of
import com.graphzero.warmaker.repository.ResourcesEntityRepository
import com.graphzero.warmaker.util.usecase.UseCaseBuilder
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

@IntegrationTest
internal class CityBuildingsServiceIntTest {
    @Autowired
    private lateinit var cityBuildingsService: CityBuildingsService

    @Autowired
    private lateinit var  useCaseBuilder: UseCaseBuilder

    @Autowired
    private lateinit var resourcesEntityRepository: ResourcesEntityRepository

    @BeforeEach
    fun setUp() {
    }

    @Test
    @DisplayName("Should upgrade gold mine when there are enough resources")
    fun scheduleGoldMineUpgrade() {
        // given
        val cityEntity = useCaseBuilder.city().withResources(10000.0).build()
        // when
        val upgradeRes = cityBuildingsService.scheduleGoldMineUpgrade(
            cityEntity.id
        )
        // then
        Assertions.assertThat(upgradeRes.isRight).isTrue
        Assertions.assertThat(upgradeRes.right()).isNotNull
    }

    @Test
    @DisplayName("Shouldn't let upgrade when there are no enough resources in queued orders")
    fun scheduleSawMillUpgrade() {
        // given
        val cityEntity = useCaseBuilder.city().withResources(5000.0).build()
        // when
        val upgradeRes = cityBuildingsService.scheduleSawMillUpgrade(
            cityEntity.id
        )
        val upgradeRes1 = cityBuildingsService.scheduleSawMillUpgrade(
            cityEntity.id
        )
        // then
        Assertions.assertThat(upgradeRes.isRight).isTrue
        Assertions.assertThat(upgradeRes.right()).isNotNull
        Assertions.assertThat(upgradeRes1.isLeft).isTrue
    }

    @Test
    @DisplayName("Should let upgrade when there are resources in queued orders")
    fun scheduleSawMillUpgrade1() {
        // given
        val cityEntity = useCaseBuilder.city().withResources(20000.0).build()
        // when
        val upgradeRes = cityBuildingsService.scheduleSawMillUpgrade(
            cityEntity.id
        )
        val upgradeRes1 = cityBuildingsService.scheduleSawMillUpgrade(
            cityEntity.id
        )
        // then
        Assertions.assertThat(upgradeRes.isRight).isTrue
        Assertions.assertThat(upgradeRes.right()).isNotNull
        Assertions.assertThat(upgradeRes1.isRight).isTrue
    }

    @Test
    @DisplayName("Should correctly persist resources after update")
    fun scheduleSawMillUpgrade2() {
        // given
        val initialResources = 7000
        val cityEntity = useCaseBuilder.city().withResources(initialResources.toDouble()).build()
        val resourcesEntity = cityEntity.storedResources
        // when
        val upgradeRes = cityBuildingsService.scheduleSawMillUpgrade(
            cityEntity.id
        )
        // then
        val entity = resourcesEntityRepository.findById(resourcesEntity.id).get()
        Assertions.assertThat(upgradeRes.isRight).isTrue
        Assertions.assertThat(upgradeRes.right()).isNotNull
        Assertions.assertThat(entity.denars).isEqualTo(initialResources - upgradeRes.right().get().resourcesUsed.denars)
        Assertions.assertThat(entity.wood).isEqualTo(initialResources - upgradeRes.right().get().resourcesUsed.wood)
        Assertions.assertThat(entity.stone).isEqualTo(initialResources - upgradeRes.right().get().resourcesUsed.stone)
        Assertions.assertThat(entity.steel).isEqualTo(initialResources - upgradeRes.right().get().resourcesUsed.steel)
    }

    @Test
    @DisplayName("Should raise resources after 100 minutes pass")
    fun test() {
        // given
        val cityEntity = useCaseBuilder.city().build()
        val storedResources = cityEntity.storedResources
        val value = of(storedResources)
        storedResources.changeDate = ZonedDateTime.now().minus(100, ChronoUnit.MINUTES)
        resourcesEntityRepository.save(storedResources)
        // when
        val city = cityBuildingsService.getCityBuildings(cityEntity.id).get()
        // then
        Assertions.assertThat(city.resources.storedResources.denars).isGreaterThan(value.denars)
        Assertions.assertThat(city.resources.storedResources.wood).isGreaterThan(value.wood)
        Assertions.assertThat(city.resources.storedResources.stone).isGreaterThan(value.stone)
        Assertions.assertThat(city.resources.storedResources.steel).isGreaterThan(value.steel)
    }

    @Test
    @DisplayName("Should update resources in db after 100 minutes pass")
    fun test1() {
        // given
        val city = useCaseBuilder.city().build()
        val storedResources = city.storedResources
        val value = of(storedResources)
        storedResources.changeDate = ZonedDateTime.now().minus(100, ChronoUnit.MINUTES)
        resourcesEntityRepository.save(storedResources)
        // when
        cityBuildingsService.getCityBuildings(city.id)
        // then
        Assertions.assertThat(city.storedResources.denars).isGreaterThan(value.denars)
        Assertions.assertThat(city.storedResources.wood).isGreaterThan(value.wood)
        Assertions.assertThat(city.storedResources.stone).isGreaterThan(value.stone)
        Assertions.assertThat(city.storedResources.steel).isGreaterThan(value.steel)
    }
}
