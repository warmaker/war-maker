package com.graphzero.warmaker.application.events.schedulable

import com.graphzero.warmaker.IntegrationTest
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher

@IntegrationTest
internal class ScheduledEventsServiceIntTest {
    private val log = LoggerFactory.getLogger(ScheduledEventsServiceIntTest::class.java)

    @Autowired
    private lateinit var scheduledEventsService: ScheduledEventsService

    @Autowired
    private lateinit var publisher: ApplicationEventPublisher

    @Test
    @Disabled("test to debug events behaviour")
    @DisplayName("Should work")
    @Throws(
        InterruptedException::class
    )
    fun test1() {
//        log.info("Start");
//        List<UpgradeBuildingScheduledEvent> list = LongStream
//            .iterate(0, i -> i + 1)
//            .limit(100)
//            .mapToObj(i -> new UpgradeBuildingScheduledEvent(this, publisher, ZonedDateTime.now(ZoneId.systemDefault()).plus(i, ChronoUnit.MILLIS), i, null, WarMakerBuildingEvent.UPGRADE_SAWMILL, UUID.randomUUID(), null))
//            .peek(scheduledEventsService::schedule)
//            .collect(Collectors.toList());
//
//        IntStream
//            .iterate(50, i -> i + 1)
//            .limit(50)
//            .forEach( i -> {
//                boolean cancel = scheduledEventsService.cancel(i, list.get(i).getUUID());
//                log.debug("numb of events: {}, {}", scheduledEventsService.executingTasks(), cancel );
//            } );
    }
}
