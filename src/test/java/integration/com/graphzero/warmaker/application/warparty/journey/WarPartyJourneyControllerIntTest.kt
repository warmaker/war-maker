package com.graphzero.warmaker.application.warparty.journey

import com.google.gson.Gson
import com.graphzero.warmaker.IntegrationTest
import com.graphzero.warmaker.application.AbstractWarMakerControllerIntTest
import com.graphzero.warmaker.application.utils.WarMakerDomainErrors
import com.graphzero.warmaker.application.world.Coordinates
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.util.*

@IntegrationTest
internal class WarPartyJourneyControllerIntTest : AbstractWarMakerControllerIntTest<WarPartyJourneyController>() {

    @Autowired
    override lateinit var testedController: WarPartyJourneyController

    @Test
    @DisplayName("Should move War Party in simple scenario")
    @Throws(
        Exception::class
    )
    fun test1() {
        // given
        val worldMap = useCaseBuilder.worldMap().build()
        val city = worldMap.playerCity
        val testedWarParty = city.army.warParties.iterator().next()
        val req = WarPartyJourneyReq(testedWarParty.id, testPath, WarPartyCommand.ATTACK)
        // when && then
        restMvc.perform(
            MockMvcRequestBuilders.post("$URL/move")
                .content(Gson().toJson(req))
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @DisplayName("Shouldn't move War Party when it's already moving and should return error")
    @Throws(
        WarPartyMovingException::class
    )
    fun test2() {
        // given
        val worldMapBuilder = useCaseBuilder.worldMap()
        worldMapBuilder.playerCity
            .army
            .warPartyCase
            .withWarPartyMoving()
        val worldMap = worldMapBuilder.build()
        val city = worldMap.playerCity
        val owner = city.owner
        val testedWarParty = city.army.warParties.iterator().next()
        // when
        val req = WarPartyJourneyReq(testedWarParty.id, testPath, WarPartyCommand.ATTACK)
        // then
        restMvc.perform(
            MockMvcRequestBuilders.post("$URL/move")
                .content(Gson().toJson(req))
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(MockMvcResultMatchers.status().isBadRequest)
            .andExpect(
                MockMvcResultMatchers.content()
                    .string(addQuotes(WarMakerDomainErrors.WAR_PARTY_ALREADY_MOVING.errorMessage))
            )
    }

    private val testPath: List<Coordinates>
        get() = listOf(Coordinates(0, 0), Coordinates(1, 1), Coordinates(2, 2))

    companion object {
        private const val URL = "/api/war-party"
    }
}
