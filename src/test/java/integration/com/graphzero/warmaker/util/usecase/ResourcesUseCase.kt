package com.graphzero.warmaker.util.usecase

import com.graphzero.warmaker.domain.ResourcesEntity
import com.graphzero.warmaker.repository.ResourcesEntityRepository
import java.time.ZonedDateTime

class ResourcesUseCase(
    private val resourcesEntityRepository: ResourcesEntityRepository,
    private val useCaseBuilder: UseCaseBuilder
) : UseCase<ResourcesEntity> {
    override fun with(): UseCaseBuilder {
        return useCaseBuilder
    }

    override fun build(): ResourcesEntity {
        val resources = ResourcesEntity()
            .denars(5000.0)
            .wood(2000.0)
            .stone(1500.0)
            .steel(1000.0)
            .food(750.0)
            .faith(50.0)
            .changeDate(ZonedDateTime.now())
        resourcesEntityRepository.save(resources)
        return resources
    }

    fun withResources(amount: Double): ResourcesEntity {
        val resources = ResourcesEntity()
            .denars(amount)
            .wood(amount)
            .stone(amount)
            .steel(amount)
            .food(amount)
            .faith(amount)
            .changeDate(ZonedDateTime.now())
        resourcesEntityRepository.save(resources)
        return resources
    }
}
