package com.graphzero.warmaker.util.usecase

interface UseCase<T> {
    fun with(): UseCaseBuilder
    fun build(): T
}
