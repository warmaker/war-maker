package com.graphzero.warmaker.util.usecase

import com.graphzero.warmaker.domain.CityEntity
import com.graphzero.warmaker.domain.FieldEntity
import com.graphzero.warmaker.repository.CityEntityRepository

class WorldMap(
    private val cityEntityRepository: CityEntityRepository,
    val worldMap: List<MutableList<FieldEntity?>>
) {
    operator fun get(x: Int, y: Int): FieldEntity? {
        return worldMap[x][y]
    }

    val playerCity: CityEntity
        get() = getCityEntity(0)
    val enemyCity: CityEntity
        get() = getCityEntity(2)

    private fun getCityEntity(i: Int): CityEntity {
        return cityEntityRepository.findAll()
            .stream()
            .filter { it.location.getxCoordinate() == i && it.location.getyCoordinate() == i }
            .findFirst()
            .orElseThrow { IllegalStateException("Couldn't find City $i Is your code ok?") }
    }
}
