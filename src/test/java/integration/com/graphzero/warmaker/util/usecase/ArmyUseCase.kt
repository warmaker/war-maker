package com.graphzero.warmaker.util.usecase

import com.graphzero.warmaker.domain.ArmyEntity
import com.graphzero.warmaker.domain.CityEntity
import com.graphzero.warmaker.domain.WarPartyEntity
import com.graphzero.warmaker.repository.ArmyEntityRepository

class ArmyUseCase(
    private val armyEntityRepository: ArmyEntityRepository,
    private val useCaseBuilder: UseCaseBuilder
) : UseCase<ArmyEntity> {
    private var cityEntity: CityEntity? = null

    val warPartyCase: WarPartyCase = useCaseBuilder.warParty()
    private var armyUnits = 100

    override fun with(): UseCaseBuilder {
        return useCaseBuilder
    }

    override fun build(): ArmyEntity {
        val armyEntity = ArmyEntity()
            .foodReq(100)
            .city(cityEntity)
        val armyUnits = useCaseBuilder.warParty().createUnits(armyUnits)
        val warParty = warPartyCase
            .withArmy(armyEntity)
            .build()
        armyEntity
            .units(armyUnits)
            .warParties(UseCaseBuilder.set(warParty))
        armyEntityRepository.save(armyEntity)
        return armyEntity
    }

    fun withArmyUnits(number: Int): ArmyUseCase {
        armyUnits = number
        return this
    }

    fun withCity(cityEntity: CityEntity): ArmyUseCase {
        this.cityEntity = cityEntity
        return this
    }

}
