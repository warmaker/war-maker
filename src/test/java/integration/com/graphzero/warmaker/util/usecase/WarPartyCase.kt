package com.graphzero.warmaker.util.usecase

import com.graphzero.warmaker.domain.ArmyEntity
import com.graphzero.warmaker.domain.FieldEntity
import com.graphzero.warmaker.domain.UnitsEntity
import com.graphzero.warmaker.domain.WarPartyEntity
import com.graphzero.warmaker.domain.enumeration.WarPartyState
import com.graphzero.warmaker.repository.UnitsEntityRepository
import com.graphzero.warmaker.repository.WarPartyEntityRepository

class WarPartyCase(
    private val warPartyEntityRepository: WarPartyEntityRepository,
    private val unitsEntityRepository: UnitsEntityRepository,
    private val useCaseBuilder: UseCaseBuilder
) : UseCase<WarPartyEntity> {
    private var warPartyPosition: FieldEntity?
    private var armyEntity: ArmyEntity? = null
    private var warPartyUnits = 50
    var warPartyXCoordinate = 0
        private set
    var warPartyYCoordinate = 0
        private set
    private var warPartyState = WarPartyState.STATIONED

    override fun with(): UseCaseBuilder {
        return useCaseBuilder
    }

    override fun build(): WarPartyEntity {
        val resources = useCaseBuilder.resources().build()
        val warPartyUnits = createUnits(warPartyUnits)
        val warParty = WarPartyEntity()
            .speed(1.0)
            .capacity(1000L)
            .state(warPartyState)
            .position(warPartyPosition)
            .units(warPartyUnits)
            .armyEntity(armyEntity)
            .resourcesCarried(resources)
        warPartyEntityRepository.save(warParty)
        return warParty
    }

    fun createUnits(i: Int): UnitsEntity {
        val warPartyUnits = UnitsEntity()
            .spearmen(i)
            .swordsmen(i)
            .archers(i)
            .crossbowmen(i)
        unitsEntityRepository.save(warPartyUnits)
        return warPartyUnits
    }

    fun withArmy(armyEntity: ArmyEntity): WarPartyCase {
        this.armyEntity = armyEntity
        return this
    }

    fun withPartyOnField(fieldEntity: FieldEntity): WarPartyCase {
        warPartyPosition = fieldEntity
        return this
    }

    fun withWarPartyUnits(number: Int): WarPartyCase {
        warPartyUnits = number
        return this
    }

    fun withWarPartyAt(enemyWarPartyXCoordinate: Int, enemyWarPartyYCoordinate: Int): WarPartyCase {
        warPartyXCoordinate = enemyWarPartyXCoordinate
        warPartyYCoordinate = enemyWarPartyYCoordinate
        return this
    }

    fun withWarPartyMoving(): WarPartyCase {
        warPartyState = WarPartyState.MOVING
        return this
    }

    init {
        warPartyPosition = useCaseBuilder.field().build()
        useCaseBuilder.player().bandit()
    }
}
