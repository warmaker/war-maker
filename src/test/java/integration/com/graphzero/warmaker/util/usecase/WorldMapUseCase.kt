package com.graphzero.warmaker.util.usecase

import com.graphzero.warmaker.domain.FieldEntity
import com.graphzero.warmaker.domain.LandEntity
import com.graphzero.warmaker.domain.WorldEntity
import com.graphzero.warmaker.domain.enumeration.*
import com.graphzero.warmaker.repository.CityEntityRepository
import com.graphzero.warmaker.repository.LandEntityRepository
import com.graphzero.warmaker.repository.WorldEntityRepository
import java.util.*

/**
 * creates default 3 x 3 World Map
 */
class WorldMapUseCase(
    private val worldEntityRepository: WorldEntityRepository,
    private val landEntityRepository: LandEntityRepository,
    cityEntityRepository: CityEntityRepository,
    val useCaseBuilder: UseCaseBuilder
) : UseCase<WorldMap> {
    private val worldMap: WorldMap
    private val worldEntity: WorldEntity
    var enemyCity = useCaseBuilder.city()
    var playerCity = useCaseBuilder.city()

    private fun createDefaultCities(useCaseBuilder: UseCaseBuilder) {
        val cityField = useCaseBuilder.field().createCityField()
        val enemyCityField = useCaseBuilder.field().createFieldEntity(2, 2, FieldType.CITY)
        worldMap.worldMap[0].add(cityField)
        worldMap.worldMap[2].add(enemyCityField)
        playerCity = playerCity.withField(cityField)
        enemyCity = enemyCity.withField(enemyCityField)
        playerCity.army
            .warPartyCase
            .withWarPartyAt(0, 0)
        enemyCity.army
            .warPartyCase
            .withWarPartyAt(2, 2)
    }

    private fun createDefaultWorldMap(cityEntityRepository: CityEntityRepository): WorldMap {
        val list = ArrayList<MutableList<FieldEntity?>>()
        for (i in 0..2) {
            list.add(ArrayList())
        }
        return WorldMap(cityEntityRepository, list)
    }

    private fun createWorldEntity(): WorldEntity {
        val worldEntity: WorldEntity = WorldEntity()
            .worldName("world-1")
            .worldNumber(1)
        worldEntityRepository.save(worldEntity)
        return worldEntity
    }

    override fun with(): UseCaseBuilder {
        return useCaseBuilder
    }

    override fun build(): WorldMap {
        val city1 = playerCity
            .build()
        createLands()
        val field =
            worldMap[enemyCity.army.warPartyCase.warPartyXCoordinate, enemyCity.army.warPartyCase.warPartyYCoordinate]
        enemyCity.army
            .warPartyCase
            .withPartyOnField(field!!)
        enemyCity
            .enemyOf(city1.owner)
            .build()
        return worldMap
    }

    private fun createLands() {
        val land_0_1 = useCaseBuilder.field().createFieldEntity(0, 1)
        worldMap.worldMap[0].add(land_0_1)
        createLand(land_0_1)
        val land_0_2 = useCaseBuilder.field().createFieldEntity(0, 2)
        worldMap.worldMap[0].add(land_0_2)
        createLand(land_0_2)
        val land_1_0 = useCaseBuilder.field().createFieldEntity(1, 0)
        worldMap.worldMap[1].add(land_1_0)
        createLand(land_1_0)
        val land_1_1 = useCaseBuilder.field().createFieldEntity(1, 1)
        worldMap.worldMap[1].add(land_1_1)
        createLand(land_1_1)
        val land_1_2 = useCaseBuilder.field().createFieldEntity(1, 2)
        worldMap.worldMap[1].add(land_1_2)
        createLand(land_1_2)
        val land_2_0 = useCaseBuilder.field().createFieldEntity(2, 0)
        worldMap.worldMap[2].add(land_2_0)
        createLand(land_2_0)
        val land_2_1 = useCaseBuilder.field().createFieldEntity(2, 1)
        worldMap.worldMap[2].add(land_2_1)
        createLand(land_2_1)
    }

    private fun createLand(field: FieldEntity): LandEntity {
        val land = LandEntity()
            .location(field)
        landEntityRepository.save(land)
        return land
    }

    init {
        worldMap = createDefaultWorldMap(cityEntityRepository)
        worldEntity = createWorldEntity()
        createDefaultCities(useCaseBuilder)
    }
}
