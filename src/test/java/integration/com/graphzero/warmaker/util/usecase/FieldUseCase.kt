package com.graphzero.warmaker.util.usecase

import com.graphzero.warmaker.domain.FieldEntity
import com.graphzero.warmaker.domain.WorldEntity
import com.graphzero.warmaker.domain.enumeration.FieldType
import com.graphzero.warmaker.repository.FieldEntityRepository
import com.graphzero.warmaker.repository.WorldEntityRepository

class FieldUseCase(
    private val fieldEntityRepository: FieldEntityRepository,
    private val worldEntityRepository: WorldEntityRepository,
    val useCaseBuilder: UseCaseBuilder
) : UseCase<FieldEntity> {
    private val worldEntity: WorldEntity

    override fun with(): UseCaseBuilder {
        return useCaseBuilder
    }

    override fun build(): FieldEntity {
        return createCityField()
    }

    fun createFieldEntity(x: Int = 0, y: Int = 0, fieldType: FieldType = FieldType.LAND): FieldEntity {
        var field = fieldEntityRepository.findByXCoordinateAndYCoordinate(x, y)
        if (field == null) {
            field = FieldEntity()
                .xCoordinate(x)
                .yCoordinate(y)
                .worldEntity(worldEntity)
                .type(fieldType)
            fieldEntityRepository.save(field)
        }
        return field
    }

    fun createCityField(): FieldEntity {
        return createFieldEntity(0, 0, FieldType.CITY)
    }

    private fun createWorldEntity(): WorldEntity {
        val worldEntity: WorldEntity = WorldEntity()
            .worldName("world-1")
            .worldNumber(1)
        worldEntityRepository.save(worldEntity)
        return worldEntity
    }

    init {
        worldEntity = createWorldEntity()
    }
}
