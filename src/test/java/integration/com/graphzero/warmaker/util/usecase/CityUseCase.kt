package com.graphzero.warmaker.util.usecase

import com.graphzero.warmaker.domain.*
import com.graphzero.warmaker.domain.enumeration.CityType
import com.graphzero.warmaker.domain.enumeration.FieldType
import com.graphzero.warmaker.repository.CityBuildingsEntityRepository
import com.graphzero.warmaker.repository.CityEntityRepository

class CityUseCase internal constructor(
    private val cityRepository: CityEntityRepository,
    private val cityBuildingsEntityRepository: CityBuildingsEntityRepository,
    private val useCaseBuilder: UseCaseBuilder
) : UseCase<CityEntity> {
    private var resources: ResourcesEntity
    private var profile: ProfileEntity
    private var cityField: FieldEntity
    val army: ArmyUseCase

    override fun with(): UseCaseBuilder {
        return useCaseBuilder
    }

    override fun build(): CityEntity {
        val city = createBaseCity()
        val cityBuildings = createCityBuildings()
        val army = army
            .withCity(city)
            .build()
        city
            .army(army)
            .location(cityField)
            .storedResources(resources)
            .buildings(cityBuildings)
            .owner(profile)
        cityRepository.save(city)
        cityBuildings.city(city)
        cityBuildingsEntityRepository.save(cityBuildings)
        return city
    }

    fun withField(cityField: FieldEntity): CityUseCase {
        this.cityField = cityField
        return this
    }

    fun withResources(amount: Double): CityUseCase {
        resources = useCaseBuilder.resources().withResources(amount)
        return this
    }

    fun enemyOf(player: ProfileEntity): CityUseCase {
        profile = useCaseBuilder.player().enemy(player)
        return this
    }

    private fun createBaseCity(): CityEntity {
        return CityEntity()
            .cityName("test-city")
            .cityType(CityType.PLAYER)
            .buildSpeedModifier(1.0)
            .recruitmentModifier(1.0)
            .location(cityField)
    }

    private fun createCityBuildings(): CityBuildingsEntity {
        val cityBuildings = CityBuildingsEntity()
            .cityHall(10)
            .barracks(5)
            .wall(5)
            .goldMine(15)
            .sawMill(10)
            .oreMine(7)
            .ironworks(5)
            .tradingPost(7)
            .chapel(2)
            .university(10)
            .forge(8)
        cityBuildingsEntityRepository.save(cityBuildings)
        return cityBuildings
    }

    init {
        resources = useCaseBuilder.resources().build()
        profile = useCaseBuilder.player().build()
        cityField = useCaseBuilder.field().createCityField()
        army = useCaseBuilder.army()
    }
}
