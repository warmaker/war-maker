package com.graphzero.warmaker.util.usecase

import com.graphzero.warmaker.repository.*
import org.springframework.stereotype.Component
import java.util.*

@Component
class UseCaseBuilder(
    private val cityEntityRepository: CityEntityRepository,
    private val warPartyEntityRepository: WarPartyEntityRepository,
    private val armyEntityRepository: ArmyEntityRepository,
    private val fieldEntityRepository: FieldEntityRepository,
    private val profileEntityRepository: ProfileEntityRepository,
    private val cityBuildingsEntityRepository: CityBuildingsEntityRepository,
    private val researchesEntityRepository: ResearchesEntityRepository,
    private val resourcesEntityRepository: ResourcesEntityRepository,
    private val unitsEntityRepository: UnitsEntityRepository,
    private val worldEntityRepository: WorldEntityRepository,
    private val landEntityRepository: LandEntityRepository,
    private val playerRelationsRepository: PlayerRelationsRepository,
    private val userRepository: UserRepository
) {
    fun city(): CityUseCase {
        return CityUseCase(
            cityEntityRepository,
            cityBuildingsEntityRepository,
            this
        )
    }

    fun player(): PlayerUseCase {
        return PlayerUseCase(
            profileEntityRepository,
            researchesEntityRepository,
            playerRelationsRepository,
            userRepository,
            this
        )
    }

    fun army(): ArmyUseCase {
        return ArmyUseCase(armyEntityRepository, this)
    }

    fun warParty(): WarPartyCase {
        return WarPartyCase(warPartyEntityRepository, unitsEntityRepository, this)
    }

    fun resources(): ResourcesUseCase {
        return ResourcesUseCase(resourcesEntityRepository, this)
    }

    fun worldMap(): WorldMapUseCase {
        return WorldMapUseCase(worldEntityRepository, landEntityRepository, cityEntityRepository, this)
    }

    fun field(): FieldUseCase {
        return FieldUseCase(fieldEntityRepository, worldEntityRepository, this)
    }

    companion object {
        fun <T> set(vararg elements: T): Set<T> {
            val set = HashSet<T>()
            set.addAll(listOf(*elements))
            return set
        }
    }
}
