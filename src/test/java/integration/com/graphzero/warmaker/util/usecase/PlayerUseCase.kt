package com.graphzero.warmaker.util.usecase

import com.graphzero.warmaker.application.player.Player
import com.graphzero.warmaker.domain.PlayerRelations
import com.graphzero.warmaker.domain.ProfileEntity
import com.graphzero.warmaker.domain.ResearchesEntity
import com.graphzero.warmaker.domain.User
import com.graphzero.warmaker.domain.enumeration.RelationType
import com.graphzero.warmaker.repository.PlayerRelationsRepository
import com.graphzero.warmaker.repository.ProfileEntityRepository
import com.graphzero.warmaker.repository.ResearchesEntityRepository
import com.graphzero.warmaker.repository.UserRepository
import com.graphzero.warmaker.util.usecase.UseCaseBuilder
import java.util.*

class PlayerUseCase(
    private val profileEntityRepository: ProfileEntityRepository,
    private val researchesEntityRepository: ResearchesEntityRepository,
    private val playerRelationsRepository: PlayerRelationsRepository,
    private val userRepository: UserRepository,
    private val useCaseBuilder: UseCaseBuilder
) : UseCase<ProfileEntity> {
    override fun with(): UseCaseBuilder {
        return useCaseBuilder
    }

    override fun build(): ProfileEntity {
        val user = createUser()
        val researchesEntity = createResearcher()
        val profileEntity = ProfileEntity()
            .accountName("profile")
            .email("profile1235124123421@gmail.com")
            .researches(researchesEntity)
            .researchTimeModifier(1.0)
            .user(user)
        profileEntityRepository.save(profileEntity)
        return profileEntity
    }

    fun enemy(enemyPlayer: ProfileEntity): ProfileEntity {
        val user = createUser()
        val researchesEntity = createResearcher()
        val createdProfileRelation = createTargetRelation(enemyPlayer)
        val profileEntity = createProfileEntity(user, researchesEntity, createdProfileRelation)
        val enemyRelation = createRelation(enemyPlayer, profileEntity)
        enemyPlayer.relations(UseCaseBuilder.set(enemyRelation))
        createdProfileRelation.owner = profileEntity
        playerRelationsRepository.save(createdProfileRelation)
        profileEntityRepository.save(enemyPlayer)
        return profileEntity
    }

    /**
     * Creates bandit user to which all owner-less War Parties will be assigned
     * @return
     */
    fun bandit(): ProfileEntity {
        val user = createUser()
        return profileEntityRepository
            .findByAccountName(Player.BANDIT_NAME)
            .orElseGet {
                val researchesEntity = createResearcher()
                val profileEntity = ProfileEntity()
                    .accountName(Player.BANDIT_NAME)
                    .email("bandit@gmail.com")
                    .researches(researchesEntity)
                    .user(user)
                    .researchTimeModifier(1.0)
                profileEntityRepository.save(profileEntity)
                profileEntity
            }
    }

    private fun createProfileEntity(
        user: User,
        researchesEntity: ResearchesEntity,
        createdProfileRelation: PlayerRelations
    ): ProfileEntity {
        val profileEntity = ProfileEntity()
            .accountName("profile")
            .email("profile1235124123421@gmail.com")
            .researches(researchesEntity)
            .researchTimeModifier(1.0)
            .relations(UseCaseBuilder.set(createdProfileRelation))
            .user(user)
        profileEntityRepository.save(profileEntity)
        return profileEntity
    }

    private fun createTargetRelation(target: ProfileEntity): PlayerRelations {
        val relation = PlayerRelations()
            .relation(RelationType.ENEMY)
            .targetPlayer(target)
        playerRelationsRepository.save(relation)
        return relation
    }

    private fun createRelation(owner: ProfileEntity, target: ProfileEntity): PlayerRelations {
        val relation = PlayerRelations()
            .relation(RelationType.ENEMY)
            .targetPlayer(target)
            .owner(owner)
        playerRelationsRepository.save(relation)
        return relation
    }

    private fun createResearcher(): ResearchesEntity {
        val researchesEntity = ResearchesEntity()
            .infantryVitality(10)
            .cavalryVitality(10)
            .lightArmorValue(10)
            .heavyArmorValue(10)
            .meleeAttack(10)
            .rangedAttack(10)
            .infantrySpeed(1)
            .cavalrySpeed(1)
            .cityBuildingSpeed(10)
            .recruitmentSpeed(10)
            .scouting(10)
        researchesEntityRepository.save(researchesEntity)
        return researchesEntity
    }

    private fun createUser(): User {
        val user = User()
        user.email = UUID.randomUUID().toString() + "@b"
        user.firstName = "Test User 1" + UUID.randomUUID()
        user.login = "login1" + UUID.randomUUID()
        user.password = "111111"
        userRepository.save(user)
        return user
    }
}
