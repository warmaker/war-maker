import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { AllianceEntityUpdateComponent } from 'app/entities/alliance-entity/alliance-entity-update.component';
import { AllianceEntityService } from 'app/entities/alliance-entity/alliance-entity.service';
import { AllianceEntity } from 'app/shared/model/alliance-entity.model';

describe('Component Tests', () => {
  describe('AllianceEntity Management Update Component', () => {
    let comp: AllianceEntityUpdateComponent;
    let fixture: ComponentFixture<AllianceEntityUpdateComponent>;
    let service: AllianceEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [AllianceEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(AllianceEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AllianceEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AllianceEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AllianceEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AllianceEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
