import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { AllianceEntityComponent } from 'app/entities/alliance-entity/alliance-entity.component';
import { AllianceEntityService } from 'app/entities/alliance-entity/alliance-entity.service';
import { AllianceEntity } from 'app/shared/model/alliance-entity.model';

describe('Component Tests', () => {
  describe('AllianceEntity Management Component', () => {
    let comp: AllianceEntityComponent;
    let fixture: ComponentFixture<AllianceEntityComponent>;
    let service: AllianceEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [AllianceEntityComponent],
      })
        .overrideTemplate(AllianceEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AllianceEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AllianceEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AllianceEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.allianceEntities && comp.allianceEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
