import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { AllianceEntityDetailComponent } from 'app/entities/alliance-entity/alliance-entity-detail.component';
import { AllianceEntity } from 'app/shared/model/alliance-entity.model';

describe('Component Tests', () => {
  describe('AllianceEntity Management Detail Component', () => {
    let comp: AllianceEntityDetailComponent;
    let fixture: ComponentFixture<AllianceEntityDetailComponent>;
    const route = ({ data: of({ allianceEntity: new AllianceEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [AllianceEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(AllianceEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AllianceEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load allianceEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.allianceEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
