import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { FortressEntityDetailComponent } from 'app/entities/fortress-entity/fortress-entity-detail.component';
import { FortressEntity } from 'app/shared/model/fortress-entity.model';

describe('Component Tests', () => {
  describe('FortressEntity Management Detail Component', () => {
    let comp: FortressEntityDetailComponent;
    let fixture: ComponentFixture<FortressEntityDetailComponent>;
    const route = ({ data: of({ fortressEntity: new FortressEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [FortressEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FortressEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FortressEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fortressEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fortressEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
