import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { FortressEntityComponent } from 'app/entities/fortress-entity/fortress-entity.component';
import { FortressEntityService } from 'app/entities/fortress-entity/fortress-entity.service';
import { FortressEntity } from 'app/shared/model/fortress-entity.model';

describe('Component Tests', () => {
  describe('FortressEntity Management Component', () => {
    let comp: FortressEntityComponent;
    let fixture: ComponentFixture<FortressEntityComponent>;
    let service: FortressEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [FortressEntityComponent],
      })
        .overrideTemplate(FortressEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FortressEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FortressEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FortressEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fortressEntities && comp.fortressEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
