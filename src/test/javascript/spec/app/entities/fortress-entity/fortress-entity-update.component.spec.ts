import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { FortressEntityUpdateComponent } from 'app/entities/fortress-entity/fortress-entity-update.component';
import { FortressEntityService } from 'app/entities/fortress-entity/fortress-entity.service';
import { FortressEntity } from 'app/shared/model/fortress-entity.model';

describe('Component Tests', () => {
  describe('FortressEntity Management Update Component', () => {
    let comp: FortressEntityUpdateComponent;
    let fixture: ComponentFixture<FortressEntityUpdateComponent>;
    let service: FortressEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [FortressEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FortressEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FortressEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FortressEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FortressEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FortressEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
