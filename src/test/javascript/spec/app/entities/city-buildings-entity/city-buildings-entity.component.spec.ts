import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { CityBuildingsEntityComponent } from 'app/entities/city-buildings-entity/city-buildings-entity.component';
import { CityBuildingsEntityService } from 'app/entities/city-buildings-entity/city-buildings-entity.service';
import { CityBuildingsEntity } from 'app/shared/model/city-buildings-entity.model';

describe('Component Tests', () => {
  describe('CityBuildingsEntity Management Component', () => {
    let comp: CityBuildingsEntityComponent;
    let fixture: ComponentFixture<CityBuildingsEntityComponent>;
    let service: CityBuildingsEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [CityBuildingsEntityComponent],
      })
        .overrideTemplate(CityBuildingsEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CityBuildingsEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CityBuildingsEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CityBuildingsEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.cityBuildingsEntities && comp.cityBuildingsEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
