import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { WarMakerTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { CityBuildingsEntityDeleteDialogComponent } from 'app/entities/city-buildings-entity/city-buildings-entity-delete-dialog.component';
import { CityBuildingsEntityService } from 'app/entities/city-buildings-entity/city-buildings-entity.service';

describe('Component Tests', () => {
  describe('CityBuildingsEntity Management Delete Component', () => {
    let comp: CityBuildingsEntityDeleteDialogComponent;
    let fixture: ComponentFixture<CityBuildingsEntityDeleteDialogComponent>;
    let service: CityBuildingsEntityService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [CityBuildingsEntityDeleteDialogComponent],
      })
        .overrideTemplate(CityBuildingsEntityDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CityBuildingsEntityDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CityBuildingsEntityService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
