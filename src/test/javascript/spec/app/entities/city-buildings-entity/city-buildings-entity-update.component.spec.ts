import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { CityBuildingsEntityUpdateComponent } from 'app/entities/city-buildings-entity/city-buildings-entity-update.component';
import { CityBuildingsEntityService } from 'app/entities/city-buildings-entity/city-buildings-entity.service';
import { CityBuildingsEntity } from 'app/shared/model/city-buildings-entity.model';

describe('Component Tests', () => {
  describe('CityBuildingsEntity Management Update Component', () => {
    let comp: CityBuildingsEntityUpdateComponent;
    let fixture: ComponentFixture<CityBuildingsEntityUpdateComponent>;
    let service: CityBuildingsEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [CityBuildingsEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CityBuildingsEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CityBuildingsEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CityBuildingsEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CityBuildingsEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CityBuildingsEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
