import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { CityBuildingsEntityDetailComponent } from 'app/entities/city-buildings-entity/city-buildings-entity-detail.component';
import { CityBuildingsEntity } from 'app/shared/model/city-buildings-entity.model';

describe('Component Tests', () => {
  describe('CityBuildingsEntity Management Detail Component', () => {
    let comp: CityBuildingsEntityDetailComponent;
    let fixture: ComponentFixture<CityBuildingsEntityDetailComponent>;
    const route = ({ data: of({ cityBuildingsEntity: new CityBuildingsEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [CityBuildingsEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CityBuildingsEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CityBuildingsEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load cityBuildingsEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.cityBuildingsEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
