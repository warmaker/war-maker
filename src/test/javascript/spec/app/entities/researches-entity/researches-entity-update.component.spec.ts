import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { ResearchesEntityUpdateComponent } from 'app/entities/researches-entity/researches-entity-update.component';
import { ResearchesEntityService } from 'app/entities/researches-entity/researches-entity.service';
import { ResearchesEntity } from 'app/shared/model/researches-entity.model';

describe('Component Tests', () => {
  describe('ResearchesEntity Management Update Component', () => {
    let comp: ResearchesEntityUpdateComponent;
    let fixture: ComponentFixture<ResearchesEntityUpdateComponent>;
    let service: ResearchesEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ResearchesEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ResearchesEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ResearchesEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ResearchesEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ResearchesEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ResearchesEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
