import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { ResearchesEntityDetailComponent } from 'app/entities/researches-entity/researches-entity-detail.component';
import { ResearchesEntity } from 'app/shared/model/researches-entity.model';

describe('Component Tests', () => {
  describe('ResearchesEntity Management Detail Component', () => {
    let comp: ResearchesEntityDetailComponent;
    let fixture: ComponentFixture<ResearchesEntityDetailComponent>;
    const route = ({ data: of({ researchesEntity: new ResearchesEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ResearchesEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ResearchesEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ResearchesEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load researchesEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.researchesEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
