import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ResearchesEntityService } from 'app/entities/researches-entity/researches-entity.service';
import { IResearchesEntity, ResearchesEntity } from 'app/shared/model/researches-entity.model';

describe('Service Tests', () => {
  describe('ResearchesEntity Service', () => {
    let injector: TestBed;
    let service: ResearchesEntityService;
    let httpMock: HttpTestingController;
    let elemDefault: IResearchesEntity;
    let expectedResult: IResearchesEntity | IResearchesEntity[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(ResearchesEntityService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new ResearchesEntity(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ResearchesEntity', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new ResearchesEntity()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ResearchesEntity', () => {
        const returnedFromService = Object.assign(
          {
            infantryVitality: 1,
            cavalryVitality: 1,
            lightArmorValue: 1,
            heavyArmorValue: 1,
            meleeAttack: 1,
            rangedAttack: 1,
            infantrySpeed: 1,
            cavalrySpeed: 1,
            cityBuildingSpeed: 1,
            recruitmentSpeed: 1,
            scouting: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ResearchesEntity', () => {
        const returnedFromService = Object.assign(
          {
            infantryVitality: 1,
            cavalryVitality: 1,
            lightArmorValue: 1,
            heavyArmorValue: 1,
            meleeAttack: 1,
            rangedAttack: 1,
            infantrySpeed: 1,
            cavalrySpeed: 1,
            cityBuildingSpeed: 1,
            recruitmentSpeed: 1,
            scouting: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ResearchesEntity', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
