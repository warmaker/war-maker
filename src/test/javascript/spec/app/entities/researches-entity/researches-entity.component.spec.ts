import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { ResearchesEntityComponent } from 'app/entities/researches-entity/researches-entity.component';
import { ResearchesEntityService } from 'app/entities/researches-entity/researches-entity.service';
import { ResearchesEntity } from 'app/shared/model/researches-entity.model';

describe('Component Tests', () => {
  describe('ResearchesEntity Management Component', () => {
    let comp: ResearchesEntityComponent;
    let fixture: ComponentFixture<ResearchesEntityComponent>;
    let service: ResearchesEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ResearchesEntityComponent],
      })
        .overrideTemplate(ResearchesEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ResearchesEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ResearchesEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ResearchesEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.researchesEntities && comp.researchesEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
