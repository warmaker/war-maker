import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { HeroEntityUpdateComponent } from 'app/entities/hero-entity/hero-entity-update.component';
import { HeroEntityService } from 'app/entities/hero-entity/hero-entity.service';
import { HeroEntity } from 'app/shared/model/hero-entity.model';

describe('Component Tests', () => {
  describe('HeroEntity Management Update Component', () => {
    let comp: HeroEntityUpdateComponent;
    let fixture: ComponentFixture<HeroEntityUpdateComponent>;
    let service: HeroEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [HeroEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(HeroEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HeroEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HeroEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new HeroEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new HeroEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
