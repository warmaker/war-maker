import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { HeroEntityComponent } from 'app/entities/hero-entity/hero-entity.component';
import { HeroEntityService } from 'app/entities/hero-entity/hero-entity.service';
import { HeroEntity } from 'app/shared/model/hero-entity.model';

describe('Component Tests', () => {
  describe('HeroEntity Management Component', () => {
    let comp: HeroEntityComponent;
    let fixture: ComponentFixture<HeroEntityComponent>;
    let service: HeroEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [HeroEntityComponent],
      })
        .overrideTemplate(HeroEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HeroEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HeroEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new HeroEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.heroEntities && comp.heroEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
