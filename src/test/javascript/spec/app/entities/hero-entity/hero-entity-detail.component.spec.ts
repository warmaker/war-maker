import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { HeroEntityDetailComponent } from 'app/entities/hero-entity/hero-entity-detail.component';
import { HeroEntity } from 'app/shared/model/hero-entity.model';

describe('Component Tests', () => {
  describe('HeroEntity Management Detail Component', () => {
    let comp: HeroEntityDetailComponent;
    let fixture: ComponentFixture<HeroEntityDetailComponent>;
    const route = ({ data: of({ heroEntity: new HeroEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [HeroEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(HeroEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HeroEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load heroEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.heroEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
