import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { ProfileEntityComponent } from 'app/entities/profile-entity/profile-entity.component';
import { ProfileEntityService } from 'app/entities/profile-entity/profile-entity.service';
import { ProfileEntity } from 'app/shared/model/profile-entity.model';

describe('Component Tests', () => {
  describe('ProfileEntity Management Component', () => {
    let comp: ProfileEntityComponent;
    let fixture: ComponentFixture<ProfileEntityComponent>;
    let service: ProfileEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ProfileEntityComponent],
      })
        .overrideTemplate(ProfileEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProfileEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProfileEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ProfileEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.profileEntities && comp.profileEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
