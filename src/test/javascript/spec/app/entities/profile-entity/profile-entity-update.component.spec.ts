import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { ProfileEntityUpdateComponent } from 'app/entities/profile-entity/profile-entity-update.component';
import { ProfileEntityService } from 'app/entities/profile-entity/profile-entity.service';
import { ProfileEntity } from 'app/shared/model/profile-entity.model';

describe('Component Tests', () => {
  describe('ProfileEntity Management Update Component', () => {
    let comp: ProfileEntityUpdateComponent;
    let fixture: ComponentFixture<ProfileEntityUpdateComponent>;
    let service: ProfileEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ProfileEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ProfileEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProfileEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProfileEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProfileEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProfileEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
