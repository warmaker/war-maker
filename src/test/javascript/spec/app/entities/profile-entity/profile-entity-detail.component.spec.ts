import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { ProfileEntityDetailComponent } from 'app/entities/profile-entity/profile-entity-detail.component';
import { ProfileEntity } from 'app/shared/model/profile-entity.model';

describe('Component Tests', () => {
  describe('ProfileEntity Management Detail Component', () => {
    let comp: ProfileEntityDetailComponent;
    let fixture: ComponentFixture<ProfileEntityDetailComponent>;
    const route = ({ data: of({ profileEntity: new ProfileEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ProfileEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ProfileEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ProfileEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load profileEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.profileEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
