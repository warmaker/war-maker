import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { CityEntityUpdateComponent } from 'app/entities/city-entity/city-entity-update.component';
import { CityEntityService } from 'app/entities/city-entity/city-entity.service';
import { CityEntity } from 'app/shared/model/city-entity.model';

describe('Component Tests', () => {
  describe('CityEntity Management Update Component', () => {
    let comp: CityEntityUpdateComponent;
    let fixture: ComponentFixture<CityEntityUpdateComponent>;
    let service: CityEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [CityEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CityEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CityEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CityEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CityEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CityEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
