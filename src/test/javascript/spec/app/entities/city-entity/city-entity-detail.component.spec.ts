import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { CityEntityDetailComponent } from 'app/entities/city-entity/city-entity-detail.component';
import { CityEntity } from 'app/shared/model/city-entity.model';

describe('Component Tests', () => {
  describe('CityEntity Management Detail Component', () => {
    let comp: CityEntityDetailComponent;
    let fixture: ComponentFixture<CityEntityDetailComponent>;
    const route = ({ data: of({ cityEntity: new CityEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [CityEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CityEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CityEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load cityEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.cityEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
