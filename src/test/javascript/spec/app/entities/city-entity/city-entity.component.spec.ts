import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { CityEntityComponent } from 'app/entities/city-entity/city-entity.component';
import { CityEntityService } from 'app/entities/city-entity/city-entity.service';
import { CityEntity } from 'app/shared/model/city-entity.model';

describe('Component Tests', () => {
  describe('CityEntity Management Component', () => {
    let comp: CityEntityComponent;
    let fixture: ComponentFixture<CityEntityComponent>;
    let service: CityEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [CityEntityComponent],
      })
        .overrideTemplate(CityEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CityEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CityEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CityEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.cityEntities && comp.cityEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
