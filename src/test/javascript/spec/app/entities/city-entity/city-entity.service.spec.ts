import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CityEntityService } from 'app/entities/city-entity/city-entity.service';
import { ICityEntity, CityEntity } from 'app/shared/model/city-entity.model';
import { CityType } from 'app/shared/model/enumerations/city-type.model';

describe('Service Tests', () => {
  describe('CityEntity Service', () => {
    let injector: TestBed;
    let service: CityEntityService;
    let httpMock: HttpTestingController;
    let elemDefault: ICityEntity;
    let expectedResult: ICityEntity | ICityEntity[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(CityEntityService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new CityEntity(0, 'AAAAAAA', CityType.PLAYER, 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a CityEntity', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new CityEntity()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a CityEntity', () => {
        const returnedFromService = Object.assign(
          {
            cityName: 'BBBBBB',
            cityType: 'BBBBBB',
            recruitmentModifier: 1,
            buildSpeedModifier: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of CityEntity', () => {
        const returnedFromService = Object.assign(
          {
            cityName: 'BBBBBB',
            cityType: 'BBBBBB',
            recruitmentModifier: 1,
            buildSpeedModifier: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a CityEntity', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
