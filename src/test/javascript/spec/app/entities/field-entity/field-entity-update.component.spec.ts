import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { FieldEntityUpdateComponent } from 'app/entities/field-entity/field-entity-update.component';
import { FieldEntityService } from 'app/entities/field-entity/field-entity.service';
import { FieldEntity } from 'app/shared/model/field-entity.model';

describe('Component Tests', () => {
  describe('FieldEntity Management Update Component', () => {
    let comp: FieldEntityUpdateComponent;
    let fixture: ComponentFixture<FieldEntityUpdateComponent>;
    let service: FieldEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [FieldEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(FieldEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FieldEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FieldEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FieldEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FieldEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
