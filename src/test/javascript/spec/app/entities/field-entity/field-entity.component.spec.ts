import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { FieldEntityComponent } from 'app/entities/field-entity/field-entity.component';
import { FieldEntityService } from 'app/entities/field-entity/field-entity.service';
import { FieldEntity } from 'app/shared/model/field-entity.model';

describe('Component Tests', () => {
  describe('FieldEntity Management Component', () => {
    let comp: FieldEntityComponent;
    let fixture: ComponentFixture<FieldEntityComponent>;
    let service: FieldEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [FieldEntityComponent],
      })
        .overrideTemplate(FieldEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FieldEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FieldEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FieldEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fieldEntities && comp.fieldEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
