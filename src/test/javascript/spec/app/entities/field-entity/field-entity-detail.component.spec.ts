import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { FieldEntityDetailComponent } from 'app/entities/field-entity/field-entity-detail.component';
import { FieldEntity } from 'app/shared/model/field-entity.model';

describe('Component Tests', () => {
  describe('FieldEntity Management Detail Component', () => {
    let comp: FieldEntityDetailComponent;
    let fixture: ComponentFixture<FieldEntityDetailComponent>;
    const route = ({ data: of({ fieldEntity: new FieldEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [FieldEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(FieldEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FieldEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load fieldEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.fieldEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
