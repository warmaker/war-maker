import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { LandEntityComponent } from 'app/entities/land-entity/land-entity.component';
import { LandEntityService } from 'app/entities/land-entity/land-entity.service';
import { LandEntity } from 'app/shared/model/land-entity.model';

describe('Component Tests', () => {
  describe('LandEntity Management Component', () => {
    let comp: LandEntityComponent;
    let fixture: ComponentFixture<LandEntityComponent>;
    let service: LandEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [LandEntityComponent],
      })
        .overrideTemplate(LandEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(LandEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(LandEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new LandEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.landEntities && comp.landEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
