import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { LandEntityDetailComponent } from 'app/entities/land-entity/land-entity-detail.component';
import { LandEntity } from 'app/shared/model/land-entity.model';

describe('Component Tests', () => {
  describe('LandEntity Management Detail Component', () => {
    let comp: LandEntityDetailComponent;
    let fixture: ComponentFixture<LandEntityDetailComponent>;
    const route = ({ data: of({ landEntity: new LandEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [LandEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(LandEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(LandEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load landEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.landEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
