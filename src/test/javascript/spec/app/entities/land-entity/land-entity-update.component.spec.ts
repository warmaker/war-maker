import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { LandEntityUpdateComponent } from 'app/entities/land-entity/land-entity-update.component';
import { LandEntityService } from 'app/entities/land-entity/land-entity.service';
import { LandEntity } from 'app/shared/model/land-entity.model';

describe('Component Tests', () => {
  describe('LandEntity Management Update Component', () => {
    let comp: LandEntityUpdateComponent;
    let fixture: ComponentFixture<LandEntityUpdateComponent>;
    let service: LandEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [LandEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(LandEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(LandEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(LandEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new LandEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new LandEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
