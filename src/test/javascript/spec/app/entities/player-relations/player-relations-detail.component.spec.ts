import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { PlayerRelationsDetailComponent } from 'app/entities/player-relations/player-relations-detail.component';
import { PlayerRelations } from 'app/shared/model/player-relations.model';

describe('Component Tests', () => {
  describe('PlayerRelations Management Detail Component', () => {
    let comp: PlayerRelationsDetailComponent;
    let fixture: ComponentFixture<PlayerRelationsDetailComponent>;
    const route = ({ data: of({ playerRelations: new PlayerRelations(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [PlayerRelationsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(PlayerRelationsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PlayerRelationsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load playerRelations on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.playerRelations).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
