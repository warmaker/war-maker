import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { PlayerRelationsComponent } from 'app/entities/player-relations/player-relations.component';
import { PlayerRelationsService } from 'app/entities/player-relations/player-relations.service';
import { PlayerRelations } from 'app/shared/model/player-relations.model';

describe('Component Tests', () => {
  describe('PlayerRelations Management Component', () => {
    let comp: PlayerRelationsComponent;
    let fixture: ComponentFixture<PlayerRelationsComponent>;
    let service: PlayerRelationsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [PlayerRelationsComponent],
      })
        .overrideTemplate(PlayerRelationsComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PlayerRelationsComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PlayerRelationsService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new PlayerRelations(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.playerRelations && comp.playerRelations[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
