import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { PlayerRelationsUpdateComponent } from 'app/entities/player-relations/player-relations-update.component';
import { PlayerRelationsService } from 'app/entities/player-relations/player-relations.service';
import { PlayerRelations } from 'app/shared/model/player-relations.model';

describe('Component Tests', () => {
  describe('PlayerRelations Management Update Component', () => {
    let comp: PlayerRelationsUpdateComponent;
    let fixture: ComponentFixture<PlayerRelationsUpdateComponent>;
    let service: PlayerRelationsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [PlayerRelationsUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(PlayerRelationsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PlayerRelationsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PlayerRelationsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new PlayerRelations(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new PlayerRelations();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
