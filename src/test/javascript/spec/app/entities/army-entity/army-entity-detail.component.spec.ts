import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { ArmyEntityDetailComponent } from 'app/entities/army-entity/army-entity-detail.component';
import { ArmyEntity } from 'app/shared/model/army-entity.model';

describe('Component Tests', () => {
  describe('ArmyEntity Management Detail Component', () => {
    let comp: ArmyEntityDetailComponent;
    let fixture: ComponentFixture<ArmyEntityDetailComponent>;
    const route = ({ data: of({ armyEntity: new ArmyEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ArmyEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ArmyEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ArmyEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load armyEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.armyEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
