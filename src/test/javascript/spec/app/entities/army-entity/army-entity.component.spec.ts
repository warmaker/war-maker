import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { ArmyEntityComponent } from 'app/entities/army-entity/army-entity.component';
import { ArmyEntityService } from 'app/entities/army-entity/army-entity.service';
import { ArmyEntity } from 'app/shared/model/army-entity.model';

describe('Component Tests', () => {
  describe('ArmyEntity Management Component', () => {
    let comp: ArmyEntityComponent;
    let fixture: ComponentFixture<ArmyEntityComponent>;
    let service: ArmyEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ArmyEntityComponent],
      })
        .overrideTemplate(ArmyEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ArmyEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ArmyEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ArmyEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.armyEntities && comp.armyEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
