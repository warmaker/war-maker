import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { ArmyEntityUpdateComponent } from 'app/entities/army-entity/army-entity-update.component';
import { ArmyEntityService } from 'app/entities/army-entity/army-entity.service';
import { ArmyEntity } from 'app/shared/model/army-entity.model';

describe('Component Tests', () => {
  describe('ArmyEntity Management Update Component', () => {
    let comp: ArmyEntityUpdateComponent;
    let fixture: ComponentFixture<ArmyEntityUpdateComponent>;
    let service: ArmyEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ArmyEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ArmyEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ArmyEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ArmyEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ArmyEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ArmyEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
