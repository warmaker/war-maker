import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { PointOfInterestEntityComponent } from 'app/entities/point-of-interest-entity/point-of-interest-entity.component';
import { PointOfInterestEntityService } from 'app/entities/point-of-interest-entity/point-of-interest-entity.service';
import { PointOfInterestEntity } from 'app/shared/model/point-of-interest-entity.model';

describe('Component Tests', () => {
  describe('PointOfInterestEntity Management Component', () => {
    let comp: PointOfInterestEntityComponent;
    let fixture: ComponentFixture<PointOfInterestEntityComponent>;
    let service: PointOfInterestEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [PointOfInterestEntityComponent],
      })
        .overrideTemplate(PointOfInterestEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PointOfInterestEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PointOfInterestEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new PointOfInterestEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.pointOfInterestEntities && comp.pointOfInterestEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
