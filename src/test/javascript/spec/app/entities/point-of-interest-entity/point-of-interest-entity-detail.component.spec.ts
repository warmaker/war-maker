import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { PointOfInterestEntityDetailComponent } from 'app/entities/point-of-interest-entity/point-of-interest-entity-detail.component';
import { PointOfInterestEntity } from 'app/shared/model/point-of-interest-entity.model';

describe('Component Tests', () => {
  describe('PointOfInterestEntity Management Detail Component', () => {
    let comp: PointOfInterestEntityDetailComponent;
    let fixture: ComponentFixture<PointOfInterestEntityDetailComponent>;
    const route = ({ data: of({ pointOfInterestEntity: new PointOfInterestEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [PointOfInterestEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(PointOfInterestEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PointOfInterestEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load pointOfInterestEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.pointOfInterestEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
