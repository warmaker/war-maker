import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { PointOfInterestEntityUpdateComponent } from 'app/entities/point-of-interest-entity/point-of-interest-entity-update.component';
import { PointOfInterestEntityService } from 'app/entities/point-of-interest-entity/point-of-interest-entity.service';
import { PointOfInterestEntity } from 'app/shared/model/point-of-interest-entity.model';

describe('Component Tests', () => {
  describe('PointOfInterestEntity Management Update Component', () => {
    let comp: PointOfInterestEntityUpdateComponent;
    let fixture: ComponentFixture<PointOfInterestEntityUpdateComponent>;
    let service: PointOfInterestEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [PointOfInterestEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(PointOfInterestEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PointOfInterestEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PointOfInterestEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new PointOfInterestEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new PointOfInterestEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
