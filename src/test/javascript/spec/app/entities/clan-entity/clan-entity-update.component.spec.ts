import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { ClanEntityUpdateComponent } from 'app/entities/clan-entity/clan-entity-update.component';
import { ClanEntityService } from 'app/entities/clan-entity/clan-entity.service';
import { ClanEntity } from 'app/shared/model/clan-entity.model';

describe('Component Tests', () => {
  describe('ClanEntity Management Update Component', () => {
    let comp: ClanEntityUpdateComponent;
    let fixture: ComponentFixture<ClanEntityUpdateComponent>;
    let service: ClanEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ClanEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ClanEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ClanEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ClanEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ClanEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ClanEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
