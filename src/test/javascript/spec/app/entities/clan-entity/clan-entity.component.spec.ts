import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { ClanEntityComponent } from 'app/entities/clan-entity/clan-entity.component';
import { ClanEntityService } from 'app/entities/clan-entity/clan-entity.service';
import { ClanEntity } from 'app/shared/model/clan-entity.model';

describe('Component Tests', () => {
  describe('ClanEntity Management Component', () => {
    let comp: ClanEntityComponent;
    let fixture: ComponentFixture<ClanEntityComponent>;
    let service: ClanEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ClanEntityComponent],
      })
        .overrideTemplate(ClanEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ClanEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ClanEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ClanEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.clanEntities && comp.clanEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
