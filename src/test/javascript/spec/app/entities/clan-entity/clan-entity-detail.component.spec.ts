import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { WarMakerTestModule } from '../../../test.module';
import { ClanEntityDetailComponent } from 'app/entities/clan-entity/clan-entity-detail.component';
import { ClanEntity } from 'app/shared/model/clan-entity.model';

describe('Component Tests', () => {
  describe('ClanEntity Management Detail Component', () => {
    let comp: ClanEntityDetailComponent;
    let fixture: ComponentFixture<ClanEntityDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ clanEntity: new ClanEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ClanEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ClanEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ClanEntityDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load clanEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.clanEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
