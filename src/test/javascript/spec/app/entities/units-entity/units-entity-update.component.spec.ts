import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { UnitsEntityUpdateComponent } from 'app/entities/units-entity/units-entity-update.component';
import { UnitsEntityService } from 'app/entities/units-entity/units-entity.service';
import { UnitsEntity } from 'app/shared/model/units-entity.model';

describe('Component Tests', () => {
  describe('UnitsEntity Management Update Component', () => {
    let comp: UnitsEntityUpdateComponent;
    let fixture: ComponentFixture<UnitsEntityUpdateComponent>;
    let service: UnitsEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [UnitsEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(UnitsEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UnitsEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UnitsEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UnitsEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UnitsEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
