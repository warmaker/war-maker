import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { UnitsEntityDetailComponent } from 'app/entities/units-entity/units-entity-detail.component';
import { UnitsEntity } from 'app/shared/model/units-entity.model';

describe('Component Tests', () => {
  describe('UnitsEntity Management Detail Component', () => {
    let comp: UnitsEntityDetailComponent;
    let fixture: ComponentFixture<UnitsEntityDetailComponent>;
    const route = ({ data: of({ unitsEntity: new UnitsEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [UnitsEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(UnitsEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UnitsEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load unitsEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.unitsEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
