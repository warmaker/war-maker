import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { UnitsEntityComponent } from 'app/entities/units-entity/units-entity.component';
import { UnitsEntityService } from 'app/entities/units-entity/units-entity.service';
import { UnitsEntity } from 'app/shared/model/units-entity.model';

describe('Component Tests', () => {
  describe('UnitsEntity Management Component', () => {
    let comp: UnitsEntityComponent;
    let fixture: ComponentFixture<UnitsEntityComponent>;
    let service: UnitsEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [UnitsEntityComponent],
      })
        .overrideTemplate(UnitsEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UnitsEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UnitsEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new UnitsEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.unitsEntities && comp.unitsEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
