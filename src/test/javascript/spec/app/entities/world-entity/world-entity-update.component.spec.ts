import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { WorldEntityUpdateComponent } from 'app/entities/world-entity/world-entity-update.component';
import { WorldEntityService } from 'app/entities/world-entity/world-entity.service';
import { WorldEntity } from 'app/shared/model/world-entity.model';

describe('Component Tests', () => {
  describe('WorldEntity Management Update Component', () => {
    let comp: WorldEntityUpdateComponent;
    let fixture: ComponentFixture<WorldEntityUpdateComponent>;
    let service: WorldEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [WorldEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(WorldEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WorldEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(WorldEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new WorldEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new WorldEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
