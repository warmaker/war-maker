import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { WorldEntityComponent } from 'app/entities/world-entity/world-entity.component';
import { WorldEntityService } from 'app/entities/world-entity/world-entity.service';
import { WorldEntity } from 'app/shared/model/world-entity.model';

describe('Component Tests', () => {
  describe('WorldEntity Management Component', () => {
    let comp: WorldEntityComponent;
    let fixture: ComponentFixture<WorldEntityComponent>;
    let service: WorldEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [WorldEntityComponent],
      })
        .overrideTemplate(WorldEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WorldEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(WorldEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new WorldEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.worldEntities && comp.worldEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
