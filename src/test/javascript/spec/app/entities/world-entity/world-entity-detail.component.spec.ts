import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { WorldEntityDetailComponent } from 'app/entities/world-entity/world-entity-detail.component';
import { WorldEntity } from 'app/shared/model/world-entity.model';

describe('Component Tests', () => {
  describe('WorldEntity Management Detail Component', () => {
    let comp: WorldEntityDetailComponent;
    let fixture: ComponentFixture<WorldEntityDetailComponent>;
    const route = ({ data: of({ worldEntity: new WorldEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [WorldEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(WorldEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(WorldEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load worldEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.worldEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
