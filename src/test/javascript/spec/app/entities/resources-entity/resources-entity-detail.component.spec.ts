import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { ResourcesEntityDetailComponent } from 'app/entities/resources-entity/resources-entity-detail.component';
import { ResourcesEntity } from 'app/shared/model/resources-entity.model';

describe('Component Tests', () => {
  describe('ResourcesEntity Management Detail Component', () => {
    let comp: ResourcesEntityDetailComponent;
    let fixture: ComponentFixture<ResourcesEntityDetailComponent>;
    const route = ({ data: of({ resourcesEntity: new ResourcesEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ResourcesEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ResourcesEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ResourcesEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load resourcesEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.resourcesEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
