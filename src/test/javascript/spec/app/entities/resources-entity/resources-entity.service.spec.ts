import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { ResourcesEntityService } from 'app/entities/resources-entity/resources-entity.service';
import { IResourcesEntity, ResourcesEntity } from 'app/shared/model/resources-entity.model';

describe('Service Tests', () => {
  describe('ResourcesEntity Service', () => {
    let injector: TestBed;
    let service: ResourcesEntityService;
    let httpMock: HttpTestingController;
    let elemDefault: IResourcesEntity;
    let expectedResult: IResourcesEntity | IResourcesEntity[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(ResourcesEntityService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new ResourcesEntity(0, 0, 0, 0, 0, 0, 0, currentDate);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            changeDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ResourcesEntity', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            changeDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            changeDate: currentDate,
          },
          returnedFromService
        );

        service.create(new ResourcesEntity()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ResourcesEntity', () => {
        const returnedFromService = Object.assign(
          {
            denars: 1,
            wood: 1,
            stone: 1,
            steel: 1,
            food: 1,
            faith: 1,
            changeDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            changeDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ResourcesEntity', () => {
        const returnedFromService = Object.assign(
          {
            denars: 1,
            wood: 1,
            stone: 1,
            steel: 1,
            food: 1,
            faith: 1,
            changeDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            changeDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ResourcesEntity', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
