import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { ResourcesEntityUpdateComponent } from 'app/entities/resources-entity/resources-entity-update.component';
import { ResourcesEntityService } from 'app/entities/resources-entity/resources-entity.service';
import { ResourcesEntity } from 'app/shared/model/resources-entity.model';

describe('Component Tests', () => {
  describe('ResourcesEntity Management Update Component', () => {
    let comp: ResourcesEntityUpdateComponent;
    let fixture: ComponentFixture<ResourcesEntityUpdateComponent>;
    let service: ResourcesEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ResourcesEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ResourcesEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ResourcesEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ResourcesEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ResourcesEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ResourcesEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
