import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { ResourcesEntityComponent } from 'app/entities/resources-entity/resources-entity.component';
import { ResourcesEntityService } from 'app/entities/resources-entity/resources-entity.service';
import { ResourcesEntity } from 'app/shared/model/resources-entity.model';

describe('Component Tests', () => {
  describe('ResourcesEntity Management Component', () => {
    let comp: ResourcesEntityComponent;
    let fixture: ComponentFixture<ResourcesEntityComponent>;
    let service: ResourcesEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [ResourcesEntityComponent],
      })
        .overrideTemplate(ResourcesEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ResourcesEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ResourcesEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ResourcesEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.resourcesEntities && comp.resourcesEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
