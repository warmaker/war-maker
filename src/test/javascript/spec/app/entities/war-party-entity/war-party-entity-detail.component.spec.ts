import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { WarPartyEntityDetailComponent } from 'app/entities/war-party-entity/war-party-entity-detail.component';
import { WarPartyEntity } from 'app/shared/model/war-party-entity.model';

describe('Component Tests', () => {
  describe('WarPartyEntity Management Detail Component', () => {
    let comp: WarPartyEntityDetailComponent;
    let fixture: ComponentFixture<WarPartyEntityDetailComponent>;
    const route = ({ data: of({ warPartyEntity: new WarPartyEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [WarPartyEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(WarPartyEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(WarPartyEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load warPartyEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.warPartyEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
