import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { WarPartyEntityComponent } from 'app/entities/war-party-entity/war-party-entity.component';
import { WarPartyEntityService } from 'app/entities/war-party-entity/war-party-entity.service';
import { WarPartyEntity } from 'app/shared/model/war-party-entity.model';

describe('Component Tests', () => {
  describe('WarPartyEntity Management Component', () => {
    let comp: WarPartyEntityComponent;
    let fixture: ComponentFixture<WarPartyEntityComponent>;
    let service: WarPartyEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [WarPartyEntityComponent],
      })
        .overrideTemplate(WarPartyEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WarPartyEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(WarPartyEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new WarPartyEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.warPartyEntities && comp.warPartyEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
