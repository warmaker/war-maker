import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { WarPartyEntityUpdateComponent } from 'app/entities/war-party-entity/war-party-entity-update.component';
import { WarPartyEntityService } from 'app/entities/war-party-entity/war-party-entity.service';
import { WarPartyEntity } from 'app/shared/model/war-party-entity.model';

describe('Component Tests', () => {
  describe('WarPartyEntity Management Update Component', () => {
    let comp: WarPartyEntityUpdateComponent;
    let fixture: ComponentFixture<WarPartyEntityUpdateComponent>;
    let service: WarPartyEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [WarPartyEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(WarPartyEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WarPartyEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(WarPartyEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new WarPartyEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new WarPartyEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
