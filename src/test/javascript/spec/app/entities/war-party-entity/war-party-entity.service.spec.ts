import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { WarPartyEntityService } from 'app/entities/war-party-entity/war-party-entity.service';
import { IWarPartyEntity, WarPartyEntity } from 'app/shared/model/war-party-entity.model';
import { WarPartyState } from 'app/shared/model/enumerations/war-party-state.model';

describe('Service Tests', () => {
  describe('WarPartyEntity Service', () => {
    let injector: TestBed;
    let service: WarPartyEntityService;
    let httpMock: HttpTestingController;
    let elemDefault: IWarPartyEntity;
    let expectedResult: IWarPartyEntity | IWarPartyEntity[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(WarPartyEntityService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new WarPartyEntity(0, 0, 0, WarPartyState.MOVING);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a WarPartyEntity', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new WarPartyEntity()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a WarPartyEntity', () => {
        const returnedFromService = Object.assign(
          {
            speed: 1,
            capacity: 1,
            state: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of WarPartyEntity', () => {
        const returnedFromService = Object.assign(
          {
            speed: 1,
            capacity: 1,
            state: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a WarPartyEntity', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
