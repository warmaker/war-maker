import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { LairEntityUpdateComponent } from 'app/entities/lair-entity/lair-entity-update.component';
import { LairEntityService } from 'app/entities/lair-entity/lair-entity.service';
import { LairEntity } from 'app/shared/model/lair-entity.model';

describe('Component Tests', () => {
  describe('LairEntity Management Update Component', () => {
    let comp: LairEntityUpdateComponent;
    let fixture: ComponentFixture<LairEntityUpdateComponent>;
    let service: LairEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [LairEntityUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(LairEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(LairEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(LairEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new LairEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new LairEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
