import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WarMakerTestModule } from '../../../test.module';
import { LairEntityDetailComponent } from 'app/entities/lair-entity/lair-entity-detail.component';
import { LairEntity } from 'app/shared/model/lair-entity.model';

describe('Component Tests', () => {
  describe('LairEntity Management Detail Component', () => {
    let comp: LairEntityDetailComponent;
    let fixture: ComponentFixture<LairEntityDetailComponent>;
    const route = ({ data: of({ lairEntity: new LairEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [LairEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(LairEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(LairEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load lairEntity on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.lairEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
