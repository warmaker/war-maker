import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { WarMakerTestModule } from '../../../test.module';
import { LairEntityComponent } from 'app/entities/lair-entity/lair-entity.component';
import { LairEntityService } from 'app/entities/lair-entity/lair-entity.service';
import { LairEntity } from 'app/shared/model/lair-entity.model';

describe('Component Tests', () => {
  describe('LairEntity Management Component', () => {
    let comp: LairEntityComponent;
    let fixture: ComponentFixture<LairEntityComponent>;
    let service: LairEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WarMakerTestModule],
        declarations: [LairEntityComponent],
      })
        .overrideTemplate(LairEntityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(LairEntityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(LairEntityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new LairEntity(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.lairEntities && comp.lairEntities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
