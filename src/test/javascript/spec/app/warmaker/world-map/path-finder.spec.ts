import { FieldType } from 'app/shared/model/field.model';
import { D3HexField } from 'app/war-maker/world-map/map/value/d3-hex-field';
import { Coordinates } from 'app/war-maker/world-map/map/value/coordinates';
import { PathFinder } from 'app/war-maker/world-map/path-finder/path-finder';
import { FieldDto } from 'app/war-maker/world-map/map/value/field-dto';
import { RelationType } from 'app/shared/model/enumerations/relation-type.model';

fdescribe('PathFinder', () => {
    let pathFinder: PathFinder;
    let map: D3HexField[][] = [];

    function createMapWithConstantModifier(rows: number, columns: number, modifier?: number): D3HexField[][] {
        const fields: D3HexField[][] = [];
        for (let i = 0; i < rows; i++) {
            const elemRow: D3HexField[] = [];
            for (let k = 0; k < columns; k++) {
                elemRow.push(new D3HexField(new Coordinates(i, k), modifier ?  new FieldDto(i, k, modifier, FieldType.CITY, RelationType.NEUTRAL, 0) : new FieldDto(i, k, 1, FieldType.CITY, RelationType.NEUTRAL, 0)));
            }
            fields.push(elemRow);
        }
        return fields;
    }

    function setModifiers(fields: D3HexField[], modifier: number): void {
        fields.forEach(field => (field.fieldData.modifier = modifier));
    }

    beforeEach(() => {
        map = createMapWithConstantModifier(6, 6);
        pathFinder = new PathFinder(map);
    });

    it('should properly calculate distance between corners of map without modifiers', () => {
        const shortestPath = pathFinder.getShortestPath(
            new D3HexField(new Coordinates(0, 0), new FieldDto(0, 0, 0, FieldType.CITY, RelationType.NEUTRAL, 0)),
            new D3HexField(new Coordinates(5, 5),  new FieldDto(5, 5, 0, FieldType.CITY, RelationType.NEUTRAL, 0))
        );
        const arrayLength = shortestPath.length;
        expect(arrayLength).toBeGreaterThan(0);
        expect(shortestPath[0]).toEqual(map[5][5]);
        expect(shortestPath[arrayLength - 1]).toEqual(map[0][0]);
    });

    it('should pass around areas with high modifiers', () => {
        setModifiers([map[0][1], map[1][1], map[2][1], map[3][1], map[5][2], map[5][3], map[4][3]], 100);
        const shortestPath = pathFinder.getShortestPath(
            new D3HexField(new Coordinates(0, 0), new FieldDto(0, 0, 0, FieldType.CITY, RelationType.NEUTRAL, 0)),
            new D3HexField(new Coordinates(5, 5),  new FieldDto(5, 5, 0, FieldType.CITY, RelationType.NEUTRAL, 0))
        );
        const arrayLength = shortestPath.length;
        expect(arrayLength).toBeGreaterThan(0);
        expect(shortestPath[0]).toEqual(map[5][5]);
        expect(shortestPath[arrayLength - 1]).toEqual(map[0][0]);
    });
});
