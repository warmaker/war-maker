# WarMaker

Hello to the WarMaker! 

## Application deployment

### Full enviroment with Message Box micro service

* Clone MessageBox repository https://gitlab.com/warmaker/message-box 
    * it should be at the same directory level as WarMaker
* At the level od WarMaker repository - go to the **/tools** directory 
* Run **./set-up-enviroment.sh start**
    * sometimes kafka will not get up in time so command should be run again
* Run WarMaker backend from **./gradlew bootRun** or from Intellij IDE
* Run WarMaker frontend with **npm start** 


### Enviroment without Message Box micro service
* At the level od WarMaker repository - go to the **/tools** directory 
* Run **./set-up-enviroment.sh start-incomplete**
* Run WarMaker backend from **./gradlew bootRun** or from Intellij IDE
* Run WarMaker frontend with **npm start** 
